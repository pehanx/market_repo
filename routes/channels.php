<?php
use App\Helpers\Chat\ChatService;
/*
|--------------------------------------------------------------------------
| Broadcast Channels
|--------------------------------------------------------------------------
|
| Here you may register all of the event broadcasting channels that your
| application supports. The given channel authorization callbacks are
| used to check if an authenticated user can listen to the channel.
|
*/

/* Broadcast::channel('App.User.{id}', function ($user, $id) {
    return (int) $user->id === (int) $id;
}); */

Broadcast::channel('chat.{profileId}.{userHash}', function ($user, $profileId, $userHash) {
    $service = new ChatService;
    return ($service::getHash($profileId) === $userHash);
});

Broadcast::channel('support.chat.{userHash}', function ($user, $userHash) {
    $service = new ChatService;
    return ($service::getHash($user->id, 'support') === $userHash);
});


