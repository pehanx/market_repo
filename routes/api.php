<?php

use Illuminate\Http\Request;
//use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::group(
    [
        'namespace' => 'Api',
    ],
    
    function () {
        Route::group(
            [
                'namespace' => 'User',
                'prefix' => 'user', 
            ],
        
            function () {
                Route::post('/favorite/{type}/{id}', 'FavoriteController@add')->name('favorite.add'); 
                Route::delete('/favorite/{type}/{id}', 'FavoriteController@remove')->name('favorite.remove'); 
                Route::post('/upload/{type}/image', 'UploadController@image')->name('upload.image'); 
                Route::delete('/upload/{type}/{id}', 'UploadController@remove')->name('upload.remove'); 

                //Чат
                Route::post('/chat/support/{chat}/message','ChatController@sendSupport')->where('chat', '[0-9]+')->name('chat.support');

                Route::get('/chat/support/get','ChatController@getChats')->name('chat.get');

                Route::post('/chat/contact/{contact}','ChatController@addToContact')->where('contact', '[0-9]+')->name('contact.add'); // Чтобы не передавать active_profile

                Route::post('/chat','ChatController@createChat')->name('chat.create');

                Route::post('/chat/disconnect','ChatController@chatClosed')->name('chat.disconnect'); // SendBeacon only post

                Route::get('/chat/connect','ChatController@chatOpened')->name('chat.connect'); //

                Route::post('/chat/support/search','ChatController@searchUsers')->name('chat.search');

                Route::get('/chat/support/{chat}/messages','ChatController@getMessages')->where('chat', '[0-9]+')->name('chat.messages');

                Route::post('/chat/message/translate','ChatController@translateSelectedMessage')->name('chat.translate'); //

                Route::get('/chat/settings','ChatController@getSettings')->name('chat.settings');

                Route::post('/chat/settings','ChatController@chatSettingsSave')->name('chat.settings.save');

                Route::delete('/chat/{chat}/message/{message}','ChatController@deleteMessage')->where('chat', '[0-9]+')->where('message', '[0-9]+')->name('chat.messages.delete'); //
            }
        );

        Route::group(
            [
                'namespace' => 'Profile',
                'prefix' => '{profile}', 
                'as' => 'profile.',
            ],
        
            function () {
                //Обновление данных в табличке с экспортами
                Route::get('/export/update','ExportController@updateIndex')->name('export.update');
                Route::get('/export/download/{export}','ExportController@downloadAll')->where('export', '[0-9]+')->name('export.download');
                Route::post('/import/upload/{type}','ImportController@importUpload')->name('import.upload');
                
                //Чат
                Route::get('/chat/{chat}/messages','ChatController@getMessages')->where('chat', '[0-9]+')->name('chat.messages');

                Route::post('/chat/{chat}/message','ChatController@sendPush')->where('chat', '[0-9]+')->name('chat.send');

                Route::post('/chat/create','ChatController@createChat')->name('chat.create');

                Route::delete('/chat/contact/{contact}','ChatController@removeContact')->where('contact', '[0-9]+')->name('contact.remove');

                Route::get('/chat/contact{contact}/write','ChatController@writeToContact')->where('contact', '[0-9]+')->name('contact.write');

                Route::get('/chat/contact','ChatController@getContacts')->name('contact.get');

                Route::get('/chat','ChatController@getChats')->name('chat.get');

                Route::post('/chat/search','ChatController@profileSearch')->name('chat.search');

                Route::delete('/chat/{chat}','ChatController@deleteChat')->where('chat', '[0-9]+')->name('chat.delete');
            }
        );

        Route::group(
            [
                'namespace' => 'Mobile',
                'prefix' => 'mobile', 
                'as' => 'mobile.',
            ],
        
            function () {
                Route::post('/register','RegistrationController@createUser')->name('register.create');
                Route::get('/categories','CategoryController@get')->name('categories.get');
                Route::get('/categories/list','CategoryController@getList')->name('categories.list');
                Route::get('/catalog','CatalogController@get')->name('catalog.get');
                Route::get('/catalog/product','ProductController@get')->name('product.get');

                Route::group(
                    [
                        'middleware' => 'auth:api'
                    ],
                
                    function () {

                        Route::get('/favorite/{type}','FavoriteController@get')->name('favorite.get');
                        Route::get('/favorite/{type}/remove','FavoriteController@remove')->name('favorite.remove');
                        Route::get('/favorite/{type}/add','FavoriteController@add')->name('favorite.add');

                        Route::post('/device/save','AccountController@saveDevice')->name('device.add');

                        Route::group(
                            [
                                'prefix' => 'cabinet'
                            ],
                        
                            function () {
                                Route::get('/profile/{type}','ProfileController@get')->name('profile.get');
                                Route::post('/create/profile','ProfileController@create')->name('profile.create');
                                Route::post('/approve/profile','ProfileController@approve')->name('profile.approve');
                                Route::get('/profile/index/{id}','ProfileController@index')->name('profile.index');

                                Route::post('/create/product/{profile}','ProductController@create')->name('product.create');
                                Route::get('/products/{profile}','ProductController@list')->name('product.list');
                                Route::get('/product/{product}','ProductController@getCabinet')->name('api.product.index');

                                Route::get('/account','AccountController@get')->name('account.get');
                                Route::post('/account/edit','AccountController@edit')->name('account.edit');                                

                                Route::get('/chat/get','ChatController@getAll')->name('chat.get');
                                Route::get('/chat/get/support/{chat}','ChatController@getSupportMessages')->name('chat.support');
                                Route::post('/chat/send/support/{chat}','ChatController@sendSupport')->name('chat.support.send');
                                Route::get('/chat/get/profile/{chat}','ChatController@getMessages')->name('chat.profile');
                                Route::post('/chat/send/profile/{chat}','ChatController@send')->name('chat.profile.send');
                            });

                        Route::group(
                        [
                            'middleware' => 'role:site_manager',
                        ],
                        function() {
                            Route::get('/companies', 'CompanyController@companyAll')->name('manager.companies');
                            Route::get('/company/profiles/', 'CompanyController@indexAll')->name('company.list');
                            Route::get('/company/create', 'CompanyController@create')->name('company.create');
                            Route::post('/company/store', 'CompanyController@store')->name('company.store');
                            Route::post('/company/invite', 'CompanyController@invite')->name('company.invite');
                            Route::get('/company/{company}/entrance/edit', 'CompanyController@edit')->where('company', '[0-9]+')->name('company.edit');
                            Route::post('/company/{company}/entrance', 'CompanyController@update')->where('company', '[0-9]+')->name('company.update');
                            Route::get('/company/{company}/profile/create/{type}', 'CompanyController@createProfile')->where('type', 'seller|buyer')->name('company.profile_create');
                            Route::get('/company/approval/{profile}', 'CompanyController@approval')->where('profile', '[0-9]+')->name('company.approve');
                        });
                    }
                );
            }
        );
    }
);   