<?php

Route::group(
[
    'prefix' => LaravelLocalization::setLocale(),
    'middleware' => ['localizationRedirect', 'localeSessionRedirect']
], 

function() {
    // Главная страница
    Route::get('/', 'MainController@index')->name('main');
    Route::get('/invite', 'MainController@loginByInvite')->name('invite.login');
    //Route::get('/passport', 'TestController@test')->name('passport.create');
    // Авторизация, регистрация, сброс пароля.
    Auth::routes(['verify' => false]);
    Route::get('/login/{network}', 'Auth\NetworkController@redirect')->name('login.network');
    Route::get('/login/{network}/callback', 'Auth\NetworkController@callback');

    // Установка валюты
    Route::get('currency/change/{currency}', 'CurrencyController@setCurrency')->where('currency', 'rub|usd|cny')->name('currency');

    // Установка локализации
    Route::get('localisation/{localisation}', 'LocalisationController@setLocalisation')->where('localisation', implode('|', config('app.locales')) )->name('localisation');

    // Личный кабинет
    Route::group(
        [
            'prefix' => 'cabinet',
            'namespace' => 'Cabinet',
            'as' => 'cabinet.',
            'middleware' => ['auth'],
        ],
        
        function () 
        {
            //Написать поставщику
            Route::get('/chat/member/{member}', 'ChatController@writeMember')->where('member', '[0-9]+')->name('chat.member');
            
            // Точка входа в личный кабинет с аутентификацией прав пользователя
            Route::get('/', 'HomeController@index')->name('home');
            
            // Личный аккаунт пользователя
            Route::resource('/account', 'AccountController')->only(['show', 'edit', 'update']);
            Route::get('/account/{account}/entrance/edit', 'AccountController@editEntrance')->where('account', '[0-9]+')->name('account.edit_entrance');
            Route::post('/account/{account}/entrance', 'AccountController@updateEntrance')->where('account', '[0-9]+')->name('account.update_entrance');
            
            // Профили пользователя
            Route::resource('/profile', 'ProfileController')->except(['index', 'create']);
            Route::get('/profile/index/{type}', 'ProfileController@index')->where('type', 'seller|buyer')->name('profile.index');
            Route::get('/profile/create/{type}', 'ProfileController@create')->where('type', 'seller|buyer')->name('profile.create');
            Route::get('/profile/{profile}/approve', 'ProfileController@approve')->where('profile', '[0-9]+')->name('profile.approve');
            Route::post('/profile/approval/{profile}', 'ProfileController@approval')->where('profile', '[0-9]+')->name('profile.approval');
            Route::get('/profile/{profile}/documents', 'ProfileController@showDocuments')->where('profile', '[0-9]+')->name('profile.show_documents');
            Route::get('/profile/{profile}/select', 'ProfileController@selectActiveProfile')->where('profile', '[0-9]+')->name('profile.select');
            Route::get('/support', 'ChatController@viewWithoutProfile')->name('chat.support');

            Route::group(['prefix' => '/{profile}', 'where' => ['profile' => '[0-9]+']], function () {
                // Менеджеры, привязанные к профилю пользователя
                Route::resource('/manager', 'ManagerController')->except(['show']);
                Route::get('/manager/{manager}/entrance/edit', 'ManagerController@editEntrance')->where('manager', '[0-9]+')->name('manager.edit_entrance');
                Route::post('/manager/{manager}/entrance', 'ManagerController@updateEntrance')->where('manager', '[0-9]+')->name('manager.update_entrance');
                
                // Товары, привязанные к профилю пользователя
                Route::resource('/product', 'ProductController')->except(['create']);
                Route::get('/product/create/choose', 'ProductController@chooseCategory')->name('product.create.choose_category');
                Route::get('/product/create/{category}/fill', 'ProductController@fillProperties')->where('category', '[0-9]+')->name('product.create.fill_properties');
                Route::get('/product/{product}/add', 'ProductController@addModel')->where('product', '[0-9]+')->name('product.add_model');
                Route::post('/product/{product}', 'ProductController@storeModel')->where('product', '[0-9]+')->name('product.store_model');
                Route::get('/product/index/inactive', 'ProductController@indexInactive')->name('product.index.inactive');
                Route::get('/product/index/deleted', 'ProductController@indexDeleted')->name('product.index.deleted');
                Route::get('/product/{product}/{model}/edit', 'ProductController@editModel')->where(['product' => '[0-9]+', 'model' => '[0-9]+'])->name('product.edit.model');
                Route::put('/product/{product}/{model}', 'ProductController@updateModel')->where(['product' => '[0-9]+', 'model' => '[0-9]+'])->name('product.update.model');
                Route::post('/product/disable/{product}', 'ProductController@disable')->where('product', '[0-9]+')->name('product.disable');
                Route::get('/product/activate/{product}', 'ProductController@activate')->where('product', '[0-9]+')->name('product.activate');
                Route::get('/product/restore/{product}', 'ProductController@restore')->where('product', '[0-9]+')->name('product.restore');
                Route::get('/product/restore/{product}/{model}', 'ProductController@restoreModel')->where(['product' => '[0-9]+', 'model' => '[0-9]+'])->name('product.restore.model');
                Route::delete('/product/delete/{product}', 'ProductController@delete')->where('product', '[0-9]+')->name('product.delete');
                Route::delete('/product/delete/{product}/{model}', 'ProductController@deleteModel')->where(['product' => '[0-9]+', 'model' => '[0-9]+'])->name('product.delete.model');
                Route::delete('/product/{product}/{model}', 'ProductController@destroyModel')->where(['product' => '[0-9]+', 'model' => '[0-9]+'])->name('product.destroy.model');
                Route::post('/product/action/make', 'ProductController@makeAction')->name('product.action');
                
                // Запросы на покупку, привязанные к профилю пользователя
                Route::resource('/request_product', 'RequestProductController')->except(['create']);
                Route::get('/request_product/create/choose', 'RequestProductController@chooseCategory')->name('request_product.create.choose_category');
                Route::get('/request_product/create/{category}/fill', 'RequestProductController@fillProperties')->where('category', '[0-9]+')->name('request_product.create.fill_properties');
                Route::post('/request_product/disable/{request_product}', 'RequestProductController@disable')->where('request_product', '[0-9]+')->name('request_product.disable');
                Route::get('/request_product/activate/{request_product}', 'RequestProductController@activate')->where('request_product', '[0-9]+')->name('request_product.activate');
                Route::delete('/request_product/delete/{request_product}', 'RequestProductController@delete')->where('request_product', '[0-9]+')->name('request_product.delete');
                Route::get('/request_product/restore/{request_product}', 'RequestProductController@restore')->where('request_product', '[0-9]+')->name('request_product.restore');
                Route::get('/request_product/index/inactive', 'RequestProductController@indexInactive')->name('request_product.index.inactive');
                Route::get('/request_product/index/deleted', 'RequestProductController@indexDeleted')->name('request_product.index.deleted');
                
                Route::post('/request_product/action/make', 'RequestProductController@makeAction')->name('request_product.action');
                
                // Экспорт
                Route::get('/export', 'ExportController@index')->name('export.index'); //ок
                Route::get('/export/create/{type}', 'ExportController@create')->name('export.create.all'); //ок
                
                Route::post('/export/page/{type}/{status}', 'ExportController@storePage')->name('export.store.page');
                Route::post('/export/all', 'ExportController@storeAll')->name('export.store.all');
                
                // Обновление данных в табличке с экспортами
                Route::get('/export/update','ExportController@updateIndex')->name('export.update');
                Route::get('/export/all/download/{export}','ExportController@download')->name('export.download');
                
                //Вьюшка с импортами
                Route::get('/import/settings/{type}/{import_id}', 'ImportController@importSettings')->where('import_id','[0-9]+')->name('import.settings');
                Route::get('/import', 'ImportController@index')->name('import.index');
                Route::get('/import/create/{type}', 'ImportController@create')->name('import.create');
                
                //Запуск импорта продуктов/запросов
                Route::get('/import/examples/table/{type}/', 'ImportController@getExampleFile')->name('import.example_file');
                Route::get('/import/examples/{type}', 'ImportController@getExampleLists')->name('import.example_lists');
                Route::post('/import/request_product/', 'ImportController@requestProductStore')->name('import.request_product');
                Route::post('/import/product/', 'ImportController@productStore')->name('import.product');
                
                //Чат
                Route::get('/chat/{tab?}', 'ChatController@view')->name('chat.index')->where('tab', 'contact|chat|search');
            });
            
            // Менеджеры, привязанные ко всем профилям пользователя
            Route::get('/{user}/managers', 'ManagerController@indexAll')->where('user', '[0-9]+')->name('manager.index_all');

            Route::group(
                [
                    'middleware' => ['auth', 'role:site_manager'],
                ],
                function() {
                    Route::get('/{user}/managers/companies', 'CompanyController@companyAll')->where('user', '[0-9]+')->name('manager.companies');
                    Route::get('/company/{user}/', 'CompanyController@indexAll')->where('user', '[0-9]+')->name('company.list');
                    Route::get('/company/create', 'CompanyController@create')->name('company.create');
                    Route::post('/company/store', 'CompanyController@store')->name('company.store');
                    Route::post('/company/invite', 'CompanyController@invite')->name('company.invite');
                    Route::get('/company/{company}/entrance/edit', 'CompanyController@edit')->where('company', '[0-9]+')->name('company.edit');
                    Route::post('/company/{company}/entrance', 'CompanyController@update')->where('company', '[0-9]+')->name('company.update');
                    Route::get('/company/{company}/profile/create/{type}', 'CompanyController@createProfile')->where('type', 'seller|buyer')->name('company.profile_create');
                    Route::get('/company/approval/{profile}', 'CompanyController@approval')->where('profile', '[0-9]+')->name('company.approve');
                });
            
            // Избранное
            Route::get('/{user}/favorites/{type}', 'FavoriteController@index')->where(['user' => '[0-9]+', 'type' => 'product|request_product'])->name('favorite.index');
            Route::delete('/{user}/favorites/{type}/{id}', 'FavoriteController@remove')->where(['user' => '[0-9]+', 'type' => 'product|request_product', 'id' => '[0-9]+'])->name('favorite.remove');   
        }
    );

    // Кабинет администратора
    Route::group(
        [
            //huy
            'prefix' => 'admin',
            'namespace' => 'Admin',
            'as' => 'admin.',
            'middleware' => ['auth', 'role:admin'],
        ],
        
        function () {
            Route::get('/', 'HomeController@index')->name('home');
            Route::post('/testRole', 'UsersController@addRole')->name('role.add');
            Route::get('/login/as', 'UsersController@login')->name('login.as');
            Route::resource('/users', 'UsersController');
            Route::resource('/profiles', 'ProfilesController');
            Route::get('/chat', 'ChatController@view')->name('chat.support');
            Route::get('/chat/user/{user}/', 'ChatController@writeToUser')->name('chat.user');
            Route::resource('/pages', 'PagesController')->except(['destroy']);
            Route::post('/profiles/approval/{profile}', 'ProfilesController@approval')->where('profile', '[0-9]+')->name('profiles.approval');
            Route::post('/profiles/disable/{profile}', 'ProfilesController@disable')->where('profile', '[0-9]+')->name('profiles.disable');
            
            Route::group(['prefix' => 'pages/{page}', 'as' => 'pages.'], function () {
                Route::post('/first', 'PagesController@first')->name('first');
                Route::post('/up', 'PagesController@up')->name('up');
                Route::post('/down', 'PagesController@down')->name('down');
                Route::post('/last', 'PagesController@last')->name('last');
                Route::get('/add/properties', 'PagesController@addProperties')->name('add_properties');
                Route::post('/properties', 'PagesController@storeProperties')->name('store_properties');
                Route::post('/disable', 'PagesController@disable')->name('disable');
                Route::get('/activate', 'PagesController@activate')->name('activate');
                Route::delete('/delete', 'PagesController@delete')->name('delete');
                Route::get('/edit/properties/{localisation}', 'PagesController@editProperties')->where('localisation', '[a-z]+')->name('edit.properties');
                Route::post('/properties/{localisation}', 'PagesController@updateProperties')->where('localisation', '[a-z]+')->name('update.properties');
                Route::delete('/properties/{localisation}', 'PagesController@deleteProperties')->where('localisation', '[a-z]+')->name('delete.properties');
            });
        }
    );

    // Каталог
    Route::group(
        [
            'prefix' => 'catalog',
            'namespace' => 'Catalog',
            'as' => 'catalog.'
        ],
        
        function () {
            Route::get('/', 'MainController@index')->name('index'); // Изменить роутинг для товара и запроса отдельно
            
            Route::get('/products/{category_path}', 'ProductController@index')->name('product.index');
            Route::get('/product/{product}', 'ProductController@show')->where('product', '[0-9]+')->name('product.show');

            Route::post('/product/{product}', 'ProductController@writeSupplierForm')->where('product', '[0-9]+')->name('product.write_supplier');
            
            Route::get('/request_products/{category_path?}', 'RequestProductController@index')->name('request_product.index');
            Route::get('/request_product/{request_product}', 'RequestProductController@show')->where('request_product', '[0-9]+')->name('request_product.show');
        }
    );
    
    // Магазины поставщиков
    Route::group(
        [
            'prefix' => 'shop/{localisation}',
            'namespace' => 'Shop',
            'where' => [ 'localisation' => implode('|', config('app.locales')) ],
            'as' => 'shop.'
        ],
        
        function () {

            Route::get('/{user}', 'MainController@show')->name('show')->where('user', '[0-9]+');
            Route::get('/{user}/contact', 'ContactController@show')->name('contact.show')->where('user', '[0-9]+')->middleware('auth');
            Route::get('/{user}/products/{category_path?}', 'ProductController@index')->name('product.index')->where('user', '[0-9]+'); // TODO: сменить category_path на group_path как понадобиться группировка кастомная в магазине
        }
    );

    // Поиск по сайту
    Route::get('/search', 'MainController@indexSearchResult')->name('search.index');

    // Для тестирования
    Route::group(['middleware' => ['auth']], function () {
        //Route::get('/test', 'TestController@index');
        Route::post('/upload', 'TestController@upload');
         
    });

    Route::get('/logout', 'Auth\LoginController@logout');

    //Route::get('/translate/{text}/{code}', 'TranslateController@translateArray');
    
    Route::get('/{page_path}', 'PageController@show')->name('page')->where('page_path', '.+');
});