<?php

    use App\Models\{Profile, Product, Localisation, Category, RequestProduct};
    use App\User;
    use App\Http\Router\{CategoryPath, PagePath};
    use DaveJamesMiller\Breadcrumbs\BreadcrumbsGenerator as Crumbs;

    // Кабинет
    Breadcrumbs::register('cabinet.home', function (Crumbs $crumbs) {
        $crumbs->push(__('breadcrumbs.cabinet.cabinet'), route('cabinet.home'));
    });
     
    // Аккаунт
    Breadcrumbs::register('cabinet.account.edit', function (Crumbs $crumbs,  string $profileId) {
        $crumbs->parent('cabinet.home');
        $crumbs->push(__('breadcrumbs.cabinet.private_information'), route('cabinet.account.edit', $profileId));
    });
    
    Breadcrumbs::register('cabinet.account.edit_entrance', function (Crumbs $crumbs,  string $profileId) {
        $crumbs->parent('cabinet.home');
        $crumbs->push(__('breadcrumbs.cabinet.entrance_information'), route('cabinet.account.edit_entrance', $profileId));
    });
    
    // Профили
    Breadcrumbs::register('cabinet.profile.index', function (Crumbs $crumbs, string $type) {
        $crumbs->parent('cabinet.home');
        $crumbs->push(__("breadcrumbs.cabinet.{$type}"), route('cabinet.profile.index', ['type' => $type]));
    });

    Breadcrumbs::register('cabinet.profile.create', function (Crumbs $crumbs, string $type) {
        $crumbs->parent('cabinet.profile.index', $type);
        $crumbs->push(__("breadcrumbs.cabinet.creating_profile"), route('cabinet.profile.create', ['type' => $type]));
    });
    
    Breadcrumbs::register('cabinet.profile.edit', function (Crumbs $crumbs, string $profileId) {
        $profile = Profile::find($profileId);
        $crumbs->parent('cabinet.profile.index', $profile->profileTypes->code);
        $crumbs->push(__('breadcrumbs.cabinet.editing_profile'));
    });
    
    Breadcrumbs::register('cabinet.profile.show', function (Crumbs $crumbs, string $profileId) {
        $profile = Profile::find($profileId);

        $crumbs->parent('cabinet.profile.index', $profile->profileTypes->code);

        $profileName = $profile->profileProperties->where('code', 'company_name')->pluck('value')->shift();

        $crumbs->push($profileName, route('cabinet.profile.show', $profileId));
    });

    Breadcrumbs::register('cabinet.profile.show_documents', function (Crumbs $crumbs, string $profileId) {
        $crumbs->parent('cabinet.profile.show', $profileId);
    });
    
    Breadcrumbs::register('cabinet.profile.approve', function (Crumbs $crumbs, Profile $profile) {
        $crumbs->parent('cabinet.profile.show', $profile->id);
    });

    // Менеджеры
    Breadcrumbs::register('cabinet.manager.index_all', function (Crumbs $crumbs) {
        $crumbs->parent('cabinet.home');
        $crumbs->push(__('breadcrumbs.cabinet.all_managers'));
    });
    
    Breadcrumbs::register('cabinet.manager.index', function (Crumbs $crumbs,  Profile $profile) {
        $crumbs->parent('cabinet.profile.show', $profile->id);
    });
    
    Breadcrumbs::register('cabinet.manager.create', function (Crumbs $crumbs,  Profile $profile) {
        $crumbs->parent('cabinet.profile.show', $profile->id);
        $crumbs->push(__('breadcrumbs.cabinet.profile_managers'), route('cabinet.manager.index', $profile->id));
        $crumbs->push(__('breadcrumbs.cabinet.new_manager'));
    });
    
    Breadcrumbs::register('cabinet.manager.edit', function (Crumbs $crumbs,  Profile $profile, Profile $manager) {
        $crumbs->parent('cabinet.profile.show', $profile->id);
        $crumbs->push(__('breadcrumbs.cabinet.profile_managers'), route('cabinet.manager.index', $profile->id));
        $crumbs->push($manager->users->first()->name);
    });
    
    Breadcrumbs::register('cabinet.manager.edit_entrance', function (Crumbs $crumbs,  Profile $profile, Profile $manager) {
        $crumbs->parent('cabinet.profile.show', $profile->id);
        $crumbs->push(__('breadcrumbs.cabinet.profile_managers'), route('cabinet.manager.index', $profile->id));
        $crumbs->push($manager->users->first()->name);
    });
    
    // Товары
    Breadcrumbs::register('cabinet.product.index', function (Crumbs $crumbs) {
        $crumbs->parent('cabinet.home');
        $crumbs->push(__('breadcrumbs.cabinet.all_products'));
    });
    
    Breadcrumbs::register('cabinet.product.index.inactive', function (Crumbs $crumbs) {
        $crumbs->parent('cabinet.home');
        $crumbs->push(__('breadcrumbs.cabinet.all_products'));
    });
    
    Breadcrumbs::register('cabinet.product.index.deleted', function (Crumbs $crumbs) {
        $crumbs->parent('cabinet.home');
        $crumbs->push(__('breadcrumbs.cabinet.all_products'));
    });
    
    Breadcrumbs::register('cabinet.product.create.choose_category', function (Crumbs $crumbs, Profile $profile) {
        $crumbs->parent('cabinet.home');
        $crumbs->push(__('breadcrumbs.cabinet.all_products'), route('cabinet.product.index', $profile->id));
        $crumbs->push(__('breadcrumbs.cabinet.creating_product'));
    });
    
    Breadcrumbs::register('cabinet.product.create.fill_properties', function (Crumbs $crumbs, Profile $profile) {
        $crumbs->parent('cabinet.home');
        $crumbs->push(__('breadcrumbs.cabinet.all_products'), route('cabinet.product.index', $profile->id));
        $crumbs->push(__('breadcrumbs.cabinet.creating_product'));
    });
    
    Breadcrumbs::register('cabinet.product.show', function (Crumbs $crumbs, Profile $profile, Product $product) {
        $crumbs->parent('cabinet.home');
        $crumbs->push(__('breadcrumbs.cabinet.all_products'), route('cabinet.product.index', $profile->id));
        //$crumbs->push($product->productProperties->where('code', 'name')->first()->value);
        $crumbs->push(request()->filled('steps') ? __('breadcrumbs.cabinet.creating_product') : __('breadcrumbs.cabinet.show_product'));
    });
    
    Breadcrumbs::register('cabinet.product.add_model', function (Crumbs $crumbs, Profile $profile, Product $product) {
        $crumbs->parent('cabinet.product.show', $profile, $product);
    });
    
    Breadcrumbs::register('cabinet.product.edit', function (Crumbs $crumbs, Profile $profile) {
        $crumbs->parent('cabinet.home');
        $crumbs->push(__('breadcrumbs.cabinet.all_products'), route('cabinet.product.index', $profile->id));
        $crumbs->push(__('breadcrumbs.cabinet.editing_product'));
    });
    
    Breadcrumbs::register('cabinet.product.edit.model', function (Crumbs $crumbs, Profile $profile) {
        $crumbs->parent('cabinet.home');
        $crumbs->push(__('breadcrumbs.cabinet.all_products'), route('cabinet.product.index', $profile->id));
        $crumbs->push(__('breadcrumbs.cabinet.editing_product_model'));
    });
    
    // Запросы на покупку
    Breadcrumbs::register('cabinet.request_product.index', function (Crumbs $crumbs) {
        $crumbs->parent('cabinet.home');
        $crumbs->push(__('breadcrumbs.cabinet.all_request_products'));
    });
    
    Breadcrumbs::register('cabinet.request_product.index.inactive', function (Crumbs $crumbs) {
        $crumbs->parent('cabinet.home');
        $crumbs->push(__('breadcrumbs.cabinet.all_request_products'));
    });
    
    Breadcrumbs::register('cabinet.request_product.index.deleted', function (Crumbs $crumbs) {
        $crumbs->parent('cabinet.home');
        $crumbs->push(__('breadcrumbs.cabinet.all_request_products'));
    });
    
    Breadcrumbs::register('cabinet.request_product.show', function (Crumbs $crumbs, Profile $profile, RequestProduct $requestProduct) {
        $crumbs->parent('cabinet.home');
        $crumbs->push(__('breadcrumbs.cabinet.all_request_products'), route('cabinet.request_product.index', $profile->id));
        $crumbs->push(__('breadcrumbs.cabinet.show_request_product'));
        //$crumbs->push($requestProduct->requestProductProperties->where('code', 'name')->first()->value);
    });
    
    Breadcrumbs::register('cabinet.request_product.create.choose_category', function (Crumbs $crumbs, Profile $profile) {
        $crumbs->parent('cabinet.home');
        $crumbs->push(__('breadcrumbs.cabinet.all_request_products'), route('cabinet.request_product.index', $profile->id));
        $crumbs->push(__('breadcrumbs.cabinet.creating_request_product'));
    });
    
    Breadcrumbs::register('cabinet.request_product.create.fill_properties', function (Crumbs $crumbs, Profile $profile) {
        $crumbs->parent('cabinet.home');
        $crumbs->push(__('breadcrumbs.cabinet.all_request_products'), route('cabinet.request_product.index', $profile->id));
        $crumbs->push(__('breadcrumbs.cabinet.creating_request_product'));
    });
    
    Breadcrumbs::register('cabinet.request_product.edit', function (Crumbs $crumbs, Profile $profile) {
        $crumbs->parent('cabinet.home');
        $crumbs->push(__('breadcrumbs.cabinet.all_request_products'), route('cabinet.request_product.index', $profile->id));
        $crumbs->push(__('breadcrumbs.cabinet.editing_request_product'));
    });

    //Экспорт
    Breadcrumbs::register('cabinet.export.index', function (Crumbs $crumbs, Profile $profile) {
        $crumbs->parent('cabinet.home');
        $crumbs->push(__("breadcrumbs.cabinet.all_{$profile->export_type}s"), route("cabinet.{$profile->export_type}.index", $profile->id));
        $crumbs->push(__('breadcrumbs.cabinet.export_view'));
    });
    
    Breadcrumbs::register('cabinet.export.create.all', function (Crumbs $crumbs, Profile $profile) {
        $crumbs->parent('cabinet.home');
        $crumbs->push(__("breadcrumbs.cabinet.all_{$profile->export_type}s"), route("cabinet.{$profile->export_type}.index", $profile->id));
        $crumbs->push(__("breadcrumbs.cabinet.export_{$profile->export_type}_create"));
    });
    
    //Каталог
    Breadcrumbs::register('catalog.index', function (Crumbs $crumbs) {
        $crumbs->push(__('breadcrumbs.pages.main'), route('main'));
        $crumbs->push(__('breadcrumbs.pages.catalog'));
    });
    
    Breadcrumbs::register('catalog.inner_category', function (Crumbs $crumbs, CategoryPath $path, int $localisationId, $model) {

        if ($path->category && $parent = $path->category->parent) 
        {
            $crumbs->parent('catalog.inner_category', $path->withCategory($parent), $localisationId, $model);
        }
        
        if ($path->category && $path->category->link !== 'catalog') 
        {
            $crumbs->push(
                $path
                    ->category
                    ->categoryProperties
                    ->where('code', 'name')
                    ->where('localisation_id', $localisationId)
                    ->first()
                    ->value, 
                    
                route("catalog.{$model}.index", $path) 
            );
            
        } elseif($path->category && $path->category->link === 'catalog' && $model === 'product')
        {
            $crumbs->push(
                $path
                    ->category
                    ->categoryProperties
                    ->where('code', 'name')
                    ->where('localisation_id', $localisationId)
                    ->first()
                    ->value, 
                    
                route('catalog.index') 
            );
        }
        
    });
    
    // Товары
    Breadcrumbs::register('catalog.product.index', function (Crumbs $crumbs, CategoryPath $path) {
        $localisationId = Localisation::where('code', app('translator')->getLocale())->first()->id;
        $crumbs->parent('catalog.inner_category', $path, (int)$localisationId, 'product');
    });
    
    Breadcrumbs::register('catalog.product.show', function (Crumbs $crumbs, Product $product) {
        $localisationId = Localisation::where('code', app('translator')->getLocale())->first()->id;
        
        $path = new CategoryPath;
        $path = $path->resolveRouteBinding(Category::find($product->category_id)->link);
        
        $crumbs->parent('catalog.inner_category', $path, (int)$localisationId, 'product');
    });
    
    // Запросы 
    Breadcrumbs::register('catalog.request_product.index', function (Crumbs $crumbs, CategoryPath $path = null) {
        $crumbs->push(__('breadcrumbs.pages.main'), route('main'));
        if($path)
        {
            $localisationId = Localisation::where('code', app('translator')->getLocale())->first()->id;
            $crumbs->parent('catalog.inner_category', $path, (int)$localisationId, 'request_product');
        }
    });
    
    Breadcrumbs::register('catalog.request_product.show', function (Crumbs $crumbs, RequestProduct $requestProduct) {
        $crumbs->push(__('breadcrumbs.pages.main'), route('main'));
        $localisationId = Localisation::where('code', app('translator')->getLocale())->first()->id;
        
        $path = new CategoryPath;
        $path = $path->resolveRouteBinding(Category::find($requestProduct->category_id)->link);
        
        $crumbs->parent('catalog.inner_category', $path, (int)$localisationId, 'request_product');
    });
    
    // Статика 
    Breadcrumbs::register('page.inner_page', function (Crumbs $crumbs, PagePath $path, int $localisationId) {

        if ($path->page && $parent = $path->page->parent) 
        {
            $crumbs->parent('page.inner_page', $path->withPage($parent), $localisationId);
        }
        
        if ($path->page) 
        {
            $crumbs->push(
                $path->page
                    ->pageProperties
                    ->where('code', 'menu_title')
                    ->where('localisation_id', $localisationId)
                    ->first() ?
                    $path->page
                        ->pageProperties
                        ->where('localisation_id', $localisationId)
                        ->where('code', 'menu_title')
                        ->first()
                        ->value
                    : $path->page
                        ->pageProperties
                        ->where('localisation_id', $localisationId)
                        ->where('code', 'title')
                        ->first()
                        ->value,
                    
                route("page", $path) 
            );
        }
        
    });
    
    Breadcrumbs::register('page', function (Crumbs $crumbs, PagePath $path) {
        $crumbs->push(__('breadcrumbs.pages.main'), route('main'));
        $localisationId = Localisation::where('code', app('translator')->getLocale())->first()->id;
        $crumbs->parent('page.inner_page', $path, (int)$localisationId);
    });
    
    //Импорт
    Breadcrumbs::register('cabinet.import.index', function (Crumbs $crumbs, Profile $profile) {
        $crumbs->parent('cabinet.home');
        $crumbs->push(__('breadcrumbs.cabinet.import_view'));
    });
    
    Breadcrumbs::register('cabinet.import.create', function (Crumbs $crumbs, Profile $profile) {
        $crumbs->parent('cabinet.home');
        $crumbs->push(__("breadcrumbs.cabinet.all_{$profile->import_type}s"), route("cabinet.{$profile->import_type}.index", $profile->id));
        $crumbs->push(__("breadcrumbs.cabinet.import_{$profile->import_type}_create"));
    });
    
    // Избранное
    Breadcrumbs::register('cabinet.favorite.index', function (Crumbs $crumbs, User $user) {
        $crumbs->parent('cabinet.home');
        $crumbs->push(__("breadcrumbs.cabinet.favorites"));
    });

    // Чат
    Breadcrumbs::register('cabinet.chat.support', function (Crumbs $crumbs) {
        $crumbs->parent('cabinet.home');
        $crumbs->push(__("breadcrumbs.cabinet.messages"));
    });

    Breadcrumbs::register('cabinet.chat.index', function (Crumbs $crumbs) {
        $crumbs->parent('cabinet.home');
        $crumbs->push(__("breadcrumbs.cabinet.messages"));
    });

    Breadcrumbs::register('cabinet.chat.member', function (Crumbs $crumbs) {
        $crumbs->parent('cabinet.home');
        $crumbs->push(__("breadcrumbs.cabinet.messages"));
    });

    Breadcrumbs::register('cabinet.company.companies', function (Crumbs $crumbs) {
        $crumbs->parent('cabinet.home');
        $crumbs->push(__("Список компаний"));
    });

    Breadcrumbs::register('cabinet.manager.companies', function (Crumbs $crumbs) {
        $crumbs->parent('cabinet.home');
        $crumbs->push(__("Список компаний"));
    });

    Breadcrumbs::register('cabinet.company.list', function (Crumbs $crumbs, $var) {
        $crumbs->parent('cabinet.home');
        $crumbs->push(__("Профили для {$var->name}"));
    });

    Breadcrumbs::register('cabinet.company.create', function (Crumbs $crumbs) {
        $crumbs->parent('cabinet.company.companies');
        $crumbs->push(__("Новый аккаунт для компании"));
    });

    Breadcrumbs::register('cabinet.company.profile_create', function (Crumbs $crumbs, string $type) {
        $crumbs->parent('cabinet.company.companies');
        $crumbs->push(__("breadcrumbs.cabinet.creating_profile"), route('cabinet.profile.create', ['type' => $type]));
    });

?>
