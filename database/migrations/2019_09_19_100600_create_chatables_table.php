<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateChatablesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('chatables', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('chat_id')->unsigned();
            $table->integer('chatable_id')->unsigned();
            $table->string('chatable_type');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('chatables');
    }
}
