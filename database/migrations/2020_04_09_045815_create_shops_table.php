<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
//use Illuminate\Support\Facades\Schema;
use Staudenmeir\LaravelMigrationViews\Facades\Schema;
use Illuminate\Support\Facades\DB;
use App\Models\ProfileType;
use App\User;
use Illuminate\Database\Eloquent\Builder;

class CreateShopsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        /* Schema::create('shops', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('profile_id')->unsigned();
            $table->timestamps();
            $table->softDeletes();
            $table->unique('profile_id');
        }); */
        $query = User::whereHas('profiles', function (Builder $query) {
            $query
                ->where([
                    ['status', 'active'],
                    ['profile_type_id', 2],
            ]);
        })
        ->select('id');
   
        Schema::createOrReplaceView('shops', $query);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropViewIfExists('shops');
    }
}
