<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTenderPropertiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tender_properties', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('tender_id')->unsigned();
			$table->integer('localisation_id')->unsigned();
            $table->integer('currency_id')->unsigned();
			$table->string('code')->comment('title, description, total_amount, country');
			$table->string('value');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tender_properties');
    }
}
