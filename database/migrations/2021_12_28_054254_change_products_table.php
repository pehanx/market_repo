<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('products', function (Blueprint $table) {
            //$table->dropColumn(['shows']);
            $table->integer('shows')->unsigned()->default(1);
			//$table->string('sociality_token', 200)->nullable()->change();
            //$table->string('email')->nullable()->change();
            //$table->dropColumn(['email_verified_at', 'sociality_token']);
            //$table->boolean('is_parent')->default('false')->after('value');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('products', function (Blueprint $table) {
            //
        });
    }
}
