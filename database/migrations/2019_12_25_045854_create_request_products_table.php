<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRequestProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('request_products', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('profile_id')->unsigned();
            $table->integer('category_id')->unsigned();
            $table->integer('parent_id')->unsigned()->nullable();
            $table->boolean('is_active')->default(true);
            $table->integer('sort')->unsigned();
            $table->string('link', 500)->comment('title product on en local');
            $table->timestamps();
            $table->softDeletes();
            $table->dateTime('destroyed_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('request_products');
    }
}
