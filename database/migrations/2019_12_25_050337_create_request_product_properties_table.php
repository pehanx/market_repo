<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRequestProductPropertiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('request_product_properties', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('request_product_id')->unsigned();
			$table->integer('localisation_id')->unsigned();
			$table->string('code')->comment('title, describle');
			$table->text('value');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('request_product_properties');
    }
}
