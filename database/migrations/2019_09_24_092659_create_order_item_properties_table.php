<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderItemPropertiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_item_properties', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('order_item_id')->unsigned();
			$table->integer('localisation_id')->unsigned();
            $table->integer('currency_id')->unsigned();
			$table->string('code')->comment('total_amount');
			$table->string('value');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_item_properties');
    }
}
