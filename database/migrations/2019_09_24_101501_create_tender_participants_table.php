<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTenderParticipantsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tender_participants', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('tender_id')->unsigned();
            $table->integer('profile_id')->unsigned();
            $table->mediumText('text')->comment('Краткое описание вашего предложения');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tender_participants');
    }
}
