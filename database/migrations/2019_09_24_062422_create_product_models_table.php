<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductModelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_models', function (Blueprint $table) {
            $table->bigIncrements('id'); 
            $table->integer('product_id')->unsigned();
            $table->integer('articul')->unsigned()->default(null);
            $table->boolean('is_active')->default(true);
            $table->integer('sort')->unsigned();
            $table->softDeletes();
            $table->timestamps();
            $table->dateTime('destroyed_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_models');
    }
}
