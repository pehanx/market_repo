<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRequestProductPricesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('request_product_prices', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('request_product_id')->unsigned();
            $table->integer('currency_id')->unsigned();
            $table->bigInteger('min')->unsigned();
            $table->bigInteger('max')->unsigned();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('request_product_prices');
    }
}
