<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */
use App\{ApplicationDictionary, User};
use App\Models\{
    Product,
    Profile,
    Category,
    ProductModel,
    ProductProperty,
    ProductPrice,
    Localisation,
    Download,
    Characteristic,
};

use Faker\Generator as Faker;
use Faker\Factory as FakerFactory;
use Illuminate\Support\Str;

$productParams = new ProductParams();

$factory->define(Product::class, function (Faker $faker) {
    return [];
});
$factory->define(ProductProperty::class, function (Faker $faker) {
    return [];
});
$factory->define(ProductPrice::class, function (Faker $faker) {
    return [];
});
$factory->define(ProductModel::class, function (Faker $faker) {
    return [];
});

$factory->state(Product::class, 'product', function (Faker $faker) use($productParams) {

    return [
        'profile_id' => $faker->randomElement($productParams->getSellers()),
        'category_id' => $faker->randomElement($productParams->getCategories()),
        'articul' => $faker->unique()->numberBetween(1000,9999),
        'is_active' => true,
        'sort' => 1,
        'link' => \Str::slug($faker->unique()->word),
    ];

});

$factory->state(ProductProperty::class, 'product_properties', function (Faker $faker) use($productParams){

    $properties = [];
    $localisation = $productParams->getLocale();

    foreach ($productParams->getProperties($localisation) as $key => $value)
    {
        array_push($properties, [
            'localisation_id' => $localisation['id'],
            'code' => $key,
            'value' => $value,
            'is_parent' => 'true',
        ]);
    }

    return $properties;
});

$factory->state(ProductPrice::class, 'product_prices', function (Faker $faker) {

    return [
        'currency_id' => 2,
        'min' => $faker->numberBetween(100,200),
        'max' => $faker->numberBetween(100,200),
        'is_parent' => true,
    ];

});

$factory->state(ProductModel::class, 'product_prices', function (Faker $faker) {

    return [
        'currency_id' => 2,
        'min' => $faker->numberBetween(100,200),
        'max' => $faker->numberBetween(100,200),
        'is_parent' => false,
    ];

});

$factory->state(ProductModel::class, 'product_models', function (Faker $faker) use($productParams){

    return $productParams->getModel($productParams->getUnits());

});

Class ProductParams
{
    /* 
     * Массив с доступными единицами измерения
     */
    public function getUnits() :array
    {
        $quantities = ApplicationDictionary::where('group_code','quantities')->get()->pluck('code')->toArray();
        $colors = ApplicationDictionary::where('group_code','colors')->get()->pluck('code')->toArray();
        $weights = ApplicationDictionary::where('group_code','weights')->get()->pluck('code')->toArray();
        $sizes = ApplicationDictionary::where('group_code','sizes')->get()->pluck('code')->toArray();

        return [
            'quantities' => $quantities,
            'colors' => $colors,
            'weights' => $weights,
            'sizes' => $sizes,
        ];
    }

    /* 
     * Получение профиля который относится к продукту
     */
    public function getProfile() :Profile
    {
        $lastProdId = Product::all()->count();
        $profileId = Product::find($lastProdId)->profile_id;
        return Profile::find($profileId);
    }

    /* 
     * Создание массива для properties
     */
    public function getProperties($localisation) :array
    {
        $faker = FakerFactory::create( $localisation['regional']);

        return [
            'name' => $faker->catchPhrase,
            'description' => $faker->catchPhrase,
            'seo_title' => $faker->catchPhrase,
            'seo_keywords' => $faker->word .' '. $faker->word .' '. $faker->word,
            'seo_descriptions' => $faker->catchPhrase,
        ];
    }

    /* 
     * Получение всех профилей продавцов
     */
    public function getSellers() :array
    {
        return Profile::where('profile_type_id', 2)->get()->pluck('id')->toArray();
    }

    /* 
     * Рандомный выбор локали
     */
    public function getLocale() :array
    {
        $lastProdId = Product::all()->count();

        $profileLocalisation = Profile::find( Product::find($lastProdId)->profile_id )->profileProperties()->first()->localisation_id;
        $localisation = \Config::get('laravellocalization.supportedLocales');


        $locale = Localisation::find($profileLocalisation);

        return [
            'id' => $locale ? $locale->id : 1,
            'regional' => $localisation[$locale ? $locale->code : 'ru']['regional'],
        ];
    }

    /* 
     * Создание характеристик модели
     */
    public function getModel(array $allUnits) :array
    {
        $faker = FakerFactory::create();

        $availableCharacteristics = [
            "available" => [
                'type' => $faker->randomElement($allUnits['quantities']),
                'value' => $faker->randomDigit,
            ],
            "color" => [
                'type' => '',
                'value' => $faker->randomElement($allUnits['colors']),
            ],
        ];

        $result = [];
        foreach ($availableCharacteristics as $char => $value)
        {
            array_push($result, [
                'code' => $char,
                'value' => $value['value'],
                'type' => $value['type'],
                'profile_id' => $this->getProfile()->id,
            ]);
        }

        return $result;
    }

    /* 
     * Рандомный выбор из существующих категорий
     */
    public function getCategories() :array
    {
        return Category::where([['is_active',true],['parent_id','<>',null]])->get()->pluck('id')->toArray();
    }
}