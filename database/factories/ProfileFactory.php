<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */
use App\User;
use App\Models\{
    Profile,
    ProfileType,
    ProfileProperty,
    Localisation,
    Download
};
use Faker\Generator as Faker;
use Faker\Factory as FakerFactory;
use Illuminate\Support\Str;

$profileProperties = new ProfileProperties();

$factory->define(Profile::class, function ()
{ 
    return [];
});

$factory->define(ProfileProperty::class, function ()
{ 
    return [];
});

$factory->state(Profile::class, 'profile_buyer', function ()
{
    return [
        'profile_type_id' => 2,
        'status' => 'active',
    ];
});

$factory->state(Profile::class, 'profile_seller', function ()
{
    return [
        'profile_type_id' => 1,
        'status' => 'active',
    ];
});


$factory->state(Profile::class, 'managers', function () {
    
    return [
        'profile_type_id' => 3,
        'status' => 'unactive',
    ];

});

$factory->state(ProfileProperty::class, 'profile_properties', function (Faker $faker, $locale) use($profileProperties)
{
    $properties = [];
    $localisation = $profileProperties->getLocale($locale['localisation_id']);

    foreach ($profileProperties->getProperties($localisation) as $key => $value)
    {
        array_push($properties, [
            'localisation_id' => $localisation['id'],
            'code' => $key,
            'value' => $value
        ]);
    }

    return $properties;
});

$factory->state(User::class, 'managers', function (Faker $faker) use($profileProperties)
{
    $faker = FakerFactory::create( $profileProperties->getProfileLocale()['regional']);
    
    return [
        'name' => $faker->firstName,
        'email' => $faker->email,
        'remember_token' => \Str::random(10),
        'registration_type' => 'native',
        'ip' => \Request::ip(),
    ];
});

$factory->state(ProfileProperty::class, 'manager_properties', function (Faker $faker, $locale) use($profileProperties)
{
    $properties = [];
    $localisation = $profileProperties->getLocale($locale['localisation_id']);

    foreach ($profileProperties->getManagerProperties() as $key => $value)
    {
        array_push($properties, [
            'localisation_id' => $localisation['id'],
            'code' => $key,
            'value' => $value
        ]);
    }

    return $properties;
});

Class ProfileProperties
{
    /* 
     * Создание массива для properties
     */
    public function getProperties($localisation)
    {
        if($localisation)
        $faker = FakerFactory::create( $localisation['regional']);

        try {
            $region = $faker->state;
        } catch (\Throwable $th) {}
        
        try {
            $region = $faker->region;
        } catch (\Throwable $th) {}

        $currentProfile = ProfileProperty::where('profile_id', 
            $this->getLastUser()->profiles()->get()->where('profile_type_id', $this->getProfile()->profile_type_id )->first()->id
        )
        ->where('localisation_id', '<>', $this->getProfile()->profileProperties()->first() ? $this->getProfile()->profileProperties()->first()->localisation_id : null)
        ->first();

        $nameString = 'company_name';                                                                                                                                                               //($this->getProfile()->profile_type_id === 1) ? 'company_name' : 'name';

        if ( $currentProfile === null )
        {
            return [
                $nameString => $faker->company,
                'company_description' => $faker->catchPhrase,
                'country' => $faker->country,
                'region' => $region,
                'city' => $faker->city,
                'index' => $faker->postcode,
                'street' => $faker->streetName,
                'number_home' => $faker->randomDigit,
                'number_build' => $faker->randomDigit,
                'number_room' => $faker->randomDigit,
                'name' => $faker->firstName,
                'last_name' => $faker->lastName,
                'position' => $faker->jobTitle,
                'number_phone' => $faker->e164PhoneNumber,
                'number_phone_mobile' => $faker->e164PhoneNumber,
            ];
        }
        $currentProfile = ProfileProperty::where('profile_id',
            $this->getLastUser()->profiles()->get()->where('profile_type_id', $this->getProfile()->profile_type_id )->first()->id
        )
        ->where('localisation_id', '<>', $this->getProfile()->profileProperties()->first() ? $this->getProfile()->profileProperties()->first()->localisation_id : null)
        ->get();
        //dd($currentProfile->where('code','company_name')->first()->value . '(' . $localisation['regional'] . ')');
        return [
            $nameString => $currentProfile->where('code',$nameString)->first()->value . '(' . $localisation['regional'] . ')',
            'company_description' => $faker->catchPhrase,
            'country' => $faker->country,
            'region' => $region,
            'city' => $faker->city,
            'index' => $faker->postcode,
            'street' => $faker->streetName,
            'number_home' => $faker->randomDigit,
            'number_build' => $faker->randomDigit,
            'number_room' => $faker->randomDigit,
            'name' => $faker->firstName,
            'last_name' => $faker->lastName,
            'position' => $faker->jobTitle,
            'number_phone' => $faker->e164PhoneNumber,
            'number_phone_mobile' => $faker->e164PhoneNumber,
        ];
    }

    /* 
     * Создание массива для properties у менеджера
     */
    public function getManagerProperties()
    {
        $localisation = $this->getProfileLocale();
        $faker = FakerFactory::create( $localisation['regional']);

        try {
            $region = $faker->state;
        } catch (\Throwable $th) {}
        
        try {
            $region = $faker->region;
        } catch (\Throwable $th) {}

        return [
            'last_name' => $faker->lastName,
            'position' => $faker->jobTitle,
            'birthday' => $faker->dateTimeThisCentury->format('d-m-Y'),
            'gender' => $faker->randomElement(['man','woman']),
            'phone' => $faker->e164PhoneNumber,
        ];
    }

    /* 
     * Рандомный выбор локали
     */
    public function getLocale($localisationId) :array
    {
        $localisation = \Config::get('laravellocalization.supportedLocales');

        $locale = Localisation::find($localisationId);

        return [
            'id' => $locale ? $locale->id : 1,
            'regional' => $localisation[$locale ? $locale->code : 'en']['regional'],
        ];
    }

    /* 
     * Рандомный выбор локали основного профиля (для свойств менеджера)
     */
    public function getProfileLocale() :array
    {
        $localisation = \Config::get('laravellocalization.supportedLocales');

        $codeIndex = $this->getProfile()->profileProperties->first()->localisation_id;
        $locale = Localisation::where('id', $codeIndex)->first();

        return [
            'id' => $locale ? $locale->id : 1,
            'regional' => $localisation[$locale ? $locale->code : 'ru']['regional'],
        ];
    }

    /* 
     * Получение профиля который относится к продукту
     */
    public function getProfile() :Profile
    {

        $lastUser = $this->getLastUser();

        $lastProfiles = $lastUser->profiles()->get()->where('status','active')->sortBy('id')->pluck('id')->toArray();

        $lastProfId = count($lastProfiles);
        return Profile::find($lastProfiles[($lastProfId - 1)]);        
    }

    private function getLastUser()
    {
        $users = User::all();
        $users = $users->sortBy('id');
        foreach ($users as $user)
        {
            if($user->hasRole('seller') && $user->hasRole('buyer'))
            {
                $lastUser = $user;
            }
        }
        return $lastUser;
    }
}