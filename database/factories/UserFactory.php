<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */
use App\User;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/
$factory->define(App\User::class, function (Faker $faker) {
    return [];
});

$factory->state(App\User::class, 'rampage', function (Faker $faker) {
    return [
        'name' => 'rampage',
        'email' => 'dmitry_batkov@mail.ru',
        'password' => bcrypt('locomotif'),
        'remember_token' => Str::random(10),
        'registration_type' => 'native',
        'ip' => \Request::ip(),
    ];
});

$factory->state(App\User::class, 'user', function (Faker $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'password' => bcrypt('locomotif'),
        'remember_token' => Str::random(10),
        'registration_type' => 'native',
        'ip' => \Request::ip(),
    ];
});

$factory->state(App\User::class, 'robot', function (Faker $faker) {
    return [
        'name' => 'RoboUser',
        'email' => 'tumanovdnet@gmail.com',
        'password' => bcrypt('tester322'),
        'remember_token' => Str::random(10),
        'registration_type' => 'native',
        'ip' => \Request::ip(),
    ];
});

$factory->state(App\User::class, 'robot2', function (Faker $faker) {
    return [
        'name' => 'RoboUser2',
        'email' => 'robouser@robo.com',
        'password' => bcrypt('tester322'),
        'remember_token' => Str::random(10),
        'registration_type' => 'native',
        'ip' => \Request::ip(),
    ];
});