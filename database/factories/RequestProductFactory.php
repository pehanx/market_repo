<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */
use App\{ApplicationDictionary, User};
use App\Models\{
    RequestProduct,
    Profile,
    Category,
    RequestProductProperty,
    RequestProductPrice,
    Localisation,
    Download,
    Characteristic,
};

use Faker\Generator as Faker;
use Faker\Factory as FakerFactory;
use Illuminate\Support\Str;

$requestProductParams = new RequestProductParams();

$factory->define(RequestProduct::class, function (Faker $faker) {
    return [];
});
$factory->define(RequestProductProperty::class, function (Faker $faker) {
    return [];
});
$factory->define(RequestProductPrice::class, function (Faker $faker) {
    return [];
});

$factory->state(RequestProduct::class, 'request_product', function (Faker $faker) use($requestProductParams)
{
    return [
        'profile_id' => $faker->randomElement($requestProductParams->getBuyers()),
        'category_id' => $faker->randomElement($requestProductParams->getCategories()),
        'is_active' => true,
        'sort' => 1,
        'link' => \Str::slug($faker->unique()->word),
    ];
});

$factory->state(RequestProductProperty::class, 'request_product_properties', function (Faker $faker) use($requestProductParams)
{
    $properties = [];
    $localisation = $requestProductParams->getLocale();

    foreach ($requestProductParams->getProperties($localisation) as $key => $value)
    {
        array_push($properties, [
            'localisation_id' => $localisation['id'],
            'code' => $key,
            'value' => $value,
        ]);
    }

    return $properties;
});

$factory->state(RequestProductPrice::class, 'request_product_prices', function (Faker $faker)
{
    return [
        'currency_id' => 1,
        'min' => $faker->numberBetween(100,200),
        'max' => $faker->numberBetween(100,200),
    ];
});

Class RequestProductParams
{
    /* 
     * Получение профиля который относится к запросу
     */
    private function getProfile()
    {
        $lastProdId = RequestProduct::all()->count();
        $profileId = RequestProduct::find($lastProdId)->profile_id;
        return Profile::find($profileId);
    }

    /* 
     * Создание массива для properties
     */
    public function getProperties($localisation) :array
    {
        $faker = FakerFactory::create( $localisation['regional']);

        return [
            'name' => $faker->catchPhrase,
            'full_description' => $faker->catchPhrase,
            'seo_title' => $faker->catchPhrase,
            'seo_keywords' => $faker->word .' '. $faker->word .' '. $faker->word,
            'seo_descriptions' => $faker->catchPhrase,
        ];
    }

    /* 
     * Получение всех профилей покупателей
     */
    public function getBuyers() :array
    {
        return Profile::where('profile_type_id', 1)->get()->pluck('id')->toArray();
    }

    /* 
     * Рандомный выбор локали
     */
    public function getLocale() :array
    {
        $lastProdId = RequestProduct::all()->count();

        $profileLocalisation = Profile::find( RequestProduct::find($lastProdId)->profile_id )->profileProperties()->first()->localisation_id;
        $localisation = \Config::get('laravellocalization.supportedLocales');


        $locale = Localisation::find($profileLocalisation);

        return [
            'id' => $locale ? $locale->id : 1,
            'regional' => $localisation[$locale ? $locale->code : 'ru']['regional'],
        ];
    }

    /* 
     * Рандомный выбор из существующих категорий
     */
    public function getCategories() :array
    {
        return Category::where([['is_active',true],['parent_id','<>',null]])->get()->pluck('id')->toArray();
    }
}