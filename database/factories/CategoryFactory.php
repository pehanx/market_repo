<?php
use App\Models\Category;
use Faker\Generator as Faker;

/** @var \Illuminate\Database\Eloquent\Factory $factory */

$factory->define(App\Models\Category::class, function (Faker $faker) {
    return [
        'link' => $faker->unique()->slug(),
        'profile_id' => $faker->numberBetween(10, 20),
        'sort' => $faker->numberBetween(1, 500),
        'is_active' => $faker->boolean,
        'parent_id' => null,
    ];
});
