<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */
use App\ApplicationDictionary;
use App\Models\{Characteristic, Product, Profile};
use Faker\Generator as Faker;
use Faker\Factory as FakerFactory;
use Illuminate\Support\Str;

$characteristicFactory = new CharacteristicFactory();

$factory->define(Characteristic::class, function (Faker $faker) {
    return [];
});

$factory->state(Characteristic::class, 'product_characteristics', function (Faker $faker) use($characteristicFactory){
    return $characteristicFactory->getProductCharacteristics($characteristicFactory->getUnits());
});

$factory->state(Characteristic::class, 'request_product_characteristics', function (Faker $faker) use($characteristicFactory)
{
    return $characteristicFactory->getRequestProductCharacteristics();
});

class CharacteristicFactory
{

    /* 
     * Массив с доступными единицами измерения
     */
    public function getUnits() :array
    {
        $quantities = ApplicationDictionary::where('group_code','quantities')->get()->pluck('code')->toArray();
        $colors = ApplicationDictionary::where('group_code','colors')->get()->pluck('code')->toArray();
        $weights = ApplicationDictionary::where('group_code','weights')->get()->pluck('code')->toArray();
        $sizes = ApplicationDictionary::where('group_code','sizes')->get()->pluck('code')->toArray();

        return [
            'quantities' => $quantities,
            'colors' => $colors,
            'weights' => $weights,
            'sizes' => $sizes,
        ];
    }

    /* 
     * Получение профиля который относится к продукту
     */
    public function getProfile() :Profile
    {
        $lastProdId = Product::all()->count();
        $profileId = Product::find($lastProdId)->profile_id;
        return Profile::find($profileId);
    }

    /* 
     * Создание массива для характеристик продукта
     */
    public function getProductCharacteristics(array $allUnits) :array
    {
        $faker = FakerFactory::create();

        $allowedCharacteristic = [
            "available" => [
                'type' => $faker->randomElement($allUnits['quantities']),
                'value' => $faker->randomDigit,
            ],
            "color" => [
                'type' => '',
                'value' => $faker->randomElement($allUnits['colors']),
            ],
            'min_order' => [
                'type' => $faker->randomElement($allUnits['quantities']),
                'value' => $faker->randomDigit,
            ],
            "country" =>  [
                'type' => '',
                'value' => $faker->countryCode,
            ],
            "weight" => [
                'type' => $faker->randomElement($allUnits['weights']),
                'value' => $faker->randomDigit,
            ],
            "lenght" => [
                'type' => $faker->randomElement($allUnits['sizes']),
                'value' => $faker->randomDigit,
            ],
            "width" => [
                'type' => $faker->randomElement($allUnits['sizes']),
                'value' => $faker->randomDigit,
            ],
            "height" => [
                'type' => $faker->randomElement($allUnits['sizes']),
                'value' => $faker->randomDigit,
            ],
        ];

        $result = [];

        foreach( $allowedCharacteristic as $char => $value)
        {
            array_push($result, [
                'code' => $char,
                'value' => $value['value'],
                'profile_id' => $this->getProfile()->id,
                'type' => $value['type'],
            ]);
        }

        return $result;
        
    }

    /* 
     * Создание массива для характеристик запроса
     */
    public function getRequestProductCharacteristics() :array
    {
        $faker = FakerFactory::create();

        $quantities = ApplicationDictionary::where('group_code','quantities')->get()->pluck('code')->toArray();

        $result = [];

        array_push($result, [
            'code' => 'min_order',
            'value' => $faker->randomDigitNotNull,
            'profile_id' => $this->getProfile()->id,
            'type' => $faker->randomElement($quantities),
        ]);

        return $result;
        
    }
}
?>