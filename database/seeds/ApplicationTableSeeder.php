<?php

use Illuminate\Database\Seeder;
use App\Models\Localisation;
use App\ApplicationDictionary;

class ApplicationTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \Cache::tags(ApplicationDictionary::class)->flush();

        ApplicationDictionary::truncate();

        $insertData = [];

        $localisationIds = [
            'ru' => Localisation::where('code', 'ru')->first()->id,
            'en' => Localisation::where('code', 'en')->first()->id,
            'cn' => Localisation::where('code', 'cn')->first()->id,
        ];

        // Страны
        include('ApplicationSeeders/CountriesSeeder.php');

        foreach ($countriesData as $localisationCode => $countries)
        {
            foreach($countries as $countryCode => $value)
            {
                array_push($insertData, [
                    'localisation_id' => $localisationIds[$localisationCode],
                    'group_code' => 'countries',
                    'code' =>  $countryCode,
                    'value' => $value,
                ]);
            }
        }

        ApplicationDictionary::insert($insertData);

        $insertData = [];

        // Цвета
        include('ApplicationSeeders/ColorsSeeder.php');

        foreach ($colorsData as $localisationCode => $colors)
        {
            foreach($colors as $colorsCode => $value)
            {
                array_push($insertData, [
                    'localisation_id' => $localisationIds[$localisationCode],
                    'group_code' => 'colors',
                    'code' =>  $colorsCode,
                    'value' => $value,
                ]);
            }
        }

        ApplicationDictionary::insert($insertData);

        $insertData = [];

        // Единицы измерения размеров, количества, весов
        include('ApplicationSeeders/ValuesSeeder.php');

        foreach ($valuesData as $valueCode => $valueFields)
        {
            foreach($valueFields as $localisationCode => $fields)
            {
                foreach($fields as $fieldCode => $value)
                {
                    array_push($insertData, [
                        'localisation_id' => $localisationIds[$localisationCode],
                        'group_code' => $valueCode,
                        'code' =>  $fieldCode,
                        'value' => $value,
                    ]);
                }
            }
        }

        ApplicationDictionary::insert($insertData);

        $insertData = [];

        // Названия колонок для импорта\экспорта
        include('ApplicationSeeders/ColumnsSeeder.php');


        foreach ($columnsData as $localisationCode => $columns)
        {
            foreach($columns as $columnCode => $value)
            {
                array_push($insertData, [
                    'localisation_id' => $localisationIds[$localisationCode],
                    'group_code' => 'columns',
                    'code' =>  $columnCode,
                    'value' => $value,
                ]);
            }
        }

        ApplicationDictionary::insert($insertData);

        $insertData = [];

        // Алфавиты
        include('ApplicationSeeders/AlphabetsSeeder.php');


        foreach ($alphabetsData as $localisationCode => $fields)
        {
            foreach($fields as $keyField => $value)
            {
                array_push($insertData, [
                    'localisation_id' => $localisationIds[$localisationCode],
                    'group_code' => 'alphabets',
                    'code' =>  $value,
                    'value' => $keyField,
                ]);
            }
        }

        ApplicationDictionary::insert($insertData);
        
        $insertData = [];

        // Магазин поставщика
        include('ApplicationSeeders/ShopsSeeder.php');


        foreach ($shopsData as $localisationCode => $fields)
        {
            foreach($fields as $keyField => $value)
            {
                array_push($insertData, [
                    'localisation_id' => $localisationIds[$localisationCode],
                    'group_code' => 'shops',
                    'code' =>  $keyField,
                    'value' => $value,
                ]);
            }
        }

        ApplicationDictionary::insert($insertData);

        $insertData = [];

        // Символы валют
        include('ApplicationSeeders/CurrencySymbolsSeeder.php');


        foreach ($symbolsData as $symbolCode => $fields)
        {
            foreach($fields as $keyField => $value)
            {
                array_push($insertData, [
                    'localisation_id' => $localisationIds[$symbolCode],
                    'group_code' => 'currency',
                    'code' =>  $keyField,
                    'value' => $value,
                ]);
            }
        }

        ApplicationDictionary::insert($insertData);
    }
}
