<?php

use Illuminate\Database\Seeder;
use App\Models\Currency;

class CurrenciesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Currency::create(
        [
            'code' => 'rub',
        ]);
		
		Currency::create(
        [
            'code' => 'usd',
            'is_base' => true
        ]);
		
		Currency::create(
        [
            'code' => 'cny',
        ]);
    }
}
