<?php
ini_set('memory_limit', '2048M');

use App\Models\{Category, CategoryProperty, Localisation};
use Carbon\Carbon;
use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{
    public function run()
    {
        \Cache::tags(Category::class)->flush();

        include('ExampleCategoriesTableSeeder/CategoriesSeeder.php');
        
        Category::truncate();
        CategoryProperty::truncate();
        
        $langs = Localisation::select('code', 'id')
            ->get()
            ->mapWithKeys(function ($item) {
                return [$item['id'] => strtoupper($item['code'])];
            });
        
        $profileId = 1;
        $parentIds = [];
        $arrayKey = 0;
        $categories = [];
        $categoryProperties = [];

        foreach ($categoriesData as $category)
        {
            if((int)$category['DEPTH_LEVEL'] === 1) // root
            {
                $categories[$arrayKey] = [
                    'link' => $category['CODE'],
                    'profile_id' => 1,
                    'sort' => 500,
                    'is_active' => true,
                    'parent_id' => null,
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now(),
                ];

            } elseif((int)$category['DEPTH_LEVEL'] === 2) // root
            {
                $categories[$arrayKey] = [
                    'link' => $category['CODE'],
                    'profile_id' => 1,
                    'sort' => $category['SORT'],
                    'is_active' => true,
                    'parent_id' => 1,
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now(),
                ];

            } else
            {
                $categories[$arrayKey] = [
                    'link' => $category['CODE'],
                    'profile_id' => 1,
                    'sort' => $category['SORT'],
                    'is_active' => true,
                    'parent_id' => $parentIds[(int)$category['IBLOCK_SECTION_ID']],
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now(),
                ];
            }
            
            foreach ($langs as $langId => $langCode)
            {
                if( $category["UF_NAME_$langCode"] === null)
                {
                    $category["UF_NAME_$langCode"] = $category["UF_NAME_RU"];
                }

                if( $category["UF_META_DESCR_$langCode"] === null)
                {
                    $category["UF_META_DESCR_$langCode"] = $category["UF_NAME_RU"];
                }

                if( $category["UF_META_TITLE_$langCode"] === null)
                {
                    $category["UF_META_TITLE_$langCode"] = $category["UF_NAME_RU"];
                }

                if( $category["UF_META_DESCR_$langCode"] === null)
                {
                    $category["UF_META_DESCR_$langCode"] = $category["UF_NAME_$langCode"];
                }

                if( $category["UF_META_TITLE_$langCode"] === null)
                {
                    $category["UF_META_TITLE_$langCode"] = $category["UF_NAME_$langCode"];
                }

                array_push(
                    $categoryProperties, 
                    
                    [
                        'category_id' => $arrayKey+1,
                        'localisation_id' => $langId,
                        'code' => 'name',
                        'value' => $category["UF_NAME_$langCode"],
                        'created_at' => Carbon::now(),
                        'updated_at' => Carbon::now(),
                    ],
                    
                    [
                        'category_id' => $arrayKey+1,
                        'localisation_id' => $langId,
                        'code' => 'seo_description',
                        'value' => $category["UF_META_DESCR_$langCode"],
                        'created_at' => Carbon::now(),
                        'updated_at' => Carbon::now(),
                    ],
                    
                    [
                        'category_id' => $arrayKey+1,
                        'localisation_id' => $langId,
                        'code' => 'seo_title',
                        'value' => $category["UF_META_TITLE_$langCode"],
                        'created_at' => Carbon::now(),
                        'updated_at' => Carbon::now(),
                    ]
                );
            }
            
            $parentIds[$category['ID']] = ++$arrayKey;
        }
        
        Category::insert($categories);
        
        $categoryProperties = ( collect($categoryProperties) )->chunk(10000);
        $categoryProperties = $categoryProperties->toArray();
        
        foreach($categoryProperties as $chunk)
        {
            CategoryProperty::insert($chunk);
        }

        Category::fixTree();
    }
}