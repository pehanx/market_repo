<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(LocalisationsTableSeeder::class);
        //$this->call(UsersTableSeeder::class);
		$this->call(ProfileTypesTableSeeder::class);
        $this->call(CurrenciesTableSeeder::class);
        $this->call(RolesAndPermissionsSeeder::class);
        $this->call(CategoriesTableSeeder::class);
        $this->call(MainPageCategoriesSeeder::class);
        $this->call(ApplicationTableSeeder::class);
        $this->call(LanguageLinesTableSeeder::class);
        //$this->call(EntitiesSeeder::class);
    }
}
