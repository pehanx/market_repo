<?php
$cabinetData = [

    //Экспорт
    [
        'group' => 'cabinet',
        'key' => 'export.export',
        'text' => [
            'ru' => 'Экспорт',
            'en' => 'Export',
            'cn' => '出口',
        ]
    ],

    [
        'group' => 'cabinet',
        'key' => 'exports',
        'text' => [
            'ru' => 'Список экспортов',
            'en' => 'List of exports',
            'cn' => '列表的出口',
        ]
    ],
    
    [
        'group' => 'cabinet',
        'key' => 'export.page',
        'text' => [
            'ru' => 'С текущей страницы',
            'en' => 'From the current page',
            'cn' => '从目前的页面',
        ]
    ],

    [
        'group' => 'cabinet',
        'key' => 'export.all_products',
        'text' => [
            'ru' => 'Все продукты',
            'en' => 'All products',
            'cn' => '所有产品',
        ]
    ],

    [
        'group' => 'cabinet',
        'key' => 'export.all_request_products',
        'text' => [
            'ru' => 'Все запросы',
            'en' => 'All requests',
            'cn' => '所有的请求',
        ]
    ],

    [
        'group' => 'cabinet',
        'key' => 'export.progress',
        'text' => [
            'ru' => 'Прогресс',
            'en' => 'Progress',
            'cn' => '进展',
        ]
    ],

    [
        'group' => 'cabinet',
        'key' => 'export.start',
        'text' => [
            'ru' => 'Начало',
            'en' => 'Beginning',
            'cn' => '开始',
        ]
    ],

    [
        'group' => 'cabinet',
        'key' => 'export.format',
        'text' => [
            'ru' => 'Формат экспорта',
            'en' => 'Export format',
            'cn' => '出口的格式',
        ]
    ],

    [
        'group' => 'cabinet',
        'key' => 'export.files',
        'text' => [
            'ru' => 'Файлы',
            'en' => 'Files',
            'cn' => '文件',
        ]
    ],

    [
        'group' => 'cabinet',
        'key' => 'export.with_models',
        'text' => [
            'ru' => 'С торговыми предложениями',
            'en' => 'With models',
            'cn' => '与模特',
        ]
    ],

    [
        'group' => 'cabinet',
        'key' => 'export.with_thumbnails',
        'text' => [
            'ru' => 'С превью',
            'en' => 'With preview',
            'cn' => '的预览',
        ]
    ],

    //Импорт
    [
        'group' => 'cabinet',
        'key' => 'import.import',
        'text' => [
            'ru' => 'Импорт',
            'en' => 'Import',
            'cn' => '进口',
        ]
    ],
    
    [
        'group' => 'cabinet',
        'key' => 'import.images_archive',
        'text' => [
            'ru' => 'Архив для импорта изображений',
            'en' => 'Archive to import images',
            'cn' => '档案进口的图像',
        ]
    ],
    
    [
        'group' => 'cabinet',
        'key' => 'import.empty_archive',
        'text' => [
            'ru' => 'Архив для импорта не содержит необходимых файлов',
            'en' => 'The files to import does not contain the necessary files',
            'cn' => '该文件的进口不含有必要的文件',
        ]
    ],

    [
        'group' => 'cabinet',
        'key' => 'import.table',
        'text' => [
            'ru' => 'Таблица для импорта',
            'en' => 'Spreadsheet import',
            'cn' => '电子表格进口',
        ]
    ],

    [
        'group' => 'cabinet',
        'key' => 'import.choose_column',
        'text' => [
            'ru' => 'Выберите значение колонки',
            'en' => 'Select the value column',
            'cn' => '选择值列',
        ]
    ],

    [
        'group' => 'cabinet',
        'key' => 'import.progress',
        'text' => [
            'ru' => 'Прогресс',
            'en' => 'Progress',
            'cn' => '进展',
        ]
    ],

    [
        'group' => 'cabinet',
        'key' => 'import.start',
        'text' => [
            'ru' => 'Начало',
            'en' => 'Beginning',
            'cn' => '开始',
        ]
    ],

    [
        'group' => 'cabinet',
        'key' => 'import.format',
        'text' => [
            'ru' => 'Формат импорта',
            'en' => 'The import format',
            'cn' => '进口的格式',
        ]
    ],
    
    [
        'group' => 'cabinet',
        'key' => 'import.name',
        'text' => [
            'ru' => 'Название',
            'en' => 'Name',
            'cn' => '名称',
        ]
    ],

    [
        'group' => 'cabinet',
        'key' => 'import.description',
        'text' => [
            'ru' => 'Описание',
            'en' => 'Description',
            'cn' => '描述',
        ]
    ],

    [
        'group' => 'cabinet',
        'key' => 'import.category',
        'text' => [
            'ru' => 'Категория',
            'en' => 'Category',
            'cn' => '类别',
        ]
    ],

    [
        'group' => 'cabinet',
        'key' => 'import.country',
        'text' => [
            'ru' => 'Страна',
            'en' => 'Country',
            'cn' => '国',
        ]
    ],

    [
        'group' => 'cabinet',
        'key' => 'import.available',
        'text' => [
            'ru' => 'Доступно',
            'en' => 'Country',
            'cn' => '国',
        ]
    ],

    [
        'group' => 'cabinet',
        'key' => 'import.available_type',
        'text' => [
            'ru' => 'Доступно(ед.изм.)',
            'en' => 'Available(unit)', 
            'cn' => '可用(单位)',
        ]
    ],

    [
        'group' => 'cabinet',
        'key' => 'import.describle_images',
        'text' => [
            'ru' => 'Изображения для блока описание',
            'en' => 'Image for product card', 
            'cn' => '图像产品的卡',
        ]
    ],

    [
        'group' => 'cabinet',
        'key' => 'import.youtube',
        'text' => [
            'ru' => 'YouTube',
            'en' => 'YouTube', 
            'cn' => 'YouTube',
        ]
    ],

    [
        'group' => 'cabinet',
        'key' => 'import.weight',
        'text' => [
            'ru' => 'Вес',
            'en' => 'Weight', 
            'cn' => '重量',
        ]
    ],

    [
        'group' => 'cabinet',
        'key' => 'import.seo_description',
        'text' => [
            'ru' => 'SEO Описание',
            'en' => 'SEO Description', 
            'cn' => 'SEO 说明',
        ]
    ],

    [
        'group' => 'cabinet',
        'key' => 'import.seo_title',
        'text' => [
            'ru' => 'SEO Заголовок',
            'en' => 'SEO Title', 
            'cn' => 'SEO 标题',
        ]
    ],

    [
        'group' => 'cabinet',
        'key' => 'import.seo_keywords',
        'text' => [
            'ru' => 'SEO Ключевые слова',
            'en' => 'SEO Keywords', 
            'cn' => 'SEO 关键字',
        ]
    ],

    [
        'group' => 'cabinet',
        'key' => 'import.weight_type',
        'text' => [
            'ru' => 'Вес (ед.изм)',
            'en' => 'Weight (units)', 
            'cn' => '重量(单位)',
        ]
    ],

    [
        'group' => 'cabinet',
        'key' => 'import.lenght',
        'text' => [
            'ru' => 'Длина',
            'en' => 'Lenght', 
            'cn' => '长',
        ]
    ],

    [
        'group' => 'cabinet',
        'key' => 'import.width',
        'text' => [
            'ru' => 'Ширина',
            'en' => 'Width', 
            'cn' => '宽',
        ]
    ],

    [
        'group' => 'cabinet',
        'key' => 'import.height',
        'text' => [
            'ru' => 'Высота',
            'en' => 'Height', 
            'cn' => '高度',
        ]
    ],

    [
        'group' => 'cabinet',
        'key' => 'import.size_type',
        'text' => [
            'ru' => 'Габариты (ед.изм)',
            'en' => 'Size (units)', 
            'cn' => '大小(单位)',
        ]
    ],

    [
        'group' => 'cabinet',
        'key' => 'import.color_example',
        'text' => [
            'ru' => 'Пример',
            'en' => 'Example',
            'cn' => '例',
        ]
    ],

    [
        'group' => 'cabinet',
        'key' => 'import.color',
        'text' => [
            'ru' => 'Цвет',
            'en' => 'Color',
            'cn' => '颜色',
        ]
    ],

    [
        'group' => 'cabinet',
        'key' => 'import.color_list',
        'text' => [
            'ru' => 'Цвета',
            'en' => 'Colors',
            'cn' => '颜色',
        ]
    ],

    [
        'group' => 'cabinet',
        'key' => 'import.country_list',
        'text' => [
            'ru' => 'Страны',
            'en' => 'Countries',
            'cn' => '国家',
        ]
    ],

    [
        'group' => 'cabinet',
        'key' => 'import.category_list',
        'text' => [
            'ru' => 'Категории',
            'en' => 'Categories',
            'cn' => '类别',
        ]
    ],

    [
        'group' => 'cabinet',
        'key' => 'import.min_price',
        'text' => [
            'ru' => 'Цена от',
            'en' => 'Price min',
            'cn' => '价格分钟',
        ]
    ],

    [
        'group' => 'cabinet',
        'key' => 'import.max_price',
        'text' => [
            'ru' => 'Цена до',
            'en' => 'Price max',
            'cn' => '价格max',
        ]
    ],

    [
        'group' => 'cabinet',
        'key' => 'import.min_order',
        'text' => [
            'ru' => 'Минимальный заказ',
            'en' => 'Minimal order',
            'cn' => '最小的了',
        ]
    ],

    [
        'group' => 'cabinet',
        'key' => 'import.articul',
        'text' => [
            'ru' => 'Артикул',
            'en' => 'Articul',
            'cn' => 'Articul',
        ]
    ],

    [
        'group' => 'cabinet',
        'key' => 'import.preview_image',
        'text' => [
            'ru' => 'Превью',
            'en' => 'Preview image',
            'cn' => '的预览的图像',
        ]
    ],

    [
        'group' => 'cabinet',
        'key' => 'import.detail_images',
        'text' => [
            'ru' => 'Доп изображения',
            'en' => 'Detail images',
            'cn' => '详细的图像',
        ]
    ],

    [
        'group' => 'cabinet',
        'key' => 'import.available_required',
        'text' => [
            'ru' => 'Не выбрана колонка для доступного количества',
            'en' => 'Not selected for column available',
            'cn' => '不选择列可用',
        ]
    ],

    [
        'group' => 'cabinet',
        'key' => 'import.articul_required',
        'text' => [
            'ru' => 'Не выбрана колонка для артикула',
            'en' => 'Not selected for column articul',
            'cn' => '不选择列articul',
        ]
    ],

    [
        'group' => 'cabinet',
        'key' => 'import.country_required',
        'text' => [
            'ru' => 'Не выбрана колонка для страны',
            'en' => 'Not selected for column country',
            'cn' => '不选择列国家',
        ]
    ],

    [
        'group' => 'cabinet',
        'key' => 'import.name_required',
        'text' => [
            'ru' => 'Не выбрана колонка для наименования',
            'en' => 'Not selected for column names',
            'cn' => '不选择列名',
        ]
    ],

    [
        'group' => 'cabinet',
        'key' => 'import.description_required',
        'text' => [
            'ru' => 'Не указана колонка для описания',
            'en' => 'Not specified column to describe',
            'cn' => '未指定柱来描述',
        ]
    ],

    [
        'group' => 'cabinet',
        'key' => 'import.category_required',
        'text' => [
            'ru' => 'Не указана колонка для категорий',
            'en' => 'Not specified column for categories',
            'cn' => '不指定列类别',
        ]
    ],

    [
        'group' => 'cabinet',
        'key' => 'import.price_min_required',
        'text' => [
            'ru' => 'Не указана колонка для минимальной цены',
            'en' => 'Not specified column for the minimum price',
            'cn' => '不指定的列于最低价格',
        ]
    ],

    [
        'group' => 'cabinet',
        'key' => 'import.price_max_required',
        'text' => [
            'ru' => 'Не указана колонка для максимальной цены',
            'en' => 'Not specified column for the maximum price',
            'cn' => '不指定列为最高价',
        ]
    ],

    [
        'group' => 'cabinet',
        'key' => 'import.order_min_required',
        'text' => [
            'ru' => 'Не указана колонка для минимального количества для заказа',
            'en' => 'Not specified column for the minimum required number',
            'cn' => '不指定列为最低要求数量',
        ]
    ],

    [
        'group' => 'cabinet',
        'key' => 'import.preview_required',
        'text' => [
            'ru' => 'Не указана колонка для превью',
            'en' => 'Not specified column for preview',
            'cn' => '不指定列于预览',
        ]
    ],
    
    //Продукты
    [
        'group' => 'cabinet',
        'key' => 'products',
        'text' => [
            'ru' => 'Продукты',
            'en' => 'Products',
            'cn' => '产品',
        ]
    ],
    
    [
        'group' => 'cabinet',
        'key' => 'image',
        'text' => [
            'ru' => 'Фото',
            'en' => 'Image',
            'cn' => '图像',
        ]
    ],

    [
        'group' => 'cabinet',
        'key' => 'product.add',
        'text' => [
            'ru' => 'Добавить товар',
            'en' => 'Add product',
            'cn' => '添加的产品',
        ]
    ],

    [
        'group' => 'cabinet',
        'key' => 'product.model',
        'text' => [
            'ru' => 'Торговое предложение',
            'en' => 'Model',
            'cn' => '模型',
        ]
    ],
    
    [
        'group' => 'cabinet',
        'key' => 'product.product_not_active',
        'text' => [
            'ru' => 'Товара нет в наличии',
            'en' => 'Product out of stock',
            'cn' => '产品缺货',
        ]
    ],

    [
        'group' => 'cabinet',
        'key' => 'product.models',
        'text' => [
            'ru' => 'Торговые предложения',
            'en' => 'Models',
            'cn' => '模型',
        ]
    ],

    [
        'group' => 'cabinet',
        'key' => 'product.status',
        'text' => [
            'ru' => 'Статус товаров',
            'en' => 'Status of products',
            'cn' => '状态的产品',
        ]
    ],

    [
        'group' => 'cabinet',
        'key' => 'product.all',
        'text' => [
            'ru' => 'Все товары',
            'en' => 'All products',
            'cn' => '所有产品',
        ]
    ],

    [
        'group' => 'cabinet',
        'key' => 'product.create',
        'text' => [
            'ru' => 'Разместить товар',
            'en' => 'Place product',
            'cn' => '地方产品',
        ]
    ],
    
    [
        'group' => 'cabinet',
        'key' => 'product.product',
        'text' => [
            'ru' => 'Товар',
            'en' => 'Product',
            'cn' => '产品',
        ]
    ],
    
    [
        'group' => 'cabinet',
        'key' => 'product.product_models',
        'text' => [
            'ru' => 'Торговые предложения',
            'en' => 'Product offers',
            'cn' => '产品',
        ]
    ],
    
    [
        'group' => 'cabinet',
        'key' => 'product.form_add_product_model',
        'text' => [
            'ru' => 'Добавление торгового предложения',
            'en' => 'Adding product offer',
            'cn' => '添加提供的产品',
        ]
    ],
    
    [
        'group' => 'cabinet',
        'key' => 'product.form_editing_product_model',
        'text' => [
            'ru' => 'Изменение торгового предложения',
            'en' => 'Editing product offer',
            'cn' => '编辑提供的产品',
        ]
    ],

    [
        'group' => 'cabinet',
        'key' => 'product.choose_category',
        'text' => [
            'ru' => 'Выбор категории',
            'en' => 'Choose category',
            'cn' => '选择类别',
        ]
    ],

    //Поля форм (Продукт)
    [
        'group' => 'cabinet',
        'key' => 'product.form_articul',
        'text' => [
            'ru' => 'Артикул',
            'en' => 'Articul',
            'cn' => 'Articul',
        ]
    ],

    [
        'group' => 'cabinet',
        'key' => 'product.form_certification',
        'text' => [
            'ru' => 'Сертификация',
            'en' => 'Certification',
            'cn' => 'Certification',
        ]
    ],

    [
        'group' => 'cabinet',
        'key' => 'product.form_new_characteristic_name',
        'text' => [
            'ru' => 'Название',
            'en' => 'Name',
            'cn' => 'Name',
        ]
    ],

    [
        'group' => 'cabinet',
        'key' => 'product.form_new_characteristic_value',
        'text' => [
            'ru' => 'Значение',
            'en' => 'Value',
            'cn' => 'Value',
        ]
    ],

    [
        'group' => 'cabinet',
        'key' => 'product.form_package_type',
        'text' => [
            'ru' => 'Тип упаковки',
            'en' => 'Package type',
            'cn' => 'Package type',
        ]
    ],

    [
        'group' => 'cabinet',
        'key' => 'product.form_shelf_life',
        'text' => [
            'ru' => 'Срок годности',
            'en' => 'Shelf life',
            'cn' => 'Shelf life',
        ]
    ],

    [
        'group' => 'cabinet',
        'key' => 'product.form_category_product',
        'text' => [
            'ru' => 'Категория товара',
            'en' => 'Product category',
            'cn' => '产品类别',
        ]
    ],
    
    [
        'group' => 'cabinet',
        'key' => 'product.form_category',
        'text' => [
            'ru' => 'Категория',
            'en' => 'Category',
            'cn' => '类别',
        ]
    ],
    
    [
        'group' => 'cabinet',
        'key' => 'product.form_main_properties',
        'text' => [
            'ru' => 'Основные характеристики',
            'en' => 'Main properties',
            'cn' => '主要性质',
        ]
    ],

    [
        'group' => 'cabinet',
        'key' => 'product.form_country',
        'text' => [
            'ru' => 'Страна',
            'en' => 'Country',
            'cn' => '国',
        ]
    ],
    
    [
        'group' => 'cabinet',
        'key' => 'product.form_producing_country',
        'text' => [
            'ru' => 'Страна производитель',
            'en' => 'Producing country',
            'cn' => '生产国',
        ]
    ],

    [
        'group' => 'cabinet',
        'key' => 'product.form_city',
        'text' => [
            'ru' => 'Город',
            'en' => 'City',
            'cn' => '城市',
        ]
    ],

    [
        'group' => 'cabinet',
        'key' => 'product.form_model',
        'text' => [
            'ru' => 'Торговое предложение',
            'en' => 'Offer',
            'cn' => '提供',
        ]
    ],

    [
        'group' => 'cabinet',
        'key' => 'product.form_model_preview_image',
        'text' => [
            'ru' => 'Изображение для превью',
            'en' => 'Offer preview',
            'cn' => '提供预览',
        ]
    ],

    [
        'group' => 'cabinet',
        'key' => 'product.form_price',
        'text' => [
            'ru' => 'Цена',
            'en' => 'Price',
            'cn' => '价格',
        ]
    ],

    [
        'group' => 'cabinet',
        'key' => 'product.form_documents',
        'text' => [
            'ru' => 'Документы',
            'en' => 'Documents',
            'cn' => '文件',
        ]
    ],

    [
        'group' => 'cabinet',
        'key' => 'product.form_min_order',
        'text' => [
            'ru' => 'Минимальная партия',
            'en' => 'Minimum order',
            'cn' => '最小的了',
        ]
    ],

    [
        'group' => 'cabinet',
        'key' => 'product.form_available',
        'text' => [
            'ru' => 'Доступное количество',
            'en' => 'Available quantity',
            'cn' => '可用数量',
        ]
    ],

    [
        'group' => 'cabinet',
        'key' => 'product.form_color_product',
        'text' => [
            'ru' => 'Цвет товара',
            'en' => 'Product color',
            'cn' => '产品颜色',
        ]
    ],
    
    [
        'group' => 'cabinet',
        'key' => 'product.form_color',
        'text' => [
            'ru' => 'Цвет',
            'en' => 'Color',
            'cn' => '颜色',
        ]
    ],

    [
        'group' => 'cabinet',
        'key' => 'product.form_description',
        'text' => [
            'ru' => 'Краткое описание товара',
            'en' => 'Description product',
            'cn' => '介绍产品',
        ]
    ],

    [
        'group' => 'cabinet',
        'key' => 'product.form_full_description',
        'text' => [
            'ru' => 'Полное описание',
            'en' => 'Full description',
            'cn' => '完整的描述',
        ]
    ],

    [
        'group' => 'cabinet',
        'key' => 'product.form_additional_description',
        'text' => [
            'ru' => 'Дополнительное описание',
            'en' => 'Additional description',
            'cn' => '附加说明',
        ]
    ],

    [
        'group' => 'cabinet',
        'key' => 'product.form_weight',
        'text' => [
            'ru' => 'Вес',
            'en' => 'Weight',
            'cn' => '重量',
        ]
    ],

    [
        'group' => 'cabinet',
        'key' => 'product.form_lenght',
        'text' => [
            'ru' => 'Длина',
            'en' => 'Lenght',
            'cn' => '长',
        ]
    ],

    [
        'group' => 'cabinet',
        'key' => 'product.form_width',
        'text' => [
            'ru' => 'Ширина',
            'en' => 'Width',
            'cn' => '宽',
        ]
    ],

    [
        'group' => 'cabinet',
        'key' => 'product.form_height',
        'text' => [
            'ru' => 'Высота',
            'en' => 'Height',
            'cn' => '高度',
        ]
    ],

    [
        'group' => 'cabinet',
        'key' => 'product.form_seo_title',
        'text' => [
            'ru' => 'Заголовок',
            'en' => 'Title',
            'cn' => '标题',
        ]
    ],

    [
        'group' => 'cabinet',
        'key' => 'product.form_seo_keywords',
        'text' => [
            'ru' => 'Ключевые слова',
            'en' => 'Keywords',
            'cn' => '关键字',
        ]
    ],

    [
        'group' => 'cabinet',
        'key' => 'product.form_seo_descriptions',
        'text' => [
            'ru' => 'Описание',
            'en' => 'Description',
            'cn' => '描述',
        ]
    ],

    [
        'group' => 'cabinet',
        'key' => 'product.form_seo_attributes',
        'text' => [
            'ru' => 'СЕО аттрибуты',
            'en' => 'SEO attributes',
            'cn' => 'SEO 属性',
        ]
    ],

    [
        'group' => 'cabinet',
        'key' => 'product.form_min_price',
        'text' => [
            'ru' => 'Минимальная цена',
            'en' => 'Minimal price',
            'cn' => '最小的代价',
        ]
    ],

    [
        'group' => 'cabinet',
        'key' => 'product.form_max_price',
        'text' => [
            'ru' => 'Максимальная цена',
            'en' => 'Maximal price',
            'cn' => '最高价',
        ]
    ],

    [
        'group' => 'cabinet',
        'key' => 'product.form_name',
        'text' => [
            'ru' => 'Название',
            'en' => 'Title',
            'cn' => '标题',
        ]
    ],

    [
        'group' => 'cabinet',
        'key' => 'product.form_preview_image',
        'text' => [
            'ru' => 'Изображение для списка карточек товаров',
            'en' => 'Image for list of products',
            'cn' => '图像的产品清单',
        ]
    ],

    [
        'group' => 'cabinet',
        'key' => 'product.form_detail_images',
        'text' => [
            'ru' => 'Изображения для просмотра карточек товаров(слайдер)',
            'en' => 'Images to view the card catalog(slider)',
            'cn' => '图像来查看卡片目录(滑板)',
        ]
    ],

    [
        'group' => 'cabinet',
        'key' => 'product.form_describle_images',
        'text' => [
            'ru' => "Изображения для блока 'Описание' карточки товара",
            'en' => "Image for a block of 'Description' of item cards",
            'cn' => "像一块'描述'的项目卡",
        ]
    ],

    [
        'group' => 'cabinet',
        'key' => 'product.form_video',
        'text' => [
            'ru' => 'Видео',
            'en' => 'Video',
            'cn' => '视频',
        ]
    ],

    [
        'group' => 'cabinet',
        'key' => 'product.form_youtube',
        'text' => [
            'ru' => 'Ссылка на ролик в YouTube',
            'en' => 'Link to the YouTube clip',
            'cn' => '链接到YouTube的剪辑',
        ]
    ],

    [
        'group' => 'cabinet',
        'key' => 'product.form_company_promo_banner_images',
        'text' => [
            'ru' => 'Изображения для рекламного баннера',
            'en' => 'Image for the banner',
            'cn' => '图像的旗帜',
        ]
    ],

    [
        'group' => 'cabinet',
        'key' => 'product.form_company_description_image',
        'text' => [
            'ru' => 'Фото для блока описание',
            'en' => 'Photo block description',
            'cn' => '照片块的介绍',
        ]
    ],

    //Запросы на покупку
    [
        'group' => 'cabinet',
        'key' => 'request_products',
        'text' => [
            'ru' => 'Запросы',
            'en' => 'Requests',
            'cn' => '请求',
        ]
    ],

    [
        'group' => 'cabinet',
        'key' => 'request_product.request_product',
        'text' => [
            'ru' => 'Запрос на покупку',
            'en' => 'Request product',
            'cn' => '请求的产品',
        ]
    ],
    
    [
        'group' => 'cabinet',
        'key' => 'request_product.request_product_not_active',
        'text' => [
            'ru' => 'Запрос снят с публикации',
            'en' => 'The request is removed from the publication',
            'cn' => '请求删除该出版物',
        ]
    ],
    
    [
        'group' => 'cabinet',
        'key' => 'request_product.add',
        'text' => [
            'ru' => 'Добавить запрос',
            'en' => 'Add request',
            'cn' => '添加的请求',
        ]
    ],

    [
        'group' => 'cabinet',
        'key' => 'request_product.choose_category',
        'text' => [
            'ru' => 'Создание запроса — Выбор категории',
            'en' => 'Request creation — category selection',
            'cn' => '请求设立类别的选择',
        ]
    ],

    [
        'group' => 'cabinet',
        'key' => 'request_product.fill_properties',
        'text' => [
            'ru' => 'Создание запроса — Заполнение характеристик',
            'en' => 'Request creation - Filling characteristics',
            'cn' => '请求设立-填写特点',
        ]
    ],

    [
        'group' => 'cabinet',
        'key' => 'request_product.status',
        'text' => [
            'ru' => 'Статус запросов',
            'en' => 'Status of requests',
            'cn' => '地位的请求',
        ]
    ],

    [
        'group' => 'cabinet',
        'key' => 'request_product.all',
        'text' => [
            'ru' => 'Все запросы',
            'en' => 'All requests',
            'cn' => '所有的请求',
        ]
    ],

    [
        'group' => 'cabinet',
        'key' => 'request_product.create',
        'text' => [
            'ru' => 'Разместить запрос',
            'en' => 'Place a request',
            'cn' => '地方一个请求',
        ]
    ],

    //Поля форм (Запросы)
    [
        'group' => 'cabinet',
        'key' => 'request_product.form_number',
        'text' => [
            'ru' => 'Номер запроса',
            'en' => 'Number of request',
            'cn' => '的请求数量',
        ]
    ],

    [
        'group' => 'cabinet',
        'key' => 'request_product.form_category',
        'text' => [
            'ru' => 'Категория',
            'en' => 'Category',
            'cn' => '类别',
        ]
    ],

    [
        'group' => 'cabinet',
        'key' => 'request_product.form_country',
        'text' => [
            'ru' => 'Страна',
            'en' => 'Country',
            'cn' => '国',
        ]
    ],

    [
        'group' => 'cabinet',
        'key' => 'request_product.form_min_order',
        'text' => [
            'ru' => 'Необходимое количество',
            'en' => 'Required quantity',
            'cn' => '需求数量',
        ]
    ],

    [
        'group' => 'cabinet',
        'key' => 'request_product.form_full_description',
        'text' => [
            'ru' => 'Полное описание',
            'en' => 'Full description',
            'cn' => '完整的描述',
        ]
    ],

    [
        'group' => 'cabinet',
        'key' => 'request_product.form_seo_title',
        'text' => [
            'ru' => 'Заголовок',
            'en' => 'Title',
            'cn' => '标题',
        ]
    ],

    [
        'group' => 'cabinet',
        'key' => 'request_product.form_seo_keywords',
        'text' => [
            'ru' => 'Ключевые слова',
            'en' => 'Keywords',
            'cn' => '关键字',
        ]
    ],

    [
        'group' => 'cabinet',
        'key' => 'request_product.form_seo_descriptions',
        'text' => [
            'ru' => 'Описание',
            'en' => 'Description',
            'cn' => '描述',
        ]
    ],

    [
        'group' => 'cabinet',
        'key' => 'request_product.form_seo_attributes',
        'text' => [
            'ru' => 'СЕО аттрибуты',
            'en' => 'SEO attributes',
            'cn' => 'SEO 属性',
        ]
    ],

    [
        'group' => 'cabinet',
        'key' => 'request_product.form_min_price',
        'text' => [
            'ru' => 'Минимальная цена',
            'en' => 'Minimal price',
            'cn' => '最小的代价',
        ]
    ],

    [
        'group' => 'cabinet',
        'key' => 'request_product.form_max_price',
        'text' => [
            'ru' => 'Максимальная цена',
            'en' => 'Maximal price',
            'cn' => '最高价',
        ]
    ],

    [
        'group' => 'cabinet',
        'key' => 'request_product.form_name',
        'text' => [
            'ru' => 'Название',
            'en' => 'Title',
            'cn' => '标题',
        ]
    ],

    [
        'group' => 'cabinet',
        'key' => 'request_product.form_detail_images',
        'text' => [
            'ru' => 'Изображения для описания запроса',
            'en' => 'Images for the request description',
            'cn' => '图像的请求的说明',
        ]
    ],

    //Профили
    [
        'group' => 'cabinet',
        'key' => 'profile.choose_language',
        'text' => [ 
            'ru' => 'Выберите язык профиля',
            'en' => 'Choose profile language',
            'cn' => '选择配置文件的语言',
        ]
    ],
    
    [
        'group' => 'cabinet',
        'key' => 'profile.choose_category',
        'text' => [ 
            'ru' => 'Выберите категорию',
            'en' => 'Choose category',
            'cn' => '选择类别',
        ]
    ],
    
    [
        'group' => 'cabinet',
        'key' => 'profile.type',
        'text' => [ 
            'ru' => 'Тип профиля',
            'en' => 'Profile type',
            'cn' => '配置文件的类型',
        ]
    ],
    
    [
        'group' => 'cabinet',
        'key' => 'profile.language',
        'text' => [ 
            'ru' => 'Language',
            'en' => 'Language',
            'cn' => '语言',
        ]
    ],

    [
        'group' => 'cabinet',
        'key' => 'profile.require_approve',
        'text' => [ 
            'ru' => 'Подтвердите хотя-бы один из ваших профилей, чтобы воспользоваться полным функционалом сайта.',
            'en' => 'Confirm at least one of your profiles to use the full functionality of the website.',
            'cn' => '确认至少一个配置文件使用的全部功能的网站。',
        ]
    ],

    [
        'group' => 'cabinet',
        'key' => 'profile.not_approved',
        'text' => [
            'ru' => 'Профиль не подтверждён!',
            'en' => 'Profile is not confirmed!',
            'cn' => '配置文件不是证实！',
        ]
    ],

    [
        'group' => 'cabinet',
        'key' => 'profile.profile',
        'text' => [ 
            'ru' => 'Профиль',
            'en' => 'Profile',
            'cn' => '配置文件',
        ]
    ],

    [
        'group' => 'cabinet',
        'key' => 'profile.buyer',
        'text' => [
            'ru' => 'Покупатель',
            'en' => 'Buyer',
            'cn' => '买家',
        ]
    ],

    [
        'group' => 'cabinet',
        'key' => 'profile.seller',
        'text' => [
            'ru' => 'Продавец',
            'en' => 'Seller',
            'cn' => '卖家',
        ]
    ],

    [
        'group' => 'cabinet',
        'key' => 'profile.name',
        'text' => [
            'ru' => 'Имя',
            'en' => 'Name',
            'cn' => '名称',
        ]
    ],

    [
        'group' => 'cabinet',
        'key' => 'profile.create_buyer',
        'text' => [
            'ru' => 'Создать профиль покупателя',
            'en' => 'Create a buyer profile',
            'cn' => '创建一个买家',
        ]
    ],
    
    [
        'group' => 'cabinet',
        'key' => 'profile.create_seller',
        'text' => [
            'ru' => 'Создать профиль продавца',
            'en' => 'Create a seller profile',
            'cn' => '创建一个卖方的配置文件',
        ]
    ],
    
    [
        'group' => 'cabinet',
        'key' => 'profile.creating_buyer_profile',
        'text' => [
            'ru' => 'Создание профиля покупателя',
            'en' => 'Creating a buyer profile',
            'cn' => '创建一个买家',
        ]
    ],
    
    [
        'group' => 'cabinet',
        'key' => 'profile.creating_seller_profile',
        'text' => [
            'ru' => 'Создание профиля продавца',
            'en' => 'Creating a seller profile',
            'cn' => '创建一个卖方的配置文件',
        ]
    ],
    
    [
        'group' => 'cabinet',
        'key' => 'profile.editing_buyer_profile',
        'text' => [
            'ru' => 'Изменение профиля покупателя',
            'en' => 'Creating a buyer profile',
            'cn' => '创建一个买家',
        ]
    ],
    
    [
        'group' => 'cabinet',
        'key' => 'profile.editing_seller_profile',
        'text' => [
            'ru' => 'Изменение профиля продавца',
            'en' => 'Creating a seller profile',
            'cn' => '创建一个卖方的配置文件',
        ]
    ],

    [
        'group' => 'cabinet',
        'key' => 'profile.actions',
        'text' => [
            'ru' => 'Действия',
            'en' => 'Actions',
            'cn' => '行动',
        ]
    ],

    [
        'group' => 'cabinet',
        'key' => 'profile.status',
        'text' => [
            'ru' => 'Статус',
            'en' => 'Status',
            'cn' => '状态',
        ]
    ],

    [
        'group' => 'cabinet',
        'key' => 'profile.language',
        'text' => [
            'ru' => 'Язык профиля',
            'en' => 'Language',
            'cn' => '语言',
        ]
    ],

    [
        'group' => 'cabinet',
        'key' => 'profile.create_profile_another_language',
        'text' => [
            'ru' => 'Создать профиль на другом языке',
            'en' => 'Create profile in another language',
            'cn' => '创建档案中的另一种语言',
        ]
    ],

    [
        'group' => 'cabinet',
        'key' => 'profile.approved',
        'text' => [
            'ru' => 'Подтверждён',
            'en' => 'Approved',
            'cn' => '批准',
        ]
    ],

    [
        'group' => 'cabinet',
        'key' => 'profile.not_approved',
        'text' => [
            'ru' => 'Не подтверждён',
            'en' => 'Not approved',
            'cn' => '未获批准',
        ]
    ],

    [
        'group' => 'cabinet',
        'key' => 'profile.wait_approve',
        'text' => [
            'ru' => 'Ожидает подтверждения',
            'en' => 'Waiting for approve',
            'cn' => '在等待批准',
        ]
    ],

    [
        'group' => 'cabinet',
        'key' => 'profile.attached_files',
        'text' => [
            'ru' => 'Прикреплённые файлы',
            'en' => 'Attached files',
            'cn' => '附加文件',
        ]
    ],

        //Поля форм (профиль)
        [
            'group' => 'cabinet',
            'key' => 'profile.category',
            'text' => [
                'ru' => 'Категория',
                'en' => 'Category',
                'cn' => '类别',
            ]
        ],

        [
            'group' => 'cabinet',
            'key' => 'profile.form_position',
            'text' => [
                'ru' => 'Должность',
                'en' => 'Position',
                'cn' => '位置',
            ]
        ],

        [
            'group' => 'cabinet',
            'key' => 'profile.form_country',
            'text' => [
                'ru' => 'Страна',
                'en' => 'Country',
                'cn' => '国',
            ]
        ],

        [
            'group' => 'cabinet',
            'key' => 'profile.form_city',
            'text' => [
                'ru' => 'Город',
                'en' => 'City',
                'cn' => '城市',
            ]
        ],

        [
            'group' => 'cabinet',
            'key' => 'profile.form_documents',
            'text' => [
                'ru' => 'Документы',
                'en' => 'Documents',
                'cn' => '文件',
            ]
        ],

        [
            'group' => 'cabinet',
            'key' => 'profile.form_description',
            'text' => [
                'ru' => 'Описание',
                'en' => 'Description',
                'cn' => '描述',
            ]
        ],

        [
            'group' => 'cabinet',
            'key' => 'profile.form_index',
            'text' => [
                'ru' => 'Индекс',
                'en' => 'Postal index',
                'cn' => '邮政指数',
            ]
        ],
        
        [
            'group' => 'cabinet',
            'key' => 'profile.form_address',
            'text' => [
                'ru' => 'Адрес',
                'en' => 'Address',
                'cn' => '地址',
            ]
        ],

        [
            'group' => 'cabinet',
            'key' => 'profile.form_street',
            'text' => [
                'ru' => 'Улица',
                'en' => 'Street',
                'cn' => '街',
            ]
        ],

        [
            'group' => 'cabinet',
            'key' => 'profile.form_number_home',
            'text' => [
                'ru' => 'Дом',
                'en' => 'House',
                'cn' => '的房子',
            ]
        ],

        [
            'group' => 'cabinet',
            'key' => 'profile.form_number_build',
            'text' => [
                'ru' => 'Корпус',
                'en' => 'Case',
                'cn' => '情况下',
            ]
        ],
        
        [
            'group' => 'cabinet',
            'key' => 'profile.form_company_youtube_video',
            'text' => [
                'ru' => 'Видео',
                'en' => 'Video',
                'cn' => '视频',
            ]
        ],

        [
            'group' => 'cabinet',
            'key' => 'profile.form_number_room',
            'text' => [
                'ru' => 'Помещение',
                'en' => 'Room',
                'cn' => '房间',
            ]
        ],

        [
            'group' => 'cabinet',
            'key' => 'profile.form_last_name',
            'text' => [
                'ru' => 'Фамилия',
                'en' => 'Last name',
                'cn' => '最后一名',
            ]
        ],

        [
            'group' => 'cabinet',
            'key' => 'profile.form_patronymic',
            'text' => [
                'ru' => 'Отчество',
                'en' => 'Patronymic',
                'cn' => '父',
            ]
        ],

        [
            'group' => 'cabinet',
            'key' => 'profile.form_gender_man',
            'text' => [
                'ru' => 'Мужской',
                'en' => 'Male',
                'cn' => '男性',
            ]
        ],

        [
            'group' => 'cabinet',
            'key' => 'profile.form_gender_women',
            'text' => [
                'ru' => 'Женский',
                'en' => 'Female',
                'cn' => '女性',
            ]
        ],

        [
            'group' => 'cabinet',
            'key' => 'profile.form_image',
            'text' => [
                'ru' => 'Изображение',
                'en' => 'Image',
                'cn' => '图像',
            ]
        ],

        [
            'group' => 'cabinet',
            'key' => 'profile.form_gender',
            'text' => [
                'ru' => 'Пол',
                'en' => 'Gender',
                'cn' => '性别问题',
            ]
        ],

        [
            'group' => 'cabinet',
            'key' => 'profile.form_phone',
            'text' => [
                'ru' => 'Телефон',
                'en' => 'Phone',
                'cn' => '电话',
            ]
        ],

        [
            'group' => 'cabinet',
            'key' => 'profile.form_birthday',
            'text' => [
                'ru' => 'День рождения',
                'en' => 'Brithday',
                'cn' => '生日',
            ]
        ],

        [
            'group' => 'cabinet',
            'key' => 'profile.form_number_phone',
            'text' => [
                'ru' => 'Телефон',
                'en' => 'Phone',
                'cn' => '电话',
            ]
        ],

        [
            'group' => 'cabinet',
            'key' => 'profile.form_number_phone_mobile',
            'text' => [
                'ru' => 'Мобильный телефон',
                'en' => 'Mobile phone',
                'cn' => '移动电话的',
            ]
        ],

        [
            'group' => 'cabinet',
            'key' => 'profile.form_profile_manager',
            'text' => [
                'ru' => 'Менеджеры профиля',
                'en' => 'Managers profile',
                'cn' => '管理人员简介',
            ]
        ],

        [
            'group' => 'cabinet',
            'key' => 'profile.form_company_name',
            'text' => [
                'ru' => 'Название компании',
                'en' => 'Company name',
                'cn' => '公司名称',
            ]
        ],

        [
            'group' => 'cabinet',
            'key' => 'profile.form_region',
            'text' => [
                'ru' => 'Область / штат / республика / край',
                'en' => 'State',
                'cn' => '状态',
            ]
        ],

        [
            'group' => 'cabinet',
            'key' => 'profile.form_information_about_company',
            'text' => [
                'ru' => 'Информация о компании',
                'en' => 'Information about company',
                'cn' => '有关的信息公司',
            ]
        ],

        [
            'group' => 'cabinet',
            'key' => 'profile.form_information_contacts',
            'text' => [
                'ru' => 'Контактная информация',
                'en' => 'Contact information',
                'cn' => '联系信息',
            ]
        ],

        [
            'group' => 'cabinet',
            'key' => 'profile.form_shop_design',
            'text' => [
                'ru' => 'Оформление главной страницы магазина',
                'en' => 'Design of the main store page',
                'cn' => '设计的主页商店',
            ]
        ],

        [
            'group' => 'cabinet',
            'key' => 'profile.form_shop_scheme',
            'text' => [
                'ru' => 'Схема магазина',
                'en' => 'The scheme of the store',
                'cn' => '该方案的商店',
            ]
        ],

        [
            'group' => 'cabinet',
            'key' => 'profile.form_photos_videos_company',
            'text' => [
                'ru' => 'Фото и видео компании',
                'en' => 'Photo and video company',
                'cn' => '照片和视频的公司',
            ]
        ],

        [
            'group' => 'cabinet',
            'key' => 'profile.form_company_main_banner_images',
            'text' => [
                'ru' => 'Главный баннер магазина(обложка)',
                'en' => 'Main banner of the store(cover)',
                'cn' => '主要旗帜的商店(封面)',
            ]
        ],

        [
            'group' => 'cabinet',
            'key' => 'profile.form_company_logo_image',
            'text' => [
                'ru' => 'Логотип компании',
                'en' => 'Company logo',
                'cn' => '公司的标志',
            ]
        ],

        [
            'group' => 'cabinet',
            'key' => 'profile.form_name',
            'text' => [
                'ru' => 'Имя',
                'en' => 'Name',
                'cn' => '名称',
            ]
        ],

        [
            'group' => 'cabinet',
            'key' => 'profile.form_video',
            'text' => [
                'ru' => 'Видео',
                'en' => 'Video',
                'cn' => '视频',
            ]
        ],

        [
            'group' => 'cabinet',
            'key' => 'profile.form_youtube',
            'text' => [
                'ru' => 'Ссылка на ролик в YouTube',
                'en' => 'Link to the YouTube clip',
                'cn' => '链接到YouTube的剪辑',
            ]
        ],

        [
            'group' => 'cabinet',
            'key' => 'profile.form_company_promo_banner_images',
            'text' => [
                'ru' => 'Изображения для рекламного баннера',
                'en' => 'Image for the banner',
                'cn' => '图像的旗帜',
            ]
        ],

        [
            'group' => 'cabinet',
            'key' => 'profile.form_company_description_image',
            'text' => [
                'ru' => 'Фото для блока описание',
                'en' => 'Photo block description',
                'cn' => '照片块的介绍',
            ]
        ],

        [
            'group' => 'cabinet',
            'key' => 'profile.form_information_contacts',
            'text' => [
                'ru' => 'Контактная информация',
                'en' => 'Contact information',
                'cn' => '联系信息',
            ]
        ],

        [
            'group' => 'cabinet',
            'key' => 'profile.form_company_description',
            'text' => [
                'ru' => 'Описание компании',
                'en' => 'Company description',
                'cn' => '公司的说明',
            ]
        ],

        [
            'group' => 'cabinet',
            'key' => 'profile.form_company_other_images',
            'text' => [
                'ru' => 'Другие фото компании',
                'en' => 'Other company photos',
                'cn' => '其他公司的照片',
            ]
        ],

        [
            'group' => 'cabinet',
            'key' => 'profile.form_company_youtube_link',
            'text' => [
                'ru' => 'Ссылка на ролик в YouTube',
                'en' => 'Link to the YouTube clip',
                'cn' => '链接到YouTube的剪辑',
            ]
        ],

    //Менеджеры
    [
        'group' => 'cabinet',
        'key' => 'manager.manager',
        'text' => [
            'ru' => 'Менеджер',
            'en' => 'Manager',
            'cn' => '经理',
        ]
    ],

    [
        'group' => 'cabinet',
        'key' => 'manager.status',
        'text' => [
            'ru' => 'Статус',
            'en' => 'Status',
            'cn' => '状态',
        ]
    ],

    [
        'group' => 'cabinet',
        'key' => 'manager.type',
        'text' => [
            'ru' => 'Тип',
            'en' => 'Type',
            'cn' => '类型',
        ]
    ],

    [
        'group' => 'cabinet',
        'key' => 'manager.profile',
        'text' => [
            'ru' => 'Профиль',
            'en' => 'Profile',
            'cn' => '配置文件',
        ]
    ],

    [
        'group' => 'cabinet',
        'key' => 'manager.mail',
        'text' => [
            'ru' => 'E-Mail',
            'en' => 'E-Mail',
            'cn' => '电子邮件',
        ]
    ],

    [
        'group' => 'cabinet',
        'key' => 'manager.actions',
        'text' => [
            'ru' => 'Действия',
            'en' => 'Actions',
            'cn' => '行动',
        ]
    ],
        //Поля форм (Менеджер)
        [
            'group' => 'cabinet',
            'key' => 'manager.form_profile',
            'text' => [
                'ru' => 'Профиль',
                'en' => 'Profile',
                'cn' => '配置文件',
            ]
        ],

        [
            'group' => 'cabinet',
            'key' => 'manager.form_company_name',
            'text' => [
                'ru' => 'Название компании',
                'en' => 'Company name',
                'cn' => '公司名称',
            ]
        ],

        [
            'group' => 'cabinet',
            'key' => 'manager.form_email',
            'text' => [
                'ru' => 'E-Mail',
                'en' => 'E-Mail',
                'cn' => '电子邮件',
            ]
        ],

        [
            'group' => 'cabinet',
            'key' => 'manager.form_password',
            'text' => [
                'ru' => 'Пароль',
                'en' => 'Password',
                'cn' => '密码',
            ]
        ],

        [
            'group' => 'cabinet',
            'key' => 'manager.approved',
            'text' => [
                'ru' => 'Подтверждён',
                'en' => 'Approved',
                'cn' => '批准',
            ]
        ],

        [
            'group' => 'cabinet',
            'key' => 'manager.not_approved',
            'text' => [
                'ru' => 'Не подтверждён',
                'en' => 'Not approved',
                'cn' => '未获批准',
            ]
        ],

        [
            'group' => 'cabinet',
            'key' => 'manager.wait_approve',
            'text' => [
                'ru' => 'В ожидании',
                'en' => 'Waiting',
                'cn' => '等待',
            ]
        ],

        [
            'group' => 'cabinet',
            'key' => 'manager.form_confirm_password',
            'text' => [
                'ru' => 'Подтвердить пароль',
                'en' => 'Password confirmation',
                'cn' => '确认密码',
            ]
        ],

        [
            'group' => 'cabinet',
            'key' => 'manager.form_last_name',
            'text' => [
                'ru' => 'Фамилия',
                'en' => 'Last name',
                'cn' => '最后一名',
            ]
        ],

        [
            'group' => 'cabinet',
            'key' => 'manager.form_position',
            'text' => [
                'ru' => 'Должность',
                'en' => 'Position',
                'cn' => '位置',
            ]
        ],

        [
            'group' => 'cabinet',
            'key' => 'manager.form_patronymic',
            'text' => [
                'ru' => 'Отчество',
                'en' => 'Patronymic',
                'cn' => '父',
            ]
        ],

        [
            'group' => 'cabinet',
            'key' => 'manager.form_gender_man',
            'text' => [
                'ru' => 'Мужской',
                'en' => 'Male',
                'cn' => '男性',
            ]
        ],

        [
            'group' => 'cabinet',
            'key' => 'manager.form_gender_women',
            'text' => [
                'ru' => 'Женский',
                'en' => 'Female',
                'cn' => '女性',
            ]
        ],

        [
            'group' => 'cabinet',
            'key' => 'manager.form_image',
            'text' => [
                'ru' => 'Изображение',
                'en' => 'Image',
                'cn' => '图像',
            ]
        ],

        [
            'group' => 'cabinet',
            'key' => 'manager.form_gender',
            'text' => [
                'ru' => 'Пол',
                'en' => 'Gender',
                'cn' => '性别问题',
            ]
        ],

        [
            'group' => 'cabinet',
            'key' => 'manager.form_phone',
            'text' => [
                'ru' => 'Телефон',
                'en' => 'Phone',
                'cn' => '电话',
            ]
        ],

        [
            'group' => 'cabinet',
            'key' => 'manager.form_birthday',
            'text' => [
                'ru' => 'День рождения',
                'en' => 'Brithday',
                'cn' => '生日',
            ]
        ],

        [
            'group' => 'cabinet',
            'key' => 'manager.form_name',
            'text' => [
                'ru' => 'Имя',
                'en' => 'Name',
                'cn' => '名称',
            ]
        ],

    //Аккаунт
    [
        'group' => 'cabinet',
        'key' => 'account.name',
        'text' => [
            'ru' => 'Имя',
            'en' => 'Account name',
            'cn' => '帐户名称',
        ]
    ],

    [
        'group' => 'cabinet',
        'key' => 'account.entrance',
        'text' => [
            'ru' => 'Учётная запись',
            'en' => 'Account',
            'cn' => '帐户',
        ]
    ],

    [
        'group' => 'cabinet',
        'key' => 'account.social',
        'text' => [
            'ru' => 'Социальный профиль',
            'en' => 'Social profile',
            'cn' => '社会轮廓',
        ]
    ],

    //Поля форм (Аккаунт)
    [
        'group' => 'cabinet',
        'key' => 'account.form_last_name',
        'text' => [
            'ru' => 'Фамилия',
            'en' => 'Last name',
            'cn' => '最后一名',
        ]
    ],

    [
        'group' => 'cabinet',
        'key' => 'account.form_patronymic',
        'text' => [
            'ru' => 'Отчество',
            'en' => 'Patronymic',
            'cn' => '父',
        ]
    ],

    [
        'group' => 'cabinet',
        'key' => 'account.form_gender_man',
        'text' => [
            'ru' => 'Мужской',
            'en' => 'Male',
            'cn' => '男性',
        ]
    ],

    [
        'group' => 'cabinet',
        'key' => 'account.form_gender_women',
        'text' => [
            'ru' => 'Женский',
            'en' => 'Female',
            'cn' => '女性',
        ]
    ],

    [
        'group' => 'cabinet',
        'key' => 'account.form_change_avatar',
        'text' => [
            'ru' => 'Сменить аватар',
            'en' => 'Change avatar',
            'cn' => '变化的化身',
        ]
    ],

    [
        'group' => 'cabinet',
        'key' => 'account.form_gender',
        'text' => [
            'ru' => 'Пол',
            'en' => 'Gender',
            'cn' => '性别问题',
        ]
    ],

    [
        'group' => 'cabinet',
        'key' => 'account.form_phone',
        'text' => [
            'ru' => 'Телефон',
            'en' => 'Phone',
            'cn' => '电话',
        ]
    ],

    [
        'group' => 'cabinet',
        'key' => 'account.form_birthday',
        'text' => [
            'ru' => 'День рождения',
            'en' => 'Brithday',
            'cn' => '生日',
        ]
    ],

    [
        'group' => 'cabinet',
        'key' => 'account.form_name',
        'text' => [
            'ru' => 'Имя',
            'en' => 'Name',
            'cn' => '名称',
        ]
    ],
    
    [
        'group' => 'cabinet',
        'key' => 'account.login',
        'text' => [
            'ru' => 'Имя',
            'en' => 'Login',
            'cn' => '登录',
        ]
    ],

    [
        'group' => 'cabinet',
        'key' => 'account.current_password',
        'text' => [
            'ru' => 'Текущий пароль',
            'en' => 'Current password',
            'cn' => '当前的密码',
        ]
    ],

    [
        'group' => 'cabinet',
        'key' => 'account.password',
        'text' => [
            'ru' => 'Пароль',
            'en' => 'Password',
            'cn' => '密码',
        ]
    ],

    [
        'group' => 'cabinet',
        'key' => 'account.email',
        'text' => [
            'ru' => 'Еmail',
            'en' => 'Еmail',
            'cn' => 'Еmail',
        ]
    ],

    [
        'group' => 'cabinet',
        'key' => 'account.new_password',
        'text' => [
            'ru' => 'Новый пароль',
            'en' => 'New password',
            'cn' => '新密码',
        ]
    ],
    
    [
        'group' => 'cabinet',
        'key' => 'account.confirm_password',
        'text' => [
            'ru' => 'Повтор пароля',
            'en' => 'Confirm password',
            'cn' => '确认密码',
        ]
    ],

    //Избранное
    [
        'group' => 'cabinet',
        'key' => 'favourite.name',
        'text' => [
            'ru' => 'Избранное',
            'en' => 'Favourite',
            'cn' => '最喜欢的',
        ]
    ],

    [
        'group' => 'cabinet',
        'key' => 'favourite.form_min_order',
        'text' => [
            'ru' => 'От',
            'en' => 'From',
            'cn' => '从',
        ]
    ],

    [
        'group' => 'cabinet',
        'key' => 'chat.support',
        'text' => [
            'ru' => 'Техническая поддержка',
            'en' => 'Support',
            'cn' => '支持',
        ]
    ],

    [
        'group' => 'cabinet',
        'key' => 'chat.index',
        'text' => [
            'ru' => 'Мои чаты',
            'en' => 'My chats',
            'cn' => '我聊天',
        ]
    ],

    [
        'group' => 'cabinet',
        'key' => 'chat.message_write',
        'text' => [
            'ru' => 'Напишите ваше сообщение',
            'en' => 'Write your message',
            'cn' => '写你的消息',
        ]
    ],

    [
        'group' => 'cabinet',
        'key' => 'chat.message_translate',
        'text' => [
            'ru' => 'Перевести',
            'en' => 'Translate',
            'cn' => '翻译',
        ]
    ],

    [
        'group' => 'cabinet',
        'key' => 'chat.message_translate',
        'text' => [
            'ru' => 'Перевести',
            'en' => 'Translate',
            'cn' => '翻译',
        ]
    ],

    [
        'group' => 'cabinet',
        'key' => 'chat.settings',
        'text' => [
            'ru' => 'Настройки',
            'en' => 'Settings',
            'cn' => '设置',
        ]
    ],

    [
        'group' => 'cabinet',
        'key' => 'chat.settings_translate',
        'text' => [
            'ru' => 'Настройки перевода',
            'en' => 'Translate settings',
            'cn' => '翻译设置',
        ]
    ],

    [
        'group' => 'cabinet',
        'key' => 'chat.select_language',
        'text' => [
            'ru' => 'Язык перевода',
            'en' => 'Translate language',
            'cn' => '翻译的语言',
        ]
    ],
];