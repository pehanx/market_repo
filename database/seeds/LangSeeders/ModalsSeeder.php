<?php
$modalsData = [

    [
        'group' => 'modals',
        'key' => 'cancel',
        'text' => [
            'ru' => 'Отмена',
            'en' => 'Cancel',
            'cn' => '取消',
        ]
    ],

    [
        'group' => 'modals',
        'key' => 'delete',
        'text' => [
            'ru' => 'Удалить',
            'en' => 'Delete',
            'cn' => '删除',
        ]
    ],
    
    [
        'group' => 'modals',
        'key' => 'approve_action',
        'text' => [
            'ru' => 'Подтвердите действие',
            'en' => 'Confirm action',
            'cn' => '确认行动',
        ]
    ],

    [
        'group' => 'modals',
        'key' => 'your_sure_delete',
        'text' => [
            'ru' => 'Вы действительно хотите удалить :item ?',
            'en' => 'Do you want to remove :item ?',
            'cn' => '你想要删除的项目？',
        ]
    ],
    
    [
        'group' => 'modals',
        'key' => 'your_sure_activate',
        'text' => [
            'ru' => 'Вы дествительно хотите активировать :item ?',
            'en' => 'You really want to activate the :item ?',
            'cn' => '你真的想要激活 :item?',
        ]
    ],

    [
        'group' => 'modals',
        'key' => 'your_sure_restore',
        'text' => [
            'ru' => 'Вы действительно хотите восстановить :item ?',
            'en' => 'Do you want to recover the :item ?',
            'cn' => '你想要恢复 :item ?',
        ]
    ],

    [
        'group' => 'modals',
        'key' => 'your_sure_disable',
        'text' => [
            'ru' => 'Вы действительно хотите отключить :item ?',
            'en' => 'Do you want to disable the :item ?',
            'cn' => '你想要禁止的 :item ?',
        ]
    ],

    [
        'group' => 'modals',
        'key' => 'your_sure_destroy',
        'text' => [
            'ru' => 'Вы действительно хотите удалить :item ?',
            'en' => 'Are you sure you want to delete :item ?',
            'cn' => 'Are you sure you want to delete :item ?',
        ]
    ],

    [
        'group' => 'modals',
        'key' => 'your_sure_destroy_finally',
        'text' => [
            'ru' => 'Вы действительно хотите окончательно удалить :item ?',
            'en' => 'Are you sure you want to permanently delete the :item ?',
            'cn' => '你确定你想要永久删除 :item ?',
        ]
    ],

    [
        'group' => 'modals',
        'key' => 'your_sure_disable_from_profiles',
        'text' => [
            'ru' => 'Вы действительно хотите отключить :item ? <br/><br/> Выберите профили, для которых необходимо отключить товар:',
            'en' => 'Do you want to disable the :item ? <br/><br/> Select the profile for which you want to disable the product:',
            'cn' => '你想到禁止:item ？<br/><br/">"选择的概要文件，你想到禁止的产品：',
        ]
    ],

    [
        'group' => 'modals',
        'key' => 'your_sure_destroy_from_profiles',
        'text' => [
            'ru' => 'Вы действительно хотите удалить товар ":item"? <br/><br/> Выберите профили, из которых необходимо удалить товар:',
            'en' => 'Do you want to remove item ":item"? <br/><br/> Select the profile from which you want to uninstall the product:',
            'cn' => '你想要删除项目"：项目"? <br/><br/">"选择的概要从其卸载 product:',
        ]
    ],

    [
        'group' => 'modals',
        'key' => 'your_sure_destroy_profile',
        'text' => [
            'ru' => 'Вы действительно хотите удалить профиль :profile ?',
            'en' => 'Do you want to delete the profile?',
            'cn' => '你想要删除的档案吗？',
        ]
    ],

    [
        'group' => 'modals',
        'key' => 'your_sure_destroy_chat',
        'text' => [
            'ru' => 'Вы действительно хотите удалить чат?',
            'en' => 'Do you want to delete the chat?',
            'cn' => '你想删除聊天?',
        ]
    ],

    [
        'group' => 'modals',
        'key' => 'your_sure_destroy_contact',
        'text' => [
            'ru' => 'Вы действительно хотите удалить контакт?',
            'en' => 'Do you want to delete the contact?',
            'cn' => '你想要删除的联系？',
        ]
    ],

    [
        'group' => 'modals',
        'key' => 'your_sure_approve_profile',
        'text' => [
            'ru' => 'Вы действительно хотите активировать профиль :profile ?',
            'en' => 'Do you want to activate the profile?',
            'cn' => '你想激活的档案吗？',
        ]
    ],

    [
        'group' => 'modals',
        'key' => 'your_sure_destroy_manager',
        'text' => [
            'ru' => 'Вы действительно хотите удалить менеджера :Manager ?',
            'en' => 'Do you want to remove the Manager :Manager ?',
            'cn' => '你想要除去的经理 :Manager ?',
        ]
    ],

    [
        'group' => 'modals',
        'key' => 'ok',
        'text' => [
            'ru' => 'ок',
            'en' => 'ok',
            'cn' => 'ok',
        ]
    ],
    
     [
        'group' => 'modals',
        'key' => 'approve',
        'text' => [
            'ru' => 'Подтвердить',
            'en' => 'Approve',
            'cn' => '批准',
        ]
    ],

    [
        'group' => 'modals',
        'key' => 'disable',
        'text' => [
            'ru' => 'Отключить',
            'en' => 'Disable',
            'cn' => '禁用',
        ]
    ],

    [
        'group' => 'modals',
        'key' => 'choose_local_profile',
        'text' => [
            'ru' => 'Выберите язык профиля',
            'en' => 'Select the language profile',
            'cn' => '选择的语言的档案',
        ]
    ],

    [
        'group' => 'modals',
        'key' => 'choose_profile_type',
        'text' => [
            'ru' => 'Выберите тип профиля',
            'en' => 'Select the profile type',
            'cn' => '选择配置的类型',
        ]
    ],
    
    [
        'group' => 'modals',
        'key' => 'create_profile',
        'text' => [
            'ru' => 'Перейти к заполнению профиля',
            'en' => 'To fill in profile',
            'cn' => '填写在档案',
        ]
    ],

    [
        'group' => 'modals',
        'key' => 'checked_elements',
        'text' => [
            'ru' => 'выбранные товары',
            'en' => 'selected products',
            'cn' => '选择的产品',
        ]
    ],

    [
        'group' => 'modals',
        'key' => 'all_elements',
        'text' => [
            'ru' => 'все товары',
            'en' => 'all products',
            'cn' => '所有产品',
        ]
    ],
    
    [
        'group' => 'modals',
        'key' => 'checked_requests',
        'text' => [
            'ru' => 'выбранные запросы',
            'en' => "selected product request's",
            'cn' => '选择的产品的请求的',
        ]
    ],

    [
        'group' => 'modals',
        'key' => 'all_requests',
        'text' => [
            'ru' => 'все запросы',
            'en' => "all product request's",
            'cn' => '所有产品的请求的',
        ]
    ],

    [
        'group' => 'modals',
        'key' => 'buyer',
        'text' => [
            'ru' => 'Покупатель',
            'en' => 'Buyer',
            'cn' => '买家',
        ]
    ],

    [
        'group' => 'modals',
        'key' => 'seller',
        'text' => [
            'ru' => 'Продавец',
            'en' => 'Seller',
            'cn' => '卖家',
        ]
    ],

    [
        'group' => 'modals',
        'key' => 'export',
        'text' => [
            'ru' => 'Выберите параметры для экспорта',
            'en' => 'Select options for export',
            'cn' => '选择选项的出口',
        ]
    ],

    [
        'group' => 'modals',
        'key' => 'export_with_thumbnails',
        'text' => [
            'ru' => 'C превью',
            'en' => 'With a preview image',
            'cn' => '的预览的图像',
        ]
    ],

    [
        'group' => 'modals',
        'key' => 'export_format',
        'text' => [
            'ru' => 'Формат выгрузки',
            'en' => 'The format of export',
            'cn' => '该格式的出口',
        ]
    ],

    [
        'group' => 'modals',
        'key' => 'export_with_models',
        'text' => [
            'ru' => 'C торговыми предложениями',
            'en' => 'With trading offers',
            'cn' => '与贸易提供',
        ]
    ],

    [
        'group' => 'modals',
        'key' => 'chat_settings',
        'text' => [
            'ru' => 'Настройки',
            'en' => 'Settings',
            'cn' => '设置',
        ]
    ],

    [
        'group' => 'modals',
        'key' => 'chat_settings_locale',
        'text' => [
            'ru' => 'Настройки перевода',
            'en' => 'Settings',
            'cn' => '设置',
        ]
    ],

    [
        'group' => 'modals',
        'key' => 'chat_settings_profile',
        'text' => [
            'ru' => 'Профиль',
            'en' => 'Profile',
            'cn' => '配置文件',
        ]
    ],
 
];
?>