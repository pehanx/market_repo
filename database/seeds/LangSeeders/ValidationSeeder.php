<?php
$validationData = [

    [
        'group' => 'validation',
        'key' => 'accepted',
        'text' => [
            'ru' => 'Вы должны принять :attribute.',
            'en' => 'You need to take :attribute.',
            'cn' => '你需要采取 :attribute.',
        ]
    ],

    [
        'group' => 'validation',
        'key' => 'active_url',
        'text' => [
            'ru' => 'Поле :attribute содержит недействительный URL.',
            'en' => 'Field :attribute contains an invalid URL.',
            'cn' => '场 :attribute 包含无效的网址。',
        ]
    ],

    [
        'group' => 'validation',
        'key' => 'after',
        'text' => [
            'ru' => 'В поле :attribute должна быть дата после :date.',
            'en' => 'In the field :attribute must be a date after :date.',
            'cn' => '在该领域 :attribute 必须一日期之后 :date.',
        ]
    ],

    [
        'group' => 'validation',
        'key' => 'after_or_equal',
        'text' => [
            'ru' => 'В поле :attribute должна быть дата после или равняться :date.',
            'en' => 'In the field :attribute must be a date after or equal to :date.',
            'cn' => '在该领域 :attribute 必须一日期之后或等于 :date.',
        ]
    ],

    [
        'group' => 'validation',
        'key' => 'alpha',
        'text' => [
            'ru' => 'Поле :attribute может содержать только буквы.',
            'en' => 'Field :attribute may only contain letters.',
            'cn' => '场 :attribute 可能只包含的信件。',
        ]
    ],

    [
        'group' => 'validation',
        'key' => 'alpha_dash',
        'text' => [
            'ru' => 'Поле :attribute может содержать только буквы, цифры, дефис и нижнее подчеркивание.',
            'en' => 'Field :attribute may only contain letters, numbers, hyphen and underscore.',
            'cn' => '场 :attribute 可能只有含有字母、数字连字符和下划线。',
        ]
    ],

    [
        'group' => 'validation',
        'key' => 'alpha_num',
        'text' => [
            'ru' => 'Поле :attribute может содержать только буквы и цифры.',
            'en' => 'Field :attribute may only contain letters and numbers.',
            'cn' => '场 :attribute 可能只有含有字母和数字。',
        ]
    ],

    [
        'group' => 'validation',
        'key' => 'array',
        'text' => [
            'ru' => 'Поле :attribute должно быть массивом.',
            'en' => 'Field :attribute must be an array.',
            'cn' => '场 :attribute 必须阵列。',
        ]
    ],

    [
        'group' => 'validation',
        'key' => 'before',
        'text' => [
            'ru' => 'В поле :attribute должна быть дата до :date.',
            'en' => 'In the field :attribute must be a date before :date.',
            'cn' => '在该领域 :attribute 必须一日期之前 :date.',
        ]
    ],
    
    [
        'group' => 'validation',
        'key' => 'before_or_equal',
        'text' => [
            'ru' => 'В поле :attribute должна быть дата до или равняться :date.',
            'en' => 'In the field :attribute must be a date before or equal to :date.',
            'cn' => '在该领域 :attribute 必须一日期之前或等于 :date.',
        ]
    ],

    //between
        [
            'group' => 'validation',
            'key' => 'between.numeric',
            'text' => [
                'ru' => 'Поле :attribute должно быть между :min и :max.',
                'en' => 'Field :attribute must be between :min and :max.',
                'cn' => '场 :attribute 必须之间 :min 和 :max.',
            ]
        ],

        [
            'group' => 'validation',
            'key' => 'between.file',
            'text' => [
                'ru' => 'Размер файла в поле :attribute должен быть между :min и :max Килобайт(а).',
                'en' => 'The size of the file in the :attribute must be between :min and :max Kilobyte(s).',
                'cn' => '该文件的大小的 :attribute 必须之间 :min 和 :max 千字节(s)。',
            ]
        ],

        [
            'group' => 'validation',
            'key' => 'between.string',
            'text' => [
                'ru' => 'Количество символов в поле :attribute должно быть между :min и :max.',
                'en' => 'The number of characters in field :attribute must be between :min and :max.',
                'cn' => '该数字在场 :attribute 必须之间 :min 和 :max.',
            ]
        ],

        [
            'group' => 'validation',
            'key' => 'between.array',
            'text' => [
                'ru' => 'Количество элементов в поле :attribute должно быть между :min и :max.',
                'en' => 'The number of elements in the field :attribute must be between :min and :max.',
                'cn' => '元件的数目在该领域 :attribute 必须之间 :min 和 :max.',
            ]
        ],

    [
        'group' => 'validation',
        'key' => 'boolean',
        'text' => [
            'ru' => 'Поле :attribute должно иметь значение логического типа.',
            'en' => 'Field :attribute must be a Boolean value.',
            'cn' => '场 :attribute 必须布尔的价值。',
        ]
    ],

    [
        'group' => 'validation',
        'key' => 'confirmed',
        'text' => [
            'ru' => 'Поле :attribute не совпадает с подтверждением.',
            'en' => 'Field :attribute does not match with the confirmation.',
            'cn' => '场 :attribute 不匹配的确认。',
        ]
    ],

    [
        'group' => 'validation',
        'key' => 'date',
        'text' => [
            'ru' => 'Поле :attribute не является датой.',
            'en' => 'Field :attribute is not a date.',
            'cn' => '场 :attribute 不是一日期。',
        ]
    ],

    [
        'group' => 'validation',
        'key' => 'date_equals',
        'text' => [
            'ru' => 'Поле :attribute должно быть датой равной :date.',
            'en' => 'Field :attribute must be a date equal to date.',
            'cn' => '场 :attribute 必须是一个日期相等的日期。',
        ]
    ],

    [
        'group' => 'validation',
        'key' => 'date_format',
        'text' => [
            'ru' => 'Поле :attribute не соответствует формату :format.',
            'en' => 'Field :attribute does not match the format :format.',
            'cn' => '场 :attribute 不匹配的格式 :format.',
        ]
    ],

    [
        'group' => 'validation',
        'key' => 'different',
        'text' => [
            'ru' => 'Поля :attribute и :other должны различаться.',
            'en' => 'Fields :attribute and :other must be different.',
            'cn' => '田 :attribute 和 :other 必须是不同的。',
        ]
    ],

    [
        'group' => 'validation',
        'key' => 'digits',
        'text' => [
            'ru' => 'Длина цифрового поля :attribute должна быть :digits.',
            'en' => 'Length numeric fields :attribute must be :digits.',
            'cn' => '长度的数字栏 :attribute 必须 :digits.',
        ]
    ],

    [
        'group' => 'validation',
        'key' => 'digits_between',
        'text' => [
            'ru' => 'Длина цифрового поля :attribute должна быть между :min и :max.',
            'en' => 'Length numeric fields :attribute must be between :min and :max.',
            'cn' => '长度的数字栏 :attribute 必须之间 :min 和 :max.',
        ]
    ],

    [
        'group' => 'validation',
        'key' => 'dimensions',
        'text' => [
            'ru' => 'Поле :attribute имеет недопустимые размеры изображения.',
            'en' => 'Field :attribute has invalid dimensions of the image.',
            'cn' => '场 :attribute 无效的尺寸的图像。',
        ]
    ],

    [
        'group' => 'validation',
        'key' => 'distinct',
        'text' => [
            'ru' => 'Поле :attribute содержит повторяющееся значение.',
            'en' => 'Field :attribute has duplicate value.',
            'cn' => '场 :attribute 具有重复的价值。',
        ]
    ],

    [
        'group' => 'validation',
        'key' => 'email',
        'text' => [
            'ru' => 'Поле :attribute должно быть действительным электронным адресом.',
            'en' => 'Field :attribute must be a valid email address.',
            'cn' => '场 :attribute 必须是有效的电子邮件地址。',
        ]
    ],

    [
        'group' => 'validation',
        'key' => 'ends_with',
        'text' => [
            'ru' => 'Поле :attribute должно заканчиваться одним из следующих значений: :values',
            'en' => 'Field :attribute name must end with one of the following values: :values',
            'cn' => '场 :attribute 名称必须结束之一的值如下： :values',
        ]
    ],

    [
        'group' => 'validation',
        'key' => 'exists',
        'text' => [
            'ru' => 'Выбранное значение для :attribute некорректно.',
            'en' => 'The selected :attribute is incorrect.',
            'cn' => '所选择的 :attribute 是不正确的。',
        ]
    ],

    [
        'group' => 'validation',
        'key' => 'file',
        'text' => [
            'ru' => 'Поле :attribute должно быть файлом.',
            'en' => 'Field :attribute must be a file.',
            'cn' => '场 :attribute 必须的文件。',
        ]
    ],

    [
        'group' => 'validation',
        'key' => 'filled',
        'text' => [
            'ru' => 'Поле :attribute обязательно для заполнения.',
            'en' => 'Field :attribute is required.',
            'cn' => '场 :attribute 是必需的。',
        ]
    ],

        //gt
        [
            'group' => 'validation',
            'key' => 'gt.numeric',
            'text' => [
                'ru' => 'Поле :attribute должно быть больше :value.',
                'en' => 'Field :attribute must be greater than :value.',
                'cn' => '场 :attribute 必须大于 :value.',
            ]
        ],

        [
            'group' => 'validation',
            'key' => 'gt.file',
            'text' => [
                'ru' => 'Размер файла в поле :attribute должен быть больше :value Килобайт(а).',
                'en' => 'The size of the file in the :attribute must be greater than :value Kilobyte(s).',
                'cn' => '该文件的大小的 :attribute 必须大于 :value 千字节(s)。',
            ]
        ],

        [
            'group' => 'validation',
            'key' => 'gt.string',
            'text' => [
                'ru' => 'Количество символов в поле :attribute должно быть больше :value.',
                'en' => 'The number of characters in field :attribute must be greater than :value.',
                'cn' => '该数字在场 :attribute 必须大于 :value.',
            ]
        ],

        [
            'group' => 'validation',
            'key' => 'gt.array',
            'text' => [
                'ru' => 'Количество элементов в поле :attribute должно быть больше :value.',
                'en' => 'The number of elements in the field :attribute must be greater than :value.',
                'cn' => '元件的数目在该领域 :attribute 必须大于 :value.',
            ]
        ],

        //gte
        [
            'group' => 'validation',
            'key' => 'gte.numeric',
            'text' => [
                'ru' => 'Поле :attribute должно быть больше или равно :value.',
                'en' => 'Field :attribute must be greater than or equal :value.',
                'cn' => '场 :attribute 必须大于或等于 :value.',
            ]
        ],

        [
            'group' => 'validation',
            'key' => 'gte.file',
            'text' => [
                'ru' => 'Размер файла в поле :attribute должен быть больше или равен :value Килобайт(а).',
                'en' => 'The size of the file in the :attribute must be greater than or equal to :value Kilobyte(s).',
                'cn' => '该文件的大小的 :attribute 必须大于或等于 :value 千字节(s)。',
            ]
        ],

        [
            'group' => 'validation',
            'key' => 'gte.string',
            'text' => [
                'ru' => 'Количество символов в поле :attribute должно быть больше или равно :value.',
                'en' => 'The number of characters in field :attribute must be greater than or equal :value.',
                'cn' => '该数字在场 :attribute 必须大于或等于 :value.',
            ]
        ],

        [
            'group' => 'validation',
            'key' => 'gte.array',
            'text' => [
                'ru' => 'Количество элементов в поле :attribute должно быть больше или равно :value.',
                'en' => 'The number of elements in the field :attribute must be greater than or equal :value.',
                'cn' => '元件的数目在该领域 :attribute 必须大于或等于 :value.',
            ]
        ],

    [
        'group' => 'validation',
        'key' => 'image',
        'text' => [
            'ru' => 'Поле :attribute должно быть изображением.',
            'en' => 'Field :attribute must be an image.',
            'cn' => '场 :attribute 必须是一个图像。',
        ]
    ],

    [
        'group' => 'validation',
        'key' => 'in',
        'text' => [
            'ru' => 'Выбранное значение для :attribute ошибочно.',
            'en' => 'The selected :attribute is wrong.',
            'cn' => '所选择的 :attribute 是错误的。',
        ]
    ],

    [
        'group' => 'validation',
        'key' => 'in_array',
        'text' => [
            'ru' => 'Поле :attribute не существует в :other.',
            'en' => 'Field :attribute does not exist in the :other.',
            'cn' => '场 :attribute 不存在的 :other.',
        ]
    ],

    [
        'group' => 'validation',
        'key' => 'integer',
        'text' => [
            'ru' => 'Поле :attribute должно быть целым числом.',
            'en' => 'Field :attribute must be an integer.',
            'cn' => '场 :attribute 必须是一个整数。',
        ]
    ],

    [
        'group' => 'validation',
        'key' => 'ip',
        'text' => [
            'ru' => 'Поле :attribute должно быть действительным IP-адресом.',
            'en' => 'Field :attribute must be a valid IP address.',
            'cn' => '场 :attribute 必须是有效的IP地址。',
        ]
    ],

    [
        'group' => 'validation',
        'key' => 'ipv4',
        'text' => [
            'ru' => 'Поле :attribute должно быть действительным IPv4-адресом.',
            'en' => 'Field :attribute must be a valid IPv4 address.',
            'cn' => '场 :attribute 必须是有效的IPv4地址。',
        ]
    ],

    [
        'group' => 'validation',
        'key' => 'ipv6',
        'text' => [
            'ru' => 'Поле :attribute должно быть действительным IPv6-адресом.',
            'en' => 'Field :attribute must be a valid IPv6 address.',
            'cn' => '场 :attribute 必须是有效的IPv6地址。',
        ]
    ],

    [
        'group' => 'validation',
        'key' => 'json',
        'text' => [
            'ru' => 'Поле :attribute должно быть JSON строкой.',
            'en' => 'Field :attribute must be a JSON string.',
            'cn' => '场 :attribute 必须JSON串。',
        ]
    ],

    //lt
        [
            'group' => 'validation',
            'key' => 'lt.numeric',
            'text' => [
                'ru' => 'Поле :attribute должно быть меньше :value.',
                'en' => 'Field :attribute must be less than :value.',
                'cn' => '场 :attribute 必须小于 :value.',
            ]
        ],

        [
            'group' => 'validation',
            'key' => 'lt.file',
            'text' => [
                'ru' => 'Размер файла в поле :attribute должен быть меньше :value Килобайт(а).',
                'en' => 'The size of the file in the :attribute must be less than :value Kilobyte(s).',
                'cn' => '该文件的大小的 :attribute 必须小于 :value 千字节(s)。',
            ]
        ],

        [
            'group' => 'validation',
            'key' => 'lt.string',
            'text' => [
                'ru' => 'Количество символов в поле :attribute должно быть меньше :value.',
                'en' => 'The number of characters in field :attribute must be less than :value.',
                'cn' => '该数字在场 :attribute 必须小于 :value.',
            ]
        ],

        [
            'group' => 'validation',
            'key' => 'lt.array',
            'text' => [
                'ru' => 'Количество элементов в поле :attribute должно быть меньше :value.',
                'en' => 'The number of elements in the field :attribute must be less than :value.',
                'cn' => '元件的数目在该领域 :attribute 必须小于 :value.',
            ]
        ],
    
    //lte
        [
            'group' => 'validation',
            'key' => 'lte.numeric',
            'text' => [
                'ru' => 'Поле :attribute должно быть меньше или равно :value.',
                'en' => 'Field :attribute must be less than or equal :value.',
                'cn' => '场 :attribute 必须小于或等于 :value.',
            ]
        ],

        [
            'group' => 'validation',
            'key' => 'lte.file',
            'text' => [
                'ru' => 'Размер файла в поле :attribute должен быть меньше или равен :value Килобайт(а).',
                'en' => 'The size of the file in the :attribute must be less than or equal to :value Kilobyte(s).',
                'cn' => '该文件的大小的 :attribute 必须小于或等于 :value 千字节(s)。',
            ]
        ],

        [
            'group' => 'validation',
            'key' => 'lte.string',
            'text' => [
                'ru' => 'Количество символов в поле :attribute должно быть меньше или равно :value.',
                'en' => 'The number of characters in field :attribute must be less than or equal :value.',
                'cn' => '该数字在场 :attribute 必须小于或等于 :value.',
            ]
        ],

        [
            'group' => 'validation',
            'key' => 'lte.array',
            'text' => [
                'ru' => 'Количество элементов в поле :attribute должно быть меньше или равно :value.',
                'en' => 'The number of elements in the field :attribute must be less than or equal :value.',
                'cn' => '元件的数目在该领域 :attribute 必须小于或等于 :value.',
            ]
        ],
    
    //max
        [
            'group' => 'validation',
            'key' => 'max.numeric',
            'text' => [
                'ru' => 'Поле :attribute не может быть более :max.',
                'en' => 'Field :attribute may not have more than :max.',
                'cn' => '场 :attribute 可能没有超过 :max.',
            ]
        ],

        [
            'group' => 'validation',
            'key' => 'max.file',
            'text' => [
                'ru' => 'Размер файла в поле :attribute не может быть более :max Килобайт(а).',
                'en' => 'The size of the file in the :attribute may not have more than :max Kilobyte(s).',
                'cn' => '该文件的大小的 :attribute 可能没有超过 :max 千字节(s)。',
            ]
        ],

        [
            'group' => 'validation',
            'key' => 'max.string',
            'text' => [
                'ru' => 'Количество символов в поле :attribute не может превышать :max.',
                'en' => 'The number of characters in field :attribute may not exceed :max.',
                'cn' => '该数字在场 :attribute 可能不超过 :max.',
            ]
        ],

        [
            'group' => 'validation',
            'key' => 'max.array',
            'text' => [
                'ru' => 'Количество элементов в поле :attribute не может превышать :max.',
                'en' => 'The number of elements in the field :attribute may not exceed :max.',
                'cn' => '元件的数目在该领域 :attribute 可能不超过 :max.',
            ]
        ],

    [
        'group' => 'validation',
        'key' => 'mimes',
        'text' => [
            'ru' => 'Поле :attribute должно быть файлом одного из следующих типов: :values.',
            'en' => 'Field :attribute must be a file one of the following types: :values.',
            'cn' => '场 :attribute 必须的文件以下类型之一: :values.',
        ]
    ],

    [
        'group' => 'validation',
        'key' => 'mimetypes',
        'text' => [
            'ru' => 'Поле :attribute должно быть файлом одного из следующих типов: :values.',
            'en' => 'Field :attribute must be a file one of the following types: :values.',
            'cn' => '场 :attribute 必须的文件以下类型之一: :values.',
        ]
    ],

    //min
        [
            'group' => 'validation',
            'key' => 'min.numeric',
            'text' => [
                'ru' => 'Поле :attribute должно быть не менее :min.',
                'en' => 'Field :attribute must be at least :min.',
                'cn' => '场 :attribute 必须至少 :min.',
            ]
        ],

        [
            'group' => 'validation',
            'key' => 'min.file',
            'text' => [
                'ru' => 'Размер файла в поле :attribute должен быть не менее :min Килобайт(а).',
                'en' => 'The size of the file in the :attribute must be at least :min Kilobyte(s).',
                'cn' => '该文件的大小的 :attribute 必须至少 :min 千字节(s).',
            ]
        ],

        [
            'group' => 'validation',
            'key' => 'min.string',
            'text' => [
                'ru' => 'Количество символов в поле :attribute должно быть не менее :min.',
                'en' => 'The number of characters in field :attribute must be at least :min.',
                'cn' => '该数字在场 :attribute 必须至少 :min.',
            ]
        ],

        [
            'group' => 'validation',
            'key' => 'min.array',
            'text' => [
                'ru' => 'Количество элементов в поле :attribute должно быть не менее :min.',
                'en' => 'The number of elements in the field :attribute must be at least :min.',
                'cn' => '元件的数目在该领域 :attribute 必须至少 :min.',
            ]
        ],

    [
        'group' => 'validation',
        'key' => 'not_in',
        'text' => [
            'ru' => 'Выбранное значение для :attribute ошибочно.',
            'en' => 'The selected :attribute is wrong.',
            'cn' => '所选择的 :attribute 是错误的。',
        ]
    ],
    
    [
        'group' => 'validation',
        'key' => 'not_regex',
        'text' => [
            'ru' => 'Выбранный формат для :attribute ошибочный.',
            'en' => 'The format for the selected :attribute is wrong.',
            'cn' => '该格式的选择 :attribute 是错误的。',
        ]
    ],

    [
        'group' => 'validation',
        'key' => 'numeric',
        'text' => [
            'ru' => 'Поле :attribute должно быть числом.',
            'en' => 'Field :attribute must be a number.',
            'cn' => '场 :attribute 必须是一个数字。',
        ]
    ],

    [
        'group' => 'validation',
        'key' => 'present',
        'text' => [
            'ru' => 'Поле :attribute должно присутствовать.',
            'en' => 'Field :attribute must be present.',
            'cn' => '场 :attribute 必须存在。',
        ]
    ],

    [
        'group' => 'validation',
        'key' => 'regex',
        'text' => [
            'ru' => 'Поле :attribute имеет ошибочный формат.',
            'en' => 'Field :attribute has incorrect format.',
            'cn' => '场 :attribute 有不正确的格式。',
        ]
    ],

    [
        'group' => 'validation',
        'key' => 'required',
        'text' => [
            'ru' => 'Поле :attribute обязательно для заполнения.',
            'en' => 'Field :attribute is required.',
            'cn' => '场 :attribute 需要.',
        ]
    ],

    [
        'group' => 'validation',
        'key' => 'required_if',
        'text' => [
            'ru' => 'Поле :attribute обязательно для заполнения, когда :other равно :value.',
            'en' => 'Field :attribute is required when :other is :value.',
            'cn' => '场 :attribute 需要的时候 :other 是 :value.',
        ]
    ],

    [
        'group' => 'validation',
        'key' => 'required_unless',
        'text' => [
            'ru' => 'Поле :attribute обязательно для заполнения, когда :other не равно :values.',
            'en' => 'Field :attribute is required when :other is :values.',
            'cn' => '场 :attribute 需要的时候 :other 是 :values.',
        ]
    ],

    [
        'group' => 'validation',
        'key' => 'required_with',
        'text' => [
            'ru' => 'Поле :attribute обязательно для заполнения, когда :values указано.',
            'en' => 'Field :attribute is required when :values is specified.',
            'cn' => '场 :attribute 需要的时候 :values 指定.',
        ]
    ],

    [
        'group' => 'validation',
        'key' => 'required_with_all',
        'text' => [
            'ru' => 'Поле :attribute обязательно для заполнения, когда :values указано.',
            'en' => 'Field :attribute is required when :values is specified.',
            'cn' => '场 :attribute 需要的时候 :values 是有规定。',
        ]
    ],

    [
        'group' => 'validation',
        'key' => 'required_without',
        'text' => [
            'ru' => 'Поле :attribute обязательно для заполнения, когда :values не указано.',
            'en' => 'Field :attribute is required when :values is not specified.',
            'cn' => '场 :attribute 需要的时候 :values 没有规定。',
        ]
    ],

    [
        'group' => 'validation',
        'key' => 'required_without_all',
        'text' => [
            'ru' => 'Поле :attribute обязательно для заполнения, когда ни одно из :values не указано.',
            'en' => 'Field :attribute is required when none of :values are not specified.',
            'cn' => '场 :attribute 需要的时候没有 :values 不是指定的。',
        ]
    ],

    [
        'group' => 'validation',
        'key' => 'same',
        'text' => [
            'ru' => 'Значения полей :attribute и :other должны совпадать.',
            'en' => 'Field values :attribute and :other must match.',
            'cn' => '场 值 :attribute 和 :other 必须 匹配.',
        ]
    ],

    //size
        [
            'group' => 'validation',
            'key' => 'size.numeric',
            'text' => [
                'ru' => 'Поле :attribute должно быть равным :size.',
                'en' => 'Field :attribute must be equal :size.',
                'cn' => '场 :attribute 必须是平等的 :size.',
            ]
        ],

        [
            'group' => 'validation',
            'key' => 'size.file',
            'text' => [
                'ru' => 'Размер файла в поле :attribute должен быть равен :size Килобайт(а).',
                'en' => 'The size of the file in the :attribute must be equal :size Kilobyte(s).',
                'cn' => '该文件的大小的 :attribute 必须是平等的 :size 千字节(s).',
            ]
        ],

        [
            'group' => 'validation',
            'key' => 'size.string',
            'text' => [
                'ru' => 'Количество символов в поле :attribute должно быть равным :size.',
                'en' => 'The number of characters in field :attribute must be equal :size.',
                'cn' => '该数字在场 :attribute 必须是平等的 :size.',
            ]
        ],

        [
            'group' => 'validation',
            'key' => 'size.array',
            'text' => [
                'ru' => 'Количество элементов в поле :attribute должно быть равным :size.',
                'en' => 'The number of elements in the field :attribute must be equal :size.',
                'cn' => '元件的数目在该领域 :attribute 必须是平等的 :size.',
            ]
        ],

    [
        'group' => 'validation',
        'key' => 'starts_with',
        'text' => [
            'ru' => 'Поле :attribute должно начинаться из одного из следующих значений: :values',
            'en' => 'Field :attribute must start from one of the following values: :values',
            'cn' => '场 :attribute 必须从以下一个 值: :values',
        ]
    ],

    [
        'group' => 'validation',
        'key' => 'string',
        'text' => [
            'ru' => 'Поле :attribute должно быть строкой.',
            'en' => 'Field :attribute must be a string.',
            'cn' => '场 :attribute 必须一字符串.',
        ]
    ],

    [
        'group' => 'validation',
        'key' => 'timezone',
        'text' => [
            'ru' => 'Поле :attribute должно быть действительным часовым поясом.',
            'en' => 'Field :attribute must be a valid time zone.',
            'cn' => '场 :attribute 必须有效时区',
        ]
    ],

    [
        'group' => 'validation',
        'key' => 'unique',
        'text' => [
            'ru' => 'Такое значение поля :attribute уже существует.',
            'en' => 'This field value :attribute already exists.',
            'cn' => '此 场 值 :attribute 已经 存在.',
        ]
    ],

    [
        'group' => 'validation',
        'key' => 'uploaded',
        'text' => [
            'ru' => 'Загрузка поля :attribute не удалась.',
            'en' => 'Download fields :attribute failed.',
            'cn' => '下载的领域 :attribute 失败的.',
        ]
    ],

    [
        'group' => 'validation',
        'key' => 'url',
        'text' => [
            'ru' => 'Поле :attribute имеет ошибочный формат.',
            'en' => 'Field :attribute has incorrect format.',
            'cn' => '场 :attribute 有不正确的格式.',
        ]
    ],

    [
        'group' => 'validation',
        'key' => 'uuid',
        'text' => [
            'ru' => 'Поле :attribute должно быть корректным UUID.',
            'en' => 'Field :attribute must be a valid UUID.',
            'cn' => '场 :attribute 必须是有效的 UUID.',
        ]
    ],

    [
        'group' => 'validation',
        'key' => 'password',
        'text' => [
            'ru' => 'Пользователь с таким паролем не найден',
            'en' => 'A user with this password is not found',
            'cn' => '用户用这个密码是找不到',
        ]
    ],

    //attributes
    [
        'group' => 'validation',
        'key' => 'attributes.login',
        'text' => [
            'ru' => 'логин',
            'en' => 'login',
            'cn' => '登录',
        ]
    ],

    [
        'group' => 'validation',
        'key' => 'attributes.documents',
        'text' => [
            'ru' => 'документы',
            'en' => 'documents',
            'cn' => '文件',
        ]
    ],

    [
        'group' => 'validation',
        'key' => 'attributes.name',
        'text' => [
            'ru' => 'имя',
            'en' => 'name',
            'cn' => '名称',
        ]
    ],

    [
        'group' => 'validation',
        'key' => 'attributes.username',
        'text' => [
            'ru' => 'никнейм',
            'en' => 'username',
            'cn' => '用户名',
        ]
    ],

    [
        'group' => 'validation',
        'key' => 'attributes.email',
        'text' => [
            'ru' => 'e-mail адрес',
            'en' => 'e-mail address',
            'cn' => 'e-mail地址',
        ]
    ],

    [
        'group' => 'validation',
        'key' => 'attributes.first_name',
        'text' => [
            'ru' => 'имя',
            'en' => 'first name',
            'cn' => '第一名',
        ]
    ],

    [
        'group' => 'validation',
        'key' => 'attributes.last_name',
        'text' => [
            'ru' => 'фамилия',
            'en' => 'last name',
            'cn' => '最后一名',
        ]
    ],

    [
        'group' => 'validation',
        'key' => 'attributes.patronymic',
        'text' => [
            'ru' => 'отчество',
            'en' => 'patronymic',
            'cn' => '父',
        ]
    ],

    [
        'group' => 'validation',
        'key' => 'attributes.company_name',
        'text' => [
            'ru' => 'название компании',
            'en' => 'company name',
            'cn' => '公司名称',
        ]
    ],

    [
        'group' => 'validation',
        'key' => 'attributes.company_adress_judicial',
        'text' => [
            'ru' => 'юридический адрес',
            'en' => 'legal address',
            'cn' => '法定地址',
        ]
    ],

    [
        'group' => 'validation',
        'key' => 'attributes.company_adress_fact',
        'text' => [
            'ru' => 'фактический адрес',
            'en' => 'the actual address',
            'cn' => '实际地址',
        ]
    ],

    [
        'group' => 'validation',
        'key' => 'attributes.image',
        'text' => [
            'ru' => 'фото',
            'en' => 'photo',
            'cn' => '照片',
        ]
    ],

    [
        'group' => 'validation',
        'key' => 'attributes.inn',
        'text' => [
            'ru' => 'инн',
            'en' => 'inn',
            'cn' => '旅馆',
        ]
    ],

    [
        'group' => 'validation',
        'key' => 'attributes.password',
        'text' => [
            'ru' => 'пароль',
            'en' => 'password',
            'cn' => '密码',
        ]
    ],

    [
        'group' => 'validation',
        'key' => 'attributes.password_confirmation',
        'text' => [
            'ru' => 'подтверждение пароля',
            'en' => 'confirm password',
            'cn' => '确认密码',
        ]
    ],

    [
        'group' => 'validation',
        'key' => 'attributes.city',
        'text' => [
            'ru' => 'город',
            'en' => 'city',
            'cn' => '城市',
        ]
    ],

    [
        'group' => 'validation',
        'key' => 'attributes.country',
        'text' => [
            'ru' => 'страна',
            'en' => 'country',
            'cn' => '国',
        ]
    ],

    [
        'group' => 'validation',
        'key' => 'attributes.address',
        'text' => [
            'ru' => 'адрес',
            'en' => 'address',
            'cn' => '地址',
        ]
    ],

    [
        'group' => 'validation',
        'key' => 'attributes.phone',
        'text' => [
            'ru' => 'телефон',
            'en' => 'phone',
            'cn' => '电话',
        ]
    ],

    [
        'group' => 'validation',
        'key' => 'attributes.number_phone',
        'text' => [
            'ru' => 'телефон',
            'en' => 'phone',
            'cn' => '电话',
        ]
    ],

    [
        'group' => 'validation',
        'key' => 'attributes.number_phone_mobile',
        'text' => [
            'ru' => 'телефон',
            'en' => 'phone',
            'cn' => '电话',
        ]
    ],

    [
        'group' => 'validation',
        'key' => 'attributes.region',
        'text' => [
            'ru' => 'область / штат / республика / край',
            'en' => 'state',
            'cn' => '状态',
        ]
    ],

    [
        'group' => 'validation',
        'key' => 'attributes.localisation',
        'text' => [
            'ru' => 'язык',
            'en' => 'localisation',
            'cn' => '本地化',
        ]
    ],

    [
        'group' => 'validation',
        'key' => 'attributes.index',
        'text' => [
            'ru' => 'индекс',
            'en' => 'index',
            'cn' => '索引',
        ]
    ],

    [
        'group' => 'validation',
        'key' => 'attributes.company_main_banner_images.*',
        'text' => [
            'ru' => 'главный баннер магазина(обложка)',
            'en' => 'main store banner',
            'cn' => '主储存的旗帜',
        ]
    ],

    [
        'group' => 'validation',
        'key' => 'attributes.mobile',
        'text' => [
            'ru' => 'моб. номер',
            'en' => 'mobile number',
            'cn' => '手机号码',
        ]
    ],

    [
        'group' => 'validation',
        'key' => 'attributes.age',
        'text' => [
            'ru' => 'возраст',
            'en' => 'age',
            'cn' => '年龄',
        ]
    ],

    [
        'group' => 'validation',
        'key' => 'attributes.gender',
        'text' => [
            'ru' => 'пол',
            'en' => 'gender',
            'cn' => '性别问题',
        ]
    ],

    [
        'group' => 'validation',
        'key' => 'attributes.birthday',
        'text' => [
            'ru' => 'день рождения',
            'en' => 'brithday',
            'cn' => '生日',
        ]
    ],

    [
        'group' => 'validation',
        'key' => 'attributes.month',
        'text' => [
            'ru' => 'месяц',
            'en' => 'month',
            'cn' => '一个月',
        ]
    ],

    [
        'group' => 'validation',
        'key' => 'attributes.year',
        'text' => [
            'ru' => 'год',
            'en' => 'year',
            'cn' => '年',
        ]
    ],

    [
        'group' => 'validation',
        'key' => 'attributes.hour',
        'text' => [
            'ru' => 'час',
            'en' => 'hour',
            'cn' => '小时',
        ]
    ],

    [
        'group' => 'validation',
        'key' => 'attributes.minute',
        'text' => [
            'ru' => 'минута',
            'en' => 'minute',
            'cn' => '钟',
        ]
    ],

    [
        'group' => 'validation',
        'key' => 'attributes.second',
        'text' => [
            'ru' => 'секунда',
            'en' => 'second',
            'cn' => '第二',
        ]
    ],

    [
        'group' => 'validation',
        'key' => 'attributes.title',
        'text' => [
            'ru' => 'наименование',
            'en' => 'title',
            'cn' => '标题',
        ]
    ],

    [
        'group' => 'validation',
        'key' => 'attributes.content',
        'text' => [
            'ru' => 'контент',
            'en' => 'content',
            'cn' => '的内容',
        ]
    ],

    [
        'group' => 'validation',
        'key' => 'attributes.description',
        'text' => [
            'ru' => 'описание',
            'en' => 'description',
            'cn' => '描述',
        ]
    ],

    [
        'group' => 'validation',
        'key' => 'attributes.company_youtube_link',
        'text' => [
            'ru' => 'ccылка на ролик в YouTube',
            'en' => 'link to the YouTube video',
            'cn' => '链接到 YouTube 的剪辑',
        ]
    ],

    [
        'group' => 'validation',
        'key' => 'attributes.full_description',
        'text' => [
            'ru' => 'полное описание',
            'en' => 'full description',
            'cn' => '描述',
        ]
    ],

    [
        'group' => 'validation',
        'key' => 'attributes.excerpt',
        'text' => [
            'ru' => 'выдержка',
            'en' => 'excerpt',
            'cn' => '摘录',
        ]
    ],

    [
        'group' => 'validation',
        'key' => 'attributes.date',
        'text' => [
            'ru' => 'дата',
            'en' => 'date',
            'cn' => '日期',
        ]
    ],

    [
        'group' => 'validation',
        'key' => 'attributes.time',
        'text' => [
            'ru' => 'время',
            'en' => 'time',
            'cn' => '时间',
        ]
    ],

    [
        'group' => 'validation',
        'key' => 'attributes.available',
        'text' => [
            'ru' => 'доступно',
            'en' => 'available',
            'cn' => '可',
        ]
    ],

    [
        'group' => 'validation',
        'key' => 'attributes.size',
        'text' => [
            'ru' => 'размер',
            'en' => 'size',
            'cn' => '尺寸',
        ]
    ],

    [
        'group' => 'validation',
        'key' => 'attributes.articul',
        'text' => [
            'ru' => 'артикул',
            'en' => 'articul',
            'cn' => 'articul',
        ]
    ],

    [
        'group' => 'validation',
        'key' => 'attributes.product_name',
        'text' => [
            'ru' => 'название товара',
            'en' => 'product name',
            'cn' => '商品名称',
        ]
    ],

    [
        'group' => 'validation',
        'key' => 'attributes.min_price',
        'text' => [
            'ru' => 'минимальная цена',
            'en' => 'minimal price',
            'cn' => '最小的代价',
        ]
    ],

    [
        'group' => 'validation',
        'key' => 'attributes.availible_cnt',
        'text' => [
            'ru' => 'доступное количество',
            'en' => 'available quantity',
            'cn' => '可用数量',
        ]
    ],

    [
        'group' => 'validation',
        'key' => 'attributes.color',
        'text' => [
            'ru' => 'цвет',
            'en' => 'color',
            'cn' => '颜色',
        ]
    ],

    [
        'group' => 'validation',
        'key' => 'attributes.preview_image',
        'text' => [
            'ru' => 'изображение для списка карточек товаров',
            'en' => 'image for list of products',
            'cn' => '图像的产品清单',
        ]
    ],

    [
        'group' => 'validation',
        'key' => 'attributes.company_logo_image',
        'text' => [
            'ru' => 'логотип компании',
            'en' => 'company logo',
            'cn' => '公司的标志',
        ]
    ],

    [
        'group' => 'validation',
        'key' => 'attributes.detail_images',
        'text' => [
            'ru' => 'изображения для описания запроса',
            'en' => 'image for query description',
            'cn' => '像对查询的说明',
        ]
    ],

    [
        'group' => 'validation',
        'key' => 'attributes.available.value',
        'text' => [
            'ru' => 'доступное количество',
            'en' => 'available value',
            'cn' => '可用价值',
        ]
    ],

    [
        'group' => 'validation',
        'key' => 'attributes.weight.value',
        'text' => [
            'ru' => 'вес',
            'en' => 'weight',
            'cn' => '重量',
        ]
    ],

    [
        'group' => 'validation',
        'key' => 'attributes.lenght.value',
        'text' => [
            'ru' => 'длина',
            'en' => 'lenght',
            'cn' => '长',
        ]
    ],

    [
        'group' => 'validation',
        'key' => 'attributes.height.value',
        'text' => [
            'ru' => 'высота',
            'en' => 'height',
            'cn' => '高度',
        ]
    ],

    [
        'group' => 'validation',
        'key' => 'attributes.width.value',
        'text' => [
            'ru' => 'ширина',
            'en' => 'width',
            'cn' => '宽',
        ]
    ],

    [
        'group' => 'validation',
        'key' => 'attributes.min_order.value',
        'text' => [
            'ru' => 'минимальное количество',
            'en' => 'minimal order',
            'cn' => '最小的了',
        ]
    ],

    [
        'group' => 'validation',
        'key' => 'attributes.max_price',
        'text' => [
            'ru' => 'максимальная цена',
            'en' => 'max price',
            'cn' => '最大的价格',
        ]
    ],

    ];
?>