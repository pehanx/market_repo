<?php
$notificationData = [

    [
        'group' => 'notification',
        'key' => 'approve_profile',
        'text' => [
            'ru' => 'Подтвердите свой профиль',
            'en' => 'Confirm your profile',
            'cn' => '确认你的档案',
        ]
    ],

    [
        'group' => 'notification',
        'key' => 'need_approve_notify',
        'text' => [
            'ru' => 'В данный момент ваш профиль не активен. Чтобы его активировать свяжитесь с администрацией сайта',
            'en' => 'At the moment your profile is not active. To activate it, contact the website',
            'cn' => '在那一刻你的档案不是活动的。 来激活它联系的网站',
        ]
    ],

    [
        'group' => 'notification',
        'key' => 'need_approve_before_edit_notify',
        'text' => [
            'ru' => 'Ваш профиль успешно обновлён. Для тогоб чтобы его активировать свяжитесь с администрацией сайта',
            'en' => 'Your profile has been successfully updated. For togob to activate it, contact the website',
            'cn' => '你的档案已成功地更新。 为togob以激活联系网站',
        ]
    ],

    [
        'group' => 'notification',
        'key' => 'fill_account',
        'text' => [
            'ru' => 'Заполните информацию о себе',
            'en' => 'Complete the information about yourself',
            'cn' => '完整的信息，关于你自己',
        ]
    ],

    [
        'group' => 'notification',
        'key' => 'fill_email_in_account_notify',
        'text' => [
            'ru' => 'Добавьте свой email, чтобы мы могли с вами связаться',
            'en' => 'Add your email so we can contact you',
            'cn' => '加入你的电子邮件所以我们可以联系你',
        ]
    ],

    [
        'group' => 'notification',
        'key' => 'approving_profile',
        'text' => [
            'ru' => 'Подтверждение профиля',
            'en' => 'Confirmation profile',
            'cn' => '确认轮廓',
        ]
    ],

    [
        'group' => 'notification',
        'key' => 'approving_profile_notify',
        'text' => [
            'ru' => 'Ваш профиль будет подтвержден в ближайшее время',
            'en' => 'Your profile will be confirmed in the near future',
            'cn' => '你的个人资料会证实在不久的将来',
        ]
    ],

    [
        'group' => 'notification',
        'key' => 'change_entrance_data',
        'text' => [
            'ru' => 'Изменение параметров учетной записи',
            'en' => 'Change account settings',
            'cn' => '改变的帐户设置',
        ]
    ],

    [
        'group' => 'notification',
        'key' => 'change_private_data',
        'text' => [
            'ru' => 'Изменение личных данных пользователя',
            'en' => 'Change of personal data',
            'cn' => '改变个人的数据',
        ]
    ],

    [
        'group' => 'notification',
        'key' => 'success_update_entrance_data_notify',
        'text' => [
            'ru' => 'Данные Вашей учетной записи успешно изменены',
            'en' => 'Your account information is successfully changed',
            'cn' => '您的账户信息是成功改变了',
        ]
    ],

    [
        'group' => 'notification',
        'key' => 'dont_update_entrance_data_notify',
        'text' => [
            'ru' => 'При изменение данных Вашей учетной записи произошла ошибка! Свяжитесь с администрацией сайта',
            'en' => 'When you change Your account credentials error has occurred! Contact the website',
            'cn' => '当你改变你的账户的凭据，错误已经发生了! 联系人网站',
        ]
    ],

    [
        'group' => 'notification',
        'key' => 'success_update_account_data_notify',
        'text' => [
            'ru' => 'Данные успешно изменены',
            'en' => 'Data successfully changed',
            'cn' => '数据成功地改变了',
        ]
    ],

    [
        'group' => 'notification',
        'key' => 'save_changes_success',
        'text' => [
            'ru' => 'Изменения успешно сохранены',
            'en' => 'The changes are saved',
            'cn' => '本改变保存',
        ]
    ],

    [
        'group' => 'notification',
        'key' => 'active_profile',
        'text' => [
            'ru' => 'Активируйте профиль',
            'en' => 'Activate profile',
            'cn' => '配置激活',
        ]
    ],

    [
        'group' => 'notification',
        'key' => 'need_active_profile',
        'text' => [
            'ru' => 'Для продолжения нужен активный профиль',
            'en' => 'To continue activate profile',
            'cn' => '继续激活廓',
        ]
    ],

    [
        'group' => 'notification',
        'key' => 'write_supplier',
        'text' => [
            'ru' => 'Написать поставщику',
            'en' => 'Write supplier',
            'cn' => '写供应商',
        ]
    ],

    [
        'group' => 'notification',
        'key' => 'add_contact',
        'text' => [
            'ru' => 'Добавить в контакты',
            'en' => 'Add to contact',
            'cn' => '添加到触',
        ]
    ],

    [
        'group' => 'notification',
        'key' => 'contact_already_added',
        'text' => [
            'ru' => 'Контакт уже добавлен',
            'en' => 'Contact already added',
            'cn' => '联系已经加入',
        ]
    ],
    
    [
        'group' => 'notification',
        'key' => 'attention',
        'text' => [
            'ru' => 'Внимание!',
            'en' => 'Attention!',
            'cn' => '注意力!',
        ]
    ],
    
    [
        'group' => 'notification',
        'key' => 'create_profile_buyer_seller',
        'text' => [
            'ru' => 'Создайте профиль покупателя или продавца, чтобы воспользоваться полным функционалом нашего сайта.',
            'en' => 'Create a buyer or seller profile to take advantage of the full functionality of our site.',
            'cn' => '创建一个买方或卖方的配置，以利用的全部功能，我们的网站。',
        ]
    ],
    
    [
        'group' => 'notification',
        'key' => 'activate_created_profile',
        'text' => [
            'ru' => 'Чтобы воспользоваться полным функционалом сайта, Вам необходимо активировать один из созданных профилей.',
            'en' => 'To use the full functionality of the site, you need to activate one of the created profiles.',
            'cn' => '使用的全部功能的网站，就需要启动一个创建档案。',
        ]
    ],
    
    [
        'group' => 'notification',
        'key' => 'select_active_profile',
        'text' => [
            'ru' => 'Выберите профиль в ЛК, от которого вы хотите написать сообщение.',
            'en' => 'Select the profile in the private cabinet from which you want to write a message.',
            'cn' => '选择在私人阁你想写一个消息。',
        ]
    ],

];
?>