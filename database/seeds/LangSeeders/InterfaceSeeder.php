<?php
$interfaceData = [

    //Статика
    [
        'group' => 'interface',
        'key' => 'reset_password',
        'text' => [
            'ru' => 'Сбросить пароль',
            'en' => 'Reset your password',
            'cn' => '你的密码重置',
        ]
    ],

    //Ошибки
    [
        'group' => 'interface',
        'key' => 'errors.not_found',
        'text' => [
            'ru' => 'Страница не найдена',
            'en' => 'Page not found',
            'cn' => '網頁未找到',
        ]
    ],

    //Главная
    [
        'group' => 'interface',
        'key' => 'main.popular_categories',
        'text' => [
            'ru' => 'Популярные категории',
            'en' => 'Popular categories',
            'cn' => '流行的类别',
        ]
    ],

    [
        'group' => 'interface',
        'key' => 'main.how_it_works',
        'text' => [
            'ru' => 'Как работает Маркет?',
            'en' => 'How market work?',
            'cn' => '如何市场的工作？',
        ]
    ],

    [
        'group' => 'interface',
        'key' => 'main.what_is_market',
        'text' => [
            'ru' => 'Что такое G2R.MARKET',
            'en' => 'What is G2R.MARKET',
            'cn' => '什么是G2R.MARKET',
        ]
    ],

    [
        'group' => 'interface',
        'key' => 'main.what_is_market_description',
        'text' => [
            'ru' => 'Это онлайн-платформа, которая помогает продавать товары на зарубежных торговых площадках',
            'en' => 'This is an online platform that helps sell products on overseas trading floors',
            'cn' => '这是一个在线平台，可帮助在海外交易大厅销售产品。',
        ]
    ],

    [
        'group' => 'interface',
        'key' => 'main.what_is_market_integration',
        'text' => [
            'ru' => 'Наш сайт интегрирован с крупнейшими международными B2B платформами — поэтому товары, которые вы размещаете у нас, автоматически размещаются и у них',
            'en' => 'Our site is integrated with the largest international B2B platforms - therefore, the products that you place with us are automatically placed with them',
            'cn' => '我们的网站已与最大的国际B2B平台集成-因此，您放置在我们这里的产品会自动随它们一起放置。',
        ]
    ],

    [
        'group' => 'interface',
        'key' => 'main.register',
        'text' => [
            'ru' => 'Регистрируйтесь',
            'en' => 'Register',
            'cn' => '注册',
        ]
    ],

    [
        'group' => 'interface',
        'key' => 'main.you_can',
        'text' => [
            'ru' => 'Вы сможете полноценно работать с сервисом, просматривать список поставщиков, участвовать в тендерах, покупать и продавать товар',
            'en' => 'You will be able to work with the service, view the list of suppliers, participate in tenders to buy and sell goods',
            'cn' => '你将能够工作的服务，查看清单的供应商参与招标购买和出售货物',
        ]
    ],

    [
        'group' => 'interface',
        'key' => 'main.to_registration',
        'text' => [
            'ru' => 'Как работает Маркет?',
            'en' => 'How market work?',
            'cn' => '如何市场的工作？',
        ]
    ],

    [
        'group' => 'interface',
        'key' => 'main.place_product_or_request',
        'text' => [
            'ru' => 'Разместить заказ или товар',
            'en' => 'Place an order or product',
            'cn' => '订购或产品',
        ]
    ],

    [
        'group' => 'interface',
        'key' => 'main.deal_agree',
        'text' => [
            'ru' => 'Договоритесь о сделке через услуги Маркета',
            'en' => 'Arrange a transaction through the services Market',
            'cn' => '安排一场交易通过服务市场',
        ]
    ],

    [
        'group' => 'interface',
        'key' => 'main.wait_product',
        'text' => [
            'ru' => 'Ждите получения или отправляйте товар',
            'en' => 'Wait for the receive or send goods',
            'cn' => '等待收到或发送的货物',
        ]
    ],

    //Кнопки
    [
        'group' => 'interface',
        'key' => 'buttons.add',
        'text' => [
            'ru' => 'Добавить',
            'en' => 'Add',
            'cn' => '添加',
        ]
    ],

    [
        'group' => 'interface',
        'key' => 'buttons.create_characteristic',
        'text' => [
            'ru' => 'Добавить характеристику',
            'en' => 'Add characteristic',
            'cn' => 'Add characteristic',
        ]
    ],
    
    [
        'group' => 'interface',
        'key' => 'buttons.request_product_post',
        'text' => [
            'ru' => 'Разместить запрос',
            'en' => 'Place a request',
            'cn' => '地方一个请求',
        ]
    ],

    [
        'group' => 'interface',
        'key' => 'buttons.login',
        'text' => [
            'ru' => 'Войти',
            'en' => 'Sign in',
            'cn' => '登录在',
        ]
    ],

    [
        'group' => 'interface',
        'key' => 'buttons.logout',
        'text' => [
            'ru' => 'Выйти',
            'en' => 'Logout',
            'cn' => '注销',
        ]
    ],

    [
        'group' => 'interface',
        'key' => 'buttons.notifications',
        'text' => [
            'ru' => 'Уведомления',
            'en' => 'Notifications',
            'cn' => '的通知',
        ]
    ],

    [
        'group' => 'interface',
        'key' => 'buttons.cabinet',
        'text' => [
            'ru' => 'Личный кабинет',
            'en' => 'Cabinet',
            'cn' => '内阁',
        ]
    ],

    [
        'group' => 'interface',
        'key' => 'buttons.contacts',
        'text' => [
            'ru' => 'Контакты',
            'en' => 'Contacts',
            'cn' => '联系人',
        ]
    ],

    [
        'group' => 'interface',
        'key' => 'buttons.register',
        'text' => [
            'ru' => 'Зарегистрироваться',
            'en' => 'Register',
            'cn' => '注册',
        ]
    ],

    [
        'group' => 'interface',
        'key' => 'buttons.localisation.ru',
        'text' => [
            'ru' => 'Русский',
            'en' => 'Russian',
            'cn' => '俄罗斯',
        ]
    ],

    [
        'group' => 'interface',
        'key' => 'buttons.localisation.en',
        'text' => [
            'ru' => 'Английский',
            'en' => 'English',
            'cn' => '英语',
        ]
    ],

    [
        'group' => 'interface',
        'key' => 'buttons.localisation.cn',
        'text' => [
            'ru' => 'Китайский',
            'en' => 'Chinese',
            'cn' => '中国',
        ]
    ],

    [
        'group' => 'interface',
        'key' => 'buttons.currency.rub',
        'text' => [
            'ru' => 'Рубль',
            'en' => 'Ruble',
            'cn' => '卢布',
        ]
    ],

    [
        'group' => 'interface',
        'key' => 'buttons.currency.usd',
        'text' => [
            'ru' => 'Доллар',
            'en' => 'Dollar',
            'cn' => '美元',
        ]
    ],

    [
        'group' => 'interface',
        'key' => 'buttons.currency.cny',
        'text' => [
            'ru' => 'Юань',
            'en' => 'Cny',
            'cn' => '人民币',
        ]
    ],

    [
        'group' => 'interface',
        'key' => 'buttons.add_request_product',
        'text' => [
            'ru' => 'Разместить запрос',
            'en' => 'Place a request',
            'cn' => '地方一个请求',
        ]
    ],

    [
        'group' => 'interface',
        'key' => 'buttons.create_product',
        'text' => [
            'ru' => 'Создать товар',
            'en' => 'Create a product',
            'cn' => '创建一个产品',
        ]
    ],

    [
        'group' => 'interface',
        'key' => 'buttons.save',
        'text' => [
            'ru' => 'Сохранить',
            'en' => 'Save',
            'cn' => '保存',
        ]
    ],

    [
        'group' => 'interface',
        'key' => 'buttons.save_changes',
        'text' => [
            'ru' => 'Сохранить изменения',
            'en' => 'Save changes',
            'cn' => '保存变化',
        ]
    ],

    [
        'group' => 'interface',
        'key' => 'buttons.destroy',
        'text' => [
            'ru' => 'Удалить окончательно',
            'en' => 'Permanently delete',
            'cn' => '删除永久地',
        ]
    ],

    [
        'group' => 'interface',
        'key' => 'buttons.activate',
        'text' => [
            'ru' => 'Активировать',
            'en' => 'Activate',
            'cn' => '激活',
        ]
    ],

    [
        'group' => 'interface',
        'key' => 'buttons.add_more',
        'text' => [
            'ru' => 'Добавить ещё',
            'en' => 'Add more',
            'cn' => '添加更多',
        ]
    ],

    [
        'group' => 'interface',
        'key' => 'buttons.add_manager',
        'text' => [
            'ru' => 'Добавить менеджера',
            'en' => 'Add Manager',
            'cn' => '添加经理',
        ]
    ],

    [
        'group' => 'interface',
        'key' => 'buttons.add_models',
        'text' => [
            'ru' => 'Добавить торговые предложения',
            'en' => 'Add trade offers',
            'cn' => '增加贸易提供',
        ]
    ],
    
    [
        'group' => 'interface',
        'key' => 'buttons.show_completely',
        'text' => [
            'ru' => 'Показать полностью',
            'en' => 'Show completely',
            'cn' => '显示完全',
        ]
    ],

    [
        'group' => 'interface',
        'key' => 'buttons.hide_completely',
        'text' => [
            'ru' => 'Скрыть',
            'en' => 'Hide',
            'cn' => '隐藏',
        ]
    ],

    [
        'group' => 'interface',
        'key' => 'buttons.goto_products',
        'text' => [
            'ru' => 'Перейти к каталогу товаров',
            'en' => "Go to product's catalog",
            'cn' => '去产品的目录',
        ]
    ],
    
    [
        'group' => 'interface',
        'key' => 'buttons.return_index_request_products',
        'text' => [
            'ru' => 'Перейти к каталогу ',
            'en' => "Return to request product's catalog",
            'cn' => '回到请求的产品的目录',
        ]
    ],

    [
        'group' => 'interface',
        'key' => 'buttons.goto_profiles',
        'text' => [
            'ru' => 'Перейти к списку профилей',
            'en' => 'Go to profiles list',
            'cn' => '去配置列表',
        ]
    ],

    [
        'group' => 'interface',
        'key' => 'buttons.favorite_add',
        'text' => [
            'ru' => 'Добавить в избранное',
            'en' => 'Add to favorites',
            'cn' => '添加的最爱',
        ]
    ],
    
    [
        'group' => 'interface',
        'key' => 'buttons.write_to_supplier',
        'text' => [
            'ru' => 'Написать поставщику',
            'en' => 'Write to supplier',
            'cn' => '写信给供应商',
        ]
    ],
    
    [
        'group' => 'interface',
        'key' => 'buttons.write_to_buyer',
        'text' => [
            'ru' => 'Написать покупателю',
            'en' => 'Write to buyer',
            'cn' => '写信给买家',
        ]
    ],

    [
        'group' => 'interface',
        'key' => 'buttons.favorite_remove',
        'text' => [
            'ru' => 'Убрать из избранного',
            'en' => 'Remove from favorites',
            'cn' => '从收藏夹中删除',
        ]
    ],

    [
        'group' => 'interface',
        'key' => 'buttons.save_changes',
        'text' => [
            'ru' => 'Сохранить изменения',
            'en' => 'Save changes',
            'cn' => '保存变化',
        ]
    ],

    [
        'group' => 'interface',
        'key' => 'buttons.edit',
        'text' => [
            'ru' => 'Редактировать',
            'en' => 'Edit',
            'cn' => '编辑',
        ]
    ],

    [
        'group' => 'interface',
        'key' => 'buttons.disable',
        'text' => [
            'ru' => 'Отключить',
            'en' => 'Disable',
            'cn' => '禁用',
        ]
    ],

    [
        'group' => 'interface',
        'key' => 'buttons.approve',
        'text' => [
            'ru' => 'Подтвердить',
            'en' => 'Approve',
            'cn' => '批准',
        ]
    ],

    [
        'group' => 'interface',
        'key' => 'buttons.settings',
        'text' => [
            'ru' => 'Настроить',
            'en' => 'Settings',
            'cn' => '设置',
        ]
    ],

    [
        'group' => 'interface',
        'key' => 'buttons.favorite',
        'text' => [
            'ru' => 'Избранное',
            'en' => 'Favorite',
            'cn' => '最喜欢的',
        ]
    ],
    
    [
        'group' => 'interface',
        'key' => 'buttons.show_active_products',
        'text' => [
            'ru' => 'В наличии',
            'en' => 'In stock',
            'cn' => '在股票',
        ]
    ],

    [
        'group' => 'interface',
        'key' => 'buttons.select',
        'text' => [
            'ru' => 'Выбрать',
            'en' => 'Select',
            'cn' => '选择',
        ]
    ],

    [
        'group' => 'interface',
        'key' => 'buttons.delete',
        'text' => [
            'ru' => 'Удалить',
            'en' => 'Delete',
            'cn' => '删除',
        ]
    ],

    [
        'group' => 'interface',
        'key' => 'buttons.choose_action',
        'text' => [
            'ru' => 'Выберите действие',
            'en' => 'Choose action',
            'cn' => '选择的行动',
        ]
    ],

    [
        'group' => 'interface',
        'key' => 'buttons.create',
        'text' => [
            'ru' => 'Создать',
            'en' => 'Create',
            'cn' => '创建',
        ]
    ],

    [
        'group' => 'interface',
        'key' => 'buttons.export_submit',
        'text' => [
            'ru' => 'Экспортировать',
            'en' => 'Export',
            'cn' => '出口',
        ]
    ],

    [
        'group' => 'interface',
        'key' => 'buttons.import_submit',
        'text' => [
            'ru' => 'Импортировать',
            'en' => 'Import',
            'cn' => '进口',
        ]
    ],

    [
        'group' => 'interface',
        'key' => 'buttons.choose_all',
        'text' => [
            'ru' => 'Выбрать все',
            'en' => 'Choose all',
            'cn' => '所有的选择',
        ]
    ],

    [
        'group' => 'interface',
        'key' => 'buttons.send_documents',
        'text' => [
            'ru' => 'Загрузить новые документы',
            'en' => 'Upload new documents',
            'cn' => '上传的新文件',
        ]
    ],

    [
        'group' => 'interface',
        'key' => 'buttons.disable_choosen',
        'text' => [
            'ru' => 'Отключить выбранные',
            'en' => 'Disable choosen',
            'cn' => '禁止选择',
        ]
    ],

    [
        'group' => 'interface',
        'key' => 'buttons.restore',
        'text' => [
            'ru' => 'Восстановить',
            'en' => 'Restore',
            'cn' => '恢复',
        ]
    ],

    [
        'group' => 'interface',
        'key' => 'buttons.restore_choosen',
        'text' => [
            'ru' => 'Восстановить выбранные',
            'en' => 'Restore choosen',
            'cn' => '恢复选择',
        ]
    ],
    
    [
        'group' => 'interface',
        'key' => 'buttons.choose_files',
        'text' => [
            'ru' => 'Выбрать файлы',
            'en' => 'Choose files',
            'cn' => '选择文件',
        ]
    ],
    
    [
        'group' => 'interface',
        'key' => 'buttons.files_not_chosen',
        'text' => [
            'ru' => 'Файлы не выбраны',
            'en' => 'Files not chosen',
            'cn' => '文件没有选择',
        ]
    ],

    [
        'group' => 'interface',
        'key' => 'buttons.restore_all',
        'text' => [
            'ru' => 'Восстановить все',
            'en' => 'Restore all',
            'cn' => '恢复所有',
        ]
    ],

    [
        'group' => 'interface',
        'key' => 'buttons.destroy_choosen',
        'text' => [
            'ru' => 'Удалить выбранные окончательно',
            'en' => 'Destroy choosen',
            'cn' => '破坏选择',
        ]
    ],
    
    [
        'group' => 'interface',
        'key' => 'buttons.destroy_all',
        'text' => [
            'ru' => 'Удалить все окончательно',
            'en' => 'Destroy all',
            'cn' => '摧毁一切',
        ]
    ],
    
    [
        'group' => 'interface',
        'key' => 'buttons.choose_images',
        'text' => [
            'ru' => 'Выбрать изображения',
            'en' => 'Сhoose images',
            'cn' => '选择像',
        ]
    ],
    
    [
        'group' => 'interface',
        'key' => 'buttons.choose_image',
        'text' => [
            'ru' => 'Выбрать изображение',
            'en' => 'Сhoose image',
            'cn' => '选择像',
        ]
    ],

    [
        'group' => 'interface',
        'key' => 'buttons.activate_choosen',
        'text' => [
            'ru' => 'Активировать выбранные',
            'en' => 'Activate choosen',
            'cn' => '激活选择',
        ]
    ],

    [
        'group' => 'interface',
        'key' => 'buttons.activate_all',
        'text' => [
            'ru' => 'Активировать все',
            'en' => 'Activate all',
            'cn' => '激活所有',
        ]
    ],

    [
        'group' => 'interface',
        'key' => 'buttons.delete_choosen',
        'text' => [
            'ru' => 'Удалить выбранные',
            'en' => 'Delete choosen',
            'cn' => '删除选择',
        ]
    ],

    [
        'group' => 'interface',
        'key' => 'buttons.disable_all',
        'text' => [
            'ru' => 'Отключить все',
            'en' => 'Disable all',
            'cn' => '禁止一切',
        ]
    ],

    [
        'group' => 'interface',
        'key' => 'buttons.delete_all',
        'text' => [
            'ru' => 'Удалить все',
            'en' => 'Delete all',
            'cn' => '删除所有',
        ]
    ],

    [
        'group' => 'interface',
        'key' => 'buttons.download',
        'text' => [
            'ru' => 'Скачать',
            'en' => 'Download',
            'cn' => '下载',
        ]
    ],

    [
        'group' => 'interface',
        'key' => 'buttons.download_all',
        'text' => [
            'ru' => 'Скачать всё',
            'en' => 'Download all',
            'cn' => '下载所有的',
        ]
    ],

    [
        'group' => 'interface',
        'key' => 'buttons.reset_password',
        'text' => [
            'ru' => 'Сбросить пароль',
            'en' => 'Reset password',
            'cn' => '密码重置',
        ]
    ],

    [
        'group' => 'interface',
        'key' => 'buttons.forgot',
        'text' => [
            'ru' => 'Забыли Ваш пароль?',
            'en' => 'Forgot Your password?',
            'cn' => '忘了密码了?',
        ]
    ],

    [
        'group' => 'interface',
        'key' => 'buttons.search',
        'text' => [
            'ru' => 'Найти',
            'en' => 'Find',
            'cn' => '找到',
        ]
    ],

    [
        'group' => 'interface',
        'key' => 'buttons.clear',
        'text' => [
            'ru' => 'Очистить',
            'en' => 'Clear',
            'cn' => '清楚的',
        ]
    ],

    [
        'group' => 'interface',
        'key' => 'buttons.remember',
        'text' => [
            'ru' => 'Запомнить',
            'en' => 'Remember',
            'cn' => '还记得',
        ]
    ],

    [
        'group' => 'interface',
        'key' => 'buttons.send',
        'text' => [
            'ru' => 'Отправить',
            'en' => 'Send',
            'cn' => '发送',
        ]
    ],

    [
        'group' => 'interface',
        'key' => 'buttons.translate_settings',
        'text' => [
            'ru' => 'Настройки перевода',
            'en' => 'Translate settings',
            'cn' => '翻译设置',
        ]
    ],

    [
        'group' => 'interface',
        'key' => 'buttons.translate',
        'text' => [
            'ru' => 'Перевести',
            'en' => 'Translate',
            'cn' => '翻译',
        ]
    ],

    [
        'group' => 'interface',
        'key' => 'buttons.delete_message',
        'text' => [
            'ru' => 'Удалить сообщение',
            'en' => 'Delete message',
            'cn' => '删除邮件',
        ]
    ],

    [
        'group' => 'interface',
        'key' => 'buttons.find_products',
        'text' => [
            'ru' => 'Найти товары',
            'en' => 'Find products',
            'cn' => '找到的产品',
        ]
    ],

    //Фильтры
    [
        'group' => 'interface',
        'key' => 'filters.min_price',
        'text' => [
            'ru' => 'Цена, от',
            'en' => 'Price from',
            'cn' => '价格从',
        ]
    ],

    [
        'group' => 'interface',
        'key' => 'filters.max_price',
        'text' => [
            'ru' => 'Цена, до',
            'en' => 'Price, up to',
            'cn' => '价格，到',
        ]
    ],

    [
        'group' => 'interface',
        'key' => 'filters.from',
        'text' => [
            'ru' => 'от',
            'en' => 'from',
            'cn' => '从',
        ]
    ],

    [
        'group' => 'interface',
        'key' => 'filters.to',
        'text' => [
            'ru' => 'до',
            'en' => 'to',
            'cn' => '要',
        ]
    ],
    [
        'group' => 'interface',
        'key' => 'filters.articul',
        'text' => [
            'ru' => 'Введите артикул товара',
            'en' => 'Enter product articul',
            'cn' => '输入产品articul',
        ]
    ],

    [
        'group' => 'interface',
        'key' => 'filters.name',
        'text' => [
            'ru' => 'Введите название товара',
            'en' => 'Enter the name of the product',
            'cn' => '该产品的名称',
        ]
    ],

    [
        'group' => 'interface',
        'key' => 'filters.category',
        'text' => [
            'ru' => 'Категория',
            'en' => 'Category',
            'cn' => '类别',
        ]
    ],

    [
        'group' => 'interface',
        'key' => 'filters.price',
        'text' => [
            'ru' => 'Цена',
            'en' => 'Price',
            'cn' => '价格',
        ]
    ],

    [
        'group' => 'interface',
        'key' => 'filters.product_color',
        'text' => [
            'ru' => 'Цвет товара',
            'en' => 'Product color',
            'cn' => '产品颜色',
        ]
    ],


    [
        'group' => 'interface',
        'key' => 'filters.request_number',
        'text' => [
            'ru' => 'Введите номер запроса',
            'en' => 'Enter request number',
            'cn' => '该请求数量',
        ]
    ],

    [
        'group' => 'interface',
        'key' => 'filters.search_request_product',
        'text' => [
            'ru' => 'Введите название запроса',
            'en' => 'Enter the name of the request product',
            'cn' => '输入的姓名请求的产品',
        ]
    ],

    [
        'group' => 'interface',
        'key' => 'filters.favourite_name',
        'text' => [
            'ru' => 'Избранное',
            'en' => 'Favourite',
            'cn' => '最喜欢的',
        ]
    ],

    [
        'group' => 'interface',
        'key' => 'filters.hide',
        'text' => [
            'ru' => 'Свернуть фильтр',
            'en' => 'Hide filter',
            'cn' => '隐藏的过滤器',
        ]
    ],

    [
        'group' => 'interface',
        'key' => 'filters.detail',
        'text' => [
            'ru' => 'Детальный фильтр',
            'en' => 'Detailed filter',
            'cn' => '详细的过滤器',
        ]
    ],

    [
        'group' => 'interface',
        'key' => 'filters.search_category',
        'text' => [
            'ru' => 'Поиск по категориям',
            'en' => 'Search by category',
            'cn' => '搜索通过的类别',
        ]
    ],

    [
        'group' => 'interface',
        'key' => 'filters.search_tenders',
        'text' => [
            'ru' => 'Поиск по тендерам',
            'en' => 'Search by tenders',
            'cn' => '搜索标书',
        ]
    ],

    [
        'group' => 'interface',
        'key' => 'filters.search_product',
        'text' => [
            'ru' => 'Введите названиие товара',
            'en' => 'Enter product name',
            'cn' => '输入产品的名称',
        ]
    ],

    //Вкладки
    [
        'group' => 'interface',
        'key' => 'tabs.choose_category',
        'text' => [
            'ru' => 'Выбор категории',
            'en' => 'Choice of category',
            'cn' => '选择类别',
        ]
    ],

    [
        'group' => 'interface',
        'key' => 'tabs.description_product',
        'text' => [
            'ru' => 'Заполнение характеристик товара',
            'en' => 'Filling in product characteristics',
            'cn' => '在填补产品的特性',
        ]
    ],
    
    [
        'group' => 'interface',
        'key' => 'tabs.add_models',
        'text' => [
            'ru' => 'Добавление торговых предложений',
            'en' => 'The addition of the shopping',
            'cn' => '此外购物',
        ]
    ],

    [
        'group' => 'interface',
        'key' => 'tabs.description_request_product',
        'text' => [
            'ru' => 'Описание запроса',
            'en' => 'Request description',
            'cn' => '请说明',
        ]
    ],

    [
        'group' => 'interface',
        'key' => 'tabs.show',
        'text' => [
            'ru' => 'Просмотр созданного товара',
            'en' => 'Show created product',
            'cn' => '显示创造的产品',
        ]
    ],

    [
        'group' => 'interface',
        'key' => 'tabs.active',
        'text' => [
            'ru' => 'Активные',
            'en' => 'Active',
            'cn' => '活性',
        ]
    ],

    [
        'group' => 'interface',
        'key' => 'tabs.on_sale',
        'text' => [
            'ru' => 'В продаже',
            'en' => 'On sale',
            'cn' => '上销售',
        ]
    ],

    [
        'group' => 'interface',
        'key' => 'tabs.inactive',
        'text' => [
            'ru' => 'Неактивные',
            'en' => 'Inactive',
            'cn' => '不活动',
        ]
    ],

    [
        'group' => 'interface',
        'key' => 'tabs.deleted',
        'text' => [
            'ru' => 'Удалённые',
            'en' => 'Deleted',
            'cn' => '已删除',
        ]
    ],

    [
        'group' => 'interface',
        'key' => 'tabs.all',
        'text' => [
            'ru' => 'Все',
            'en' => 'All',
            'cn' => '所有',
        ]
    ],

    [
        'group' => 'interface',
        'key' => 'tabs.request_products',
        'text' => [
            'ru' => 'Запросы на покупку',
            'en' => 'Purchase requests',
            'cn' => '采购申请',
        ]
    ],

    [
        'group' => 'interface',
        'key' => 'tabs.products',
        'text' => [
            'ru' => 'Товары',
            'en' => 'Products',
            'cn' => '产品',
        ]
    ],

    [
        'group' => 'interface',
        'key' => 'tabs.personal_data',
        'text' => [
            'ru' => 'Личные данные',
            'en' => 'Personal data',
            'cn' => '个人数据',
        ]
    ],

    [
        'group' => 'interface',
        'key' => 'tabs.account',
        'text' => [
            'ru' => 'Учётная запись',
            'en' => 'Account',
            'cn' => '帐户',
        ]
    ],

    [
        'group' => 'interface',
        'key' => 'tabs.common_information',
        'text' => [
            'ru' => 'Общая информация',
            'en' => 'Common information',
            'cn' => '公共信息',
        ]
    ],

    [
        'group' => 'interface',
        'key' => 'tabs.managers_profile',
        'text' => [
            'ru' => 'Менеджеры профиля',
            'en' => 'Managers profile',
            'cn' => '管理人员简介',
        ]
    ],

    [
        'group' => 'interface',
        'key' => 'tabs.documents',
        'text' => [
            'ru' => 'Документы',
            'en' => 'Documents',
            'cn' => '文件',
        ]
    ],

    //Messages
    [
        'group' => 'interface',
        'key' => 'messages.entrance',
        'text' => [
            'ru' => 'Войти',
            'en' => 'Login',
            'cn' => '登录',
        ]
    ],

    [
        'group' => 'interface',
        'key' => 'messages.success',
        'text' => [
            'ru' => 'Успешно',
            'en' => 'Success',
            'cn' => 'Success',
        ]
    ],

    [
        'group' => 'interface',
        'key' => 'messages.back_main',
        'text' => [
            'ru' => 'Вернуться на главную',
            'en' => 'Back to main',
            'cn' => '回到主要的',
        ]
    ],

    [
        'group' => 'interface',
        'key' => 'messages.cabinet',
        'text' => [
            'ru' => 'Личный кабинет',
            'en' => 'Cabinet',
            'cn' => '内阁',
        ]
    ],

    [
        'group' => 'interface',
        'key' => 'messages.enter',
        'text' => [
            'ru' => 'Вход',
            'en' => 'Login',
            'cn' => '登录',
        ]
    ],

    [
        'group' => 'interface',
        'key' => 'messages.forgot',
        'text' => [
            'ru' => 'Забыли Ваш пароль?',
            'en' => 'Forgot Your password?',
            'cn' => '忘了密码了?',
        ]
    ],

    [
        'group' => 'interface',
        'key' => 'messages.remember',
        'text' => [
            'ru' => 'Запомнить',
            'en' => 'Remember',
            'cn' => '还记得',
        ]
    ],

    [
        'group' => 'interface',
        'key' => 'messages.login',
        'text' => [
            'ru' => 'Имя',
            'en' => 'Login',
            'cn' => '登录',
        ]
    ],

    [
        'group' => 'interface',
        'key' => 'messages.current_password',
        'text' => [
            'ru' => 'Текущий пароль',
            'en' => 'Current password',
            'cn' => '当前的密码',
        ]
    ],

    [
        'group' => 'interface',
        'key' => 'messages.password',
        'text' => [
            'ru' => 'Пароль',
            'en' => 'Password',
            'cn' => '密码',
        ]
    ],

    [
        'group' => 'interface',
        'key' => 'messages.email',
        'text' => [
            'ru' => 'Еmail',
            'en' => 'Еmail',
            'cn' => 'Еmail',
        ]
    ],

    [
        'group' => 'interface',
        'key' => 'messages.new_password',
        'text' => [
            'ru' => 'Новый пароль',
            'en' => 'New password',
            'cn' => '新密码',
        ]
    ],

    [
        'group' => 'interface',
        'key' => 'messages.confirm_password',
        'text' => [
            'ru' => 'Повторите пароль',
            'en' => 'Repeat password',
            'cn' => '重复密码',
        ]
    ],

    [
        'group' => 'interface',
        'key' => 'messages.registration',
        'text' => [
            'ru' => 'Регистрация',
            'en' => 'Registration',
            'cn' => '注册',
        ]
    ],

    [
        'group' => 'interface',
        'key' => 'messages.registrate',
        'text' => [
            'ru' => 'Зарегестрировать',
            'en' => 'Register',
            'cn' => '注册',
        ]
    ],

    [
        'group' => 'interface',
        'key' => 'messages.register',
        'text' => [
            'ru' => 'Зарегистрироваться',
            'en' => 'To register',
            'cn' => '注册',
        ]
    ],

    [
        'group' => 'interface',
        'key' => 'messages.forgot_password',
        'text' => [
            'ru' => 'Забыли свой пароль?',
            'en' => 'Forgot your password?',
            'cn' => '忘了密码了?',
        ]
    ],

    [
        'group' => 'interface',
        'key' => 'messages.settings',
        'text' => [
            'ru' => 'Настройки',
            'en' => 'Settings',
            'cn' => '设置',
        ]
    ],

    [
        'group' => 'interface',
        'key' => 'messages.create_profile_seller',
        'text' => [
            'ru' => 'Для того, чтобы полноценно работать с сервисом, необходимо создать профиль продавца. Узнайте как правильно заполнить информацию о вашей компании, оформить витрину и сделать её привлекательной для покупателя',
            'en' => 'In order to fully work with the service, you need to create a profile of your company. Find out how to fill in the information about the company correctly, design the window and make it attractive to the buyer',
            'cn' => '为了全面工作的服务，需要创建一个配置你的公司。 找出如何填补在有关公司的信息正确，设计的窗口，并使其具有吸引力的买方',
        ]
    ],
    
    [
        'group' => 'interface',
        'key' => 'messages.create_profile_buyer',
        'text' => [
            'ru' => 'Для того, чтобы начать полноценно работать с сервисом, размещать запросы на покупку и общаться с поставщиками товаров и услуг, Вам необходимо создать и заполнить профиль.',
            'en' => 'In order to fully work with the service, you need to create a profile of your company. Find out how to fill in the information about the company correctly, design the window and make it attractive to the buyer',
            'cn' => '为了全面工作的服务，需要创建一个配置你的公司。 找出如何填补在有关公司的信息正确，设计的窗口，并使其具有吸引力的买方',
        ]
    ],

    [
        'group' => 'interface',
        'key' => 'messages.approve_profile',
        'text' => [
            'ru' => 'Учредительные документы компании',
            'en' => 'Constituent documents of the company',
            'cn' => '组成文件的公司',
        ]
    ],

    [
        'group' => 'interface',
        'key' => 'messages.need_documents',
        'text' => [
            'ru' => 'Добавьте фото или скан Свидетельства о регистрации ИНН вашего юридического лица или ОГРНИП для индивидуальных предпринимателей. Так мы сможем оперативно подтвердить профиль Вашей компании, а Вы - использовать полный функционал сайта',
            'en' => "Add a photo or scan of the Certificate of registration was your legal entity OGRN or for individual entrepreneurs. So we can quickly confirm Your company's profile, and You can use the full functionality of the site",
            'cn' => '添加一张照片或扫描的注册证书是您的法律实体OGRN或个别企业家。 因此，我们可以迅速确认你的公司简介，并且可以使用的全部功能的网站',
        ]
    ],

    [
        'group' => 'interface',
        'key' => 'messages.current',
        'text' => [
            'ru' => 'текущий',
            'en' => 'current',
            'cn' => '电流',
        ]
    ],

    [
        'group' => 'interface',
        'key' => 'messages.from_socials',
        'text' => [
            'ru' => 'Через социальные сети',
            'en' => 'Through social networks',
            'cn' => '通过社交网络',
        ]
    ],

    [
        'group' => 'interface',
        'key' => 'messages.restoring_password',
        'text' => [
            'ru' => 'Восстановление пароля',
            'en' => 'Password recovery',
            'cn' => '密码恢复',
        ]
    ],

    [
        'group' => 'interface',
        'key' => 'messages.restore',
        'text' => [
            'ru' => 'Восстановить',
            'en' => 'Restore',
            'cn' => '恢复',
        ]
    ],

    [
        'group' => 'interface',
        'key' => 'messages.enter_email',
        'text' => [
            'ru' => 'Введите Ваш E-mail',
            'en' => 'Enter Your E-mail',
            'cn' => '输入你的电子邮件',
        ]
    ],

    [
        'group' => 'interface',
        'key' => 'messages.accepting_private_data',
        'text' => [
            'ru' => 'Я соглашаюсь на обработку персональных данных в соответствии с',
            'en' => 'I agree to the processing of personal data in accordance with',
            'cn' => '我同意，以个人数据的处理按照',
        ]
    ],

    [
        'group' => 'interface',
        'key' => 'messages.conditions',
        'text' => [
            'ru' => 'Условиями использования сайта',
            'en' => 'Terms of use of website',
            'cn' => '使用条款的网站',
        ]
    ],

    [
        'group' => 'interface',
        'key' => 'messages.profile_rekvizits',
        'text' => [
            'ru' => 'Если реквизиты компании были изменены, добавьте фото или скан Уведомления о смене реквизитов или выписки ЕГРЮЛ. После подтверждения все закрывающие документы будут оформлены на новые реквизиты.',
            'en' => 'If your company details have been changed, add a photo or scan of the Notice of change of details or extracts from EGRUL. After confirmation of all closing documents will be issued on a new account.',
            'cn' => '如果你的公司的详细信息已经更改，添加一张照片或扫描述的通知，更改的细节或提取物EGRUL. 后确认的所有最后文件将发出一个新的帐户。',
        ]
    ],

    [
        'group' => 'interface',
        'key' => 'messages.politics',
        'text' => [
            'ru' => 'Политикой обработки персональных данных',
            'en' => 'Policy the processing of personal data',
            'cn' => '政策的个人数据的处理',
        ]
    ],

    [
        'group' => 'interface',
        'key' => 'messages.get_messages',
        'text' => [
            'ru' => 'и на получение сообщений в процессе обработки заказа.',
            'en' => 'and to receive messages in the order process.',
            'cn' => '和接收信息的顺序处理。',
        ]
    ],

    [
        'group' => 'interface',
        'key' => 'messages.start_conversation',
        'text' => [
            'ru' => 'Вы можете начать беседу, выбрав нужный диалог',
            'en' => 'You can start a conversation, after selecting the dialogue',
            'cn' => '你可以开始对话之后，选择对话',
        ]
    ],

    //Header
    [
        'group' => 'interface',
        'key' => 'header.catalog',
        'text' => [
            'ru' => 'Каталог',
            'en' => 'Catalog',
            'cn' => '目录',
        ]
    ],
    
    [
        'group' => 'interface',
        'key' => 'header.what_you_search',
        'text' => [
            'ru' => 'Что вы ищете?',
            'en' => 'What you search?',
            'cn' => '你搜索什么?',
        ]
    ],

    [
        'group' => 'interface',
        'key' => 'header.buy',
        'text' => [
            'ru' => 'Запросы на покупку',
            'en' => 'Purchase requests',
            'cn' => '采购申请',
        ]
    ],

    [
        'group' => 'interface',
        'key' => 'header.services',
        'text' => [
            'ru' => 'Сервисы',
            'en' => 'Services',
            'cn' => '服务',
        ]
    ],

    [
        'group' => 'interface',
        'key' => 'header.buyer',
        'text' => [
            'ru' => 'Покупатель',
            'en' => 'Buyer',
            'cn' => '买家',
        ]
    ],

    [
        'group' => 'interface',
        'key' => 'header.seller',
        'text' => [
            'ru' => 'Продавец',
            'en' => 'Seller',
            'cn' => '卖家',
        ]
    ],

    [
        'group' => 'interface',
        'key' => 'header.my_profiles',
        'text' => [
            'ru' => 'Мои профили',
            'en' => 'My profiles',
            'cn' => '我的档案',
        ]
    ],

    [
        'group' => 'interface',
        'key' => 'header.to_suppliers',
        'text' => [
            'ru' => 'Поставщикам',
            'en' => 'To suppliers',
            'cn' => '供应商',
        ]
    ],

    [
        'group' => 'interface',
        'key' => 'header.help',
        'text' => [
            'ru' => 'Помощь',
            'en' => 'Help',
            'cn' => '帮助',
        ]
    ],

    //Footer
    [
        'group' => 'interface',
        'key' => 'footer.company',
        'text' => [
            'ru' => 'Компания',
            'en' => 'About us',
            'cn' => '关于我们',
        ]
    ],

    [
        'group' => 'interface',
        'key' => 'footer.about_company',
        'text' => [
            'ru' => 'О компании',
            'en' => 'About us',
            'cn' => '关于我们',
        ]
    ],

    [
        'group' => 'interface',
        'key' => 'footer.services',
        'text' => [
            'ru' => 'Сервисы',
            'en' => 'Services',
            'cn' => '服务',
        ]
    ],

    [
        'group' => 'interface',
        'key' => 'footer.our_services',
        'text' => [
            'ru' => 'Услуги',
            'en' => 'Services',
            'cn' => '服务',
        ]
    ],

    [
        'group' => 'interface',
        'key' => 'footer.help',
        'text' => [
            'ru' => 'Помощь',
            'en' => 'Help',
            'cn' => '帮助',
        ]
    ],

    [
        'group' => 'interface',
        'key' => 'footer.site_instruction',
        'text' => [
            'ru' => 'Инструкция по работе с сайтом',
            'en' => 'Instructions for working with the site',
            'cn' => '说明工作与网站',
        ]
    ],

    [
        'group' => 'interface',
        'key' => 'footer.qa',
        'text' => [
            'ru' => 'Вопросы - Ответы',
            'en' => 'Questions - Answers',
            'cn' => '问题的答案',
        ]
    ],

    [
        'group' => 'interface',
        'key' => 'footer.contact_us',
        'text' => [
            'ru' => 'Свяжитесь с нами',
            'en' => 'Contact us',
            'cn' => '联系我们',
        ]
    ],

    [
        'group' => 'interface',
        'key' => 'footer.documents',
        'text' => [
            'ru' => 'Документы',
            'en' => 'Documents',
            'cn' => '文件',
        ]
    ],

    [
        'group' => 'interface',
        'key' => 'footer.terms',
        'text' => [
            'ru' => 'Условия использования',
            'en' => 'Terms of use',
            'cn' => '术语的使用',
        ]
    ],

    [
        'group' => 'interface',
        'key' => 'footer.privacy',
        'text' => [
            'ru' => 'Политика конфиденциальности',
            'en' => 'Privacy policy',
            'cn' => '隐私权政策',
        ]
    ],

    [
        'group' => 'interface',
        'key' => 'footer.personal',
        'text' => [
            'ru' => 'Политика обработки персональных данных',
            'en' => 'Policy the processing of personal data',
            'cn' => '政策的个人数据的处理',
        ]
    ],

    [
        'group' => 'interface',
        'key' => 'footer.contacts',
        'text' => [
            'ru' => 'Контакты',
            'en' => 'Contacts',
            'cn' => '联系人',
        ]
    ],

    [
        'group' => 'interface',
        'key' => 'footer.copyrights',
        'text' => [
            'ru' => 'Все права защищены',
            'en' => 'All rights reserved',
            'cn' => '所有权保留',
        ]
    ],

    [
        'group' => 'interface',
        'key' => 'footer.search_request',
        'text' => [
            'ru' => 'Запрос на поиск',
            'en' => 'Search request',
            'cn' => '搜索请求',
        ]
    ],

    [
        'group' => 'interface',
        'key' => 'footer.logistical_service',
        'text' => [
            'ru' => 'Логистический сервис',
            'en' => 'Logistical service',
            'cn' => '后勤服务',
        ]
    ],

    // Каталог
    [
        'group' => 'interface',
        'key' => 'catalog.all_categories',
        'text' => [
            'ru' => 'Все категории',
            'en' => 'All categories',
            'cn' => '所有类别',
        ]
    ],
    
    [
        'group' => 'interface',
        'key' => 'catalog.request_product_id',
        'text' => [
            'ru' => 'Номер запроса',
            'en' => 'Request number',
            'cn' => '请求数',
        ]
    ],
    
    [
        'group' => 'interface',
        'key' => 'catalog.required_quantity',
        'text' => [
            'ru' => 'Необходимое количетсво',
            'en' => 'Required quantity',
            'cn' => '需求数量',
        ]
    ],
    
    [
        'group' => 'interface',
        'key' => 'catalog.buyer',
        'text' => [
            'ru' => 'Покупатель',
            'en' => 'Buyer',
            'cn' => '买家',
        ]
    ],
    
    [
        'group' => 'interface',
        'key' => 'catalog.request_product_describle',
        'text' => [
            'ru' => 'Описание запроса',
            'en' => 'Describle request',
            'cn' => 'Describle请求',
        ]
    ],

    [
        'group' => 'interface',
        'key' => 'catalog.size',
        'text' => [
            'ru' => 'Габариты',
            'en' => 'Sizes',
            'cn' => 'Sizes',
        ]
    ],

    [
        'group' => 'interface',
        'key' => 'catalog.characteristics',
        'text' => [
            'ru' => 'Характеристики',
            'en' => 'Characteristics',
            'cn' => 'Characteristics',
        ]
    ],
    
    [
        'group' => 'interface',
        'key' => 'catalog.price_on_request',
        'text' => [
            'ru' => 'Цена по запросу',
            'en' => 'Price on request',
            'cn' => '价格上请求',
        ]
    ],
    
    [
        'group' => 'interface',
        'key' => 'title.add_to_contacts',
        'text' => [
            'ru' => 'Добавить в контакты',
            'en' => 'Add to contacts',
            'cn' => '添加到触',
        ]
    ],
    
    
    [
        'group' => 'interface',
        'key' => 'title.remove_from_contacts',
        'text' => [
            'ru' => 'Убрать из списка котактов',
            'en' => 'Remove from contacts',
            'cn' => '除去联系',
        ]
    ],
    
    [
        'group' => 'interface',
        'key' => 'catalog.remove_from_favorites',
        'text' => [
            'ru' => 'Убрать из избранного',
            'en' => 'Remove from favorites',
            'cn' => '从收藏夹中删除',
        ]
    ],
    
    [
        'group' => 'interface',
        'key' => 'catalog.popular_supplier_products',
        'text' => [
            'ru' => 'Популярные товары поставщика',
            'en' => 'Popular supplier products',
            'cn' => '受欢迎的供应商的产品',
        ]
    ],
    
    [
        'group' => 'interface',
        'key' => 'catalog.all_sulppier_products',
        'text' => [
            'ru' => 'Все товары поставщика',
            'en' => "All supplier's products",
            'cn' => '所有 供应商 的产品',
        ]
    ],
    
    [
        'group' => 'interface',
        'key' => 'catalog.min_order',
        'text' => [
            'ru' => '(мин. заказ)',
            'en' => '(min. order)',
            'cn' => '(min。 为了)',
        ]
    ],
    
    [
        'group' => 'interface',
        'key' => 'catalog.minimal_order',
        'text' => [
            'ru' => 'Минимальный заказ',
            'en' => 'Minimum order',
            'cn' => '最小的了',
        ]
    ],
    
    [
        'group' => 'interface',
        'key' => 'catalog.all_categories',
        'text' => [
            'ru' => 'Все категории',
            'en' => 'All categories',
            'cn' => '所有类别',
        ]
    ],
    
    [
        'group' => 'interface',
        'key' => 'catalog.available',
        'text' => [
            'ru' => 'Доступно',
            'en' => 'Available',
            'cn' => '可',
        ]
    ],

    [
        'group' => 'interface',
        'key' => 'catalog.product_describle',
        'text' => [
            'ru' => 'Описание товара',
            'en' => "Product describle",
            'cn' => '产品describle',
        ]
    ],
    
    [
        'group' => 'interface',
        'key' => 'catalog.catalog',
        'text' => [
            'ru' => 'Каталог',
            'en' => 'Catalog',
            'cn' => '目录',
        ]
    ],
    
    [
        'group' => 'interface',
        'key' => 'catalog.purchase_requests',
        'text' => [
            'ru' => 'Запросы на покупку',
            'en' => 'Purchase requests',
            'cn' => '采购申请',
        ]
    ],
    
    [
        'group' => 'interface',
        'key' => 'catalog.supplier_warranty_one',
        'text' => [
            'ru' => 'Защита покупателей в течении 60 дней',
            'en' => 'Customer protection for 60 days',
            'cn' => '客户保护60天',
        ]
    ],
    
    [
        'group' => 'interface',
        'key' => 'catalog.supplier_warranty_two',
        'text' => [
            'ru' => 'Только проверенные поставщики',
            'en' => 'Only verified suppliers',
            'cn' => '只有通过验证的供应商',
        ]
    ],
    
    [
        'group' => 'interface',
        'key' => 'catalog.supplier_warranty_three',
        'text' => [
            'ru' => 'Бесплатный возврат',
            'en' => 'Free refund',
            'cn' => '免费退款',
        ]
    ],
    
    [
        'group' => 'interface',
        'key' => 'catalog.supplier_warranty_four',
        'text' => [
            'ru' => 'Делаем сложное простым',
            'en' => 'Making complex things simple',
            'cn' => '做复杂的事情简单',
        ]
    ],
    
    [
        'group' => 'interface',
        'key' => 'catalog.supplier',
        'text' => [
            'ru' => 'Поставщик',
            'en' => 'Supplier',
            'cn' => '供应商',
        ]
    ],
    
    [
        'group' => 'interface',
        'key' => 'catalog.price',
        'text' => [
            'ru' => 'Цена',
            'en' => 'Price',
            'cn' => '价格',
        ]
    ],
    
    [
        'group' => 'interface',
        'key' => 'catalog.articul',
        'text' => [
            'ru' => 'Артикул',
            'en' => 'Articul',
            'cn' => 'Articul',
        ]
    ],

    [
        'group' => 'interface',
        'key' => 'catalog.product_models',
        'text' => [
            'ru' => 'Торговые предложения',
            'en' => 'Models',
            'cn' => '模型',
        ]
    ],
    
    [
        'group' => 'interface',
        'key' => 'sort.shows',
        'text' => [
            'ru' => 'Популярность',
            'en' => 'Popularity',
            'cn' => '普及',
        ]
    ],
    
    [
        'group' => 'interface',
        'key' => 'sort.price',
        'text' => [
            'ru' => 'Цена',
            'en' => 'Price',
            'cn' => '价格',
        ]
    ],
    
    [
        'group' => 'interface',
        'key' => 'sort.rating',
        'text' => [
            'ru' => 'Рейтинг',
            'en' => 'Rating',
            'cn' => '评价',
        ]
    ],
    
    [
        'group' => 'interface',
        'key' => 'title.add_to_favorites',
        'text' => [
            'ru' => 'Добавить в избранное',
            'en' => 'Add to favorites',
            'cn' => '添加的最爱',
        ]
    ],
    
    [
        'group' => 'interface',
        'key' => 'title.remove_to_favorites',
        'text' => [
            'ru' => 'Убрать из избранного',
            'en' => 'Remove from favorites',
            'cn' => '从收藏夹中删除',
        ]
    ],
    
    [
        'group' => 'interface',
        'key' => 'buttons.remove_from_favorites',
        'text' => [
            'ru' => 'Убрать из избранного',
            'en' => 'Remove from favorites',
            'cn' => '从收藏夹中删除',
        ]
    ],

    [
        'group' => 'interface',
        'key' => 'buttons.show_all',
        'text' => [
            'ru' => 'Показать все',
            'en' => 'Show all',
            'cn' => '显示所有',
        ]
    ],

    [
        'group' => 'interface',
        'key' => 'buttons.product_find',
        'text' => [
            'ru' => 'Найти товары',
            'en' => 'Find products',
            'cn' => '找到的产品',
        ]
    ],

    // Поиск
    [
        'group' => 'interface',
        'key' => 'search.found_on_request',
        'text' => [
            'ru' => 'Найдено по запросу',
            'en' => 'Found on request',
            'cn' => '上找到的请求',
        ]
    ],

    [
        'group' => 'interface',
        'key' => 'search.in_category',
        'text' => [
            'ru' => 'в категории',
            'en' => 'in',
            'cn' => 'in',
        ]
    ],

    [
        'group' => 'interface',
        'key' => 'search.found_in_category',
        'text' => [
            'ru' => 'Найдено в категории',
            'en' => 'Found in category',
            'cn' => '中发现的类别',
        ]
    ],

    [
        'group' => 'interface',
        'key' => 'localisation.ru',
        'text' => [
            'ru' => 'Русский',
            'en' => 'Russian',
            'cn' => '俄罗斯',
        ]
    ],

    [
        'group' => 'interface',
        'key' => 'localisation.en',
        'text' => [
            'ru' => 'Английский',
            'en' => 'English',
            'cn' => '英语',
        ]
    ],

    [
        'group' => 'interface',
        'key' => 'localisation.cn',
        'text' => [
            'ru' => 'Китайский',
            'en' => 'Chinese',
            'cn' => '中国',
        ]
    ],

    // Даты
    [
        'group' => 'interface',
        'key' => 'date.minutes_ago',
        'text' => [
            'ru' => 'минут назад',
            'en' => 'minutes ago',
            'cn' => '几分钟前',
        ]
    ],

    [
        'group' => 'interface',
        'key' => 'date.hours_ago',
        'text' => [
            'ru' => 'часов назад',
            'en' => 'hours ago',
            'cn' => '小时前',
        ]
    ],

    [
        'group' => 'interface',
        'key' => 'date.years_ago',
        'text' => [
            'ru' => 'лет назад',
            'en' => 'years ago',
            'cn' => '多年前',
        ]
    ],

    [
        'group' => 'interface',
        'key' => 'date.months_ago',
        'text' => [
            'ru' => 'месяцев назад',
            'en' => 'months ago',
            'cn' => '几个月前',
        ]
    ],

    [
        'group' => 'interface',
        'key' => 'date.seconds_ago',
        'text' => [
            'ru' => 'секунд назад',
            'en' => 'seconds ago',
            'cn' => '几秒钟前',
        ]
    ],

    [
        'group' => 'interface',
        'key' => 'date.now',
        'text' => [
            'ru' => 'сейчас',
            'en' => 'now',
            'cn' => '现在',
        ]
    ],

    //Meta
    [
        'group' => 'interface',
        'key' => 'meta.buy_on_market',
        'text' => [
            'ru' => 'Купить на G2R.Market',
            'en' => 'buy on G2R.Market',
            'cn' => '在市場上購買',
        ]
    ],

    [
        'group' => 'interface',
        'key' => 'meta.sell_on_market',
        'text' => [
            'ru' => 'Продать на G2R.Market',
            'en' => 'sell on G2R.Market',
            'cn' => '在市場上出售',
        ]
    ],

    [
        'group' => 'interface',
        'key' => 'meta.supplier_products',
        'text' => [
            'ru' => 'Товары поставщика',
            'en' => 'Products',
            'cn' => '產品展示',
        ]
    ],

    [
        'group' => 'interface',
        'key' => 'meta.supplier_contacts',
        'text' => [
            'ru' => 'Контакты поставщика',
            'en' => 'Contacts',
            'cn' => '聯絡人',
        ]
    ],
];
?>
