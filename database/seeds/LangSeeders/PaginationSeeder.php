<?php
$paginateData = [

    [
        'group' => 'pagination',
        'key' => 'previous',
        'text' => [
            'ru' => 'Назад',
            'en' => 'Previous',
            'cn' => '以前的',
        ]
    ],

    [
        'group' => 'pagination',
        'key' => 'next',
        'text' => [
            'ru' => 'Вперёд',
            'en' => 'Next',
            'cn' => '下',
        ]
    ],
 
];
?>