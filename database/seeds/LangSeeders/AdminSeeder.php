<?php
$adminData = [

    [
        'group' => 'admin',
        'key' => 'pages.title',
        'text' => [
            'ru' => 'Название страницы',
            'en' => 'Page title',
            'cn' => '',
        ]
    ],
    
    [
        'group' => 'admin',
        'key' => 'pages.parent',
        'text' => [
            'ru' => 'Родительская страница',
            'en' => 'Parent page',
            'cn' => '',
        ]
    ],
    
    [
        'group' => 'admin',
        'key' => 'pages.content',
        'text' => [
            'ru' => 'Контент страницы',
            'en' => 'Content',
            'cn' => '',
        ]
    ],
    
    [
        'group' => 'admin',
        'key' => 'pages.link',
        'text' => [
            'ru' => 'Символьный код',
            'en' => 'Link',
            'cn' => '',
        ]
    ],
    
    [
        'group' => 'admin',
        'key' => 'pages.localisation',
        'text' => [
            'ru' => 'Язык локализации сайта',
            'en' => 'Application localisation',
            'cn' => '',
        ]
    ],
];
?>
