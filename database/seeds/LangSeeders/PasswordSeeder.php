<?php
$passwordData = [

    [
        'group' => 'passwords',
        'key' => 'password',
        'text' => [
            'ru' => 'Пароль должен быть не менее восьми символов и совпадать с подтверждением.',
            'en' => 'The password must be at least eight characters and match the confirmation.',
            'cn' => '密码必须至少八个字，并相匹配的确认。',
        ]
    ],

    [
        'group' => 'passwords',
        'key' => 'reset',
        'text' => [
            'ru' => 'Ваш пароль был сброшен!',
            'en' => 'Your password has been reset!',
            'cn' => '你的密码已经重置！',
        ]
    ],

    [
        'group' => 'passwords',
        'key' => 'sent',
        'text' => [
            'ru' => 'Ссылка на сброс пароля была отправлена!',
            'en' => 'Link to reset password has been sent!',
            'cn' => '位于密码已经发送！',
        ]
    ],

    [
        'group' => 'passwords',
        'key' => 'token',
        'text' => [
            'ru' => 'Ошибочный код сброса пароля.',
            'en' => 'This password reset token is invalid',
            'cn' => '这个密码重置标记是无效的',
        ]
    ],

    [
        'group' => 'passwords',
        'key' => 'user',
        'text' => [
            'ru' => 'Не удалось найти пользователя с указанным электронным адресом.',
            'en' => "We can't find a user with that e-mail address.",
            'cn' => '我们不可能找到一个用户的电子邮件地址。',
        ]
    ],

];
?>