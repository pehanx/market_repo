<?php
$breadcrumbsData = [

    //Кабинет
    [
        'group' => 'breadcrumbs',
        'key' => 'cabinet.cabinet',
        'text' => [
            'ru' => 'Кабинет',
            'en' => 'Cabinet',
            'cn' => '内阁',
        ]
    ],

    [
        'group' => 'breadcrumbs',
        'key' => 'cabinet.private_information',
        'text' => [
            'ru' => 'Личные данные',
            'en' => 'Private information',
            'cn' => '私人信息',
        ]
    ],

    [
        'group' => 'breadcrumbs',
        'key' => 'cabinet.seller',
        'text' => [
            'ru' => 'Продавец',
            'en' => 'Seller',
            'cn' => '卖家',
        ]
    ],

      [
        'group' => 'breadcrumbs',
        'key' => 'cabinet.buyer',
        'text' => [
            'ru' => 'Покупатель',
            'en' => 'Buyer',
            'cn' => '买家',
        ]
    ],

    [
        'group' => 'breadcrumbs',
        'key' => 'cabinet.all_profiles',
        'text' => [
            'ru' => 'Все профили',
            'en' => 'All profiles',
            'cn' => '所有的档案',
        ]
    ],

    [
        'group' => 'breadcrumbs',
        'key' => 'cabinet.all_managers',
        'text' => [
            'ru' => 'Все менеджеры',
            'en' => 'All managers',
            'cn' => '所有管理人员',
        ]
    ],

    [
        'group' => 'breadcrumbs',
        'key' => 'cabinet.all_products',
        'text' => [
            'ru' => 'Каталог товаров',
            'en' => "Product's catalog",
            'cn' => '产品目录',
        ]
    ],
    
    [
        'group' => 'breadcrumbs',
        'key' => 'cabinet.show_product',
        'text' => [
            'ru' => 'Просмотр товара',
            'en' => 'Viewing the product',
            'cn' => '视产品',
        ]
    ],
    
    [
        'group' => 'breadcrumbs',
        'key' => 'cabinet.show_request_product',
        'text' => [
            'ru' => 'Просмотр запроса',
            'en' => 'Viewing the request product',
            'cn' => '视所要求的产品',
        ]
    ],

    [
        'group' => 'breadcrumbs',
        'key' => 'cabinet.profile_create',
        'text' => [
            'ru' => 'Создание профиля',
            'en' => 'Create a profile',
            'cn' => '创建档案',
        ]
    ],

    [
        'group' => 'breadcrumbs',
        'key' => 'cabinet.profile_managers',
        'text' => [
            'ru' => 'Менеджеры профиля',
            'en' => 'Managers profile',
            'cn' => '管理人员简介',
        ]
    ],

    [
        'group' => 'breadcrumbs',
        'key' => 'cabinet.new_profile',
        'text' => [
            'ru' => 'Новый профиль',
            'en' => 'New profile',
            'cn' => '新的轮廓',
        ]
    ],

    [
        'group' => 'breadcrumbs',
        'key' => 'cabinet.new_manager',
        'text' => [
            'ru' => 'Новый менеджер',
            'en' => 'New Manager',
            'cn' => '新经理',
        ]
    ],

    [
        'group' => 'breadcrumbs',
        'key' => 'cabinet.creating_product',
        'text' => [
            'ru' => 'Создание товара',
            'en' => 'Create a product',
            'cn' => '创建一个产品',
        ]
    ],

    [
        'group' => 'breadcrumbs',
        'key' => 'cabinet.editing_product_model',
        'text' => [
            'ru' => 'Изменение торгового предложения',
            'en' => 'Change in trade proposals',
            'cn' => '改变中的贸易的建议',
        ]
    ],

    [
        'group' => 'breadcrumbs',
        'key' => 'cabinet.editing_product',
        'text' => [
            'ru' => 'Изменение товара',
            'en' => 'Product change',
            'cn' => '产品改变',
        ]
    ],

    [
        'group' => 'breadcrumbs',
        'key' => 'cabinet.editing_profile',
        'text' => [
            'ru' => 'Изменение профиля',
            'en' => 'Edit profile',
            'cn' => '编辑档案',
        ]
    ],

    [
        'group' => 'breadcrumbs',
        'key' => 'cabinet.creating_profile',
        'text' => [
            'ru' => 'Создание профиля',
            'en' => 'Creating profile',
            'cn' => '创建档案',
        ]
    ],

    [
        'group' => 'breadcrumbs',
        'key' => 'cabinet.all_request_products',
        'text' => [
            'ru' => 'Каталог запросов',
            'en' => "Request's catalog",
            'cn' => '请求的目录',
        ]
    ],

    [
        'group' => 'breadcrumbs',
        'key' => 'cabinet.creating_request_product',
        'text' => [
            'ru' => 'Создание запроса',
            'en' => 'Create a query',
            'cn' => '',
        ]
    ],

    [
        'group' => 'breadcrumbs',
        'key' => 'cabinet.editing_request_product',
        'text' => [
            'ru' => 'Изменение запроса',
            'en' => 'Change request',
            'cn' => '变更的请求',
        ]
    ],

    [
        'group' => 'breadcrumbs',
        'key' => 'cabinet.export_view',
        'text' => [
            'ru' => 'Экспорт',
            'en' => 'Export',
            'cn' => '出口',
        ]
    ],

    [
        'group' => 'breadcrumbs',
        'key' => 'cabinet.export_create',
        'text' => [
            'ru' => 'Экспортировать',
            'en' => 'Create export',
            'cn' => '创建出口',
        ]
    ],

    [
        'group' => 'breadcrumbs',
        'key' => 'cabinet.export_product_create',
        'text' => [
            'ru' => 'Экспорт товаров',
            'en' => 'Exports of products',
            'cn' => '出口的产品',
        ]
    ],

    [
        'group' => 'breadcrumbs',
        'key' => 'cabinet.export_request_product_create',
        'text' => [
            'ru' => 'Экспорт запросов',
            'en' => 'Export of requests',
            'cn' => '出口的请求',
        ]
    ],
    
    [
        'group' => 'breadcrumbs',
        'key' => 'cabinet.messages',
        'text' => [
            'ru' => 'Сообщения',
            'en' => 'Messages',
            'cn' => '消息',
        ]
    ],

    [
        'group' => 'breadcrumbs',
        'key' => 'pages.main',
        'text' => [
            'ru' => 'Главная',
            'en' => 'Main',
            'cn' => '主',
        ]
    ],

    [
        'group' => 'breadcrumbs',
        'key' => 'pages.catalog',
        'text' => [
            'ru' => 'Каталог',
            'en' => 'Catalog',
            'cn' => '目录',
        ]
    ],

    [
        'group' => 'breadcrumbs',
        'key' => 'cabinet.import_view',
        'text' => [
            'ru' => 'Список импортов',
            'en' => 'Import list',
            'cn' => '进口列表',
        ]
    ],

    [
        'group' => 'breadcrumbs',
        'key' => 'cabinet.import_request_product_create',
        'text' => [
            'ru' => 'Импорт запросов',
            'en' => 'Import of requests',
            'cn' => '进口请求',
        ]
    ],

    [
        'group' => 'breadcrumbs',
        'key' => 'cabinet.import_product_create',
        'text' => [
            'ru' => 'Импорт товаров',
            'en' => 'Import of products',
            'cn' => '进口的产品',
        ]
    ],

    [
        'group' => 'breadcrumbs',
        'key' => 'cabinet.creating_buyer_profile',
        'text' => [
            'ru' => 'Создание профиля покупателя',
            'en' => 'Creating a buyer profile',
            'cn' => '创建一个买家',
        ]
    ],

    [
        'group' => 'breadcrumbs',
        'key' => 'cabinet.creating_seller_profile',
        'text' => [
            'ru' => 'Создание профиля продавца',
            'en' => 'Creating a seller profile',
            'cn' => '创建一个卖方的配置文件',
        ]
    ],
    
    [
        'group' => 'breadcrumbs',
        'key' => 'cabinet.favorites',
        'text' => [
            'ru' => 'Избранное',
            'en' => 'Favorites',
            'cn' => '最爱',
        ]
    ],

    [
        'group' => 'breadcrumbs',
        'key' => 'cabinet.messages',
        'text' => [
            'ru' => 'Сообщения',
            'en' => 'Messages',
            'cn' => '消息',
        ]
    ],
];
