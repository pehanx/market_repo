<?php
$authData = [

    [
        'group' => 'auth',
        'key' => 'failed',
        'text' => [
            'ru' => 'Имя пользователя и пароль не совпадают.',
            'en' => 'These credentials do not match our records.',
            'cn' => '这些凭证不匹配我们的记录。',
        ]
    ],

    [
        'group' => 'auth',
        'key' => 'throttle',
        'text' => [
            'ru' => 'Слишком много попыток входа. Пожалуйста, попробуйте еще раз через :seconds секунд.',
            'en' => 'Too many login attempts. Please try again in :seconds seconds.',
            'cn' => '太多登录尝试。 请再试一次在：秒秒钟。',
        ]
    ],

];
?>