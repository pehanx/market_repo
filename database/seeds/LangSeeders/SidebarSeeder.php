<?
$sidebarData = [

    [
        'group' => 'sidebars',
        'key' => 'my_account',
        'text' => [
            'ru' => 'Аккаунт',
            'en' => 'Account',
            'cn' => '帐户',
        ]
    ],

    [
        'group' => 'sidebars',
        'key' => 'entrance_account',
        'text' => [
            'ru' => 'Учетная запись',
            'en' => 'Entrance account',
            'cn' => '入口处帐户',
        ]
    ],

    [
        'group' => 'sidebars',
        'key' => 'profiles',
        'text' => [
            'ru' => 'Профили',
            'en' => 'Profiles',
            'cn' => '配置文件',
        ]
    ],
    
    [
        'group' => 'sidebars',
        'key' => 'seller',
        'text' => [
            'ru' => 'Продавец',
            'en' => 'Seller',
            'cn' => '卖家',
        ]
    ],
    
    [
        'group' => 'sidebars',
        'key' => 'buyer',
        'text' => [
            'ru' => 'Покупатель',
            'en' => 'Buyer',
            'cn' => '买家',
        ]
    ],

    [
        'group' => 'sidebars',
        'key' => 'admin_panel',
        'text' => [
            'ru' => 'Панель администратора',
            'en' => 'Admin panel',
            'cn' => '管理面板',
        ]
    ],

    [
        'group' => 'sidebars',
        'key' => 'user',
        'text' => [
            'ru' => 'Пользователи',
            'en' => 'Users',
            'cn' => '用户',
        ]
    ],

    [
        'group' => 'sidebars',
        'key' => 'managers',
        'text' => [
            'ru' => 'Менеджеры',
            'en' => 'Managers',
            'cn' => '管理人员',
        ]
    ],

    [
        'group' => 'sidebars',
        'key' => 'managers_profiles',
        'text' => [
            'ru' => 'Менеджеры профилей',
            'en' => 'Managers profiles',
            'cn' => '管理人员概况',
        ]
    ],

    [
        'group' => 'sidebars',
        'key' => 'private_information',
        'text' => [
            'ru' => 'Личные данные',
            'en' => 'Personal data',
            'cn' => '个人数据',
        ]
    ],

    [
        'group' => 'sidebars',
        'key' => 'products',
        'text' => [
            'ru' => 'Товары',
            'en' => 'Products',
            'cn' => '产品',
        ]
    ],

    [
        'group' => 'sidebars',
        'key' => 'catalog_products',
        'text' => [
            'ru' => 'Каталог товаров',
            'en' => "Product's catalog",
            'cn' => '产品目录',
        ]
    ],
    
    [
        'group' => 'sidebars',
        'key' => 'import_products',
        'text' => [
            'ru' => 'Импорт товаров',
            'en' => 'Import products',
            'cn' => '进口的产品',
        ]
    ],
    
    [
        'group' => 'sidebars',
        'key' => 'export_products',
        'text' => [
            'ru' => 'Экспорт товаров',
            'en' => 'Export products',
            'cn' => '出口产品',
        ]
    ],

    [
        'group' => 'sidebars',
        'key' => 'request_products',
        'text' => [
            'ru' => 'Запросы на покупку',
            'en' => "Purchase request's catalog",
            'cn' => '采购请求的目录',
        ]
    ],

    [
        'group' => 'sidebars',
        'key' => 'catalog_requests',
        'text' => [
            'ru' => 'Каталог запросов',
            'en' => 'Requests',
            'cn' => '请求',
        ]
    ],

    [
        'group' => 'sidebars',
        'key' => 'import_request_products',
        'text' => [
            'ru' => 'Импорт запросов',
            'en' => 'Import requests',
            'cn' => '进口请求',
        ]
    ],
    
    [
        'group' => 'sidebars',
        'key' => 'export_request_products',
        'text' => [
            'ru' => 'Экспорт запросов',
            'en' => 'Export requests',
            'cn' => '出口的请求',
        ]
    ],

    [
        'group' => 'sidebars',
        'key' => 'favorite',
        'text' => [
            'ru' => 'Избранное',
            'en' => 'Favorites',
            'cn' => '最爱',
        ]
    ],
    
    [
        'group' => 'sidebars',
        'key' => 'pages',
        'text' => [
            'ru' => 'Страницы',
            'en' => 'Pages',
            'cn' => '网页',
        ]
    ],
    
    [
        'group' => 'sidebars',
        'key' => 'messages',
        'text' => [
            'ru' => 'Сообщения',
            'en' => 'Messages',
            'cn' => '消息',
        ]
    ],
    
    [
        'group' => 'sidebars',
        'key' => 'chats',
        'text' => [
            'ru' => 'Чаты',
            'en' => 'Chats',
            'cn' => '聊天',
        ]
    ],

    [
        'group' => 'sidebars',
        'key' => 'chats_support',
        'text' => [
            'ru' => 'Техническая поддержка',
            'en' => 'Support',
            'cn' => '支持',
        ]
    ],
    
    [
        'group' => 'sidebars',
        'key' => 'contacts',
        'text' => [
            'ru' => 'Контакты',
            'en' => 'Contacts',
            'cn' => '联系人',
        ]
    ],
    
    [
        'group' => 'sidebars',
        'key' => 'site_settings',
        'text' => [
            'ru' => 'Настройки сайта',
            'en' => 'Site settings',
            'cn' => '网站的设置',
        ]
    ],

    [
        'group' => 'sidebars',
        'key' => 'companies',
        'text' => [
            'ru' => 'Компании',
            'en' => 'Companies',
            'cn' => '公司',
        ]
    ],

    [
        'group' => 'sidebars',
        'key' => 'manager_cabinet',
        'text' => [
            'ru' => 'Кабинет менеджера',
            'en' => "Manager's cabinet",
            'cn' => '經理辦公室',
        ]
    ],

];
?>