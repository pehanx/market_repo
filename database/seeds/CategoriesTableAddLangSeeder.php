<?php

use App\Models\{Category, CategoryProperty, Localisation};
use Carbon\Carbon;
use Illuminate\Database\Seeder;

class CategoriesTableAddLangSeeder extends Seeder
{
    public function run()
    {
        $langCode = 'de'; //Код нового языка из базы
        
        include('ExampleCategoriesTableSeeder/CategoriesSeeder.php'); //categoriesData

        $lang = Localisation::where('code',$langCode)->first();

        if($lang === null)
        {
            dd('Не указан код для языка, либо его нет в базе');
        }

        $langCode = strtoupper($langCode);

        foreach ($categoriesData as $newLangCategory)
        {
            $category = Category::where('link', $newLangCategory['CODE'])->first();
            

            if($category !== null)
            {
                $filterableArray = [];

                if(isset($newLangCategory["UF_NAME_$langCode"]) && $newLangCategory["UF_NAME_$langCode"] !== null)
                {
                    $filterableArray = [
                        [
                            'localisation_id' => $lang->id,
                            'code' => 'seo_description',
                            'value' => $newLangCategory["UF_META_DESCR_$langCode"] ?? $newLangCategory["UF_NAME_$langCode"],
                        ],
                        [
                            'localisation_id' => $lang->id,
                            'code' => 'seo_title',
                            'value' => $newLangCategory["UF_META_TITLE_$langCode"] ?? $newLangCategory["UF_NAME_$langCode"],
                        ],
                        [
                            'localisation_id' => $lang->id,
                            'code' => 'name',
                            'value' => $newLangCategory["UF_NAME_$langCode"],
                        ],
                    ];
                }

                $category->categoryProperties()->createMany($filterableArray);
            }
        }
        
    }
}