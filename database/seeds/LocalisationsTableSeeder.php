<?php

use Illuminate\Database\Seeder;
use App\Models\Localisation;

class LocalisationsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Localisation::create(
        [
            'code' => 'ru',
			'value' => 'russian'
        ]);
		
		Localisation::create(
        [
            'code' => 'cn',
			'value' => 'chinese'
        ]);
		
		Localisation::create(
        [
            'code' => 'en',
			'value' => 'english'
        ]);
    }
}
