<?php

use App\Models\{Category, CategoryProperty, Localisation, Product, RequestProduct};
use Carbon\Carbon;
use Illuminate\Database\Seeder;

class CategoriesTableUpdateSeeder extends Seeder
{

    public function run()
    {
        $parentCategories = [];
        $newProperties = [];

        $langs = Localisation::select('code', 'id')
            ->get()
            ->mapWithKeys(function ($item) {
                return [$item['id'] => strtoupper($item['code'])];
            });

        include('ExampleCategoriesTableSeeder/CategoriesParents.php'); //categoriesData

        foreach ($categoriesData as $category)
        {
            $parentCategories[$category['ID']] = $category['CODE'];
        }

        foreach (Category::all() as $dbCategory)
        {
            if( !in_array($dbCategory->link, $parentCategories) && $dbCategory->is_active === false )
            {
                $dbCategory->delete();
            }
        }

        foreach ($categoriesData as $category)
        {
            $oldCategory = Category::where('link', $category['CODE'])->first();

            foreach ($langs as $langId => $langCode)
            {
                $this->updateProperties($category, $oldCategory, $langId, $langCode);
            }

            $this->updateParents($category, $oldCategory, $parentCategories);
            $this->updateSort($category, $oldCategory, $parentCategories);
        }

        if($newProperties !== [])
        {
            CategoryProperty::insert($newProperties);
        }
    }

    /* 
     * Обновление сортировки
     */
    private function updateSort(array $category, $oldCategory)
    {
        if($category['SORT'] !== $oldCategory->sort)
        {
            $oldCategory->sort = $category['SORT'];
            $oldCategory->save();
        }
    }

    /* 
     * Обновление родителей
     */
    private function updateParents(array $category, $oldCategory, array $parentCategories)
    {

        if( (int)$category['DEPTH_LEVEL'] === 2 && $oldCategory->parent_id !== 1 )
        {
            $oldCategory->parent()->associate(Category::find(1))->save();
            
        }
        
        if( (int)$category['DEPTH_LEVEL'] > 2 
            && isset( $parentCategories[ $category['IBLOCK_SECTION_ID'] ] )
            && $oldCategory->parent_id !== $parentCategories[ $category['IBLOCK_SECTION_ID'] ] 
            && in_array( $category['IBLOCK_SECTION_ID'], array_keys($parentCategories) )
            )
        {

            $newParentCode = $parentCategories[ $category['IBLOCK_SECTION_ID'] ]; 

            $dbParent = Category::where('link',$newParentCode)->first();

            
            $oldCategory->parent_id = $dbParent->id;
            $oldCategory->update();
        }
    }

    /* 
     * Обновление свойств
     */
    private function updateProperties( array $category, $oldCategory, int $langId, string $langCode)
    {
        if($oldCategory !== null)
        {
            $categoryProperties = CategoryProperty::where('category_id',$oldCategory->id)->get();
                
            //Название
            $name = $categoryProperties->where('code','name')
                ->where('localisation_id',$langId)
                ->first();

            $this->writeAttributes($category ,$name, "UF_NAME_$langCode", $langId);
            
            //Заголовок
            $title = $categoryProperties->where('code','seo_title')
                ->where('localisation_id',$langId)
                ->first();

            $this->writeAttributes($category ,$title, "UF_META_TITLE_$langCode", $langId);
            

            //Описание
            $description = $categoryProperties->where('code','seo_description')
                ->where('localisation_id',$langId)
                ->first();

            $this->writeAttributes($category ,$description, "UF_META_DESCR_$langCode", $langId);
        }
    }

    /* 
     * Запись свойств
     */
    private function writeAttributes(array $category, $property, string $codeInArray, int $langId)
    {
        if( $property !== null
        && isset($category[$codeInArray])
        && $category[$codeInArray] !== $property->value
        )
        {
            $property->value = $category[$codeInArray];
            $property->update();

        } elseif($property === null&& isset($category[$codeInArray]) )
        {
            array_push($newProperties, [
                'category_id' => $property->id,
                'code' => $property->code,
                'value' => $category[$codeInArray],
                'localisation_id' => $langId,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ]);
        }
    }
}