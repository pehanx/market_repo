<?php

use App\Models\{Category, CategoryProperty, Localisation};
use Carbon\Carbon;
use Illuminate\Http\File;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Seeder;

class MainPageCategoriesSeeder extends Seeder
{
    const IMAGES_DIR = '/assemble/images/mainpageCategories/';
    const LOCALISATION_ID = 1; //ru

    /* создание категорий с картинками */
    public function run()
    {        
        $popularCategories = [
            10 => 'Продукты питания и напитки',
            20 => 'Одежда',
            30 => 'Мебель и интерьер', 
            40 => 'Промышленные товары и оборудование',
            50 => 'Сельское хозяйство',
            60 => 'Оборудование для химической промышленности',
            70 => 'Текстиль и фурнитура',
            80 => 'Автотранспорт',
            90 => 'Упаковка для товаров, промостенды'
        ];

        $categoriesWithImages = Category::whereHas('categoryProperties', function (Builder $query) use($popularCategories) {
            $query
                ->where([
                    ['localisation_id', self::LOCALISATION_ID],
                    ['code','name'],                    
                ])
                ->whereIn('value', $popularCategories);
        })
        ->with('categoryProperties')
        ->limit(9)->get();

        if( is_dir( public_path() . self::IMAGES_DIR ) )
        {
            $data = [];

            $filesInFolder = scandir( public_path() . self::IMAGES_DIR );

            foreach ($filesInFolder as $file)
            {                
                if( is_file(public_path() . self::IMAGES_DIR . $file) )
                {
                    $image = new File( public_path() . self::IMAGES_DIR . $file );

                    $imageName = strstr( basename($image->path()), '.' , true ); //Получаем название изображения без формата, чтобы соотнести его с категорией

                    $category = $categoriesWithImages->where('link', $imageName)->first();

                    if($category)
                    {
                        array_push($data, [
                            'category' => $category,
                            'image' => $image,
                        ]);
                    }
                }
            }
    
            foreach ($data as $categoryAndImage)
            {
                //Получаем относительный путь для записи в бд
                $cleanPath = str_replace(
                    [realpath(public_path() . '/assemble'), '\\'],
                    ['', '/'],
                    $categoryAndImage['image']->getRealPath()
                );

                $categoryAndImage['category']->downloads()->create(
                    [
                        'title' => basename($categoryAndImage['image']),
                        'path' => $cleanPath,
                        'size' => $categoryAndImage['image']->getSize(),
                        'mime_type' => $categoryAndImage['image']->getMimeType(),
                        'type' => 'category_banner',
                        'user_id' => 1,
                    ]
                );
            }

            $properties = CategoryProperty::whereIn('value', array_values($popularCategories))->with('categories')->get();

            foreach ($popularCategories as $sort => $name)
            {
                $category = $properties->where('value', $name)->first()->categories;

                $category->sort = $sort;
                $category->save();
            }
        }
    }
}