<?php

use App\Models\{
    Profile,
    ProfileProperty,
    ProfileType,
    Product,
    ProductProperty,
    ProductPrice,
    ProductModel,
    RequestProduct,
    RequestProductPrice,
    RequestProductProperty,
    Characteristic,
};
use App\Events\Profile\ProfileAdded;
use Illuminate\Http\File;
use App\{User,Localisation};
use Illuminate\Database\Seeder;
use Faker\Generator as Faker;

class EntitiesSeeder extends Seeder
{
    const PROFILE_NEED = 20;
    const PRODUCT_NEED = 40; //Общее количество, которое рандомно запишется продавцам
    const MODELS_ON = 1; // 0 - Модели создаваться не будут, 1 - Модели будут создаваться у некоторых продуктов
    const MODELS_MAX = 4; //Максимальное количество моделей у продуктов
    const REQUEST_PRODUCT_NEED = 20; //Общее количество, которое рандомно запишется покупателям
    const MANAGERS_NEED = 2;

    //Файлы сущностей, которые существуют в единственном числе
    const ONE_FILE = [
        'preview_image',
        'company_logo_image',
        'company_description_image'
    ];

    private $user;

    public function run()
    {
        // tumanovdnet@gmail.com
        // tester322
        factory(User::class, 1)->state('robot')->make()->each(function(User $user)
        {
            $user->save();
            $user->assignRole(['buyer','seller']);
            $this->user = $user;
            $this->createProfiles();
        });

        factory(User::class, 1)->state('robot2')->make()->each(function(User $user)
        {
            $user->save();
            $user->assignRole(['buyer','seller']);
            $this->user = $user;
            $this->createProfiles();
        });
        
        echo("Profiles: Ready \n");

        factory(Product::class, self::PRODUCT_NEED)->state('product')->make()->each(function(Product $product)
        {
            $product->save();
            
            $product->productProperties()->createMany( 
                factory(ProductProperty::class, 1)->state('product_properties')->make()->toArray()[0]
            );

            $product->productPrices()->create( 
                factory(ProductPrice::class, 1)->state('product_prices')->make()->toArray()[0]
            );

            $product->characteristics()->createMany(
                factory(Characteristic::class, 1)->state('product_characteristics')->make()->toArray()[0]
            );
            
            $this->makeEntityFiles($product, 'products');

            if(rand(0,self::MODELS_ON) === 1) //Добавлять модели продукту или нет
            {
                $i = 0;
                while ($i <= rand(1,self::MODELS_MAX))
                {
                    $this->makeModels($product);
                    $i++;
                }
            }
        });

        echo("Products: Ready \n");

        factory(RequestProduct::class, self::REQUEST_PRODUCT_NEED)->state('request_product')->make()->each(function(RequestProduct $requestProduct)
        {
            $requestProduct->save();
            
            $requestProduct->requestProductProperties()->createMany( 
                factory(RequestProductProperty::class, 1)->state('request_product_properties')->make()->toArray()[0]
            );            
            
            $requestProduct->requestProductPrices()->create( 
                factory(RequestProductPrice::class, 1)->state('request_product_prices')->make()->toArray()[0]
            );
            
            $requestProduct->characteristics()->createMany(
                factory(Characteristic::class, 1)->state('request_product_characteristics')->make()->toArray()[0]
            );
            
            $this->makeEntityFiles($requestProduct, 'request_products');
        });

        echo("RequestProducts: Ready \n");
       
    }

    /* 
     * Запуск фабрики профилей
     */
    private function createProfiles()
    {
        foreach ([1,2,3] as $locale)
        {
            factory(Profile::class, 1)->state('profile_seller')->make()->each(function(Profile $profile) use($locale) {
                $profile = $this->user->profiles()->create( $profile->toArray() );
                if (!$this->checkLocaleExist($profile))
                {
                    $propertyArray = factory(ProfileProperty::class, 1)->state('profile_properties')->make(['localisation_id'=>$locale])->toArray()[0];
                    unset($propertyArray['localisation_id']);
                    $profile->profileProperties()->createMany(
                        $propertyArray
                    );

                    $this->makeEntityFiles($profile, 'uploads');

                    $this->createManagers($profile);
                }
            });

            factory(Profile::class, 1)->state('profile_buyer')->make()->each(function(Profile $profile) use($locale) {
                $profile = $this->user->profiles()->create( $profile->toArray() );
                if (!$this->checkLocaleExist($profile))
                {
                    $propertyArray = factory(ProfileProperty::class, 1)->state('profile_properties')->make(['localisation_id'=>$locale])->toArray()[0];
                    unset($propertyArray['localisation_id']);

                    $profile->profileProperties()->createMany(
                        $propertyArray    
                    );

                    $this->makeEntityFiles($profile, 'uploads');

                    $this->createManagers($profile);
                }
            });
        }
    }

    /* 
     * Cоздание манагеров
     */
    private function createManagers($companyProfile)
    {
        factory(Profile::class, self::MANAGERS_NEED)->state('managers')->make()->each(function(Profile $profile) use($companyProfile) {
            
            $managerUser = factory(User::class, 1)->state('managers')->make()->toArray()[0];
            $managerUser['password'] = bcrypt('tester322');

            $manager = $companyProfile->users()->create($managerUser);

            $managerProfile = $manager->profiles()->create( $profile->toArray() );

            $propertyArray = factory(ProfileProperty::class, 1)->state('manager_properties')->make(['localisation_id'=>1])->toArray()[0];

            unset($propertyArray['localisation_id']);
            $managerProfile->profileProperties()->createMany(
                $propertyArray
            );

            $role = ProfileType::where('id', $companyProfile->profile_type_id)->first()->code;
            $role .= '_manager';

            event( new ProfileAdded( $companyProfile, $role, $manager ));
        });
    }

    /* 
     * Создание картинок для продукта
     */
    private function makeEntityFiles($entity, string $path)
    {
        $result = [];

        if($path === 'products')
        {
            $entityFiles = $this->getProductImages();

        } elseif($path === 'request_products')
        {
            $entityFiles = $this->getRequestProductImages();

        } elseif($path === 'uploads')
        {
            $entityFiles = $this->getProfileImages();
        }

        foreach ($entityFiles as $type => $imageName)
        {
            //Копирование файлов из папки сидера, в папку сущности
            @mkdir( \Storage::disk( env('FILESYSTEM_DRIVER') )->path('public/' . $path . '/') . $entity->profile_id , 0777, true);
            @chmod( \Storage::disk( env('FILESYSTEM_DRIVER') )->path('public/' . $path . '/') . $entity->profile_id , 0777);
            if ( in_array($type, self::ONE_FILE) )
            {
                copy(
                    \Storage::disk( env('FILESYSTEM_DRIVER') )->path('public/seederImages/') . $imageName,
                    \Storage::disk( env('FILESYSTEM_DRIVER') )->path('public/' . $path . '/') . $entity->profile_id . '/' . $imageName
                );

                $imageFile = new File( \Storage::disk( env('FILESYSTEM_DRIVER') )->path('public/' . $path . '/') . $entity->profile_id . '/' . $imageName );
                array_push($result,[
                    'type' => $type,
                    'file' => $imageFile,
                ]);

            } else
            {
                foreach($imageName as $innerImage)
                {
                    copy(
                        \Storage::disk( env('FILESYSTEM_DRIVER') )->path('public/seederImages/') . $innerImage,
                        \Storage::disk( env('FILESYSTEM_DRIVER') )->path('public/' . $path . '/') . $entity->profile_id . '/' . $innerImage
                    );
    
                    $imageFile = new File( \Storage::disk( env('FILESYSTEM_DRIVER') )->path('public/' . $path . '/') . $entity->profile_id . '/' . $innerImage );
                    array_push($result,[
                        'type' => $type,
                        'file' => $imageFile,
                    ]);
                }
            }
        }

        foreach ($result as $file)
        {
            $filePath = str_replace(
                realpath(\Storage::disk(env('FILESYSTEM_DRIVER'))->path('') . 'public/'),
                '',
                $file['file']->getRealPath()
            );

            $entity->downloads()->create([
                'title' => basename($file['file']->path()),
                'path' => $filePath,
                'size' => $file['file']->getSize(),
                'mime_type' => $file['file']->getMimeType(),
                'path' => $filePath,
                'user_id' => $this->user->id,
                'type' => $file['type'],
            ]);

            unset($filePath);
        }
    }

    /* 
     * Создание моделей для продукта
     */
    private function makeModels($product)
    {
        $articulPrefix = ProductModel::getPrefixForArticul($product->id);
        
        $productModel = $product->productModels()->create([
            'articul' => $articulPrefix,
            'sort' => 1
        ]);

        $productModel->characteristics()->createMany(
            factory(ProductModel::class, 1)->state('product_models')->make()->toArray()[0]
        ); 

        $productModel->productPrices()->create(
            factory(ProductModel::class, 1)->state('product_prices')->make()->toArray()[0]
        );

        $file = $this->makeModelFile($product)[0];

        $filePath = str_replace(
            realpath(\Storage::disk(env('FILESYSTEM_DRIVER'))->path('') . 'public/'),
            '',
            $file['file']->getRealPath()
        );

        $productModel->downloads()->create([
            'title' => basename($file['file']->path()),
            'path' => $filePath,
            'size' => $file['file']->getSize(),
            'mime_type' => $file['file']->getMimeType(),
            'path' => $filePath,
            'user_id' => $this->user->id,
            'type' => $file['type'],
        ]);
    }

    /* 
     * Создание картинок для моделей продукта
     */
    private function makeModelFile($product) :array
    {
        $result = [];

        $imageName = $this->getModelImage();

        copy(
            \Storage::disk( env('FILESYSTEM_DRIVER') )->path('public/seederImages/') . $imageName,
            \Storage::disk( env('FILESYSTEM_DRIVER') )->path('public/products/') . $product->profile_id . '/' . $imageName
        );

        $imageFile = new File( \Storage::disk( env('FILESYSTEM_DRIVER') )->path('public/products/') . $product->profile_id . '/' . $imageName );
        
        array_push($result,[
            'type' => 'preview_image',
            'file' => $imageFile,
        ]);

        return $result;
    }

    /* 
     * Получение рандомных картинок для продукта
     */
    private function getProductImages()
    {
        $imagesArray = \Storage::disk( env('FILESYSTEM_DRIVER') )->files('public/seederImages');
       
        $previewImage = basename($imagesArray[rand(0, count($imagesArray)-1)]);

        $describleImages = [ 
            basename($imagesArray[rand(0, count($imagesArray)-1)]),
            basename($imagesArray[rand(0, count($imagesArray)-1)]),
        ];

        $detailImages = [ 
            basename($imagesArray[rand(0, count($imagesArray)-1)]),
            basename($imagesArray[rand(0, count($imagesArray)-1)]),
            basename($imagesArray[rand(0, count($imagesArray)-1)]),
        ];

        return [
            'preview_image' => $previewImage,
            'describle_images' => $describleImages,
            'detail_images' => $detailImages,
        ];
    }

    /* 
     * Получение рандомной картинки для модели продукта
     */
    private function getProfileImages()
    {
        $imagesArray = \Storage::disk( env('FILESYSTEM_DRIVER') )->files('public/seederImages');

        $companyMainBannerImages = [ 
            basename($imagesArray[rand(0, count($imagesArray)-1)]),
            basename($imagesArray[rand(0, count($imagesArray)-1)]),
            basename($imagesArray[rand(0, count($imagesArray)-1)]),
        ];

        $companyPromoBannerImages = [ 
            basename($imagesArray[rand(0, count($imagesArray)-1)]),
            basename($imagesArray[rand(0, count($imagesArray)-1)]),
            basename($imagesArray[rand(0, count($imagesArray)-1)]),
        ];

        $companyOtherImages = [ 
            basename($imagesArray[rand(0, count($imagesArray)-1)]),
            basename($imagesArray[rand(0, count($imagesArray)-1)]),
            basename($imagesArray[rand(0, count($imagesArray)-1)]),
        ];

        $companyLogoImage = basename($imagesArray[rand(0, count($imagesArray)-1)]);
        $companyDescriptionImage = basename($imagesArray[rand(0, count($imagesArray)-1)]);

        if (rand(1,2) === 2) //Либо много картинок, либо мало
        {
            return [
                'company_main_banner_images' => $companyMainBannerImages,
                'company_logo_image' => $companyLogoImage,
            ];

        } else
        {
            return [
                'company_main_banner_images' => $companyMainBannerImages,
                'company_logo_image' => $companyLogoImage,
                'company_promo_banner_images' => $companyPromoBannerImages,
                'company_description_image' => $companyDescriptionImage,
                'company_other_images' => $companyOtherImages,
            ];
        }
    }

    /* 
     * Получение рандомной картинки для модели продукта
     */
    private function getModelImage()
    {
        $imagesArray = \Storage::disk( env('FILESYSTEM_DRIVER') )->files('public/seederImages');
       
        $previewImage = basename($imagesArray[rand(0, count($imagesArray)-1)]);

        return $previewImage;
    }

    /* 
     * Получение рандомных картинок для запроса на покупку
     */
    private function getRequestProductImages()
    {
        $imagesArray = \Storage::disk( env('FILESYSTEM_DRIVER') )->files('public/seederImages');

        $detailImages = [ 
            basename($imagesArray[rand(0, count($imagesArray)-1)]),
            basename($imagesArray[rand(0, count($imagesArray)-1)]),
            basename($imagesArray[rand(0, count($imagesArray)-1)]),
        ];

        return [
            'preview_image' => $detailImages[0],
            'detail_images' => $detailImages,
        ];
    }

    private function checkLocaleExist(Profile $profile) : bool
    {
        $uniqueLocales = $profile->profileProperties()->first()
            ? $profile->profileProperties()->where('code','company_name')->whereIn('localisation_id', [1,2,3])->get()->pluck('localisation_id')->toArray()
            : [];
        
        foreach ([1,2,3] as $value)
        {
            if(in_array($value, $uniqueLocales))
            {
                return true;
            }
        }
        return false;
    }
}