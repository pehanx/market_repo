<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use App\User;

class RolesAndPermissionsSeeder extends Seeder
{
    /**
     * Создаём необходимые права, присваиваем ролям их права.
     *
     * @return void
     */
    public function run()
    {
        // Роли и права кэшируются. Сбросим кэш
        app()['cache']->forget('spatie.permission.cache');

        // Создаем права
        Permission::create(['name' => 'list product']);
        Permission::create(['name' => 'create product']);
        Permission::create(['name' => 'edit product']);
        Permission::create(['name' => 'delete product']);
        
        Permission::create(['name' => 'list request_product']);
        Permission::create(['name' => 'create request_product']);
        Permission::create(['name' => 'edit request_product']);
        Permission::create(['name' => 'delete request_product']);
        
        Permission::create(['name' => 'list tender']);
        Permission::create(['name' => 'create tender']);
        Permission::create(['name' => 'edit tender']);
        Permission::create(['name' => 'delete tender']);
        
        Permission::create(['name' => 'participate tender']);
        Permission::create(['name' => 'leave tender']);
        
        Permission::create(['name' => 'list order']);
        Permission::create(['name' => 'create order']);
        Permission::create(['name' => 'edit order']);
        Permission::create(['name' => 'delete order']);
        
        Permission::create(['name' => 'list profile']);
        Permission::create(['name' => 'create profile']);
        Permission::create(['name' => 'edit profile']);
        Permission::create(['name' => 'approve profile']);
        Permission::create(['name' => 'delete profile']);
        
        Permission::create(['name' => 'list manager']);
        Permission::create(['name' => 'create manager']);
        Permission::create(['name' => 'edit manager']);
        Permission::create(['name' => 'delete manager']);
        
        Permission::create(['name' => 'list account']);
        Permission::create(['name' => 'edit account']);
        Permission::create(['name' => 'delete account']);
        
        Permission::create(['name' => 'list message']);
        Permission::create(['name' => 'send message']);
        Permission::create(['name' => 'send admin']);
        
        
        // Создаем роли и присваиваем им права
        $role = Role::create(['name' => 'member']);
        $role->givePermissionTo([
            'list message',
            'send admin',
            'list profile',
            'create profile',
            'list account',
            'edit account',
            'delete account',
        ]);
        
        $role = Role::create(['name' => 'buyer']);
        $role->givePermissionTo([
            'list request_product',
            'create request_product',
            'edit request_product',
            'delete request_product',
            'list tender',
            'create tender',
            'edit tender',
            'delete tender',
            'list profile',
            'create profile',
            'edit profile',
            'delete profile',
            'approve profile',
            'list order',
            'create order', 
            'edit order',
            'delete order',
            'list message',
            'send message',
            'send admin',
            'list manager',
            'create manager',
            'edit manager',
            'delete manager',
            'list account',
            'edit account',
            'delete account',
        ]);
        
        $role = Role::create(['name' => 'seller']);
        $role->givePermissionTo([
            'list product',
            'create product',
            'edit product',
            'delete product',
            'list tender',
            'participate tender',
            'leave tender',
            'list profile',
            'create profile',
            'edit profile',
            'approve profile',
            'delete profile',
            'list message',
            'send message',
            'send admin',
            'list manager',
            'create manager',
            'edit manager',
            'delete manager',
            'list account',
            'edit account',
            'delete account',
        ]);
        
        $role = Role::create(['name' => 'seller_manager']);
        $role->givePermissionTo([
            'list product',
            'create product',
            'edit product',
            'delete product',
            'list tender',
            'participate tender',
            'leave tender',
            'list message',
            'send message',
            'send admin',
            'edit manager',
            'list account',
            'edit account',
            'delete account',
        ]);
        
        $role = Role::create(['name' => 'buyer_manager']);
        $role->givePermissionTo([
            'list tender',
            'create tender',
            'edit tender',
            'delete tender',
            'list order',
            'create order', 
            'edit order',
            'delete order',
            'list message',
            'send message',
            'send admin',
            'edit manager',
            'list account',
            'edit account',
            'delete account',
        ]);

        $role = Role::create(['name' => 'site_manager']);
        $role->givePermissionTo([
            'list product',
            'create product',
            'edit product',
            'delete product',
            'list tender',
            'participate tender',
            'leave tender',
            'list profile',
            'create profile',
            'edit profile',
            'approve profile',
            'delete profile',
            'list message',
            'send message',
            'send admin',
            'list manager',
            'create manager',
            'edit manager',
            'delete manager',
            'list account',
            'edit account',
            'delete account',
        ]);
        
        $role = Role::create(['name' => 'admin']);
        $role->givePermissionTo(Permission::all());

        $user = User::create(
            [
                'name' => 'admin',
                'email' => 'g2r.market@gmail.com',
                'password' => bcrypt('wH3eBDcmUvnUxgUN'),
                'registration_type' => 'native',
            ]
        );
        
        $user->assignRole('admin');
    }
}
