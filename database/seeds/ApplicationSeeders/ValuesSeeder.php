<?php
    $valuesData = [
        'quantities' => [
            
            'ru' => [
                'pcs' => 'шт',
                'bags' => 'коробок',
                'kg' => 'килограмм',
            ], 
            
            'en' => [
                'pcs' => 'pcs',
                'bags' => 'bags',
                'kg' => 'kilogram',
            ],
            
            'cn' => [
                'pcs' => '片',
                'bags' => '盒子',
                'kg' => '公斤',
            ],
        ],
        
        'sizes' => [
            
            'ru' => [
                'mm' => 'мм',
                'cm' => 'см',
                'm' => 'м',
            ], 
            
            'en' => [
                'mm' => 'mm',
                'cm' => 'cm',
                'm' => 'm',
            ],
            
            'cn' => [
                'mm' => '毫米',
                'cm' => '厘米',
                'm' => '米',
            ],
        ],
        
        'weights' => [
            
            'ru' => [
                'gr' => 'грамм',
                'kg' => 'килограмм',
                'ton' => 'тонн',
            ], 
            
            'en' => [
                'gr' => 'gram',
                'kg' => 'kilogram',
                'ton' => 'ton',
            ],
            
            'cn' => [
                'gr' => '克',
                'kg' => '公斤',
                'ton' => '吨',
            ],
        ],
    ];
?>