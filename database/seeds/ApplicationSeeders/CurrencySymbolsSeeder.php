<?php
$symbolsData = [
    'en' => [
        'usd'=>'$',
        'cny'=>'¥',
        'rub'=>'₽'
    ],
    'ru' => [
        'usd'=>'$',
        'cny'=>'¥',
        'rub'=>'₽'
    ],
    'cn' => [
        'usd'=>'$',
        'cny'=>'¥',
        'rub'=>'₽'
    ],
];
?>