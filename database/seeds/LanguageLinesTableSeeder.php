<?php

/* 
 *
 * - cabinet
    * -- import
        * --- Формы form_
        * --- Заголовки таблиц
    * -- export
        * --- Формы form_
        * --- Заголовки таблиц
    * -- account
        * --- Заголовки 
        * --- Формы form_
    * -- manager
        * --- Заголовки 
        * --- Формы form_
    * -- profile
        * --- Заголовки 
        * --- Формы form_
    * -- product
        * --- Заголовки 
        * --- Формы form_
    * -- request_product
        * --- Заголовки 
        * --- Формы form_
    * -- favorite
 * - interface
    * -- header (статика хидера)
    * -- footer (статика футера)
    * -- buttons
    * -- tabs (вкладки)
    * -- filters (фильтры)
    * -- messages (Не зависимые от сущностей и не относящиеся к модалкам)
 * - breadcrumbs
 * - notifications
    * -- Все всплывашки итд
 * - sidebar 
 * - validation (Дефолт)
 * - modals (Сообщения с модалок)
 * - auth (Дефолт)
 * - password (Дефолт)
 * - pagination (Дефолт)
 * 
 * Для отображения полей с формы используется cabinet.сущность.form_название_поля
 */

use Illuminate\Database\Seeder;
use App\Models\Localisation;
use App\Models\LanguageLine;

class LanguageLinesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if(\Redis::command('keys', ['spatie.lang.*']))
        {
            \Redis::command('del', [\Redis::command('keys', ['spatie.lang.*'])]);
        }
        
        LanguageLine::truncate();

        
        /* 
         * Модалки (соббщения для модалок)
         */
        include('LangSeeders/ModalsSeeder.php');

        foreach($modalsData as & $data)
        {
            $data['text'] = json_encode($data['text']);
        }

        LanguageLine::insert($modalsData);

        unset($data);


        /* 
         * Интерфейс (Кнопки, Сообщения, Вкладки, Фильтры, Сообщения (статичные)) 
         */
        include('LangSeeders/InterfaceSeeder.php');

        foreach($interfaceData as & $data)
        {
            $data['text'] = json_encode($data['text']);
        }

        LanguageLine::insert($interfaceData);

        unset($data);

        /* 
         * Уведомления (тосты и прочие всплывашки)
         */
        include('LangSeeders/NotificationSeeder.php');

        foreach($notificationData as & $data)
        {
            $data['text'] = json_encode($data['text']);
        }

        LanguageLine::insert($notificationData);

        unset($data);

        /*
         * Сайдбар
         */
        include('LangSeeders/SidebarSeeder.php');

        foreach($sidebarData as & $data)
        {
            $data['text'] = json_encode($data['text']);
        }

        LanguageLine::insert($sidebarData);

        unset($data);

        /* 
         * Валидация 
         */
        include('LangSeeders/ValidationSeeder.php');

        foreach($validationData as & $data)
        {
            $data['text'] = json_encode($data['text']);
        }

        LanguageLine::insert($validationData);

        unset($data);

        /*
         * Ошибки при обработке паролей
         */
        include('LangSeeders/PasswordSeeder.php');

        foreach($passwordData as & $data)
        {
            $data['text'] = json_encode($data['text']);
        }

        LanguageLine::insert($passwordData);

        unset($data);

        /* 
         * Ошибки при авторизации
         */
        include('LangSeeders/AuthSeeder.php');

        foreach($authData as & $data)
        {
            $data['text'] = json_encode($data['text']);
        }

        LanguageLine::insert($authData);

        unset($data);

        /* 
         * Кабинет (Профили, продукты, запросы и формы которые относятся к этим сущностям)
         */
        include('LangSeeders/CabinetSeeder.php');

        foreach($cabinetData as & $data)
        {
            $data['text'] = json_encode($data['text']);
        }

        LanguageLine::insert($cabinetData);

        unset($data);

        /* 
         * Хлебные крошки
         */
        include('LangSeeders/BreadcrumbSeeder.php');

        foreach($breadcrumbsData as & $data)
        {
            $data['text'] = json_encode($data['text']);
        }

        LanguageLine::insert($breadcrumbsData);

        unset($data);

        /* 
         * Пагинация
         */
        include('LangSeeders/PaginationSeeder.php');

        foreach($paginateData as & $data)
        {
            $data['text'] = json_encode($data['text']);
        }

        LanguageLine::insert($paginateData);

        unset($data);
        
        /* 
         * Админка сайта
         */
        include('LangSeeders/AdminSeeder.php');

        foreach($adminData as & $data)
        {
            $data['text'] = json_encode($data['text']);
        }

        LanguageLine::insert($adminData);

        unset($data);

    }
}


//Перепроверить модалки
//Распарсить grt
