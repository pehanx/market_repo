<?php

use Illuminate\Database\Seeder;
use App\Models\ProfileType;

class ProfileTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        ProfileType::create(
        [
            'code' => 'buyer',
        ]);
		
		ProfileType::create(
        [
            'code' => 'seller',
        ]);
        
        ProfileType::create(
        [
            'code' => 'account',
        ]);
    }
}
