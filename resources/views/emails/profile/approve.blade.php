@component('mail::message')

{{ __('mails.messages.user_need_approve_profile', ['name' => $user->name]) }}:

@component('mail::button', ['url' => route('admin.profiles.index', ['id' => $profile->id])])
{{ __('mails.approve')}}
@endcomponent

@endcomponent
