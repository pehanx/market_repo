@component('mail::message')

<p>
    Новая заявка:<br><br>

    Страница: {{$data['product_link']}}

    Товар: {{$data['product_name']}}
    
    Заказчик: {{$data['company_name']}}
    Почта: {{$data['email']}}
    Телефон: {{$data['phone']}}

</p>

@endcomponent