@component('mail::message')

    Здравствуйте, {{ $user['name'] }},
<p>
    Вас пригласили на {{ config('app.name') }} 
</p>

@component('mail::button', ['url' => route('invite.login', ['invite_token' => $user['invite_token'] ]) , 'color' => 'blue'])
    Войти
@endcomponent

@endcomponent