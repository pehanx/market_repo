@extends('catalog.home')

@section('title')
    {{ $productMetaTitle }} - {{ __('interface.meta.buy_on_market') }}
@endsection

@section('meta')
    <meta name="description" content="{{ $productMetaDescription }}">
	<meta name="title" content="{{ $productMetaTitle }}">
	@isset($product->downloads['preview_image'])
	<meta property="og:image" content="{{ asset('storage/'.$product->downloads['preview_image'][0]->path) }}">
	@endisset
	<meta property="og:title" content="{{ $productMetaTitle }}">
@endsection

@section('catalog-content')
<div class="product">
	<div class="container">
		@section('breadcrumbs', Breadcrumbs::render())
        @yield('breadcrumbs')
		<h2 class="product__title">{{ $product->productProperties->where('code', 'name')->first()->value }}</h2>
		<div class="product__card">
			<div class="product__card__blockImage">
				<div class="product__card__image owl-carousel owl-theme" data-libraries="lightgallery">
                    @isset($product->downloads['preview_image'])
                        @foreach($product->downloads['preview_image'] as $preview_image)
                        <div 
                            data-src="{{ asset('storage/'.$preview_image->path) }}"
                            class="product__card__image__item" 
                            style="background-image: url('{{ asset('storage/'.$preview_image->path) }}');"
                        >
                            <img class="lightgallery-slider" src="{{ asset('storage/'.$preview_image->path) }}">
                        </div>
                        @endforeach
					@endisset
                    @isset($product->downloads['detail_images'])
                        @foreach($product->downloads['detail_images'] as $detailImage)
                        <div 
                            data-src="{{ asset('storage/'.$detailImage->path) }}"
                            class="product__card__image__item" 
                            style="background-image: url('{{ asset('storage/'.$detailImage->path) }}');"
                        >
                            <img class="lightgallery-slider" src="{{ asset('storage/'.$detailImage->path) }}">
                        </div>
                        @endforeach
					@endisset
                    @if($product->productModels->first())
                        @foreach($product->productModels as $productModel)
                        <div 
                            data-src="{{ asset('storage/'.$productModel->downloads->where('type', 'preview_image')->first()->path) }}"
                            class="product__card__image__item" 
                            style="background-image: url('{{ asset('storage/'.$productModel->downloads->where('type', 'preview_image')->first()->path) }}');"
                        >
                            <img class="lightgallery-slider" src="{{ asset('storage/'.$productModel->downloads->where('type', 'preview_image')->first()->path) }}">
                        </div>
                        @endforeach
                    @endif
				</div>
				<div class="product__card__imageControl owl-carousel owl-theme">
                    @isset($product->downloads['preview_image'])
                        @foreach($product->downloads['preview_image'] as $previewImageKey => $previewImageSlider)
                        <div class="product__card__imageControl__item" data-go-slider="{{ $previewImageKey }}" style="background-image: url('{{ asset('storage/'.$previewImageSlider->path) }}');"></div>
                        @endforeach
                    @endisset
                    @isset($product->downloads['detail_images'])
                        @foreach($product->downloads['detail_images'] as $detailImageKey => $detailImageSlider)
                        <div class="product__card__imageControl__item" data-go-slider="{{ (isset($previewImageKey) ? 1 : 0) + $detailImageKey }}" style="background-image: url('{{ asset('storage/'.$detailImageSlider->path) }}');"></div>
                        @endforeach
                    @endisset
                    @if($product->productModels->first())
                        @foreach($product->productModels as $productModelKey => $productModelSlider)
                        <div 
                            class="product__card__imageControl__item" 
                            data-go-slider="{{ (isset($previewImageKey) ? 1 : 0) + ($detailImageKey ?? 0) + ++$productModelKey }}" 
                            style="background-image: url('{{ asset('storage/'.$productModelSlider->downloads->where('type', 'preview_image')->first()->path) }}');"
                        >
                        </div>
                        @endforeach
                    @endif
				</div>
			</div>
			<div class="product__card__blockInformation">
				<div id="product__main__info">
					<div class="product__card__id">{{ __('interface.catalog.articul') }}: {{ $product->articul }}</div>
					<div class="product__card__price">
						<div class="product__card__price__label">{{ __('interface.catalog.price') }}</div>
						@php 
							$prices = $product->productPrices->first() ;
							$currency = $currencyRates->where('code', $currentCurrencyCode)->first();
						@endphp
						<div class="product__card__price__value">{{ $prices->min * $currency->rate }}{{ $currency->symbol }} {{ ( $prices->max * $currency->rate )? ' — '. ( $prices->max * $currency->rate ) . $currency->symbol : '' }}</div>
					</div>
					<div class="product__card__count">
						<div class="product__card__count__label">{{ __('interface.catalog.minimal_order') }}</div>
						<div class="product__card__count__value">{{ $product->characteristics['min_order']['value'] ?? '—' }} {{ $product->characteristics['min_order']['type'] ?? '' }}</div>
					</div>
					<div class="product__card__available">
						<div class="product__card__available__label">{{ __('interface.catalog.available') }}</div>
						<div class="product__card__available__value">{{ $product->characteristics['available']['value'] ?? '—'}} {{ $product->characteristics['available']['type'] ?? ''}}</div>
					</div>
					<div class="product__card__color">
						<div class="product__card__color__label">{{ __('cabinet.product.form_color_product') }}</div>
						<div class="product__card__color__value">
							<span class="simplecolorpicker icon" title="{{ $product->characteristics['color']['value'] ?? '—' }}" style="background-color: {{ $product->characteristics['color']['type'] }};"></span>
						</div>
					</div>
				</div>
                
				@if($product->productModels->first())
				@foreach($product->productModels as $productModel)
				<div class="product__model__info" data-model="{{$productModel->id}}" style="display: none">
					<div class="product__card__id">{{ __('interface.catalog.articul') }}: {{ $product->articul .'-'. $productModel->articul }}</div>
					<div class="product__card__price">
						<div class="product__card__price__label">{{ __('interface.catalog.price') }}</div>
						@php 
							$modelPrices = $productModel->productPrices->first();
						@endphp
						<div class="product__card__price__value">{{ $modelPrices->min * $currency->rate }}{{ $currency->symbol }} {{ 
							( $modelPrices->max * $currency->rate )
							? ' — '. ($modelPrices->max * $currency->rate) . $currency->symbol
							: ( ($prices->max * $currency->rate) ? ' — '. ($prices->max * $currency->rate) . $currency->symbol : '' )}}</div>
					</div>
					<div class="product__card__count">
						<div class="product__card__count__label">{{ __('interface.catalog.minimal_order') }}</div>
						<div class="product__card__count__value">{{ $product->characteristics['min_order']['value'] ?? '—' }} {{ $product->characteristics['min_order']['type'] ?? '' }}</div>
					</div>
					<div class="product__card__available">
						<div class="product__card__available__label">{{ __('interface.catalog.available') }}</div>
						<div class="product__card__available__value">{{ $productModel->characteristics['available']['value'] ?? '—'}} {{ $productModel->characteristics['available']['type'] ?? ''}}</div>
					</div>
					<div class="product__card__color">
						<div class="product__card__color__label">{{ __('cabinet.product.form_color_product') }}</div>
						<div class="product__card__color__value">
							<span class="simplecolorpicker icon" title="{{ $productModel->characteristics['color']['value'] }}" style="background-color: {{ $productModel->characteristics['color']['type'] }};"></span>
						</div>
					</div>
				</div>
				@endforeach
                <div class="product__card__model">
                    <div class="product__card__model__label">{{ __('interface.catalog.product_models') }}</div>
                    <div class="product__card__model__items">
                        @foreach($product->productModels as $productModelKey => $productModelSlider)
                        <div 
							class="product__card__model__items__element"
							data-id="{{$productModelSlider->id}}"
                            data-go-slider="{{ (isset($previewImageKey) ? 1 : 0) + ($detailImageKey ?? 0) + ++$productModelKey }}" 
                            style="background-image: url('{{ asset('storage/'.$productModelSlider->downloads->where('type', 'preview_image')->first()->path) }}');"
                        >
                        </div>
                        @endforeach
                    </div>
				</div>
                @endif
			</div>
            
			<div class="product__card__blockContact">
				<div class="product__card__name">
					<div class="product__card__name__label">{{ __('interface.catalog.supplier') }}</div>
					<a target="_blank" href="{{ $product->shop_link }}">
                        <div class="product__card__name__value">
                            {{ $product->profiles->profileProperties->where('code', 'company_name')->first()->value ?? '' }}
                        </div>
                    </a>
				</div>
				<div class="product__card__warranty">
					<div class="product__card__warranty__element">
						<div class="product__card__warranty__element__icon product__card__warranty__element__icon-warranty1"></div>
						<div class="product__card__warranty__element__info">
							<div class="product__card__warranty__element__info__title">{{ __('interface.catalog.supplier_warranty_one') }}</div>
							<div class="product__card__warranty__element__info__description">{{ __('interface.catalog.supplier_warranty_two') }}</div>
						</div>
					</div>
					<div class="product__card__warranty__element">
						<div class="product__card__warranty__element__icon product__card__warranty__element__icon-warranty2"></div>
						<div class="product__card__warranty__element__info">
							<div class="product__card__warranty__element__info__title">{{ __('interface.catalog.supplier_warranty_three') }}</div>
							<div class="product__card__warranty__element__info__description">{{ __('interface.catalog.supplier_warranty_four') }}</div>
						</div>
					</div>
					{{--<div class="product__card__warranty__element">
						<div class="product__card__warranty__element__icon product__card__warranty__element__icon-warranty3"></div>
						<div class="product__card__warranty__element__info">
							<div class="product__card__warranty__element__info__title">Делаем сложное простым</div>
							<div class="product__card__warranty__element__info__description">Удобный в работе сервис</div>
						</div>
					</div>
                    --}}
				</div>
                
                @guest 
                <form method="POST" class="favorite-add-form" action="{{ route('favorite.add', ['type' => 'product', 'id' => $product->id]) }}">
                    @csrf
                    <div class="product__card__favorites">
                        <div class="product__card__favorites__icon"></div>
                        <div class="product__card__favorites__text">{{ __('interface.buttons.favorite_add') }}</div>
                    </div>
                </form>
                @endguest
                
                @auth 
                    @if( $product->favorites->where('user_id', $user->id)->first() )
                        <div class="product__card__favorites favorite-remove" data-route="{{ route('favorite.remove', ['type' => 'product', 'id' => $product->id]) }}">
                            <div class="product__card__favorites__icon active"></div>
                            <div class="product__card__favorites__text">{{ __('interface.buttons.favorite_remove') }}</div>
                        </div>
                    @else
                        <div class="product__card__favorites favorite-add"  data-route="{{ route('favorite.add', ['type' => 'product', 'id' => $product->id]) }}">
                            <div class="product__card__favorites__icon"></div>
                            <div class="product__card__favorites__text">{{ __('interface.buttons.favorite_add') }}</div>
                        </div>
					@endif
					
					@if ( session()->has('active_profile') )
						@if(!$isContact && !$myProfile)
							<div data-route-contact='{{ route('contact.add', ['contact'=>$supplier]) }}' class="product__card__contacts contact-add">
							</div>
						@endif
					@else
						<form method="POST" class="contact-add-form" action="{{ route('contact.add', ['contact'=>$supplier]) }}">
							@csrf
							<div class="product__card__contacts">
							</div>
						</form>
					@endif
				@endauth
				@guest
					<form method="POST" class="contact-add-form" action="{{ route('contact.add', ['contact'=>$supplier]) }}">
						@csrf
						<div class="product__card__contacts">
						</div>
					</form>
				@endguest
				@if(!$myProfile)
                    <div class="product__card__message">
						<a href="{{ route('cabinet.chat.member', ['member'=>$supplier]) }}" class="button button-primary button-inline">{{ __('interface.buttons.write_to_supplier') }}</a>
					</div>
				@endif
				</div>
			</div>
			
		<div class="product__cardDescription">
			<h3 class="product__cardDescription__title">{{ __('interface.catalog.product_describle') }}</h3>
			@if($product->characteristics->isNotEmpty() || $product->productProperties->isNotEmpty())
			<div class="product__cardDescription__text__subtitle">{{ __('interface.catalog.characteristics') }}</div>
			<div class="product__cardDescription__text__columns">
				@isset($product->characteristics)
				@php
				$sizeProduct = (isset($product->characteristics['lenght']) ? $product->characteristics['lenght']['value'] : 0) . ' x ' .
					(isset($product->characteristics['width']) ? $product->characteristics['width']['value'] : 0) . ' x ' . 
					(isset($product->characteristics['height']) ? $product->characteristics['height']['value'] : 0);

				$sizeType = $product->characteristics['lenght']['type'] ??($product->characteristics['height']['type'] ?? ($product->characteristics['width']['type'] ?? 'mm'));
				@endphp
				@if($sizeProduct !== '0 x 0 x 0')
				<div class="product__cardDescription__text__columns__element">
					<div class="product__cardDescription__text__columns__element__title">{{ __('interface.catalog.size') }}</div>
					<div class="product__cardDescription__text__columns__element__value">{{ $sizeProduct }} {{ $sizeType }}</div>
				</div>
				@endif
				@if(isset($product->characteristics['weight']))
				<div class="product__cardDescription__text__columns__element">
					<div class="product__cardDescription__text__columns__element__title">{{__('cabinet.product.form_weight')}}</div>
					<div class="product__cardDescription__text__columns__element__value">{{ $product->characteristics['weight']['value'] }} {{ $product->characteristics['weight']['type'] }}</div>
				</div>
				@endif
				@endisset
				@if(isset($product->characteristics['color']))
				@php
					$prodColors = $product->characteristics['color']['value'];

					if($product->productModels->isNotEmpty())
					{
						$modelColors = [$product->characteristics['color']['value']];

						foreach ($product->productModels as $prodModel)
						{
							array_push($modelColors, $prodModel->characteristics['color']['value']);
						}

						$prodColors = implode(' / ', $modelColors);
					}

				@endphp
				<div class="product__cardDescription__text__columns__element">
					<div class="product__cardDescription__text__columns__element__title">{{ __('cabinet.product.form_color_product') }}</div>
					<div class="product__cardDescription__text__columns__element__value">{{$prodColors}}</div>
				</div>
				@endif
				@if($product->productProperties->where('code', 'certification')->first())
				<div class="product__cardDescription__text__columns__element">
					<div class="product__cardDescription__text__columns__element__title">{{__('cabinet.product.form_certification')}}</div>
					<div class="product__cardDescription__text__columns__element__value">{{$product->productProperties->where('code', 'certification')->first()->value}}</div>
				</div>
				@endif
				@if($product->productProperties->where('code', 'shelf_life')->first())
				<div class="product__cardDescription__text__columns__element">
					<div class="product__cardDescription__text__columns__element__title">{{__('cabinet.product.form_shelf_life')}}</div>
					<div class="product__cardDescription__text__columns__element__value">{{$product->productProperties->where('code', 'shelf_life')->first()->value}}</div>
				</div>
				@endif
				@if($product->productProperties->where('code', 'package_type')->first())
				<div class="product__cardDescription__text__columns__element">
					<div class="product__cardDescription__text__columns__element__title">{{__('cabinet.product.form_package_type')}}</div>
					<div class="product__cardDescription__text__columns__element__value">{{$product->productProperties->where('code', 'package_type')->first()->value}}</div>
				</div>
				@endif
				{{-- <div class="product__cardDescription__text__columns__element">
					<div class="product__cardDescription__text__columns__element__title">Процессор</div>
					<div class="product__cardDescription__text__columns__element__value">Устройство, док-станция Qualcomm®Львиный зев™855 Plus (Восьмиядерный, 7nm, до 2,96 ГГц), С устройство, док-станция Qualcomm AI двигателя</div>
				</div>
				<div class="product__cardDescription__text__columns__element">
					<div class="product__cardDescription__text__columns__element__title">Графический процессор</div>
					<div class="product__cardDescription__text__columns__element__value">Adreno 640</div>
				</div>
				<div class="product__cardDescription__text__columns__element">
					<div class="product__cardDescription__text__columns__element__title">Аудио</div>
					<div class="product__cardDescription__text__columns__element__value">Двойной стерео колонки, шум отмены поддержка, Dolby Atmos</div>
				</div>
				<div class="product__cardDescription__text__columns__element">
					<div class="product__cardDescription__text__columns__element__title">Коробка для хранения</div>
					<div class="product__cardDescription__text__columns__element__value">Коробка для хранения</div>
				</div>
				<div class="product__cardDescription__text__columns__element">
					<div class="product__cardDescription__text__columns__element__title">Оперативная память</div>
					<div class="product__cardDescription__text__columns__element__value">8 ГБ/12 ГБ LPDDR4X</div>
				</div>
				<div class="product__cardDescription__text__columns__element">
					<div class="product__cardDescription__text__columns__element__title">Коробка для хранения</div>
					<div class="product__cardDescription__text__columns__element__value">Коробка для хранения</div>
				</div>
				<div class="product__cardDescription__text__columns__element">
					<div class="product__cardDescription__text__columns__element__title">Датчики давления</div>
					<div class="product__cardDescription__text__columns__element__value">В-дисплей отпечатков пальцев Сенсор, акселерометр, электронный компас, гироскоп, датчик окружающего светильник Сенсор, близость, Сенсор, Сенсор Core, лазерная гравировка машины Сенсор</div>
				</div>
				<div class="product__cardDescription__text__columns__element">
					<div class="product__cardDescription__text__columns__element__title">Порты</div>
					<div class="product__cardDescription__text__columns__element__value">USB 3,1 GEN1, type-C, поддержка стандартных наушников type-C Двойной слот nano-SIM</div>
				</div>
				<div class="product__cardDescription__text__columns__element">
					<div class="product__cardDescription__text__columns__element__title">Батарея</div>
					<div class="product__cardDescription__text__columns__element__value">4085 мАч (несъемный)<br>Warp заряжайте 30 T быстрой зарядки (5 В/6A)</div>
				</div>
				<div class="product__cardDescription__text__columns__element">
					<div class="product__cardDescription__text__columns__element__title">Кнопки</div>
					<div class="product__cardDescription__text__columns__element__value">Жесты и поддержка навигации на экране<br>Оповещение о слайдер</div>
				</div> --}}
			</div>
			<div class="product__cardDescription__text__spacer"></div>
			@endif
			<div class="product__cardDescription__text__subtitle">Описание</div>
			<div class="product__cardDescription__text">
				<p class="product__cardDescription__text__paragraph">
                    {{ $product->productProperties->where('code', 'description')->first()->value ?? '' }}
                </p>
                @isset($product->youtube_frame)
				<div class="product__cardDescription__text__video">
                    {!! $product->youtube_frame !!}
				</div>
                <div class="product__cardDescription__text__spacer"></div>
                @endisset

                @isset($product->downloads['describle_images'])
                    <div data-libraries="lightgallery" class="lg-catalog-product">
                        @foreach($product->downloads['describle_images'] as $describleImage)
                        <div data-src="{{ asset('storage/'.$describleImage->path) }}" class="product__cardDescription__text__image">
                            <img src="{{ asset('storage/'.$describleImage->path) }}" width="350px", height="350px">
                        </div>
                        @endforeach
                    </div>
                @endisset
					
                {{-- <div class="product__cardDescription__text__subsubtitle">
					Невероятное качество товара 1
				</div>
				<div class="product__cardDescription__text__image">
					<img src="dist/assets/img/temporary/product-desc-image-1.jpg" alt="">
				</div>
				<div class="product__cardDescription__text__subsubtitle">
					Какой-то текст о невероятных плюсах товара
				</div>
				<p class="product__cardDescription__text__paragraph">LED-телевизор LG 43UK6300PLB оснащен простой и удобной операционной системой WebOS и функцией Smart TV, которая дает возможность установки дополнительных программ, а также воспроизведения видеоресурсов интернета. Дисплей диагональю 43 дюйма характеризуется сверхвысокой четкостью разрешения стандарта Ultra HD.</p>
				<div class="product__cardDescription__text__image">
					<img src="dist/assets/img/temporary/product-desc-image-2.jpg" alt="">
				</div>
				<div class="product__cardDescription__text__subsubtitle">
					Какой-то текст о невероятных плюсах товара + узкие фото
				</div>
				<p class="product__cardDescription__text__paragraph">LED-телевизор LG 43UK6300PLB оснащен простой и удобной операционной системой WebOS и функцией Smart TV</p>
				<div class="product__cardDescription__text__image">
					<img src="dist/assets/img/temporary/product-desc-image-3.jpg" alt="">
				</div>
				<div class="product__cardDescription__text__subsubtitle">
					Какой-то текст о невероятных плюсах товара + узкие фото
				</div>
				<div class="product__cardDescription__text__image">
					<img src="dist/assets/img/temporary/product-desc-image-4.jpg" alt="">
				</div> --}}
				<div class="product__cardDescription__text__spacer"></div>
				{{-- 
				<div class="product__cardDescription__text__subtitle">Сервис</div>
				<div class="product__cardDescription__text__subsubtitle">Гарантия качества</div>
				<p class="product__cardDescription__text__paragraph">Все товары на Market сертифицированы для продажи в России и закупаются y официальных поставщиков.</p>
				<div class="product__cardDescription__text__subsubtitle">Возврат товара</div>
				<p class="product__cardDescription__text__paragraph product__cardDescription__text__paragraph-star">Вы можете вернуть товар без объяснения причин в течение 15 дней с момента получения.</p>
				<p class="product__cardDescription__text__paragraph product__cardDescription__text__paragraph-graystar">Пpи уcлoвии coxpaнeния тoвapнoгo видa и цeлocтнocти упaкoвки, зa иcключeниeм тoвapoв нaдлeжaщeгo кaчecтвa из пepeчнeй, утвepждeнныx Пocтaнoвлeниями Пpaвитeльcтвa № 55 oт 19.01.1998 и № 942 oт 10.11.2011 (в т. ч. Тoвapы личнoй гигиeны и пo уxoду зa peбeнкoм, тpикoтaж, пocудa и дp.), и тoвapoв имеющиx индивидуaльнo-oпpеделенные cвoйcтвa.</p>
				<div class="product__cardDescription__text__subsubtitle">Что-то еще</div>
				<p class="product__cardDescription__text__paragraph">Все товары на Market сертифицированы для продажи в России и закупаются y официальных поставщиков.</p>
				<div class="product__cardDescription__text__spacer"></div>
				<div class="product__cardDescription__text__subtitle">Логистический сервис</div>
				<p class="product__cardDescription__text__paragraph">LED-телевизор LG 43UK6300PLB оснащен простой и удобной операционной системой WebOS и функцией Smart TV, которая дает возможность установки дополнительных программ, а также воспроизведения видеоресурсов интернета. Дисплей диагональю 43 дюйма характеризуется сверхвысокой четкостью разрешения стандарта Ultra HD.</p> --}}
				
                @if($product->similarProducts->first())
				<div class="product__cardDescription__text__subtitle">{{ __('interface.catalog.popular_supplier_products') }}</div>
				<div class="product__cardDescription__text__items">
                    @foreach($product->similarProducts as $similarProduct)
					<div class="product__cardDescription__text__items__item">
                        <a href="{{ route('catalog.product.show', ['product' => $similarProduct->id]) }}">
                            <div 
                                class="product__cardDescription__text__items__item__image" 
                                style="background-image: url('{{ asset('storage/' . $similarProduct->downloads->where('type', 'preview_image')->first()->path) }}');"
                            >
                            </div>
                            <div class="product__cardDescription__text__items__item__price">
                                {{ ( $similarProduct->productPrices->first()->min * $currency->rate ) }}{{ $currency->symbol }}
                                {{ ( $similarProduct->productPrices->first()->max * $currency->rate ) ? ' — ' . ( $similarProduct->productPrices->first()->max * $currency->rate ) . $currency->symbol : '' }}
                            </div>
                        </a>
                    </div>
					@endforeach
				</div>
				@endif
				
			</div>
		</div>
	</div>
</div>
@endsection