@extends('catalog.home')

@section('search')
    @include ('widgets.search', ['route' => '', 'page' => 'catalog'])
@endsection

@section('title')
    {{ $categoryMetaTitle }}
@endsection

@section('meta')
    <meta name="description" content="{{ $categoryMetaDescription }}">
    <meta name="title" content="{{ $categoryMetaTitle }}">
@endsection
 
@section('catalog-content')
<div class="catalog">
	<div class="container">
        @section('breadcrumbs', Breadcrumbs::render())
        @yield('breadcrumbs')
		<h2 class="catalog__title">{{ $categoryName }}</h2>
		<div class="catalog__filterTop">
        
			<a href="{{ $back->url }}" class="catalog__filterTop__back" title="{{ $back->title }}"></a>

			<div class="catalog__filterTop__area">
                <div class="catalog__filterTop__sort">
                    <select name="order_by" id='order-by' class="catalog__filterTop__sort__select">
                        <option {{ (request('order_by') == 'shows_asc')? 'selected' : '' }} value="shows_asc">{{ __('interface.sort.shows') }} &nbsp;&#xf160;</option>
                        <option {{ (request('order_by') == 'shows_desc')? 'selected' : '' }} value="shows_desc">{{ __('interface.sort.shows') }}&nbsp;&#xf161;</option>
                        <option {{ (request('order_by') == 'price_asc')? 'selected' : '' }} value="price_asc">{{ __('interface.sort.price') }} &nbsp;&#xf160;</option>
                        <option {{ (request('order_by') == 'price_desc')? 'selected' : '' }} value="price_desc">{{ __('interface.sort.price') }}&nbsp;&#xf161;</option>
                        <option {{ (request('order_by') == 'rating_asc')? 'selected' : '' }} value="rating_asc">{{ __('interface.sort.rating') }}&nbsp;&#xf160;</option>
                        <option {{ (request('order_by') == 'rating_desc')? 'selected' : '' }} value="rating_desc">{{ __('interface.sort.rating') }}&nbsp;&#xf161;</option>
                    </select>
                </div>
            </div>
            
		</div>
        
		<div class="catalog__content">
			
            @include ('catalog.product._filters', ['categoryDescendants' => $categoryDescendants ?? null, 'countries' => $countries ?? null])

			<div class="catalog__content__right">
				<div class="catalog__products">
                
                    @php
                        $currency = $currencyRates->where('code', $currentCurrencyCode)->first();
                    @endphp

					@foreach($products as $product)
                        <div class="catalog__products__item"> 
                            <a href="{{ route('catalog.product.show', ['product' => $product->id]) }}" class="catalog__products__item__image"  style="background-image: url('{{ asset('storage/' . $product->downloads->where('type', 'preview_image')->first()->path) }}');">
                                
                                @guest 
                                <form method="POST" class="favorite-add-form" action="{{ route('favorite.add', ['type' => 'product', 'id' => $product->id]) }}">
                                    @csrf
                                    <div class="catalog__products__item__favorite" title="{{ __('interface.title.add_to_favorites') }}"></div>
                                </form>
                                @endguest 
                                
                                @auth 
                                    @if( $product->favorites->where('user_id', $user->id)->first() )
                                        <div class="catalog__products__item__favorite active favorite-remove" data-route="{{ route('favorite.remove', ['type' => 'product', 'id' => $product->id]) }}" title="{{ __('interface.title.remove_to_favorites') }}"></div>
                                    @else
                                        <div class="catalog__products__item__favorite favorite-add" data-route="{{ route('favorite.add', ['type' => 'product', 'id' => $product->id]) }}" title="{{ __('interface.title.add_to_favorites') }}"></div>
                                    @endif
                                @endauth
         
                            </a>
                            <div class="catalog__products__item__title">{{ $product->productProperties->where('code', 'name')->first()->value ?? '' }}</div>
                            <div class="catalog__products__item__price">
                                {{ ( $product->productPrices->first()->min * $currency->rate ) }}{{$currency->symbol}}
                                {{ ( $product->productPrices->first()->max * $currency->rate ) ? ' — ' . ( $product->productPrices->first()->max * $currency->rate ) . " $currency->symbol" : '' }}
                            </div>
                            @if(!empty($product->characteristics['min_order']))
                            <div class="catalog__products__item__minOrder">
                                {{ $product->characteristics['min_order']['value'] }} 
                                {{ $product->characteristics['min_order']['type'] }}
                                <span>{{ __('interface.catalog.min_order') }}</span>
                            </div>
                            @endif
                            <a href="{{ $product->shop_link }}" class="catalog__products__item__salerLink" target="_blank">{{ __('interface.catalog.all_sulppier_products') }}</a>
                        </div>
                    @endforeach
                    
				</div>
                
                {{ $products->render('widgets.pagination') }}
                
			</div>
		</div>
	</div>
</div>
@endsection

@section('content-script')
<script>
    document.addEventListener('DOMContentLoaded', function() {
        /* $('select[name="color"]').simplecolorpicker({
            picker: true,
            theme: 'fontawesome'
        }); */
        
        /* $('.select2.countries').select2({
            placeholder: "Выберите страну",
            allowClear: true
        }); */ 
        
        /* $('.catalog__categories__block__links__showAll').on('click', function(e){
            $(this).closest('ul').find("li[style='display:none;']").toggle();
            $(this).toggle();
        }); */
    });
</script>
@endsection