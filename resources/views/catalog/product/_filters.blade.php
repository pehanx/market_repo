{{--
<div class="row mt-4"> 
    <div class="col-12">
        <label>Страна производитель</label>
        <select class="select2 countries w-100" name="country">
            <option></option>
            @foreach($countries as $country)
            <option {{ (request('country') == $country['id']) ? 'selected' : '' }} value="{{ $country['id'] }}">{{ $country['text'] }}</option>
            @endforeach
        </select>
    </div>
</div>
--}}

<div class="catalog__content__left">
    @if($categoryDescendants && $categoryDescendants->first())
    <div class="catalog__filter catalog__categories">
        @foreach($categoryDescendants as $categoryDescendant)
        <div class="catalog__categories__block">
            @if($categoryDescendant->children->first())
            <div class="catalog__categories__block__title showAll"> {{ $categoryDescendant->categoryProperties->where('code', 'name')->first()->value }}</div>
            <div class="catalog__categories__block__links">
                <ul>
                    @foreach($categoryDescendant->children as $key => $categoryChildren)
                        @if($key <= 4)
                        <li>
                            <a href="{{ $categoryChildren->link }}">{{ $categoryChildren->categoryProperties->where('code', 'name')->first()->value }}</a>
                        </li>
                        @elseif($key > 4)
                        <li class="overflow-links d-none">
                            <a href="{{ $categoryChildren->link }}">{{ $categoryChildren->categoryProperties->where('code', 'name')->first()->value }}</a>
                        </li>
                        @endif
                    @endforeach
                    
                    @if($categoryDescendant->children->count() > 5)
                        <li><a class="catalog__categories__block__links__showAll" data-text-show="{{  __('interface.buttons.show_completely') }}" data-text-hide="{{  __('interface.buttons.hide_completely') }}">{{  __('interface.buttons.show_completely') }}</a></li>
                    @endif
                </ul>
            </div>
            @else
            <div class="catalog__categories__block__title">
                <a href="{{ $categoryDescendant->link }}">
                    {{ $categoryDescendant->categoryProperties->where('code', 'name')->first()->value }}
                </a>
            </div>
            @endif 
        </div>
        @endforeach
    </div>
    @else
    <form id="filters">
        <div class="catalog__filter catalog__digitalFilter">
            <div class="catalog__digitalFilter__title">{{ __('interface.filters.price') }}</div>
            <div class="catalog__digitalFilter__inputs">
                <div class="catalog__digitalFilter__inputs__input">
                    <span>{{ __('interface.filters.from') }}</span> 
                    <input type="text" name="min_price" placeholder="0" value="{{ old('min_price', (request('min_price'))?? '') }}">
                </div>
                <div class="catalog__digitalFilter__inputs__input">
                    <span>{{ __('interface.filters.to') }}</span> 
                    <input type="text" name="max_price" placeholder="1000000" value="{{ old('max_price', (request('max_price'))?? '') }}">
                </div>
            </div>
        </div>

        <div class="catalog__filter catalog__filterColors">
            <div class="catalog__filterColors__title">{{ __('interface.filters.product_color') }}</div>
            <div class="catalog__filterColors__content">
                <select name="color" class="colorpicker-handle">
                    @foreach($colors as $color)
                    <option {{(request('color') === $color->code) ? 'selected' : ''}} value="{{ $color->code }}">{{ $color->value }}</option>
                    @endforeach
                </select>
            </div>    
        </div>
        <button type="button" class="catalog__filter catalog__filter__button">{{ __('interface.buttons.find_products') }}</button>
    </form>
    @endif
</div>