@extends('layouts.market.index')

@section('content')
<div class="categories">
    <div class="container">
        @section('breadcrumbs', Breadcrumbs::render())
        @yield('breadcrumbs')
        <h2 class="categories__title">{{ __('interface.catalog.catalog')}}</h2>
    <div class="categories__selector">
        <div class="categories__selector__all" data-toggled='true'>{{ __('interface.catalog.all_categories')}}</div>
            <div class="categories__selector__liters">
                <div class="categories__selector__liters__block">
                @foreach($allGroups as $allGroupsItem)
                    <div class="categories__selector__liters__item categories__selector__liters__item__disabled">{{ $allGroupsItem }}</div>
                @endforeach
            </div>
        </div>
    </div>
    <div class="categories__content">
        @foreach($categories as $group => $collection)
            <div class="categories__liter" data-liter="{{ mb_strtoupper($group) }}">
                <div class="categories__liter__title">{{ mb_strtoupper($group) }}</div>
                <div class="categories__liter__content">
                    @if($collection->first())
                        @foreach($collection as $categoryDepthOne)
                            <div class="categories__element">
                                <div class="categories__element__title">
                                    <a href="{{ $categoryDepthOne->link }}">{{ $categoryDepthOne->categoryProperties->where('code', 'name')->first()->value }}</a>
                                </div>
                                <div class="categories__element__content">
                                    <div class="categories__catalog">
                                        @if($categoryDepthOne->children->first())
                                            @foreach($categoryDepthOne->children as $keyDepthTwo => $categoryDepthTwo)
                                                <div class="categories__catalog__title">
                                                    <a href="{{ $categoryDepthTwo->link }}">{{ $categoryDepthTwo->categoryProperties->where('code', 'name')->first()->value }}</a>
                                                </div>
                                                <div class="categories__catalog__content">
                                                    <ul>
                                                        @if($categoryDepthTwo->children->first())
                                                            @foreach($categoryDepthTwo->children as $keyDepthTree => $categoryDepthTree)
                                                                <li><a href="{{ $categoryDepthTree->link }}">{{ $categoryDepthTree->categoryProperties->where('code', 'name')->first()->value }}</a></li>
                                                            @endforeach
                                                        @endif
                                                    </ul>
                                                </div>
                                            @endforeach
                                        @endif
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    @endif
                </div>
            </div>
        @endforeach
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script>
document.addEventListener('DOMContentLoaded', function() {
    // Доступные категории
    let categoriesLiter = $('.categories__liter');

    // Блок со всем категориями
    let categoriesBlock = $(".categories__selector__liters__block");

    //Фильтруем блок со всеми категориями доступными
    categoriesBlock.children().each(function (index, element) {
        
       categoriesLiter.each(function (index, element1) {
           
           if ( $(element).html() == $(element1).data("liter") ) 
           {
                $(element).toggleClass("categories__selector__liters__item__disabled");
           }
       })
    })

    $(".categories__selector__liters__item:not(.categories__selector__liters__item__disabled)").on("click", function (e) {
        
        $(".categories__selector__all").data('toggled',false);

        if(!$(this).hasClass('categories__selector__liters__item__active'))
        {
            // Убираем класс с "Все категории"
            $(".categories__selector__all").removeClass("categories__selector__liters__item__active");

            $(".categories__selector__liters__item").removeClass('categories__selector__liters__item__active');

            // Класс "categories__selector__liters__item__active" текущей букве
            $(this).toggleClass("categories__selector__liters__item__active");

            // Подсвечиваем букву при клике
            $(".categories__liter").each(function( index, element ) {

                $(this).hide();
                /* if ( !$(this).hasClass("categories__selector__liters__item__active")) 
                {
                    $(this).hide();
                } */
                if (e.target.innerText == element.dataset.liter ) 
                {
                    $(this).show();
                    //$(this).toggleClass("categories__selector__liters__item__active").toggle();
                }
            });
        }
    })

    // Показ категорий при клике по "Все категории"
    $(".categories__selector__all").on("click", function (e) {

        //Очищаем список активных букв
        $(".categories__selector__liters__item").each(function( index, element ) {
            if ($(this).hasClass("categories__selector__liters__item__active")) 
            {
                $(this).removeClass("categories__selector__liters__item__active");
            }
        });

        $('.categories__content').children().show();
    });
});
</script>
@endsection