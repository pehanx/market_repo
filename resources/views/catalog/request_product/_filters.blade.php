<div class="catalog__content__left">
    @if($categoryDescendants && $categoryDescendants->first())
    <div class="catalog__filter catalog__categories">
        @foreach($categoryDescendants as $categoryDescendant)
        <div class="catalog__categories__block">
            @if($categoryDescendant->children->first())
            <div class="catalog__categories__block__title showAll"> {{ $categoryDescendant->categoryProperties->where('code', 'name')->first()->value }}</div>
            <div class="catalog__categories__block__links">
                <ul>
                    @foreach($categoryDescendant->children as $key => $categoryChildren)
                        @if($key <= 4)
                        <li>
                            <a href="{{ $categoryChildren->link }}">{{ $categoryChildren->categoryProperties->where('code', 'name')->first()->value }}</a>
                        </li>
                        @elseif($key > 4)
                        <li class="overflow-links d-none">
                            <a href="{{ $categoryChildren->link }}">{{ $categoryChildren->categoryProperties->where('code', 'name')->first()->value }}</a>
                        </li>
                        @endif
                    @endforeach
                    
                    @if($categoryDescendant->children->count() > 5)
                        <li><a class="catalog__categories__block__links__showAll" data-text-show="{{  __('interface.buttons.show_completely') }}" data-text-hide="{{  __('interface.buttons.hide_completely') }}">{{  __('interface.buttons.show_completely') }}</a></li>
                    @endif
                </ul>
            </div>
            @else
            <div class="catalog__categories__block__title">
                <a href="{{ $categoryDescendant->link }}">
                    {{ $categoryDescendant->categoryProperties->where('code', 'name')->first()->value }}
                </a>
            </div>
            @endif 
        </div>
        @endforeach
    </div>
    @endif 
</div>