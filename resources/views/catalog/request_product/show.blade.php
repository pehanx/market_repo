@extends('catalog.home')

@section('title')
    {{ $requestProductMetaTitle }} - {{ __('interface.meta.sell_on_market') }}
@endsection

@section('meta')
    <meta name="description" content="{{ $requestProductMetaDescription }}">
    <meta name="title" content="{{ $requestProductMetaTitle }}">
    <meta property="og:title" content="{{ $productMetaTitle }}">
@endsection

{{--                    
@section('catalog-content')
<div class="col-12">
    @section('breadcrumbs', Breadcrumbs::render())
    @yield('breadcrumbs')
</div>  
<div class="col-12 border-bottom mb-2 pb-2">
{{ $requestProduct->requestProductProperties->where('code', 'name')->first()->value }}
</div>
<div class="col-5">
    <div class="row">
        @if($src = $requestProduct->downloads->where('type', 'preview_image')->first())
        <div class="col-12 mb-3">
            <img width="200" height="200" src="{{ asset('storage/'.$src->path) }}">   
        </div>
        @endif
        
        @if($srcs = $requestProduct->downloads->where('type', 'detail_images')->all())
        <div class="col-12">
            @foreach($srcs as $src)
            <img width="50" height="50" src="{{ asset('storage/'.$src->path) }}">   
            @endforeach
        </div>
        @endif
    </div>
</div>
<div class="col-4">
    <div class="row">
        <div class="col-12 text-black-50 mb-3">
            Артикул: {{ $requestProduct->articul }}
        </div>
        <div class="col-12 mb-1">
            Цена
        </div>
        <div class="col-12 mb-3">
            @php $prices = $requestProduct->requestProductPrices->first() @endphp
            {{ $prices->min }} {{ ($prices->max)? ' - '.$prices->max.'$' : '$' }}
        </div>
        <div class="col-12 mb-1">
            Необходимое количество
        </div>
        <div class="col-12 mb-3">
            {{ $requestProduct->characteristics['min_order']['value'] ?? '—' }} {{ $requestProduct->characteristics['min_order']['type'] ?? '' }}
        </div> 
    </div>
</div>
<div class="col-3">
    <div class="row border">
        <div class="col-12 text-black-50 mb-1">
            Поставщик
        </div>
        <div class="col-12 mb-3">
            {{ $requestProduct->profiles->profileProperties->where('code', 'name')->first()->value }}
        </div>
         <div class="col-12 mb-3">
            Много текста<br>
            Много текста<br>
            Много текста<br>
            Много текста<br>
            Много текста<br>
            Много текста<br>
            Много текста<br>
            Много текста<br>
        </div>
        <div class="col-12 mb-3">
            @guest 
                <form method="POST" action="{{ route('favorite.add', ['type' => 'request_product', 'id' => $requestProduct->id]) }}" class="mr-1">
                    @csrf
                    <button class="btn btn-sm px-0"><span class="fa fa-star-o"></span>Добавить в избранное</button>
                </form>
            @endguest
            
            @auth 
                @if( $requestProduct->favorites->where('user_id', $user->id)->first() )
                    <button class="btn btn-sm px-0 favorite-remove" data-route="{{ route('favorite.remove', ['type' => 'request_product', 'id' => $requestProduct->id]) }}"><span class="fa fa-star"></span>Удалить из избранного</button>
                @else
                    <button class="btn btn-sm px-0 favorite-add" data-route="{{ route('favorite.add', ['type' => 'request_product', 'id' => $requestProduct->id]) }}"><span class="fa fa-star-o"></span>Добавить в избранное</button>
                @endif
            @endauth
            
            <button>Написать сообщение</button>
        </div>
    </div>
</div>
<div class="col-12 mt-5">
    <div class="row border pl-3">
        <div class="col-12 border-bottom my-3">
           Описание запроса
        </div>
        <div class="col-12 mb-3">
           {{ $requestProduct->requestProductProperties->where('code', 'description')->first()->value ?? '' }}
        </div>
    </div>
</div>
@endsection
--}}

@section('catalog-content')
<div class="product">
	<div class="container">
		@section('breadcrumbs', Breadcrumbs::render())
        @yield('breadcrumbs')
		<h2 class="product__title">{{ $requestProduct->requestProductProperties->where('code', 'name')->first()->value }}</h2>
		<div class="product__card">
			<div class="product__card__blockImage">
				<div class="product__card__image owl-carousel owl-theme" data-libraries="lightgallery">
                    @isset($requestProduct->downloads['detail_images'])
                        @foreach($requestProduct->downloads['detail_images'] as $detailImage)
                        <div 
                            data-src="{{ asset('storage/'.$detailImage->path) }}"
                            class="product__card__image__item" 
                            style="background-image: url('{{ asset('storage/'.$detailImage->path) }}');"
                        >
                            <img class="lightgallery-slider" src="{{ asset('storage/'.$detailImage->path) }}">
                        </div>
                        @endforeach
					@endisset
				</div>
				<div class="product__card__imageControl owl-carousel owl-theme">
                    @isset($requestProduct->downloads['detail_images'])
                        @foreach($requestProduct->downloads['detail_images'] as $detailImageKey => $detailImageSlider)
                        <div class="product__card__imageControl__item" data-go-slider="{{ $detailImageKey }}" style="background-image: url('{{ asset('storage/'.$detailImageSlider->path) }}');"></div>
                        @endforeach
                    @endisset
				</div>
			</div>
			<div class="product__card__blockInformation">
				<div class="product__card__id">{{ __('interface.catalog.request_product_id') }}: {{ $requestProduct->id }}</div>
                
                @php
                    $prices = $requestProduct->requestProductPrices->first();
                    $currency = $currencyRates->where('code', $currentCurrencyCode)->first();
                @endphp
                
                @if($prices->min !== 0) 
				<div class="product__card__price">
					<div class="product__card__price__label">{{ __('interface.catalog.price') }}</div>
					<div class="product__card__price__value">{{ $prices->min * $currency->rate }}{{$currency->symbol}} {{ $prices->max * $currency->rate ? ' — ' . $prices->max * $currency->rate . $currency->symbol : '' }}</div>
				</div>
                @else
                <div class="product__card__priceRequest">{{ __('interface.catalog.price_on_request') }}</div>
                @endif
                
				<div class="product__card__count">
					<div class="product__card__count__label">{{ __('interface.catalog.required_quantity') }}</div>
					<div class="product__card__count__value">
                    {{ $requestProduct->characteristics['min_order']['value'] ?? '—' }} 
                    {{ $requestProduct->characteristics['min_order']['type'] ?? '' }}
                    </div>
				</div>              
			</div>
            
			<div class="product__card__blockContact">
				<div class="product__card__name">
					<div class="product__card__name__label">{{ __('interface.catalog.buyer') }}</div>
                    <div class="product__card__name__value">
                        {{ $requestProduct->profiles->profileProperties->where('code', 'company_name')->first()->value ?? '' }}
                        {{ $requestProduct->profiles->profileProperties->where('code', 'name')->first()->value ?? '' }}
                    </div>
				</div>
                
                @guest 
                <form method="POST" class="favorite-add-form" action="{{ route('favorite.add', ['type' => 'request_product', 'id' => $requestProduct->id]) }}">
                    @csrf
                    <div class="product__card__favorites">
                        <div class="product__card__favorites__icon"></div>
                        <div class="product__card__favorites__text">{{ __('interface.buttons.favorite_add') }}</div>
                    </div>
                </form>
                @endguest
                
                @auth 
                    @if( $requestProduct->favorites->where('user_id', $user->id)->first() )
                        <div class="product__card__favorites favorite-remove" data-route="{{ route('favorite.remove', ['type' => 'request_product', 'id' => $requestProduct->id]) }}">
                            <div class="product__card__favorites__icon active"></div>
                            <div class="product__card__favorites__text">{{ __('interface.buttons.favorite_remove') }}</div>
                        </div>
                    @else
                        <div class="product__card__favorites favorite-add"  data-route="{{ route('favorite.add', ['type' => 'request_product', 'id' => $requestProduct->id]) }}">
                            <div class="product__card__favorites__icon"></div>
                            <div class="product__card__favorites__text">{{ __('interface.buttons.favorite_add') }}</div>
                        </div>
                    @endif
                
                    @if ( session()->has('active_profile') )
						@if(!$isContact && !$myProfile)
                            <div data-route-contact='{{ route('contact.add', ['contact'=>$buyer]) }}' class="product__card__contacts contact-add">
                            </div>
                        @endif
                    @else
                        <form method="POST" class="contact-add-form" action="{{ route('contact.add', ['contact'=>$buyer]) }}">
                            @csrf
                            <div class="product__card__contacts">
                            </div>
                        </form>
                    @endif
                @endauth
                @guest
                    <form method="POST" class="contact-add-form" action="{{ route('contact.add', ['contact'=>$buyer]) }}">
                        @csrf
                        <div class="product__card__contacts">
                        </div>
                    </form>
                @endguest
                @if(!$myProfile)
                <div class="product__card__message">
                    <a href="{{ route('cabinet.chat.member', ['member' => $buyer]) }}" class="button button-primary button-inline">{{ __('interface.buttons.write_to_buyer') }}</a>
                </div>
                @endif
			</div>
		</div>
        
        @if($describle = $requestProduct->requestProductProperties->where('code', 'full_description')->first())
		<div class="product__cardDescription">
			<h3 class="product__cardDescription__title">{{ __('interface.catalog.request_product_describle') }}</h3>
			<div class="product__cardDescription__text">
				<p class="product__cardDescription__text__paragraph">
                    {{ $describle->value }}
                </p>
            </div>
		</div>
        @endif
	</div>
</div>
@endsection