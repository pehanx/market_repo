@extends('layouts.market.index')

@if(View::hasSection('title'))
    @section('title')
        @yield('title')
    @endsection
@endif

@section('meta')
    @yield('meta')
@endsection

@section('content')
    @yield('catalog-content') 
@endsection

@section('scripts')
    <script src="{{ mix('js/catalog.js', 'assemble') }}"></script>
    <script>        
        document.addEventListener('DOMContentLoaded', function() {
            
        });
    </script>
    @yield('content-script')
@endsection