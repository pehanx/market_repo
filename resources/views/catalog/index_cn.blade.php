@extends('layouts.market.index')

@section('content')
<div class="categories">
    <div class="container">
        @section('breadcrumbs', Breadcrumbs::render())
        @yield('breadcrumbs')
        <h2 class="categories__title">{{ __('interface.catalog.catalog')}}</h2>
    <div class="categories__content">
        @foreach($categories as $group => $collection)
            <div class="categories__liter" data-liter="{{ mb_strtoupper($group) }}">
                {{-- <div class="categories__liter__title">{{ mb_strtoupper($group) }}</div> --}}
                <div class="categories__liter__content">
                    @if($collection->first())
                        @foreach($collection as $categoryDepthOne)
                            <div class="categories__element">
                                <div class="categories__element__title">
                                    <a href="{{ $categoryDepthOne->link }}">{{ $categoryDepthOne->categoryProperties->where('code', 'name')->first()->value }}</a>
                                </div>
                                <div class="categories__element__content">
                                    <div class="categories__catalog">
                                        @if($categoryDepthOne->children->first())
                                            @foreach($categoryDepthOne->children as $keyDepthTwo => $categoryDepthTwo)
                                                <div class="categories__catalog__title">
                                                    <a href="{{ $categoryDepthTwo->link }}">{{ $categoryDepthTwo->categoryProperties->where('code', 'name')->first()->value }}</a>
                                                </div>
                                                <div class="categories__catalog__content">
                                                    <ul>
                                                        @if($categoryDepthTwo->children->first())
                                                            @foreach($categoryDepthTwo->children as $keyDepthTree => $categoryDepthTree)
                                                                <li><a href="{{ $categoryDepthTree->link }}">{{ $categoryDepthTree->categoryProperties->where('code', 'name')->first()->value }}</a></li>
                                                            @endforeach
                                                        @endif
                                                    </ul>
                                                </div>
                                            @endforeach
                                        @endif
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    @endif
                </div>
            </div>
        @endforeach
        </div>
    </div>
</div>
@endsection

@section('scripts')
@endsection