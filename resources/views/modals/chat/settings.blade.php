<div class="modal fade bd-example-modal-lg contacts__modal__init" id="settings-modal" data-route-settings="{{route('chat.settings')}}" data-route-save="{{ route('chat.settings.save') }}">
    <div class="modal-dialog modal-dialog-centered modal-lg contacts__modal" role="document">
        <div class="modal-content contacts__modal__content">

            <div class="modal-body contacts__modal__body">
                <div class="modal__content__modalmenu">
                    <div class="content__modalmenu__title">{{ __('modals.chat_settings') }}</div>
                    <div class="modalmenu__item active" data-type="localisation">
                        <div class="item__icon translate"></div>
                        <div class="item__name">{{ __('modals.chat_settings_locale') }}</div>
                        <div class="item__arrow"></div>
                    </div>
                    {{-- @if( session()->get('active_profile') !== null )
                        <div class="modalmenu__item" data-type="profile">
                            <div class="item__icon profile"></div>
                            <div class="item__name">{{ __('modals.chat_settings_profile') }}</div>
                            <div class="item__arrow"></div>
                        </div>
                    @endif --}}
                </div>
                <div class="modal__content__settings">
                        <div class="content__settings__wrapper">
                            <div class="content__settings__title"></div>
                            <div class="settings__item"></div>
                        </div>
                        <div class="content__settings__button-wrapper">
                            <button class="button button-primary-bordered width-fit-content" id="chat_settings_save">{{ __('interface.buttons.save_changes') }}</button>
                        </div>
                    </div>
                </div>
                <button type="button" class="close modal__content__close" data-dismiss="modal" aria-label="Close">
                </button>
            </div>
            
        </div><!-- /.модальное окно-Содержание -->
    </div><!-- /.модальное окно-диалог -->
</div><!-- /.модальное окно -->