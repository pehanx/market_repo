<div class="modal fade" id="export-modal">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <div class="d-flex">{{ __('modals.export') }}</div>
        </div>
        <form id="export-modal-form" class="mb-0" method="post">
            @csrf
            <div class="modal-body d-flex flex-column">
                <div class="models-wrapper">
                    <input type="checkbox" name="with_models" value="true">
                    {{ __('modals.export_with_models') }}
                </div>
                
                <div class="mt-2">
                    <input type="checkbox" name="with_thumbnails" value="true">
                    {{ __('modals.export_with_thumbnails') }}
                </div>
                
                <div class="my-2">{{ __('modals.export_format') }}</div>
                <select class="export-formats w-100 mt-2" name="export_format">
                    @foreach ($exportFormats  as $exportFormat)
                    <option value="{{ $exportFormat }}">{{ $exportFormat }}</option>
                    @endforeach
                </select>
                
            </div>
            
            <div class="modal-footer justify-content-start border-0">
                <button type="submit" class="btn btn-secondary">
                    {{ __('interface.buttons.export_submit') }}
                </button>
            </div>
            <input type="hidden" name="elements_id" value="">
        </form>
            
        </div><!-- /.модальное окно-Содержание -->
    </div><!-- /.модальное окно-диалог -->
</div><!-- /.модальное окно -->