<div class="modal fade" id="write-supplier-modal">
    <div class="modal-dialog modal-dialog-top modal-lg" role="document">
        <div class="modal-content">
        
            <div class="modal-header">
                <h1>{{ __('interface.buttons.write_to_supplier') }}</h1>
            </div>

            <div class="modal-body">
                <form action="{{ route('catalog.product.write_supplier', ['product' => $product]) }}" id="write-supplier-form">
                <div class="form__row form__row-required">
                    <input class="form__row__input" name="company_name" type="text" value="" id="company_name" required>
                    <label for="company-name" class="form__row__label">{{ __('cabinet.profile.form_company_name') }}</label>
                </div>

                <div class="form__double__row form__double__row-between">
                        
                <div class="form__row form__row-required"> <!-- form__row-error -->
                    <input class="form__row__input" name="phone" type="text" value="" id="phone" required>
                    <label for="phone" class="form__row__label">{{ __('cabinet.account.form_phone') }}</label>
                </div>

                <div class="form__row form__row-required @if($errors->has('email')) {{ 'form__row-error' }} @endif"> <!-- form__row-error form__row-hasText-->
                    <input class="form__row__input" id="email" name="email" type="text" value="" required>
                    <label for="email" class="form__row__label">{{ __('cabinet.account.email') }}</label>
                </div>
                </form>
            </div>
            
            <div class="modal-footer">
                <button type="button" class="button button-primary-bordered width-fit-content" data-dismiss="modal">
                    <i class="fa fa-times" aria-hidden="true"></i> 
                    {{ __('modals.cancel') }}
                </button>
                <button id="write-supplier-button" type="button" class="button button-primary width-fit-content submit">
                    <i class="fa fa-check-square-o" aria-hidden="true"></i> 
                    {{ __('modals.approve') }}
                </button>
            </div>
        </div><!-- /.модальное окно-Содержание -->
    </div><!-- /.модальное окно-диалог -->
</div><!-- /.модальное окно -->