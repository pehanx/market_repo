<div class="modal fade bd-example-modal-lg" id="disable-modal">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
        
            <div class="modal-header">
                {{ __('modals.approve_action') }}
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            
            <div class="modal-body">

            </div>
            
            <div class="modal-footer">
                <button type="button" class="button button-primary-bordered width-fit-content" data-dismiss="modal">
                    <i class="fa fa-times" aria-hidden="true"></i> 
                    {{ __('modals.cancel') }}
                </button>
                
                <form method="POST" id="disable-modal-form" class="mb-0">
                    @csrf
                    <button type="submit" class="button button-primary width-fit-content">
                        <i class="fa fa fa-check-square-o" aria-hidden="true"></i> 
                        {{ __('modals.disable') }}
                    </button>
                </form>
            </div>
        </div><!-- /.модальное окно-Содержание -->
    </div><!-- /.модальное окно-диалог -->
</div><!-- /.модальное окно -->