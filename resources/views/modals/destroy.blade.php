<div class="modal fade bd-example-modal-lg" id="destroy-modal">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
        
            <div class="modal-header">
                {{ __('modals.approve_action') }}
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            
            <div class="modal-body">

            </div>
            
            <div class="modal-footer">
                <button type="button" class="button button-primary-bordered width-fit-content" data-dismiss="modal">
                    <i class="fa fa-times" aria-hidden="true"></i> 
                    {{ __('modals.cancel') }}
                </button>
                
                <form id="destroy-modal-form">
                    @csrf
                    {{ method_field('DELETE') }}
                    <button type="submit" class="button button-primary width-fit-content">
                        <i class="fa fa-trash" aria-hidden="true"></i> 
                        {{ __('modals.delete') }}
                    </button>
                </form>
            </div>
            
        </div><!-- /.модальное окно-Содержание -->
    </div><!-- /.модальное окно-диалог -->
</div><!-- /.модальное окно -->

{{--
<div class="login">
	<div class="container">
		<div class="login__wrapper">
			<div class="login__title">Восстановление пароля</div>
			<div class="login__form form">
				<div class="form__row"> <!-- form__row-error -->
					<div class="login__form__success">
						<p>На E-mail <span class="login__form__success__mail"></span> было отправлено письмо. Следуйте инструкции, чтобы восстановить пароль.</p>
					</div>
				</div>
				<div class="form__row">
					<button type="button" class="button button-primary">Вернуться на главную</button>
				</div>
			</div>
		</div>
	</div>
</div>
--}}