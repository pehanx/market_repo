<div class="modal fade" id="info-modal">
    <div class="modal-dialog modal-dialog-top modal-lg" role="document">
        <div class="modal-content">
        
            <div class="modal-header">

            </div>
            
            <div class="modal-body">

            </div>
            
            <div class="modal-footer">
                <button type="button" class="button button-primary-bordered width-fit-content" data-dismiss="modal">
                    &nbsp;&nbsp;&nbsp;
                    <i class="fa fa-check-square-o" aria-hidden="true"></i> 
                    {{ __('modals.ok') }}
                    &nbsp;&nbsp;&nbsp;
                </button>
            </div>
        </div><!-- /.модальное окно-Содержание -->
    </div><!-- /.модальное окно-диалог -->
</div><!-- /.модальное окно -->