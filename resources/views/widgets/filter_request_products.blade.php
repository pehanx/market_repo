{{--
<form id="filter-request-products" action="?" method="GET">
    <div class="card">
    
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        
        <div class="card-body">
            <div class="row ml-0">
                <input type="text" name="name" placeholder="{{ __('interface.filters.request_product') }}" class="w-50" value="{{ request('name') }}">
            </div>
        </div>
        
        <div class="card-body" id="full-filter-products">
            <div class="row">
                <div class="col-4">
                    <label>{{ __('interface.filters.request_number') }}</label>
                    <input type="text" name="id" class="mr-4 w-100 filter" value="{{ request('id') }}">
                </div>
                
                <div class="col-4">
                    <label>{{ __('interface.filters.category') }}</label>
                    <select class="select2 categories w-100 filter" name="category">
                        <option {{ request('category') ? '' : 'selected' }}></option>
                        @foreach($categories as $category)
                        <option {{ (request('category') == $category['id']) ? 'selected' : '' }} value="{{ $category['id'] }}">{{ $category['text'] }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
        </div>
        
        <div class="card-body">
            <div class="row">
                <div class="col-8">
                    <button type="submit" class="btn btn-primary">{{ __('interface.buttons.search') }}</button>
                    <a href="{{ Request::url() }}" class="btn btn-light mr-5">{{ __('interface.buttons.clear') }}</a>
                </div>
            </div>
        </div>
        
    </div>
</form>
--}}

<div class="cabinet__content__filter">
    <div class="cabinet__content__filter__row cabinet__content__filter__row-space-between">
        <form id="filter-products" action="?" method="GET">
            <div class="cabinet__content__filter__search">
                
                <input 
                    type="text" 
                    class="cabinet__content__filter__search__input" 
                    placeholder="{{ __('interface.filters.search_request_product') }}" 
                    value="{{ request('name') }}"
                    name="name"
                >
                
                <!-- При активации добавляется класс cabinet__content__filter__search__more-active -->
                <div 
                    class="cabinet__content__filter__search__more cabinet__content__filter__search__more-active"
                >
                    
                    <div class="cabinet__content__filter__search__more__row">

                        <div class="cabinet__content__filter__search__more__row__element">
                        {{-- <label for="category">{{ __('interface.filters.category') }}</label> --}}
                            
                            <select id="category" class="filter" name="category">
                                <option {{ request('category') ? '' : 'selected' }}></option>
                                @foreach($categories as $category)
                                <option {{ (request('category') == $category['id']) ? 'selected' : '' }} value="{{ $category['id'] }}">
                                    {{ $category['text'] }}
                                </option>
                                @endforeach
                            </select>
                            
                        </div>
                        
                    </div>
                    
                    <div class="cabinet__content__filter__search__more__row">
                        <div class="cabinet__content__filter__search__more__row__element">
                            <input type="text" id="id" class="filter" value="{{ request('id') }}"  placeholder="{{ __('interface.filters.request_number') }}" name="id">
                        </div>
                    </div>

                </div>
                
                <button type="submit" class="button button-primary button-inline">{{ __('interface.buttons.search') }}</button>
                <a href="{{ Request::url() }}" class="button button-inline button-primary-bordered">{{ __('interface.buttons.clear') }}</a>
                
            </div>
        </form>
 
    </div>
</div>