{{--
<form id="filter-favorites" action="?" method="GET">
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-4">
                    <input type="text" name="name" placeholder="{{ __('interface.filters.favourite_name') }}" class="w-100" value="{{ request('name') }}">
                </div>
                
                <div class="col-4">
                    <select class="select2 categories w-100 filter" name="category_id">
                        <option {{ request('category_id') ? '' : 'selected' }}></option>
                        @foreach($categories as $category)
                        <option {{ (request('category_id') == $category['id']) ? 'selected' : '' }} value="{{ $category['id'] }}">{{ $category['text'] }}</option>
                        @endforeach
                    </select>
                </div>
                
                <div class="col-4">
                <button type="submit" class="btn btn-primary"> {{ __('interface.buttons.search') }}</button>
                    <button id="filter-favorites-reset" type="button" class="btn btn-light mr-5">{{ __('interface.buttons.clear') }}</button>
                </div>
            </div>
        </div>
    </div>
</form>
--}}

<div class="cabinet__content__filter">
    <div class="cabinet__content__filter__row cabinet__content__filter__row-space-between">
        <form id="filter-favorites" action="?" method="GET">
            <div class="cabinet__content__filter__search">
                
                <input 
                    type="text" 
                    class="cabinet__content__filter__search__input" 
                    placeholder="{{ __('interface.filters.search_' . request()->route('type')) }}" 
                    value="{{ request('name') }}"
                    name="name"
                >

                <div 
                    class="cabinet__content__filter__search__more cabinet__content__filter__search__more-active"
                >
                    
                    <div class="cabinet__content__filter__search__more__row">

                        <div class="cabinet__content__filter__search__more__row__element">
                            
                            <select id="favorites-category-id" class="filter" name="category_id">
                                <option {{ request('category') ? '' : 'selected' }}></option>
                                @foreach($categories as $category)
                                <option {{ (request('category_id') == $category['id']) ? 'selected' : '' }} value="{{ $category['id'] }}">
                                    {{ $category['text'] }}
                                </option>
                                @endforeach
                            </select>
                            
                        </div>
                        
                    </div>

                </div>
                
                <button type="submit" class="button button-primary button-inline">{{ __('interface.buttons.search') }}</button>
                <button id="filter-favorites-reset" type="button" class="button button-inline button-primary-bordered">{{ __('interface.buttons.clear') }}</button>
                
            </div>
        </form>
 
    </div>
</div>