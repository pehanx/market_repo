<form id="search" action="{{ $route }}" method="GET" class="header__search"> <!--class="header__search__form"--> 
    @if(!empty($page) && $page === 'catalog')
    <input type="text" name="search" class="header__search__input" value="{{ request('search') }}">
    <button type="button" class="catalog-search header__search__button">{{ __('interface.buttons.search') }}</button>
    @else
    <select id="select" class="header__search__input"></select>
    <button type="button" class="header__search__button">{{ __('interface.buttons.search') }}</button>
    @endif
</form>

@if(empty($page) || !empty($page) && $page !== 'catalog')
    @section('widget-script')
    <script src="{{ mix('js/widgets/search.js', 'assemble') }}"></script>
    <script>
        document.addEventListener('DOMContentLoaded', function() {
            let data = @json($categories, JSON_PRETTY_PRINT);

            WidgetSearch.init(data, "{{ __('interface.header.what_you_search') }}");

            $('#select').trigger({
                type: 'select2:close',
            });
        });
    </script>
    @show
@endif