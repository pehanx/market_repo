<div class="card border-0">
    <div class="card-header"> 
        {{ __('cabinet.profile.not_approved') }}
    </div>
    <div class="card-body"> 
        {{ __('cabinet.profile.requre_approve') }}
    </div>
    <div class="card-footer"> 
        <a href="{{ route('cabinet.profile.index') }}" class="btn btn-secondary">{{ __('interface.buttons.goto_profiles') }}</a>
    </div>
</div>