{{--
<form id="filter-products" action="?" method="GET">
    <div class="card">
    
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        
        <div class="card-body">
            <div class="row ml-0">
                <input type="text" name="name" placeholder="{{ __('interface.filters.search_product') }}" class="w-75" value="{{ request('name') }}">
            </div>
        </div>
        
        <div class="card-body {{ $area === 'show' ? '' : 'd-none' }}" data-area="{{ $area === 'show' ? 'show' : 'hide' }}" id="full-filter-products">
            <div class="row">
                <div class="col-4">
                    <label>@isset($id){{ __('interface.filters.number') }}@else{{ __('interface.filters.articul') }}@endisset</label>
                    <input type="text" name="@isset($id){{'id'}}@else{{'articul'}}@endisset" class="mr-4 w-100 filter" {{ $area === 'show' ? '' : 'disabled' }} value="{{ request('articul') }}">
                </div>
                
                <div class="col-4">
                    <label>{{ __('interface.filters.category') }}</label>
                    <select class="select2 categories w-100 filter" name="category" {{ $area === 'show' ? '' : 'disabled' }}>
                        <option {{ request('category') ? '' : 'selected' }}></option>
                        @foreach($categories as $category)
                        <option {{ (request('category') == $category['id']) ? 'selected' : '' }} value="{{ $category['id'] }}">{{ $category['text'] }}</option>
                        @endforeach
                    </select>
                </div>
            </div>

            <label class="mt-3">{{ __('interface.filters.price') }}, $</label>
            <div class="row">
                <div class="col-4 d-flex justify-content-between">
                    <span>{{ __('interface.filters.min_price') }}</span>
                    <input type="text" name="min_price" class="w-25 mr-5 filter" {{ $area === 'show' ? '' : 'disabled' }} value="{{ request('min_price') }}">
                    <span>{{ __('interface.filters.max_price') }}</span>
                    <input type="text" name="max_price" class="w-25 filter" {{ $area === 'show' ? '' : 'disabled' }} value="{{ request('max_price') }}">
                </div>
            </div>

        </div>
        
        <div class="card-body">
            <div class="row">
                <div class="col-8">
                    <button type="submit" class="btn btn-primary">{{ __('interface.buttons.search') }}</button>
                    <a href="{{ Request::url() }}" class="btn btn-light mr-5">{{ __('interface.buttons.clear') }}</a>
                    <button type="button" class="btn border" data-hide="{{ __('interface.filters.hide') }}" data-show="{{ __('interface.filters.detail') }}" data-area="{{ $area === 'hide' ? 'hide' : 'show' }}" id="toggle-filter-products">
                        {{ $area === 'show' ? __('interface.filters.hide') : __('interface.filters.detail')}}
                    </button>
                    <input name="full_filter" type="hidden" value="{{ $area === 'show' ? 'show' : 'hide' }}">
                </div>
            </div>
        </div>
        
    </div>
</form>
--}}

<div class="cabinet__content__filter">
    <div class="cabinet__content__filter__row cabinet__content__filter__row-space-between">
        <form id="filter-products" action="?" method="GET">
            <div class="cabinet__content__filter__search">
                
                <input 
                    type="text" 
                    class="cabinet__content__filter__search__input" 
                    placeholder="{{ __('interface.filters.search_product') }}" 
                    value="{{ request('name') }}"
                    name="name"
                >
                
                <!-- При активации добавляется класс cabinet__content__filter__search__more-active -->
                <div 
                    class="cabinet__content__filter__search__more {{ $area === 'show' ? 'cabinet__content__filter__search__more-active' : '' }}"
                    data-area="{{ $area === 'show' ? 'show' : 'hide' }}" 
                    id="full-filter-products"
                >
                    
                    <div class="cabinet__content__filter__search__more__row">

                        <div class="cabinet__content__filter__search__more__row__element">
                        {{-- <label for="category">{{ __('interface.filters.category') }}</label> --}}
                            
                            <select id="category" class="filter" name="category" {{ $area === 'show' ? '' : 'disabled' }}>
                                <option {{ request('category') ? '' : 'selected' }}></option>
                                @foreach($categories as $category)
                                <option {{ (request('category') == $category['id']) ? 'selected' : '' }} value="{{ $category['id'] }}">
                                    {{ $category['text'] }}
                                </option>
                                @endforeach
                            </select>
                            
                        </div>
                        
                    </div>
                    
                    <div class="cabinet__content__filter__search__more__row">
                        
                        <div class="cabinet__content__filter__search__more__row__element">
                        {{--
                            <label for="articul">
                                {{ __('interface.filters.articul') }}
                            </label>
                        --}}    
                            <input type="text" id="articul" class="filter" value="{{ request('articul') }}" {{ $area === 'show' ? '' : 'disabled' }} placeholder="{{ __('interface.filters.articul') }}" name="articul">
                            
                        </div>

                    </div>
                    
                    <div class="cabinet__content__filter__search__more__row priceRange">
                        <div class="cabinet__content__filter__search__more__row__element">
                        {{-- <span>{{ __('interface.filters.min_price') }}</span> --}}
                            <input type="text" class="filter" name="min_price" {{ $area === 'show' ? '' : 'disabled' }} value="{{ request('min_price') }}" placeholder="{{ __('interface.filters.min_price') }}">
                        </div>
                        <div class="cabinet__content__filter__search__more__row__element">
                        {{-- <span>{{ __('interface.filters.max_price') }}</span> --}}
                            <input type="text" class="filter" name="max_price" {{ $area === 'show' ? '' : 'disabled' }} value="{{ request('max_price') }}" placeholder="{{ __('interface.filters.max_price') }}">
                        </div>
                    </div>
                    
                </div>
                
                <button type="submit" class="button button-primary button-inline">{{ __('interface.buttons.search') }}</button>
                <a href="{{ Request::url() }}" class="button button-inline button-primary-bordered">{{ __('interface.buttons.clear') }}</a>
                <input name="full_filter" type="hidden" value="{{ $area === 'show' ? 'show' : 'hide' }}">
                
            </div>
        </form>
        
        <!-- При активации добавляется класс cabinet__content__filter__showDetailFilter-active -->
        <div 
            class="cabinet__content__filter__showDetailFilter cabinet__content__filter__showDetailFilter-active"
            id="toggle-filter-products"
            data-hide="{{ __('interface.filters.hide') }}"
            data-show="{{ __('interface.filters.detail') }}"
            data-area="{{ $area === 'hide' ? 'hide' : 'show' }}"
        >
            {{ $area === 'show' ? __('interface.filters.hide') : __('interface.filters.detail') }}
        </div>
        
    </div>
</div>