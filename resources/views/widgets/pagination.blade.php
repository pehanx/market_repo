@if ($paginator->hasPages())
    <div class="catalog__pagination">
        <ul>
            {{-- Previous Page Link --}}
            @if ($paginator->onFirstPage())

            @else
                <li class="catalog__pagination__item catalog__pagination__item-prev">
                    <a href="{{ $paginator->previousPageUrl() }}">@lang('pagination.previous')</a>
                </li>
            @endif

            {{-- Pagination Elements --}}
            @foreach ($elements as $element)
                {{-- "Three Dots" Separator --}}
                @if (is_string($element))
                    <li class="catalog__pagination__item disabled" aria-disabled="true"><span class="page-link">{{ $element }}</span></li>
                @endif

                {{-- Array Of Links --}}
                @if (is_array($element))
                    @foreach ($element as $page => $url)
                        @if ($page == $paginator->currentPage())
                            <li class="catalog__pagination__item catalog__pagination__item-active" aria-current="page"><span class="page-link">{{ $page }}</span></li>
                        @else
                            <li class="catalog__pagination__item"><a class="page-link" href="{{ $url }}">{{ $page }}</a></li>
                        @endif
                    @endforeach
                @endif
            @endforeach

            {{-- Next Page Link --}}
            @if ($paginator->hasMorePages())
                <li class="catalog__pagination__item catalog__pagination__item-next">
                    <a href="{{ $paginator->nextPageUrl() }}">@lang('pagination.next')</a>
                </li>
            @else

            @endif
        </ul>
    </div>
@endif