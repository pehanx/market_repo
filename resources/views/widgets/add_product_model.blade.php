{{--
<form id="add-model-form" method="POST" accept-charset="UTF-8" class="form-vertical" enctype="multipart/form-data">
    @csrf
    
    @if (count($errors)>0) 
    <div class="text-danger">
        @foreach ($errors->all() as $error)
            <span>{{$error}}</span><br/>
        @endforeach
    </div>
    @endif

    <div class="form-group row mt-3">
        <label class="col-md-6 col-form-label">{{ __('cabinet.product.form_model_preview_image') }}</label>
        <div class="col-sm-6">
            <input type="file" name="preview_image" accept="image/*" value="{{ old('preview_image') }}">
        </div>
    </div>
    <div class="form-group row">
        <label for="example-email-input" class="col-12 col-md-6 col-form-label">{{ __('cabinet.product.form_color') }}</label>
        <div class="col-12 col-md-6">
            <select name="color">
                @foreach($colors as $color)
                    <option value="{{ $color->code }}">{{ $color->value }}</option>
                @endforeach
            </select>
        </div>
    </div>
    <div class="form-group row">
        <div class="input-group">
            <label for="example-text-input" class="col-12 col-md-6 col-form-label">{{ __('cabinet.product.form_available') }}</label>
            <div class="col-12 col-md-6">
                <div class="row">
                    <div class="col-5">
                        <input class="form-control" name="available[value]" type="text" value="" >
                    </div>
                    <div class="col-2">
                        <select class="h-100" name="available[type]">
                            @foreach($quantities as $quantity)
                                <option value="{{ $quantity->code }}">{{ $quantity->value }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="form-group row">
        <label for="example-text-input" class="col-12 col-md-6 col-form-label">{{ __('cabinet.product.form_min_price') }}, ($)</label>
        <div class="col-12 col-md-6">
            <input class="form-control" name="min_price" type="text" value="{{ old('min_price') }}" >
        </div>
    </div>	
    <div class="form-group row">
        <label for="example-text-input" class="col-12 col-md-6 col-form-label">{{ __('cabinet.product.form_max_price') }}, ($)</label>
        <div class="col-12 col-md-6">
            <input class="form-control" name="max_price" type="text" value="{{ old('max_price') }}" >
        </div>
    </div>
    
    <hr class="mt-5">
    <div class="form-group row justify-content-center">
        <button type="button" id="delete-model" class="btn btn-secondary mt-3 mr-5">{{ __('interface.buttons.delete') }}</button>
    </div>
</form>

<div id="button-wrapper" class="row">
    <button type="button" id="add-model" class="btn btn-secondary mt-3 mr-5">{{ __('interface.buttons.add_more') }}</button>
    <button type="button" id="save-models" data-action="{{ $action }}" class="btn btn-secondary mt-3">{{ __('interface.buttons.save') }}</button>
</div>
--}}

<form id="add-model-form" method="POST" accept-charset="UTF-8" enctype="multipart/form-data" class="cabinet__createModel__model">
    @csrf
    <div class="cabinet__createModel__section cabinet__createModel__content form">
        
        <div class="cabinet__subtitle">{{ __('cabinet.product.form_add_product_model') }} {{ $productArticul }} — <span class="prefix">{{ $articulPrefix }}</span></div>
        
        <div class="form__row form__row-required">
            <div class="form__row__uploadImages">
                <div class="form__row__uploadImages__label">{{ __('cabinet.product.form_model_preview_image') }}</div>

                <input id="preview-image" type="file" class="form__row__uploadFile__input" name="preview_image" accept="image/*">
                
                <label for="preview-image" class="form__row__uploadFile__label">
                    <div class="button button-primary-bordered button-inline">{{ __('interface.buttons.choose_image') }}</div>
                </label>
            </div>
            <div class="form__row__error"></div>
        </div>
        
        <div class="form__double__row form__double__row-between">
                        
            <div class="form__row form__row-required">
                <input name="min_price" type="text" value="{{ old('min_price') }}" class="form__row__input" id="min-price" required>
                <label for="min-price" class="form__row__label">{{ __('cabinet.product.form_min_price') }}, ($)</label>
                <div class="form__row__error"></div>
            </div>
            
            <div class="form__row">
                <input name="max_price" type="text" value="{{ old('max_price') }}" class="form__row__input" id="max-price">
                <label for="max-price" class="form__row__label">{{ __('cabinet.product.form_max_price') }}, ($)<label>
                <div class="form__row__error"></div>
            </div>
            
        </div>
        
        <div class="form__double__row form__double__row-between">
                        
            <div class="form__row form__row-required">
                <input type="text" name="available[value]" class="form__row__input" id="available-value" value="{{ old('available[value]') }}" required>
                <label for="available-value" class="form__row__label">{{ __('cabinet.product.form_available') }}</label>
                <div class="form__row__error"></div>
            </div>
            
            <div class="form__row form__row-required">
                <select name="available[type]">
                    @foreach($quantities as $quantity)
                    <option 
                        value="{{ $quantity->code }}" 
                        {{ (old('available[type]') === $quantity->code) ? 'selected' : '' }}
                    >
                        {{ $quantity->value }}
                    </option>
                    @endforeach
                </select>
            </div>
            
        </div>
        
        <div class="form__row form__row-required">
            <div class="form__row__selectorColor">
                <div class="form__row__selectorColor__label">{{ __('cabinet.product.form_color') }}</div>
                <div class="form__row__selectorColor__content">
                    <select name="color" class="colorpicker-handle">
                        @foreach($colors as $color)
                        <option {{(request('color') === $color->code) ? 'selected' : ''}} value="{{ $color->code }}">{{ $color->value }}</option>
                        @endforeach
                    </select>
                </div>  
            </div>
        </div>
                    
        <div class="cabinet__createModel__actions">
            <button type="button" id="delete-model" class="button button-primary-bordered button-inline d-none">
                {{ __('interface.buttons.delete') }}
            </button>
        </div>

    </div>
</form>