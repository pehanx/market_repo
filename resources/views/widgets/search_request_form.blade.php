<div class="cabinet__createProduct__section form">
                
    <div class="cabinet__createProduct__wrapper">
        <div class="cabinet__subtitle">{{ __('cabinet.product.form_find_request') }}</div>

        <div class="form__row">
            <label for="find-description" class="form__row__label form__row__label__textarea">{{ __('Опишите подробнее что вы ищите') }}</label>
            <textarea rows="5" class="form__row__textarea form__row__input" placeholder="{{ __('Опишите подробнее что вы ищите') }}" id="find-description" name="find-description"></textarea>
        </div>

        <div class="form__double__row form__double__row-between">
            
            <div class="form__row form__row-required">
                <label for="find-name" class="form__row__label">{{__('Ваше имя')}}</label>
                <input class="form__row__input" name="find-name" id="find-name" type="text" value="" >
            </div>

            <div class="form__row form__row-required">
                <label for="find-email" class="form__row__label">{{__('Ваш E-mail')}}</label>
                <input class="form__row__input" name="find-email" id="find-email" type="text" value="" >
            </div>

        </div>

        <div class="form__double__row form__double__row-between">
        
            <div class="form__row form__row-required">
                <label for="find-name" class="form__row__label">{{__('Ваш телефон')}}</label>
                <input class="form__row__input" name="find-name" id="find-name" type="text" value="" >
            </div>

            <div class="form__row form__row-required">
                <label for="find-name" class="form__row__label">{{__('Название компании')}}</label>
                <input class="form__row__input" name="find-name" id="find-name" type="text" value="" >
            </div>

        </div>

    </div>
    
</div>