@section('header')
<html lang="{{ $htmlLang }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=600">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>{{ config('app.name') }}</title>

        <!-- Scripts -->
        <script src="{{mix('js/manifest.js', 'assemble')}}"></script>
        <script src="{{ mix('js/vendor.js', 'assemble')}}"></script>
        <!-- Styles -->
        <link href="{{ mix('css/main.css', 'assemble') }}" rel="stylesheet">
    </head>
    
    <body>
        <div class="preloader">
            <img src="/../assemble/images/spinner/spinner.svg" class="preloader__logo">
            <div class="loadingio-spinner-eclipse-indrmqmaclg preloader__image"><div class="ldio-2ak7hgotj2i"><div></div></div></div>
        </div>
    @include('layouts.market.cabinet.header')
@show
        <main class="cabinet__main">
            @yield('sidebar')
            @yield('content') 
        </main>
         
        @section('footer')
            @include('layouts.market.cabinet.footer') 
            @include('modals.info')
        @show
        
        <script src="{{ mix('js/app.js', 'assemble') }}"></script>
        <script src="{{ mix('js/index.js', 'assemble') }}"></script>
        <script src="{{ mix('js/main.js', 'assemble') }}"></script>
        <script src="{{ mix('js/Notify.js', 'assemble') }}"></script>
        <script src="{{ mix('js/Info.js', 'assemble') }}"></script>
        <script>
            document.addEventListener('DOMContentLoaded', function() {
                
                let message = @if(Session::has('message')) @json(Session::get('message'), JSON_FORCE_OBJECT) @else 'none' @endif;
                
                if(message !== 'none')
                {
                    let notify = new Notify ();
                    notify.show(message.body, message.title, message.type);
                }
                
                let infoMessage = @if(Session::has('info_message')) @json(Session::get('info_message'), JSON_FORCE_OBJECT) @else 'none' @endif;
                
                if(infoMessage !== 'none')
                {
                    let infoModal = new Info($('#info-modal'));
                    infoModal.show(infoMessage.body, infoMessage.title, infoMessage.type);
                }
                
            });
        </script>
        @yield('scripts')
    </body>
</html>