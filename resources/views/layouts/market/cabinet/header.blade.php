{{--
<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=600">
		<title>Market</title>
		<link rel="stylesheet" href="dist/assets/css/vendors.min.css">
		<link rel="stylesheet" href="dist/assets/css/market.min.css">
	</head>
	<body>
		<header class="header header-fullwidth">
			<div class="header header__top">
				<div class="container">
					<a href="/" class="header__logo"></a>
					<div class="header__search">
						<input type="text" class="header__search__input" placeholder="{{ __('interface.filters.search_tenders') }}">
						<button type="button" class="header__search__button">{{ __('interface.buttons.search') }}</button>
					</div>
					<div class="header__controls">
						<div class="header__controls__user"><div class="header__controls__icon header__controls__user__icon"></div><div class="header__controls__title header__controls__user__title">{{ __('interface.buttons.login') }}</div></div>
						<div class="header__controls__favorites"><div class="header__controls__icon header__controls__favorites__icon"></div><div class="header__controls__title header__controls__favorites__title">{{ __('interface.buttons.favorite') }}</div></div>
						<div class="header__controls__settings"><div class="header__controls__icon header__controls__settings__icon"></div><div class="header__controls__title header__controls__settings__title">{{ __('interface.buttons.settings') }}</div></div>
					</div>
				</div>
			</div>
		</header>
		<div class="headerNavigation">
			<div class="container">
				<div class="headerNavigation__main">
					<div class="headerNavigation__main__item active" data-content-block="1">Продукты питания</div>
					<div class="headerNavigation__main__item" data-content-block="2">Мебель</div>
				</div>
				<div class="headerNavigation__content">
					<div class="headerNavigation__content__block active" id="headerNavigation__content__block-1">
						<div class="headerNavigation__content__title">Продукты питания</div>
						<div class="headerNavigation__content__items">
							<div class="headerNavigation__content__item">
								<div class="headerNavigation__content__item__title">Кондитерские изделия</div>
								<div class="headerNavigation__content__item__list">
									<ul>
										<li><a href="#">Шоколад</a></li>
										<li><a href="#">Конфеты</a></li>
										<li><a href="#">Камедь</a></li>
										<li><a href="#">Жидкая конфета</a></li>
									</ul>
								</div>
							</div>
							<div class="headerNavigation__content__item">
								<div class="headerNavigation__content__item__title">Приправы и специи</div>
								<div class="headerNavigation__content__item__list">
									<ul>
										<li><a href="#">Специи и продукты из трав</a></li>
										<li><a href="#">Сахар</a></li>
										<li><a href="#">Глутамат натрия</a></li>
										<li><a href="#">Соус</a></li>
										<li><a href="#">Соевый соус</a></li>
										<li><a href="#">Приправы из морепродуктов</a></li>
										<li><a href="#">Соленья</a></li>
										<li><a href="#">Соль</a></li>
									</ul>
								</div>
							</div>
							<div class="headerNavigation__content__item">
								<div class="headerNavigation__content__item__title">Морепродукты</div>
								<div class="headerNavigation__content__item__list">
									<ul>
										<li><a href="#">Рыба</a></li>
										<li><a href="#">Морские водоросли</a></li>
										<li><a href="#">Кальмар</a></li>
										<li><a href="#">Омар</a></li>
										<li><a href="#">Креветка</a></li>
										<li><a href="#">Косуля</a></li>
										<li><a href="#">Краб</a></li>
										<li><a href="#">Моллюск</a></li>
									</ul>
								</div>
							</div>
							<div class="headerNavigation__content__item">
								<div class="headerNavigation__content__item__title">Чай</div>
								<div class="headerNavigation__content__item__list">
									<ul>
										<li><a href="#">Чай для похудения</a></li>
										<li><a href="#">Черный чай</a></li>
										<li><a href="#">Зеленый чай</a></li>
										<li><a href="#">Ароматизированный чай</a></li>
										<li><a href="#">Чай улун</a></li>
										<li><a href="#">Чай пуэр</a></li>
										<li><a href="#">Цветущий чай</a></li>
										<li><a href="#">Белый чай</a></li>
									</ul>
								</div>
							</div>
							<div class="headerNavigation__content__item">
								<div class="headerNavigation__content__item__title">Зерновые продукты</div>
								<div class="headerNavigation__content__item__list">
									<ul>
										<li><a href="#">Лапша</a></li>
										<li><a href="#">Макаронные изделия</a></li>
										<li><a href="#">Мука</a></li>
										<li><a href="#">Рисовая лапша</a></li>
										<li><a href="#">Крупа</a></li>
										<li><a href="#">Крупнозернистые продукты</a></li>
										<li><a href="#">Китайская закуска</a></li>
										<li><a href="#">Глютен</a></li>
									</ul>
								</div>
							</div>
							<div class="headerNavigation__content__item">
								<div class="headerNavigation__content__item__title">Фруктовые продукты</div>
								<div class="headerNavigation__content__item__list">
									<ul>
										<li><a href="#">Сухофрукты</a></li>
										<li><a href="#">Замороженные фрукты</a></li>
										<li><a href="#">Консервированные продукты</a></li>
									</ul>
								</div>
							</div>
							<div class="headerNavigation__content__item">
								<div class="headerNavigation__content__item__title">Молочные продукты</div>
								<div class="headerNavigation__content__item__list">
									<ul>
										<li><a href="#">Масло</a></li>
										<li><a href="#">Сыр</a></li>
										<li><a href="#">Сухое молоко</a></li>
										<li><a href="#">Молоко</a></li>
										<li><a href="#">Мороженое</a></li>
										<li><a href="#">Йогурт</a></li>
										<li><a href="#">Сгущеное молоко</a></li>
										<li><a href="#">Молочный экстракт</a></li>
									</ul>
								</div>
							</div>
							<div class="headerNavigation__content__item">
								<div class="headerNavigation__content__item__title">Безалкогольные напитки</div>
								<div class="headerNavigation__content__item__list">
									<ul>
										<li><a href="#">Фруктово-овощной сок</a></li>
										<li><a href="#">Энергетические напитки</a></li>
										<li><a href="#">Зерновой и ореховый сок</a></li>
										<li><a href="#">Чайные напитки</a></li>
										<li><a href="#">Газированные напитки</a></li>
										<li><a href="#">Кофейные напитки</a></li>
										<li><a href="#">Растворимые и порошковые напитки</a></li>
									</ul>
								</div>
							</div>
						</div>
					</div>
					<div class="headerNavigation__content__block" id="headerNavigation__content__block-2">
						<div class="headerNavigation__content__title">Мебель</div>
						<div class="headerNavigation__content__items">
							<div class="headerNavigation__content__item">
								<div class="headerNavigation__content__item__title">Мягкая мебель</div>
								<div class="headerNavigation__content__item__list">
									<ul>
										<li><a href="#">Кресла-мешки</a></li>
										<li><a href="#">Кресла офисные</a></li>
										<li><a href="#">Игровые кресла</a></li>
										<li><a href="#">Кресла</a></li>
										<li><a href="#">Пуфики и банкетки</a></li>
										<li><a href="#">Кровати и матрасы</a></li>
										<li><a href="#">Диваны</a></li>
										<li><a href="#">Кушетки</a></li>
										<li><a href="#">Надувная мебель</a></li>
										<li><a href="#">Подвесные кресла</a></li>
										<li><a href="#">Комплекты мягкой мебели</a></li>
									</ul>
								</div>
							</div>
							<div class="headerNavigation__content__item">
								<div class="headerNavigation__content__item__title">Столы и стулья</div>
								<div class="headerNavigation__content__item__list">
									<ul>
										<li><a href="#">Компьютерные и письменные столы</a></li>
										<li><a href="#">Столы</a></li>
										<li><a href="#">Стулья</a></li>
										<li><a href="#">Табуретки</a></li>
										<li><a href="#">Барные стулья</a></li>
										<li><a href="#">Журнальные столы</a></li>
										<li><a href="#">Туалетные столики</a></li>
										<li><a href="#">Трюмо, консоли</a></li>
									</ul>
								</div>
							</div>

						</div>
					</div>
				</div>
			</div>
		</div>
		<main>
--}}

<header class="header header-fullwidth">
    <div class="header header__top">
        <div class="container">
            <div class="header__burger">
                <div class="header__burger__icon" data-toggle="modal" data-target="#sidebar-modal"></div>
                <a href="{{ route('main') }}" class="header__logo"></a>
            </div>
            
            
            @section('search')
                @include ('widgets.search', ['route' => route('search.index'), 'searchButtonType' => 'submit'])
            @show
            

            <div class="header__controls">
                <div class="header__controls__menu">
                    <ul class="header__controls__menu__topmenu">
                        <li>
                            <a>
                                <div class="header__controls__user">
                                    <div 
                                        class="header__controls__avatar header__controls__user__avatar" 
                                        @if($user->downloads->where('type', 'avatar')->first())
                                        style="background-image: url('{{ asset('storage/' . $user->downloads->where('type', 'avatar')->first()->path ?? '') }}');"
                                        @endif
                                    >
                                    </div>
                                    <div class="header__controls__title header__controls__user__title">{{ $user->name }}</div>
                                </div>
                            </a>
                            <ul class="header__controls__menu__submenu">
                                <li>
                                    <a href="{{ route('cabinet.home') }}">{{ __('interface.buttons.cabinet') }}</a>
								</li>
                                <li>
                                    <a href="{{ route('cabinet.favorite.index', ['user' => $user, 'type' => 'product']) }}">{{ __('interface.buttons.favorite') }}</a>
                                </li>
								@if(session()->get('active_profile'))
                                <li>
                                    <a href="{{ route('cabinet.chat.index', ['profile'=>session()->get('active_profile')->id,'tab'=>'contact']) }}">{{ __('interface.buttons.contacts') }}</a>
                                </li>
                                @endif
                                <li class="header__controls__menu__submenu__exit">
                                    <a href="{{ route('logout') }}">{{ __('interface.buttons.logout') }}</a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>

                <div class="header__controls__menu">
                    <ul class="header__controls__menu__topmenu">
                        <li>
                            <a>
                                <div class="header__controls__user">
                                    @if(session()->get('active_profile'))
                                        @php $profile = session()->get('active_profile') @endphp
                                        <div class="header__controls__avatar flag flag-{{ $profile->profileProperties->first()->localisations->code }}"></div>
                                        <div class="header__controls__title header__controls__title__company">{{ $profile->profileProperties->where('code', 'company_name')->first()->value ?? ''}}</div>
                                    @else
                                        <div class="header__controls__icon header__controls__profiles__icon"></div>
                                        <div class="header__controls__title">{{__('interface.header.my_profiles')}}</div>
                                    @endif
                                </div>
                            </a>

                            @if($profiles && $profiles->first())
                            <ul class="header__controls__menu__submenu header__controls__menu__submenu__profiles">
                                @foreach ($profiles as $type => $items)
                                    <li class="header__controls__menu__submenu__title">{{ __("sidebars.$type") }}</li>
                                    @foreach ($items as $item)
                                    <li>
                                        <div class="flag flag-{{ $item->profileProperties->first()->localisations->code }}"></div>
                                        <a class="header__controls__menu__submenu__profile @if( isset($profile) && $profile->id === $item->id ) active @endif" href="{{route('cabinet.profile.select', ['profile' => $item->id])}}">{{ $item->profileProperties->where('code', 'company_name')->first()->value ?? ''}}</a>
                                    </li>
                                    @endforeach
                                @endforeach
                            </ul>
                            @else
                            <ul class="header__controls__menu__submenu">
                                <li>
                                    <a href="{{ route('cabinet.profile.index', ['type' => 'buyer']) }}">{{ __('interface.header.buyer') }}</a>
                                </li>
                                <li>
                                    <a href="{{ route('cabinet.profile.index', ['type' => 'seller']) }}">{{ __('interface.header.seller') }}</a>
                                </li>
                            </ul>
                            @endif

                        </li>
                    </ul>
                </div>

                <div class="header__controls__menu">
                    <ul class="header__controls__menu__topmenu">
                        <li>
                            <a>
                                <div class="header__controls__settings">
                                    <div class="header__controls__icon header__controls__settings__icon"></div>
                                    <div class="header__controls__title header__controls__settings__title">{{ __('interface.buttons.settings') }}</div>
                                </div>
                            </a>
                            <ul class="header__controls__menu__submenu">
                                <div class="header__controls__menu__submenu__settings">
                                    <div class="header__controls__menu__submenu__select">
                                        <select id="set-locale">
                                            @foreach($localisations as $localisation)
                                            <option 
                                                @if(session()->get('locale') === $localisation->code) 
                                                    selected 
                                                @endif   
                                                value="{{ route('localisation', ['localisation' => $localisation->code]) }}"
                                            >
                                                {{ __('interface.buttons.localisation.' . $localisation->code) }}
                                            </option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </ul>
                        </li>
                    </ul>
                </div>
                
            </div>
            
        </div>
    </div>   
</header>