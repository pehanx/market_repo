<footer class="footer footer-fullwidth">
    {{-- <div class="footer__top">
        <div class="container">
            <div class="footer__top__left">
                <a href="/" class="footer__logo"></a>
            </div>
            <div class="footer__top__right">
                <nav class="navigation navigation__footer navigation__footer__top__left">
                    <ul>
                        <li><a href="#">{{ __('interface.footer.search_request') }}</a></li>
                        <li><a href="#">{{ __('interface.footer.logistical_service') }}</a></li>
                        <li><a href="#">English</a></li>
                    </ul>
                </nav>
            </div>
        </div>
    </div>
    <div class="footer__middle">
        <div class="container">
            <div class="footer__middle__left">
                <div class="footer__middle__navblock">
                    <ul>
                        <li class="footer__middle__navblock__title">{{ __('interface.footer.company') }}</li>
                        <li><a href="#">{{ __('interface.footer.about_company') }}</a></li>
                        <li><a href="#">{{ __('interface.footer.services') }}</a></li>
                        <li><a href="#">{{ __('interface.footer.our_services') }}</a></li>
                    </ul>
                </div>
                <div class="footer__middle__navblock">
                    <ul>
                        <li class="footer__middle__navblock__title">{{ __('interface.footer.help') }}</li>
                        <li><a href="#">{{ __('interface.footer.site_instruction') }}</a></li>
                        <li><a href="#">{{ __('interface.footer.qa') }}</a></li>
                        <li><a href="#">{{ __('interface.footer.contact_us') }}</a></li>
                    </ul>
                </div>
            </div>
            <div class="footer__middle__right">
                <div class="footer__middle__navblock">
                    <ul>
                        <li class="footer__middle__navblock__title">{{ __('interface.footer.documents') }}</li>
                        <li><a href="#">{{ __('interface.footer.terms') }}</a></li>
                        <li><a href="#">{{ __('interface.footer.privacy') }}</a></li>
                        <li><a href="#">{{ __('interface.footer.personal') }}</a></li>
                    </ul>
                </div>
                <div class="footer__middle__navblock">
                    <ul>
                        <li class="footer__middle__navblock__title">{{ __('interface.footer.contacts') }}</li>
                        <li><a href="tel:88009751800">8-800-975-18 00</a></li>
                        <li><a href="mailto:market@info.com">market@info.com</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div> --}}
    <div class="footer__bottom">
        <div class="container">
            <p>{{ date('Y') }} &copy; Market {{ __('interface.footer.copyrights') }}</p>
        </div>
    </div>
</footer>