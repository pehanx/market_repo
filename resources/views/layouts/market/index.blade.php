@section('header')
<html lang="{{ $htmlLang }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=1200">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
        <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
        <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
        <link rel="manifest" href="/site.webmanifest">
        <link rel="mask-icon" href="/safari-pinned-tab.svg" color="#5bbad5">
        <meta name="msapplication-TileColor" content="#da532c">
        <meta name="theme-color" content="#ffffff">

        <title>
            @if(View::hasSection('title'))
                @yield('title')
            @else
                {{ config('app.name') }}
            @endif
        </title>

        <!-- Scripts -->
        <script src="{{ mix('js/manifest.js', 'assemble')}}"></script>
        <script src="{{ mix('js/vendor.js', 'assemble')}}"></script>
        <!-- Styles -->
        <link href="{{ mix('css/main.css', 'assemble') }}" rel="stylesheet">
        @yield('meta')
    </head>
    
    <body>
        <!-- Прелоадер -->

    <div class="preloader">
        <img src="/../assemble/images/spinner/spinner.svg" class="preloader__logo">
        <div class="loadingio-spinner-eclipse-indrmqmaclg preloader__image"><div class="ldio-2ak7hgotj2i"><div></div></div></div>
    </div>
    @if( !(bool)strstr(\Route::currentRouteName(), 'catalog'))
        @if(!(bool)strstr(\Route::currentRouteName(), 'shop'))
            @include('layouts.market.header')
        @endif
    @endif
@show
        <main>
            @yield('content') 
        </main>
         
        @section('footer')
            @include('layouts.market.footer') 
            @include('modals.info')
        @show
        
        <script src="{{ mix('js/app.js', 'assemble') }}"></script>
        <script src="{{ mix('js/index.js', 'assemble') }}"></script>
        <script src="{{ mix('js/main.js', 'assemble') }}"></script>
        <script src="{{ mix('js/Notify.js', 'assemble') }}"></script>
        <script src="{{ mix('js/Info.js', 'assemble') }}"></script>
        <script>
            document.addEventListener('DOMContentLoaded', function() {
                
                let message = @if(Session::has('message')) @json(Session::get('message'), JSON_FORCE_OBJECT) @else 'none' @endif;
                
                if(message !== 'none')
                {
                    let notify = new Notify();
                    notify.show(message.body, message.title, message.type);
                }
                
                let infoMessage = @if(Session::has('info_message')) @json(Session::get('info_message'), JSON_FORCE_OBJECT) @else 'none' @endif;
                
                if(infoMessage !== 'none')
                {
                    let infoModal = new Info($('#info-modal'));
                    infoModal.show(infoMessage.body, infoMessage.title, infoMessage.type);
                }
                
            });
        </script>
        @yield('scripts')
    </body>
</html>