<header class="header">
    
  <!-- Основной контент страницы -->
    <div class="header header__top header__main">
        <div class="container">
            <div class="header__main__left">
                <a href="{{ route('main') }}" class="header__logo"></a>
                <div class="header__bottom__left">
                    <div class="header__bottom__left__icon"></div>
                    <div class="header__bottom__left__title">{{ __('interface.header.catalog') }}</div>
                </div>
            </div>

            <div class="header__main__center">


            @section('search')
                @include ('widgets.search', ['route' => route('search.index'), 'searchButtonType' => 'submit'])
            @show
                <div class="header__bottom__right">
                    <nav class="navigation navigation__header navigation__header__bottom">
                        <ul>
                            <li><a href="{{ route('catalog.request_product.index') }}">{{ __('interface.header.buy') }}</a></li>
{{--                            <li><a href="/services">{{ __('interface.header.services') }}</a></li>--}}
{{--                            <li><a href="/for-suppliers">{{ __('interface.header.to_suppliers') }}</a></li>--}}
{{--                            <li><a href="/help">{{ __('interface.header.help') }}</a></li>--}}
                        </ul>
                    </nav>
                </div>
            </div>

            @guest
            <div class="header__controls">
                <div class="header__controls__menu">
                    <ul class="header__controls__menu__topmenu">
                        <li>
                            <a>
                            <div class="header__controls__user">
                                <div class="header__controls__icon header__controls__user__icon"></div>
                                <div class="header__controls__title header__controls__user__title">{{ __('interface.buttons.login') }}</div>
                            </div>
                            </a>
                            <ul class="header__controls__menu__submenu">
                                <li><a href="{{ route('login') }}">{{ __('interface.buttons.login') }}</a></li>
                                <li><a href="{{ route('register') }}">{{ __('interface.buttons.register') }}</a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
                <div class="header__controls__menu">
                    <ul class="header__controls__menu__topmenu">
                        <li>
                            <a>
                                <div class="header__controls__settings">
                                    <div class="header__controls__icon header__controls__settings__icon"></div>
                                    <div class="header__controls__title header__controls__settings__title">{{ __('interface.buttons.settings') }}</div>
                                </div>
                            </a>
                            <ul class="header__controls__menu__submenu">
                                <div class="header__controls__menu__submenu__settings">
                                    <div class="header__controls__menu__submenu__select">
                                        <select id="set-locale">
                                            @foreach($localisations as $localisation)
                                            <option 
                                                @if(session()->get('locale') === $localisation->code) 
                                                    selected 
                                                @endif   
                                                value="{{ route('localisation', ['localisation' => $localisation->code]) }}"
                                            >
                                                {{ __('interface.buttons.localisation.' . $localisation->code) }}
                                            </option>
                                            @endforeach
                                        </select>
                                    </div>
                                    @if($currencies->first())
                                    <div class="header__controls__menu__submenu__settings">
                                        <div class="header__controls__menu__submenu__select">
                                            <select id="set-currency">
                                                @foreach($currencies as $currency)
                                                <option 
                                                    @if(session()->get('currency') === $currency->code) 
                                                        selected 
                                                    @endif   
                                                    value="{{ route('currency', ['currency' => $currency->code]) }}"
                                                >
                                                    {{ __('interface.buttons.currency.' . $currency->code) }}
                                                </option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    @endif
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
            @endguest

            @auth
            <div class="header__controls">
                <div class="header__controls__menu">
                    <ul class="header__controls__menu__topmenu">
                        <li>
                            <a>
                                <div class="header__controls__user">
                                    <div 
                                        class="header__controls__avatar header__controls__user__avatar" 
                                        @if($user->downloads->where('type', 'avatar')->first())
                                        style="background-image: url('{{ asset('storage/' . $user->downloads->where('type', 'avatar')->first()->path ?? '') }}');"
                                        @endif
                                    >
                                    </div>
                                        <div class="header__controls__title header__controls__user__title">{{ $user->name }}</div>
                                </div>
                            </a>
                            <ul class="header__controls__menu__submenu">
                                <li>
                                    <a href="{{ route('cabinet.home') }}">{{ __('interface.buttons.cabinet') }}</a>
                                </li>
                                <li>
                                    <a href="{{ route('cabinet.favorite.index', ['user' => $user, 'type' => 'product']) }}">{{ __('interface.buttons.favorite') }}</a>
                                </li>
                                @if(session()->get('active_profile'))
                                    <li>
                                        <a href="{{ route('cabinet.chat.index', ['profile'=>session()->get('active_profile')->id,'tab'=>'contact']) }}">{{ __('interface.buttons.contacts') }}</a>
                                    </li>
                                @endif
                                <li class="header__controls__menu__submenu__exit">
                                    <a href="{{ route('logout') }}">{{ __('interface.buttons.logout') }}</a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>

                <div class="header__controls__menu">
                    <ul class="header__controls__menu__topmenu">
                        <li>
                            <a>
                                <div class="header__controls__user">
                                    @if(session()->get('active_profile'))
                                        @php $profile = session()->get('active_profile') @endphp
                                        <div class="header__controls__avatar flag flag-{{ $profile->profileProperties->first()->localisations->code }}"></div>
                                        <div class="header__controls__title header__controls__title__company">{{ $profile->profileProperties->where('code', 'company_name')->first()->value ?? ''}}</div>
                                    @else
                                        <div class="header__controls__icon header__controls__profiles__icon"></div>
                                        <div class="header__controls__title">{{__('interface.header.my_profiles')}}</div>
                                    @endif
                                </div>
                            </a>

                            @if($profiles && $profiles->first())
                            <ul class="header__controls__menu__submenu header__controls__menu__submenu__profiles">
                                @foreach ($profiles as $type => $items)
                                    <li class="header__controls__menu__submenu__title">{{ __("sidebars.$type") }}</li>
                                    @foreach ($items as $item)
                                    <li>
                                        <div class="flag flag-{{ $item->profileProperties->first()->localisations->code }}"></div>
                                        <a class="header__controls__menu__submenu__profile @if( isset($profile) && $profile->id === $item->id ) active @endif" href="{{route('cabinet.profile.select', ['profile' => $item->id])}}">{{ $item->profileProperties->where('code', 'company_name')->first()->value ?? ''}}</a>
                                    </li>
                                    @endforeach
                                @endforeach
                            </ul>
                            @else
                            <ul class="header__controls__menu__submenu">
                                <li>
                                    <a href="{{ route('cabinet.profile.index', ['type' => 'buyer']) }}">{{ __('interface.header.buyer') }}</a>
                                </li>
                                <li>
                                    <a href="{{ route('cabinet.profile.index', ['type' => 'seller']) }}">{{ __('interface.header.seller') }}</a>
                                </li>
                            </ul>
                            @endif

                        </li>
                    </ul>
                </div>

                <div class="header__controls__menu">
                    <ul class="header__controls__menu__topmenu">
                        <li>
                            <a>
                                <div class="header__controls__settings">
                                    <div class="header__controls__icon header__controls__settings__icon"></div>
                                    <div class="header__controls__title header__controls__settings__title">{{ __('interface.buttons.settings') }}</div>
                                </div>
                            </a>
                            <ul class="header__controls__menu__submenu">
                                <div class="header__controls__menu__submenu__settings">
                                    <div class="header__controls__menu__submenu__select">
                                        <select id="set-locale">
                                            @foreach($localisations as $localisation)
                                            <option 
                                                @if(session()->get('locale') === $localisation->code) 
                                                    selected 
                                                @endif   
                                                value="{{ route('localisation', ['localisation' => $localisation->code]) }}"
                                            >
                                                {{ __('interface.buttons.localisation.' . $localisation->code) }}
                                            </option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                @if($currencies->first())
                                <div class="header__controls__menu__submenu__settings">
                                    <div class="header__controls__menu__submenu__select">
                                        <select id="set-currency">
                                            @foreach($currencies as $currency)
                                            <option 
                                                @if(session()->get('currency') === $currency->code) 
                                                    selected 
                                                @endif   
                                                value="{{ route('currency', ['currency' => $currency->code]) }}"
                                            >
                                                {{ __('interface.buttons.currency.' . $currency->code) }}
                                            </option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                @endif
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
            @endauth
            
        </div>
    </div>
    
    @section('header-bottom')
{{--    <div class="header header__bottom">--}}
{{--        <div class="container">--}}
{{--            <div class="header__bottom__left">--}}
{{--                <div class="header__bottom__left__icon"></div>--}}
{{--                <div class="header__bottom__left__title">{{ __('interface.header.catalog') }}</div>--}}
{{--            </div>--}}
{{--            <div class="header__bottom__right">--}}
{{--                <nav class="navigation navigation__header navigation__header__bottom">--}}
{{--                    <ul>--}}
{{--                        <li><a href="{{ route('catalog.request_product.index') }}">{{ __('interface.header.buy') }}</a></li>--}}
{{--                            <li><a href="/services">{{ __('interface.header.services') }}</a></li>--}}
{{--                        <li><a href="/for-suppliers">{{ __('interface.header.to_suppliers') }}</a></li>--}}
{{--                            <li><a href="/help">{{ __('interface.header.help') }}</a></li>--}}
{{--                    </ul>--}}
{{--                </nav>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </div>--}}
    @show
    
</header>
<div class="headerNavigation">
    <div class="container">
        @if($mainMenu && $appLocId)
        <div class="headerNavigation__main">
            @foreach($mainMenu as $key => $category)
            <div class="headerNavigation__main__item {{ $key === 0 ? 'active' : '' }}" data-content-block="{{ $key }}">{{ $category->categoryProperties->where('code', 'name')->where('localisation_id', $appLocId)->first()->value }}</div>
            @endforeach
            <a href="/catalog">{{ __('interface.catalog.all_categories') }}</a>
        </div>
        <div class="headerNavigation__content">
            @foreach($mainMenu as $keyDepthOne => $categoryDepthOne)
            <div class="headerNavigation__content__block active" id="headerNavigation__content__block-{{$keyDepthOne}}">
                <div class="headerNavigation__content__title">{{ $categoryDepthOne->categoryProperties->where('code', 'name')->where('localisation_id', $appLocId)->first()->value }}</div>
                <div class="headerNavigation__content__items">
                    @if($categoryDepthOne->children->first())
                        @foreach($categoryDepthOne->children as $keyDepthTwo => $categoryDepthTwo)
                        <div class="headerNavigation__content__item">
                            <div class="headerNavigation__content__item__title">{{ $categoryDepthTwo->categoryProperties->where('code', 'name')->where('localisation_id', $appLocId)->first()->value }}</div>
                            <div class="headerNavigation__content__item__list">
                                <ul>
                                @if($categoryDepthTwo->children->first())
                                    @foreach($categoryDepthTwo->children as $keyDepthTree => $categoryDepthTree)
                                        <li><a href="{{ $categoryDepthTree->link }}">{{ $categoryDepthTree->categoryProperties->where('code', 'name')->where('localisation_id', $appLocId)->first()->value }}</a></li>
                                    @endforeach
                                @endif
                                </ul>
                            </div>
                        </div>
                        @endforeach
                    @endif
                </div>
            </div>
            @endforeach
        </div>
        @endif
    </div>
</div>