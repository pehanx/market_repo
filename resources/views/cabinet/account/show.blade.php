@extends('cabinet.home')

@section('sidebar')
    @include ('cabinet.sidebar', ['page' => 'account'])
@endsection

@section('cabinet-content')
    <div class="col-12 col-md-9">
        @section('breadcrumbs', Breadcrumbs::render())
        @yield('breadcrumbs')
        <div class="card">
            <div class="card-body"> 
            
                @if($src = $user->downloads->where('type', 'avatar')->first())
                <div class="form-group row mb-5">
                    <div class="col-6 col-md-3 align-self-center">
                        {{ __('cabinet.account.form_image') }}
                    </div>
                    <div class="col-6 col-md-9">
                        <img class="rounded-circle ml-md-3"  width="150" height="150" src="{{ asset('storage/'.$src->path) }}">
                    </div>
                </div>   
                @endif
                
                @if($account)
                    @foreach ($account->profileProperties as $profileProperty)
                    <div class="form-group row">
                        <div class="col-6 col-md-4">
                            {{ __('cabinet.account.form_'.$profileProperty->code) }}
                        </div>
                        <div class="col-6 col-md-8">
                            @if($profileProperty->code === 'gender')
                                {{ __('cabinet.account.form_'.$profileProperty->value) }}
                            @else
                                {{ $profileProperty->value }}
                            @endif
                        </div>
                    </div>
                    @endforeach
                @endif
                   
                <div class="form-group row">
                    <div class="col-6 col-md-4">
                        {{ __('cabinet.account.name') }}
                    </div>
                    <div class="col-6 col-md-8">
                        {{ $user->name }}
                    </div>
                </div>
                
                @if($user->email)
                <div class="form-group row">
                    <div class="col-6 col-md-4">
                        {{ __('interface.messages.email') }}
                    </div>
                    <div class="col-6 col-md-8">
                        {{ $user->email }}
                    </div>
                </div>
                @endif

                <hr class="mt-5">
                <div class="form-group row mt-1 ml-1">
                    <a class="btn btn-secondary mt-3" href="{{ route('cabinet.account.edit', $user->id) }}">{{ __('interface.buttons.edit') }}</a>
                </div>
            </div>
        </div>
    </div>
@endsection