@extends('cabinet.home')

@section('sidebar')
    @include ('cabinet.sidebar', ['page' => 'entrance'])
@endsection

{{--
@section('cabinet-content')

    @if($user->socialAccounts->first())
    <div class="form-group row">
        <label for="example-email-input" class="col-12 col-md-4 col-form-label">{{ __('cabinet.account.social') }}</label>
        <div class="col-12 col-md-8">
        
        @foreach($user->socialAccounts as $socialAccount)
            
            @if($socialAccount->social === 'vkontakte')
                <i class="fa fa-vk fa-2x"></i>
            @elseif($socialAccount->social === 'odnoklassniki')
                <i class="fa fa-odnoklassniki fa-2x"></i>
            @endif
        @endforeach
        </div>
    </div>
    @endif

</div>
@endsection
--}}

@section('cabinet-content')
<div class="cabinet">
	@section('breadcrumbs', Breadcrumbs::render())
    @yield('breadcrumbs')
	<div class="cabinet__title">{{ Breadcrumbs::generate()->last()->title }}</div>
    <div class="cabinet__content cabinet__content__personalData">
        <form method="POST" accept-charset="UTF-8" id="user-update-form" enctype="multipart/form-data" action="{{ route('cabinet.account.update_entrance', ['account' => $user->id]) }}">
            @csrf
            <div class="form cabinet__content__personalData__form">
               
                <div class="form__row form__row-required @if($errors->has('email')) {{ 'form__row-error' }} @endif"> <!-- form__row-error form__row-hasText-->
                    <input class="form__row__input" id="email" name="email" type="text" value="{{ old('email', ($user->email) ?? '' ) }}" required>
                    <label for="email" class="form__row__label">{{ __('cabinet.account.email') }}</label>
                    @error('email')
                    <div class="form__row__error">{{ $message }}</div>
                    @enderror
                </div>
                
                @if($needOldPassword)
                <div class="form__row form__row-required @if($errors->has('password_current')) {{ 'form__row-error' }} @endif"> <!-- form__row-error form__row-hasText-->
                    <input class="form__row__input" name="password_current" type="password" required id="password-current">
                    <label for="password-current" class="form__row__label">{{ __('cabinet.account.current_password') }}</label>
                    <div class="form__row__showPassword"></div>
                    @error('password_current')
                    <div class="form__row__error">{{ $message }}</div>
                    @enderror
                </div>
                @endif
                
                <div class="form__row form__row-required @if($errors->has('password')) {{ 'form__row-error' }} @endif"> <!-- form__row-error form__row-hasText-->
                    <input class="form__row__input" name="password" type="password" required id="password"> 
                    <label for="password" class="form__row__label">{{ __('cabinet.account.new_password') }}</label>
                    <div class="form__row__showPassword"></div>
                    @error('password')
                    <div class="form__row__error">{{ $message }}</div>
                    @enderror
                </div>
                
                <div class="form__row form__row-required @if($errors->has('password_confirmation')) {{ 'form__row-error' }} @endif"> <!-- form__row-error form__row-hasText-->
                    <input class="form__row__input" name="password_confirmation" type="password" required id="password-confirmation">
                    <label for="password-confirmation" class="form__row__label">{{ __('cabinet.account.confirm_password') }}</label>
                    <div class="form__row__showPassword"></div>
                    @error('password_confirmation')
                    <div class="form__row__error">{{ $message }}</div>
                    @enderror
                </div>
                <div class="form__row">
                    <button type="submit" class="button button-inline button-primary">{{ __('interface.buttons.save_changes') }}</button>
                </div>
            </div>
        </form>
    </div>
</div>
@endsection