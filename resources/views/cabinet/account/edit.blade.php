@extends('cabinet.home')

@section('sidebar')
    @include ('cabinet.sidebar', ['page' => 'account'])
@endsection

@section('cabinet-content')
<div class="cabinet">
	@section('breadcrumbs', Breadcrumbs::render())
    @yield('breadcrumbs')
	<div class="cabinet__title">{{ Breadcrumbs::generate()->last()->title }}</div>
	<div class="cabinet__content cabinet__content__personalData">
        <form method="POST" accept-charset="UTF-8" id="account-edit-form" enctype="multipart/form-data" action="{{ route('cabinet.account.update', Auth::user()->id) }}">
            @csrf
            @method('PUT')
            <div class="form cabinet__content__personalData__form">

                <div class="form__row">
                    <div class="form__row__uploadImages">
                        @if($avatar = $user->downloads->where('type', 'avatar')->first())
                        <div class="form__row__uploadImages__preview"><!--container-->
                            <div class="form__row__uploadImages__preview__image"><!--wrapper-->
                                <div class="form__row__uploadImages__preview__image__remove" data-input-name="avatar" data-id="{{ $avatar->id }}">
                                </div><!--delete-->
                                <div 
                                    class="form__row__uploadImages__preview__image" 
                                    data-id-file="{{ $avatar->id }}" 
                                    style="background-image: url('{{ asset('storage/'.$avatar->path) }}');"                            
                                ><!--image-->
                                </div>
                            </div>
                        </div>
                        @endif
                        <input id="avatar" name="avatar" class="form__row__uploadFile__input file-handler" type="file" accept="image/*">
                        <label for="avatar" class="form__row__uploadFile__label">
                            <div class="button button-primary-bordered button-inline">{{ __('cabinet.account.form_change_avatar') }}</div>
                        </label>
                    </div>
                </div>
        
                <div class="form__row form__row-required @if($errors->has('name')) {{ 'form__row-error' }} @endif">
                    <input class="form__row__input" name="name" type="text" value="{{ old('name', ($user->name)?? '') }}" id="name" required>
                    <label for="name" class="form__row__label">{{ __('cabinet.account.form_name') }}</label>
                    @error('name')
                    <div class="form__row__error">{{ $message }}</div>
                    @enderror
                </div>
                
                <div class="form__row @if($errors->has('last_name')) {{ 'form__row-error' }} @endif">
                    <input class="form__row__input" name="last_name" type="text" value="{{ old('patronymic', ($account['last_name'])?? '') }}" id="last-name">
                    <label for="last-name" class="form__row__label">{{ __('cabinet.account.form_last_name') }}</label>
                    @error('last_name')
                    <div class="form__row__error">{{ $message }}</div>
                    @enderror
                </div>
                
                <div class="form__row @if($errors->has('patronymic')) {{ 'form__row-error' }} @endif">
                    <input class="form__row__input" name="patronymic" type="text" value="{{ old('patronymic', ($account['patronymic'])?? '') }}" id="patronymic">
                    <label for="patronymic" class="form__row__label">{{ __('cabinet.account.form_patronymic') }}</label>
                    @error('patronymic')
                    <div class="form__row__error">{{ $message }}</div>
                    @enderror
                </div>
                
                <div class="form__row @if($errors->has('birthday')) {{ 'form__row-error' }} @endif"> <!-- form__row-error -->
                    <input class="form__row__input datapicker-handle" name="birthday" type="text" id="datepicker" value="{{ old('birthday', ($account['birthday'])?? '') }}" autocomplete="off">
                    <label for="datepicker" class="form__row__label">{{ __('cabinet.account.form_birthday') }}</label>
                    @error('birthday')
                    <div class="form__row__error">{{ $message }}</div>
                    @enderror
                </div>
                                
                <div class="form__row @if($errors->has('phone')) {{ 'form__row-error' }} @endif"> <!-- form__row-error -->
                    <input class="form__row__input" name="phone" type="text" value="{{ old('phone', ($account['phone'])?? '') }}" id="phone">
                    <label for="phone" class="form__row__label">{{ __('cabinet.account.form_phone') }}</label>
                    @error('phone')
                    <div class="form__row__error">{{ $message }}</div>
                    @enderror
                </div>
                
                <div class="form__row">
                    <label class="form__row__labelRadiobox">{{ __('cabinet.account.form_gender') }}</label>
                    <div class="form__row__radiobox">
                        <input type="radio" class="form__row__radiobox__input" name="gender" id="male" value="man" @if(isset($account['gender']) && $account['gender'] === 'man') {{'checked'}} @endif>
                        <label class="form__row__radiobox__label" for="male">{{ __('cabinet.account.form_gender_man') }}</label>
                    </div>
                    <div class="form__row__radiobox">
                        <input type="radio" class="form__row__radiobox__input" name="gender" id="female" value="women" @if(isset($account['gender']) && $account['gender'] === 'women') {{'checked'}} @endif>
                        <label class="form__row__radiobox__label" for="female">{{ __('cabinet.account.form_gender_women') }}</label>
                    </div>
                </div>
                
                <div class="form__row">
                    <button class="button button-inline button-primary submit-form" type="button">{{ __('interface.buttons.save_changes') }}</button>
                </div>
            </div>
        </form>
	</div>
    
</div>
@endsection

@section('content-script')
<script src="{{ mix('js/account_edit.js', 'assemble') }}"></script>  
<script>
document.addEventListener('DOMContentLoaded', function() {
    $('.datapicker-handle').datepicker({
        language: '{{ $locale }}',
        autoclose: true,
        orientation: "top",
        startView: "days",
        minViewMode: "days",
        format: 'dd.mm.yyyy'
    }); 
});
</script>
@endsection