@extends('cabinet.home')

@section('sidebar')
    @include ('cabinet.sidebar', ['page' => 'exports.create'])
@endsection

@section('cabinet-content')
    <div class="col-12 col-md-9">
        @section('breadcrumbs', Breadcrumbs::render())
        @yield('breadcrumbs')

        <form method="post" action="{{ route('cabinet.export.store.all', ['profile' => $profile->id]) }}">
            @csrf
            <div class="form-group row">
                <label for="example-text-input" class="col-12 col-md-4 col-form-label">{{ __('cabinet.request_product.status') }}</label>
                <div class="col-12 col-md-8">
                    <select name="status">
                        <option value="active">{{ __('interface.tabs.active') }}</option>
                        <option value="inactive">{{ __('interface.tabs.inactive') }}</option>
                        <option value="deleted">{{ __('interface.tabs.deleted') }}</option>
                        <option value="all">{{ __('interface.tabs.all') }}</option>
                    </select>
                </div>
            </div>	
            
            <div class="form-group row">
                <label for="example-text-input" class="col-12 col-md-4 col-form-label">{{ __('cabinet.export.format') }}</label>
                <div class="col-12 col-md-8">
                    <select name="export_format">
                        @foreach ($allowedFormats as $exportFormat)
                            <option value="{{$exportFormat}}">{{$exportFormat}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            
            <div class="form-group row">
                <label for="example-text-input" class="col-12 col-md-4 col-form-label">{{ __('cabinet.export.with_thumbnails') }}</label>
                <div class="col-12 col-md-8">
                    <input type='radio' name='with_thumbnails' id='with_thumbnails' value='true' checked>
                    <input type='radio' name='with_thumbnails' id='with_thumbnails1' value='false'>
                </div>
            </div>

            <button id='send-export-form' type="submit" class="btn btn-secondary">
                {{ __('interface.buttons.export_submit') }}
            </button>
            
            <input type='hidden' name="type" value="request_product">
        </form>
    </div>
@endsection