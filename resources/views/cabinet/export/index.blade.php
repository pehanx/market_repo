@extends('cabinet.home')

@section('sidebar')
    @include ('cabinet.sidebar', ['page' => 'exports'])
@endsection

@section('cabinet-content')
    <div class="col-12 col-md-9">

        @section('breadcrumbs', Breadcrumbs::render())
        @yield('breadcrumbs')

        <div id='exports-table' data-route="{{ route('profile.export.update', ['profile' => $profile->id]) }}">
            <table class="w-100">
                <thead>
                    <th>#</th>
                    <th>{{ __('cabinet.export.progress') }}</th>
                    <th>{{ __('cabinet.export.files') }}</th>
                    <th>Название профиля</th>
                    <th>{{ __('cabinet.export.start') }}</th>
                </thead>
                <tbody>
                    @foreach ($exports as $export)
                    <tr id="export-{{$export->id}}">    
                        <td>{{$export->id}}</td>
                        <td width="400px">
                            <div class="progress">
                                <progress class="w-100" max="100" value="{{$export->progress}}"> {{$export->progress}} </progress>
                            </div>
                        </td>
                        <td>
                            <button id="navbar-drop" class="btn btn-sm btn-light border border-black mr-1 dropdown-toggle" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                {{ __('interface.buttons.download') }}
                            </button>
                            <div class="dropdown-menu border-0" aria-labelledby="navbar-drop">
                                <ul class="navbar-nav">
                                    @if( $export->downloads()->first() )

                                        @foreach ( $export->downloads()->get() as $downloadFiles )
                                        <a class="exports" href="/storage/{{$downloadFiles->path}}">
                                            {{$downloadFiles->title}}
                                        </a>
                                        @endforeach

                                        <button class="btn btn-black download-all" data-link="{{ route('profile.export.download', ['profile'=>$profile->id, 'export' => $export->id]) }}">
                                            {{ __('interface.buttons.download_all') }}
                                        </button>
                                    @else

                                    <button class="btn btn-black download-all d-none" data-link="{{ route('profile.export.download', ['profile'=>$profile->id, 'export' => $export->id]) }}">
                                        {{ __('cabinet.export.download_all') }}
                                    </button>

                                    @endif
                                </ul>
                            </div>
                        </td>
                        <td>Профиль</td>
                        <td>
                            <p>{{ $export->created_at }}</p>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection

@section('content-script')
    <script>
        document.addEventListener('DOMContentLoaded', function() {

            $('#exports-table').on('load',function(){ //Для скролла внутри страницы
                $('#exports-table').css({
                    'overflow' : 'auto', 
                    'height' : window.screen.height/1.5+'px'
                });
            });
            
            $('#exports-table').trigger('load');

            cabinet.init('#exports-table','load','showExportParams');
            cabinet.init('.download-all', 'click', 'downloadExports');

            /* setInterval(() => {
                $('#exports-table').trigger('load');
            }, 45000); */

        });
    </script>
@endsection