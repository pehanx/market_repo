@extends('cabinet.home')

@section('sidebar')
    @include ('cabinet.sidebar', ['page' => 'chats_support'])
@endsection

@section('cabinet-content')
<div class="cabinet">
{{--    @section('breadcrumbs', Breadcrumbs::render())--}}
{{--    @yield('breadcrumbs')--}}
{{--    <div class="cabinet__title">{{ Breadcrumbs::generate()->last()->title }}</div>--}}
    <div class="cabinet__content">
        <div class="cabinet__contacts"
            data-type="support"
            data-domain="{{env('APP_URL')}}"
            data-user="{{auth()->user()->id}}"
            data-support-token="{{$userSupportHash}}"
            data-queue-prefix="{{env('RABBITMQ_PREFIX')}}"
            data-localisation="{{ session()->get('locale') }}"
            data-route-disconnect="{{ route('chat.disconnect') }}"
            data-route-connect="{{ route('chat.connect') }}"
            >
            <div class="contacts__sidebar">
                <div class="sidebar__contact">
                    <div class="sidebar__contact__container-name">
                        <div class="sidebar__contact__container-name__logo online" style="background-image: url('{{ $userAvatar }}');"></div>
                        <div class="sidebar__contact__container-name__name">{{ auth()->user()->name }}</div>
                    </div>
                    {{-- <div class="sidebar__contact__settings"></div> --}}
                </div>
                <div class="sidebar__tabs">
                    <div class="tabs__header d-none"
                        data-route="{{ route('chat.get') }}"
                        >
                        <div class="tabs__header__search disabled"></div>
                        <div class="tabs__header__contacts disabled"></div>
                        <div class="tabs__header__chats active"></div>
                    </div>
                    <div class="tabs__content__chats">

                        {{--                        Отображение чатов в левом сайдабре--}}
                        {{-- <div class="tabs__content__chats__chat" data-route-send="http://market/api/user/chat/support/1/message" data-route-messages="http://market/api/user/chat/support/1/messages" data-profile="undefined" data-chat="1" data-page="1" data-type="support" data-can-load="true">
                            <div class="tabs__content__chats__chat__logo offline" style="background-image: url('http://market/assemble/images/cabinetImages/avatar_default.png');"></div>
                            <div class="tabs__content__chats__chat__container">
                                <div class="tabs__content__chats__chat__name fix">Техническая поддержка</div>
                                <div class="tabs__content__chats__chat__date">вчера</div>
                                <div class="tabs__content__chats__chat__history">aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa</div>
                                <div class="tabs__content__chats__chat__messages-count">8</div>
                                <div class="tabs__content__chats__chat__remove-chat"></div>
                            </div>
                        </div>
                        <div class="tabs__content__chats__chat" data-route-send="http://market/api/user/chat/support/1/message" data-route-messages="http://market/api/user/chat/support/1/messages" data-profile="undefined" data-chat="1" data-page="1" data-type="support" data-can-load="true">
                            <div class="tabs__content__chats__chat__logo offline" style="background-image: url('http://market/assemble/images/cabinetImages/avatar_default.png');"></div>
                            <div class="tabs__content__chats__chat__container">
                                <div class="tabs__content__chats__chat__name fix">Техническая поддержка</div>
                                <div class="tabs__content__chats__chat__date">9 ноя</div>
                                <div class="tabs__content__chats__chat__history">aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa</div>
                                <div class="tabs__content__chats__chat__messages-count">20</div>
                                <div class="tabs__content__chats__chat__remove-chat"></div>
                            </div>
                        </div> --}}
                        {{--                        Отображение чатов в левом сайдабре--}}
                    </div>
                </div>
            </div>
            <div class="contacts__chat empty">
                <div class="chat__message-empty">{{ __('interface.messages.start_conversation') }}</div>
            </div>
            <div class="contacts__chat" style="display:none">
                <div class="chat__header">
                    <div class="chat__header__right">
                        <span class="chat__header__name">
                            <a href="#" target="_blank">{{-- Администрация --}}</a>
                        </span>
                        <span class="chat__header__was-online">{{-- был(а) 4 часа назад --}}</span>
                    </div>
                    <div class="chat__header__left">
                        <div class="chat__header__logo"
                            style="background-image: url('dist/assets/img/temporary/avatar.jpg');"></div>
                    </div>
                </div>
                <div class="chat__body">
                    <div class="chat__content">
                        <div class="chat__content__date">
                            {{-- 9 ноября --}}
                        </div>
                        {{-- <div data-id="3" data-readed="false" data-my="true" data-route-delete="http://market/api/user/chat/1/message/3" class="chat__content__mess-stack">
                            <div class="chat__content__mess-stack__logo online" style="background-image:url('http://market/storage/uploads/7KTNRzqt2SlQLQ0zveCRClMnFWcPXpu4dJApj87p.jpeg')"></div>
                            <div class="chat__content__mess-stack__content">
                                <div class="chat__content__mess-stack__name">pehanx</div>
                                <ul class="mess-stack__messages">
                                    <li class="message__text">
                                        фывфывфы
                                    </li>
                                </ul>
                            </div>
                            <div class="chat__content__mess-stack message__arrow"></div>
                            <div class="chat__content__mess-stack message__time">17:20</div>
                        </div>
                        <div data-id="3" data-readed="false" data-my="true" data-route-delete="http://market/api/user/chat/1/message/3" class="chat__content__mess-stack right unread">

                            <div class="chat__content__mess-stack__logo online" style="background-image:url('http://market/storage/uploads/7KTNRzqt2SlQLQ0zveCRClMnFWcPXpu4dJApj87p.jpeg')"></div>
                            <div class="chat__content__mess-stack__content">
                                <div class="chat__content__mess-stack__name">pehanx</div>
                                <ul class="mess-stack__messages">
                                    <li class="message__text">
                                        фывфывфы
                                    </li>
                                </ul>
                            </div>
                            <div class="chat__content__mess-stack message__arrow"></div>
                            <div class="chat__content__mess-stack message__time">17:20</div>
                        </div>
                        <div data-id="3" data-readed="false" data-my="true" data-route-delete="http://market/api/user/chat/1/message/3" class="chat__content__mess-stack right">
                            <div class="chat__content__mess-stack__logo online" style="background-image:url('http://market/storage/uploads/7KTNRzqt2SlQLQ0zveCRClMnFWcPXpu4dJApj87p.jpeg')"></div>
                            <div class="chat__content__mess-stack__content">
                                <div class="chat__content__mess-stack__name">pehanx</div>
                                <ul class="mess-stack__messages">
                                    <li class="message__text">
                                        фывфывфы
                                    </li>
                                </ul>
                            </div>
                            <div class="chat__content__mess-stack message__arrow"></div>
                            <div class="chat__content__mess-stack message__time">17:21</div>
                        </div> --}}
                    </div>
                </div>
                <div class="chat__input">
                    <div class="chat__input__content">
                        <textarea class="chat__input__text" aria-label="{{ __('cabinet.chat.message_write') }}" placeholder="{{ __('cabinet.chat.message_write') }}"></textarea>
                    </div>
                    <button data-chat="" data-route="" data-profile="" class="chat__input__button">{{ __('interface.buttons.send') }}</button>
                </div>
                <menu class="chat__menu">
                    <li class="chat__menu__menu-item" data-type="translate_settings">
                        {{ __('interface.buttons.translate_settings') }}
                    </li>
                    {{-- <li class="chat__menu__menu-item" data-route="{{route('chat.translate')}}" data-type="translate">
                        {{ __('interface.buttons.translate') }}
                    </li> --}}
                    <li class="chat__menu__menu-item" data-type="delete_message">
                        {{ __('interface.buttons.delete_message') }}
                    </li>
                </menu>
            </div>
        </div>
    </div>
</div>
@include('modals.chat.settings')
@endsection
@section('content-script')
<script src="{{ mix('js/chat.js', 'assemble') }}"></script>
<script>
    document.addEventListener('DOMContentLoaded', function() {
        
    });
</script>
@endsection