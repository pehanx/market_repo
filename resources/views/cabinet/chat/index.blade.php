@extends('cabinet.home')

@section('sidebar')
    @include ('cabinet.sidebar', ['page' => 'chats'])
@endsection

@section('cabinet-content')
<div class="cabinet">
{{--    @section('breadcrumbs', Breadcrumbs::render())--}}
{{--    @yield('breadcrumbs')--}}
{{--    <div class="cabinet__title">{{ __('cabinet.chat.index') }}</div>--}}
    <div class="cabinet__content">
        <div class="cabinet__contacts"
                data-type="{{$chatType}}"
                @if ($chatType === 'with_member')
                data-chat="{{$chatId}}"
                @endif
                data-domain="{{env('APP_URL')}}"
                data-user="{{auth()->user()->id}}"
                data-support-token="{{$userSupportHash}}"
                data-token="{{$userHash}}"
                data-queue-prefix="{{env('RABBITMQ_PREFIX')}}"
                data-id="{{$profile->id}}"
                data-localisation="{{ session()->get('locale') }}"
                data-route-disconnect="{{ route('chat.disconnect') }}"
                data-route-connect="{{ route('chat.connect') }}"
            >
            <div class="contacts__sidebar">
                <div class="sidebar__contact">
                    <div class="sidebar__contact__container-name">
                        <div class="sidebar__contact__container-name__logo online" style="background-image: url('{{ $currentProfileImage }}');"></div>
                        <div class="sidebar__contact__container-name__name">{{ $currentProfileName }}</div>
                    </div>
                    <div class="sidebar__contact__settings"></div>
                </div>
                <div class="sidebar__tabs">
                    <div class="tabs__header"
                        data-route="{{ route('profile.chat.get', ['profile' => $profile]) }}"
                        data-route-contact="{{ route('profile.contact.get', ['profile' => $profile]) }}"
                        >
                        <div class="tabs__header__search @if($activeTab === 'search') active @endif">Поиск</div>
                        <div class="tabs__header__contacts @if($activeTab === 'contact') active @endif">Контакты</div>
                        <div class="tabs__header__chats @if($activeTab === 'chat') active @endif">Чаты</div>
                    </div>
                    <div style='display:none' class="tabs__content__search">
                        <div class="tabs__content__search__container">
                            <!--<input type="text" class="tabs__content__search__container__input" placeholder="Поиск по сообщениям">-->
                            <select data-type="all" data-action="{{ route('profile.chat.search', ['profile' => $profile]) }}" class="tabs__content__search__container__input" id="chat_search" data-type='all'></select>
                        </div>
                    </div>
                    <div style='display:none' class="tabs__content__contacts">

                    </div>
                    <div class="tabs__content__chats">

                    </div>
                </div>
            </div>
            <div class="contacts__chat empty">
                <div class="chat__message-empty">{{ __('interface.messages.start_conversation') }}</div>
            </div>
            <div class="contacts__chat" style="display:none">
                <div class="chat__header">
                    <div class="chat__header__right">
                        <span class="chat__header__name">
                            <a href="#" target="_blank">{{-- Администрация --}}</a>
                        </span>
                        <span class="chat__header__was-online">{{-- был(а) 4 часа назад --}}</span>
                    </div>
                    <div class="chat__header__left">
                        <div class="chat__header__logo"></div>
                    </div>
                </div>
                <div class="chat__body">
                    <div class="chat__content">
                        <div class="chat__content__date">
                            {{-- 9 ноября --}}
                        </div>
                    </div>
                </div>
                <div class="chat__input">
                    <div class="chat__input__content">
                        <textarea class="chat__input__text" aria-label="{{ __('cabinet.chat.message_write') }}" placeholder="{{ __('cabinet.chat.message_write') }}"></textarea>
                    </div>
                    <button data-chat="" data-route="" data-profile="" class="chat__input__button">{{ __('interface.buttons.send') }}</button>
                </div>
                <menu class="chat__menu">
                    <li class="chat__menu__menu-item" data-type="translate_settings">
                        {{ __('interface.buttons.translate_settings') }}
                    </li>
                    <li class="chat__menu__menu-item" data-route="{{route('chat.translate')}}" data-type="translate">
                        {{ __('interface.buttons.translate') }}
                    </li>
                    <li class="chat__menu__menu-item" data-type="delete_message">
                        {{ __('interface.buttons.delete_message') }}
                    </li>
                </menu>
            </div>
        </div>
    </div>
</div>
@include('modals.chat.settings')
@include('modals.destroy')
<div id="destroy-modal-text" class='d-none' data-text-delete-contact="{{ __('modals.your_sure_destroy_contact') }}" data-text-delete-chat="{{ __('modals.your_sure_destroy_chat') }}"></div>
@endsection
@section('content-script')
<script src="{{ mix('js/chat.js', 'assemble') }}"></script>
<script>
    document.addEventListener('DOMContentLoaded', function() {
        
    });
</script>
@endsection