<br>
<div class="cabinet__content__tabs">
    <ul class="tabs__titles">
        <li class="{{ $page === 'active' ? ' tabs__titles__active' : '' }}">
            <a href="{{ route('cabinet.product.index', $profile->id) }}">
                {{ __('interface.tabs.on_sale') }}
            </a>
        </li>
        <li class="{{ $page === 'inactive' ? ' tabs__titles__active' : '' }}">
            <a href="{{ route('cabinet.product.index.inactive', $profile->id) }}">
                {{ __('interface.tabs.inactive') }}
            </a>
        </li>
        <li class="{{ $page === 'deleted' ? ' tabs__titles__active' : '' }}">
            <a href="{{ route('cabinet.product.index.deleted', $profile->id) }}">
                {{ __('interface.tabs.deleted') }}
            </a>
        </li>
    </ul>
</div>