<div class="cabinet__steps">
    <div class="cabinet__steps__content">
        <div class="cabinet__steps__step">
            <div class="cabinet__steps__step__marker {{ $page === 'choose' ? ' cabinet__steps__step__marker-active' : '' }}"></div>
            <div class="cabinet__steps__step__name">{{ __('interface.tabs.choose_category') }}</div>
        </div>
        <div class="cabinet__steps__step">
            <div class="cabinet__steps__step__marker {{ $page === 'fill' ? ' cabinet__steps__step__marker-active' : '' }}"></div>
            <div class="cabinet__steps__step__name">{{ __('interface.tabs.description_product') }}</div>
        </div>
        <div class="cabinet__steps__step">
            <div class="cabinet__steps__step__marker {{ $page === 'show' ? ' cabinet__steps__step__marker-active' : '' }}"></div>
            <div class="cabinet__steps__step__name">{{ __('interface.tabs.show') }}</div>
        </div>
        <div class="cabinet__steps__step">
            <div class="cabinet__steps__step__marker {{ $page === 'add' ? ' cabinet__steps__step__marker-active' : '' }}"></div>
            <div class="cabinet__steps__step__name">{{ __('interface.tabs.add_models') }}</div>
        </div>
    </div>
</div>