@extends('cabinet.home')

@section('sidebar')
    @include ('cabinet.sidebar', ['page' => ''])
@endsection

{{--
@section('cabinet-content')
    <div class="col-12 col-md-9">
    @section('breadcrumbs', Breadcrumbs::render())
    @yield('breadcrumbs')
    
    @if($steps)
        @include ('cabinet.product._steps', ['page' => 'add'])
    @endif
        <div class="row">
            @if($src = $product->downloads->where('type', 'preview_image')->first())
            <img width="200" height="200" src="{{ asset('storage/'.$src->path) }}">   
            @endif
        </div>
        
         <div class="row mt-5">
            @if($srcs = $product->downloads->where('type', 'detail_images')->all())
                @foreach($srcs as $src)
                <img width="50" height="50" src="{{ asset('storage/'.$src->path) }}">   
                @endforeach
            @endif
        </div>
        
        <div class="form-group row mt-3 text-black-50">
            {{ __('cabinet.product.form_articul') }}
        </div> 
        <div class="form-group row">
            {{ $product->articul }}
        </div> 
        
        <div class="form-group row mt-3 text-black-50">
            {{ __('cabinet.product.form_name') }}
        </div> 
        <div class="form-group row">
            {{ $product->productProperties->where('code', 'name')->first()->value ?? '' }}
        </div> 
        
        <div class="form-group row mt-3 text-black-50">
            {{ __('cabinet.product.form_category') }}
        </div> 
        <div class="form-group row">
            {{ $category->categoryProperties->where('code', 'name')->first()->value ?? '' }}
        </div> 
        
        <div class="form-group row mt-3 text-black-50">
            {{ __('cabinet.product.form_price') }}
        </div> 
        <div class="form-group row">
            @php $prices = $product->productPrices->first() @endphp
            {{ $prices->min }} {{ ($prices->max)? ' - '.$prices->min : '' }}
        </div> 
        
        <div class="form-group row mt-3 text-black-50">
            {{ __('cabinet.product.form_min_order') }}
        </div> 
        <div class="form-group row">
            {{ $product->characteristics['min_order']['value'] ?? '' }} {{ $product->characteristics['min_order']['type'] ?? ''}}
        </div> 
        
        <div class="form-group row mt-3 text-black-50">
            {{ __('cabinet.product.form_available') }}
        </div> 
        <div class="form-group row">
            {{ $product->characteristics['available']['value'] ?? '' }} {{ $product->characteristics['available']['type'] ?? '' }}
        </div> 
        
        <div class="form-group row mt-3 text-black-50">
            {{ __('cabinet.product.form_color') }}
        </div> 
        <div class="form-group row">
            <span class="rounded-circle" style="background-color: {{ $product->characteristics['color']['type'] }}">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
        </div> 
        
        <div class="form-group row mt-3 text-black-50">
            {{ __('cabinet.product.form_description') }}
        </div> 
        <div class="form-group row">
            {{ $product->productProperties->where('code', 'description')->first()->value ?? '' }}
        </div> 
        
        <div class="form-group row">
            <h4>{{ __('cabinet.product.form_additional_description') }}</h4>
        </div> 
        
        <div class="form-group row mt-3 text-black-50">
            {{ __('cabinet.product.form_weight') }}
        </div> 
        <div class="form-group row">
            {{ $product->characteristics['weight']['value'] ?? '' }} {{ $product->characteristics['weight']['type'] ?? '' }}
        </div> 
        
        <div class="form-group row mt-3 text-black-50">
            {{ __('cabinet.product.form_lenght') }} x {{ __('cabinet.product.form_width') }} x {{ __('cabinet.product.form_height') }}
        </div> 
        <div class="form-group row">
            {{ $product->characteristics['lenght']['value'] ?? '' }} x {{ $product->characteristics['width']['value'] ?? '' }} x {{ $product->characteristics['height']['value'] ?? '' }} {{ $product->characteristics['lenght']['type'] ?? '' }}
        </div> 
        
        <div class="form-group row">
            <h4>{{ __('cabinet.product.form_seo_attributes') }}</h4>
        </div> 
        
        <div class="form-group row mt-3 text-black-50">
            {{ __('cabinet.product.form_seo_title') }}
        </div> 
        <div class="form-group row">
            {{ $product->productProperties->where('code', 'seo_title')->first() ? $product->productProperties->where('code', 'seo_title')->first()->value : '-'}}
        </div> 
        
        <div class="form-group row mt-3 text-black-50">
            {{ __('cabinet.product.form_seo_keywords') }}
        </div> 
        <div class="form-group row">
            {{ $product->productProperties->where('code', 'seo_keywords')->first() ? $product->productProperties->where('code', 'seo_keywords')->first()->value : '-'}}
        </div> 
        
        <div class="form-group row mt-3 text-black-50">
            {{ __('cabinet.product.form_seo_descriptions') }}
        </div> 
        <div class="form-group row">
            {{ $product->productProperties->where('code', 'seo_descriptions')->first() ? $product->productProperties->where('code', 'seo_descriptions')->first()->value : '-'}}
        </div> 
        
        @if($product->productModels->first())
            <div class="form-group row ml-0">
                <h4>Торговые предложения</h4>
            </div>
            @foreach($product->productModels as $model)
                @if($src = $model->downloads->where('type', 'preview_image')->first())
                <img width="50" height="50" src="{{ asset('storage/'.$src->path) }}">   
                @endif
                
                <div class="form-group row ml-0 mt-3 text-black-50">
                {{ __('cabinet.product.form_articul') }}
                </div> 
                <div class="form-group row ml-0">
                {{ $model->articul }}
                </div> 
                
                <div class="form-group row ml-0 mt-3 text-black-50">
                {{ __('cabinet.product.form_price') }}
                </div> 
                <div class="form-group row ml-0">
                @php $prices = $model->productPrices->first() @endphp
                {{ $prices->min }} {{ ($prices->max)? ' - '.$prices->max.'$' : '$' }}
                </div> 
                
                <div class="form-group row ml-0 mt-3 text-black-50">
                {{ __('cabinet.product.form_available') }}
                </div> 
                <div class="form-group row ml-0">
                {{ $model->characteristics['available']['value'] ?? '—' }} {{ $model->characteristics['available']['type'] ?? '—' }}
                </div> 
                
                <div class="form-group row ml-0 mt-3 text-black-50">
                {{ __('cabinet.product.form_color') }}
                </div> 
                <div class="form-group row ml-0">
                    <span class="rounded-circle" style="background-color: {{ $model->characteristics['color']['type'] }}">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                </div>   
            @endforeach
        @endif

        <hr class="mt-5">
        
        <div class="form-group row">
            <h4>{{ __('cabinet.product.model') }}</h4>
        </div>
        <div class="form-container">
        @include ('widgets.add_product_model', ['colors' => $colors, 'quantities' => $quantities, 'action' => route('cabinet.product.store_model', ['profile' => $profile, 'product' => $product])  ])
        <div>
    </div>
@endsection
--}}

@section('cabinet-content')
<div class="cabinet">
	@section('breadcrumbs', Breadcrumbs::render())
    @yield('breadcrumbs')
	<div class="cabinet__title">{{ Breadcrumbs::generate()->last()->title }}</div>
	
    @if($steps)
        @include ('cabinet.product._steps', ['page' => 'add'])
    @endif

	<div class="cabinet__createProduct cabinet__hideArea">
		
        <div class="cabinet__createProduct__section formView">
			<div class="cabinet__createProduct__wrapper">
				
                <div class="cabinet__subtitle">{{ __('cabinet.product.product') }} — {{ $product->productProperties->where('code', 'name')->first()->value }}  ({{ $product->articul }})</div>
				
                @foreach ($product->downloads as $type => $download)
                    @if($type === 'preview_image')
                        <div class="formView__row" style="margin-top: 30px;">
                            <div class="formView__row__label">{{ __('cabinet.product.form_preview_image') }}</div>
                            <div class="formView__row__image"><img src="{{ asset('storage/'.$download->first()->path) }}" alt=""></div>
                        </div>
                        @continue
                    @endif
                
                <div class="formView__row">
                    <div class="formView__row__label">{{ __('cabinet.product.form_'.$type) }}</div>
                    <div class="formView__row__images">
                        @foreach ($download as $item)
                        <div class="formView__row__images__image" style="background-image: url('{{ asset('storage/'.$item->path) }}');"></div>
                        @endforeach
                    </div>
                </div>
                @endforeach

			</div>
            
		</div>
        
		<div class="cabinet__createProduct__section formView cabinet__hide">
			<div class="cabinet__createProduct__wrapper">
            {{-- <div class="cabinet__subtitle">Основные характеристики</div> --}}
				{{--
                <div class="formView__row" style="margin-top: 30px;">
					<div class="formView__row__label">Название</div>
					<div class="formView__row__value">Юбка летняя</div>
				</div>
                --}}
				<div class="formView__row">
					<div class="formView__row__label">{{ __('cabinet.product.form_category_product') }}</div>
					<div class="formView__row__value">{{ $category->categoryProperties->where('code', 'name')->first()->value }}</div>
				</div>
                
				<div class="formView__row">
					<div class="formView__row__label">{{ __('cabinet.product.form_price') }}</div>
					<div class="formView__row__value">
                        @php $prices = $product->productPrices->first() @endphp
                        {{ $prices->min }} {{ ($prices->max)? ' — '.$prices->max.'$' : '$' }}
                    </div>
				</div>
                
				<div class="formView__row">
					<div class="formView__row__label">{{ __('cabinet.product.form_min_order') }}</div>
					<div class="formView__row__value">
                        {{ $product->characteristics['min_order']['value'] }} {{ $product->characteristics['min_order']['type'] }}
                    </div>
				</div>
                
                <div class="formView__row">
					<div class="formView__row__label">{{ __('cabinet.product.form_available') }}</div>
					<div class="formView__row__value">
                        {{ $product->characteristics['available']['value'] }} {{ $product->characteristics['available']['type'] }}
                    </div>
				</div>
                
				<div class="form__row__selectorColor">
                    <div class="form__row__selectorColor__label">{{ __('cabinet.product.form_color_product') }}</div>
                    <div class="form__row__selectorColor__content">
                        <span class="simplecolorpicker icon" title="{{ $product->characteristics['color']['value'] }}" style="background-color: {{ $product->characteristics['color']['type'] }};"></span>
                    </div>
                </div>
                
                <br>
                
                @if($description = $product->productProperties->where('code', 'description')->first())
				<div class="formView__row">
					<div class="formView__row__label">{{ __('cabinet.product.form_description') }}</div>
					<div class="formView__row__value">{{ $description->value }}</div>
				</div>
                @endif
                
				@if(!empty($product->youtube_frame))
                <div class="formView__row">
                    <div class="formView__row__label">{{ __('cabinet.product.form_video') }}</div>
                    <div class="formView__row__video">
                        {!! $product->youtube_frame !!}
                    </div>
                </div>
                @endif
                
			</div>
		</div>
        
		<div class="cabinet__createProduct__section formView cabinet__hide">
			<div class="cabinet__createProduct__wrapper">
            {{-- <div class="cabinet__subtitle">Дополнительное описание</div> --}}
				
                @isset($product->characteristics['weight'])
                <div class="formView__row" style="margin-top: 30px;">
					<div class="formView__row__label">{{ __('cabinet.product.form_weight') }}</div>
					<div class="formView__row__value">
                        {{ $product->characteristics['weight']['value'] }} {{ $product->characteristics['weight']['type'] }}
                    </div>
				</div>
                @endisset
                
                @if(isset($product->characteristics['lenght']) || isset($product->characteristics['width']) || isset($product->characteristics['height']))
				<div class="formView__row">
					<div class="formView__row__label">
                        {{ __('cabinet.product.form_lenght') }} x {{ __('cabinet.product.form_width') }} x {{ __('cabinet.product.form_height') }}
                    </div>
					<div class="formView__row__value">
                        {{ $product->characteristics['lenght']['value'] ?? '...' }} x {{ $product->characteristics['width']['value'] ?? '...'}} x {{ $product->characteristics['height']['value'] ?? '...' }} {{ $product->characteristics['lenght']['type'] ?? '' }}
                    </div>
				</div>
                @endif
                
			</div>
		</div>
        
        @php
            $seoTitle = $product->productProperties->where('code', 'seo_title')->first();
            $seoKeywords = $product->productProperties->where('code', 'seo_keywords')->first();
            $seoDescriptions = $product->productProperties->where('code', 'seo_descriptions')->first();
        @endphp

        @if($seoTitle || $seoKeywords || $seoDescriptions)
		<div class="cabinet__createProduct__section formView cabinet__hide">
			<div class="cabinet__createProduct__wrapper">
				<div class="cabinet__subtitle">{{ __('cabinet.product.form_seo_attributes') }}</div>
				
                <div class="formView__row" style="margin-top: 20px;">
					<div class="formView__row__label">{{ __('cabinet.product.form_seo_title') }}</div>
					<div class="formView__row__value">{{ (isset($seoTitle) && $seoTitle->value) ? $seoTitle->value : '—' }}</div>
				</div>
                
				<div class="formView__row">
					<div class="formView__row__label">{{ __('cabinet.product.form_seo_keywords') }}</div>
					<div class="formView__row__value">{{ (isset($seoKeywords) && $seoKeywords->value) ? $seoKeywords->value : '—' }}</div>
				</div>
                
				<div class="formView__row">
					<div class="formView__row__label">{{ __('cabinet.product.form_seo_descriptions') }}</div>
					<div class="formView__row__value">{{ (isset($seoDescriptions) && $seoDescriptions->value) ? $seoDescriptions->value : '—' }}</div>
				</div>
                
			</div>
		</div>
        @endif

		<div class="cabinet__unhide" style="margin-top: 30px;">{{ __('interface.buttons.show_completely') }}</div>
	</div>
    
    @if($product->productModels->first())
        <div class="cabinet__subtitle">{{ __('cabinet.product.product_models') }}</div>
        @foreach($product->productModels as $model)
            <div class="cabinet__hideArea">
                <div class="cabinet__createModel__section cabinet__createModel__content formView">
                    <div class="cabinet__subtitle">{{ $product->articul }} — {{ $model->articul }}</div>
                    @if($src = $model->downloads->where('type', 'preview_image')->first())
                    <div class="formView__row" style="margin-top: 20px;">
                        <div class="formView__row__image">
                            <div class="formView__row__images__image" style="background-image: url('{{ asset('storage/'.$src->path) }}');"></div>
                        </div>
                    </div>
                    @endif
                    
                    <div class="formView__row cabinet__hide">
                        <div class="formView__row__label">{{ __('cabinet.product.form_price') }}</div>
                        <div class="formView__row__value">
                            @php $prices = $model->productPrices->first() @endphp
                            {{ $prices->min }} {{ ($prices->max)? ' — '.$prices->max.'$' : '$' }}
                        </div>
                    </div>
                    
                    <div class="formView__row cabinet__hide">
                        <div class="formView__row__label">{{ __('cabinet.product.form_available') }}</div>
                        <div class="formView__row__value">{{ $model->characteristics['available']['value'] }} {{ $model->characteristics['available']['type'] }}</div>
                    </div>
                    
                    <div class="formView__row cabinet__hide">
                        <div class="formView__row__label">{{ __('cabinet.product.form_color') }}</div>
                        <div class="form__row__selectorColor__content">
                            <span class="simplecolorpicker icon" title="{{ $model->characteristics['color']['value'] }}" style="background-color: {{ $model->characteristics['color']['type'] }};"></span>
                        </div>
                    </div>
                    
                </div>
                <div class="cabinet__unhide" style="margin-top: 30px;">{{ __('interface.buttons.show_completely') }}</div>
            </div>
        @endforeach
    @endif
    
    <div class="cabinet__createModel">
        @include (
            'widgets.add_product_model', 
            [
                'colors' => $colors,
                'quantities' => $quantities, 
                'productArticul' => $product->articul,
                'articulPrefix' => $articulPrefix
            ]
        )
    </div>
    
    <div id="button-wrapper" class="cabinet__createProduct__section cabinet__createProduct__section-finish">
        <button id="add-model" class="button button-primary-bordered button-inline">{{ __('interface.buttons.add_more') }}</button>
        <button 
            id="save-models" 
            data-action="{{ route('cabinet.product.store_model', ['profile' => $profile, 'product' => $product]) }}" 
            class="button button-primary button-inline"
        >
            {{ __('interface.buttons.save') }}
        </button>
    </div>
    
</div>
@endsection

@section('content-script')
<script src="{{ mix('js/product_model_add.js', 'assemble') }}"></script>    
@endsection