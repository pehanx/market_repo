@extends('cabinet.home')

@section('sidebar')
    @include ('cabinet.sidebar', ['page' => ''])
@endsection

{{--
    <div class="col-12 col-md-9">

    @section('breadcrumbs', Breadcrumbs::render())
    @yield('breadcrumbs')

    @include ('cabinet.product._steps', ['page' => 'fill'])
    
    <form method="POST" accept-charset="UTF-8" id="product-create-form" class="form-vertical" enctype="multipart/form-data" action="{{ route('cabinet.product.store', ['profile' => $profile]) }}">
            @csrf
            
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
  
            
            <div class="form-group row">
                <div class="col-6 col-md-4">
                    {{ __('cabinet.product.form_category') }}
                </div>
                <div class="col-6 col-md-8">
                    {{ $category->categoryProperties->pluck('value')->first() }}
                </div>
            </div>
        
            
            <div class="form-group row">
                <label for="example-text-input" class="col-12 col-md-4 col-form-label">{{ __('cabinet.product.form_articul') }}</label>
                <div class="col-12 col-md-8">
                    <input class="form-control" name="articul" type="text" value="{{ old('articul') }}" >
                </div>
            </div>	
            
            <div class="form-group row">
                <label for="example-text-input" class="col-12 col-md-4 col-form-label">{{ __('cabinet.product.form_name') }}</label>
                <div class="col-12 col-md-8">
                    <input class="form-control" name="name" type="text" value="{{ old('name') }}" >
                </div>
            </div>	
            
            <div class="form-group row">
                <label for="example-email-input" class="col-12 col-md-4 col-form-label">{{ __('cabinet.product.form_country') }}</label>
                <div class="col-12 col-md-8">
                    <select class="select2 countries w-100" name="country"></select>
                </div>
            </div>
            
            <div class="form-group row">
                <label for="example-text-input" class="col-12 col-md-4 col-form-label">{{ __('cabinet.product.form_min_price') }}, ($)</label>
                <div class="col-12 col-md-8">
                    <input class="form-control" name="min_price" type="text" value="{{ old('min_price') }}" >
                </div>
            </div>	
            
            <div class="form-group row">
                <label for="example-text-input" class="col-12 col-md-4 col-form-label">{{ __('cabinet.product.form_max_price') }}, ($)</label>
                <div class="col-12 col-md-8">
                    <input class="form-control" name="max_price" type="text" value="{{ old('max_price') }}" >
                </div>
            </div>
            
            <div class="form-group row">
                <div class="input-group">
                    <label for="example-text-input" class="col-12 col-md-4 col-form-label">{{ __('cabinet.product.form_min_order') }}</label>
                    <div class="col-12 col-md-8">
                        <div class="row">
                            <div class="col-9">
                                <input type="text" class="form-control" name="min_order[value]">
                            </div>
                            <div class="col-3">
                                <select class="h-100" name="min_order[type]">
                                    @foreach($quantities as $quantity)
                                        <option value="{{ $quantity->code }}">{{ $quantity->value }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="form-group row">
                <div class="input-group">
                    <label for="example-text-input" class="col-12 col-md-4 col-form-label">{{ __('cabinet.product.form_available') }}</label>
                    <div class="col-12 col-md-8">
                        <div class="row">
                            <div class="col-9">
                                <input class="form-control" name="available[value]" type="text" value="" >
                            </div>
                            <div class="col-3">
                                <select class="h-100" name="available[type]">
                                    @foreach($quantities as $quantity)
                                        <option value="{{ $quantity->code }}">{{ $quantity->value }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="form-group row">
                <label for="example-email-input" class="col-12 col-md-4 col-form-label">{{ __('cabinet.product.form_color') }}</label>
                <div class="col-12 col-md-8">
                    <select name="color">
                        @foreach($colors as $color)
                            <option value="{{ $color->code }}">{{ $color->value }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
    
            <div class="form-group row">
                <label for="example-email-input" class="col-12 col-md-4 col-form-label">{{ __('cabinet.product.form_description') }}</label>
                <div class="col-12 col-md-8">
                    <textarea class="form-control" name="description" >{{ old('description') }}</textarea >
                </div>
            </div>
            
            <div class="form-group row mt-3">
                <label class="col-md-4 col-form-label">{{ __('cabinet.product.form_preview_image') }}</label>
                <div class="col-sm-8">
                    <input type="file" class="file-handler" name="preview_image" accept="image/*" value="{{ old('preview_image') }}">
                </div>
            </div>
            
            <div class="form-group row mt-3">
                <label class="col-md-4 col-form-label">{{ __('cabinet.product.form_detail_images') }}</label>
                <div class="col-sm-8">
                    <input type="file" class="file-handler" name="detail_images[]"  multiple accept="image/*" value="{{ old('detail_images[]') }}">
                </div>
            </div>
            
            <div class="form-group row mt-3">
                <label class="col-md-4 col-form-label">{{ __('cabinet.product.form_describle_images') }}</label>
                <div class="col-sm-8">
                    <input type="file" class="file-handler" name="describle_images[]"  multiple accept="image/*" value="{{ old('describle_images[]') }}">
                </div>
            </div>
            
            <hr class="mt-5"></hr>
            
            <div class="video-container">
                <div class="form-group row">
                    <label for="example-text-input" class="col-12 col-md-4 col-form-label">{{ __('cabinet.product.form_youtube') }}</label>
                    <div class="col-12 col-md-8">
                        <input class="form-control video-handler" name="youtube" type="text" value="{{ old('youtube') }}" >
                    </div>
                    <iframe class="video-render d-none" width="580" height="290" frameborder="0"
                        allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                        allowfullscreen>
                    </iframe>
                </div>
            </div>
            
            <h2>{{ __('cabinet.product.form_additional_description') }}</h2>
            
            
            
            <div class="form-group row">
                <div class="input-group">
                    <label for="example-text-input" class="col-12 col-md-4 col-form-label">{{ __('cabinet.product.form_weight') }}</label>
                    <div class="col-12 col-md-8">
                        <div class="row">
                            <div class="col-9">
                                <input class="form-control" name="weight[value]" type="text" value="" >
                            </div>
                            <div class="col-3">
                                <select class="h-100" name="weight[type]">
                                    @foreach($weights as $weight)
                                        <option value="{{ $weight->code }}">{{ $weight->value }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="form-group row">
                <label for="example-text-input" class="col-12 col-md-4 col-form-label">{{ __('cabinet.product.form_lenght') }} x {{ __('cabinet.product.form_width') }} х {{ __('cabinet.product.form_height') }}</label>
                <div class="col-12 col-md-8">
                    <div class="input-group">
                    
                        <input class="form-control mr-3" name="lenght[value]" type="text" value="{{ old('lenght[value]') }}">
                        <input class="form-control mr-3" name="width[value]" type="text" value="{{ old('width[value]') }}">
                        <input class="form-control mr-3" name="height[value]" type="text" value="{{ old('height[value]') }}">
                        
                        <input class="form-control mr-3" name="lenght[type]" type="hidden" value="{{ old('lenght[type]') }}">
                        <input class="form-control mr-3" name="width[type]" type="hidden" value="{{ old('width[type]') }}">
                        <input class="form-control mr-3" name="height[type]" type="hidden" value="{{ old('height[type]') }}">
                        
                        <select class="form-control" id="size-type">
                            @foreach($sizes as $size)
                                <option value="{{ $size->code }}">{{ $size->value }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
            
            <h2>{{ __('cabinet.product.form_seo_attributes') }}</h2>
            
            <div class="form-group row">
                <label for="example-phone-input" class="col-12 col-md-4 col-form-label">{{ __('cabinet.product.form_seo_title') }}</label>
                <div class="col-12 col-md-8">
                    <input class="form-control" placeholder="" name="seo_title" type="text" value="{{ old('seo_title') }}" >
                </div>
            </div>
            
            <div class="form-group row">
                <label for="example-phone-input" class="col-12 col-md-4 col-form-label">{{ __('cabinet.product.form_seo_keywords') }}</label>
                <div class="col-12 col-md-8">
                    <input class="form-control" placeholder="" name="seo_keywords" type="text" value="{{ old('seo_keywords') }}" >
                </div>
            </div>
            
            <div class="form-group row">
                <label for="example-phone-input" class="col-12 col-md-4 col-form-label">{{ __('cabinet.product.form_seo_descriptions') }}</label>
                <div class="col-12 col-md-8">
                    <input class="form-control" placeholder="" name="seo_descriptions" type="text" value="{{ old('seo_descriptions') }}" >
                </div>
            </div>
            
            <hr class="mt-5">
            <div class="form-group row ml-1">
                <button type="button"  class="btn btn-secondary mt-3 submit-form">{{ __('interface.buttons.add_product') }}</button>
            </div>
            
            <input type="hidden" name="category_id" value="{{ $category->id }}">
        </form>
</div> 
--}}

@section('cabinet-content')
<div class="cabinet">
    @section('breadcrumbs', Breadcrumbs::render())
    @yield('breadcrumbs')
    <div class="cabinet__title">{{ Breadcrumbs::generate()->last()->title }}</div>
    
    @include ('cabinet.product._steps', ['page' => 'fill'])
    
    <form method="POST" accept-charset="UTF-8" id="product-create-form" enctype="multipart/form-data" action="{{ route('cabinet.product.store', ['profile' => $profile]) }}">
        @csrf
        <div class="cabinet__createProduct">
            <div class="cabinet__createProduct__section form">
                <div class="cabinet__createProduct__wrapper">
                    
                    <div class="cabinet__subtitle">{{ __('cabinet.product.form_category_product') }}</div>
                
                    <div class="form__row"> 
                        {{ $category->categoryProperties->pluck('value')->first() }}
                    </div>
                
                    <div class="cabinet__subtitle">{{ __('cabinet.product.form_main_properties') }}</div>
                    
                    {{-- <div class="form__row form__row-required @if($errors->has('articul')) {{ 'form__row-error' }} @endif">
                        <input name="articul" type="text" value="{{ old('articul') }}" class="form__row__input" id="articul" required>
                        <label for="articul" class="form__row__label">{{ __('cabinet.product.form_articul') }}</label>
                        @error('articul')
                        <div class="form__row__error">{{ $message }}</div>
                        @enderror
                    </div> --}}
                    
                    <div class="form__row form__row-required @if($errors->has('name')) {{ 'form__row-error' }} @endif">
                        <input name="name" type="text" value="{{ old('name') }}" class="form__row__input" id="name" required>
                        <label for="name" class="form__row__label">{{ __('cabinet.product.form_name') }}</label>
                        @error('name')
                        <div class="form__row__error">{{ $message }}</div>
                        @enderror
                    </div>
                    
                    
                    <div class="form__row form__row-required @if($errors->has('country')) {{ 'form__row-error' }} @endif"> 
                        <select name="country">
                            <option></option>
                            @foreach($countries as $country)
                            <option value="{{ $country['id'] }}"
                            {{ (old('country') === $country['id']) ? 'selected' : '' }}>
                                {{ $country['text'] }}
                            </option>
                            @endforeach
                        </select>
                        @error('country')
                        <div class="form__row__error">{{ $message }}</div>
                        @enderror
                    </div>
                    
                    <div class="form__double__row form__double__row-between">
                        
                        <div class="form__row form__row-required @if($errors->has('min_price')) {{ 'form__row-error' }} @endif">
                            <input name="min_price" type="text" value="{{ old('min_price') }}" class="form__row__input" id="min-price" required>
                            <label for="min-price" class="form__row__label">{{ __('cabinet.product.form_min_price') }}, ($)</label>
                            @error('min_price')
                            <div class="form__row__error">{{ $message }}</div>
                            @enderror
                        </div>
                        
                        <div class="form__row @if($errors->has('max_price')) {{ 'form__row-error' }} @endif">
                            <input name="max_price" type="text" value="{{ old('max_price') }}" class="form__row__input" id="max-price">
                            <label for="max-price" class="form__row__label">{{ __('cabinet.product.form_max_price') }}, ($)</label>
                            @error('max_price')
                            <div class="form__row__error">{{ $message }}</div>
                            @enderror
                        </div>
                        
                    </div>

                    <div class="form__double__row form__double__row-between">
                        
                        
                        
                    </div>
                    
                    <div class="form__double__row form__double__row-between">
                        
                        <div class="form__row form__row-required @if($errors->has('min_order.value')) {{ 'form__row-error' }} @endif">
                            <input type="text" name="min_order[value]" class="form__row__input" id="min-order-value" value="{{ old('min_order.value') }}" required>
                            <label for="min-order-value" class="form__row__label">{{ __('cabinet.product.form_min_order') }}</label>
                            @error('min_order.value')
                            <div class="form__row__error">{{ $message }}</div>
                            @enderror
                        </div>
                        
                        
                        <div class="form__row form__row-required">
                            <select name="min_order[type]">
                                @foreach($quantities as $quantity)
                                <option 
                                    value="{{ $quantity->code }}" 
                                    {{ (old('min_order[type]') === $quantity->code) ? 'selected' : '' }}
                                >
                                    {{ $quantity->value }}
                                </option>
                                @endforeach
                            </select>
                        </div>
                        
                    </div>
                    
                    <div class="form__double__row form__double__row-between">
                        
                        <div class="form__row form__row-required @if($errors->has('available.value')) {{ 'form__row-error' }} @endif">
                            <input type="text" name="available[value]" class="form__row__input" id="available-value" value="{{ old('available.value') }}" required>
                            <label for="available-value" class="form__row__label">{{ __('cabinet.product.form_available') }}</label>
                            @error('available.value')
                            <div class="form__row__error">{{ $message }}</div>
                            @enderror
                        </div>
                        
                        <div class="form__row form__row-required">
                            <select name="available[type]">
                                @foreach($quantities as $quantity)
                                <option 
                                    value="{{ $quantity->code }}" 
                                    {{ (old('available[type]') === $quantity->code) ? 'selected' : '' }}
                                >
                                    {{ $quantity->value }}
                                </option>
                                @endforeach
                            </select>
                        </div>
                        
                    </div>
                    
                    <div class="form__row form__row-required">
                        <div class="form__row__selectorColor">
                            <div class="form__row__selectorColor__label">{{ __('cabinet.product.form_color_product') }}</div>
                            <div class="form__row__selectorColor__content">
                                <select name="color" class="colorpicker-handle">
                                    @foreach($colors as $color)
                                    <option {{(request('color') === $color->code) ? 'selected' : ''}} value="{{ $color->code }}">{{ $color->value }}</option>
                                    @endforeach
                                </select>
                            </div>  
                        </div>
                    </div>
                    
                    <div class="form__row">
                        <label for="description" class="form__row__label form__row__label__textarea">{{ __('cabinet.product.form_description') }}</label>
                        <textarea rows="10" placeholder="{{ __('cabinet.product.form_description') }}" class="form__row__textarea form__row__input" id="description" name="description" >{{ old('description') }}</textarea>
                    </div>
                    
                </div>
            </div>
            <div class="cabinet__createProduct__section form">
                <div class="cabinet__createProduct__wrapper">
                    
                    <div class="cabinet__subtitle">{{ __('cabinet.image') }}</div>
                    
                    <div class="form__row form__row-required @if($errors->has('preview_image')) {{ 'form__row-error' }} @endif">
                        <div class="form__row__uploadImages">
                            <div class="form__row__uploadImages__label">{{ __('cabinet.product.form_preview_image') }}</div>

                            <input id="preview-image" type="file" class="form__row__uploadFile__input file-handler" name="preview_image" accept="image/*">
                            
                            <label for="preview-image" class="form__row__uploadFile__label">
                                <div class="button button-primary-bordered button-inline">{{ __('interface.buttons.choose_image') }}</div>
                            </label>
                        </div>
                        @error('preview_image')
                        <div class="form__row__error">{{ $message }}</div>
                        @enderror
                    </div>
                    
                    <div class="form__row @if($errors->has('detail_images')) {{ 'form__row-error' }} @endif">
                        
                        <div class="form__row__uploadImages">
                            <div class="form__row__uploadImages__label">{{ __('cabinet.product.form_detail_images') }}</div>
                            <input id="detail-images" type="file" class="form__row__uploadFile__input file-handler" name="detail_images[]"  multiple accept="image/*">
                            <label for="detail-images" class="form__row__uploadFile__label">
                                <div class="button button-primary-bordered button-inline file-handler-multiple-button">{{ __('interface.buttons.choose_images') }}</div>
                            </label>
                        
                        </div>
                        @error('detail_images')
                        <div class="form__row__error">{{ $message }}</div>
                        @enderror
                   
                   </div>
                   
                   <div class="form__row @if($errors->has('describle_images')) {{ 'form__row-error' }} @endif">
                        
                        <div class="form__row__uploadImages">
                            <div class="form__row__uploadImages__label">{{ __('cabinet.product.form_describle_images') }}</div>
                            <input id="describle-images" type="file" class="form__row__uploadFile__input file-handler" name="describle_images[]"  multiple accept="image/*">
                            <label for="describle-images" class="form__row__uploadFile__label">
                                <div class="button button-primary-bordered button-inline file-handler-multiple-button">{{ __('interface.buttons.choose_images') }}</div>
                            </label>
                        
                        </div>
                        @error('describle_images')
                        <div class="form__row__error">{{ $message }}</div>
                        @enderror
                   
                   </div>
                   
                </div>
            </div>
            
            <div class="cabinet__createProduct__section form">
                <div class="cabinet__createProduct__wrapper">
                    <div class="cabinet__subtitle">{{ __('cabinet.product.form_video') }}</div>
                    
                    <div class="form__row">
                       
                       <div class="form__row__youtube">
                            
                            <label for="youtube" class="form__row__youtube__label form__row__label-youtube">
                                {{ __('cabinet.product.form_youtube') }}
                            </label>
                            
                            <input id="youtube" class="form__row__youtube__input video-handler" name="youtube" type="text" value="{{ old('youtube') }}" placeholder="https://www.youtube.com/watch">
                           
                            <div class="form__row__youtube__video">
                                <iframe 
                                    class="video-render" 
                                    width="580" 
                                    height="290" 
                                    frameborder="0"
                                    allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                                    allowfullscreen
                                >
                                </iframe>
                            </div>
                            
                        </div>
                        
                    </div>
                </div>
            </div>
            
            <div class="cabinet__createProduct__section form">

                {{-- Сертификация
                Тип продукта
                Упаковка
                Срок годности --}}
                
                <div class="cabinet__createProduct__wrapper">
                    <div class="cabinet__subtitle">{{ __('cabinet.product.form_additional_description') }}</div>

                    <div class="form__row form__row @if($errors->has('certification')) {{ 'form__row-error' }} @endif">
                        <input name="certification" type="text" value="{{ old('certification') }}" class="form__row__input" id="certification">
                        <label for="certification" class="form__row__label">{{ __('cabinet.product.form_certification') }}</label>
                        @error('certification')
                        <div class="form__row__error">{{ $message }}</div>
                        @enderror
                    </div>

                    <div class="form__row form__row @if($errors->has('package_type')) {{ 'form__row-error' }} @endif">
                        <input name="package_type" type="text" value="{{ old('package_type') }}" class="form__row__input" id="package_type">
                        <label for="package_type" class="form__row__label">{{ __('cabinet.product.package_type') }}</label>
                        @error('package_type')
                        <div class="form__row__error">{{ $message }}</div>
                        @enderror
                    </div>

                    <div class="form__row form__row @if($errors->has('shelf_life')) {{ 'form__row-error' }} @endif">
                        <input name="shelf_life" type="text" value="{{ old('shelf_life') }}" class="form__row__input" id="shelf_life">
                        <label for="shelf_life" class="form__row__label">{{ __('cabinet.product.shelf_life') }}</label>
                        @error('shelf_life')
                        <div class="form__row__error">{{ $message }}</div>
                        @enderror
                    </div>
                    
{{--                    <div class="form__custom__row-1">--}}
                    <div class="form__double__row form__double__row-between">
                        

                        <div class="form__row @if($errors->has('weight.value')) {{ 'form__row-error' }} @endif">
                            <input type="text" name="weight[value]" class="form__row__input" id="weight" value="{{ old('weight.value') }}">
                            <label for="weight" class="form__row__label">{{ __('cabinet.product.form_weight') }}</label>
                            @error('weight.value')
                            <div class="form__row__error">{{ $message }}</div>
                            @enderror
                        </div>
                        
                        <div class="form__row">
                            <select name="weight[type]">
                                @foreach($weights as $weight)
                                <option 
                                    value="{{ $quantity->code }}" 
                                    {{ (old('weight[type]') === $weight->code) ? 'selected' : '' }}
                                >
                                    {{ $weight->value }}
                                </option>
                                @endforeach
                            </select>
                        </div>
                        
                    </div>
                    
                    
                    <div class="form__custom__row-2">
                        
                        <div class="form__row @if($errors->has('lenght.value')) {{ 'form__row-error' }} @endif">
                            <input type="text" name="lenght[value]" class="form__row__input" id="length" value="{{ old('lenght[value]') }}">
                            <label for="length" class="form__row__label">{{ __('cabinet.product.form_lenght') }}</label>
                            @error('lenght.value')
                            <div class="form__row__error">{{ $message }}</div>
                            @enderror
                            <input name="lenght[type]" type="hidden" value="{{ old('lenght[type]') }}">
                        </div>
                        
                        <div class="form__row @if($errors->has('width.value')) {{ 'form__row-error' }} @endif">
                            <input type="text" name="width[value]" class="form__row__input" id="width" value="{{ old('width[value]') }}">
                            <label for="width" class="form__row__label">{{ __('cabinet.product.form_width') }}</label>
                            @error('width.value')
                            <div class="form__row__error">{{ $message }}</div>
                            @enderror
                            <input name="width[type]" type="hidden" value="{{ old('width[type]') }}">
                        </div>
                        
                        <div class="form__row @if($errors->has('height.value')) {{ 'form__row-error' }} @endif">
                            <input type="text" name="height[value]" class="form__row__input" id="height" value="{{ old('height[value]') }}">
                            <label for="height" class="form__row__label">{{ __('cabinet.product.form_height') }}</label>
                            @error('height.value')
                            <div class="form__row__error">{{ $message }}</div>
                            @enderror
                            <input name="height[type]" type="hidden" value="{{ old('height[type]') }}">
                        </div>
                        
                        
                        <div class="form__row">
                            <select id="size-type" class="form__row__select">
                                @foreach($sizes as $size)
                                <option 
                                    value="{{ $size->code }}" 
                                    {{ ( (old('lenght[type]') === $size->code) || (old('width[type]') === $size->code) || (old('height[type]') === $size->code) )? 'selected' : '' }}
                                >
                                    {{ $size->value }}
                                </option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
                <div class="cabinet__createProduct__wrapper">
                    <button id="plus-button" class="button button-primary" type="button">{{ __('interface.buttons.create_characteristic') }}</button>
                </div>
            </div>
            
            <div class="cabinet__createProduct__section form">
                
                <div class="cabinet__createProduct__wrapper">
                    <div class="cabinet__subtitle">{{ __('cabinet.product.form_seo_attributes') }}</div>
                    
                    <div class="form__row @if($errors->has('seo_title')) {{ 'form__row-error' }} @endif">
                        <input class="form__row__input" name="seo_title" id="seo-title" type="text" value="{{ old('seo_title') }}" >
                        <label for="seo-title" class="form__row__label">{{ __('cabinet.product.form_seo_title') }}</label>
                        @error('seo_title')
                        <div class="form__row__error">{{ $message }}</div>
                        @enderror
                    </div>
                    
                    <div class="form__row @if($errors->has('seo_keywords')) {{ 'form__row-error' }} @endif">
                        <input class="form__row__input" name="seo_keywords" id="seo-keywords" type="text" value="{{ old('seo_keywords') }}" >
                        <label for="seo-keywords" class="form__row__label">{{ __('cabinet.product.form_seo_keywords') }}</label>
                        @error('seo_keywords')
                        <div class="form__row__error">{{ $message }}</div>
                        @enderror
                    </div>
                    
                    <div class="form__row">
                        <label for="seo-descriptions" class="form__row__label form__row__label__textarea">{{ __('cabinet.product.form_seo_descriptions') }}</label>
                        <textarea rows="10" placeholder="{{ __('cabinet.product.form_seo_descriptions') }}" class="form__row__textarea form__row__input" id="seo-descriptions" name="seo_descriptions">{{ old('seo_descriptions') }}</textarea>
                    </div>
                    
                </div>
                
            </div>
            
            <div class="cabinet__createProduct__section cabinet__createProduct__section-finish">
                <input type="hidden" name="category_id" value="{{ $category->id }}">
                <button class="button button-primary button-inline submit-form" type="button">{{ __('interface.buttons.create_product') }}</button>
            </div>
            
        </div>
    </form>
</div>
@endsection

@section('content-script')
<script>
    document.addEventListener('DOMContentLoaded', function() {
        // Запоминаем единицу измерения для длины, ширины, высоты.
        $('#size-type').on('change', function(){
            $('[name="lenght[type]"], [name="width[type]"], [name="height[type]"]').val( $(this).val() );
        }).trigger('change');


        $('#plus-button').on('click', function(){
            $newCharBlock = `<div class="form__double__row form__double__row-between">
            <div class="form__row">
                <input name="new_char[name][]" type="text" value="" class="form__row__input">
                <label for="new_char[name][]" class="form__row__label">{{ __('cabinet.product.form_new_characteristic_name') }}</label>
            </div>
            
            <div class="form__row">
                <input name="new_char[value][]" type="text" value="" class="form__row__input">
                <label for="new_char[value][]" class="form__row__label">{{ __('cabinet.product.form_new_characteristic_value') }}</label>
            </div>
            <i class="fa fa-close remove-characteristic"></i>
            </div>`;

            $('#plus-button').closest('div').append($newCharBlock);

        });

        $('.cabinet__createProduct__wrapper').on('click', '.remove-characteristic', function(){
            $(this).closest('.form__double__row-between').remove();
        });
    });
</script>
<script src="{{ mix('js/product_create.js', 'assemble') }}"></script>       
@endsection