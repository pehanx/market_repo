@extends('cabinet.home')

@section('sidebar')
    @include ('cabinet.sidebar', ['page' => 'product'])
@endsection

{{--
@section('cabinet-content')
    <div class="col-12 col-md-9">
        @section('breadcrumbs', Breadcrumbs::render())
        @yield('breadcrumbs')
        
        @include ('widgets.filter_products', ['area' => Request::get('full_filter')?? ''])
        
        <div class="card border-0">
            <div class="card-header bg-white">
                <div class="form-group row mt-1">
                    <a class="btn btn-light border border-black mx-1" href="{{ route('cabinet.product.create.choose_category', $profile->id) }}">+{{ __('cabinet.product.add') }}</a>
                    <a class="btn btn-light border border-black mr-1">{{ __('cabinet.import.import') }}</a>
                    <ul class="navbar-nav w-25">
                                
                        <li class="nav-item dropdown">
                            <a id="navbar-drop" class="btn btn-light border border-black mr-1 dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                {{ __('cabinet.export.export') }}
                            </a>

                            <div class="dropdown-menu border-0" aria-labelledby="navbar-drop">
                                <ul class="navbar-nav">
                                    <a class="exports mt-3"
                                        data-route="{{ route('cabinet.export.store.page', ['profile' => $profile, 'type' => 'product', 'status'=>'inactive']) }}" 
                                        data-elements="@json($products->pluck('id'))">
                                        {{ __('cabinet.export.page') }}
                                    </a>

                                    <a class="exports mt-3" data-action-input="export_all" target="_blank" href="{{ route('cabinet.export.create.all', ['profile' => $profile, 'type' => 'product']) }}">
                                        {{ __('cabinet.export.all_products') }}
                                    </a>
                                </ul>
                            </div>
                        </li>

                    </ul>
                </div>
                
                @include ('cabinet.product._nav', ['page' => 'inactive'])
                
            </div>
            @if($products->first())
            <div class="card-header bg-white">
                <div class="row align-items-center">
                    <input type="checkbox" id="toogle-elements"><span class="mr-5"> {{ __('interface.buttons.choose_all') }}</span>
                    <ul class="navbar-nav w-25">
                                
                        <li class="nav-item dropdown">
                            <a id="navbar-drop" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                {{ __('interface.buttons.action') }}
                            </a>

                            <div class="dropdown-menu border-0" aria-labelledby="navbar-drop">
                                <ul class="navbar-nav">
                                    <a class="actions mt-3" data-action-input="activate_checked" data-text="{{ __('modals.your_sure_activate', ['item' => __('modals.checked_elements') ]) }}">
                                        {{ __('interface.buttons.activate_choosen') }}
                                    </a>
                                    
                                    <a class="actions mt-3" data-action-input="delete_checked" data-text="{{ __('modals.your_sure_delete', ['item' => __('modals.checked_elements') ]) }}">
                                        {{ __('interface.buttons.delete_choosen') }}
                                    </a>
                                    <a class="actions mt-3" data-action-input="activate_all" data-text="{{ __('modals.your_sure_activate', ['item' => __('modals.all_elements') ]) }}">
                                        {{ __('interface.buttons.activate_all') }}
                                    </a>
                                    
                                    <a class="actions mt-3" data-action-input="delete_unactive_all" data-text="{{ __('modals.your_sure_delete', ['item' => __('modals.all_elements') ]) }}">
                                        {{ __('interface.buttons.delete_all') }}
                                    </a>
                                </ul>
                            </div>
                        </li>
                        
                    </ul>
                </div>
            </div>
            <form method="POST" accept-charset="UTF-8" id="product-action-form" class="form-vertical" enctype="multipart/form-data" action="{{ route('cabinet.product.action', ['profile' => $profile]) }}">
                @csrf
                <div class="card-body">
                    @foreach($products as $key => $product)
                        <div class="row my-3 py-3 shadow">
                            <div class="col-12">
                                <input type="checkbox" class="action-elements" name="elements[{{ $key }}][id]" value="{{ $product->id }}">
                                <input class="action-elements-type" disabled type="hidden" name="elements[{{ $key }}][type]" value="product">
                            </div>
                            <div class="col-2">
                                @if($src = $product->downloads->where('type', 'preview_image')->first())
                                <img width="100" height="150" src="{{ asset('storage/'.$src->path) }}">   
                                @endif
                            </div>
                            <div class="col-8">
                                @if($product->productProperties->where('code', 'name')->first())
                                <div class="row">
                                {{ $product->productProperties->where('code', 'name')->first()->value }}
                                </div>
                                @endif
                                <div class="row">
                                {{ __('cabinet.product.form_articul') }}: {{ $product->articul }}
                                </div>
                                <div class="row text-black-50">
                                {{ __('cabinet.product.form_category') }}: {{ $product->categories->name }}
                                </div>
                                <div class="row mt-5">
                                    <a class="mr-2 btn btn-primary text-white" href="{{ route('cabinet.product.edit', ['profile' => $profile->id, 'product' => $product->id]) }}">
                                        {{ __('interface.buttons.edit') }}
                                    </a>
                                    
                                    <a class="mr-2 btn btn-success" href="{{ route('cabinet.product.activate', ['profile' => $profile, 'product' => $product]) }}">
                                        {{ __('interface.buttons.activate') }}
                                    </a>
                                    
                                    <button type="button" class="delete-product-btn mr-2 btn btn-danger" data-text="{{ __('modals.your_sure_destroy', ['item' => $product->productProperties->where('code', 'name')->first()->value]) }}" data-route="{{ route('cabinet.product.delete', ['profile' => $profile, 'product' => $product]) }}">
                                        {{ __('interface.buttons.delete') }}
                                    </button>
                                </div>
                            </div>
                            <div class="col-2">
                                @if($prices = $product->productPrices->first())
                                <div class="row justify-content-end mr-2">
                                ${{ $prices->min }} {{ ($prices->max)? ' - $'.$prices->max : '' }}
                                </div>
                                @endif
                                
                            </div>
                            @if($product->productModels->first())
                                @foreach($product->productModels as $model)
                                <hr class="w-100">
                                <div class="col-12 my-3 ml-3 background-secondary">
                                    <div class="row py-3 pr-3">
                                        <div class="col-2">
                                            @if($src = $model->downloads->where('type', 'preview_image')->first())
                                            <img width="85" height="125" src="{{ asset('storage/'.$src->path) }}">   
                                            @endif
                                        </div>
                                        <div class="col-8">
                                            <div class="row">
                                            {{ __('cabinet.product.model') }}: {{ $model->articul }}
                                            </div>

                                        </div>
                                        <div class="col-2">
                                            @if($prices = $model->productPrices->first())
                                            <div class="row justify-content-end mr-2">
                                            ${{ $prices->min }} {{ ($prices->max)? ' - $'.$prices->max : '' }}
                                            </div>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                @endforeach
                            @endif
                        </div>
                    @endforeach
                </div>
                <input type="hidden" name="action" value="">
            </form>
            <div class="row justify-content-center mt-5">
            {{ $products->links() }}
            </div>
            @endif
        </div>
    </div>
    @include('modals.destroy') 
    @include('modals.action')   
    @include('modals.export')
@endsection
--}}

@section('cabinet-content')
    <div class="cabinet">
        @section('breadcrumbs', Breadcrumbs::render())
        @yield('breadcrumbs')

        <div class="cabinet__content__title__wrapper">
            <div class="cabinet__title">{{ Breadcrumbs::generate()->last()->title }}</div>
            <a class="button button-inline button-primary-bordered" href="{{ route('cabinet.product.create.choose_category', $profile->id) }}">
                {{ __('cabinet.product.add') }}
            </a>
        </div>
            
        <div class="cabinet__content">

            @include ('widgets.filter_products', ['area' => Request::get('full_filter')?? '', 'categories' => $categories])

            <div class="cabinet__content__products">

                @include ('cabinet.product._nav', ['page' => 'inactive'])

                @if($products->first())
                    <div class="cabinet__products__checkboxActions cabinet__products__checkboxActions-active">
                        <div class="form__row__checkbox">
                            <input type="checkbox" class="form__row__checkbox__input" id="toogle-elements">
                            <label for="toogle-elements" class="form__row__checkbox__label">{{ __('interface.buttons.choose_all') }}</label>
                        </div>
                        
                        <div class="cabinet__products__checkboxActions__select">
                            <div class="cabinet__products__checkboxActions__select__button">{{ __('interface.buttons.choose_action') }}</div>
                            <div class="cabinet__products__checkboxActions__select__items">
                                <ul>
                                    <li class="actions" data-action-input="activate_checked" data-text="{{ __('modals.your_sure_activate', ['item' => __('modals.checked_elements') ]) }}">
                                        {{ __('interface.buttons.activate_choosen') }}
                                    </li>
                                    
                                    <li class="actions" data-action-input="delete_checked" data-text="{{ __('modals.your_sure_delete', ['item' => __('modals.checked_elements') ]) }}">
                                        {{ __('interface.buttons.delete_choosen') }}
                                    </li>
                                    
                                    <li class="actions" data-action-input="activate_all" data-text="{{ __('modals.your_sure_activate', ['item' => __('modals.all_elements') ]) }}">
                                        {{ __('interface.buttons.activate_all') }}
                                    </li>
                                    
                                    <li class="actions" data-action-input="delete_unactive_all" data-text="{{ __('modals.your_sure_delete', ['item' => __('modals.all_elements') ]) }}">
                                        {{ __('interface.buttons.delete_all') }}
                                    </li>
                                </ul>
                            </div>
                        </div>
                        
                    </div>
                    
                    <form method="POST" accept-charset="UTF-8" id="product-action-form" enctype="multipart/form-data" action="{{ route('cabinet.product.action', ['profile' => $profile]) }}">
                    @csrf
                        
                        @foreach($products as $key => $product)
                            <div class="cabinet__products__item">
                                
                                <div class="cabinet__products__item__main">

                                    <div class="cabinet__products__item__main__checkbox">
                                        <div class="form__row__checkbox">
                                            <input 
                                                type="checkbox" 
                                                class="form__row__checkbox__input action-elements" 
                                                id="element-{{ $product->id }}" 
                                                name="elements[{{ $key }}][id]" 
                                                value="{{ $product->id }}"
                                            >
                                            <label for="element-{{ $product->id }}"></label>
                                            <input 
                                                class="action-elements-type" 
                                                disabled 
                                                type="hidden" 
                                                name="elements[{{ $key }}][type]" 
                                                value="product"
                                            >
                                        </div>
                                    </div>
                                    
                                    <a href="{{ route('cabinet.product.show', ['profile' => $profile->id, 'product' => $product->id]) }}">
                                        <div 
                                            class="cabinet__products__item__main__photo"
                                            @if($product->downloads->where('type', 'preview_image')->first())
                                            style="background-image: url('{{ asset('storage/' . $product->downloads->where('type', 'preview_image')->first()->path) }}');"
                                            @endif
                                        >
                                        </div>
                                    </a>
                                    
                                    <div class="cabinet__products__item__main__info">
                                        <a href="{{ route('cabinet.product.show', ['profile' => $profile->id, 'product' => $product->id]) }}">
                                            <div class="cabinet__products__item__main__title">
                                                <div class="cabinet__products__item__main__title__text">
                                                    {{ $product->productProperties->where('code', 'name')->first()->value ?? '' }}
                                                </div>
                                                <div class="cabinet__products__item__main__title__art">
                                                    <strong>{{ __('cabinet.product.form_articul') }}:</strong>
                                                    {{ $product->articul }}
                                                </div>
                                                
                                                <div class="cabinet__products__item__main__title__category">
                                                    {{ __('cabinet.product.form_category') }}: {{ $product->categories->name }}
                                                </div>
                                            </div>
                                        </a>
                                        
                                        <div class="cabinet__products__item__main__actions">
                                            
                                            <a 
                                                class="button button-inline button-primary" 
                                                href="{{ route('cabinet.product.edit', ['profile' => $profile->id, 'product' => $product->id]) }}"
                                            >
                                                {{ __('interface.buttons.edit') }}
                                            </a>

                                            <a 
                                                class="button button-inline button-primary-bordered" 
                                                href="{{ route('cabinet.product.activate', ['profile' => $profile, 'product' => $product]) }}"
                                            >
                                                {{ __('interface.buttons.activate') }}
                                            </a>
                                            
                                            <button 
                                                type="button" 
                                                class="delete-product-btn button button-inline button-primary-link" 
                                                data-text="{{ __('modals.your_sure_destroy', ['item' => $product->productProperties->where('code', 'name')->first()->value]) }}" 
                                                data-route="{{ route('cabinet.product.delete', ['profile' => $profile, 'product' => $product]) }}"
                                            >
                                                {{ __('interface.buttons.delete') }}
                                            </button>
                                            
                                        </div>
                                        
                                    </div>
                                    
                                    @if($prices = $product->productPrices->first())
                                    <div class="cabinet__products__item__main__price">
                                        ${{ $product->productPrices->first()->min }} 
                                        {{ $product->productPrices->first()->max ? ' — $' . $product->productPrices->first()->max : '' }}
                                    </div>
                                    @endif
                                    
                                </div>
                                
                                @if($product->productModels->first())
                                    <div class="cabinet__products__item__offers">
                                        @foreach($product->productModels as $model)
                                            <div class="cabinet__products__item__offers__element">
                                                <div 
                                                    class="cabinet__products__item__offers__image" 
                                                    style="background-image: url('{{ asset('storage/' . $model->downloads->where('type', 'preview_image')->first()->path) }}');"
                                                >
                                                </div>
                                                <div class="cabinet__products__item__offers__info">
                                                    <div class="cabinet__products__item__offers__info__title">
                                                        <strong>{{ __('cabinet.product.model') }}:</strong>
                                                        {{ $product->articul }} — {{ $model->articul }}
                                                    </div>
                                                    <div class="cabinet__products__item__offers__info__actions">
                                                        <a 
                                                            class="button button-inline button-primary" 
                                                            href="{{ route('cabinet.product.edit.model', ['profile' => $profile->id, 'product' => $product->id, 'model' => $model->id]) }}"
                                                        >
                                                            {{ __('interface.buttons.edit') }}
                                                        </a>
                                                        {{-- 
                                                        
                                                        
                                                        <button 
                                                            type="button" 
                                                            class="delete-product-btn button button-inline button-primary-link" 
                                                            data-text="{{ __('modals.your_sure_destroy', ['item' => $product->articul.$model->articul]) }}" 
                                                            data-route="{{ route('cabinet.product.delete.model', ['profile' => $profile, 'product' => $product, 'model' => $model]) }}"
                                                        >
                                                            {{ __('interface.buttons.delete') }}
                                                        </button> --}}
                                                    
                                                    </div>
                                                </div>
                                                
                                                @if($prices = $model->productPrices->first())
                                                <div class="cabinet__products__item__offers__price">
                                                ${{ $model->productPrices->first()->min }} 
                                                {{ $model->productPrices->first()->max ? ' — $' . $model->productPrices->first()->max : '' }}
                                                </div>
                                                @endif
                                                
                                            </div>
                                        @endforeach
                                    </div>
                                @endif
                                
                            </div>
                        @endforeach
                        
                        <input type="hidden" name="action">
                    </form>
                    
                    {{ $products->render('widgets.pagination') }}
                
                @endif
            </div>
        </div>
    </div>
    
    @include('modals.destroy') 
    @include('modals.action')   
    {{-- @include('modals.export') --}}
    
@endsection

@section('content-script')
<script>
document.addEventListener('DOMContentLoaded', function() {
    cabinet.init('#toggle-filter-products', 'click', 'toogleFilterProducts');

    cabinet.init('.delete-product-btn', 'click', 'deleteProductOrModel');
    
    cabinet.init('.actions', 'click', 'makeActionProduct');
    cabinet.init('#toogle-elements', 'click', 'toogleActionElements');
    cabinet.init('.action-elements', 'click', 'setCheckedElementType');
    {{-- cabinet.init('.exports', 'click', 'exportForm'); --}}
    
    $(".action-elements:checked")
        .trigger('click')
        .trigger('click');
});
</script>
@endsection