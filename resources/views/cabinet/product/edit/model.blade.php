@extends('cabinet.home')

@section('sidebar')
    @include ('cabinet.sidebar', ['page' => ''])
@endsection

{{--
@section('cabinet-content')
    <div class="col-12 col-md-9">
    @section('breadcrumbs', Breadcrumbs::render())
    @yield('breadcrumbs')
         
        <!--Редактируемое торговое предложение-->
        <div class="form-group row m-0">
            <h4>{{ __('cabinet.product.model') }} {{ $product->articul.$model->articul }}</h4>
        </div>
        
        <form method="POST" accept-charset="UTF-8" class="form-vertical" enctype="multipart/form-data" action="{{ route('cabinet.product.update.model', ['profile' => $profile, 'product' => $product, 'model' => $model]) }}">
            @csrf
            @method('PUT')

            <div class="form-group row mt-5">
                <label class="col-sm-6 col-form-label">{{ __('cabinet.product.form_preview_image') }}</label>
                <div class="col-sm-6">
                    @foreach ($model->downloads as $download)
                        @if($download->type === 'preview_image')
                        <div class="container-file-reader">
                            <div class="col-6 col-md-2 d-flex align-items-start">
                                <button type="button" class="delete-file-btn btn btn-secondary" data-input-name="preview_image" data-id="{{ $download->id }}"><i class="fa fa-close"></i></button>
                                <img width="50" height="50" data-id-file="{{ $download->id }}" src="{{ asset('storage/'.$download->path) }}">
                            </div>
                        </div>
                        @endif
                    @endforeach 
                    
                    @error('preview_image')
                    <span class="text-danger">{{ $message }}</span>
                    @enderror
                    <input type="file" class="file-handler" name="preview_image" accept="image/*" value="{{ old('preview_image') }}">
                </div>    
            </div>
            
            <div class="form-group row">
                <label for="example-email-input" class="col-12 col-md-6 col-form-label">{{ __('cabinet.product.form_color') }}</label>
                <div class="col-12 col-md-6">
                    <select name="color">
                        @foreach($colors as $color)
                            <option {{ ( ($model->characteristics['color']['type']?? '') && $model->characteristics['color']['type'] ==  $color->code)? 'selected' : '' }} value="{{ $color->code }}">{{ $color->value }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            
            <div class="form-group row">
                <div class="input-group">
                    <label for="example-text-input" class="col-12 col-md-6 col-form-label">{{ __('cabinet.product.form_available') }}</label>
                    <div class="col-12 col-md-6">
                        <div class="row">
                            <div class="col-5">
                                @error('available.value')
                                <span class="text-danger">{{ $message }}</span>
                                @enderror
                                <input class="form-control" name="available[value]" type="text" value="{{ old('available[value]', $model->characteristics['available']['value'] ?? '') }}" >
                            </div>
                            <div class="col-2">
                                <select class="h-100" name="available[type]">
                                    @foreach($quantities as $quantity)
                                        <option {{ ( ($model->characteristics['available']['type']?? '') && $model->characteristics['available']['type'] ==  $quantity->value)? 'selected' : '' }} value="{{ $quantity->code }}">{{ $quantity->value }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="form-group row">
                <label for="example-text-input" class="col-12 col-md-6 col-form-label">{{ __('cabinet.product.form_min_price') }}, ($)</label>
                <div class="col-12 col-md-6">
                    @error('min_price')
                    <span class="text-danger">{{ $message }}</span>
                    @enderror
                    <input class="form-control" name="min_price" type="text" value="{{ old('min_price', $model->productPrices->first()->min) }}" >
                </div>
            </div>	
            <div class="form-group row">
                <label for="example-text-input" class="col-12 col-md-6 col-form-label">{{ __('cabinet.product.form_max_price') }}, ($)</label>
                <div class="col-12 col-md-6">
                    @error('max_price')
                    <span class="text-danger">{{ $message }}</span>
                    @enderror
                    <input class="form-control" name="max_price" type="text" value="{{ old('max_price', $model->productPrices->first()->max ?? '') }}" >
                </div>
            </div>
            
            <div class="form-group row justify-content-center">
                <button type="button" class="btn btn-secondary mt-3 submit-form">{{ __('interface.buttons.save_changes') }}</button>
            </div>
            <hr class="mt-5">
        </form>
        <!--Конец--> 
        
        @if($src = $product->downloads->where('type', 'preview_image')->first())
        <div class="row ml-0">
            <img width="200" height="200" src="{{ asset('storage/'.$src->path) }}">   
        </div>
        @endif
        
        @if($srcs = $product->downloads->where('type', 'detail_images')->all())
        <div class="row ml-0 mt-5">
            @foreach($srcs as $src)
            <img width="50" height="50" src="{{ asset('storage/'.$src->path) }}">   
            @endforeach
        </div>
        @endif
         
        <div class="form-group row ml-0 mt-3 text-black-50">
            {{ __('cabinet.product.form_articul') }}
        </div> 
        <div class="form-group row ml-0">
            {{ $product->articul }}
        </div> 
        
        <div class="form-group row ml-0 mt-3 text-black-50">
            {{ __('cabinet.product.form_name') }}
        </div> 
        <div class="form-group row ml-0">
            {{ $product->productProperties->where('code', 'name')->first()->value ?? '—' }}
        </div> 
        
        <div class="form-group row ml-0 mt-3 text-black-50">
            {{ __('cabinet.product.form_category') }}
        </div> 
        <div class="form-group row ml-0">
            {{ $category->categoryProperties->where('code', 'name')->first()->value ?? '—' }}
        </div> 
        
        <div class="form-group row ml-0 mt-3 text-black-50">
            {{ __('cabinet.product.form_price') }}
        </div> 
        <div class="form-group row ml-0">
            @php $prices = $product->productPrices->first() @endphp
            {{ $prices->min }} {{ ($prices->max)? ' - '.$prices->max.'$' : '$' }}
        </div> 
        
        <div class="form-group row ml-0 mt-3 text-black-50">
            {{ __('cabinet.product.form_min_order') }}
        </div> 
        <div class="form-group row ml-0">
            {{ $product->characteristics['min_order']['value'] ?? '—' }} {{ $product->characteristics['min_order']['type'] ?? '' }}
        </div> 
        
        <div class="form-group row ml-0 mt-3 text-black-50">
            {{ __('cabinet.product.form_available') }}
        </div> 
        <div class="form-group row ml-0">
            {{ $product->characteristics['available']['value'] ?? '—'}} {{ $product->characteristics['available']['type'] ?? ''}}
        </div> 
        
        <div class="form-group row ml-0 mt-3 text-black-50">
            {{ __('cabinet.product.form_color') }}
        </div> 
        
        <div class="form-group row ml-0">
            <span class="rounded-circle" style="background-color: {{ $product->characteristics['color']['type'] ?? ''}}">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
        </div> 
        
        <div class="form-group row ml-0 mt-3 text-black-50">
            {{ __('cabinet.product.form_description') }}
        </div> 
        <div class="form-group row ml-0">
            {{ $product->productProperties->where('code', 'description')->first()->value ?? '—' }}
        </div> 
        
        <div class="form-group row ml-0">
            <h4>{{ __('cabinet.product.form_additional_description') }}</h4>
        </div> 
        
        <div class="form-group row ml-0 mt-3 text-black-50">
            {{ __('cabinet.product.form_weight') }}
        </div> 
        <div class="form-group row ml-0">
            {{ $product->characteristics['weight']['value'] ?? '—' }} {{ $product->characteristics['weight']['type'] ?? '' }}
        </div> 
        
        <div class="form-group row ml-0 mt-3 text-black-50">
            {{ __('cabinet.product.form_lenght') }} x {{ __('cabinet.product.form_width') }} x {{ __('cabinet.product.form_height') }}
        </div> 
        <div class="form-group row ml-0">
            {{ $product->characteristics['lenght']['value'] ?? '...' }} — {{ $product->characteristics['width']['value'] ?? '...'}} — {{ $product->characteristics['height']['value'] ?? '...' }} {{ $product->characteristics['lenght']['type'] ?? '' }}
        </div> 
        
        <div class="form-group row ml-0">
            <h4>{{ __('cabinet.product.form_seo_attributes') }}</h4>
        </div> 
        
        <div class="form-group row ml-0 mt-3 text-black-50">
            {{ __('cabinet.product.form_seo_title') }}
        </div> 
        <div class="form-group row ml-0">
            {{ $product->productProperties->where('code', 'seo_title')->first() ? $product->productProperties->where('code', 'seo_title')->first()->value : '—'}}
        </div> 
        
        <div class="form-group row ml-0 mt-3 text-black-50">
            {{ __('cabinet.product.form_seo_keywords') }}
        </div> 
        <div class="form-group row ml-0">
            {{ $product->productProperties->where('code', 'seo_keywords')->first() ? $product->productProperties->where('code', 'seo_keywords')->first()->value : '—'}}
        </div> 
        
        <div class="form-group row ml-0 mt-3 text-black-50">
            {{ __('cabinet.product.form_seo_descriptions') }}
        </div> 
        <div class="form-group row ml-0">
            {{ $product->productProperties->where('code', 'seo_descriptions')->first() ? $product->productProperties->where('code', 'seo_descriptions')->first()->value : '—'}}
        </div>
        
        @if($product->productModels->first())
            <div class="form-group row ml-0">
                <h4>{{ __('cabinet.product.models') }}</h4>
            </div>
            @foreach($product->productModels as $productModel)
                @if($src =  $productModel->downloads->where('type', 'preview_image')->first())
                <img width="50" height="50" src="{{ asset('storage/'.$src->path) }}">   
                @endif
                
                <div class="form-group row ml-0 mt-3 text-black-50">
                {{ __('cabinet.product.form_articul') }}
                </div> 
                <div class="form-group row ml-0">
                {{  $productModel->articul }}
                </div> 
                
                <div class="form-group row ml-0 mt-3 text-black-50">
                {{ __('cabinet.product.form_price') }}
                </div> 
                <div class="form-group row ml-0">
                @php $prices =  $productModel->productPrices->first() @endphp
                {{ $prices->min }} {{ ($prices->max)? ' - '.$prices->max.'$' : '$' }}
                </div> 
                
                <div class="form-group row ml-0 mt-3 text-black-50">
                {{ __('cabinet.product.form_available') }}
                </div> 
                <div class="form-group row ml-0">
                {{  $productModel->characteristics['available']['value'] ?? '—' }} {{  $productModel->characteristics['available']['type'] ?? '—' }}
                </div> 
                
                <div class="form-group row ml-0 mt-3 text-black-50">
                {{ __('cabinet.product.form_color') }}
                </div> 
                <div class="form-group row ml-0">
                    <span class="rounded-circle" style="background-color: {{  $productModel->characteristics['color']['type'] }}">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                </div> 
                
                <hr>
            @endforeach
        @endif
        
    </div>
@endsection
--}}

@section('cabinet-content')
<div class="cabinet">
	@section('breadcrumbs', Breadcrumbs::render())
    @yield('breadcrumbs')
	<div class="cabinet__title">{{ Breadcrumbs::generate()->last()->title }}</div>

	<div class="cabinet__createProduct cabinet__hideArea">
		
        <div class="cabinet__createProduct__section formView">
			<div class="cabinet__createProduct__wrapper">
				
                <div class="cabinet__subtitle">{{ __('cabinet.product.product') }} — {{ $product->productProperties->where('code', 'name')->first()->value }}  ({{ $product->articul }})</div>
				
                @foreach ($product->downloads as $type => $download)
                    @if($type === 'preview_image')
                        <div class="formView__row" style="margin-top: 30px;">
                            <div class="formView__row__label">{{ __('cabinet.product.form_preview_image') }}</div>
                            <div class="formView__row__image"><img src="{{ asset('storage/'.$download->first()->path) }}" alt=""></div>
                        </div>
                        @continue
                    @endif
                
                <div class="formView__row">
                    <div class="formView__row__label">{{ __('cabinet.product.form_'.$type) }}</div>
                    <div class="formView__row__images">
                        @foreach ($download as $item)
                        <div class="formView__row__images__image" style="background-image: url('{{ asset('storage/'.$item->path) }}');"></div>
                        @endforeach
                    </div>
                </div>
                @endforeach

			</div>
            
		</div>
        
		<div class="cabinet__createProduct__section formView cabinet__hide">
			<div class="cabinet__createProduct__wrapper">
            {{-- <div class="cabinet__subtitle">Основные характеристики</div> --}}
				{{--
                <div class="formView__row" style="margin-top: 30px;">
					<div class="formView__row__label">Название</div>
					<div class="formView__row__value">Юбка летняя</div>
				</div>
                --}}
				<div class="formView__row">
					<div class="formView__row__label">{{ __('cabinet.product.form_category_product') }}</div>
					<div class="formView__row__value">{{ $category->categoryProperties->where('code', 'name')->first()->value }}</div>
				</div>
                
				<div class="formView__row">
					<div class="formView__row__label">{{ __('cabinet.product.form_price') }}</div>
					<div class="formView__row__value">
                        @php $prices = $product->productPrices->first() @endphp
                        {{ $prices->min }} {{ ($prices->max)? ' — '.$prices->max.'$' : '$' }}
                    </div>
				</div>
                
				<div class="formView__row">
					<div class="formView__row__label">{{ __('cabinet.product.form_min_order') }}</div>
					<div class="formView__row__value">
                        {{ $product->characteristics['min_order']['value'] }} {{ $product->characteristics['min_order']['type'] }}
                    </div>
				</div>
                
                <div class="formView__row">
					<div class="formView__row__label">{{ __('cabinet.product.form_available') }}</div>
					<div class="formView__row__value">
                        {{ $product->characteristics['available']['value'] }} {{ $product->characteristics['available']['type'] }}
                    </div>
				</div>
                
				<div class="form__row__selectorColor">
                    <div class="form__row__selectorColor__label">{{ __('cabinet.product.form_color_product') }}</div>
                    <div class="form__row__selectorColor__content">
                        <span class="simplecolorpicker icon" title="{{ $product->characteristics['color']['value'] }}" style="background-color: {{ $product->characteristics['color']['type'] }};"></span>
                    </div>
                </div>
                
                <br>
                
                @if($description = $product->productProperties->where('code', 'description')->first())
				<div class="formView__row">
					<div class="formView__row__label">{{ __('cabinet.product.form_description') }}</div>
					<div class="formView__row__value">{{ $description->value }}</div>
				</div>
                @endif
                
				@if(!empty($product->youtube_frame))
                <div class="formView__row">
                    <div class="formView__row__label">{{ __('cabinet.product.form_video') }}</div>
                    <div class="formView__row__video">
                        {!! $product->youtube_frame !!}
                    </div>
                </div>
                @endif
                
			</div>
		</div>
        
		<div class="cabinet__createProduct__section formView cabinet__hide">
			<div class="cabinet__createProduct__wrapper">
            {{-- <div class="cabinet__subtitle">Дополнительное описание</div> --}}
				
                @isset($product->characteristics['weight'])
                <div class="formView__row" style="margin-top: 30px;">
					<div class="formView__row__label">{{ __('cabinet.product.form_weight') }}</div>
					<div class="formView__row__value">
                        {{ $product->characteristics['weight']['value'] }} {{ $product->characteristics['weight']['type'] }}
                    </div>
				</div>
                @endisset
                
                @if(isset($product->characteristics['lenght']) || isset($product->characteristics['width']) || isset($product->characteristics['height']))
				<div class="formView__row">
					<div class="formView__row__label">
                        {{ __('cabinet.product.form_lenght') }} x {{ __('cabinet.product.form_width') }} x {{ __('cabinet.product.form_height') }}
                    </div>
					<div class="formView__row__value">
                        {{ $product->characteristics['lenght']['value'] ?? '...' }} x {{ $product->characteristics['width']['value'] ?? '...'}} x {{ $product->characteristics['height']['value'] ?? '...' }} {{ $product->characteristics['lenght']['type'] ?? '' }}
                    </div>
				</div>
                @endif
                
			</div>
		</div>
        
        @if( 
            $seoTitle = $product->productProperties->where('code', 'seo_title')->first() 
                OR
            $seoKeywords = $product->productProperties->where('code', 'seo_keywords')->first() 
                OR
            $seoDescriptions = $product->productProperties->where('code', 'seo_descriptions')->first() 
        )
		<div class="cabinet__createProduct__section formView cabinet__hide">
			<div class="cabinet__createProduct__wrapper">
				<div class="cabinet__subtitle">{{ __('cabinet.product.form_seo_attributes') }}</div>
				
                <div class="formView__row" style="margin-top: 20px;">
					<div class="formView__row__label">{{ __('cabinet.product.form_seo_title') }}</div>
					<div class="formView__row__value">{{ (isset($seoTitle) && $seoTitle->value) ? $seoTitle->value : '—' }}</div>
				</div>
                
				<div class="formView__row">
					<div class="formView__row__label">{{ __('cabinet.product.form_seo_keywords') }}</div>
					<div class="formView__row__value">{{ (isset($seoKeywords) && $seoKeywords->value) ? $seoKeywords->value : '—' }}</div>
				</div>
                
				<div class="formView__row">
					<div class="formView__row__label">{{ __('cabinet.product.form_seo_descriptions') }}</div>
					<div class="formView__row__value">{{ (isset($seoDescriptions) && $seoDescriptions->value) ? $seoDescriptions->value : '—' }}</div>
				</div>
                
			</div>
		</div>
        @endif

		<div class="cabinet__unhide" style="margin-top: 30px;">{{ __('interface.buttons.show_completely') }}</div>
	</div>
    
    @if($product->productModels->first())
        <div class="cabinet__subtitle">{{ __('cabinet.product.product_models') }}</div>
        @foreach($product->productModels as $model)
            <div class="cabinet__hideArea">
                <div class="cabinet__createModel__section cabinet__createModel__content formView">
                    <div class="cabinet__subtitle">{{ $product->articul }} — {{ $model->articul }}</div>
                    @if($src = $model->downloads->where('type', 'preview_image')->first())
                    <div class="formView__row" style="margin-top: 20px;">
                        <div class="formView__row__image">
                            <div class="formView__row__images__image" style="background-image: url('{{ asset('storage/'.$src->path) }}');"></div>
                        </div>
                    </div>
                    @endif
                    
                    <div class="formView__row cabinet__hide">
                        <div class="formView__row__label">{{ __('cabinet.product.form_price') }}</div>
                        <div class="formView__row__value">
                            @php $prices = $model->productPrices->first() @endphp
                            {{ $prices->min }} {{ ($prices->max)? ' — '.$prices->max.'$' : '$' }}
                        </div>
                    </div>
                    
                    <div class="formView__row cabinet__hide">
                        <div class="formView__row__label">{{ __('cabinet.product.form_available') }}</div>
                        <div class="formView__row__value">{{ $model->characteristics['available']['value'] }} {{ $model->characteristics['available']['type'] }}</div>
                    </div>
                    
                    <div class="formView__row cabinet__hide">
                        <div class="formView__row__label">{{ __('cabinet.product.form_color') }}</div>
                        <div class="form__row__selectorColor__content">
                            <span class="simplecolorpicker icon" title="{{ $model->characteristics['color']['value'] }}" style="background-color: {{ $model->characteristics['color']['type'] }};"></span>
                        </div>
                    </div>
                    
                </div>
                <div class="cabinet__unhide" style="margin-top: 30px;">{{ __('interface.buttons.show_completely') }}</div>
            </div>
        @endforeach
    @endif
    
    <form id="add-model-form" method="POST" accept-charset="UTF-8" enctype="multipart/form-data" action="{{ route('cabinet.product.update.model', ['profile' => $profile, 'product' => $product, 'model' => $editedModel]) }}">
        @csrf
        @method('PUT')
        <div class="cabinet__createModel">
            <div class="cabinet__createModel__section cabinet__createModel__content form">
                
                <div class="cabinet__subtitle">{{ __('cabinet.product.form_editing_product_model') }} {{ $product->articul }} — <span class="prefix">{{ $editedModel->articul }}</span></div>
                
                <div class="form__row form__row-required @if($errors->has('preview_image')) {{ 'form__row-error' }} @endif">
                    <div class="form__row__uploadImages">
                    
                        <div class="form__row__uploadImages__label">{{ __('cabinet.product.form_model_preview_image') }}</div>
                        
                        @if($preview = $editedModel->downloads->where('type', 'preview_image')->first())
                        <div class="form__row__uploadImages__preview"><!--container-->
                            <div class="form__row__uploadImages__preview__image"><!--wrapper-->
                                <div class="form__row__uploadImages__preview__image__remove" data-input-name="preview_image" data-id="{{ $preview->id }}">
                                </div><!--delete-->
                                <div 
                                    class="form__row__uploadImages__preview__image" 
                                    data-id-file="{{ $preview->id }}" 
                                    style="background-image: url('{{ asset('storage/'.$preview->path) }}');"                            
                                ><!--image-->
                                </div>
                            </div>
                        </div>
                        @endif

                        <input id="preview-image" type="file" class="form__row__uploadFile__input file-handler" name="preview_image" accept="image/*">
                        
                        <label for="preview-image" class="form__row__uploadFile__label">
                            <div class="button button-primary-bordered button-inline">{{ __('interface.buttons.choose_image') }}</div>
                        </label>
                    </div>
                    @error('preview_image')
                    <div class="form__row__error">{{ $message }}</div>
                    @enderror
                </div>
                
                <div class="form__double__row form__double__row-between">
                {{-- dd((string)$editedModel->productPrices->first()->min) --}}
                    <div class="form__row form__row-required @if($errors->has('min_price')) {{ 'form__row-error' }} @endif">
                        <input name="min_price" type="text" value="{{ old('min_price', $editedModel->productPrices->first()->min) }}" class="form__row__input" id="min-price" required>
                        <label for="min-price" class="form__row__label">{{ __('cabinet.product.form_min_price') }}, ($)</label>
                        @error('min_price')
                        <div class="form__row__error">{{ $message }}</div>
                        @enderror
                    </div>
                    
                    <div class="form__row @if($errors->has('max_price')) {{ 'form__row-error' }} @endif">
                        <input name="max_price" type="text" value="{{ old('max_price', ($editedModel->productPrices->first()->max ? $editedModel->productPrices->first()->max : '')) }}" class="form__row__input" id="max-price">
                        <label for="max-price" class="form__row__label">{{ __('cabinet.product.form_max_price') }}, ($)<label>
                        @error('max_price')
                        <div class="form__row__error">{{ $message }}</div>
                        @enderror
                    </div>
                    
                </div>
                
                <div class="form__double__row form__double__row-between">
                                
                    <div class="form__row form__row-required @if($errors->has('available.value')) {{ 'form__row-error' }} @endif">
                        <input type="text" name="available[value]" class="form__row__input" id="available-value" value="{{ old('available.value', $editedModel->characteristics['available']['value'] ?? '') }}" required>
                        <label for="available-value" class="form__row__label">{{ __('cabinet.product.form_available') }}</label>
                        @error('available.value')
                        <div class="form__row__error">{{ $message }}</div>
                        @enderror
                    </div>
                    
                    <div class="form__row form__row-required">
                        <select name="available[type]">
                            @foreach($quantities as $quantity)
                            <option {{ ( ($editedModel->characteristics['available']['type']?? '') && $editedModel->characteristics['available']['type'] ==  $quantity->value)? 'selected' : '' }} value="{{ $quantity->code }}">{{ $quantity->value }}</option>
                            @endforeach
                        </select>
                    </div>
                    
                </div>
                
                <div class="form__row form__row-required">
                    <div class="form__row__selectorColor">
                        <div class="form__row__selectorColor__label">{{ __('cabinet.product.form_color') }}</div>
                        <div class="form__row__selectorColor__content">
                            <select name="color" class="colorpicker-handle">
                                @foreach($colors as $color)
                                <option {{ ( ($editedModel->characteristics['color']['type']?? '') && $editedModel->characteristics['color']['type'] ==  $color->code)? 'selected' : '' }} value="{{ $color->code }}">{{ $color->value }}</option>
                                @endforeach
                            </select>
                        </div>  
                    </div>
                </div>
                            
                <div class="cabinet__createModel__actions">
                    
                </div>

            </div>
        </div>
    
        <div class="cabinet__createProduct__section cabinet__createProduct__section-finish">
            <button type="button" class="button button-primary-bordered button-inline submit-form">
                {{ __('interface.buttons.save_changes') }}
            </button>
            <a 
                class="button button-inline button-primary-link" 
                href="{{ route('cabinet.product.index', $profile->id) }}"
            >
                {{ __('interface.buttons.goto_products') }}
            </a>
        </div>
    </form>
    
</div>
@endsection

@section('content-script')
<script src="{{ mix('js/product_model_edit.js', 'assemble') }}"></script>   
<script>
    document.addEventListener('DOMContentLoaded', function() {
        $('select[name="color"]').simplecolorpicker({picker: true, theme: 'glyphicons'});
    });
</script>
@endsection