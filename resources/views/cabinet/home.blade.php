@extends('layouts.market.cabinet.index')

@section('sidebar')
    @include ('cabinet.sidebar', ['page' => ''])
@endsection

@section('content')
    @yield('cabinet-content')
@endsection

@section('scripts')
    <script src="{{ mix('js/Cabinet.js', 'assemble') }}"></script>
    <script src="{{ mix('js/Tree.js', 'assemble') }}"></script>
    <script>
    
        const cabinet = new Cabinet ();
        const addModelFormClone = $('#add-model-form').clone(); // Чистый клон без инициализированных библиотек
        
        document.addEventListener('DOMContentLoaded', function() {

            $("[name='country']").select2({
                placeholder: "{{ __('cabinet.profile.form_country') }}",
                theme: "marketplace-forms",
                width: "style",
                language: "{{ session()->get('locale') }}"
            });

            $("select[name='localisation']").select2({
                placeholder: "{{ __('cabinet.profile.choose_language') }}",
                theme: "marketplace-forms",
                width: "style",
                language: "{{ session()->get('locale') }}" 
            });
            
            // Фильтр в списке
            $("select[name='category'], select#favorites-category-id").select2({
                placeholder: "{{ __('cabinet.profile.choose_category') }}",
                theme: "marketplace-forms",
                width: "450px",
                language: "{{ session()->get('locale') }}" 
            });
            
            // Редактирование товара
            $("select[name='category_id']:not(#favorites-category-id)").select2({
                placeholder: "{{ __('cabinet.profile.choose_category') }}",
                theme: "marketplace-forms",
                width: "style",
                language: "{{ session()->get('locale') }}" 
            });
            
            $("select[name='min_order[type]'], select[name='available[type]'], select[name='weight[type]'], select#size-type").select2({
                theme: "marketplace-forms",
                width: "style",
                language: "{{ session()->get('locale') }}" // Задать через veiwComposer locale
            });
            
            $(".cabinet__createModel__model select[name='available[type]']").data('language', "{{ session()->get('locale') }}");
        });
    </script>
    @yield('content-script')
@endsection