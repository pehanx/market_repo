<div class="cabinet__content__tabs">
    <ul class="tabs__titles">
        <li class="{{ $page === 'product' ? ' tabs__titles__active' : '' }}">
            <a href="{{ route('cabinet.favorite.index', ['user' => Auth::user()->id, 'type' => 'product']) }}">
                {{ __('interface.tabs.products') }}
            </a>
        </li>
        <li class="{{ $page === 'request_product' ? ' tabs__titles__active' : '' }}">
            <a href="{{ route('cabinet.favorite.index', ['user' => Auth::user()->id, 'type' => 'request_product']) }}">
                {{ __('interface.tabs.request_products') }}
            </a>
        </li>
    </ul>
</div>