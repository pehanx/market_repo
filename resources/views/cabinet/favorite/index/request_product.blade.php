@extends('cabinet.home')

@section('sidebar')
    @include ('cabinet.sidebar', ['page' => 'favorite'])
@endsection

{{--
@section('cabinet-content')
    <div class="col-12 col-md-9">
        @include ('widgets.filter_favorites', ['categories' => $categories])
        <div class="row mt-3">
        @include ('cabinet.favorite._nav', ['page' => 'request_product'])
        </div>
        <hr class="mt-2">
            <input type="checkbox" class="" name="is_active" value="true" id="toogle-active" @if(request('is_active') === 'true') checked @endif >В наличии
            @if($favorites->first())

                <div class="card-body">
                    @foreach($favorites as $key => $requestProduct)
                        <div class="row my-3 py-3 shadow">
                            <div class="col-2">
                                @if($src = $requestProduct->downloads->where('type', 'preview_image')->first())
                                    <img width="100" height="150" src="{{ asset('storage/'.$src->path) }}">   
                                @endif
                            </div>
                            <div class="col-7">
                                    @if($requestProduct->requestProductProperties->where('code', 'name')->first())
                                    <div class="row">
                                    {{ $requestProduct->requestProductProperties->where('code', 'name')->first()->value }}
                                    </div>
                                    @endif
                                    <div class="row">
                                    {{ __('interface.tabs.request_products') }} {{ $requestProduct->articul }}
                                    </div>
                                    <div class="row text-black-50">
                                        {{ __('interface.tabs.category') }}: {{ $requestProduct->categories->name }}
                                    </div>
                                <div class="row mt-5">
                                    
                                    <button type="button" class="send-message-btn btn-sm mr-2 btn btn-warning" data-text="{{ __('modals.your_sure_disable', ['item' => $requestProduct->requestProductProperties->where('code', 'name')->first()->value]) }}" data-route="">
                                        {{ __('interface.buttons.write_buyer') }}
                                    </button>
                                    
                                    <button type="button" class="delete-product-btn mr-2 btn-sm btn btn-danger" data-text="{{ __('modals.your_sure_destroy', ['item' => $requestProduct->requestProductProperties->where('code', 'name')->first()->value]) }}" data-route="">
                                        {{ __('interface.buttons.favourite_remove') }}
                                    </button>
                                </div>
                            </div>
                            <div class="col-3 d-flex align-items-start flex-column">
                                @if($prices = $requestProduct->requestProductPrices->first())
                                <div class="row mr-2 mb-2">
                                ${{ $prices->min }} {{ ($prices->max)? ' - $'.$prices->max : '' }}<br>
                                </div>
                                @endif
                                
                                @if($characteristics = $requestProduct->characteristics->first())
                                <div class="row mr-2">
                                {{ __('cabinet.favorite.form_min_order') }}: {{ $characteristics['value'].' '.$characteristics['type']}} 
                                </div>
                                @endif
                               
                            </div>
                        </div>
                    @endforeach
                </div>
            @endif
        </div>
    </div>
    @include('modals.destroy') 
@endsection
--}}

@section('cabinet-content')
<div class="cabinet">
	@section('breadcrumbs', Breadcrumbs::render())
    @yield('breadcrumbs')
        
	<div class="cabinet__title">{{ Breadcrumbs::generate()->last()->title }}</div>
	
    <div class="cabinet__content">
		
        @include ('widgets.filter_favorites', ['categories' => $categories])
        
		<div class="cabinet__content__products">
			
            @include ('cabinet.favorite._nav', ['page' => 'request_product'])
            
            @if($favorites->first())
            
                <div class="cabinet__content__productsFilters">
                    
                    <div class="cabinet__content__productsFilters__first">
                        <div class="form">
                            <div class="form__row__checkbox">
                                <input type="checkbox" name="is_active" value="true" id="toogle-active" @if(request('is_active') === 'true') checked @endif >
                                <label for="toogle-active">{{ __('interface.buttons.show_active_products') }}</label>
                            </div>
                        </div>
                    </div>
                    
                    {{--
                    <div class="cabinet__content__productsFilters__second">
                        <select name="order_by" id='order-by' class="cabinet__content__productsFilters__select- catalog__filterTop__sort__select">
                            <option {{ (request('order_by') == 'shows_asc')? 'selected' : '' }} value="shows_asc">{{ __('interface.sort.shows') }} &nbsp;&#xf160;</option>
                            <option {{ (request('order_by') == 'shows_desc')? 'selected' : '' }} value="shows_desc">{{ __('interface.sort.shows') }}&nbsp;&#xf161;</option>
                            <option {{ (request('order_by') == 'price_asc')? 'selected' : '' }} value="price_asc">{{ __('interface.sort.price') }} &nbsp;&#xf160;</option>
                            <option {{ (request('order_by') == 'price_desc')? 'selected' : '' }} value="price_desc">{{ __('interface.sort.price') }}&nbsp;&#xf161;</option>
                            <option {{ (request('order_by') == 'rating_asc')? 'selected' : '' }} value="rating_asc">{{ __('interface.sort.rating') }}&nbsp;&#xf160;</option>
                            <option {{ (request('order_by') == 'rating_desc')? 'selected' : '' }} value="rating_desc">{{ __('interface.sort.rating') }}&nbsp;&#xf161;</option>
                        </select>
                    </div>
                    --}}
                    
                </div>
                
                @foreach($favorites as $key => $product)
                <div class="cabinet__products__item cabinet__products__item-nocheckbox">
                    <div class="cabinet__products__item__main">
                        @if($product->is_active)
                        <a target="_blank" href="{{ route('catalog.request_product.show', ['request_product' => $product->id]) }}">
                            <div 
                                class="cabinet__products__item__main__photo" 
                                @if($src = $product->downloads->where('type', 'preview_image')->first())
                                style="background-image: url('{{ asset('storage/'.$src->path) }}');"
                                @endif
                            >
                            </div>
                        </a>
                        @else
                        <div 
                            class="cabinet__products__item__main__photo" 
                            @if($src = $product->downloads->where('type', 'preview_image')->first())
                            style="background-image: url('{{ asset('storage/'.$src->path) }}'); opacity: 0.6;"
                            @endif
                        >
                        </div>
                        @endif
                        <div class="cabinet__products__item__main__info">
                           
                           @if(!$product->is_active)
                            <div class="cabinet__products__item__main__title">
                                <div class="cabinet__products__item__main__title__text">
                                    {{ $product->requestProductProperties->where('code', 'name')->first()->value ?? '' }}
                                </div>
                                
                                <div class="cabinet__products__item__main__title__text">
                                    <strong>{{ __('cabinet.request_product.request_product_not_active') }}</strong>
                                </div>
                               
                                <div class="cabinet__products__item__main__title__art">
                                    <strong>{{ __('cabinet.request_product.form_number') }}:</strong>
                                    {{ $product->id }}
                                </div>
                                
                                <div class="cabinet__products__item__main__title__category">
                                    {{ __('cabinet.product.form_category') }}: {{ $product->categories->name }}
                                </div>
                                
                            </div>
                            @else
                            <a target="_blank" href="{{ route('catalog.request_product.show', ['request_product' => $product->id]) }}">
                                <div class="cabinet__products__item__main__title">
                                    <div class="cabinet__products__item__main__title__text">
                                        {{ $product->requestProductProperties->where('code', 'name')->first()->value ?? '' }}
                                    </div>
                                   
                                    <div class="cabinet__products__item__main__title__art">
                                        <strong>{{ __('cabinet.request_product.form_number') }}:</strong>
                                        {{ $product->id }}
                                    </div>
                                    
                                    <div class="cabinet__products__item__main__title__category">
                                        {{ __('cabinet.product.form_category') }}: {{ $product->categories->name }}
                                    </div>
                                    
                                </div>
                            </a>
                            @endif

                            <div class="cabinet__products__item__main__actions">
                                <a target="_blank" href="{{ route('cabinet.chat.member', ['member' => $product->profiles->id]) }}" class="button button-inline button-primary">
                                    {{ __('interface.buttons.write_to_buyer') }}
                                </a>
                                <button 
                                    type="button" 
                                    class="delete-product-btn button button-inline button-primary-link" 
                                    data-text="{{ __('modals.your_sure_destroy', ['item' => $product->requestProductProperties->where('code', 'name')->first()->value]) }}" 
                                    data-route="{{ route('cabinet.favorite.remove', ['user' => Auth::user(), 'type' => 'request_product', 'id' => $product->id]) }}">
                                    {{ __('interface.buttons.remove_from_favorites') }}
                                </button>
                            </div>
                        </div>
                        
                        @if($product->requestProductPrices->first())
                        <div class="cabinet__products__item__main__price">
                            <div class="cabinet__products__item__main__price__value">
                            {{ ($product->requestProductPrices->first()->min ?? '') ? '$' . $product->requestProductPrices->first()->min : '' }} 
                            {{ ($product->requestProductPrices->first()->max ?? '') ? ' — ' . '$' . $product->requestProductPrices->first()->max : '' }}
                            </div>
                            @if(!empty($product->characteristics['min_order']))
                                <div class="cabinet__products__item__main__price__minimum">
                                    {{ $product->characteristics['min_order']['value'] }} 
                                    {{ $product->characteristics['min_order']['type'] }}
                                    <span>{{ __('interface.catalog.min_order') }}</span>
                                </div>
                            @endif
                        </div>
                        @else
                        <div class="cabinet__products__item__main__price">
                            <div class="cabinet__products__item__main__price__value">
                                {{ __('interface.catalog.price_on_request') }}
                            </div>
                        </div>
                        @endif
                          
                          {{--
                        
                            <div class="cabinet__products__item__main__price__value">$150 035.00 — $150 036.40</div>
                            <div class="cabinet__products__item__main__price__minimum">50 шт <span>(мин. заказ)</span></div>
                        </div>
                          --}}
                    </div>
                </div>
                @endforeach
                
            @endif
		</div>
	</div>
</div>
@include('modals.destroy') 
@endsection

@section('content-script')
<script>
document.addEventListener('DOMContentLoaded', function() {
    cabinet.init('.delete-product-btn', 'click', 'deleteProductOrModel');
    cabinet.init('#toogle-active', 'click', 'toogleActiveElements');
    cabinet.init('#filter-favorites-reset', 'click', 'resetFavoriteFilters');
});
</script>
@endsection