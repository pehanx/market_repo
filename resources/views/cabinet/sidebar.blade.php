{{-- <div class="col-12 col-md-3">
    <div id="cabinet-sidebar">
        @if( Session::has('active_profile') )
            @php $profile = Session::get('active_profile') @endphp
            <h6>{{ $profile->profileProperties->where('code', 'company_name')->first()->value ?? ''}} {{ '(' . __('cabinet.profile.'.$profile->profileTypes->code) . ')' }} </h6>
        @endif
        
        <ul class="list-group list-group-flush toogler-sidebar">
            <li class="list-group-item border-0">
                <a  class="collapsed collapse-item" data-toggle="collapse" data-target="#accordion-sidebar" aria-expanded="false"> 
                    {{ __('sidebars.cabinet') }}
                </a>
            </li>
        </ul>
        <div class="accordion collapse" id="accordion-sidebar">
            <div id="heading-one">
                <ul class="list-group list-group-flush">
                    <li class="list-group-item">
                        <a class="{{ ($page === 'account' || $page === 'entrance') ? '' : 'collapsed' }} collapse-item" data-toggle="collapse" data-target="#collapse-one" aria-expanded="{{ ($page === 'account' || $page === 'entrance') ? 'true' : 'false' }}" aria-controls="collapse-one">
                            {{ __('sidebars.my_account') }}
                        </a>
                        
                    </li>
                </ul>
            </div>
            <div id="collapse-one" class="collapse {{ ($page === 'account' || $page === 'entrance') ? 'show' : '' }}" aria-labelledby="heading-one" data-parent="#accordion-sidebar">
                <ul class="list-group list-group-flush ml-4">
                    <li class="list-group-item">
                        <a href="{{ route('cabinet.account.show', Auth::user()->id) }}" class="{{ $page === 'account' ? ' active' : '' }}">{{ __('sidebars.private_information') }}</a>
                    </li> 
                    <li class="list-group-item  border-0">
                        <a href="{{ route('cabinet.account.edit_entrance', Auth::user()->id) }}" class="{{ $page === 'entrance' ? ' active' : '' }}">{{ __('sidebars.entrance_account') }}</a>
                    </li> 
                </ul>
            </div>
            
            @hasanyrole('seller|buyer|member')
            <div id="heading-two">
                <ul class="list-group list-group-flush">
                    <li class="list-group-item">
                        <a class="{{ ($page === 'profile' || $page === 'seller' || $page === 'buyer') ? '' : 'collapsed' }} collapse-item" data-toggle="collapse" data-target="#collapse-two" aria-expanded="{{ ($page === 'profile' || $page === 'seller' || $page === 'buyer') ? 'true' : 'false' }}" aria-controls="collapse-two">
                            {{ __('sidebars.profiles') }}
                        </a>
                        
                    </li>
                </ul>
            </div>
            <div id="collapse-two" class="collapse {{ ($page === 'profile' || $page === 'seller' || $page === 'buyer') ? 'show' : '' }}" aria-labelledby="heading-two" data-parent="#accordion-sidebar">
                <ul class="list-group list-group-flush ml-4">
                    <li class="list-group-item">
                        <a href="{{ route('cabinet.profile.index', ['type' => 'seller']) }}" class="{{ ($page === 'seller') ? 'active' : '' }}">{{ __('cabinet.profile.seller') }}</a>
                    </li> 
                    <li class="list-group-item border-0">
                        <a href="{{ route('cabinet.profile.index', ['type' => 'buyer']) }}" class="{{ ($page === 'buyer') ? 'active' : '' }}">{{ __('cabinet.profile.buyer') }}</a>
                    </li> 
                </ul> 
            </div>
            @endhasanyrole
            
            
            @hasanyrole('seller|buyer')
         
             <div id="heading-three">
                <ul class="list-group list-group-flush">
                    <li class="list-group-item">
                        <a class="{{ ($page === 'manager') ? '' : 'collapsed' }} collapse-item" data-toggle="collapse" data-target="#collapse-three" aria-expanded="{{ ($page === 'manager') ? 'true' : 'false' }}" aria-controls="collapse-three">
                            {{ __('sidebars.managers') }}
                        </a>
                    </li>
                </ul>
            </div>
            <div id="collapse-three" class="collapse {{ ($page === 'manager') ? 'show' : '' }}" aria-labelledby="heading-three" data-parent="#accordion-sidebar">
                <ul class="list-group list-group-flush ml-4">
                    <li class="list-group-item border-0">
                        <a href="{{ route('cabinet.manager.index_all', Auth::user()->id) }}" class="{{ ($page === 'manager') ? 'active' : '' }}">{{ __('sidebars.managers_profiles') }}</a>
                    </li>
                 
                </ul> 
            </div>
            
            @endhasanyrole
             
            @hasanyrole('seller')
                @if( Session::has('active_profile') && Session::get('active_profile')->profileTypes->code === 'seller' )
                <div id="heading-four">
                    <ul class="list-group list-group-flush">
                        <li class="list-group-item">
                            <a class="{{ ($page === 'product') ? '' : 'collapsed' }} collapse-item" data-toggle="collapse" data-target="#collapse-four" aria-expanded="{{ ($page === 'product') ? 'true' : 'false' }}" aria-controls="collapse-four">
                                {{ __('sidebars.products') }}
                            </a>
                            
                        </li>
                    </ul>
                </div>
                <div id="collapse-four" class="collapse {{ ($page === 'product') ? 'show' : '' }}" aria-labelledby="heading-four" data-parent="#accordion-sidebar">
                    <ul class="list-group list-group-flush ml-4">
                        <li class="list-group-item border-0">
                            <a href="{{ route('cabinet.product.index', $profile->id) }}" class="{{ ($page === 'product') ? 'active' : '' }}">{{ __('sidebars.all_products') }}</a>
                        </li>
                       <li class="list-group-item border-0">
                            <a href="{{ route('cabinet.export.index', $profile->id) }}" class="{{ ($page === 'exports') ? 'active' : '' }}">{{ __('sidebars.exports') }}</a>
                        </li>
                     
                    </ul> 
                </div>
                @endif
            @endhasanyrole
            
            @hasanyrole('buyer')
                @if( Session::has('active_profile') && Session::get('active_profile')->profileTypes->code === 'buyer' )
                <div id="heading-five">
                    <ul class="list-group list-group-flush">
                        <li class="list-group-item">
                            <a class="{{ ($page === 'request_product') ? '' : 'collapsed' }} collapse-item" data-toggle="collapse" data-target="#collapse-five" aria-expanded="{{ ($page === 'request_product') ? 'true' : 'false' }}" aria-controls="collapse-five">
                                {{ __('sidebars.request_products') }}
                            </a>
                            
                        </li>
                    </ul>
                </div>
                <div id="collapse-five" class="collapse {{ ($page === 'request_product') ? 'show' : '' }}" aria-labelledby="heading-five" data-parent="#accordion-sidebar">
                    <ul class="list-group list-group-flush ml-4">
                        <li class="list-group-item border-0">
                            <a href="{{ route('cabinet.request_product.index', $profile->id) }}" class="{{ ($page === 'request_product') ? 'active' : '' }}">{{ __('sidebars.all_request_products') }}</a>
                        </li>
                        <li class="list-group-item border-0">
                            <a href="{{ route('cabinet.export.index', $profile->id) }}" class="{{ ($page === 'exports') ? 'active' : '' }}">{{ __('sidebars.exports') }}</a>
                        </li>

                    </ul> 
                </div>
                @endif
            @endhasanyrole
            
            <div id="heading-fifth">
                <ul class="list-group list-group-flush">
                    <li class="list-group-item">
                        <a class="{{ ($page === 'favorite') ? '' : 'collapsed' }} collapse-item" data-toggle="collapse" data-target="#collapse-fifth" aria-expanded="{{ ($page === 'favorite') ? 'true' : 'false' }}" aria-controls="collapse-fifth">
                            {{ __('sidebars.favorite') }}
                        </a>
                        
                    </li>
                </ul>
            </div>
            <div id="collapse-fifth" class="collapse {{ ($page === 'favorite') ? 'show' : '' }}" aria-labelledby="heading-fifth" data-parent="#accordion-sidebar">
                <ul class="list-group list-group-flush ml-4">
                    <li class="list-group-item border-0">
                        <a href="{{ route('cabinet.favorite.index', ['user' => Auth::user()->id, 'type' => 'product']) }}" class="{{ ($page === 'favorite') ? 'active' : '' }}">{{ __('sidebars.favorite') }}</a>
                    </li>
                 
                </ul> 
            </div>
            
            @role('admin')
            <ul class="list-group list-group-flush">
                <li class="list-group-item">
                    <a href="{{ route('admin.home') }}">{{ __('sidebars.admin_panel') }}</a>
                </li> 
            </ul>
            @endrole
            
        </div>
    </div>
</div> --}}

{{--<div class="modal left fade" id="sidebarModal" tabindex="-1" role="dialog">--}}
{{--<div class="sidebar modal-dialog">--}}
{{--    <div class="sidebar__menu">--}}

@if( Session::has('active_profile') )
    @php $profile = Session::get('active_profile') @endphp
@endif

<div class="modal left fade" id="sidebar-modal">
    <div class="modal-dialog sidebar" role="document">
        <div class="sidebar__menu">

        @role('admin')
        @else
            <div class="sidebar__menu__item sidebar__menu__item__burger">
                <div class="sidebar__menu__item__icon" data-toggle="modal" data-target="#sidebar-modal"></div>
                <a href="{{ route('main') }}" class="header__logo"></a>
            </div>
            <div class="sidebar__menu__item sidebar__menu__item__user">
                <div class="sidebar__menu__item__icon"></div>
                <div class="sidebar__menu__item__title"> {{ __('sidebars.my_account') }}</div>
                <div class="sidebar__menu__item__showMore"></div>
                <div class="sidebar__menu__item__childs hidden">
                    <ul>
                        <li><a href="{{ route('cabinet.account.edit', auth()->user()->id) }}">{{ __('sidebars.private_information') }}</a></li>
                        <li><a href="{{ route('cabinet.account.edit_entrance', auth()->user()->id) }}">{{ __('sidebars.entrance_account') }}</a></li>
                    </ul>
                </div>
            </div>

            @if(auth()->user()->hasRole('site_manager'))
            <div class="sidebar__menu__item sidebar__menu__item__operator">
                <div class="sidebar__menu__item__icon"></div>
                <div class="sidebar__menu__item__title">{{ __('sidebars.manager_cabinet') }}</div>
                <div class="sidebar__menu__item__showMore"></div>
                <div class="sidebar__menu__item__childs hidden">
                    <ul>
                        <li><a href="{{ route('cabinet.manager.companies', auth()->user()->id) }}">{{ __('sidebars.companies') }}</a></li>
                    </ul>
                </div>
            </div>
            @endif

            <div class="sidebar__menu__item sidebar__menu__item__learning">
                <div class="sidebar__menu__item__icon"></div>
                <div class="sidebar__menu__item__title">{{ __('sidebars.profiles') }}</div>
                <div class="sidebar__menu__item__showMore"></div>
                <div class="sidebar__menu__item__childs hidden">
                    <ul>
                        <li><a href="{{ route('cabinet.profile.index', ['type' => 'seller']) }}">{{ __('sidebars.seller') }}</a></li>
                        <li><a href="{{ route('cabinet.profile.index', ['type' => 'buyer']) }}">{{ __('sidebars.buyer') }}</a></li>
                    </ul>
                </div>
            </div>

            {{-- @if(auth()->user()->hasRole('buyer', 'seller'))
            <div class="sidebar__menu__item sidebar__menu__item__operator">
                <div class="sidebar__menu__item__icon"></div>
                <div class="sidebar__menu__item__title">{{ __('sidebars.managers') }}</div>
                <div class="sidebar__menu__item__showMore"></div>
                <div class="sidebar__menu__item__childs hidden">
                    <ul>
                        <li><a href="{{ route('cabinet.manager.index_all', auth()->user()->id) }}">{{ __('sidebars.managers_profiles') }}</a></li>
                    </ul>
                </div>
            </div>
            @endif --}}

            <div class="sidebar__menu__item sidebar__menu__item__messages">
                <div class="sidebar__menu__item__icon"></div>
                <div class="sidebar__menu__item__title">{{ __('sidebars.messages') }}</div>
                <div class="sidebar__menu__item__showMore"></div>
                <div class="sidebar__menu__item__childs hidden">
                    <ul>
                        @if( Session::has('active_profile') )
                            <li><a href="{{ route('cabinet.chat.index', ['profile'=>$profile->id,'tab'=>'contact']) }}">{{ __('sidebars.contacts') }}</a></li>
                            <li><a href="{{ route('cabinet.chat.index', $profile->id) }}">{{ __('sidebars.chats') }}</a></li>
                        @else
                            <li><a href="{{ route('cabinet.chat.support') }}">{{ __('sidebars.chats_support') }}</a></li>
                        @endif
                    </ul>
                </div>
            </div>

            <div class="sidebar__menu__item sidebar__menu__item__list">
                <div class="sidebar__menu__item__icon"></div>
                <div class="sidebar__menu__item__title">{{ __('sidebars.favorite') }}</div>
                <div class="sidebar__menu__item__showMore"></div>
                <div class="sidebar__menu__item__childs hidden">
                    <ul>
                        <li><a href="{{ route('cabinet.favorite.index', ['user' => auth()->user()->id, 'type' => 'product']) }}">{{ __('sidebars.products') }}</a></li>
                        <li><a href="{{ route('cabinet.favorite.index', ['user' => auth()->user()->id, 'type' => 'request_product']) }}">{{ __('sidebars.request_products') }}</a></li>
                    </ul>
                </div>
            </div>

            @if( Session::has('active_profile') && Session::get('active_profile')->profileTypes->code === 'seller' )
                @role('seller|site_manager')
                <div class="sidebar__menu__spacer"></div>
                <div class="sidebar__menu__item sidebar__menu__item__package">
                    <div class="sidebar__menu__item__icon"></div>
                    <div class="sidebar__menu__item__title">{{ __('sidebars.products') }}</div>
                    <div class="sidebar__menu__item__showMore"></div>
                    <div class="sidebar__menu__item__childs hidden">
                        <ul>
                            <li><a href="{{ route('cabinet.product.index', $profile->id) }}">{{ __('sidebars.catalog_products') }}</a></li>
                            {{-- <li><a href="{{ route('cabinet.import.index', $profile->id) }}">{{ __('sidebars.import_products') }}</a></li>
                            <li><a href="{{ route('cabinet.export.index', $profile->id) }}">{{ __('sidebars.export_products') }}</a></li> --}}
                        </ul>
                    </div>
                </div>
                @endrole
            @endif

            @if( Session::has('active_profile') && Session::get('active_profile')->profileTypes->code === 'buyer' )
                @role('buyer|site_manager')
                <div class="sidebar__menu__spacer"></div>
                <div class="sidebar__menu__item sidebar__menu__item__order">
                    <div class="sidebar__menu__item__icon"></div>
                    <div class="sidebar__menu__item__title">{{ __('sidebars.request_products') }}</div>
                    <div class="sidebar__menu__item__showMore"></div>
                    <div class="sidebar__menu__item__childs hidden">
                        <ul>
                            <li><a href="{{ route('cabinet.request_product.index', $profile->id) }}">{{ __('sidebars.catalog_requests') }}</a></li>
                            {{-- <li><a href="{{ route('cabinet.import.index', $profile->id) }}">{{ __('sidebars.import_request_products') }}</a></li>
                            <li><a href="{{ route('cabinet.export.index', $profile->id) }}">{{ __('sidebars.export_request_products') }}</a></li> --}}
                        </ul>
                    </div>
                </div>
                @endrole
            @endif
        @endrole

        @role('admin')
        <div class="sidebar__menu__item sidebar__menu__item__user">
			<div class="sidebar__menu__item__icon"></div>
			<div class="sidebar__menu__item__title">{{ __('sidebars.site_settings') }}</div>
			<div class="sidebar__menu__item__showMore"></div>
			<div class="sidebar__menu__item__childs hidden">
				<ul>
					<li><a href="{{ route('admin.home') }}">{{ __('sidebars.admin_panel') }}</a></li>
				</ul>
			</div>
		</div>
        @endrole
	</div>
</div>
</div>
