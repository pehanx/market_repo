@extends('cabinet.home')

@section('sidebar')
    @include ('cabinet.sidebar', ['page' => 'request_product'])
@endsection

{{--
@section('cabinet-content')
    <div class="col-12 col-md-9">
    
        @section('breadcrumbs', Breadcrumbs::render())
        @yield('breadcrumbs')
     
        @include ('widgets.filter_request_products', ['categories' => $categories])

        <div class="card border-0">
            <div class="card-header bg-white">
                <div class="form-group row mt-1">
                    <a class="btn btn-light border border-black mx-1" href="{{ route('cabinet.request_product.create.choose_category', $profile->id) }}">{{ __('cabinet.request_product.add') }}</a>
                    <a class="btn btn-light border border-black mr-1" href="{{ route('cabinet.import.create', ['profile' => $profile->id, 'type' => 'request_product']) }}">{{ __('cabinet.import.import') }}</a>
                    <ul class="navbar-nav w-25">
                                
                        <li class="nav-item dropdown">
                            <button id="navbar-drop" class="btn btn-light border border-black mr-1 dropdown-toggle" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                {{ __('cabinet.export.export') }}
                            </button>

                            <div class="dropdown-menu border-0" aria-labelledby="navbar-drop">
                                <ul class="navbar-nav">
                                    <a class="export-page mt-3" 
                                        data-route="{{ route('cabinet.export.store.page', ['profile' => $profile, 'type' => 'request_product', 'status'=>'active']) }}" 
                                        data-elements="@json($requestProducts->pluck('id'))"
                                        >
                                        {{ __('cabinet.export.page') }}
                                    </a>

                                    <a class="mt-3" href="{{ route('cabinet.export.create.all', ['profile' => $profile, 'type' => 'request_product']) }}">
                                        {{ __('cabinet.export.all_request_products') }}
                                    </a>
                                </ul>
                            </div>
                        </li>

                    </ul>
                </div>
                
                @include ('cabinet.request_product._nav', ['page' => 'active'])
                
            </div>

            @if($requestProducts->first())
            <div class="card-header bg-white">
                <div class="row align-items-center">
                    <input type="checkbox" id="toogle-elements"><span class="mr-5">{{ __('interface.buttons.choose_all') }}</span>
                    <ul class="navbar-nav w-25">
                                
                        <li class="nav-item dropdown">
                            <a id="navbar-drop" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                {{ __('interface.buttons.action') }}
                            </a>

                            <div class="dropdown-menu border-0" aria-labelledby="navbar-drop">
                                <ul class="navbar-nav">
                                    <a class="actions mt-3" data-action-input="disable_checked" data-text="{{ __('modals.your_sure_disable', ['item' => __('modals.checked_elements') ]) }}">
                                        {{ __('interface.buttons.disable_choosen') }}
                                    </a>
                                    
                                    <a class="actions mt-3" data-action-input="delete_checked" data-text="{{ __('modals.your_sure_delete', ['item' => __('modals.checked_elements') ]) }}">
                                        {{ __('interface.buttons.delete_choosen') }}
                                    </a>
                                    <a class="actions mt-3" data-action-input="disable_all" data-text="{{ __('modals.your_sure_disable', ['item' => __('modals.all_elements') ]) }}">
                                        {{ __('interface.buttons.disable_all') }}
                                    </a>
                                    
                                    <a class="actions mt-3" data-action-input="delete_active_all" data-text="{{ __('modals.your_sure_delete', ['item' => __('modals.all_elements') ]) }}">
                                        {{ __('interface.buttons.delete_all') }}
                                    </a>
                                </ul>
                            </div>
                        </li>
                        
                    </ul>
                </div>
            </div>
            <form method="POST" accept-charset="UTF-8" id="product-action-form" class="form-vertical" enctype="multipart/form-data" action="{{ route('cabinet.request_product.action', ['profile' => $profile]) }}">
                @csrf
                <div class="card-body">
                    @foreach($requestProducts as $key => $product)
                        <div class="row my-3 py-3 shadow">
                            <div class="col-12">
                                <input type="checkbox" class="action-elements" name="elements[{{ $key }}]" value="{{ $product->id }}">
                            </div>
                            <div class="col-2">
                                @if($src = $product->downloads->where('type', 'preview_image')->first())
                                <a class="" href="{{ route('cabinet.request_product.show', ['profile' => $profile->id, 'request_product' => $product->id]) }}">
                                    <img width="100" height="150" src="{{ asset('storage/'.$src->path) }}">   
                                </a>
                                @endif
                            </div>
                            <div class="col-8">
                                <a class="" href="{{ route('cabinet.request_product.show', ['profile' => $profile->id, 'request_product' => $product->id]) }}">
                                    @if($product->requestProductProperties->where('code', 'name')->first())
                                    <div class="row">
                                    {{ $product->requestProductProperties->where('code', 'name')->first()->value }}
                                    </div>
                                    @endif
                                    <div class="row">
                                    {{ __('cabinet.request_product.form_number') }}: {{ $product->id }}
                                    </div>
                                    <div class="row text-black-50">
                                    {{ __('cabinet.request_product.form_category') }}: {{ $product->categories->name }}
                                    </div>
                                </a>
                                <div class="row mt-5">
                                    <a class="mr-2 btn btn-primary text-white" href="{{ route('cabinet.request_product.edit', ['profile' => $profile->id, 'request_product' => $product->id]) }}">
                                        {{ __('interface.buttons.edit') }}
                                    </a>
                                    
                                    <button type="button" class="disable-product-btn mr-2 btn btn-warning" data-text="{{ __('modals.your_sure_disable', ['item' => $product->requestProductProperties->where('code', 'name')->first()->value]) }}" data-route="{{ route('cabinet.request_product.disable', ['profile' => $profile, 'request_product' => $product]) }}">
                                        {{ __('interface.buttons.disable') }}
                                    </button>
                                    
                                    <button type="button" class="delete-product-btn mr-2 btn btn-danger" data-text="{{ __('modals.your_sure_destroy', ['item' => $product->requestProductProperties->where('code', 'name')->first()->value]) }}" data-route="{{ route('cabinet.request_product.delete', ['profile' => $profile, 'request_product' => $product]) }}">
                                        {{ __('interface.buttons.delete') }}
                                    </button>
                                </div>
                            </div>
                            <div class="col-2 d-flex justify-content-end">
                                @if($prices = $product->requestProductPrices->first())
                                <div class="row mr-2">
                                ${{ $prices->min }} {{ ($prices->max)? ' - $'.$prices->max : '' }}
                                </div>
                                @endif
                            </div>
                        </div>
                    @endforeach
                </div>
                <input type="hidden" name="action" value="">
            </form>
            <div class="row justify-content-center mt-5">
            {{ $requestProducts->links() }}
            </div>
            @endif
        </div>
    </div>
    @include('modals.destroy') 
    @include('modals.disable')
    @include('modals.action')     
    @include('modals.export')
@endsection
--}}

@section('cabinet-content')
<div class="cabinet">
	@section('breadcrumbs', Breadcrumbs::render())
    @yield('breadcrumbs')

    <div class="cabinet__content__title__wrapper">
        <div class="cabinet__title">{{ Breadcrumbs::generate()->last()->title }}</div>
        <a class="button button-inline button-primary-bordered" href="{{ route('cabinet.request_product.create.choose_category', $profile->id) }}">
            {{ __('cabinet.request_product.add') }}
        </a>
    </div>
    
	<div class="cabinet__content">
    
		@include ('widgets.filter_request_products', ['categories' => $categories])

		<div class="cabinet__content__products">
        
			@include ('cabinet.request_product._nav', ['page' => 'active'])
           
            @if($requestProducts->first())
                
                <div class="cabinet__products__checkboxActions">
                
                    <div class="form__row__checkbox">
                        <input type="checkbox" class="form__row__checkbox__input" id="toogle-elements">
                        <label for="toogle-elements" class="form__row__checkbox__label">{{ __('interface.buttons.choose_all') }}</label>
                    </div>
                    
                    <div class="cabinet__products__checkboxActions__select">
                        <div class="cabinet__products__checkboxActions__select__button">{{ __('interface.buttons.choose_action') }}</div>
                        <div class="cabinet__products__checkboxActions__select__items"> 
                            <ul>
                                <li class="actions" data-action-input="disable_checked" data-text="{{ __('modals.your_sure_disable', ['item' => __('modals.checked_requests') ]) }}">
                                    {{ __('interface.buttons.disable_choosen') }}
                                </li>
                                
                                <li class="actions" data-action-input="delete_checked" data-text="{{ __('modals.your_sure_delete', ['item' => __('modals.checked_requests') ]) }}">
                                    {{ __('interface.buttons.delete_choosen') }}
                                </li>
                                
                                <li class="actions" data-action-input="disable_all" data-text="{{ __('modals.your_sure_disable', ['item' => __('modals.all_requests') ]) }}">
                                    {{ __('interface.buttons.disable_all') }}
                                </li>
                                
                                <li class="actions" data-action-input="delete_active_all" data-text="{{ __('modals.your_sure_delete', ['item' => __('modals.all_requests') ]) }}">
                                    {{ __('interface.buttons.delete_all') }}
                                </li>
                            </ul>
                        </div>
                    </div>
                    
                </div>
                
                <form method="POST" accept-charset="UTF-8" id="product-action-form" enctype="multipart/form-data" action="{{ route('cabinet.request_product.action', ['profile' => $profile]) }}">
                    @csrf
                    @foreach($requestProducts as $key => $product)
                        <div class="cabinet__products__item">
                            
                            <div class="cabinet__products__item__main">

                                <div class="cabinet__products__item__main__checkbox">
                                    <div class="form__row__checkbox">
                                        <input 
                                            type="checkbox" 
                                            class="form__row__checkbox__input action-elements" 
                                            id="element-{{ $product->id }}" 
                                            name="elements[{{ $key }}]" 
                                            value="{{ $product->id }}"
                                        >
                                        <label for="element-{{ $product->id }}"></label>
                                    </div>
                                </div>
                                
                                <a href="{{ route('cabinet.request_product.show', ['profile' => $profile->id, 'request_product' => $product->id]) }}">
                                    <div 
                                        class="cabinet__products__item__main__photo"
                                        @if($product->downloads->where('type', 'preview_image')->first())
                                        style="background-image: url('{{ asset('storage/' . $product->downloads->where('type', 'preview_image')->first()->path) }}');"
                                        @endif
                                    >
                                    </div>
                                </a>
                                
                                <div class="cabinet__products__item__main__info">
                                    
                                    <a href="{{ route('cabinet.request_product.show', ['profile' => $profile->id, 'request_product' => $product->id]) }}">
                                        <div class="cabinet__products__item__main__title">
                                            <div class="cabinet__products__item__main__title__text">
                                                {{ $product->requestProductProperties->where('code', 'name')->first()->value ?? '' }}
                                            </div>
                                            <div class="cabinet__products__item__main__title__art">
                                                <strong>{{ __('cabinet.request_product.form_number') }}:</strong>
                                                {{ $product->id }}
                                            </div>
                                            
                                            <div class="cabinet__products__item__main__title__category">
                                                {{ __('cabinet.product.form_category') }}: {{ $product->categories->name }}
                                            </div>
                                        </div>
                                    </a>
                                    
                                    <div class="cabinet__products__item__main__actions">
                                        
                                        <a 
                                            class="button button-inline button-primary" 
                                            href="{{ route('cabinet.request_product.edit', ['profile' => $profile->id, 'request_product' => $product->id]) }}"
                                        >
                                            {{ __('interface.buttons.edit') }}
                                        </a>
                                        
                                        <button 
                                            type="button" class="button button-inline button-primary-bordered disable-product-btn" 
                                            data-text="{{ __('modals.your_sure_disable', ['item' => $product->requestProductProperties->where('code', 'name')->first()->value]) }}" 
                                            data-route="{{ route('cabinet.request_product.disable', ['profile' => $profile, 'request_product' => $product]) }}"
                                        >
                                            {{ __('interface.buttons.disable') }}
                                        </button>
                                        
                                        <button 
                                            type="button" 
                                            class="delete-product-btn button button-inline button-primary-link" 
                                            data-text="{{ __('modals.your_sure_destroy', ['item' => $product->requestProductProperties->where('code', 'name')->first()->value]) }}" 
                                            data-route="{{ route('cabinet.request_product.delete', ['profile' => $profile, 'request_product' => $product]) }}"
                                        >
                                            {{ __('interface.buttons.delete') }}
                                        </button>
                                        
                                    </div>
                                    
                                </div>
                                
                                @if($prices = $product->requestProductPrices->first() && (bool)$product->requestProductPrices->first()->min)
                                <div class="cabinet__products__item__main__price">
                                    ${{ $product->requestProductPrices->first()->min }} 
                                    {{ $product->requestProductPrices->first()->max ? ' — $' . $product->requestProductPrices->first()->max : '' }}
                                </div>
                                @else
                                <div class="cabinet__products__item__main__price">
                                    {{ __('interface.catalog.price_on_request') }}
                                </div>
                                @endif
                                
                            </div>

                        </div>
                    @endforeach
                    
                    <input type="hidden" name="action">
                </form>

                {{ $requestProducts->render('widgets.pagination') }}
                
            @endif
            
        </div>
    </div>
</div>
@include('modals.destroy') 
@include('modals.disable')
@include('modals.action')     
@endsection

@section('content-script')
<script>
document.addEventListener('DOMContentLoaded', function() {
    
    cabinet.init('.delete-product-btn', 'click', 'deleteProductOrModel');
    cabinet.init('.disable-product-btn', 'click', 'disableProduct');
    
    cabinet.init('.actions', 'click', 'makeActionProduct');
    cabinet.init('#toogle-elements', 'click', 'toogleActionElements');
    cabinet.init('.export-page', 'click', 'exportElements');
    
    $(".action-elements:checked")
        .trigger('click')
        .trigger('click');
});
</script>
@endsection