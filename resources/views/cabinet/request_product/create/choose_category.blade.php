@extends('cabinet.home')

@section('sidebar')
    @include ('cabinet.sidebar', ['page' => ''])
@endsection

{{--
@section('cabinet-content')
    <div class="col-12 col-md-9">
        @section('breadcrumbs', Breadcrumbs::render())
        @yield('breadcrumbs')

        <h3>{{ __('cabinet.request_product.choose_category') }}</h3>
        <input id="filter" autocomplete="off" class="w-75 my-4" placeholder="{{ __('interface.filters.search_category') }}">
        <button class="btn btn-sm border border-dark" id="search-filter" type="button">{{ __('interface.buttons.search') }}</button>
            
        <table id="tree" class="table table-responsive">
            <tbody  class="mh-500">
                <tr> 
                    <td class="w-25"></td>
                    <td class="w-25"></td>
                    <td class="w-25"></td>
                    <td class="w-25"></td>

                </tr>
            </tbody>
        </table> 
        
           <form method="GET" accept-charset="UTF-8" id="set-category-form" class="form-vertical" enctype="multipart/form-data">
                @csrf
                <button class="btn border border-dark" id="set-category-btn" type="button" data-route="{{ route('cabinet.request_product.create.fill_properties', ['profile' => $profile->id, 'category' =>'category']) }}">{{ __('interface.buttons.select') }}</button>
            </form>
    </div>
@endsection
--}}

<style>
    table.fancytree-ext-columnview tbody tr td > ul
    {
        height: 600px;
        width: 290px;
    }
</style>

@section('cabinet-content')
<div class="cabinet">
    @section('breadcrumbs', Breadcrumbs::render())
    @yield('breadcrumbs')
     
    <div class="cabinet__title">
        <div class="cabinet__title__text">
            {{ __('cabinet.request_product.choose_category') }}
        </div>
    </div>
     
    <div class="cabinet__content">
        <div class="cabinet__subtitle">{{ __('cabinet.product.choose_category') }}</div>
        
        <div class="cabinet__content__category__search">
            <input id="filter" autocomplete="off" type="text" class="cabinet__content__category__search__input" placeholder="">
            <button id="search-filter" type="button" class="cabinet__content__category__search__button">{{ __('interface.buttons.search') }}</button>
        </div>

        <table id="tree">
            <tbody>
                <tr> 
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>

                </tr>
            </tbody>
        </table> 
        
        <div class="cabinet__content__category__confirm">
            <form method="GET" accept-charset="UTF-8" id="set-category-form" class="form-vertical" enctype="multipart/form-data">
                @csrf
                <button 
                    class="cabinet__content__category__confirm__button" 
                    id="set-category-btn" 
                    type="button" 
                    data-route="{{ route('cabinet.request_product.create.fill_properties', ['profile' => $profile->id, 'category' =>'category']) }}"
                >{{ __('interface.buttons.select') }}
                </button>
            </form>
        </div>
        
    </div>
</div>
@endsection

@section('content-script')
<script>
        let treeCategories =  new Tree({tree: '#tree', filter: '#filter', searchFilter: '#search-filter'}, @json($categories, JSON_PRETTY_PRINT), ['filter', 'columnview']);
    document.addEventListener('DOMContentLoaded', function() {
        cabinet.init('#set-category-btn', 'click', 'setCategory');
    });
</script>
@endsection