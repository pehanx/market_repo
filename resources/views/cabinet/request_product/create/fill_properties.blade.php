@extends('cabinet.home')

@section('sidebar')
    @include ('cabinet.sidebar', ['page' => ''])
@endsection

{{--
@section('cabinet-content')
    <div class="col-12 col-md-9">
    @section('breadcrumbs', Breadcrumbs::render())
    @yield('breadcrumbs')
    
    <h3>{{ __('cabinet.request_product.fill_properties') }}</h3>

    <form method="POST" accept-charset="UTF-8" id="request_product-create-form" class="form-vertical mt-4 shadow shadow-sm p-2" enctype="multipart/form-data" action="{{ route('cabinet.request_product.store', ['profile' => $profile]) }}">
        @csrf
            
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
  
            
            <div class="form-group row">
                <div class="col-6 col-md-4">
                    {{ __('cabinet.request_product.form_category') }}
                </div>
                <div class="col-6 col-md-8">
                    {{ $category->categoryProperties->pluck('value')->first() }}
                </div>
            </div>
            
            <div class="form-group row">
                <label for="example-text-input" class="col-12 col-md-4 col-form-label">{{ __('cabinet.product.form_name') }}</label>
                <div class="col-12 col-md-8">
                    <input class="form-control" name="name" type="text" value="{{ old('name') }}" >
                </div>
            </div>	
            
            <div class="form-group row">
                <label for="example-text-input" class="col-12 col-md-4 col-form-label">{{ __('cabinet.request_product.form_min_price') }}, ($)</label>
                <div class="col-12 col-md-8">
                    <input class="form-control" name="min_price" type="text" value="{{ old('min_price') }}" >
                </div>
            </div>	
            
            <div class="form-group row">
                <label for="example-text-input" class="col-12 col-md-4 col-form-label">{{ __('cabinet.request_product.form_max_price') }}, ($)</label>
                <div class="col-12 col-md-8">
                    <input class="form-control" name="max_price" type="text" value="{{ old('max_price') }}" >
                </div>
            </div>
            
            <div class="form-group row">
                <div class="input-group">
                    <label for="example-text-input" class="col-12 col-md-4 col-form-label">{{ __('cabinet.request_product.form_min_order') }}</label>
                    <div class="col-12 col-md-8">
                        <div class="row">
                            <div class="col-9">
                                <input type="text" class="form-control" name="min_order[value]">
                            </div>
                            <div class="col-3">
                                <select class="h-100" name="min_order[type]">
                                    @foreach($quantities as $quantity)
                                        <option value="{{ $quantity->code }}">{{ $quantity->value }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    
            <div class="form-group row">
                <label for="example-email-input" class="col-12 col-md-4 col-form-label">{{ __('cabinet.request_product.form_full_description') }}</label>
                <div class="col-12 col-md-8">
                    <textarea class="form-control" name="full_description" >{{ old('full_description') }}</textarea >
                </div>
            </div>
            
            <div class="form-group row mt-3">
                <label class="col-md-4 col-form-label">{{ __('cabinet.request_product.form_detail_images') }}</label>
                <div class="col-sm-8">
                    <input type="file" class="file-handler" name="detail_images[]"  multiple accept="image/*" value="{{ old('detail_images[]') }}">
                </div>
            </div>
            
            <h2>{{ __('cabinet.request_product.form_seo_attributes') }}</h2>
            
            <div class="form-group row">
                <label for="example-phone-input" class="col-12 col-md-4 col-form-label">{{ __('cabinet.request_product.form_seo_title') }}</label>
                <div class="col-12 col-md-8">
                    <input class="form-control" placeholder="" name="seo_title" type="text" value="{{ old('seo_title') }}" >
                </div>
            </div>
            
            <div class="form-group row">
                <label for="example-phone-input" class="col-12 col-md-4 col-form-label">{{ __('cabinet.request_product.form_seo_keywords') }}</label>
                <div class="col-12 col-md-8">
                    <input class="form-control" placeholder="" name="seo_keywords" type="text" value="{{ old('seo_keywords') }}" >
                </div>
            </div>
            
            <div class="form-group row">
                <label for="example-phone-input" class="col-12 col-md-4 col-form-label">{{ __('cabinet.request_product.form_seo_descriptions') }}</label>
                <div class="col-12 col-md-8">
                    <input class="form-control" placeholder="" name="seo_descriptions" type="text" value="{{ old('seo_descriptions') }}" >
                </div>
            </div>
            
            <hr class="mt-5">
            <div class="form-group row ml-1">
                <button type="button" class="btn btn-secondary mt-3 submit-form">{{ __('interface.buttons.request_product_post') }}</button>
            </div>
            
            <input type="hidden" name="category_id" value="{{ $category->id }}">
        </form>
    </div>
@endsection
--}}

@section('cabinet-content')
<div class="cabinet">
    @section('breadcrumbs', Breadcrumbs::render())
    @yield('breadcrumbs')
    <div class="cabinet__title">{{ __('cabinet.request_product.fill_properties') }}</div>
    
    <div class="cabinet__createRequest">
        <form method="POST" accept-charset="UTF-8" id="request-product-create-form" enctype="multipart/form-data" action="{{ route('cabinet.request_product.store', ['profile' => $profile]) }}">
            @csrf
            <div class="cabinet__createRequest__section form">
                <div class="cabinet__createRequest__wrapper">
                    <div class="cabinet__subtitle">{{ __('cabinet.request_product.form_category') }}</div>
                
                    <div class="form__row"> 
                        {{ $category->categoryProperties->pluck('value')->first() }}
                    </div>
                    
                    <div class="cabinet__subtitle">{{ __('cabinet.product.form_main_properties') }}</div>
                    
                    <div class="form__row form__row-required @if($errors->has('name')) {{ 'form__row-error' }} @endif">
                        <input name="name" type="text" value="{{ old('name') }}" class="form__row__input" id="name" required>
                        <label for="name" class="form__row__label">{{ __('cabinet.product.form_name') }}</label>
                        @error('name')
                        <div class="form__row__error">{{ $message }}</div>
                        @enderror
                    </div>
                    
                    <div class="form__double__row form__double__row-between">
                        <div class="form__row form__row-required @if($errors->has('min_price')) {{ 'form__row-error' }} @endif">
                            <input name="min_price" type="text" value="{{ old('min_price') }}" class="form__row__input" id="min-price" required>
                            <label for="min-price" class="form__row__label">{{ __('cabinet.product.form_min_price') }}, ($)</label>
                            @error('min_price')
                            <div class="form__row__error">{{ $message }}</div>
                            @enderror
                        </div>
                        
                        <div class="form__row @if($errors->has('max_price')) {{ 'form__row-error' }} @endif">
                            <input name="max_price" type="text" value="{{ old('max_price') }}" class="form__row__input" id="max-price">
                            <label for="max-price" class="form__row__label">{{ __('cabinet.product.form_max_price') }}, ($)<label>
                            @error('max_price')
                            <div class="form__row__error">{{ $message }}</div>
                            @enderror
                        </div>
                    </div>
                    
                    <div class="form__double__row form__double__row-between">
                        <div class="form__row form__row-required @if($errors->has('min_order.value')) {{ 'form__row-error' }} @endif">
                            <input type="text" name="min_order[value]" class="form__row__input" id="min-order-value" value="{{ old('min_order.value') }}" required>
                            <label for="min-order-value" class="form__row__label">{{ __('cabinet.request_product.form_min_order') }}</label>
                            @error('min_order.value')
                            <div class="form__row__error">{{ $message }}</div>
                            @enderror
                        </div>
                        
                        
                        <div class="form__row form__row-required">
                            <select name="min_order[type]">
                                @foreach($quantities as $quantity)
                                <option 
                                    value="{{ $quantity->code }}" 
                                    {{ (old('min_order[type]') === $quantity->code) ? 'selected' : '' }}
                                >
                                    {{ $quantity->value }}
                                </option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="form__row form__row-required @if($errors->has('full_description')) {{ 'form__row-error' }} @endif">
                        <label for="full-description" class="form__row__label form__row__label__textarea">
                            {{ __('cabinet.request_product.form_full_description') }}
                        </label>
                        <textarea 
                            rows="10"
                            placeholder="{{ __('cabinet.request_product.form_full_description') }}" 
                            class="form__row__textarea form__row__input" 
                            id="full-description"
                            name="full_description">{{ old('full_description') }}</textarea>
                        @error('full_description')
                        <div class="form__row__error">{{ $message }}</div>
                        @enderror
                    </div>
                    
                </div>
            </div>
            
            <div class="cabinet__createRequest__section form ">
                <div class="cabinet__createRequest__wrapper">
                
                    <div class="cabinet__subtitle">{{ __('cabinet.image') }}</div>
                    
                    <div class="form__row @if($errors->has('detail_images')) {{ 'form__row-error' }} @endif">
                        
                        <div class="form__row__uploadImages">
                            <div class="form__row__uploadImages__label">{{ __('cabinet.request_product.form_detail_images') }}</div>
                            <input id="detail-images" type="file" class="form__row__uploadFile__input file-handler" name="detail_images[]"  multiple accept="image/*">
                            <label for="detail-images" class="form__row__uploadFile__label">
                                <div class="button button-primary-bordered button-inline file-handler-multiple-button">{{ __('interface.buttons.choose_images') }}</div>
                            </label>
                        
                        </div>
                        @error('detail_images')
                        <div class="form__row__error">{{ $message }}</div>
                        @enderror
                   
                   </div>
                 
                </div>
            </div>
            
            <div class="cabinet__createRequest__section form">
                <div class="cabinet__createRequest__wrapper">
                    <div class="cabinet__subtitle">{{ __('cabinet.request_product.form_seo_attributes') }}</div>
                    
                    <div class="form__row @if($errors->has('seo_title')) {{ 'form__row-error' }} @endif">
                        <input class="form__row__input" name="seo_title" id="seo-title" type="text" value="{{ old('seo_title') }}" >
                        <label for="seo-title" class="form__row__label">{{ __('cabinet.product.form_seo_title') }}</label>
                        @error('seo_title')
                        <div class="form__row__error">{{ $message }}</div>
                        @enderror
                    </div>
                    
                    <div class="form__row @if($errors->has('seo_keywords')) {{ 'form__row-error' }} @endif">
                        <input class="form__row__input" name="seo_keywords" id="seo-keywords" type="text" value="{{ old('seo_keywords') }}" >
                        <label for="seo-keywords" class="form__row__label">{{ __('cabinet.product.form_seo_keywords') }}</label>
                        @error('seo_keywords')
                        <div class="form__row__error">{{ $message }}</div>
                        @enderror
                    </div>
                    
                    <div class="form__row">
                        <label for="seo-descriptions" class="form__row__label form__row__label__textarea">
                            {{ __('cabinet.product.form_seo_descriptions') }}
                        </label>
                        <textarea rows="10" placeholder="{{ __('cabinet.product.form_seo_descriptions') }}" class="form__row__textarea form__row__input" id="seo-descriptions" name="seo_descriptions">{{ old('seo_descriptions') }}</textarea>
                    </div>
                    
                </div>
            </div>
            
            <div class="cabinet__createRequest__section cabinet__createRequest__section-finish">
                <input type="hidden" name="category_id" value="{{ $category->id }}">
                <button class="button button-primary button-inline submit-form" type="submit">{{ __('interface.buttons.request_product_post') }}</button>
            </div>
            
        </form>
    </div>
</div>
@endsection

@section('content-script')
<script src="{{ mix('js/request_product_create.js', 'assemble') }}"></script>   
@endsection