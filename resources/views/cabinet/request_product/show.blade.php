@extends('cabinet.home')

@section('sidebar')
    @include ('cabinet.sidebar', ['page' => 'product'])
@endsection

{{--
@section('cabinet-content')
    <div class="col-12 col-md-9">
        @section('breadcrumbs', Breadcrumbs::render())
        @yield('breadcrumbs')   
        
        @if($src = $requestProduct->downloads->where('type', 'preview_image')->first())
        <div class="row ml-0">
            <img width="200" height="200" src="{{ asset('storage/'.$src->path) }}">   
        </div>
        @endif
        
        @if($srcs = $requestProduct->downloads->where('type', 'detail_images')->all())
        <div class="row ml-0 mt-5">
            @foreach($srcs as $src)
            <img width="50" height="50" src="{{ asset('storage/'.$src->path) }}">   
            @endforeach
        </div>
        @endif
         
        <div class="form-group row ml-0 mt-3 text-black-50">
            {{ __('cabinet.request_product.form_number') }}
        </div> 
        <div class="form-group row ml-0">
            {{ $requestProduct->id }}
        </div> 
        
        <div class="form-group row ml-0 mt-3 text-black-50">
            {{ __('cabinet.request_product.form_name') }}
        </div> 
        <div class="form-group row ml-0">
            {{ $requestProduct->requestProductProperties->where('code', 'name')->first()->value ?? '—' }}
        </div> 
        
        <div class="form-group row ml-0 mt-3 text-black-50">
            {{ __('cabinet.request_product.form_category') }}
        </div> 
        <div class="form-group row ml-0">
            {{ $requestProduct->category }}
        </div> 
        
        <div class="form-group row ml-0 mt-3 text-black-50">
            {{ __('cabinet.product.form_price') }}
        </div> 
        <div class="form-group row ml-0">
            @php $prices = $requestProduct->requestProductPrices->first() @endphp
            {{ $prices->min }} {{ ($prices->max)? ' - '.$prices->max.'$' : '$' }}
        </div> 
        
        <div class="form-group row ml-0 mt-3 text-black-50">
            {{ __('cabinet.product.form_description') }}
        </div> 
        <div class="form-group row ml-0">
            {{ $requestProduct->requestProductProperties->where('code', 'description')->first()->value ?? '—' }}
        </div> 

        <div class="form-group row ml-0">
            <h4>{{ __('cabinet.request_product.form_seo_attributes') }}</h4>
        </div> 
        
        <div class="form-group row ml-0 mt-3 text-black-50">
            {{ __('cabinet.request_product.form_seo_title') }}
        </div> 
        <div class="form-group row ml-0">
            {{ $requestProduct->requestProductProperties->where('code', 'seo_title')->first() ? $requestProduct->requestProductProperties->where('code', 'seo_title')->first()->value : '—'}}
        </div> 
        
        <div class="form-group row ml-0 mt-3 text-black-50">
            {{ __('cabinet.request_product.form_seo_keywords') }}
        </div> 
        <div class="form-group row ml-0">
            {{ $requestProduct->requestProductProperties->where('code', 'seo_keywords')->first() ? $requestProduct->requestProductProperties->where('code', 'seo_keywords')->first()->value : '—'}}
        </div> 
        
        <div class="form-group row ml-0 mt-3 text-black-50">
            {{ __('cabinet.request_product.form_seo_descriptions') }}
        </div> 
        <div class="form-group row ml-0">
            {{ $requestProduct->requestProductProperties->where('code', 'seo_descriptions')->first() ? $requestProduct->requestProductProperties->where('code', 'seo_descriptions')->first()->value : '—'}}
        </div>
        
    </div>
@endsection
--}}

@section('cabinet-content')
<div class="cabinet">
	@section('breadcrumbs', Breadcrumbs::render())
    @yield('breadcrumbs')
    
	<div class="cabinet__title">{{ Breadcrumbs::generate()->last()->title }}</div>
    
    <div class="cabinet__createProduct__section formView">
        <div class="cabinet__createProduct__wrapper">
            
            <div class="cabinet__subtitle">{{ __('cabinet.request_product.request_product') }} — {{ $requestProduct->requestProductProperties->where('code', 'name')->first()->value }}  ({{ $requestProduct->id }})</div>
            
            @foreach ($requestProduct->downloads as $type => $download)
                @if($type === 'preview_image')
                    <div class="formView__row" style="margin-top: 30px;">
                        <div class="formView__row__image"><img src="{{ asset('storage/'.$download->first()->path) }}"></div>
                    </div>
                    @continue
                @endif
                    
            <div class="formView__row">
                <div class="formView__row__label">{{ __('cabinet.request_product.form_detail_images') }}</div>
                <div class="formView__row__images">
                    @foreach ($download as $item)
                    <div class="formView__row__images__image" style="background-image: url('{{ asset('storage/'.$item->path) }}');"></div>
                    @endforeach
                </div>
            </div>
            @endforeach

        </div>
        
    </div>
    
    <div class="cabinet__createProduct__section formView">
        <div class="cabinet__createProduct__wrapper">

            <div class="formView__row">
                <div class="formView__row__label">{{ __('cabinet.product.form_category_product') }}</div>
                <div class="formView__row__value">{{ $category->categoryProperties->where('code', 'name')->first()->value }}</div>
            </div>
            
            <div class="formView__row">
                <div class="formView__row__label">{{ __('cabinet.product.form_price') }}</div>
                @if($prices = $requestProduct->requestProductPrices->first() && (bool)$requestProduct->requestProductPrices->first()->min)
                <div class="formView__row__value">
                    ${{ $requestProduct->requestProductPrices->first()->min }} 
                    {{ $requestProduct->requestProductPrices->first()->max ? ' — $' . $requestProduct->requestProductPrices->first()->max : '' }}
                </div>
                @else
                <div class="formView__row__value">
                    {{ __('interface.catalog.price_on_request') }}
                </div>
                @endif
            </div>
            
            <div class="formView__row">
                <div class="formView__row__label">{{ __('cabinet.product.form_min_order') }}</div>
                <div class="formView__row__value">
                    {{ $requestProduct->characteristics['min_order']['value'] }} {{ $requestProduct->characteristics['min_order']['type'] }}
                </div>
            </div>
            
            @if($description = $requestProduct->requestProductProperties->where('code', 'full_description')->first())
            <div class="formView__row">
                <div class="formView__row__label">{{ __('cabinet.product.form_description') }}</div>
                <div class="formView__row__value">{{ $description->value }}</div>
            </div>
            @endif
            
        </div>
    </div>

    @php
        $seoTitle = $requestProduct->requestProductProperties->where('code', 'seo_title')->first();
        $seoKeywords = $requestProduct->requestProductProperties->where('code', 'seo_keywords')->first();
        $seoDescriptions = $requestProduct->requestProductProperties->where('code', 'seo_descriptions')->first();
    @endphp

    @if($seoTitle || $seoKeywords || $seoDescriptions)
    <div class="cabinet__createProduct__section">
        <div class="cabinet__createProduct__wrapper">
            <div class="cabinet__subtitle">{{ __('cabinet.product.form_seo_attributes') }}</div>
            
            <div class="formView__row" style="margin-top: 30px;">
                <div class="formView__row__label">{{ __('cabinet.product.form_seo_title') }}</div>
                <div class="formView__row__value">{{ (isset($seoTitle) && $seoTitle->value) ? $seoTitle->value : '—' }}</div>
            </div>
            
            <div class="formView__row">
                <div class="formView__row__label">{{ __('cabinet.product.form_seo_keywords') }}</div>
                <div class="formView__row__value">{{ (isset($seoKeywords) && $seoKeywords->value) ? $seoKeywords->value : '—' }}</div>
            </div>
            
            <div class="formView__row">
                <div class="formView__row__label">{{ __('cabinet.product.form_seo_descriptions') }}</div>
                <div class="formView__row__value">{{ (isset($seoDescriptions) && $seoDescriptions->value) ? $seoDescriptions->value : '—' }}</div>
            </div>
            
        </div>
    </div>
    @endif

    <div class="cabinet__createProduct__section cabinet__createProduct__section-finish">

        <a 
            class="button button-inline button-primary-bordered" 
            href="{{ route('cabinet.request_product.edit', ['profile' => $profile->id, 'request_product' => $requestProduct->id]) }}"
        >
            {{ __('interface.buttons.edit') }}
        </a>
        
        <a 
            class="button button-inline button-primary-link" 
            href="{{ route('cabinet.request_product.index', $profile->id) }}"
        >
            {{ __('interface.buttons.return_index_request_products') }}
        </a>
        
    </div>
        
</div>
@endsection