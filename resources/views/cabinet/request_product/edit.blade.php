@extends('cabinet.home')

@section('sidebar')
    @include ('cabinet.sidebar', ['page' => ''])
@endsection

{{--
@section('cabinet-content')
    <div class="col-12 col-md-9">
    
    @section('breadcrumbs', Breadcrumbs::render())
    @yield('breadcrumbs')
    
    <form method="POST" accept-charset="UTF-8" id="request-product-edit-form" class="form-vertical" enctype="multipart/form-data" action="{{ route('cabinet.request_product.update', ['profile' => $profile, 'request_product' => $requestProduct->id]) }}">
            @csrf
            @method('PUT')
            
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            
            <div class="form-group row">
                <label for="example-email-input" class="col-12 col-md-4 col-form-label">{{ __('cabinet.request_product.form_category') }}</label>
                <div class="col-12 col-md-8">
                    <select class="select2-handle" name="category_id">
                        @foreach($categories as $category)
                        <option {{ ($requestProduct->category_id ==  $category['id'])? 'selected' : '' }} value="{{ $category['id'] }}">{{ $category['text'] }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            
            <div class="form-group row">
                <label for="example-text-input" class="col-12 col-md-4 col-form-label">{{ __('cabinet.request_product.form_name') }}</label>
                <div class="col-12 col-md-8">
                    <input class="form-control" name="name" type="text" value="{{ old('name', $requestProduct->requestProductProperties->where('code', 'name')->first()->value ?? '') }}" >
                </div>
            </div>	
            
            <div class="form-group row">
                <label for="example-text-input" class="col-12 col-md-4 col-form-label">{{ __('cabinet.request_product.form_min_price') }}, ($)</label>
                <div class="col-12 col-md-8">
                    <input class="form-control" name="min_price" type="text" value="{{ old('min_price', $requestProduct->requestProductPrices->first()->min) }}" >
                </div>
            </div>	
            
            <div class="form-group row">
                <label for="example-text-input" class="col-12 col-md-4 col-form-label">{{ __('cabinet.request_product.form_max_price') }}, ($)</label>
                <div class="col-12 col-md-8">
                    <input class="form-control" name="max_price" type="text" value="{{ old('max_price', $requestProduct->requestProductPrices->first()->max ?? '') }}" >
                </div>
            </div>			
            
            <div class="form-group row">
                <div class="input-group">
                    <label for="example-text-input" class="col-12 col-md-4 col-form-label">{{ __('cabinet.request_product.form_min_order') }}</label>
                    <div class="col-12 col-md-8">
                        <div class="row">
                            <div class="col-9">
                                <input type="text" class="form-control" name="min_order[value]" value="{{ old('min_order[value]', $requestProduct->characteristics['min_order']['value'] ?? '') }}">
                            </div>
                            <div class="col-3">
                                <select class="h-100" name="min_order[type]">
                                    @foreach($quantities as $quantity)
                                        <option {{ ( ($requestProduct->characteristics['min_order']['type']?? '') && $requestProduct->characteristics['min_order']['type'] ==  $quantity->value)? 'selected' : '' }} value="{{ $quantity->code }}">{{ $quantity->value }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    
            <div class="form-group row">
                <label for="example-email-input" class="col-12 col-md-4 col-form-label">{{ __('cabinet.request_product.form_full_description') }}</label>
                <div class="col-12 col-md-8">
                    <textarea class="form-control" name="full_description" >{{ old('description', $requestProduct->requestProductProperties->where('code', 'full_description')->first()->value ?? '') }}</textarea >
                </div>
            </div>
            
            <div class="form-group row mt-5">
                <label class="col-sm-6 col-form-label">{{ __('cabinet.request_product.form_detail_images') }}</label>
                <div class="col-sm-6">
                    @foreach ($requestProduct->downloads as $download)
                        @if($download->type === 'detail_images')
                        <div class="container-file-reader">
                            <div class="col-6 col-md-2 d-flex align-items-start">
                                <button type="button" class="delete-file-btn btn btn-secondary"  data-id="{{ $download->id }}"><i class="fa fa-close"></i></button>
                                <img width="50" height="50" data-id-file="{{ $download->id }}" src="{{ asset('storage/'.$download->path) }}">
                            </div>
                        </div>
                        @endif
                    @endforeach 
                    
                    @error('detail_images')
                    <span class="text-danger">{{ $message }}</span>
                    @enderror
                    <input type="file" class="file-handler" name="detail_images[]" multiple accept="image/*" value="{{ old('detail_images') }}">
                </div>    
            </div>
            
            <h2>{{ __('cabinet.request_product.form_seo_attributes') }}</h2>
            
            <div class="form-group row">
                <label for="example-phone-input" class="col-12 col-md-4 col-form-label">{{ __('cabinet.request_product.form_seo_title') }}</label>
                <div class="col-12 col-md-8">
                    <input class="form-control" placeholder="" name="seo_title" type="text" value="{{ old('seo_title', $requestProduct->requestProductProperties->where('code', 'seo_title')->first()->value ?? '') }}" >
                </div>
            </div>
            
            <div class="form-group row">
                <label for="example-phone-input" class="col-12 col-md-4 col-form-label">{{ __('cabinet.request_product.form_seo_keywords') }}</label>
                <div class="col-12 col-md-8">
                    <input class="form-control" placeholder="" name="seo_keywords" type="text" value="{{ old('seo_keywords', $requestProduct->requestProductProperties->where('code', 'seo_keywords')->first()->value ?? '') }}" >
                </div>
            </div>
            
            <div class="form-group row">
                <label for="example-phone-input" class="col-12 col-md-4 col-form-label">{{ __('cabinet.request_product.form_seo_descriptions') }}</label>
                <div class="col-12 col-md-8">
                    <input class="form-control" placeholder="" name="seo_descriptions" type="text" value="{{ old('seo_descriptions', $requestProduct->requestProductProperties->where('code', 'seo_descriptions')->first()->value ?? '') }}" >
                </div>
            </div>
            
            <hr class="mt-5">
            <div class="form-group row ml-1">
                <button type="button" class="btn btn-secondary mt-3 submit-form">{{ __('interface.buttons.save_changes') }}</button>
            </div>
            
        </form>
    </div>
@endsection
--}}

@section('cabinet-content')
<div class="cabinet">
    @section('breadcrumbs', Breadcrumbs::render())
    @yield('breadcrumbs')
    <div class="cabinet__title">{{ Breadcrumbs::generate()->last()->title }}</div>
    
    <form method="POST" accept-charset="UTF-8" id="request-product-edit-form" enctype="multipart/form-data" action="{{ route('cabinet.request_product.update', ['profile' => $profile, 'request_product' => $requestProduct->id]) }}">
        @csrf
        @method('PUT')
        <div class="cabinet__createProduct">
            <div class="cabinet__createProduct__section form">
                <div class="cabinet__createProduct__wrapper">
                    
                    <div class="cabinet__subtitle">{{ __('cabinet.product.form_category_product') }}</div>
                
                    <div class="form__row"> 
                        <select name="category_id">
                            @foreach($categories as $category)
                            <option {{ ($requestProduct->category_id ==  $category['id'])? 'selected' : '' }} value="{{ $category['id'] }}">{{ $category['text'] }}</option>
                            @endforeach
                        </select>
                    </div>
                
                    <div class="cabinet__subtitle">{{ __('cabinet.product.form_main_properties') }}</div>
                    
                    <div class="form__row form__row-required @if($errors->has('name')) {{ 'form__row-error' }} @endif">
                        <input name="name" type="text" value="{{ old('name', $requestProduct->requestProductProperties->where('code', 'name')->first()->value ?? '') }}" class="form__row__input" id="name" required>
                        <label for="name" class="form__row__label">{{ __('cabinet.product.form_name') }}</label>
                        @error('name')
                        <div class="form__row__error">{{ $message }}</div>
                        @enderror
                    </div>
                    
                    <div class="form__double__row form__double__row-between">
                        
                        <div class="form__row form__row-required @if($errors->has('min_price')) {{ 'form__row-error' }} @endif">
                            @if($requestProduct->requestProductPrices->first() && (bool)$requestProduct->requestProductPrices->first()->min)
                            <input name="min_price" type="text" value="{{ old('min_price', $requestProduct->requestProductPrices->first()->min) }}" class="form__row__input" id="min-price" required>
                            @else
                            <input name="min_price" type="text" value="{{ old('min_price') }}" class="form__row__input" id="min-price" required>
                            @endif
                            <label for="min-price" class="form__row__label">{{ __('cabinet.product.form_min_price') }}, ($)</label>
                            @error('min_price')
                            <div class="form__row__error">{{ $message }}</div>
                            @enderror
                        </div>
                        
                        <div class="form__row @if($errors->has('max_price')) {{ 'form__row-error' }} @endif">
                            @if($requestProduct->requestProductPrices->first() && (bool)$requestProduct->requestProductPrices->first()->max)
                            <input name="max_price" type="text" value="{{ old('max_price', $requestProduct->requestProductPrices->first()->max) }}" class="form__row__input" id="max-price">
                            @else
                            <input name="max_price" type="text" value="{{ old('max_price') }}" class="form__row__input" id="max-price">
                            @endif
                            <label for="max-price" class="form__row__label">{{ __('cabinet.product.form_max_price') }}, ($)<label>
                            @error('max_price')
                            <div class="form__row__error">{{ $message }}</div>
                            @enderror
                        </div>
                        
                    </div>
                    
                    <div class="form__double__row form__double__row-between">
                        
                        <div class="form__row form__row-required @if($errors->has('min_order.value')) {{ 'form__row-error' }} @endif">
                            <input type="text" name="min_order[value]" class="form__row__input" id="min-order-value" value="{{ old('available.value', $requestProduct->characteristics['min_order']['value'] ?? '') }}" required>
                            <label for="min-order-value" class="form__row__label">{{ __('cabinet.product.form_min_order') }}</label>
                            @error('min_order.value')
                            <div class="form__row__error">{{ $message }}</div>
                            @enderror
                        </div>
                        
                        
                        <div class="form__row form__row-required">
                            <select name="min_order[type]">
                                @foreach($quantities as $quantity)
                                <option {{ ( ($requestProduct->characteristics['min_order']['type']?? '') && $requestProduct->characteristics['min_order']['type'] ==  $quantity->value)? 'selected' : '' }} value="{{ $quantity->code }}">{{ $quantity->value }}</option>
                                @endforeach
                            </select>
                        </div>
                        
                    </div>
                    
                    <div class="form__row form__row-required">
                        <label for="full-description" class="form__row__label form__row__label__textarea">{{ __('cabinet.request_product.form_full_description') }}</label> 
                        <textarea rows="10" placeholder="{{ __('cabinet.request_product.form_full_description') }}" class="form__row__textarea form__row__input" id="full-description" name="description" >{{ old('full_description', $requestProduct->requestProductProperties->where('code', 'full_description')->first()->value ?? '') }}</textarea>
                    </div>
                    
                </div>
            </div>
            <div class="cabinet__createProduct__section form">
                <div class="cabinet__createProduct__wrapper">
                    
                    <div class="cabinet__subtitle">{{ __('cabinet.image') }}</div>
                    
                    <div class="form__row @if($errors->has('detail_images')) {{ 'form__row-error' }} @endif">
                        
                        <div class="form__row__uploadImages">
                            <div class="form__row__uploadImages__label">{{ __('cabinet.product.form_detail_images') }}</div>
                            
                            @isset($requestProduct->downloads['detail_images'])
                            <div class="form__row__uploadImages__preview"><!--container-->
                                @foreach($requestProduct->downloads['detail_images'] as $detailImage)
                                <div class="form__row__uploadImages__preview__wrapper"><!--wrapper-->
                                    <div class="form__row__uploadImages__preview__image__remove" data-id="{{ $detailImage->id }}">
                                    </div><!--delete-->
                                    <div 
                                        class="form__row__uploadImages__preview__image" 
                                        data-id-file="{{ $detailImage->id }}" 
                                        style="background-image: url('{{ asset('storage/'.$detailImage->path) }}');"                            
                                    ><!--image-->
                                    </div>
                                </div>
                                @endforeach
                            </div>
                            @endisset
                            
                            <input id="detail-images" type="file" class="form__row__uploadFile__input file-handler" name="detail_images[]"  multiple accept="image/*">
                            <label for="detail-images" class="form__row__uploadFile__label">
                                <div class="button button-primary-bordered button-inline file-handler-multiple-button">{{ __('interface.buttons.choose_images') }}</div>
                            </label>
                        
                        </div>
                        @error('detail_images')
                        <div class="form__row__error">{{ $message }}</div>
                        @enderror
                   
                   </div>
                </div>
            </div>

            <div class="cabinet__createProduct__section form">
                
                <div class="cabinet__createProduct__wrapper">
                    <div class="cabinet__subtitle">{{ __('cabinet.product.form_seo_attributes') }}</div>
                    
                    <div class="form__row @if($errors->has('seo_title')) {{ 'form__row-error' }} @endif">
                        <input class="form__row__input" name="seo_title" id="seo-title" type="text" value="{{ old('seo_title', $requestProduct->requestProductProperties->where('code', 'seo_title')->first()->value ?? '') }}" >
                        <label for="seo-title" class="form__row__label">{{ __('cabinet.product.form_seo_title') }}</label>
                        @error('seo_title')
                        <div class="form__row__error">{{ $message }}</div>
                        @enderror
                    </div>
                    
                    <div class="form__row @if($errors->has('seo_keywords')) {{ 'form__row-error' }} @endif">
                        <input class="form__row__input" name="seo_keywords" id="seo-keywords" type="text" value="{{ old('seo_keywords', $requestProduct->requestProductProperties->where('code', 'seo_keywords')->first()->value ?? '') }}" >
                        <label for="seo-keywords" class="form__row__label">{{ __('cabinet.product.form_seo_keywords') }}</label>
                        @error('seo_keywords')
                        <div class="form__row__error">{{ $message }}</div>
                        @enderror
                    </div>
                    
                    <div class="form__row">
                        <label for="description" class="form__row__label form__row__label__textarea">{{ __('cabinet.product.form_seo_descriptions') }}</label>
                        <textarea rows="10" class="form__row__textarea form__row__input" placeholder="{{ __('cabinet.product.form_seo_descriptions') }}" id="seo-descriptions" name="seo_descriptions">{{ old('seo_descriptions', $requestProduct->requestProductProperties->where('code', 'seo_descriptions')->first()->value ?? '') }}</textarea>
                    </div>
                    
                </div>
                
            </div>
            
            <div class="cabinet__createProduct__section cabinet__createProduct__section-finish">
                <button type="button" class="button button-primary-bordered button-inline submit-form">
                {{ __('interface.buttons.save_changes') }}
                </button>
                <a 
                    class="button button-inline button-primary-link" 
                    href="{{ route('cabinet.request_product.index', $profile->id) }}"
                >
                    {{ __('interface.buttons.return_index_request_products') }}
                </a>
            </div>
            
        </div>
    </form>
</div>
@endsection

@section('content-script')
<script src="{{ mix('js/request_product_edit.js', 'assemble') }}"></script>   
@endsection