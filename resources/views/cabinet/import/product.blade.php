@extends('cabinet.home')

@section('sidebar')
    @include ('cabinet.sidebar', ['page' => 'exports.create'])
@endsection

@section('cabinet-content')
    {{-- @section('breadcrumbs', Breadcrumbs::render('cabinet.export.create',$type))
    @yield('breadcrumbs') --}}
    <div class="col-md-6 overflow-auto mw-100">
        <form id="import_form" method="post" action="{{ route('cabinet.import.product',['profile' => $profile])}}">
            @csrf
            <input type="hidden" name='import_id' value='{{$importId}}'>
            <table class="table table-striped" width="20px">
                <thead>
                    <th>Значение колонки</th>
                    <th>Столбец в файле</th>
                    <th></th>
                </thead>
                <tbody id='import_added'>
                    @foreach ($tableHeadings as $tableKey => $tableHeading)
                    <tr>
                        <td>
                            <span class='column-value'>{{$tableHeading}}</span>
                        </td>
                        <td>
                            <select class="column select2" name="import_heading[{{$loop->iteration}}]" data-placeholder="{{ __('cabinet.import.choose_column') }}">
                                @foreach($allowedColumns as $allowedColumnKey => $allowedColumnValue)
                                    <option value="{{$allowedColumnKey}}__{{$tableKey}}">{{$allowedColumnValue}}</option>
                                @endforeach
                            </select>
                        </td>
                        <td>
                            <i class='col-close fa fa-close'></i>
                            <i style='display:none' class='col-restore fa fa-reply'></i>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            <input type="hidden" name='type' value='product'>
        <button type="submit" class="btn btn-black">
            <i class="fa fa-thumbs-o-up" aria-hidden="true"></i>
            {{ __('interface.buttons.import_submit') }}
        </button>
        </form>
    </div>
    <div class="col-md-3">
        <table class="table table-striped">
            <thead>
                <th>Не используется</th>
                <th></th>
            </thead>
            <tbody class='import_removed'>
                
            </tbody>
        </table>
    </div>
@endsection

@section('content-script')
<script>
    document.addEventListener('DOMContentLoaded', function() {
        
        $('.column.select2').val('')
            .select2({
                width: "100%",
                placeholder: $(this).data('placeholder')
            });

        cabinet.init('.column', 'change', 'selectImportedColumn');

        $('table').on('click', '.col-restore' ,function() {
            cabinet.init('.col-restore', 'click', 'restoreImportedColumn');
        });

        $('table').on('click', '.col-close', function() {
            cabinet.init('.col-close', 'click', 'removeImportedColumn');
        });
    });
</script>
@endsection
