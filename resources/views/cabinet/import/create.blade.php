@extends('cabinet.home')

@section('sidebar')
    @include ('cabinet.sidebar', ['page' => 'exports.create'])
@endsection

@section('cabinet-content')
    <div class="col-12 col-md-9">

        @section('breadcrumbs', Breadcrumbs::render('cabinet.import.create',$profile))
        @yield('breadcrumbs')

        <div class="">
            <a href="{{ route('cabinet.import.example_file',['profile' => $profile->id, 'type' => $profile->import_type])}}">Пример таблицы импорта</a>
            <a href="{{ route('cabinet.import.example_lists',['profile' => $profile->id, 'type' => $profile->import_type])}}">
                @if($profile->import_type === 'product')
                Таблица со списком категорий, стран и цветов
                @else
                Таблица со списком категорий
                @endif
            </a>
        </div>

        <form method="post" enctype="multipart/form-data" action="{{ route('profile.import.upload',['profile' => $profile->id, 'type' => $profile->import_type])}}">
        @csrf
        <div class="form-group row">
            <label for="import_images" class="col-12 col-md-4 col-form-label">{{ __('cabinet.import.images_archive') }}</label>
            <div class="col-12 col-md-8">
                <input required type="file" name='import_images' accept=".zip,.rar,.gzip,.tar,.gz,.tar.gz,.7z">
            </div>
        </div>
        
        <div class="form-group row">
            <label for="import_table" class="col-12 col-md-4 col-form-label">{{ __('cabinet.import.table') }}</label>
            <div class="col-12 col-md-8">
                <input reqired type="file" name='import_table' accept=".csv,.xls,.xlsx">
            </div>
        </div>

        <input type='hidden' name="type" value="{{$profile->import_type}}">

        </form>

        <button id='send-import-form' data-route="{{ route('cabinet.import.settings',['profile' => $profile->id, 'type' => $profile->import_type, 'import_id' => 'import_id' ]) }}" class="btn btn-secondary">
            {{ __('interface.buttons.import_submit') }}
        </button>
    </div>
@endsection

@section('content-script')
<script>
    document.addEventListener('DOMContentLoaded', function() {
        
        cabinet.init('#send-import-form', 'click', 'sendImportForm');
        
    });
</script>
@endsection