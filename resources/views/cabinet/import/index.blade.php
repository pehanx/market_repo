@extends('cabinet.home')

@section('sidebar')
    @include ('cabinet.sidebar', ['page' => 'imports'])
@endsection

@section('cabinet-content')
    <div class="col-12 col-md-9">

        @section('breadcrumbs', Breadcrumbs::render())
        @yield('breadcrumbs')

        <div id='imports-table'>
            <table class="w-100">
                <thead>
                    <th>#</th>
                    <th>{{ __('cabinet.import.progress') }}</th>
                    <th>{{ __('cabinet.import.start') }}</th>
                </thead>
                <tbody>
                    @foreach ($imports as $import)
                    <tr id="import-{{$import->id}}">    
                        <td>{{$import->id}}</td>
                        <td width="400px">
                            <div class="progress">
                                <progress class="w-100" max="100" value="{{$import->progress}}"> {{$import->progress}} </progress>
                            </div>
                        </td>
                        <td>
                            <p>{{ $import->created_at }}</p>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection

@section('content-script')

@endsection