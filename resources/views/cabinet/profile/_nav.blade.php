<div class="cabinet__content__tabs">
    <ul class="tabs__titles">
        <li class="{{ $page === 'profile' ? ' tabs__titles__active' : '' }}">
            <a href="{{ route('cabinet.profile.show', $profile->id) }}">
                {{ __('interface.tabs.common_information') }}
            </a>
        </li>
        @if($profile->status === 'active')
        {{--
        <li class="{{ $page === 'managers' ? ' tabs__titles__active' : '' }}">
            <a href="{{ route('cabinet.manager.index', $profile->id) }}">
                {{ __('interface.tabs.managers_profile') }}
            </a>
        </li>
        --}}
        <li class="{{ $page === 'documents' ? ' tabs__titles__active' : '' }}">
            <a href="{{ route('cabinet.profile.show_documents', $profile->id) }}">
                {{ __('interface.tabs.documents') }}
            </a>
        </li>
        @endif
    </ul>
</div>