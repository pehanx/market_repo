@extends('cabinet.home')

{{--
@section('cabinet-content')
    <div class="col col-md-9">
        @section('breadcrumbs', Breadcrumbs::render())
        @yield('breadcrumbs')
        <h2>{{ __("cabinet.profile.creating_{$type}_profile") }}</h2>
        <form method="POST" accept-charset="UTF-8" id="profile-buyer-create-form" class="form-vertical mt-5" enctype="multipart/form-data" action="{{ route('cabinet.profile.store') }}">
            @csrf
            
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

            <div class="form-group row">
                <label for="example-email-input" class="col-12 col-md-4 col-form-label">{{ __('cabinet.profile.language') }}</label>
                <div class="col-12 col-md-8">
                    <select class="select2-handle" name="localisation">
                        @foreach($localisations as $localisation)
                        <option value="{{ $localisation }}">{{ __('interface.localisation.'.$localisation) }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
                
            <h5 class="text-black mb-4">{{ __('cabinet.profile.form_information_about_company') }}</h5>
            
            <div class="form-group row">
                <label for="example-text-input" class="col-12 col-md-4 col-form-label">{{ __('cabinet.profile.form_company_name') }}</label>
                <div class="col-12 col-md-8">
                    <input class="form-control" name="company_name" type="text" value="{{ old('company_name') }}" required>
                </div>
            </div>	
           
            <div class="form-group row">
                <label for="example-text-input" class="col-12 col-md-4 col-form-label">{{ __('cabinet.profile.form_country') }}</label>
                <div class="col-12 col-md-8">
                    <input class="form-control" name="country" type="text" value="{{ old('country') }}" required>
                </div>
            </div>	
            
            <div class="form-group row">
                <label for="example-text-input" class="col-12 col-md-4 col-form-label">{{ __('cabinet.profile.form_region') }}</label>
                <div class="col-12 col-md-8">
                    <input class="form-control" name="region" type="text" value="{{ old('region') }}" required>
                </div>
            </div>	
            
            <div class="form-group row">
                <label for="example-text-input" class="col-12 col-md-4 col-form-label">{{ __('cabinet.profile.form_city') }}</label>
                <div class="col-12 col-md-8">
                    <input class="form-control" name="city" type="text" value="{{ old('city') }}" required>
                </div>
            </div>	
            
            <div class="form-group row">
                <label for="example-text-input" class="col-12 col-md-4 col-form-label">{{ __('cabinet.profile.form_index') }}</label>
                <div class="col-12 col-md-8">
                    <input class="form-control" name="index" type="text" value="{{ old('index') }}" required>
                </div>
            </div>	
            
            <div class="form-group row">
                <label for="example-text-input" class="col-12 col-md-4 col-form-label">{{ __('cabinet.profile.form_street') }}</label>
                <div class="col-12 col-md-8">
                    <input class="form-control" name="street" type="text" value="{{ old('street') }}" required>
                </div>
            </div>
            
            <div class="form-group row">
                <label for="example-text-input" class="col-12 col-md-4 col-form-label">{{ __('cabinet.profile.form_number_home') }} / {{ __('cabinet.profile.form_number_build') }} / {{ __('cabinet.profile.form_number_room') }}</label>
                <div class="col-12 col-md-8">
                    <div class="input-group">                  
                        <input class="form-control" name="number_home" type="text" value="{{ old('number_home') }}">
                        <input class="form-control mr-3 ml-3" name="number_build" type="text" value="{{ old('number_build') }}">
                        <input class="form-control" name="number_room" type="text" value="{{ old('number_room') }}">
                    </div>
                </div>
            </div>
            
            <hr class="text-black mt-5"></hr>
            
            <h5 class="text-black mb-3">{{ __('cabinet.profile.form_information_contacts') }}</h5>
            
            <div class="form-group row">
                <label for="example-text-input" class="col-12 col-md-4 col-form-label">{{ __('cabinet.profile.form_name') }}</label>
                <div class="col-12 col-md-8">
                    <input class="form-control" name="name" type="text" value="{{ old('name', ($account['name'])?? '') }}" required>
                </div>
            </div>
            
            <div class="form-group row">
                <label for="example-tel-input" class="col-12 col-md-4 col-form-label">{{ __('cabinet.profile.form_last_name') }}</label>
                <div class="col-12 col-md-8">
                    <input class="form-control" name="last_name" type="text" value="{{ old('last_name', ($account['last_name'])?? '') }}" required>
                </div>
            </div>
            
            <div class="form-group row">
                <label for="example-text-input" class="col-12 col-md-4 col-form-label">{{ __('cabinet.profile.form_patronymic') }}</label>
                <div class="col-12 col-md-8">
                    <input class="form-control" name="patronymic" type="text" value="{{ old('patronymic', ($account['patronymic'])?? '') }}">
                </div>
            </div>
            
            <div class="form-group row">
                <label for="example-email-input" class="col-12 col-md-4 col-form-label">{{ __('cabinet.profile.form_position') }}</label>
                <div class="col-12 col-md-8">
                    <input class="form-control" name="position" type="text" value="{{ old('position') }}">
                </div>
            </div>
            
            <div class="form-group row">
                <label for="example-phone-input" class="col-12 col-md-4 col-form-label">{{ __('cabinet.profile.form_number_phone') }}</label>
                <div class="col-12 col-md-8">
                    <input class="form-control" placeholder="" name="number_phone" type="text" value="{{ old('number_phone', ($account['number_phone'])?? '') }}" required>
                </div>
            </div>
            
            <div class="form-group row">
                <label for="example-phone-input" class="col-12 col-md-4 col-form-label">{{ __('cabinet.profile.form_number_phone_mobile') }}</label>
                <div class="col-12 col-md-8">
                    <input class="form-control" placeholder="" name="number_phone_mobile" type="text" value="{{ old('number_phone_mobile', ($account['number_phone_mobile'])?? '') }}" required>
                </div>
            </div>
            
            <hr class="mt-5">
            <div class="form-group row ml-1">
                <button type="submit" class="btn btn-secondary mt-3">{{ __('interface.buttons.create') }}</button>
            </div>
            
            <input type="hidden" name="profile_type" value="{{ $type }}">
        </form>
    </div>
@endsection
--}}

@section('cabinet-content')
<div class="cabinet">
    @section('breadcrumbs', Breadcrumbs::render())
    @yield('breadcrumbs')
	<div class="cabinet__title">
		<div class="cabinet__title__text">{{ Breadcrumbs::generate()->last()->title }}</div>
	</div>
    <form method="POST" accept-charset="UTF-8" id="profile-buyer-create-form" enctype="multipart/form-data" action="{{ route('cabinet.profile.store') }}">
        @csrf
        <div class="cabinet__section cabinet__content">
            <div class="form">
            
                <div class="cabinet__subtitle">{{ __('cabinet.profile.language') }}</div>
                
                <div class="form__row form__row-required @if($errors->has('localisation')) {{ 'form__row-error' }} @endif)"> 
                    <select class="" name="localisation">
                        <option></option>
                        @foreach($localisations as $localisation)
                        <option value="{{ $localisation }}">{{ __('interface.localisation.'.$localisation) }}</option>
                        @endforeach
                    </select>
                    @error('localisation')
                    <div class="form__row__error">{{ $message }}</div>
                    @enderror
                </div>
                
                <div class="cabinet__subtitle">{{ __('cabinet.profile.form_information_about_company') }}</div>
                
                <div class="form__row form__row-required @if($errors->has('company_name')) {{ 'form__row-error' }} @endif)">
                    <input class="form__row__input" name="company_name" type="text" value="{{ old('company_name') }}" id="company-name" required>
                    <label for="company-name" class="form__row__label">{{ __('cabinet.profile.form_company_name') }}</label>
                    @error('company_name')
                    <div class="form__row__error">{{ $message }}</div>
                    @enderror
                </div>
                
                <div class="form__row form__row-required @if($errors->has('country')) {{ 'form__row-error' }} @endif"> 
                    <select class="" name="country">
                        <option></option>
                        @foreach($countries as $country)
                        <option value="{{ $country['id'] }}">{{ $country['text'] }}</option>
                        @endforeach
                    </select>
                    @error('country')
                    <div class="form__row__error">{{ $message }}</div>
                    @enderror
                </div>
                
                <div class="form__row form__row-required @if($errors->has('region')) {{ 'form__row-error' }} @endif">
                    <input class="form__row__input" name="region" type="text" value="{{ old('region') }}" id="region" required>
                    <label for="region" class="form__row__label">{{ __('cabinet.profile.form_region') }}</label>
                    @error('region')
                    <div class="form__row__error">{{ $message }}</div>
                    @enderror
                </div>
                
                <div class="form__row form__row-required @if($errors->has('city')) {{ 'form__row-error' }} @endif">
                    <input class="form__row__input" name="city" type="text" value="{{ old('city') }}" id="city" required>
                    <label for="city" class="form__row__label">{{ __('cabinet.profile.form_city') }}</label>
                    @error('city')
                    <div class="form__row__error">{{ $message }}</div>
                    @enderror
                </div>
                
                <div class="form__row @if($errors->has('index')) {{ 'form__row-error' }} @endif">
                    <input class="form__row__input" name="index" type="text" value="{{ old('index') }}" id="index">
                    <label for="index" class="form__row__label">{{ __('cabinet.profile.form_index') }}</label>
                    @error('index')
                    <div class="form__row__error">{{ $message }}</div>
                    @enderror
                </div>
                
                <div class="form__row @if($errors->has('street')) {{ 'form__row-error' }} @endif">
                    <input class="form__row__input" name="street" type="text" value="{{ old('street') }}" id="street">
                    <label for="street" class="form__row__label">{{ __('cabinet.profile.form_street') }}</label>
                    @error('street')
                    <div class="form__row__error">{{ $message }}</div>
                    @enderror
                </div>
                
                <div class="form__triple__row">
                    <div class="form__row @if($errors->has('number_home')) {{ 'form__row-error' }} @endif"> 
                        <input class="form__row__input" name="number_home" type="text" value="{{ old('number_home') }}" id="number-home">
                        <label for="number-home" class="form__row__label">{{ __('cabinet.profile.form_number_home') }}</label>
                        @error('number_home')
                        <div class="form__row__error">{{ $message }}</div>
                        @enderror
                    </div>
                    <div class="form__row @if($errors->has('number_build')) {{ 'form__row-error' }} @endif"> 
                        <input class="form__row__input" name="number_build" type="text" value="{{ old('number_build') }}" id="number-build">
                        <label for="number-build" class="form__row__label">{{ __('cabinet.profile.form_number_build') }}</label>
                        @error('number_build')
                        <div class="form__row__error">{{ $message }}</div>
                        @enderror
                    </div>
                    <div class="form__row @if($errors->has('number_room')) {{ 'form__row-error' }} @endif">
                        <input class="form__row__input" name="number_room" type="text" value="{{ old('number_room') }}" id="number-room">
                        <label for="number-room" class="form__row__label">{{ __('cabinet.profile.form_number_room') }}</label>
                        @error('number_room')
                        <div class="form__row__error">{{ $message }}</div>
                        @enderror
                    </div>
                </div>
                
            </div>
        </div>
        
        <div class="cabinet__section cabinet__content">
            <div class="cabinet__subtitle">{{ __('cabinet.profile.form_information_contacts') }}</div>
            <div class="form">
                
                <div class="form__row form__row-required @if($errors->has('name')) {{ 'form__row-error' }} @endif">
                    <input class="form__row__input" name="name" type="text" value="{{ old('name', ($user->name)?? '') }}" id="name" required>
                    <label for="name" class="form__row__label">{{ __('cabinet.profile.form_name') }}</label>
                    @error('name')
                    <div class="form__row__error">{{ $message }}</div>
                    @enderror
                </div>
                
                <div class="form__row form__row-required @if($errors->has('last_name')) {{ 'form__row-error' }} @endif">
                    <input class="form__row__input" name="last_name" type="text" value="{{ old('last_name', ($account['last_name'])?? '') }}"  id="last-name"required>
                    <label for="last-name" class="form__row__label">{{ __('cabinet.profile.form_last_name') }}</label>
                    @error('last_name')
                    <div class="form__row__error">{{ $message }}</div>
                    @enderror
                </div>
                
                <div class="form__row form__row-required @if($errors->has('patronymic')) {{ 'form__row-error' }} @endif">
                    <input class="form__row__input" name="patronymic" type="text" value="{{ old('patronymic', ($account['patronymic'])?? '') }}" id="patronymic">
                    <label for="patronymic" class="form__row__label">{{ __('cabinet.profile.form_patronymic') }}</label>
                    @error('patronymic')
                    <div class="form__row__error">{{ $message }}</div>
                    @enderror
                </div>

                <div class="form__row form__row-required @if($errors->has('number_phone')) {{ 'form__row-error' }} @endif">
                    <input class="form__row__input" name="number_phone" type="text" value="{{ old('number_phone', ($account['phone'])?? '') }}" required id="number_phone">
                    <label for="number_phone" class="form__row__label">{{ __('cabinet.profile.form_number_phone') }}</label>
                    @error('number_phone')
                    <div class="form__row__error">{{ $message }}</div>
                    @enderror
                </div>
                
                <div class="form__row @if($errors->has('position')) {{ 'form__row-error' }} @endif">
                    <input class="form__row__input" name="position" type="text" value="{{ old('position') }}" id="position">
                    <label for="position" class="form__row__label">{{ __('cabinet.profile.form_position') }}</label>
                    @error('position')
                    <div class="form__row__error">{{ $message }}</div>
                    @enderror
                </div>
                
                <div class="form__row @if($errors->has('number_phone_mobile')) {{ 'form__row-error' }} @endif"> <!-- form__row-error -->
                    <input class="form__row__input" name="number_phone_mobile" type="text" value="{{ old('number_phone_mobile', ($account['phone'])?? '') }}" id="number_phone_mobile">
                    <label for="number_phone_mobile" class="form__row__label">{{ __('cabinet.profile.form_number_phone_mobile') }}</label>
                    @error('number_phone_mobile')
                    <div class="form__row__error">{{ $message }}</div>
                    @enderror
                </div>
                
            </div>
        </div>
        
        <input type="hidden" name="profile_type" value="{{ $type }}">

        @if(auth()->user()->hasRole('site_manager'))
        <input type="hidden" name="company_user" value="{{ $user->id }}">
        @endif
        
        <div class="cabinet__content cabinet__content__finishActions">
            <button type="submit" class="button button-inline button-primary">{{ __('interface.buttons.create') }}</button>
        </div>
        
    </form>
</div>
@endsection

@section('content-script')
<script>
    document.addEventListener('DOMContentLoaded', function() {

    });
</script>
@endsection