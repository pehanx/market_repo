@extends('cabinet.home')

{{--
@section('cabinet-content')
   
   <div class="col col-md-9">
    @if($profile)
        @section('breadcrumbs', Breadcrumbs::render())
        @yield('breadcrumbs')
        
        @include ('cabinet.profile._nav', ['page' => 'profile'])
        @foreach ($profile->profileProperties as $profileProperty)
        <div class="form-group row">
            <div class="col-6 col-md-4">
                {{ __('cabinet.profile.form_'.$profileProperty->code) }}
            </div>
            <div class="col-6 col-md-8">
                {{ $profileProperty->value }}
            </div>
        </div>
        @endforeach
    @endif

    @if($profile->downloads->first())
        <hr class="mt-3">
        <div class="form-group row">
            <div class="col-6 col-md-4">
                <h5>{{ __('cabinet.profile.attached_files') }}</h5>
            </div>
        </div>
        <div class="form-group row mt-5 mb-5">
        @foreach ($profile->downloads as $download)
            @if($download->type === 'company_video')
            <div class="col-12 col-md-12 d-flex align-items-start mt-5">
                <video width="100" height="100" controls="">
                    <source src="{{ asset('storage/'.$download->path) }}" type="video/mp4">
                </video>
            </div>
            @else
            <div class="col-6 col-md-2 d-flex align-items-start">
                <a href="{{ asset('storage/'.$download->path) }}"><img class="rounded-circle" width="100" height="100" data-id-file="{{$download->id}}" src="{{ asset('storage/'.$download->path) }}"></a>
            </div>
            @endif
        @endforeach 
        </div>
    @endif

    <hr class="mt-5">
    <div class="form-group row ml-1">
        <a class="btn btn-secondary mt-3 mr-5" href="{{ route('cabinet.profile.edit', $profile->id) }}">{{ __('interface.buttons.edit') }}</a>
    </div>
    
    </div>
@endsection
--}}

@section('cabinet-content')
<div class="cabinet">
	@section('breadcrumbs', Breadcrumbs::render())
    @yield('breadcrumbs')
    
	<div class="cabinet__title">
		<div class="cabinet__title__text">{{ __('interface.tabs.common_information') }}</div>
	</div>
    
	<div class="cabinet__section cabinet__content">
        
        <div class="cabinet__subtitle">{{ __('cabinet.profile.language') }}</div>
        <div class="formView"> 
            <div class="formView__row">
                <div class="flag flag-{{ $localisation }}"></div>
                &nbsp;
                {{ __('interface.localisation.'.$localisation) }}
            </div>
        </div>
        
		<div class="cabinet__subtitle">{{ __('cabinet.profile.form_information_about_company') }}</div>
		<div class="formView">
            @foreach ($profile->profileProperties as $propertyCode => $profileProperty)
                @if($propertyCode === 'company_name')
                <div class="formView__row">
                    <div class="formView__row__label">{{ __('cabinet.profile.form_' . $profileProperty->code) }}</div>
                    <div class="formView__row__value">{{ $profileProperty->value }}</div>
                </div>
                @endif
                {{-- Вывести страну из характеристик! --}}
                @if($loop->iteration === 1 && !empty($profile->characteristics['country']['value']))
                <div class="formView__row">
                    <div class="formView__row__label">{{ __('cabinet.profile.form_country') }}</div>
                    <div class="formView__row__value">
                        {{ $profile->characteristics['country']['value'] }}
                    </div>
                </div>
                @endif
                @if($propertyCode === 'region')
                <div class="formView__row">
                    <div class="formView__row__label">{{ __('cabinet.profile.form_' . $profileProperty->code) }}</div>
                    <div class="formView__row__value">{{ $profileProperty->value }}</div>
                </div>
                @endif
                @if($propertyCode === 'city')
                <div class="formView__row">
                    <div class="formView__row__label">{{ __('cabinet.profile.form_' . $profileProperty->code) }}</div>
                    <div class="formView__row__value">{{ $profileProperty->value }}</div>
                </div>
                @endif
            @endforeach
            @isset($profile->profileProperties['street'])
            <div class="formView__row">
                <div class="formView__row__label">{{ __('cabinet.profile.form_address') }}</div>
                <div class="formView__row__value">
                    @isset($profile->profileProperties['street']) 
                    {{ Str::lower(__('cabinet.profile.form_street')) }} &nbsp; {{ $profile->profileProperties['street']->value }}
                    @endisset
                    @isset($profile->profileProperties['number_home']) 
                    &sbquo;&nbsp;&nbsp;{{ Str::lower(__('cabinet.profile.form_number_home')) }} &nbsp; {{ $profile->profileProperties['number_home']->value }}
                    @endisset
                    @isset($profile->profileProperties['number_build']) 
                    &sbquo;&nbsp;&nbsp;{{ Str::lower(__('cabinet.profile.form_number_build')) }} &nbsp; {{ $profile->profileProperties['number_build']->value }}
                    @endisset
                    @isset($profile->profileProperties['number_room']) 
                    &sbquo;&nbsp;&nbsp;{{ Str::lower(__('cabinet.profile.form_number_room')) }} &nbsp; {{ $profile->profileProperties['number_room']->value }}
                    @endisset
                </div>
            </div>
            @endisset
		</div>
	</div>
    
	<div class="cabinet__section cabinet__content">
		<div class="cabinet__subtitle">{{ __('cabinet.profile.form_information_contacts') }}</div>
		<div class="formView">
        @foreach ($profile->profileProperties as $propertyCode => $profileProperty)
            @if($propertyCode === 'name')
            <div class="formView__row">
                <div class="formView__row__label">{{ __('cabinet.profile.form_' . $profileProperty->code) }}</div>
                <div class="formView__row__value">{{ $profileProperty->value }}</div>
            </div>
            @endif
            @if($propertyCode === 'last_name')
            <div class="formView__row">
                <div class="formView__row__label">{{ __('cabinet.profile.form_' . $profileProperty->code) }}</div>
                <div class="formView__row__value">{{ $profileProperty->value }}</div>
            </div>
            @endif
            @if($propertyCode === 'patronymic')
            <div class="formView__row">
                <div class="formView__row__label">{{ __('cabinet.profile.form_' . $profileProperty->code) }}</div>
                <div class="formView__row__value">{{ $profileProperty->value }}</div>
            </div>
            @endif
            @if($propertyCode === 'position')
            <div class="formView__row">
                <div class="formView__row__label">{{ __('cabinet.profile.form_' . $profileProperty->code) }}</div>
                <div class="formView__row__value">{{ $profileProperty->value }}</div>
            </div>
            @endif
            @if($propertyCode === 'number_phone')
            <div class="formView__row">
                <div class="formView__row__label">{{ __('cabinet.profile.form_' . $profileProperty->code) }}</div>
                <div class="formView__row__value">{{ $profileProperty->value }}</div>
            </div>
            @endif
            @if($propertyCode === 'number_phone_mobile')
            <div class="formView__row">
                <div class="formView__row__label">{{ __('cabinet.profile.form_' . $profileProperty->code) }}</div>
                <div class="formView__row__value">{{ $profileProperty->value }}</div>
            </div>
            @endif
        @endforeach
		</div>
    </div>
    
    @if(auth()->user()->hasRole("site_manager") && $profile->status == 'inactive')
        <div class="cabinet__content cabinet__content__finishActions">
            <a href="{{ route('cabinet.company.approve', ['profile' => $profile]) }}" class="button button-inline button-primary-bordered width-fit-content">   {{ __('interface.buttons.approve') }}
            </a>
        </div>
        <br>
    @endif

	<div class="cabinet__content cabinet__content__finishActions">
        <a class="button button-inline button-primary" href="{{ route('cabinet.profile.edit', $profile->id) }}">
            {{ __('interface.buttons.edit') }}
        </a>
	</div>
</div>
@endsection