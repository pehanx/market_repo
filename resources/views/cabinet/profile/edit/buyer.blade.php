@extends('cabinet.home')

{{--
@section('cabinet-content')
    <div class="col col-md-9">
        @section('breadcrumbs', Breadcrumbs::render())
        @yield('breadcrumbs')
        
        <h2>{{ __("cabinet.profile.editing_{$type}_profile") }}</h2>
        
        <div class="form-group row my-3">
            <label for="example-email-input" class="col-12 col-md-4 col-form-label">{{ __('cabinet.profile.language') }}</label>
            <div class="col-12 col-md-8">
                {{ __('interface.localisation.'.$localisation) }}</option>
            </div>
        </div>
            
        <form method="POST" accept-charset="UTF-8" id="profile-buyer-edit-form" class="form-vertical" enctype="multipart/form-data" action="{{ route('cabinet.profile.update', $profile->id) }}">
            @csrf
            @method('PUT')
            
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
  
            <h5 class="text-black mb-4">{{ __('cabinet.profile.form_information_about_company') }}</h5>
            
            <div class="form-group row">
                <label for="example-text-input" class="col-12 col-md-4 col-form-label">{{ __('cabinet.profile.form_company_name') }}</label>
                <div class="col-12 col-md-8">
                    <input class="form-control" name="company_name" type="text" value="{{ old('company_name', ($profile->profileProperties['company_name'])?? '') }}" required>
                </div>
            </div>	
           
            <div class="form-group row">
                <label for="example-text-input" class="col-12 col-md-4 col-form-label">{{ __('cabinet.profile.form_country') }}</label>
                <div class="col-12 col-md-8">
                    <input class="form-control" name="country" type="text" value="{{ old('country', ($profile->profileProperties['country'])?? '') }}" required>
                </div>
            </div>	
            
            <div class="form-group row">
                <label for="example-text-input" class="col-12 col-md-4 col-form-label">{{ __('cabinet.profile.form_region') }}</label>
                <div class="col-12 col-md-8">
                    <input class="form-control" name="region" type="text" value="{{ old('region', ($profile->profileProperties['region'])?? '') }}" required>
                </div>
            </div>	
            
            <div class="form-group row">
                <label for="example-text-input" class="col-12 col-md-4 col-form-label">{{ __('cabinet.profile.form_city') }}</label>
                <div class="col-12 col-md-8">
                    <input class="form-control" name="city" type="text" value="{{ old('city', ($profile->profileProperties['city'])?? '') }}" required>
                </div>
            </div>	
            
            <div class="form-group row">
                <label for="example-text-input" class="col-12 col-md-4 col-form-label">{{ __('cabinet.profile.form_index') }}</label>
                <div class="col-12 col-md-8">
                    <input class="form-control" name="index" type="text" value="{{ old('index', ($profile->profileProperties['index'])?? '') }}" required>
                </div>
            </div>	
            
            <div class="form-group row">
                <label for="example-text-input" class="col-12 col-md-4 col-form-label">{{ __('cabinet.profile.form_street') }}</label>
                <div class="col-12 col-md-8">
                    <input class="form-control" name="street" type="text" value="{{ old('street', ($profile->profileProperties['street'])?? '') }}">
                </div>
            </div>
            
            <div class="form-group row">
                <label for="example-text-input" class="col-12 col-md-4 col-form-label">{{ __('cabinet.profile.form_number_home') }} / {{ __('cabinet.profile.form_number_build') }} / {{ __('cabinet.profile.form_number_room') }}</label>
                <div class="col-12 col-md-8">
                    <div class="input-group">                  
                        <input class="form-control" name="number_home" type="text" value="{{ old('number_home', ($profile->profileProperties['number_home'])?? '') }}">
                        <input class="form-control mr-3 ml-3" name="number_build" type="text" value="{{ old('number_build', ($profile->profileProperties['number_build'])?? '') }}">
                        <input class="form-control" name="number_room" type="text" value="{{ old('number_room', ($profile->profileProperties['number_room'])?? '') }}">
                    </div>
                </div>
            </div>
            
            <hr class="text-black mt-5"></hr>
            
            <h5 class="text-black mb-3">{{ __('cabinet.profile.form_information_contacts') }}</h5>
            <div class="form-group row">
                <label for="example-text-input" class="col-12 col-md-4 col-form-label">{{ __('cabinet.profile.form_name') }}</label>
                <div class="col-12 col-md-8">
                    <input class="form-control" name="name" type="text" value="{{ old('name', ($profile->profileProperties['name'])?? '') }}" required>
                </div>
            </div>
            
            <div class="form-group row">
                <label for="example-tel-input" class="col-12 col-md-4 col-form-label">{{ __('cabinet.profile.form_last_name') }}</label>
                <div class="col-12 col-md-8">
                    <input class="form-control" name="last_name" type="text" value="{{ old('last_name', ($profile->profileProperties['last_name'])?? '') }}" required>
                </div>
            </div>
            
            <div class="form-group row">
                <label for="example-text-input" class="col-12 col-md-4 col-form-label">{{ __('cabinet.profile.form_patronymic') }}</label>
                <div class="col-12 col-md-8">
                    <input class="form-control" name="patronymic" type="text" value="{{ old('patronymic', ($profile->profileProperties['patronymic'])?? '') }}">
                </div>
            </div>
            
            <div class="form-group row">
                <label for="example-email-input" class="col-12 col-md-4 col-form-label">{{ __('cabinet.profile.form_position') }}</label>
                <div class="col-12 col-md-8">
                    <input class="form-control" name="position" type="text" value="{{ old('position', ($profile->profileProperties['position'])?? '') }}">
                </div>
            </div>
            
            <div class="form-group row">
                <label for="example-phone-input" class="col-12 col-md-4 col-form-label">{{ __('cabinet.profile.form_number_phone') }}</label>
                <div class="col-12 col-md-8">
                    <input class="form-control" placeholder="" name="number_phone" type="text" value="{{ old('number_phone', ($profile->profileProperties['number_phone'])?? '') }}" required>
                </div>
            </div>
            
            <div class="form-group row">
                <label for="example-phone-input" class="col-12 col-md-4 col-form-label">{{ __('cabinet.profile.form_number_phone_mobile') }}</label>
                <div class="col-12 col-md-8">
                    <input class="form-control" placeholder="" name="number_phone_mobile" type="text" value="{{ old('number_phone_mobile', ($profile->profileProperties['number_phone_mobile'])?? '') }}">
                </div>
            </div>
            
            <hr class="mt-5">
            <div class="form-group row ml-1">
                <button type="submit" class="btn btn-secondary mt-3">{{ __('interface.buttons.save_changes') }}</button>
            </div>
            
            <input type="hidden" name="profile_type" value="{{ $type }}">
            <input type="hidden" name="localisation" value="{{ $localisation }}">
        </form>
    </div>
@endsection
--}}

@section('cabinet-content')
<div class="cabinet">
    @section('breadcrumbs', Breadcrumbs::render())
    @yield('breadcrumbs')
	<div class="cabinet__title">
		<div class="cabinet__title__text">{{ Breadcrumbs::generate()->last()->title }}</div>
	</div>
    <form method="POST" accept-charset="UTF-8" id="profile-buyer-edit-form" enctype="multipart/form-data" action="{{ route('cabinet.profile.update', $profile->id) }}">
        @csrf
        @method('PUT')
        <div class="cabinet__section cabinet__content">
            <div class="form">

                <div class="cabinet__subtitle">{{ __('cabinet.profile.language') }}</div>
                
                <div class="form__row"> 
                    <div class="flag flag-{{ $localisation }}"></div>
                    &nbsp;
                    {{ __('interface.localisation.'.$localisation) }}
                </div>

                <div class="cabinet__subtitle">{{ __('cabinet.profile.form_information_about_company') }}</div>
                
                <div class="form__row form__row-required @if($errors->has('company_name')) {{ 'form__row-error' }} @endif)">
                    <input class="form__row__input" name="company_name" type="text" value="{{ old('company_name', ($profile->profileProperties['company_name'])?? '') }}" id="company-name" required>
                    <label for="company-name" class="form__row__label">{{ __('cabinet.profile.form_company_name') }}</label>
                    @error('company_name')
                    <div class="form__row__error">{{ $message }}</div>
                    @enderror
                </div>
                
                {{-- Теперь поле храниться в characteristics, а не в profileProperties !!! --}}
                <div class="form__row form__row-required @if($errors->has('country')) {{ 'form__row-error' }} @endif"> 
                    <select class="" name="country">
                        <option></option>
                        @foreach($countries as $country)
                        <option value="{{ $country['id'] }}"
                        {{  (($profile->characteristics['country']['type']?? '') && $profile->characteristics['country']['type'] ==  $country['id'])? 'selected' : '' }}>
                            {{ $country['text'] }}
                        </option>
                        @endforeach
                    </select>
                    @error('country')
                    <div class="form__row__error">{{ $message }}</div>
                    @enderror
                </div>
                
                <div class="form__row form__row-required @if($errors->has('region')) {{ 'form__row-error' }} @endif">
                    <input class="form__row__input" name="region" type="text" value="{{ old('region', ($profile->profileProperties['region'])?? '') }}" id="region" required>
                    <label for="region" class="form__row__label">{{ __('cabinet.profile.form_region') }}</label>
                    @error('region')
                    <div class="form__row__error">{{ $message }}</div>
                    @enderror
                </div>
                
                <div class="form__row form__row-required @if($errors->has('city')) {{ 'form__row-error' }} @endif">
                    <input class="form__row__input" name="city" type="text" value="{{ old('city', ($profile->profileProperties['city'])?? '') }}" id="city" required>
                    <label for="city" class="form__row__label">{{ __('cabinet.profile.form_city') }}</label>
                    @error('city')
                    <div class="form__row__error">{{ $message }}</div>
                    @enderror
                </div>
                
                <div class="form__row @if($errors->has('index')) {{ 'form__row-error' }} @endif">
                    <input class="form__row__input" name="index" type="text" value="{{ old('index', ($profile->profileProperties['index'])?? '') }}" id="index">
                    <label for="index" class="form__row__label">{{ __('cabinet.profile.form_index') }}</label>
                    @error('index')
                    <div class="form__row__error">{{ $message }}</div>
                    @enderror
                </div>
                
                <div class="form__row @if($errors->has('street')) {{ 'form__row-error' }} @endif">
                    <input class="form__row__input" name="street" type="text" value="{{ old('street', ($profile->profileProperties['street'])?? '') }}" id="street">
                    <label for="street" class="form__row__label">{{ __('cabinet.profile.form_street') }}</label>
                    @error('street')
                    <div class="form__row__error">{{ $message }}</div>
                    @enderror
                </div>
                
                <div class="form__triple__row">
                    <div class="form__row @if($errors->has('number_home')) {{ 'form__row-error' }} @endif"> 
                        <input class="form__row__input" name="number_home" type="text" value="{{ old('number_home', ($profile->profileProperties['number_home'])?? '') }}" id="number-home">
                        <label for="number-home" class="form__row__label">{{ __('cabinet.profile.form_number_home') }}</label>
                        @error('number_home')
                        <div class="form__row__error">{{ $message }}</div>
                        @enderror
                    </div>
                    <div class="form__row @if($errors->has('number_build')) {{ 'form__row-error' }} @endif"> 
                        <input class="form__row__input" name="number_build" type="text" value="{{ old('number_build', ($profile->profileProperties['number_build'])?? '') }}" id="number-build">
                        <label for="number-build" class="form__row__label">{{ __('cabinet.profile.form_number_build') }}</label>
                        @error('number_build')
                        <div class="form__row__error">{{ $message }}</div>
                        @enderror
                    </div>
                    <div class="form__row @if($errors->has('number_room')) {{ 'form__row-error' }} @endif">
                        <input class="form__row__input" name="number_room" type="text" value="{{ old('number_room', ($profile->profileProperties['number_room'])?? '') }}" id="number-room">
                        <label for="number-room" class="form__row__label">{{ __('cabinet.profile.form_number_room') }}</label>
                        @error('number_room')
                        <div class="form__row__error">{{ $message }}</div>
                        @enderror
                    </div>
                </div>
                
            </div>
        </div>
        
        <div class="cabinet__section cabinet__content">
            <div class="cabinet__subtitle">{{ __('cabinet.profile.form_information_contacts') }}</div>
            <div class="form">
                
                <div class="form__row form__row-required @if($errors->has('name')) {{ 'form__row-error' }} @endif">
                    <input class="form__row__input" name="name" type="text" value="{{ old('name', ($profile->profileProperties['name'])?? '') }}" id="name" required>
                    <label for="name" class="form__row__label">{{ __('cabinet.profile.form_name') }}</label>
                    @error('name')
                    <div class="form__row__error">{{ $message }}</div>
                    @enderror
                </div>
                
                <div class="form__row form__row-required @if($errors->has('last_name')) {{ 'form__row-error' }} @endif">
                    <input class="form__row__input" name="last_name" type="text" value="{{ old('last_name', ($profile->profileProperties['last_name'])?? '') }}"  id="last-name" required>
                    <label for="last-name" class="form__row__label">{{ __('cabinet.profile.form_last_name') }}</label>
                    @error('last_name')
                    <div class="form__row__error">{{ $message }}</div>
                    @enderror
                </div>
                
                <div class="form__row @if($errors->has('patronymic')) {{ 'form__row-error' }} @endif">
                    <input class="form__row__input" name="patronymic" type="text" value="{{ old('patronymic', ($profile->profileProperties['patronymic'])?? '') }}" id="patronymic">
                    <label for="patronymic" class="form__row__label">{{ __('cabinet.profile.form_patronymic') }}</label>
                    @error('patronymic')
                    <div class="form__row__error">{{ $message }}</div>
                    @enderror
                </div>

                <div class="form__row form__row-required @if($errors->has('number_phone')) {{ 'form__row-error' }} @endif">
                    <input class="form__row__input" name="number_phone" type="text" value="{{ old('number_phone', ($profile->profileProperties['number_phone'])?? '') }}" required id="number_phone">
                    <label for="number_phone" class="form__row__label">{{ __('cabinet.profile.form_number_phone') }}</label>
                    @error('number_phone')
                    <div class="form__row__error">{{ $message }}</div>
                    @enderror
                </div>
                
                <div class="form__row @if($errors->has('position')) {{ 'form__row-error' }} @endif">
                    <input class="form__row__input" name="position" type="text" value="{{ old('position', ($profile->profileProperties['position'])?? '') }}" id="position">
                    <label for="position" class="form__row__label">{{ __('cabinet.profile.form_position') }}</label>
                    @error('position')
                    <div class="form__row__error">{{ $message }}</div>
                    @enderror
                </div>
                
                <div class="form__row @if($errors->has('number_phone_mobile')) {{ 'form__row-error' }} @endif"> <!-- form__row-error -->
                    <input class="form__row__input" name="number_phone_mobile" type="text" value="{{ old('number_phone_mobile', ($profile->profileProperties['number_phone_mobile'])?? '') }}" id="number_phone_mobile">
                    <label for="number_phone_mobile" class="form__row__label">{{ __('cabinet.profile.form_number_phone_mobile') }}</label>
                    @error('number_phone_mobile')
                    <div class="form__row__error">{{ $message }}</div>
                    @enderror
                </div>
                
            </div>
        </div>

        <div class="cabinet__content cabinet__content__finishActions">
            <input type="hidden" name="profile_type" value="{{ $type }}">
            <input type="hidden" name="localisation" value="{{ $localisation }}">
            <button class="button button-primary button-inline submit-form" type="submit">{{ __('interface.buttons.save_changes') }}</button>
        </div>
        
    </form>
</div>
@endsection

@section('content-script')
<script>
    document.addEventListener('DOMContentLoaded', function() {

    });
</script>
@endsection