@extends('cabinet.home')

@section('sidebar')
    @include ('cabinet.sidebar', ['page' => $type])
@endsection

{{--
@section('cabinet-content')
    <div class="col-12 col-md-9">
        @section('breadcrumbs', Breadcrumbs::render())
        @yield('breadcrumbs')

        @if($profiles->first())
        <table class="table table-responsive border-0 table-sm table-bordered table-hover">
            <thead>
                <tr>
                    <th class="">
                        {{ __('cabinet.profile.name') }}
                    </th>  
                    <th class="">
                        {{ __('cabinet.profile.language') }}
                    </th> 
                    <th class="">
                        {{ __('cabinet.profile.status') }}
                    </th> 
                    <th class="th-actions">
                        {{ __('cabinet.profile.actions') }}
                    </th>
                </tr>
            </thead> 
            
            <tbody>
                @foreach ($profiles as $profile)
                <tr>
                    @if ($nameCompany = $profile->profileProperties->where('code', 'company_name')->first())
                    <td data-label="{{ __('cabinet.profile.profile') }}"><a href="{{ route('cabinet.profile.show', $profile->id) }}">{{ $nameCompany->value }}</a></td>
                    @endif
                    
                    <td data-label="{{ __('cabinet.profile.language') }}"><a href="{{ route('cabinet.profile.show', $profile->id) }}">{{ __('interface.localisation.'.$profile->profileProperties->first()->localisations->code) }}</a></td>
                    
                    @if ($profile->status === 'active')
                    <td data-label="{{ __('cabinet.profile.status') }}"><a href="{{ route('cabinet.profile.show', $profile->id) }}"><span class="badge badge-pill badge-primary mb-1">{{ __('cabinet.profile.approved') }}</span></a></td>
                    @elseif($profile->status === 'inactive')
                    <td data-label="{{ __('cabinet.profile.status') }}"><a href="{{ route('cabinet.profile.show', $profile->id) }}"><span class="badge badge-pill badge-danger mb-1">{{ __('cabinet.profile.not_approved') }}</span></a></td>
                    @elseif($profile->status === 'wait')
                    <td data-label="{{ __('cabinet.profile.status') }}"><a href="{{ route('cabinet.profile.show', $profile->id) }}"><span class="badge badge-pill badge-warning mb-1">{{ __('cabinet.profile.wait_approve') }}</span></a></td>
                    @endif

                    <td data-label="{{ __('cabinet.profile.actions') }}">
                        <p class="action m-0">
                            <a href="{{ route('cabinet.profile.edit', $profile->id) }}" class="mr-2">
                                <i class="fa fa-pencil" title="{{ __('interface.buttons.edit') }}"></i>
                            </a>
                            
                            @if($profile->status === 'inactive')
                            <a href="{{ route('cabinet.profile.approve', $profile->id) }}" class="mr-2">
                                <i class="fa fa-check-circle-o" title="{{ __('interface.buttons.approve') }}"> </i>
                            </a>
                            @endif
                            
                            @if($profile->status === 'active')
                            <a href="{{ route('cabinet.profile.select', $profile->id) }}" class="mr-2">
                                <i class="fa fa-check" aria-hidden="true" title="{{ __('interface.buttons.select') }}"></i>
                            </a>
                            @endif
                            
                            <button type="button" class="delete-profile-btn btn-sm px-0" data-text="{{ __('modals.your_sure_destroy_profile').' '.$nameCompany->value.' ?' }}" data-route="{{ route('cabinet.profile.destroy', $profile->id) }}" >
                                <i class="fa fa-trash" title="{{ __('interface.buttons.delete') }}"></i>
                            </button>
                        </p>
                    </td>
                    
                </tr>
                @endforeach
            </tbody>
        </table>
        
            @if($hasPossibleLocales)
            <a href="{{ route('cabinet.profile.create', ['type' => $type]) }}" class="btn btn-secondary">{{ __('cabinet.profile.create_profile_another_language') }}</a>
            @endif
            
        @else
        <div class="d-flex flex-column align-items-start">
            <div class="mb-5">{{ __('interface.messages.create_profile_seller') }}<a href="/help/for-sellers/company-profile/how-create-company-profile
" target="_blank"><i class="fa fa-info-circle ml-2" aria-hidden="true"></i></a></div>
            <a href="{{ route('cabinet.profile.create', ['type' => $type]) }}" class="btn btn-secondary">{{ __('cabinet.profile.create_seller') }}</a>
        </div>
        @endif
    </div>
    @include('modals.destroy')
@endsection
--}}

@section('cabinet-content')
    <div class="cabinet">
        @section('breadcrumbs', Breadcrumbs::render())
        @yield('breadcrumbs')
        <div class="cabinet__title">{{ Breadcrumbs::generate()->last()->title }}</div>
        @if($profiles->first())
        <div class="cabinet__content cabinet__content__profiles">

            <div class="cabinet__content__profiles__elements">
                @foreach ($profiles as $profile)
                <div class="cabinet__content__profiles__element @if(session()->has('active_profile') && session()->get('active_profile')->id === $profile->id) active @endif">
                    <a href="{{ route('cabinet.profile.show', $profile->id) }}">
                        <div 
                            class="cabinet__content__profiles__image" 
                            @if($logo = $profile->downloads->where('type', 'company_logo_image')->first())
                            style="background-image: url('{{ asset('storage/'.$logo->path) }}');" 
                            @endif
                        >
                        </div>
                    </a>
                    <div class="cabinet__content__profiles__content">
                        <a href="{{ route('cabinet.profile.show', $profile->id) }}">
                            <div class="cabinet__content__profiles__content__title">
                            {{ $profile->profileProperties->where('code', 'company_name')->first()->value }}
                            </div>
                            <div class="cabinet__content__profiles__content__parameter">
                                <span>{{ __('cabinet.profile.language') }}:</span>
                                {{ __('interface.localisation.' . $profile->profileProperties->first()->localisations->code) }}
                            </div>
                            <div class="cabinet__content__profiles__content__parameter">
                                <span>{{ __('cabinet.profile.status') }}:</span> 
                                @if ($profile->status === 'active')
                                    {{ __('cabinet.profile.approved') }}
                                @elseif($profile->status === 'inactive')
                                    {{ __('cabinet.profile.not_approved') }}
                                @elseif($profile->status === 'wait')
                                    {{ __('cabinet.profile.wait_approve') }}
                                @endif
                            </div>
                         </a>
                    </div>
                    <div class="cabinet__content__profiles__actions">
                        @if (
                            ($profile->status === 'active' && !session()->has('active_profile')) 
                                OR 
                            ($profile->status === 'active' && session()->has('active_profile') && session()->get('active_profile')->id !== $profile->id)
                        )
                        <a href="{{ route('cabinet.profile.select', $profile->id) }}" class="button button-primary-bordered">
                            {{ __('interface.buttons.select') }}
                        </a>
                        @elseif($profile->status === 'inactive')
                        <a href="{{ route('cabinet.profile.approve', $profile->id) }}" class="button button-primary-bordered">
                            {{ __('interface.buttons.approve') }}
                        </a>
                        @endif
                        <a href="{{ route('cabinet.profile.edit', $profile->id) }}" class="button button-primary-bordered">
                            {{ __('interface.buttons.edit') }}
                        </a>
                        <button 
                            type="button" 
                            class="button button-primary-link button-primary-link-line delete-profile-btn" 
                            data-text="{{ __('modals.your_sure_destroy_profile', ['profile' => $profile->profileProperties->where('code', 'company_name')->first()->value]) }}" 
                            data-route="{{ route('cabinet.profile.destroy', $profile->id) }}" 
                        >
                            {{ __('interface.buttons.delete') }}
                        </button>
                    </div>
                </div>
                @endforeach
            </div>
            @if($hasPossibleLocales)
                <a href="{{ route('cabinet.profile.create', ['type' => $type]) }}" class="button button-inline button-primary-bordered width-fit-content">
                    {{ __('cabinet.profile.create_profile_another_language') }}
                </a>
            @endif
        </div>
        @else
        <div class="cabinet__content cabinet__content__profiles">
            <div class="cabinet__content__profiles-empty">
                <span>{{ __('interface.messages.create_profile_seller') }}</span>
                <a href="/help/for-sellers/company-profile/how-create-company-profile" target="_blank">
                    <i class="fa fa-info-circle" aria-hidden="true"></i>
                </a>
            </div>
            
            <a href="{{ route('cabinet.profile.create', ['type' => $type]) }}" class="button button-inline button-primary-bordered width-fit-content">  {{ __('cabinet.profile.create_seller') }}
            </a>
        </div>
        @endif
    </div>
    @include('modals.destroy')
@endsection

@section('content-script')
<script>
    document.addEventListener('DOMContentLoaded', function() {
        cabinet.init('.delete-profile-btn', 'click', 'deleteProfile');
    });
</script>
@endsection