@extends('cabinet.home')

{{--
@section('cabinet-content')
    <div class="col col-md-9">
        <div class="card">
            <div class="card-header"> 
                {{ __('interface.messages.approve_profile') }}
            </div>
            <div class="card-body"> 
                <form method="POST" accept-charset="UTF-8" id="profile-approve-form" class="form-vertical" enctype="multipart/form-data" action="{{ route('cabinet.profile.approval', $profile->id) }}">
                    
                    
                    @csrf
                    
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    
                    <div class="form-group row mb-5">
                        <div class="col-sm-12">
                        {{ __('interface.messages.need_documents') }}
                        </div>
                        
                        <div class="col-sm-12 mt-5">
                            <label class="mr-5">{{ __('cabinet.profile.form_documents').':' }}</label>
                            <input type="file" name="documents[]" accept="image/*"  multiple value="{{ old('documents[]') }}">
                        </div>
                        
                    </div>
                        
                    <hr class="mt-5">
                    <div class="form-group row ml-1">
                        <button type="submit" class="btn btn-secondary mt-3">{{ __('interface.buttons.approve') }}</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
--}}

@section('cabinet-content')
<div class="cabinet">
	@section('breadcrumbs', Breadcrumbs::render())
    @yield('breadcrumbs')
    <div class="cabinet__title">
		<div class="cabinet__title__text">{{ __('interface.messages.approve_profile') }}</div>
	</div>
    
    <form method="POST" accept-charset="UTF-8" id="profile-approve-form" class="form-vertical" enctype="multipart/form-data" action="{{ route('cabinet.profile.approval', $profile->id) }}">
        @csrf
        <div class="cabinet__content cabinet__content__managers">
            <div class="cabinet__content__managers__text">{{ __('interface.messages.need_documents') }}</div>
            <div class="form cabinet__content__managers__uploadfile">
                <div class="form__row @if($errors->has('documents.*')) {{ 'form__row-error' }} @endif">
                    <div class="form__row__uploadFile">
                        <input id="documents" class="form__row__uploadFile__input" multiple type="file" name="documents[]">
                        <label for="documents" class="form__row__uploadFile__label">
                            <div class="form__row__uploadFile__button button button-primary-bordered button-inline">
                                {{ __('interface.buttons.choose_files') }}
                            </div>
                            <div class="form__row__uploadFile__text"></div>
                        </label>
                    </div>

                    @error('documents.*')
                    <div class="form__row__error">{{ $message }}</div>
                    @enderror
                </div>

            </div>
            <div class="cabinet__content__managers__actionButtons cabinet__content__managers__actionButtons-line">
                <button type="submit" class="button button-primary button-inline">{{ __('interface.buttons.approve') }}</button>
            </div>
        </div>
    </form>
</div>
@endsection