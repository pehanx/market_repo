@extends('cabinet.home')

@section('cabinet-content')
    <div class="col col-md-9">
        <div class="card">
            <div class="card-body">
                <form method="POST" accept-charset="UTF-8" id="profile-create-form" class="form-vertical mb-5" enctype="multipart/form-data" action="{{ route('cabinet.profile.store') }}">
                    @csrf
                    
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif

                    <h2 class="text-black mb-3">{{ __('cabinet.manager.form_tell_about') }}</h2>
                    <div class="form-group row">
                        <label for="example-text-input" class="col-4 col-sm-3 col-form-label">{{ __('cabinet.manager.form_name') }}</label>
                        <div class="col-12 col-sm-9">
                            <input class="form-control" name="name" type="text" value="{{ old('name', $user->name)?? Auth::user()->name }}" required>
                        </div>
                    </div>
                    
                    <div class="form-group row">
                        <label for="example-tel-input" class="col-4 col-sm-3 col-form-label">{{ __('cabinet.manager.form_last_name') }}</label>
                        <div class="col-12 col-sm-9">
                            <input class="form-control" name="last_name" type="text" value="{{ old('last_name', $user->last_name) }}" required>
                        </div>
                    </div>
                    
                    <div class="form-group row">
                        <label for="example-text-input" class="col-4 col-sm-3 col-form-label">{{ __('cabinet.manager.form_patronymic') }}</label>
                        <div class="col-12 col-sm-9">
                            <input class="form-control" name="patronymic" type="text" value="{{ old('patronymic', $user->patronymic) }}">
                        </div>
                    </div>
                    
                    <div class="form-group row">
                        <label for="example-tel-input" class="col-4 col-sm-3 col-form-label">{{ __('cabinet.manager.form_birthday') }}</label>
                        <div class="col-12 col-sm-9">
                            <input class="form-control" name="birthday" type="text" id="datepicker" value="{{ old('birthday', $user->birthday) }}">
                        </div>
                    </div>
                    
                    <div class="form-group row">
                        <label for="example-radio-input" class="col-4 col-sm-3 col-form-label">{{ __('cabinet.manager.form_gender') }}</label>
                        <div class="col-12 col-sm-9">
                         
                            <label class="form-check-label ml-4">
                                <input type="radio" class="form-check-input" name="gender" id="gender1" value="man" checked>
                                {{ __('cabinet.manager.form_gender_man') }}
                            </label>
                           
                          
                            <label class="form-check-label offset-2">
                                <input type="radio" class="form-check-input" name="gender" id="gender2" value="women">
                                {{ __('cabinet.manager.form_gender_woman') }}
                            </label>
                            
                        </div>
                    </div>
                    
                    <div class="form-group row">
                        <label for="example-phone-input" class="col-3 col-md-3 col-form-label">{{ __('cabinet.manager.form_phone') }}</label>
                        <div class="col-12 col-sm-9">
                            <div class="input-group">
                                <input class="form-control  border-right-0" placeholder="" name="phone" type="text" value="{{ old('phone', $user->phone) }}" required>
                            </div>
                        </div>
                    </div>
                    
                    <div class="form-group row mb-5">
                        <label for="example-email-input" class="col-4 col-sm-3 col-form-label">{{ __('messages.email') }}</label>
                        <div class="col-12 col-sm-9">
                            <input class="form-control" name="email" type="text" value="{{ old('email', $user->email)?? Auth::user()->email }}" required>
                        </div>
                    </div>
                    
                    <!--Сделать проверку. Аватар загружает пользователь если он ещё member-->
                    <div class="form-group row">
                        <label class="col-sm-3 col-form-label">{{ __('cabinet.manager.form_image') }}</label>
                        <div class="col-sm-8">
                            <img class="rounded" src="https://g2r.biz/img/logo.png">
                            <br>
                            <br>
                            <input type="file" name="image" accept="image/*" value="{{ old('image', $user->image) }}">
                        </div>
                    </div>

                    <hr class="text-black mt-5"></hr>
                    <h2 class="text-black mt-5 mb-3">{{ __('cabinet.manager.form_company_about') }}</h2>
                    
                    <div class="form-group row">
                        <label for="example-radio-input" class="col-4 col-sm-3 col-form-label">{{ __('cabinet.manager.form_you') }}</label>
                        <div class="col-12 col-sm-9">
                         
                            <label class="form-check-label ml-4">
                                <input type="radio" class="form-check-input" name="profile_type" id="profile_type1" value="buyer" checked>
                                {{ __('cabinet.manager.form_buyer') }}
                            </label>
                           
                          
                            <label class="form-check-label offset-2">
                                <input type="radio" class="form-check-input" name="profile_type" id="profile_type2" value="seller">
                                {{ __('cabinet.manager.form_seller') }}
                            </label>
                            
                        </div>
                    </div>
                    
                    <div class="form-group row">
                        <label for="example-text-input" class="col-3 col-md-3 col-form-label">{{ __('cabinet.manager.form_company_name') }}</label>
                        <div class="col-12 col-sm-9">
                            <input class="form-control" name="company_name" type="text" value="{{ old('company_name', $user->company_name) }}" required>
                        </div>
                    </div>	
                    
                    <div class="form-group row">
                        <label for="example-text-input" class="col-3 col-md-3 col-form-label">{{ __('cabinet.manager.form_country') }}</label>
                        <div class="col-12 col-sm-9">
                            <input class="form-control" name="country" type="text" value="{{ old('country', $user->country) }}" required>
                        </div>
                    </div>	
                    
                    <div class="form-group row">
                        <label for="example-text-input" class="col-3 col-md-3 col-form-label">{{ __('cabinet.manager.form_city') }}</label>
                        <div class="col-12 col-sm-9">
                            <input class="form-control" name="city" type="text" value="{{ old('city', $user->city) }}" required>
                        </div>
                    </div>	
                    
                    <div class="form-group row">
                        <label for="example-text-input" class="col-3 col-md-3 col-form-label">{{ __('cabinet.manager.form_company_adress_judicial') }}</label>
                        <div class="col-12 col-sm-9">
                            <input class="form-control" name="company_adress_judicial" type="text" value="{{ old('company_adress_judicial', $user->company_adress_judicial) }}" required>
                        </div>
                    </div>			
                    
                    <div class="form-group row">
                        <label for="example-email-input" class="col-3 col-md-3 col-form-label">{{ __('cabinet.manager.form_company_adress_fact') }}</label>
                        <div class="col-12 col-sm-9">
                            <input class="form-control" name="company_adress_fact" type="text" value="{{ old('company_adress_fact', $user->company_adress_fact) }}" required>
                        </div>
                    </div>
                    
                    <div class="form-group row">
                        <label for="example-email-input" class="col-3 col-md-3 col-form-label">{{ __('cabinet.manager.form_inn') }}</label>
                        <div class="col-12 col-sm-9">
                            <input class="form-control" name="inn" type="text" value="{{ old('inn', $user->inn) }}">
                        </div>
                    </div>
                    
                    <div class="form-group row">
                        <label for="example-email-input" class="col-3 col-md-3 col-form-label">{{ __('cabinet.manager.form_position') }}</label>
                        <div class="col-12 col-sm-9">
                            <input class="form-control" name="position" type="text" value="{{ old('position', $user->position) }}">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-sm-3 col-form-label">&nbsp;</label>
                        <div class="col-sm-8">
                            <button type="submit" class="btn btn-warning">{{ __('interface.buttons.send') }}</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('profile-script')
<script>
    class Profile 
    {
        constructor () 
        {
            
        }
    }
</script>
@endsection