@extends('cabinet.home')

@section('cabinet-content')
<div class="col col-md-9">
    @section('breadcrumbs', Breadcrumbs::render())
    @yield('breadcrumbs')
    
    @include ('cabinet.profile._nav', ['page' => 'managers'])
    <div class="form-group row">
        <div class="col-6 col-md-4">
            <h6>{{ __('cabinet.manager.form_company_name') }}</h6>
        </div>
        <div class="col-6 col-md-8">
            {{ $profile->profileProperties->where('code', 'company_name')->pluck('value')->shift() ?? '' }}
        </div>
    </div>
    <table class="table table-responsive- table-striped- table-sm table-bordered table-hover border-0">
        <thead>
            <tr>
                <th class="">
                    {{ __('cabinet.manager.manager') }}
                </th>
                <th class="">
                    {{ __('cabinet.manager.mail') }}
                </th> 
                <th class="th-actions">
                    {{ __('cabinet.manager.actions') }}
                </th>
            </tr>
        </thead> 
        
        <tbody>
            @if($managers->first())
                @foreach ($managers as $manager)
                <tr>
                    <td data-label="{{ __('cabinet.manager.name') }}">{{ $manager->name }}</td>
                    <td data-label="{{ __('cabinet.manager.mail') }}">{{ $manager->email }}</td>
                    <td data-label="{{ __('cabinet.manager.actions') }}">
                        <a href="{{ route('cabinet.manager.edit', ['profile' => $profile->id, 'manager' => $manager->profiles->first()->id ]) }}" class="mr-2">
                            <i class="fa fa-pencil" title="{{ __('interface.buttons.change') }}"></i>
                        </a>
                        <button type="button" class="delete-profile-btn btn-sm px-0" data-text="{{ __('modals.your_sure_destroy_manager', ['manager' => $manager->name]) }}" data-route="{{ route('cabinet.manager.destroy', ['profile' => $profile->id, 'manager' => $manager->profiles->first()->id]) }}" >
                            <i class="fa fa-trash" title="{{ __('interface.buttons.delete') }}"></i>
                        </button>
                    </td>
                </tr>
                @endforeach
            @else
            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            @endif
        </tbody>
    </table>
    
    <div class="form-group row ml-1">
        @if($profile->status === 'active')
        <a class="btn btn-secondary mt-3" href="{{ route('cabinet.manager.create', $profile->id) }}">{{ __('interface.buttons.add_manager') }}</a>
        @endif
    </div>
    @include('modals.destroy')
</div>
@endsection

@section('content-script')
<script>
    document.addEventListener('DOMContentLoaded', function() {
        cabinet.init('.delete-profile-btn', 'click', 'deleteProfile');
    });
</script>
@endsection