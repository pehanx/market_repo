<div class="form-group row">
    <div class="col-auto col-md-4">
        <a class="{{ $page === 'edit' ? ' active' : '' }}" href="{{ route('cabinet.manager.edit', ['manager' => $manager->id, 'profile' => $profile->id]) }}"><h6>{{ __('interface.tabs.personal_data') }}</h6></a>
    </div>
    <div class="col-auto col-md-4">
        <a class="{{ $page === 'edit_entrance' ? ' active' : '' }}" href="{{ route('cabinet.manager.edit_entrance', ['manager' => $manager->id, 'profile' => $profile->id]) }}"><h6>{{ __('interface.tabs.account') }}</h6></a>
    </div>
</div>
<hr class="mt-2">

    
   
