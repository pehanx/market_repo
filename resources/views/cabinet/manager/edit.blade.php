@extends('cabinet.home')

@section('cabinet-content')

<div class="col col-md-9">
        @section('breadcrumbs', Breadcrumbs::render())
        @yield('breadcrumbs')
        @include ('cabinet.manager._nav', ['page' => 'edit'])
    <div class="form-group row">
        <div class="col-6 col-md-4">
            <h6>{{ __('cabinet.manager.form_profile') }}</h6>
        </div>
        <div class="col-6 col-md-8">
            {{ $companyName }}
        </div>
    </div>

    <form method="POST" accept-charset="UTF-8" id="profile-create-form" class="form-vertical" enctype="multipart/form-data" action="{{ route('cabinet.manager.update', ['profile' => $profile->id, 'manager' => $manager->id]) }}">
        @csrf
        @method('PUT')
        
        <div class="form-group row">
            <label for="example-text-input" class="col-12 col-md-4 col-form-label">{{ __('cabinet.manager.form_name') }}</label>
            <div class="col-12 col-md-8">
                <input class="form-control" name="name" type="text" value="{{ old('name', $manager->users->first()->name) }}" >
            </div>
        </div>
        
        <div class="form-group row">
            <label for="example-tel-input" class="col-12 col-md-4 col-form-label">{{ __('cabinet.manager.form_last_name') }}</label>
            <div class="col-12 col-md-8">
                <input class="form-control" name="last_name" type="text" value="{{ old('last_name', ($manager->profileProperties->where('code', 'last_name')->pluck('value')->shift() )?? '') }}" >
            </div>
        </div>
        
        <div class="form-group row">
            <label for="example-text-input" class="col-12 col-md-4 col-form-label">{{ __('cabinet.manager.form_patronymic') }}</label>
            <div class="col-12 col-md-8">
                <input class="form-control" name="patronymic" type="text" value="{{ old('patronymic', ($manager->profileProperties->where('code', 'patronymic')->pluck('value')->shift() )?? '') }}">
            </div>
        </div>
        
        <div class="form-group row">
            <label for="example-email-input" class="col-12 col-md-4 col-form-label">{{ __('cabinet.manager.form_position') }}</label>
            <div class="col-12 col-md-8">
                <input class="form-control" name="position" type="text" value="{{ old('position', ($manager->profileProperties->where('code', 'position')->pluck('value')->shift() )?? '') }}">
            </div>
        </div>
        
        <div class="form-group row">
            <label for="example-tel-input" class="col-12 col-md-4 col-form-label">{{ __('cabinet.manager.form_birthday') }}</label>
            <div class="col-12 col-md-8">
                <input class="form-control" name="birthday" type="text" id="datepicker" value="{{ old('birthday', ($manager->profileProperties->where('code', 'birthday')->pluck('value')->shift() )?? '') }}">
            </div>
        </div>
        
        <div class="form-group row">
            <label for="example-radio-input" class="col-12 col-md-4 col-form-label">{{ __('cabinet.manager.form_gender') }}</label>
            <div class="col-12 col-md-8">
             
                <label class="form-check-label ml-4">
                    <input type="radio" class="form-check-input" name="gender" id="gender1" value="man" checked>
                    {{ __('cabinet.manager.form_gender_man') }}
                </label>
               
              
                <label class="form-check-label offset-2">
                    <input type="radio" class="form-check-input" name="gender" id="gender2" value="women">
                    {{ __('cabinet.manager.form_gender_women') }}
                </label>
                
            </div>
        </div>
        
        <div class="form-group row">
            <label for="example-phone-input" class="col-12 col-md-4 col-form-label">{{ __('cabinet.manager.form_phone') }}</label>
            <div class="col-12 col-md-8">
                <input class="form-control" placeholder="" name="phone" type="text" value="{{ old('phone', ($manager->profileProperties->where('code', 'phone')->pluck('value')->shift() )?? '') }}">
            </div>
        </div>

        <hr class="mt-4">
        <div class="form-group row ml-1">
            <button type="submit" class="btn btn-secondary mt-3">{{ __('interface.buttons.save') }}</button>
        </div>
    </form>
</div>
@endsection