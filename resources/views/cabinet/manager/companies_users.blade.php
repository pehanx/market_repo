@extends('cabinet.home')

@section('sidebar')
    @include ('cabinet.sidebar', ['page' => 'manager'])
@endsection

@section('cabinet-content')
    <div class="cabinet">
        @section('breadcrumbs', Breadcrumbs::render())
        @yield('breadcrumbs')
        <div class="cabinet__title">{{ Breadcrumbs::generate()->last()->title }}</div>
        @if($users->first())
        <div class="cabinet__content cabinet__content__profiles">

            <div class="cabinet__content__profiles__elements">
                @foreach ($users as $user)
                <div class="cabinet__content__profiles__element">
                    
                    @if($user->downloads->first() !== null)
                        @php $logo = $user->downloads->where('type', 'avatar')->first();@endphp
                        @if($logo !== null)
                        <a href="{{ route('cabinet.company.list', $user->id) }}">
                            <div 
                                class="cabinet__content__profiles__image"
                                style="background-image: url('{{ asset('storage/'.$logo->path) }}');"
                            >
                            </div>
                        </a>
                        @endif
                    @elseif($user->profiles->first() !== null)
                        @php $logo = $user->profiles->first()->downloads->where('type', 'company_logo_image')->first() @endphp
                        @if($logo !== null)
                        <a href="{{ route('cabinet.company.list', $user->id) }}">
                            <div 
                                class="cabinet__content__profiles__image"
                                style="background-image: url('{{ asset('storage/'.$logo->path) }}');"
                            >
                            </div>
                        </a>
                        @endif
                    @endif
                    <div class="cabinet__content__profiles__content">
                        <a href="{{ route('cabinet.company.list', $user->id) }}">
                            <div class="cabinet__content__profiles__content__title">
                            {{ $user->name }}
                            </div>
                            <div class="cabinet__content__profiles__content__parameter">
                                <span>{{ __('Созданные профили') }}:</span>
                                @foreach ($user->profiles as $profile)
                                    {{ __('interface.localisation.' . $profile->profileProperties->first()->localisations->code) }}
                                @endforeach
                            </div>
                        </a>
                    </div>
                    <div class="cabinet__content__profiles__actions">

                        <button data-route="{{ route('cabinet.company.invite') }}" data-user="{{ $user->id }}" id="sendInvite" class="button button-primary-bordered">
                            {{ __('Пригласить') }}
                        </button>
                        
                        <a href="{{ route('cabinet.company.list', $user->id) }}" class="button button-primary-bordered">
                            {{ __('Список профилей') }}
                        </a>

                        <a href="{{ route('cabinet.company.edit', $user->id) }}" class="button button-primary-bordered">
                            {{ __('Редактировать') }}
                        </a>
                        
                    </div>
                </div>
                @endforeach
            </div>
            <a href="{{ route('cabinet.company.create') }}" class="button button-inline button-primary-bordered width-fit-content">
                {{ __('Создать новую компанию') }}
            </a>
        </div>
        @else
        <div class="cabinet__content cabinet__content__profiles">
            <div class="cabinet__content__profiles-empty">
                <span>{{ __('У вас ещё нет созданных компаний') }}</span>
            </div>
            
            <a href="{{ route('cabinet.company.create') }}" class="button button-inline button-primary-bordered width-fit-content">
                {{ __('Создать компанию') }}
            </a>
        </div>
        @endif
    </div>
    @include('modals.destroy')
@endsection

@section('content-script')
<script>
    document.addEventListener('DOMContentLoaded', function() {
        cabinet.init('.delete-profile-btn', 'click', 'deleteProfile');

        $('#sendInvite').click(function(){
            let user = $(this).data('user');
            let url = $(this).data('route');

            axios
            .post(url, {user: user}, {

            }).then(response => {
                location.reload();
            });
        });
    });
</script>
@endsection