@extends('cabinet.home')

@section('sidebar')
    @include ('cabinet.sidebar', ['page' => 'manager'])
@endsection

@section('cabinet-content')
<div class="col col-md-9">
    @section('breadcrumbs', Breadcrumbs::render())
    @yield('breadcrumbs')
    <table class="table table-sm border-0 table-bordered table-responsive">
        <thead>
            <tr>
                <th>{{ __('cabinet.manager.profile') }}</th>
                <th>{{ __('cabinet.manager.type') }}</th>
                <th>{{ __('cabinet.manager.status') }}</th>
                <th>{{ __('cabinet.manager.manager') }}</th> 
                <th>{{ __('cabinet.manager.mail') }}</th> 
                <th>{{ __('cabinet.manager.actions') }}</th>
            </tr>
        </thead>
        <tbody>
        @if($profiles->first())
            @foreach ($profiles as $profile)
                @if($profile->profileProperties->first())
                <tr>
                    <td data-label="{{ __('cabinet.manager.profile') }}" @if($profile->users->first()) rowspan="{{ $profile->users->count() }}" @endif >{{ $profile->profileProperties->where('code', 'company_name')->first()->value }}</td>
                    <td data-label="{{ __('cabinet.manager.type') }}" @if($profile->users->first()) rowspan="{{ $profile->users->count() }}" @endif >{{ __('cabinet.profile.'.$profile->profileTypes->code) }}</td>
                    <td data-label="{{ __('cabinet.manager.status') }}" @if($profile->users->first()) rowspan="{{ $profile->users->count() }}" @endif >
                        @if ($profile->status === 'active')
                            {{ __('cabinet.manager.approved') }}
                        @elseif($profile->status === 'inactive')
                            {{ __('cabinet.manager.not_approved') }}
                        @elseif($profile->status === 'wait')
                            {{ __('cabinet.manager.wait_approve') }}
                        @endif
                    </td>
                    
                    @if($profile->users()->first())
                        @foreach ($profile->users as $key => $manager)
                                <td data-label="{{ __('cabinet.manager.login') }}">{{ $manager->name }}</td>
                                <td data-label="{{ __('cabinet.manager.mail') }}">{{ $manager->email }}</td>
                                <td data-label="{{ __('cabinet.manager.actions') }}">
                                    <a href="{{ route('cabinet.manager.edit', ['profile' => $profile->id, 'manager' => $manager->profiles->first()->id ]) }}" class="mr-2">
                                        <i class="fa fa-pencil" title="{{ __('interface.buttons.change') }}"></i>
                                    </a>
                                    <button type="button" class="delete-profile-btn btn-sm px-0" data-text="{{ __('modals.your_sure_destroy_manager', ['manager' => $manager->name]) }}" data-route="{{ route('cabinet.manager.destroy', ['profile' => $profile->id, 'manager' => $manager->profiles->first()->id]) }}" >
                                        <i class="fa fa-trash" title="{{ __('interface.buttons.delete') }}"></i>
                                    </button>
                                </td>
                            </tr>
                                @if( $key+1 !== $profile->users->count() )
                                    <tr> 
                            @endif
                        @endforeach
                    @else
                    <td data-label="{{ __('cabinet.manager.manager') }}">&nbsp;</td>
                    <td data-label="{{ __('cabinet.manager.mail') }}">&nbsp;</td>
                    <td data-label="{{ __('cabinet.manager.actions') }}">&nbsp;</td>
                    @endif
                    </tr>
                @endif
            @endforeach
        @endif
        </tbody>
    </table>
     
    <div class="row justify-content-center mt-5">
    {{ $profiles->links() }}
    </div>
    @include('modals.destroy')
</div>
@endsection

@section('content-script')
<script>
    document.addEventListener('DOMContentLoaded', function() {
        cabinet.init('.delete-profile-btn', 'click', 'deleteProfile');
    });
</script>
@endsection