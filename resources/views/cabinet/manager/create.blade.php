@extends('cabinet.home')

@section('cabinet-content')
<div class="col col-md-9">
    @section('breadcrumbs', Breadcrumbs::render())
    @yield('breadcrumbs')
    
    <div class="form-group row">
        <div class="col-6 col-md-4">
            <h6>{{ __('cabinet.profile.profile') }}</h6>
        </div>
        <div class="col-6 col-md-8">
            {{ $companyName }}
        </div>
    </div>

    <form method="POST" accept-charset="UTF-8" id="profile-create-form" class="form-vertical" enctype="multipart/form-data" action="{{ route('cabinet.manager.store', ['profile' => $profile->id]) }}">
        @csrf
        <div class="form-group row">
            <label for="example-text-input" class="col-12 col-md-4 col-form-label">{{ __('cabinet.manager.form_name') }}</label>
            <div class="col-12 col-md-8">
                <input class="form-control" name="name" type="text" value="{{ old('name', ($account['name'])?? '') }}" required>
            </div>
        </div>
        
        <div class="form-group row">
            <label for="example-tel-input" class="col-12 col-md-4 col-form-label">{{ __('cabinet.manager.form_last_name') }}</label>
            <div class="col-12 col-md-8">
                <input class="form-control" name="last_name" type="text" value="{{ old('last_name', ($account['last_name'])?? '') }}" >
            </div>
        </div>
        
        <div class="form-group row">
            <label for="example-text-input" class="col-12 col-md-4 col-form-label">{{ __('cabinet.manager.form_patronymic') }}</label>
            <div class="col-12 col-md-8">
                <input class="form-control" name="patronymic" type="text" value="{{ old('patronymic', ($account['patronymic'])?? '') }}">
            </div>
        </div>
        
        <div class="form-group row">
            <label for="example-email-input" class="col-12 col-md-4 col-form-label">{{ __('cabinet.manager.form_position') }}</label>
            <div class="col-12 col-md-8">
                <input class="form-control" name="position" type="text" value="{{ old('position') }}">
            </div>
        </div>
        
        <div class="form-group row">
            <label for="example-tel-input" class="col-12 col-md-4 col-form-label">{{ __('cabinet.manager.form_birthday') }}</label>
            <div class="col-12 col-md-8">
                <input class="form-control" name="birthday" type="text" id="datepicker" value="{{ old('birthday', ($account['birthday'])?? '') }}">
            </div>
        </div>
        
        <div class="form-group row">
            <label for="example-radio-input" class="col-12 col-md-4 col-form-label">{{ __('cabinet.manager.form_gender') }}</label>
            <div class="col-12 col-md-8">
             
                <label class="form-check-label ml-4">
                    <input type="radio" class="form-check-input" name="gender" id="gender1" value="man" checked>
                    {{ __('cabinet.manager.form_gender_man') }}
                </label>
               
              
                <label class="form-check-label offset-2">
                    <input type="radio" class="form-check-input" name="gender" id="gender2" value="women">
                    {{ __('cabinet.manager.form_gender_women') }}
                </label>
                
            </div>
        </div>
        
        <div class="form-group row">
            <label for="example-phone-input" class="col-12 col-md-4 col-form-label">{{ __('cabinet.manager.form_phone') }}</label>
            <div class="col-12 col-md-8">
                <input class="form-control" placeholder="" name="phone" type="text" value="{{ old('phone', ($account['phone'])?? '') }}">
            </div>
        </div>
        
        <hr class="my-4">

        <div class="form-group row">
            <label for="example-email-input" class="col-12 col-md-4 col-form-label">{{ __('cabinet.manager.form_email') }}</label>
            <div class="col-12 col-md-8">
                @error('email')
                <span class="text-danger">{{ $message }}</span>
                @enderror
                <input class="form-control" placeholder="" name="email" type="text" value="{{ old('email') }}" >
            </div>
        </div>

        <div class="form-group row">
            <label for="example-password-input" class="col-12 col-md-4 col-form-label">{{ __('cabinet.manager.form_password') }}</label>
            <div class="col-12 col-md-8">
                @error('password')
                <span class="text-danger">{{ $message }}</span>
                @enderror
                <input class="form-control" placeholder="" name="password" type="password" value="{{ old('password') }}" required>
            </div>
        </div>
        
        <div class="form-group row">
            <label for="example-password-confirmation-input" class="col-12 col-md-4 col-form-label">{{ __('cabinet.manager.form_confirm_password') }}</label>
            <div class="col-12 col-md-8">
                <input class="form-control" placeholder="" name="password_confirmation" type="password" value="{{ old('password_confirmation') }}" required>
            </div>
        </div>
        
        <hr class="mt-4">
        <div class="form-group row ml-1">
            <button type="submit" class="btn btn-secondary mt-3">{{ __('interface.buttons.add_manager') }}</button>
        </div>
    </form>
</div>
@endsection