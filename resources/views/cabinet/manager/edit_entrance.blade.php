@extends('cabinet.home')

@section('cabinet-content')
<div class="col col-md-9">
        @section('breadcrumbs', Breadcrumbs::render())
        @yield('breadcrumbs')
        @include ('cabinet.manager._nav', ['page' => 'edit_entrance'])
    <div class="form-group row">
        <div class="col-6 col-md-4">
            <h6>{{ __('cabinet.manager.form_profile') }}</h6>
        </div>
        <div class="col-6 col-md-8">
            {{ $companyName }}
        </div>
    </div>

    <form method="POST" accept-charset="UTF-8" id="profile-create-form" class="form-vertical" enctype="multipart/form-data" action="{{ route('cabinet.manager.update_entrance', ['manager' => $manager->id, 'profile' => $profile->id]) }}">
        @csrf
        <div class="form-group row">
            <label for="example-email-input" class="col-12 col-md-4 col-form-label">{{ __('messages.email') }}</label>
            <div class="col-12 col-md-8">
                @error('email')
                <span class="text-danger">{{ $message }}</span>
                @enderror
                <input class="form-control" placeholder="" name="email" type="text" value="{{ old('email', ($manager->users->first()->email) ?? '') }}" required>
            </div>
        </div>

        <div class="form-group row">
            <label for="example-password-input" class="col-12 col-md-4 col-form-label">{{ __('messages.password') }}</label>
            <div class="col-12 col-md-8">
                @error('password')
                <span class="text-danger">{{ $message }}</span>
                @enderror
                <input class="form-control" placeholder="" name="password" type="password" value="{{ old('password') }}" required>
            </div>
        </div>
        
        <div class="form-group row">
            <label for="example-password-confirmation-input" class="col-12 col-md-4 col-form-label">{{ __('messages.confirm_password') }}</label>
            <div class="col-12 col-md-8">
                <input class="form-control" placeholder="" name="password_confirmation" type="password" value="{{ old('password_confirmation') }}" required>
            </div>
        </div>
        
        <hr class="mt-4">
        <div class="form-group row ml-1">
            <button type="submit" class="btn btn-secondary mt-3">{{ __('interface.buttons.save') }}</button>
        </div>
    </form>
</div>
@endsection