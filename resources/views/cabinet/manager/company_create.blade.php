@extends('cabinet.home')

@section('sidebar')
    @include ('cabinet.sidebar', ['page' => 'entrance'])
@endsection

{{--
@section('cabinet-content')

    @if($user->socialAccounts->first())
    <div class="form-group row">
        <label for="example-email-input" class="col-12 col-md-4 col-form-label">{{ __('cabinet.account.social') }}</label>
        <div class="col-12 col-md-8">
        
        @foreach($user->socialAccounts as $socialAccount)
            
            @if($socialAccount->social === 'vkontakte')
                <i class="fa fa-vk fa-2x"></i>
            @elseif($socialAccount->social === 'odnoklassniki')
                <i class="fa fa-odnoklassniki fa-2x"></i>
            @endif
        @endforeach
        </div>
    </div>
    @endif

</div>
@endsection
--}}

@section('cabinet-content')
<div class="cabinet">
	@section('breadcrumbs', Breadcrumbs::render())
    @yield('breadcrumbs')
	<div class="cabinet__title">{{ Breadcrumbs::generate()->last()->title }}</div>
    <div class="cabinet__content cabinet__content__personalData">
        <form method="POST" accept-charset="UTF-8" id="user-update-form" enctype="multipart/form-data" action="{{ route('cabinet.company.store') }}">
            @csrf
            <div class="form cabinet__content__personalData__form">

                <div class="form__row form__row-required @if($errors->has('name')) {{ 'form__row-error' }} @endif"> <!-- form__row-error form__row-hasText-->
                    <input class="form__row__input" id="name" name="name" type="text" value="{{ old('name' ?? '' ) }}" required>
                    <label for="name" class="form__row__label">{{ __('cabinet.account.name') }}</label>
                    @error('name')
                    <div class="form__row__error">{{ $message }}</div>
                    @enderror
                </div>
               
                <div class="form__row form__row-required @if($errors->has('email')) {{ 'form__row-error' }} @endif"> <!-- form__row-error form__row-hasText-->
                    <input class="form__row__input" id="email" name="email" type="text" value="{{ old('email' ?? '' ) }}" required>
                    <label for="email" class="form__row__label">{{ __('cabinet.account.email') }}</label>
                    @error('email')
                    <div class="form__row__error">{{ $message }}</div>
                    @enderror
                </div>
                
                <div class="form__row">
                    <button type="submit" class="button button-inline button-primary">{{ __('Создать') }}</button>
                </div>
            </div>
        </form>
    </div>
</div>
@endsection