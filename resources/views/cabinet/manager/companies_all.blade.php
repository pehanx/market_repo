@extends('cabinet.home')

@section('sidebar')
    @include ('cabinet.sidebar', ['page' => 'manager'])
@endsection

@section('cabinet-content')
    <div class="cabinet">
        @section('breadcrumbs', Breadcrumbs::render())
        @yield('breadcrumbs')
        <div class="cabinet__title">{{ Breadcrumbs::generate()->last()->title }}</div>
        @if($companies->first())
        <div class="cabinet__content cabinet__content__profiles">

            <div class="cabinet__content__profiles__elements">
                @foreach ($companies as $company)
                <div class="cabinet__content__profiles__element @if(session()->has('active_profile') && session()->get('active_profile')->id === $company->id) active @endif">
                    @php $logo = $company->downloads->where('type', 'company_logo_image')->first() @endphp
                    @if($logo)
                    <a href="{{ route('cabinet.profile.show', $company->id) }}">
                        <div 
                            class="cabinet__content__profiles__image"
                            style="background-image: url('{{ asset('storage/'.$logo->path) }}');"
                        >
                        </div>
                    </a>
                    @endif
                    <div class="cabinet__content__profiles__content">
                        <a href="{{ route('cabinet.profile.show', $company->id) }}">
                            <div class="cabinet__content__profiles__content__title">
                            {{ $company->profileProperties->where('code', 'company_name')->first()->value }}
                            </div>
                            <div class="cabinet__content__profiles__content__parameter">
                                <span>{{ __('cabinet.profile.language') }}:</span>
                                {{ __('interface.localisation.' . $company->profileProperties->first()->localisations->code) }}
                            </div>

                            <div class="cabinet__content__profiles__content__parameter">
                                <span>Тип:</span>
                                @if ($company->profile_type_id === 2)
                                    Продавец
                                @else
                                    Покупатель
                                @endif
                            </div>

                            <div class="cabinet__content__profiles__content__parameter">
                                <span>{{ __('cabinet.profile.status') }}:</span> 
                                @if ($company->status === 'active')
                                    {{ __('cabinet.profile.approved') }}
                                @elseif($company->status === 'inactive')
                                    {{ __('cabinet.profile.not_approved') }}
                                @elseif($company->status === 'wait')
                                    {{ __('cabinet.profile.wait_approve') }}
                                @endif
                            </div>
                        </a>
                    </div>
                    <div class="cabinet__content__profiles__actions">
                        @if (
                            ($company->status === 'active' && !session()->has('active_profile')) 
                                OR 
                            ($company->status === 'active' && session()->has('active_profile') && session()->get('active_profile')->id !== $company->id)
                        )
                        <a href="{{ route('cabinet.profile.select', $company->id) }}" class="button button-primary-bordered">
                            {{ __('interface.buttons.select') }}
                        </a>
                        @elseif($company->status === 'inactive')
                        <a href="{{ route('cabinet.company.approve', $company->id) }}" class="button button-primary-bordered">
                            {{ __('interface.buttons.approve') }}
                        </a>
                        @endif
                        <a href="{{ route('cabinet.profile.edit', $company->id) }}" class="button button-primary-bordered">
                            {{ __('interface.buttons.edit') }}
                        </a>
                        <button 
                            type="button" 
                            class="button button-primary-link button-primary-link-line delete-profile-btn" 
                            data-text="{{ __('modals.your_sure_destroy_profile', ['profile' => $company->profileProperties->where('code', 'company_name')->first()->value]) }}" 
                            data-route="{{ route('cabinet.profile.destroy', $company->id) }}" 
                        >
                            {{ __('interface.buttons.delete') }}
                        </button>
                    </div>
                </div>
                @endforeach
            </div>
            <a href="{{ route('cabinet.company.profile_create', ['company' => $selectedCompany, 'type' => 'seller']) }}" class="button button-inline button-primary-bordered width-fit-content">
                {{ __('cabinet.profile.create_seller') }}
            </a>
            <a href="{{ route('cabinet.company.profile_create', ['company' => $selectedCompany, 'type' => 'buyer']) }}" class="button button-inline button-primary-bordered width-fit-content">
                {{ __('cabinet.profile.create_buyer') }}
            </a>
        </div>
        @else
        <div class="cabinet__content cabinet__content__profiles">
            <div class="cabinet__content__profiles-empty">
                <span>{{ __('interface.messages.create_profile_buyer') }}</span>
                <a href="/help/for-buyers/buer-profile/how-create-buyer-profile" target="_blank">
                    <i class="fa fa-info-circle" aria-hidden="true"></i>
                </a>
            </div>
            
            <a href="{{ route('cabinet.company.profile_create', ['company' => $selectedCompany, 'type' => 'seller']) }}" class="button button-inline button-primary-bordered width-fit-content">   {{ __('cabinet.profile.create_seller') }}
            </a>
            <a href="{{ route('cabinet.company.profile_create', ['company' => $selectedCompany, 'type' => 'buyer']) }}" class="button button-inline button-primary-bordered width-fit-content">   {{ __('cabinet.profile.create_buyer') }}
            </a>
        </div>
        @endif
    </div>
    @include('modals.destroy')
@endsection

@section('content-script')
<script>
    document.addEventListener('DOMContentLoaded', function() {
        cabinet.init('.delete-profile-btn', 'click', 'deleteProfile');
    });
</script>
@endsection