@extends('cabinet.home')

@section('sidebar')
    @include ('cabinet.sidebar', ['page' => 'account'])
@endsection

@section('cabinet-content')
<div class="cabinet">
	<div class="cabinet__title"></div>
	<div class="cabinet__content cabinet__content__personalData">
        <form method="POST" accept-charset="UTF-8" id="token-create-form" enctype="multipart/form-data" action="/oauth/clients">
            @csrf
            <div class="form cabinet__content__personalData__form">
        
                <div class="form__row form__row-required @if($errors->has('name')) {{ 'form__row-error' }} @endif">
                    <input class="form__row__input" name="name" type="text" value="" id="name" required>
                    <label for="name" class="form__row__label">{{ __('cabinet.account.form_name') }}</label>
                    @error('name')
                    <div class="form__row__error">{{ $message }}</div>
                    @enderror
                </div>
                
                <div class="form__row form__row-required @if($errors->has('redirect')) {{ 'form__row-error' }} @endif">
                    <input class="form__row__input" name="redirect" type="text" value="" id="redirect-url" required>
                    <label for="redirect-url" class="form__row__label">RedirectURL</label>
                    @error('redirect')
                    <div class="form__row__error">{{ $message }}</div>
                    @enderror
                </div>
                
                <div class="form__row">
                    <button class="button button-inline button-primary submit-form" type="submit">{{ __('interface.buttons.save_changes') }}</button>
                </div>
            </div>
        </form>
	</div>
</div>
@endsection

@section('content-script')
<script>
document.addEventListener('DOMContentLoaded', function()
{
    
});
</script>
@endsection