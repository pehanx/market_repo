@extends('shop.home')

@section('title')
{{ $seller->profile->profileProperties->where('code', 'company_name')->first()->value }} - {{ __('interface.meta.supplier_contacts') }}
@endsection

@section('header-bottom')
@endsection

@section('content')
<div class="store">
	<div class="store__top">
		<div class="container">
			<div class="store__top__saler"><span>{{ $interface['supplier']->value }}</span><strong>{{ $seller->profile->profileProperties->where('code', 'company_name')->first()->value }}</strong></div>
			{{-- <div class="store__top__contacts"><div class="store__top__contacts__icon"></div><div class="store__top__contacts__text">{{ $interface['add_contacts']->value }}</div></div> --}}
		</div>
	</div>
    
	<div class="store__header">
		<div class="container">
			<div class="store__header__name">
				<div class="store__header__name__image" style="background-image: url('{{ asset('storage/' . $seller->profile->downloads->where('type', 'company_logo_image')->first()->path ?? '') }}');"></div>
				<div class="store__header__name__title">{{ $seller->profile->profileProperties->where('code', 'company_name')->first()->value }}</div>
			</div>
            
			@include ('shop._search', ['interface' => $interface, 'action' => '?', 'type' => 'button'])
            
		</div>
	</div>
    
	@include ('shop._nav', ['page' => 'contact', 'interface' => $interface])
    
	<div class="store__contacts">
		<div class="container">
			<h2 class="store__contacts__title">{{ $interface['contacts']->value }}</h2>
            <h3 class="store__contacts__subtitle">{{ $interface['main_information']->value }}</h3>
			<div class="formPreview">
				<div class="formPreview__row">
					<div class="formPreview__row__label">{{ $interface['company_name']->value }}</div>
					<div class="formPreview__row__value">{{ $seller->profile->profileProperties->where('code', 'company_name')->first()->value }}</div>
				</div>
				@if($seller->profile->profileProperties->contains('code', 'index'))
				@else
				<div class="formPreview__row">
					<div class="formPreview__row__label">{{ $interface['index']->value }}</div>
					<div class="formPreview__row__value">{{ $seller->profile->profileProperties->where('code', 'index')->first()->value }}</div>
				</div>
				@endif
				@empty($seller->profile->characteristics['country']['value'])
				@else
				<div class="formPreview__row">
					<div class="formPreview__row__label">{{ $interface['country']->value }}</div>
					<div class="formPreview__row__value">{{ $seller->profile->characteristics['country']['value'] }}</div>
				</div>
				@endempty
				@if($seller->profile->profileProperties->contains('code', 'region'))
				@else
				<div class="formPreview__row">
					<div class="formPreview__row__label">{{ $interface['region']->value }}</div>
					<div class="formPreview__row__value">{{ $seller->profile->profileProperties->where('code', 'region')->first()->value }}</div>
				</div>
				@endif
				@if($seller->profile->profileProperties->contains('code', 'city'))
				@else
				<div class="formPreview__row">
					<div class="formPreview__row__label">{{ $interface['city']->value }}</div>
					<div class="formPreview__row__value">{{ $seller->profile->profileProperties->where('code', 'city')->first()->value }}</div>
				</div>
				@endif
				<div class="formPreview__row">
					<div class="formPreview__row__label">{{ $interface['address']->value }}</div>
					<div class="formPreview__row__value">
						@if($seller->profile->profileProperties->contains('code', 'street'))
						{{ Str::lower( $interface['street']->value ) }} &nbsp; {{ $seller->profile->profileProperties->where('code', 'street')->first()->value ?? '' }}
						@endif
						@if( $seller->profile->profileProperties->contains('code', 'number_home') ) 
						&sbquo;&nbsp;&nbsp;{{ Str::lower( $interface['number_home']->value ) }} &nbsp; {{ $seller->profile->profileProperties->where('code', 'number_home')->first()->value ?? '' }}
						@endif
						@if( $seller->profile->profileProperties->contains('code', 'number_build') ) 
						&sbquo;&nbsp;&nbsp;{{ Str::lower( $interface['number_build']->value ) }} &nbsp; {{ $seller->profile->profileProperties->where('code', 'number_build')->first()->value ?? '' }}
						@endif
						@if( $seller->profile->profileProperties->contains('code', 'number_room') ) 
						&sbquo;&nbsp;&nbsp;{{ Str::lower( $interface['number_room']->value ) }} &nbsp; {{ $seller->profile->profileProperties->where('code', 'number_room')->first()->value ?? '' }}
						@endif
                    </div>
				</div>
			</div>
			<h3 class="store__contacts__subtitle">{{ $interface['contact_information']->value }}</h3>
			<div class="formPreview">
				@if( $seller->profile->profileProperties->where('code', 'name')->first() || $seller->profile->profileProperties->where('code', 'last_name')->first() )
				<div class="formPreview__row">
					<div class="formPreview__row__label">{{ $interface['contact_person']->value }}</div>
					<div class="formPreview__row__value">
                    {{ $seller->profile->profileProperties->where('code', 'name')->first()->value }}
                    {{ $seller->profile->profileProperties->where('code', 'last_name')->first()->value }}
                    </div>
				</div>
				@endif
				@if($seller->profile->profileProperties->where('code', 'position')->first())
				<div class="formPreview__row">
					<div class="formPreview__row__label">{{ $interface['position']->value }}</div>
					<div class="formPreview__row__value">{{ $seller->profile->profileProperties->where('code', 'position')->first()->value }}</div>
				</div>
				@endif
				@if($seller->profile->profileProperties->where('code', 'number_phone')->first())
				<div class="formPreview__row">
					<div class="formPreview__row__label">{{ $interface['phone']->value }}</div>
					<div class="formPreview__row__value">{{ $seller->profile->profileProperties->where('code', 'number_phone')->first()->value }}</div>
				</div>
				@endif
			</div>
			@if(!$myProfile)
			<div class="store__contacts__actions">
				<a href="{{ route('cabinet.chat.member', ['member'=>$seller->profile]) }}" class="button button-primary button-inline">{{ $interface['write_supplier']->value }}</a>
				@if(!$isContact)
					<div class="store__top__contacts" data-route-contact="{{ route('contact.add', ['contact'=>$seller->profile]) }}"><div class="store__top__contacts__icon"></div><div class="store__top__contacts__text">{{ $interface['add_contacts']->value }}</div></div>
				@endif
			</div>
			@endif
		</div>
	</div>
</div>
@endsection