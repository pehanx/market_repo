@extends('shop.home')

@section('title')
{{ $seller->profile->profileProperties->where('code', 'company_name')->first()->value }}
@endsection

@section('meta')
    <meta name="description" content="{{ $seller->profile->profileProperties->where('code', 'company_description')->first()->value ?? '' }}">    
    <meta name="title" content="{{ $seller->profile->profileProperties->where('code', 'company_name')->first()->value }}">
    <meta property="og:image" content="{{ asset('storage/' . $seller->profile->downloads['company_logo_image']->first()->path) }}">
    <meta property="og:title" content="{{ $seller->profile->profileProperties->where('code', 'company_name')->first()->value }}">
@endsection

@section('header-bottom')
@endsection

@section('content')
<div class="store">
	<div class="store__top">
		<div class="container">
			<div class="store__top__saler"><span>{{ $interface['supplier']->value }}</span><strong>{{ $seller->profile->profileProperties->where('code', 'company_name')->first()->value }}</strong></div>
			{{-- <div class="store__top__contacts"><div class="store__top__contacts__icon"></div><div class="store__top__contacts__text">{{ $interface['add_contacts']->value }}</div></div> --}}
		</div>
	</div>
	<div class="store__header">
		<div class="container">
			<div class="store__header__name">
				<div class="store__header__name__image" style="background-image: url('{{ asset('storage/' . $seller->profile->downloads['company_logo_image']->first()->path) }}');"></div>
				<div class="store__header__name__title">{{ $seller->profile->profileProperties->where('code', 'company_name')->first()->value }}</div>
			</div>
            @include ('shop._search', ['interface' => $interface, 'action' => route('shop.product.index', ['user' => $seller->id, 'localisation' => $seller->currentLocalisation]), 'type' => 'submit'])
		</div>
	</div>
    
    @include ('shop._nav', ['page' => 'main', 'interface' => $interface])
	
	<div class="store__mainSlider owl-carousel owl-theme">
        @foreach($seller->profile->downloads['company_main_banner_images'] as $mainSlider)
		<div class="store__mainSlider__item" style="background-image:url('{{ asset('storage/' . $mainSlider->path) }}'); background-size: contain; background-repeat: no-repeat;"></div>
        @endforeach
	</div>

	<div class="store__languages">
		<div class="container">
			<div class="store__languages__title">{{ $interface['multilingual_sites']->value }}</div>
			<div class="store__languages__flags">
                @foreach($seller->localisations as $localisation)
                <a target="_blank" href="{{ route('shop.show', ['user' => $seller->id, 'localisation' => $localisation->code]) }}">
                    <div class="store__languages__flags__item">
                        <div class="store__languages__flags__item__flag store__languages__flags__item__flag-{{ $localisation->value }}"></div>
                        <div class="store__languages__flags__item__title">{{ $localisation->value }}</div>
                    </div>
                </a>
                @endforeach
			</div>
		</div>
	</div>

	<div class="store__categories">
		<div class="container">
			<div class="store__categories__title">{{ $interface['categories']->value }}</div>
			<div class="store__categories__wrapper">
                @foreach($seller->categories as $category)
				<a href="{{ '/' . request()->path() . $category->shop_link }}">
                    <div class="store__categories__item" style="background-image: url('{{ asset('storage/' . $category->image->path) }}');">
                        <div class="store__categories__item__title">{{ $category->categoryProperties->where('code', 'name')->first()->value }}</div>
                    </div>
                </a>
				@endforeach
			</div>
		</div>
	</div>
    
    @if($seller->topProducts->first())
	<div class="store__catalog">
		<div class="container">
			<div class="store__catalog__title">{{ $interface['top_products']->value }}</div>
			<div class="store__catalog__wrapper" style="width:100%;">
                @foreach($seller->topProducts as $topProduct)
 
                <div class="store__catalog__item"> 
                    <a href="{{ route('catalog.product.show', ['product' => $topProduct->id]) }}" class="store__catalog__item__image"  style="background-image: url('{{ asset('storage/' . $topProduct->downloads->where('type', 'preview_image')->first()->path) }}');">
                        {{--
                        @guest 
                        <form method="POST" class="favorite-add-form" action="{{ route('favorite.add', ['type' => 'product', 'id' => $topProduct->id]) }}">
                            @csrf
                            <div class="store__catalog__item__favorite" title="{{ $interface['add_favorites']->value }}"></div>
                        </form>
                        @endguest
                        
                        @auth 
                            @if( $topProduct->favorites->where('user_id', $seller->id)->first() )
                                <div class="store__catalog__item__favorite active favorite-remove" data-route="{{ route('favorite.remove', ['type' => 'product', 'id' => $topProduct->id]) }}" title="{{ $interface['remove_favorites']->value }}"></div>
                            @else
                                <div class="store__catalog__item__favorite favorite-add" data-route="{{ route('favorite.add', ['type' => 'product', 'id' => $topProduct->id]) }}" title="{{ $interface['add_favorites']->value }}"></div>
                            @endif
                        @endauth
                        --}}
 
                    </a>
                    <div class="store__catalog__item__title">{{ $topProduct->productProperties->where('code', 'name')->first()->value }}</div>
                    <div class="store__catalog__item__price">

                        @php
                            $currency = $currencyRates->where('code', $currentCurrencyCode)->first();
                        @endphp

                        {{ $currency->symbol }}{{ $topProduct->productPrices->first()->min * $currency->rate }} 
                        {{ $topProduct->productPrices->first()->max * $currency->rate ? ' — '.$currency->symbol . $topProduct->productPrices->first()->max * $currency->rate : '' }}
                    </div>
                    @if(!empty($topProduct->characteristics['min_order']))
                    <div class="store__catalog__item__minOrder">
                        {{ $topProduct->characteristics['min_order']['value'] }} 
                        {{ $topProduct->characteristics['min_order']['type'] }}
                        <span>({{ $interface['min_order']->value }})</span>
                    </div>
                    @endif
                </div>
                
				@endforeach
			</div>
		</div>
	</div>
    @endif
        
	<div class="store__about">
		<div class="container">
			<div class="store__about__title">{{ $interface['about_company']->value }}</div>
			<div class="store__about__block store__about__block-1">
            
            @if( !empty($seller->profile->downloads['company_description_image']) )
            <div class="store__about__block-1__image" style="background-image: url('{{ asset('storage/' . $seller->profile->downloads['company_description_image']->first()->path) }}');"></div>
            @endif
            
				<div class="store__about__block-1__text">
					<strong>{{ $seller->profile->profileProperties->where('code', 'company_name')->first()->value }}</strong>
					<p>{{ $seller->profile->profileProperties->where('code', 'company_description')->first()->value ?? '' }}</p>
				</div>
			</div>
                       
            @if( !empty($seller->profile->downloads['company_promo_banner_images']) )
            <div class="store__about__block store__about__block-2">
                <div class="store__about__block__slider owl-carousel owl-theme">
                    @foreach($seller->profile->downloads['company_promo_banner_images'] as $promoSlider)
                    <div class="store__about__block__slider__item" style="background-image: url('{{ asset('storage/' . $promoSlider->path) }}');     background-size: contain;">
                    </div>
                    @endforeach
                </div>
            </div>
            @endif
             
            @if($seller->profile->youtube_frame)
			<div class="store__about__block store__about__block-3">
				<div class="store__about__block-3__video">
                    {!! $seller->profile->youtube_frame !!}
				</div>
			</div>
            @endif
            
            @if( !empty($seller->profile->downloads['company_other_images']) )
			<div class="store__about__block store__about__block-4">
                @foreach($seller->profile->downloads['company_other_images'] as $otherImage)
				<div class="store__about__block-4__photo" style="background-image: url('{{ asset('storage/' . $otherImage->path) }}');"></div>
                @endforeach
			</div>
            @endif
		</div>
	</div>
</div>
@endsection

@section('content-script')
<script src="{{ mix('js/seller_main.js', 'assemble') }}"></script>      
@endsection