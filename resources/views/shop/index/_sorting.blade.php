<div class="catalog__filterTop__area">
    <div class="catalog__filterTop__sort">
        <select name="order_by" id='order-by' class="catalog__filterTop__sort__select">
            <option {{ (request('order_by') == 'price_asc')? 'selected' : '' }} value="price_asc">{{ $interface['price_asc']->value }}&nbsp;&#xf160;</option>
            <option {{ (request('order_by') == 'price_desc')? 'selected' : '' }} value="price_desc">{{ $interface['price_desc']->value }}&nbsp;&#xf161;</option>
            <option {{ (request('order_by') == 'shows_asc')? 'selected' : '' }} value="shows_asc">{{ $interface['shows_asc']->value }}&nbsp;&#xf160;</option>
            <option {{ (request('order_by') == 'shows_desc')? 'selected' : '' }} value="shows_desc">{{ $interface['shows_desc']->value }}&nbsp;&#xf161;</option>
        </select>
    </div>
</div>