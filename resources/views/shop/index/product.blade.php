@extends('shop.home')

@section('title')
{{ $seller->profile->profileProperties->where('code', 'company_name')->first()->value }} - {{ __('interface.meta.supplier_products') }}
@endsection

@section('header-bottom')
@endsection

@section('content')
<div class="store">
	<div class="store__top">
		<div class="container">
			<div class="store__top__saler"><span>{{ $interface['supplier']->value }}</span><strong>{{ $seller->profile->profileProperties->where('code', 'company_name')->first()->value }}</strong></div>
			{{-- <div class="store__top__contacts"><div class="store__top__contacts__icon"></div><div class="store__top__contacts__text">{{ $interface['add_contacts']->value }}</div></div> --}}
		</div>
	</div>
    
	<div class="store__header">
		<div class="container">
			<div class="store__header__name">
				<div class="store__header__name__image" style="background-image: url('{{ asset('storage/' . $seller->profile->downloads->where('type', 'company_logo_image')->first()->path ?? '') }}');"></div>
				<div class="store__header__name__title">{{ $seller->profile->profileProperties->where('code', 'company_name')->first()->value }}</div>
			</div>
            
			@include ('shop._search', ['interface' => $interface, 'action' => '?', 'type' => 'button'])
            
		</div>
	</div>
    
	@include ('shop._nav', ['page' => 'product', 'interface' => $interface])
    
	<div class="store__catalog catalog">
		<div class="container">
            @isset($currentCategory)
			<h2 class="catalog__title">{{ $currentCategory->name }}</h2>
            @else
            <h2 class="catalog__title">{{ $interface['all_products']->value }}</h2>
            @endisset
			<div class="catalog__filterTop">
                @if(!empty($back))
                    <a 
                        href="{{  $back->shop_link }}"
                        class="catalog__filterTop__back" 
                        title="{{ $back->categoryProperties->first()->value }}">
                    </a>
                @elseif(empty($back) && isset($currentCategory))
                    <a 
                        href="{{ route('shop.product.index', ['user' => $seller->id, 'localisation' => $seller->currentLocalisation]) }}"
                        class="catalog__filterTop__back" 
                        title="Все товары поставщика">
                    </a>
                @endif
                
                @include ('shop.index._sorting', ['interface' => $interface])
                
            </div>
            
			<div class="catalog__content">
				<div class="catalog__content__left">

                @include ('shop.index._filters', ['interface' => $interface])
                    
				</div>
				<div class="catalog__content__right">
					<div class="catalog__products">
                        @php
                            $currency = $currencyRates->where('code', $currentCurrencyCode)->first();
                        @endphp
                        @foreach($products as $product)
                        <div class="store__catalog__item"> 
                            <a href="{{ route('catalog.product.show', ['product' => $product->id]) }}" class="store__catalog__item__image"  style="background-image: url('{{ asset('storage/' . $product->downloads->where('type', 'preview_image')->first()->path) }}');">
                                
                                @guest 
                                <form method="POST" class="favorite-add-form" action="{{ route('favorite.add', ['type' => 'product', 'id' => $product->id]) }}">
                                    @csrf
                                    <div class="store__catalog__item__favorite" title="{{ $interface['add_favorites']->value }}"></div>
                                </form>
                                @endguest
                                
                                @auth
                                    @if( $product->favorites->where('user_id', auth()->user()->id)->first() )
                                        <div class="store__catalog__item__favorite active favorite-remove" data-route="{{ route('favorite.remove', ['type' => 'product', 'id' => $product->id]) }}" title="{{ $interface['remove_favorites']->value }}"></div>
                                    @else
                                        <div class="store__catalog__item__favorite favorite-add" data-route="{{ route('favorite.add', ['type' => 'product', 'id' => $product->id]) }}" title="{{ $interface['add_favorites']->value }}"></div>
                                    @endif
                                @endauth
         
                            </a>
                            <div class="store__catalog__item__title">{{ $product->productProperties->where('code', 'name')->first()->value ?? '' }}</div>
                            <div class="store__catalog__item__price">
                                {{$currency->symbol}}{{ $product->productPrices->first()->min * $currency->rate }} 
                                {{ $product->productPrices->first()->max * $currency->rate ? ' — '.$currency->symbol . $product->productPrices->first()->max * $currency->rate : '' }}
                            </div>
                            @if(!empty($product->characteristics['min_order']))
                            <div class="store__catalog__item__minOrder">
                                {{ $product->characteristics['min_order']['value'] }} 
                                {{ $product->characteristics['min_order']['type'] }}
                                <span>({{ $interface['min_order']->value }})</span>
                            </div>
                            @endif
                        </div>
                        @endforeach
					</div>
                    
                    {{ $products->render('widgets.pagination') }}

				</div>
			</div>
		</div>
	</div>
</div>
@endsection

@section('content-script')
<script src="{{ mix('js/seller_product.js', 'assemble') }}"></script>       
@endsection