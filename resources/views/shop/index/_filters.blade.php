@isset($categories)
<div class="catalog__filter catalog__categories">
    @foreach($categories as $category)
    <div class="catalog__categories__block">
        <div class="catalog__categories__block__title">
            <a href="{{ $category->shop_link }}">{{ $category->categoryProperties->where('code', 'name')->first()->value }}</a>
        </div>
    </div>
    @endforeach
</div> 
@endisset
