@extends('layouts.market.index')

@if(View::hasSection('title'))
    @section('title')
        @yield('title')
    @endsection
@endif

@section('scripts')
    <script src="{{ mix('js/seller.js', 'assemble') }}"></script>
    @yield('content-script')
@endsection