<div class="store__navigation">
    <div class="container">
        <nav class="store__navigation__links">
            <ul>
                <li>
                    <a href="{{ route('shop.show', ['user' => $seller->id, 'localisation' => $seller->currentLocalisation]) }}" class="store__navigation__links__link{{ $page === 'main' ? ' store__navigation__links__link-active' : '' }}">{{ $interface['main_page']->value }}
                    </a>
                </li>
                <li><a href="{{ route('shop.product.index', ['user' => $seller->id, 'localisation' => $seller->currentLocalisation]) }}" class="store__navigation__links__link{{ $page === 'product' ? ' store__navigation__links__link-active' : '' }}">{{ $interface['products']->value }}</a></li>
                <li><a href="{{ route('shop.contact.show', ['user' => $seller->id, 'localisation' => $seller->currentLocalisation]) }}" class="store__navigation__links__link{{ $page === 'contact' ? ' store__navigation__links__link-active' : '' }}">{{ $interface['contacts']->value }}</a></li>
            </ul>
        </nav>
    </div>	
</div>
    
   
