<form action="{{ $action }}" method="get">
    <div class="store__header__search">
        <input 
            type="text" 
            name="seller_search" 
            class="store__header__search__input" 
            placeholder="{{ $interface['find_products']->value }}"
            value="{{ request('seller_search') }}"
        >
        <button id="seller-search" type="{{ $type }}" class="button button-primary store__header__search__button">{{ $interface['find']->value }}</button>
    </div>
</form>