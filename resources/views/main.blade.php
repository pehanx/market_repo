@extends('layouts.market.index')

@section('content')
<div class="mainpage__whatIsMarket">
	<div class="container">
		<div class="mainpage__whatIsMarket__items">
			<div class="mainpage__whatIsMarket__item">
				<div class="mainpage__whatIsMarket__item__content">
					<div class="mainpage__whatIsMarket__item__title">{{ __('interface.main.what_is_market') }}</div> 
					<p class="mainpage__whatIsMarket__item__description">{{ __('interface.main.what_is_market_description') }}.</p>
					<p class="mainpage__whatIsMarket__item__description"><strong>{{ __('interface.main.what_is_market_integration') }}.</strong></p> 
				</div>
			</div>
			<div class="mainpage__whatIsMarket__item">
				<div class="mainpage__whatIsMarket__item__icon"></div>
			</div>
		</div>
	</div>
</div>
<div class="mainpage__howItWork">
	<div class="container">
		<h2 class="mainpage__howItWork__title">{{ __('interface.main.how_it_works') }}</h2>
		<div class="mainpage__howItWork__items">
			<div class="mainpage__howItWork__item">
				<div class="mainpage__howItWork__item__icon"></div>
				<div class="mainpage__howItWork__item__content">
					<div class="mainpage__howItWork__item__title">{{ __('interface.main.register') }}</div>
					<p class="mainpage__howItWork__item__description">{{ __('interface.main.you_can') }}</p>
					@guest <a href="{{route('register')}}" class="mainpage__howItWork__item__link">{{ __('interface.buttons.register') }} →</a> @endguest
				</div>
			</div>
			<div class="mainpage__howItWork__item">
				<div class="mainpage__howItWork__item__icon"></div>
				<div class="mainpage__howItWork__item__content">
					<div class="mainpage__howItWork__item__title">{{ __('interface.main.place_product_or_request') }}</div>
					<p class="mainpage__howItWork__item__description">{{ __('interface.main.you_can') }}</p>
				</div>
			</div>
			<div class="mainpage__howItWork__item">
				<div class="mainpage__howItWork__item__icon"></div>
				<div class="mainpage__howItWork__item__content">
					<div class="mainpage__howItWork__item__title">{{ __('interface.main.deal_agree') }}</div>
					<p class="mainpage__howItWork__item__description">{{ __('interface.main.you_can') }}</p>
				</div>
			</div>
			<div class="mainpage__howItWork__item">
				<div class="mainpage__howItWork__item__icon"></div>
				<div class="mainpage__howItWork__item__content">
					<div class="mainpage__howItWork__item__title">{{ __('interface.main.wait_product') }}</div>
					<p class="mainpage__howItWork__item__description">{{ __('interface.main.you_can') }}</p>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="mainpage__categories">
	<div class="container">
		<h2 class="mainpage__categories__title">{{ __('interface.main.popular_categories') }}</h2>
		<div class="mainpage__categories__items">
			@foreach ($categoriesWithImages as $category)
				<div class="mainpage__categories__item">
					<a href="{{ $category->catalog_link }}">
						<div class="mainpage__categories__item__image" style="background-image: url('{{ asset('assemble' . $category->banner) }}');"></div>
						<div class="mainpage__categories__item__title">{{ $category->title }}</div>
					</a>
				</div>
			@endforeach
		</div>
	</div>
</div>
@endsection
