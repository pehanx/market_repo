{{--
<div><a href="{{ $pageDescendant->link }}" @if(('/' . request()->path()) ===  $pageDescendant->link) class="active" @endif> @php echo str_repeat('*', $loop->depth); @endphp {{ $pageDescendant->title }}</a></div>
@if($pageDescendant->children->first())
    @foreach($pageDescendant->children as $key => $pageChildren)
        @include ('pages._nav', ['pageDescendant' => $pageChildren])
    @endforeach
@endif
--}}

@if($pageDescendant->children->first())
<div class="{{ ($loop->depth > 1) ? '' : 'static__sidebar__list' }}">
    <div class="{{ ($loop->depth > 1) ? 'static__sidebar__list__ul__title' : 'static__sidebar__list__title' }}">{{ $pageDescendant->title }}</div>
    <ul class="{{ ($loop->depth > 1) ? 'static__sidebar__list__ul__sublist' : 'static__sidebar__list__ul' }}">
        @foreach($pageDescendant->children as $key => $pageChildren)
            @include ('pages._nav', ['pageDescendant' => $pageChildren])
        @endforeach
    </ul>
</div>
@elseif($loop->depth > 1)
<li>
    <a class="static__sidebar__list__ul__link" href="{{ $pageDescendant->link }}">
        {{ $pageDescendant->title }} 
    </a>
</li>
@else
<a class="static__sidebar__link" href="{{ $pageDescendant->link }}">
    {{ $pageDescendant->title }} 
</a>
@endif