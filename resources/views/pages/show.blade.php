@extends('layouts.market.index')

@section('meta')
    <meta name="description" content="{{ $page->pageProperties->where('code', 'seo_descriptions')->first()->value ?? '' }}">
    <meta name="title" content="{{ $page->pageProperties->where('code', 'seo_title')->first()->value ?? '' }}">
@endsection

{{--  
@section('content')

    <div class="col-12">
        @section('breadcrumbs', Breadcrumbs::render())
        @yield('breadcrumbs')
    </div>

    <div class="col-12 border-bottom">
        <h2>{{ $page->title }}</h2>
    </div>

    @if(!empty($pageDescendants))
        
        <div class="col-3">
        @foreach($pageDescendants as $pageDescendant)
            @include ('pages._nav', ['pageDescendant' => $pageDescendant])
        @endforeach
        </div>
        
        <div class="col-9">
        {!! clean( $page->pageProperties->where('code', 'content')->first()->value ) !!}
        </div>
        
    @else
        
        <div class="col-12">
        {!! clean( $page->pageProperties->where('code', 'content')->first()->value ) !!}
        </div>
        
    @endif
    
@endsection
--}}

@section('content')
<div class="static">
	<div class="container">
		@section('breadcrumbs', Breadcrumbs::render())
        @yield('breadcrumbs')
		<h1 class="static__title">{{ $page->title }}</h1>
		<div class="static__wrapper">
            @if(!empty($pageDescendants))
                <div class="static__sidebar">
                    <div class="static__sidebar__block">
                        <div class="static__sidebar__block__content">
                            @foreach($pageDescendants as $pageDescendant)
                                @include ('pages._nav', ['pageDescendant' => $pageDescendant])
                            @endforeach
                        </div>
                    </div>
                </div>
            @endif
			<div class="static__content">
                
                {!! clean( $page->pageProperties->where('code', 'content')->first()->value ) !!}
                
                {{--
                <h2>Выгрузка прайс-листа на Маркет из Microsoft Excel</h2>
				<h3>ОС Windows 7 и 10</h3>
				<p>При работе в Windows 7 и 10 прайс-лист можно выгрузить на Маркет одним из способов:</p>
				<ul>
					<li>напрямую из программы Microsoft Excel;</li>
					<li>в <a href="#">личном кабинете</a> Маркета.</li>
				</ul>
				<h3>ОС Windows 7 и 10</h3>
				<p>При работе в других операционных системах прайс-лист можно загрузить только в личном кабинете Маркета</p>
				<hr>
				<h2>Выгрузка прайс-листа на Маркет напрямую из Microsoft Excel</h2>
				<p><strong>Шаг 1. Получите данные для авторизации</strong></p>
				<p class="star">Данные для авторизации — это ключ и номер кампании, их выдает Маркет.</p>
				<ol>
					<li>Перейдите в личном кабинете на страницу <strong>Ассортимент → Прайс-лист</strong> и нажмите кнопку <strong>Добавить прайс-лист</strong></li>
					<li>Откроется окно <strong>Добавление прайс-листа</strong>. Выберите опцию <strong>Макрос в Excel</strong></li>
					<li>Нажмите ссылку <strong>Получить ключ.</strong> Откроется страница сервиса <strong>Яндекс.Oauth</strong></li>
					<li>Нажмите кнопку <strong>Разрешить</strong>. Вам будет выдан ключ, затем откроется страница <strong>личного кабинета Макрета.</strong></li>
					<li>Скопируйте ключ и номер кампании.</li>
				</ol>
				<img src="dist/assets/img/temporary/static-page-image-1.jpg">
				<p><strong>Шаг 2. Выгрузите прайс-лист</strong></p>
				<ol>
					<li>Выберите <strong>Подстройки</strong> на верхней панели шаблона Excel и нажмите кнопку <strong>Выгрузить товары</strong><br><img src="dist/assets/img/temporary/static-page-image-1.jpg" alt=""><br>* Если есть ошибки, появится предупреждение об этом. Исправьте ошибки и повторите действие</li>
					<li>Откроется окно для ввода данных для авторизации:
						<ul>
							<li>При первой выгрузке прайс-листа нужно ввести ключ, номер кампании и придумать пароль</li>
							<li>При повторной выгрузке нужно ввести только пароль</li>
						</ul>
					</li>
				</ol>
				<hr>
				<h2>Заголовок</h2>
				<p>Дедуктивный метод решительно представляет собой бабувизм. Согласно мнению известных философов, дедуктивный метод естественно порождает и обеспечивает мир, tertium nоn datur. Созерцание непредсказуемо. Структурализм абстрактен. Отсюда естественно следует, что автоматизация дискредитирует предмет деятельности. Надстройка нет</p>
				<p>Интеллект естественно понимает под собой интеллигибельный закон внешнего мира, открывая новые горизонты. Дискретность амбивалентно транспонирует гравитационный парадокс. Сомнение рефлектирует естественный закон исключённого третьего.. Согласно мнению известных философов, дедуктивный метод естественно порождает и обеспечивает мир, terti</p>
				<img src="dist/assets/img/temporary/static-page-image-3.jpg" alt="">
                --}}
			</div>
		</div>
	</div>
</div>
@endsection