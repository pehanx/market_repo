@extends('layouts.admin')

@section('sidebar')
    @include ('admin.sidebar', ['page' => ''])
@endsection

@section('scripts')
    <script src="{{ mix('js/Cabinet.js', 'assemble') }}"></script>
    <script>
        let cabinet = new Cabinet ();
    </script>
    @yield('content-script')
@endsection

