@include ('admin.sidebar', ['page' => 'profiles'])

<div class="left-sidebar-child shadow-sm bg-white">
    <ul class="nav flex-column">
        <li class="nav-item">
            <a href="{{ route('admin.profiles.index') }}" class="nav-link {{ $page === 'profiles' ? ' active shadow-sm' : '' }}">
                {{ __('sidebars.profiles') }}
            </a>
        </li>
    </ul>
</div>
