@extends('admin.home')

@section('sidebar')
    @include ('admin.profiles.sidebar', ['page' => 'profiles'])
@endsection
  
@section('content')
<h2 class="mb-3">{{ __('admin.profiles') }}</h2>

<div class="card mb-3 py-0 rounded-0">
    <div class="card-body py-0">
        <form action="?" method="GET" class="mb-0">
            <div class="row">
                <div class="col-auto">
                    <div class="form-group">
                        <label for="login" class="col-form-label">{{ __('tables.headers.login') }}</label>
                        <input id="login" class="form-control rounded-0" name="login" value="{{ request('login') }}">
                    </div>
                </div>
                 <div class="col-auto">
                    <div class="form-group">
                        <label for="name" class="col-form-label">{{ __('tables.headers.profile') }}</label>
                        <input id="name" class="form-control rounded-0" name="name" value="{{ request('name') }}">
                    </div>
                </div>
                
                <div class="col-auto">
                    <div class="form-group">
                        <label for="type" class="col-form-label">{{ __('tables.headers.type') }}</label>
                        <select id="type" class="form-control rounded-0" name="type">
                            <option value="">{{ __('admin.all') }}</option>
                            @foreach ($profileTypes as $profileType)
                                <option {{ (request('type') === $profileType->code)? 'selected' : '' }} value="{{ $profileType->code }}"> {{ __('admin.'.$profileType->code) }} </option>
                            @endforeach
                        </select>
                    </div>
                </div>
                
                <div class="col-auto">
                    <div class="form-group">
                        <label for="status" class="col-form-label">{{ __('tables.headers.status') }}</label>
                        <select id="status" class="form-control rounded-0" name="status">
                            <option value="">{{ __('admin.no_matter') }}</option>
                            <option {{ (request('status') == 'active')? 'selected' : '' }} value="active">{{ __('tables.data.approved') }}</option>
                            <option {{ (request('status') == 'inactive')? 'selected' : '' }} value="inactive">{{ __('tables.data.not_approved') }}</option>
                            <option {{ (request('status') == 'wait')? 'selected' : '' }} value="wait">{{ __('tables.data.wait_approve') }}</option>
                        </select>
                    </div>
                </div>
                
                <div class="col-auto">
                    <div class="form-group">
                        <label class="col-form-label">&nbsp;</label><br />
                        <button type="submit" class="btn btn-primary" title = "{{ __('admin.search') }}">
                            <i class="fa fa-search" aria-hidden="true"></i>
                        </button>
                        <a href="{{ route('admin.profiles.index') }}" class="btn btn-primary text-white" title="{{ __('admin.clear_filters') }}">
                            <i class="fa fa-refresh" aria-hidden="true"></i>
                        </a>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>

<div class="table-div">
    <table class="table table-bordered table-sm text-left">
        <thead>
        <tr>
            <th>{{ __('tables.headers.login') }}</th>
            <th>{{ __('tables.headers.profile') }}</th>
            <th>{{ __('tables.headers.type') }}</th>
            <th>{{ __('tables.headers.status') }}</th>
            <th>{{ __('tables.headers.actions') }}</th>
        </tr>
        </thead>
        <tbody>
        
        @foreach ($users as $key => $user)
            <tr>
                <td @if($user->profiles->first()) rowspan="{{ $user->profiles->count() }}" class="align-middle" @endif >{{ $user->name }}</td>
                @if($user->profiles->first())
                    @foreach ($user->profiles as $keyProfile => $profile)
                        @if($profile->profileProperties)
                            <td>{{ $nameCompany = $profile->profileProperties->where('code', 'company_name')->first()->value }}</td>
                        @endif
                        
                        @if($profile->profileTypes)
                        <td>{{  __('tables.data.'.$profile->profileTypes->code) }}</td>
                        @endif
                        
                        @if ($profile->status === 'active')
                        <td><span class="badge badge-pill badge-primary mb-1">{{ __('tables.data.approved') }}</span></td>
                        @elseif($profile->status === 'inactive')
                        <td><span class="badge badge-pill badge-danger mb-1">{{ __('tables.data.not_approved') }}</span></td>
                        @elseif($profile->status === 'wait')
                        <td><span class="badge badge-pill badge-warning mb-1">{{ __('tables.data.wait_approve') }}</span></td>
                        @endif
                        
                        <td> 
                            <a href="" class="">
                                <span class="badge badge-pill badge-primary mb-1">{{ __('tables.data.change') }}</span>
                            </a>
                            <a href="{{ route('admin.chat.user',$user->id) }}" target="blank" class="">
                                <span class="badge badge-pill badge-primary mb-1">{{ __('tables.data.send_message') }}</span>
                            </a>
                            
                            @if ($profile->status === 'inactive' || $profile->status === 'wait')
                            <button type="button" class="approve-profile-btn px-0" data-text="{{ __('modals.your_sure_approve_profile').' '.$nameCompany.' ?' }}" data-route="{{ route('admin.profiles.approval', $profile->id) }}" >
                                <span class="badge badge-pill badge-primary mb-1">{{ __('tables.data.approve') }}</span>
                            </button> 
                            @else
                            <button type="button" class="disable-profile-btn px-0" data-text="{{ __('modals.your_sure_disable_profile').' '.$nameCompany.' ?' }}" data-route="{{ route('admin.profiles.disable', $profile->id) }}" >
                                <span class="badge badge-pill badge-danger mb-1">{{ __('tables.data.disable') }}</span>
                            </button> 
                            @endif

                            <button type="button" class="delete-profile-btn px-0" data-text="{{ __('modals.your_sure_destroy_profile').' '.$nameCompany.' ?' }}" data-route="{{ route('admin.profiles.destroy', $profile->id) }}" >
                                <span class="badge badge-pill badge-danger mb-1">{{ __('tables.data.delete') }}</span>
                            </button> 
                            
                        </td>
                    </tr>
                        @if( $keyProfile+1 !== $user->profiles->count() )
                            <tr> 
                        @endif
                    @endforeach
                @else
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                @endif
            </tr>
        @endforeach

        </tbody>
    </table>
     
    <div class="row justify-content-center mt-5">
    {{ $users->links() }}
    </div>
</div>

@include('modals.destroy')
@include('modals.approve')
@include('modals.disable')

@endsection

@section('content-script')
<script>
    document.addEventListener('DOMContentLoaded', function() {
        cabinet.init('.delete-profile-btn', 'click', 'deleteProfile');
        cabinet.init('.approve-profile-btn', 'click', 'approveProfile');
        cabinet.init('.disable-profile-btn', 'click', 'disableProfile');
    });
</script>
@endsection