@extends('layouts.admin')

@section('sidebar')
    @include ('admin.users.sidebar', ['page' => 'users'])
@endsection
  
@section('content')

<h2>{{ __('sidebars.users') }}</h2>

<div class="table-div">
    <table id="mainTable" data-route="{{route('admin.role.add')}}" class="table table-bordered table-sm">
        <thead>
        <tr>
            <th>ID</th>
            <th>{{ __('tables.headers.login') }}</th>
            <th>{{ __('tables.headers.email') }}</th>
            <th>{{ __('tables.headers.roles') }}</th>
            <th>{{ __('tables.headers.registration_type') }}</th>
            <th>{{ __('tables.headers.actions') }}</th>
        </tr>
        </thead>
        <tbody>
        
        @foreach ($users as $key => $user)
            <tr>
                <td>{{ $user->id }}</td>
                <td>{{ $user->name }}</td>
                <td>{{ $user->email }}</td>
                <td> {{ ( count($user->getRoleNames()) !== 0)? $user->getRoleNames()->implode(', ') : '' }} <button data-user="{{$user->id}}" id="addRole">+</button></td>
                <td>{{ __('tables.data.'.$user->registration_type) }}</td>
                <td></td>
            </tr>
        @endforeach
        </tbody>
    </table>
     
    <div class="row justify-content-center mt-5">
    {{ $users->links() }}
    </div>
</div>

@endsection

@section('scripts')
    <script>
        $('#addRole').click(function(){

            let user = $(this).data('user');
            let role = 'site_manager';

            axios
            .post($('#mainTable').data('route'), {user: user, role: role}, {

            }).then(response => {
                location.reload();
            });

            
        })
    </script>
@endsection