@include ('admin.sidebar', ['page' => 'users'])

<div class="left-sidebar-child shadow-sm bg-white">
    <ul class="nav flex-column">
        <li class="nav-item">
            <a href="{{ route('admin.users.index') }}" class="nav-link{{ $page === 'users' ? ' active shadow-sm' : '' }}">
                {{ __('sidebars.users') }}
            </a>
        </li>
    </ul>
</div>