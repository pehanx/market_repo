@extends('layouts.app')

@section('content')

    <table class="table table-bordered table-striped">
        <thead>
        <tr>
            <th>ParentName</th>
            <th>Name</th>
        </tr>
        
        </thead>
        <tbody>

        @foreach ($categories as $category)
            <tr>
                <td>{{ $category->depth }}</td>
                <td>{{ $category->link }}</td>
            </tr>
        @endforeach

        </tbody>
    </table>

@endsection