<div class="left-sidebar-main shadow-sm bg-white">
    <ul class="nav flex-column py-3">
        <li class="nav-item">
            <a href="{{ route('admin.users.index') }}" class="nav-link{{ $page === 'users' ? ' active' : '' }}">
                <i class="fa fa-users mr-3 fa-2x"></i>
            </a>
        </li>
        <li class="nav-item">
            <a href="{{ route('admin.profiles.index') }}" class="nav-link{{ $page === 'profiles' ? ' active' : '' }}">
                <i class="fa fa-id-card-o mr-3 fa-2x"></i>
            </a>
        </li>
        
        <li class="nav-item">
            <a href="{{ route('admin.pages.index') }}" class="nav-link{{ $page === 'pages' ? ' active' : '' }}">
                <i class="fa fa-files-o mr-3 fa-2x"></i>
            </a>
        </li>
        
        {{--
        <li class="nav-item">
            <a href="" class="nav-link">
                <i class="fa fa-wrench mr-3 fa-2x"></i>
            </a>
        </li>
        --}}
    </ul>
</div>
 


