@include ('admin.sidebar', ['page' => 'page'])

<div class="left-sidebar-child shadow-sm bg-white">
    <ul class="nav flex-column">
        <li class="nav-item">
            <a href="{{ route('admin.pages.index') }}" class="nav-link{{ $page === 'page' ? ' active shadow-sm' : '' }}">
                {{ __('sidebars.pages') }}
            </a>
        </li>
    </ul>
</div>