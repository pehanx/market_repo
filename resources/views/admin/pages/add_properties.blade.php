@extends('layouts.admin')

@section('sidebar')
    @include ('admin.pages.sidebar', ['page' => 'page'])
@endsection
  
@section('content')

<div class="d-flex justify-content-start m-0 align-items-center">
    <h2>Описание страницы: ({{ $page->id }})</h2>
</div>

@if($page->pageProperties->first())
    <div class="overflow-auto h-25">
        @foreach($page->pageProperties as $localisationCode => $properties)
            <div class="border px-3 py-3 my-3">
                <h2>{{ $properties->where('code', 'title')->first()->value . ' (' . $localisationCode . ')'}}</h2>
                
                @foreach($properties as $property)    
                <div class="">
                    @if($property->code === 'content')
                            {!! clean($property->value) !!}
                        @break
                    @endif
                </div>
                @endforeach
            </div>
        @endforeach
   </div>
@endif

<h2>Добавление характеристик для страницы</h2>

<form method="POST" action="{{ route('admin.pages.store_properties', ['page' => $page]) }}">
    @csrf
    <div class="form-group">
        <div class="form-group row">
            <div class="col-6 col-md-3">
                Символьный код:
            </div>
            <div class="col-6 col-md-6">
                {{ $page->link }}
            </div>
        </div> 
    </div>
    
    @if($parent)
    <div class="form-group">
        <div class="form-group row">
            <div class="col-6 col-md-3">
                Родительская страница:
            </div>
            <div class="col-6 col-md-6">
                {{ $parent->title }}
            </div>
         </div> 
    </div>
    @endif
    
    <div class="form-group">
        <label for="localisation" class="col-form-label">Локализация сайта</label>
        
        <select class="form-control{{ $errors->has('localisation') ? ' is-invalid' : '' }} select2" name="localisation">
            @foreach ($localisations as $localisation)
                <option value="{{ $localisation->id }}"{{ $localisation->id == old('localisation_id') ? ' selected' : '' }}>
                    {{ __("interface.localisation." . $localisation->code) }}
                </option>
            @endforeach
            
        </select>
        @if ($errors->has('localisation'))
            <span class="invalid-feedback"><strong>{{ $errors->first('localisation') }}</strong></span>
        @endif
    </div>

    <div class="form-group">
        <label for="title" class="col-form-label">Название страницы</label>
        <input id="title" class="form-control{{ $errors->has('title') ? ' is-invalid' : '' }}" name="title" value="{{ old('title') }}" required>
        @if ($errors->has('title'))
            <span class="invalid-feedback"><strong>{{ $errors->first('title') }}</strong></span>
        @endif
    </div>

    <div class="form-group">
        <label for="menu_title" class="col-form-label">Название страницы (для отображения в меню)</label>
        <input id="menu_title" class="form-control{{ $errors->has('menu_title') ? ' is-invalid' : '' }}" name="menu_title" value="{{ old('menu_title') }}">
        @if ($errors->has('menu_title'))
            <span class="invalid-feedback"><strong>{{ $errors->first('menu_title') }}</strong></span>
        @endif
    </div>
    
    <div class="form-group row">
        <label for="example-phone-input" class="col-12 col-md-4 col-form-label">Seo title</label>
        <div class="col-12 col-md-12">
            <input class="form-control" placeholder="" name="seo_title" type="text" value="{{ old('seo_title') }}" >
        </div>
    </div>
        
    <div class="form-group row">
        <label for="example-phone-input" class="col-12 col-md-4 col-form-label">Seo keywords</label>
        <div class="col-12 col-md-12">
            <input class="form-control" placeholder="" name="seo_keywords" type="text" value="{{ old('seo_keywords') }}" >
        </div>
    </div>
    
    <div class="form-group row">
        <label for="example-phone-input" class="col-12 col-md-4 col-form-label">Seo description</label>
        <div class="col-12 col-md-12">
            <textarea name="seo_descriptions" class="w-100" rows="10">{{ old('seo_descriptions') }}</textarea>
        </div>
    </div>

    <div class="form-group">
        <label for="content" class="col-form-label">Контент страницы</label>
        <textarea id="content" class="form-control{{ $errors->has('content') ? ' is-invalid' : '' }} summernote" data-route="{{ route('upload.image', ['type' => 'page']) }}" name="content" rows="10">{{ old('content') }}</textarea>
        @if ($errors->has('content'))
            <span class="invalid-feedback"><strong>{{ $errors->first('content') }}</strong></span>
        @endif
    </div>

    <div class="form-group">
       
    </div>

    <div class="form-group">
        <button type="submit" class="btn btn-primary">Добавить</button>
    </div>
</form>
@endsection

@section('scripts')
<script src="{{ mix('js/api/SummernoteCallbacks.js', 'assemble') }}"></script>
<script>
    document.addEventListener('DOMContentLoaded', function() {
        $('.select2').select2(); 
        
        $('.summernote').summernote({
            height: 500,
            lang: 'ru-RU',
            callbacks: {
                onImageUpload: SummernoteCallbacks.onImageUpload,
                onMediaDelete: SummernoteCallbacks.onMediaDelete
            } 
        });
    });
</script>
@endsection