@extends('admin.home')

@section('sidebar')
    @include ('admin.pages.sidebar', ['page' => 'page'])
@endsection
  
@section('content')

<div class="d-flex justify-content-between m-0 align-items-center">
    <div class="d-flex flex-column justify-content-start">
        <h2>Просмотр страницы ({{ $page->id }})</h2>
        <a href="{{ $link }}">{{ $link }}</a>
    </div>
    <div class="d-flex justify-content-between m-0 align-items-center">
        <a href="{{ route('admin.pages.edit', $page) }}" class="btn btn-primary mr-1">Редактировать страницу</a>
        @if($page->is_active)
        <button type="button" class="disable-page-btn btn btn-secondary" data-text="{{ __('modals.your_sure_disable', ['item' => $page->id]) }}" data-route="{{ route('admin.pages.disable', $page) }}" >
            Отключить
        </button> 
        @else
        <a href="{{ route('admin.pages.activate', $page) }}" class="btn btn-success mr-1">Активировать</a>
        @endif
        <button type="button" class="delete-page-btn btn btn-danger ml-2" data-text="{{ __('modals.your_sure_destroy', ['item' => $page->id]) }}" data-route="{{ route('admin.pages.delete', $page) }}" >
            Удалить
        </button> 
    </div>
</div>

@if($page->pageProperties->first())
    <div class="h-25">
        @foreach($page->pageProperties as $localisationCode => $properties)
            <div class="d-flex flex-column border px-3 py-3 my-3">
                <h2>{{ $properties->where('code', 'title')->first()->value . ' (' . $localisationCode . ')'}}</h2>
                
                @foreach($properties as $property)    
                <div class="">
                    @if($property->code === 'content')
                        {!! clean($property->value) !!}
                    @endif
                </div>
                @endforeach
                
                <div class="d-flex flex-row mb-3">
                    <a href="{{ route('admin.pages.edit.properties', ['page' => $page, 'localisation' => $localisationCode]) }}" class="btn btn-primary mr-1">Редактировать описание</a>
                    <button type="button" class="delete-page-btn btn btn-danger ml-2" data-text="{{ __('modals.your_sure_destroy', ['item' => $properties->where('code', 'title')->first()->value]) }}" data-route="{{ route('admin.pages.delete.properties', ['page' => $page, 'localisation' => $localisationCode]) }}" >
                        Удалить
                    </button> 
                </div>
                
            </div>
        @endforeach
   </div>
@endif

@if($addMoreProperties)
<div class="d-flex justify-content-between mb-5 align-items-center">
    <a href="{{ route('admin.pages.add_properties', $page) }}" class="btn btn-secondary mr-1">Добавить описание на другом языке</a>
</div>
@endif

@include('modals.destroy')
@include('modals.disable')

@endsection

@section('content-script')
<script>
    document.addEventListener('DOMContentLoaded', function() {
        cabinet.init('.delete-page-btn', 'click', 'deletePage');
        cabinet.init('.disable-page-btn', 'click', 'disablePage');
    });
</script>
@endsection