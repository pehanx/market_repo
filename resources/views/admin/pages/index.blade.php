@extends('admin.home')

@section('sidebar')
    @include ('admin.pages.sidebar', ['page' => 'page'])
@endsection
  
@section('content')

<div class="row justify-content-between m-0 align-items-center">
    <h2>{{ __('sidebars.pages') }}</h2>
    <a href="{{ route('admin.pages.create') }}" class="btn btn-primary"><i class="fa fa-plus" aria-hidden="true"></i></a>
</div>

<div class="table-div">
      <table class="table table-bordered table-striped">
        <thead>
        <tr>
            <th></th>
            <th>Название</th>
            <th>Символьный код</th>
            <th>Переместить</th>
        </tr>
        </thead>
        <tbody>

        @foreach ($pages as $page)
            <tr>
                <td>
                    @for ($i = 0; $i < $page->depth; $i++) &mdash; @endfor
                    <a href="{{ route('admin.pages.show', $page) }}">{{ $page->title }}</a>
                </td>
                <td>{{ $page->title }}</td>
                <td>{{ $page->link }}</td>
                <td>
                    <div class="d-flex flex-row">
                        <form method="POST" action="{{ route('admin.pages.first', $page) }}" class="mr-1">
                            @csrf
                            <button class="btn btn-sm btn-outline-primary"><span class="fa fa-angle-double-up"></span></button>
                        </form>
                        <form method="POST" action="{{ route('admin.pages.up', $page) }}" class="mr-1">
                            @csrf
                            <button class="btn btn-sm btn-outline-primary"><span class="fa fa-angle-up"></span></button>
                        </form>
                        <form method="POST" action="{{ route('admin.pages.down', $page) }}" class="mr-1">
                            @csrf
                            <button class="btn btn-sm btn-outline-primary"><span class="fa fa-angle-down"></span></button>
                        </form>
                        <form method="POST" action="{{ route('admin.pages.last', $page) }}" class="mr-1">
                            @csrf
                            <button class="btn btn-sm btn-outline-primary"><span class="fa fa-angle-double-down"></span></span></button>
                        </form>
                    </div>
                </td>
            </tr>
        @endforeach

        </tbody>
    </table>
     
    <div class="row justify-content-center my-5">
    {{ $pages->links() }}
    </div>
</div>

@endsection