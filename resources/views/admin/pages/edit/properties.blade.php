@extends('admin.home')

@section('sidebar')
    @include ('admin.pages.sidebar')
@endsection
  
@section('content')

<h2>Редактирование описания страницы: {{ $page->title }}</h2><br>

<form method="POST" action="{{ route('admin.pages.update.properties', ['page' => $page, 'localisation' => $localisation->code]) }}">
    @csrf
    <div class="form-group">
        <div class="form-group row">
            <div class="col-6 col-md-3">
                Символьный код:
            </div>
            <div class="col-6 col-md-6">
                {{ $page->link }}
            </div>
        </div> 
    </div>
    
    @if($parent)
    <div class="form-group">
        <div class="form-group row">
            <div class="col-6 col-md-3">
                Родительская страница:
            </div>
            <div class="col-6 col-md-6">
                {{ $parent->title }}
            </div>
         </div> 
    </div>
    @endif
    
    <div class="form-group">
        <div class="form-group row">
            <div class="col-6 col-md-3">
                Локализация сайта
            </div>
            <div class="col-6 col-md-6">
                {{ $localisation->code }}
            </div>
        </div> 
    </div>

    <div class="form-group">
        <label for="title" class="col-form-label">Название страницы</label>
        <input id="title" class="form-control{{ $errors->has('title') ? ' is-invalid' : '' }}" name="title" value="{{ old('title', $page->pageProperties->where('code', 'title')->first()->value ?? '') }}" required>
        @if ($errors->has('title'))
            <span class="invalid-feedback"><strong>{{ $errors->first('title') }}</strong></span>
        @endif
    </div>

    <div class="form-group">
        <label for="menu_title" class="col-form-label">Название страницы (для отображения в меню)</label>
        <input id="menu_title" class="form-control{{ $errors->has('menu_title') ? ' is-invalid' : '' }}" name="menu_title" value="{{ old('menu_title', $page->pageProperties->where('code', 'menu_title')->first()->value ?? '') }}">
        @if ($errors->has('menu_title'))
            <span class="invalid-feedback"><strong>{{ $errors->first('menu_title') }}</strong></span>
        @endif
    </div>
    
    <div class="form-group row">
        <label for="example-phone-input" class="col-12 col-md-4 col-form-label">Seo title</label>
        <div class="col-12 col-md-12">
            <input class="form-control" placeholder="" name="seo_title" type="text" value="{{ old('seo_title', $page->pageProperties->where('code', 'seo_title')->first()->value ?? '') }}" >
        </div>
    </div>
        
    <div class="form-group row">
        <label for="example-phone-input" class="col-12 col-md-4 col-form-label">Seo keywords</label>
        <div class="col-12 col-md-12">
            <input class="form-control" placeholder="" name="seo_keywords" type="text" value="{{ old('seo_keywords', $page->pageProperties->where('code', 'seo_keywords')->first()->value ?? '') }}" >
        </div>
    </div>
    
    <div class="form-group row">
        <label for="example-phone-input" class="col-12 col-md-4 col-form-label">Seo description</label>
        <div class="col-12 col-md-12">
            <textarea name="seo_descriptions" class="w-100" rows="10">{{ old('seo_descriptions', $page->pageProperties->where('code', 'seo_descriptions')->first()->value ?? '') }}</textarea>
        </div>
    </div>

    <div class="form-group">
        <label for="content" class="col-form-label">Контент страницы</label>
        <textarea id="content" class="form-control{{ $errors->has('content') ? ' is-invalid' : '' }} summernote" data-route="{{ route('upload.image', ['type' => 'page']) }}" name="content" rows="10">
            {{ old('content') }}
        </textarea>
        @if ($errors->has('content'))
            <span class="invalid-feedback"><strong>{{ $errors->first('content') }}</strong></span>
        @endif
    </div>

    <div class="form-group mb-5">
        <button type="submit" class="btn btn-primary">Сохранить изменения</button>
    </div>
</form>
@endsection

@section('scripts')
<script src="{{ mix('js/api/SummernoteCallbacks.js', 'assemble') }}"></script>
<script>
    document.addEventListener('DOMContentLoaded', function() {
        $('.select2').select2(); 
        
        $('.summernote').summernote({
            height: 500,
            lang: 'ru-RU',
            callbacks: {
                onImageUpload: SummernoteCallbacks.onImageUpload,
                onMediaDelete: SummernoteCallbacks.onMediaDelete
            } 
        });
        
        $('.summernote').summernote('code', `{!! clean($page->pageProperties->where('code', 'content')->first()->value ?? '') !!}`);

    });
</script>
@endsection