@extends('admin.home')

@section('sidebar')
    @include ('admin.pages.sidebar', ['page' => 'page'])
@endsection

@section('content')
    <h2>Редактирование страницы {{ $page->id }}</h2>
    <form method="POST" action="{{ route('admin.pages.update', $page) }}">
        @csrf
        @method('PUT')
         
        <div class="form-group">
            <label for="link" class="col-form-label">Символьный код (на английском через дефис, прописными буквами) <b class="text-danger">*</b></label>
            <input 
                id="link" 
                type="text" 
                class="form-control{{ $errors->has('link') ? ' is-invalid' : '' }}" 
                name="link" 
                value="{{ old('link', $page->link) }}" 
                required
            >
        
            @if ($errors->has('link'))
                <span class="invalid-feedback"><strong>{{ $errors->first('link') }}</strong></span>
            @endif
        </div>

        <div class="form-group">
            <label for="parent" class="col-form-label">Родительская страница</label>
            <select id="parent" class="form-control{{ $errors->has('parent') ? ' is-invalid' : '' }} select2" name="parent">
                <option value=""></option>

                @foreach ($parents as $parent)
                    <option value="{{ $parent->id }}"{{ ($parent->id === $page->parent_id) ? ' selected' : '' }}>
                        @for ($i = 0; $i < $parent->depth; $i++) &mdash; @endfor
                        {{ $parent->title }}
                    </option>
                @endforeach;
            </select>
            @if ($errors->has('parent'))
                <span class="invalid-feedback"><strong>{{ $errors->first('parent') }}</strong></span>
            @endif
        </div>

        <div class="form-group">
            <button type="submit" class="btn btn-primary">Сохранить изменения</button>
        </div>
    </form>
@endsection