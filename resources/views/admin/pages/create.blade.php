@extends('admin.home')

@section('sidebar')
    @include ('admin.pages.sidebar', ['page' => 'page'])
@endsection

@section('content')
    <h2>Создание страницы</h2>
    <form method="POST" action="{{ route('admin.pages.store') }}">
        @csrf
        
        <div class="form-group">
            <label for="localisation" class="col-form-label">Язык локализации сайта <b class="text-danger">*</b></label>
            
            <select class="form-control{{ $errors->has('localisation') ? ' is-invalid' : '' }} select2" name="localisation">
                @foreach ($localisations as $localisation)
                    <option value="{{ $localisation->id }}"{{ $localisation->id == old('localisation_id') ? ' selected' : '' }}>
                        {{ __("interface.localisation." . $localisation->code) }}
                    </option>
                @endforeach
                
            </select>
            @if ($errors->has('localisation'))
                <span class="invalid-feedback"><strong>{{ $errors->first('localisation') }}</strong></span>
            @endif
        </div>

        <div class="form-group">
            <label for="title" class="col-form-label">Название страницы(с заглавной буквы) <b class="text-danger">*</b></label>
            <input id="title" class="form-control{{ $errors->has('title') ? ' is-invalid' : '' }}" name="title" value="{{ old('title') }}" required>
            @if ($errors->has('title'))
                <span class="invalid-feedback"><strong>{{ $errors->first('title') }}</strong></span>
            @endif
        </div>

        <div class="form-group">
            <label for="menu_title" class="col-form-label">Название страницы (для отображения в меню, с заглавной буквы)</label>
            <input id="menu_title" class="form-control{{ $errors->has('menu_title') ? ' is-invalid' : '' }}" name="menu_title" value="{{ old('menu_title') }}">
            @if ($errors->has('menu_title'))
                <span class="invalid-feedback"><strong>{{ $errors->first('menu_title') }}</strong></span>
            @endif
        </div>

        <div class="form-group">
            <label for="link" class="col-form-label">Символьный код (на английском через дефис, прописными буквами) <b class="text-danger">*</b></label>
            <input 
                id="link" 
                type="text" 
                class="form-control{{ $errors->has('link') ? ' is-invalid' : '' }}" 
                name="link" 
                value="{{ old('link') }}" 
                required
            >
        
            @if ($errors->has('link'))
                <span class="invalid-feedback"><strong>{{ $errors->first('link') }}</strong></span>
            @endif
        </div>

        <div class="form-group">
            <label for="parent" class="col-form-label">Родительская страница</label>
            <select id="parent" class="form-control{{ $errors->has('parent') ? ' is-invalid' : '' }} select2" name="parent">
                <option value=""></option>
                @foreach ($parents as $parent)
                    <option value="{{ $parent->id }}"{{ $parent->id == old('parent') ? ' selected' : '' }}>
                        @for ($i = 0; $i < $parent->depth; $i++) &mdash; @endfor
                        {{ $parent->title }}
                    </option>
                @endforeach;
            </select>
            @if ($errors->has('parent'))
                <span class="invalid-feedback"><strong>{{ $errors->first('parent') }}</strong></span>
            @endif
        </div>
        
        <div class="form-group row">
            <label for="example-phone-input" class="col-12 col-md-4 col-form-label">Seo title</label>
            <div class="col-12 col-md-12">
                <input class="form-control" placeholder="" name="seo_title" type="text" value="{{ old('seo_title') }}" >
            </div>
        </div>
            
        <div class="form-group row">
            <label for="example-phone-input" class="col-12 col-md-4 col-form-label">Seo keywords</label>
            <div class="col-12 col-md-12">
                <input class="form-control" placeholder="" name="seo_keywords" type="text" value="{{ old('seo_keywords') }}" >
            </div>
        </div>
        
        <div class="form-group row">
            <label for="example-phone-input" class="col-12 col-md-4 col-form-label">Seo description</label>
            <div class="col-12 col-md-12">
                <textarea name="seo_descriptions" class="w-100" rows="10">{{ old('seo_descriptions') }}</textarea>
            </div>
        </div>

        <div class="form-group">
            <label for="content" class="col-form-label">Контент страницы <b class="text-danger">*</b></label>
            <textarea id="content" class="form-control{{ $errors->has('content') ? ' is-invalid' : '' }} summernote" data-route="{{ route('upload.image', ['type' => 'page']) }}" name="content" rows="10">{{ old('content') }}</textarea>
            @if ($errors->has('content'))
                <span class="invalid-feedback"><strong>{{ $errors->first('content') }}</strong></span>
            @endif
        </div>

        <div class="form-group">
           
        </div>

        <div class="form-group">
            <button type="submit" class="btn btn-primary">Создать</button>
        </div>
    </form>
@endsection

@section('scripts')
<script src="{{ mix('js/api/SummernoteCallbacks.js', 'assemble') }}"></script>
<script>
    document.addEventListener('DOMContentLoaded', function() {
        $('.select2').select2(); 
        
        $('.summernote').summernote({
            height: 500,
            lang: 'ru-RU',
            callbacks: {
                onImageUpload: SummernoteCallbacks.onImageUpload,
                onMediaDelete: SummernoteCallbacks.onMediaDelete
            } 
        });
    });
</script>
@endsection