<ul class="catalog tabs__titles">
    <li class="{{ $page === 'product' ? ' tabs__titles__active' : '' }}">
        <a href="{{ route('search.index', Request()->except('entity') ) }}">
            {{ __('interface.tabs.products') }}
        </a>
    </li>
    <li class="{{ $page === 'request_product' ? ' tabs__titles__active' : '' }}">
        <a href="{{ route('search.index', array_merge(Request()->all(), ['entity' => 'request_product']) ) }}">
            {{ __('interface.tabs.request_products') }}
        </a>
    </li>
</ul>