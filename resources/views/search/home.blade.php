@extends('layouts.market.index')

@section('content')
    @yield('search-content') 
@endsection

@section('scripts')
    <script src="{{ mix('js/search.js', 'assemble') }}"></script>
    @yield('content-script')
@endsection