@extends('search.home')

@section('search-content')
<div class="catalog">
	<div class="container">
        <div class="breadcrumbs">
			<ul>
				<li><a href="{{ route('main') }}">{{ __('breadcrumbs.pages.main') }}</a></li>
			</ul>
		</div>
        <h2 class="catalog__title">
            @if(!empty($categoryName) && !empty($search))
                {{ __('interface.search.found_on_request') }} "{{ $search }}" {{ __('interface.search.in_category') }} "{{ $categoryName }}"   
            @endif
            
            @if(!empty($categoryName) && empty($search))
                {{ __('interface.search.found_in_category') }} "{{ $categoryName }}"   
            @endif
            
            @if(empty($categoryName) && !empty($search))
                {{ __('interface.search.found_on_request') }} "{{ $search }}"    
            @endif
        </h2>

        @include ('search._nav', ['page' => $entity])
        
        <div class="catalog__filterTop">
 
            @if(!empty($backLink))
            <a href="{{ $backLink }}" title="{{ $previousCategoryName ?? '' }}" class="catalog__filterTop__back"></a>
            @endif
     
            <div class="catalog__filterTop__area">
                <div class="catalog__filterTop__sort">
                    <select name="order_by" id='order-by' class="catalog__filterTop__sort__select">
                        <option {{ (request('order_by') == 'shows_asc')? 'selected' : '' }} value="shows_asc">{{ __('interface.sort.shows') }} &nbsp;&#xf160;</option>
                        <option {{ (request('order_by') == 'shows_desc')? 'selected' : '' }} value="shows_desc">{{ __('interface.sort.shows') }} &nbsp;&#xf161;</option>
                        <option {{ (request('order_by') == 'rating_asc')? 'selected' : '' }}  value="rating_asc">{{ __('interface.sort.rating') }} &nbsp;&#xf160;</option>
                        <option {{ (request('order_by') == 'rating_desc')? 'selected' : '' }}  value="rating_desc">{{ __('interface.sort.rating') }} &nbsp;&#xf161;</option>
                    </select>
                </div>
            </div>
            
        </div>
                    
        <div class="catalog__content">
        
            @include ('search._filters', ['categoryDescendants' => $categoryDescendants ?? null])

            <div class="catalog__content__right">
                <div class="catalog__products">

                    @php
                        $currency = $currencyRates->where('code', $currentCurrencyCode)->first();
                    @endphp
                
                    @foreach($results as $product)
                        <div class="catalog__products__item"> 
                            @php $previewImg = $product->downloads->where('type', 'preview_image')->first(); @endphp
                            <a 
                                href="{{ route('catalog.request_product.show', ['request_product' => $product->id]) }}" 
                                class="catalog__products__item__image"  
                                @if($previewImg)
                                style="background-image: url('{{ asset('storage/' . $previewImg->path) }}');" 
                                @endif
                                >
                                @guest 
                                <form method="POST" class="favorite-add-form" action="{{ route('favorite.add', ['type' => 'request_product', 'id' => $product->id]) }}">
                                    @csrf
                                    <div class="catalog__products__item__favorite" title="{{ __('interface.title.add_to_favorites') }}"></div>
                                </form>
                                @endguest 
                                
                                @auth 
                                    @if( $product->favorites->where('user_id', $user->id)->first() )
                                        <div class="catalog__products__item__favorite active favorite-remove" data-route="{{ route('favorite.remove', ['type' => 'request_product', 'id' => $product->id]) }}" title="{{ __('interface.title.remove_to_favorites') }}"></div>
                                    @else
                                        <div class="catalog__products__item__favorite favorite-add" data-route="{{ route('favorite.add', ['type' => 'request_product', 'id' => $product->id]) }}" title="{{ __('interface.title.add_to_favorites') }}"></div>
                                    @endif
                                @endauth
         
                            </a>
                            
                            <div class="catalog__products__item__title">
                                {{ $product->requestProductProperties->where('code', 'name')->first()->value ?? '' }}
                            </div>
                            
                            @if($product->requestProductPrices->first())
                            <div class="catalog__products__item__price">
                                {{ ($product->requestProductPrices->first()->min * $currency->rate ?? '') ? . $currency->symbol . $product->requestProductPrices->first()->min * $currency->rate : '' }} 
                                {{ ($product->requestProductPrices->first()->max * $currency->rate ?? '') ? ' — ' . $currency->symbol . $product->requestProductPrices->first()->max * $currency->rate : '' }}
                            </div>
                            @else
                            <div class="catalog__products__item__price">
                                {{ __('interface.catalog.price_on_request') }}
                            </div>
                            @endif

                            @if(!empty($product->characteristics['min_order']))
                            <div class="catalog__products__item__minOrder">
                                {{ $product->characteristics['min_order']['value'] }} 
                                {{ $product->characteristics['min_order']['type'] }}
                                <span>{{ __('interface.catalog.min_order') }}</span>
                            </div>
                            @endif
                        </div>
                    @endforeach
                    
                </div>
                
                {{ $results->render('widgets.pagination') }}

            </div>
        </div>
    </div>
</div>
@endsection