{{--
<div class="col-3">
    @if($categoryDescendants && $categoryDescendants->first())
        @foreach($categoryDescendants as $categoryDescendant)
            <div><b><a href="{{ $categoryDescendant->link }}">{{ $categoryDescendant->categoryProperties->where('code', 'name')->first()->value }}</a></b></div>
            @if($categoryDescendant->children->first())
                @foreach($categoryDescendant->children as $key => $categoryChildren)
                    @if($key == 5)
                        @break
                    @endif
                    <div><a href="{{ $categoryChildren->link }}">{{ $categoryChildren->categoryProperties->where('code', 'name')->first()->value }}</a></div>
                @endforeach
            @endif 
        @endforeach
        
    @elseif($entity !== 'request_product')
        <label class="mt-3">Цена, $</label>
        <form id="filters">
            <div class="row">
                <div class="col-12 d-flex justify-content-between">
                    <span>от</span>
                    <input type="text" name="min_price" class="w-25 mr-5 filter" value="{{ old('min_price', (request('min_price'))?? '') }}">
                    <span>до</span>
                    <input type="text" name="max_price" class="w-25 filter" value="{{ old('max_price', (request('max_price'))?? '') }}">
                </div>
            </div>
            
            <div class="row mt-4"> 
                <div class="col-12">
                    <label>Страна производитель</label>
                    <select class="select2 countries w-100" name="country">
                        <option></option>
                        @foreach($countries as $country)
                        <option {{ (request('country') == $country['id']) ? 'selected' : '' }} value="{{ $country['id'] }}">{{ $country['text'] }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            
            <label for="example-email-input" class="mt-3">Цвет товара</label>
            <div class="row">
                <div class="col-12">
                    <select name="color">
                        @foreach($colors as $color)
                            <option value="{{ $color->code }}">{{ $color->value }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            
            <div class="row mt-5">
                <div class="col-12">
                    <button type="button" class="btn btn-light col-12">Найти товары</button>
                </div>
            </div> 
        </form>
    @endif
</div>
--}}

<div class="catalog__content__left">
    @if($categoryDescendants && $categoryDescendants->first())
    <div class="catalog__filter catalog__categories">
        @foreach($categoryDescendants as $categoryDescendant)
        <div class="catalog__categories__block">
            @if($categoryDescendant->children->first())
            <div class="catalog__categories__block__title showAll">{{ $categoryDescendant->categoryProperties->where('code', 'name')->first()->value }}</div>
            <div class="catalog__categories__block__links">
                <ul>
                    @foreach($categoryDescendant->children as $key => $categoryChildren)
                        @if($key <= 4)
                        <li>
                            <a href="{{ $categoryChildren->link }}">{{ $categoryChildren->categoryProperties->where('code', 'name')->first()->value }}</a>
                        </li>
                        @elseif($key > 4)
                        <li style="display:none;">
                            <a href="{{ $categoryChildren->link }}">{{ $categoryChildren->categoryProperties->where('code', 'name')->first()->value }}</a>
                        </li>
                        @endif
                    @endforeach
                    
                    @if($categoryDescendant->children->count() > 4)
                        <li><a class="catalog__categories__block__links__showAll">{{ __('interface.buttons.show_all') }}</a></li>
                    @endif
                </ul>
            </div>
            @else
            <div class="catalog__categories__block__title">
                <a href="{{ $categoryDescendant->link }}">
                    {{ $categoryDescendant->categoryProperties->where('code', 'name')->first()->value }}
                </a>
            </div>
            @endif 
        </div>
        @endforeach
    </div>
    @elseif($entity !== 'request_product')
    <form id="filters">
        <div class="catalog__filter catalog__digitalFilter">
        <div class="catalog__digitalFilter__title">{{ __('interface.filters.price') }}</div>
            <div class="catalog__digitalFilter__inputs">
                <div class="catalog__digitalFilter__inputs__input">
                    <span>от</span> 
                    <input type="text" name="min_price" placeholder="0" value="{{ old('min_price', (request('min_price'))?? '') }}">
                </div>
                <div class="catalog__digitalFilter__inputs__input">
                    <span>до</span> 
                    <input type="text" name="max_price" placeholder="1000000" value="{{ old('max_price', (request('max_price'))?? '') }}">
                </div>
            </div>
        </div>

        <div class="catalog__filter catalog__filterColors">
            <div class="catalog__filterColors__title">{{ __('interface.filters.product_color') }}</div>
            <div class="catalog__filterColors__content">
                <select name="color" class="colorpicker-handle">
                    @foreach($colors as $color)
                    <option {{(request('color') === $color->code) ? 'selected' : ''}} value="{{ $color->code }}">{{ $color->value }}</option>
                    @endforeach
                </select>
            </div>    
        </div>
        <button type="button" class="catalog__filter catalog__filter__button">{{ __('interface.buttons.find_products') }}</button>
    </form>
    @endif
</div>