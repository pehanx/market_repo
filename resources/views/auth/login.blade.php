{{--
@extends('layouts.app')

@section('breadcrumbs', '')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('interface.messages.enter') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('login') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('interface.messages.email') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('interface.messages.password') }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-6 offset-md-4">
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                                    <label class="form-check-label" for="remember">
                                        {{ __('interface.messages.remember') }}
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-8 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{  __('interface.messages.entrance') }}
                                </button>
                                @if (Route::has('password.request'))
                                    <a class="btn btn-link" href="{{ route('password.request') }}">
                                        {{ __('interface.messages.forgot_password') }}
                                    </a>
                                @endif
                            </div>

                            
                        </div>
                    </form>
                    <div class="form-group row justify-content-center mt-3">
                            
                            <div class="col-md-4">
                                <a href="{{ route('login.network', 'vkontakte') }}" class="vk btn">
                                    <i class="fa fa-vk fa-2x"></i> 
                                </a>
                                 <a href="{{ route('login.network', 'odnoklassniki') }}" class="ok btn offset-md-2">
                                    <i class="fa fa-odnoklassniki fa-2x"></i> 
                                </a>
                            </div>

                            
                            <!--
                            <div class="col-md-8">
                                <a href="#" class="twitter btn">
                                    <i class="fa fa-twitter fa-fw"></i> Login with Twitter
                                </a>
                            </div>
                            <div class="col-md-8">
                                <a href="#" class="google btn"><i class="fa fa-google fa-fw">
                                    </i> Login with Google+
                                </a>
                            </div>
                            <div class="col-md-8">
                                <a href="{{ route('login.network', 'facebook') }}" class="fb btn">
                                    <i class="fa fa-facebook fa-fw"></i> 
                                    Login with Facebook
                                </a>
                            </div>
                            -->    
                    </div>
                    
                    
                </div>
            </div>
        </div>

    </div>
</div>
@endsection
--}}

@extends('layouts.market.index')

@section('content')
<div class="login">
	<div class="container">
		<div class="login__wrapper">
			<div class="login__title">{{ __('interface.messages.enter') }}</div>
			{{-- <div class="login__social">
				<div class="login__social__description">{{ __('interface.messages.from_socials') }}</div>
				<div class="login__social__elements">
					<a href="{{ route('login.network', 'vkontakte') }}"><div class="login__social__elements__element login__social__elements__element-vk"></div></a>
					<a href="{{ route('login.network', 'odnoklassniki') }}"><div class="login__social__elements__element login__social__elements__element-ok"></div></a>
					<a href="#"><div class="login__social__elements__element login__social__elements__element-google"></div></a>
				</div>
			</div> --}}
            <form method="POST" action="{{ route('login') }}">
                @csrf
                <div class="login__form form">
                    <div class="form__row form__row-required @if($errors->has('email')) {{ 'form__row-error' }} @endif">
                        <input id="email" type="email" class="form__row__input" name="email" value="{{ old('email') }}" required autocomplete="email">
                        <label class="form__row__label" for="email">{{ __('interface.messages.email') }}</label>
                        @error('email')
                            <div class="form__row__error">{{ $message }}</div>
                        @enderror
                    </div>
                    <div class="form__row form__row-required">
                        <input type="password" class="form__row__input" name="password" id="password">
                        <label class="form__row__label" for="password" autocomplete="current-password">{{ __('interface.messages.password') }}</label>
                        <div class="form__row__showPassword"></div>
                    </div>
                    <div class="form__double__row form__double__row-space-between">
                        <div class="form__row">
                            <div class="form__row__checkbox">
                                <input type="checkbox" name="remember" class="form__row__checkbox__input" id="remember-me" checked="checked"><label for="remember-me">{{ __('interface.messages.remember') }}</label>
                            </div>
                        </div>
                        <div class="form__row">
                            <a href="{{ route('password.request') }}" class="form__row__link">{{ __('interface.messages.forgot_password') }}</a>
                        </div>
                    </div>
                    <div class="form__row">
                        <button type="submit" class="button button-primary">{{  __('interface.messages.entrance') }}</button>
                        <a class="button button-transparent" href="{{ route('register') }}">{{ __('interface.messages.register') }}</a>
                    </div>
                </div>
            </form>
		</div>
	</div>
</div>
@endsection 