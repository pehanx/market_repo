{{--
@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('interface.messages.registration') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('register') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('interface.messages.login') }}</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>

                                @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('interface.messages.email') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('interface.messages.password') }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('interface.messages.confirm_password') }}</label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('interface.messages.registrate') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
--}}

@extends('layouts.market.index')

@section('content')
<div class="login">
	<div class="container">
		<div class="login__wrapper">
			<div class="login__title">{{ __('interface.messages.registration') }}</div>
			<div class="login__form form">
                <form method="POST" action="{{ route('register') }}">
                    @csrf
                    <div class="form__row form__row-required @if($errors->has('name')) {{ 'form__row-error' }} @endif">
                        <input type="text" class="form__row__input" name="name" id="name" value="{{ old('name') }}" required>
                        <label for="name" class="form__row__label">{{ __('interface.messages.login') }}</label>
                        @error('name')
                            <div class="form__row__error">{{ $message }}</div>
                        @enderror
                    </div>
                    <div class="form__row form__row-required @if($errors->has('email')) {{ 'form__row-error' }} @endif">
                        <input type="email" class="form__row__input" name="email" id="email" value="{{ old('email') }}" required>
                        <label for="email" class="form__row__label">{{ __('interface.messages.email') }}</label>
                        @error('email')
                            <div class="form__row__error">{{ $message }}</div>
                        @enderror
                    </div>
                    <div class="form__row form__row-required @if($errors->has('password')) {{ 'form__row-error' }} @endif">
                        <input type="password" class="form__row__input" name="password" id="password" autocomplete="new-password" required>
                        <label for="password" class="form__row__label">{{ __('interface.messages.password') }}</label>
                        <div class="form__row__showPassword"></div>
                        @error('password')
                            <div class="form__row__error">{{ $message }}</div>
                        @enderror
                    </div>
                    <div class="form__row form__row-required">
                        <input type="password" class="form__row__input" name="password_confirmation" id="password-confirm" autocomplete="new-password" required>
                        <label for="password-confirm" class="form__row__label">{{ __('interface.messages.confirm_password') }}</label>
                        <div class="form__row__showPassword"></div>
                    </div>
                    <div class="form__row">
                        <div class="form__row__checkbox">
                            <input type="checkbox" class="form__row__checkbox__input" id="accept">
                            <label for="accept">
                                {{ __('interface.messages.accepting_private_data') }} 
                                {{-- <a href="#">{{ __('interface.messages.conditions') }}</a>,  
                                <a href="#">{{ __('interface.messages.politics') }}</a>  --}}
                                {{ __('interface.messages.get_messages') }}
                            </label>
                        </div>
                    </div>
                    <div class="form__row">
                        <button type="submit" class="button button-primary">{{ __('interface.messages.register') }}</button>
                        <a href="{{ route('login') }}" class="button button-transparent">{{ __('interface.messages.entrance') }}</a> 
                    </div>
                </form>
			</div>
		</div>
	</div>
</div>
@endsection