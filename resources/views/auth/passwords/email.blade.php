{{--
@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Reset Password') }}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <form method="POST" action="{{ route('password.email') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Send Password Reset Link') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
--}}

@extends('layouts.market.index')

@section('content')
        
<div class="login">
	<div class="container">
		<div class="login__wrapper">
            <div class="login__title">{{ __('interface.messages.restoring_password') }}</div>
            @if(!session()->has('status'))
            <div class="login__form form">
                <form method="POST" action="{{ route('password.email') }}">
                    @csrf
                    <div class="form__row @if($errors->has('email')) {{ 'form__row-error' }} @endif">
                        <input type="email" class="form__row__input" name="email" id="email" value="{{ old('email') }}" required autocomplete="email">
                        <label for="email" class="form__row__label">{{ __('interface.messages.enter_email') }}</label>
                        @error('email')
                            <div class="form__row__error">{{ $message }}</div>
                        @enderror
                    </div>
                    
                    <div class="form__row">
                        <button type="submit" class="button button-primary">{{ __('interface.messages.restore') }}</button>
                    </div>
                </form>
            </div>
            @else
            <div class="login__form form">
                    <div class="form-row">
                        <div class="login__form__success">
                            {{ session('status') }}
                        </div>
                    </div>
                    <div class="form__row">
                        <a href="{{ route('main') }}" class="button button-primary">{{ __('interface.messages.back_main') }}</a>
                    </div>
                
            </div> 
            @endif
		</div>
	</div>
</div>
@endsection