{{-- @extends('layouts.market.index')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('interface.reset_password') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('password.update') }}">
                        @csrf

                        <input type="hidden" name="token" value="{{ $token }}">

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('interface.messages.email') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ $email ?? old('email') }}" required autocomplete="email" autofocus>

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('interface.messages.new_password') }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('interface.messages.confirm_password') }}</label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('interface.buttons.reset_password') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection --}}

@extends('layouts.market.index')

@section('content')
<div class="login">
	<div class="container">
		<div class="login__wrapper">
			<div class="login__title">{{ __('interface.reset_password') }}</div>
			<div class="login__form form">
                <form method="POST" action="{{ route('password.update') }}">
                    @csrf

                    <input type="hidden" name="token" value="{{ $token }}">

                    <div class="form__row form__row-required @if($errors->has('email')) {{ 'form__row-error' }} @endif">
                        <input type="email" class="form__row__input" id="email" name="email" value="{{ $email ?? old('email') }}" required>
                        <label for="email" class="form__row__label">{{ __('interface.messages.email') }}</label>
                        @error('email')
                            <div class="form__row__error">{{ $message }}</div>
                        @enderror
                    </div>
                    
                    <div class="form__row form__row-required @if($errors->has('password')) {{ 'form__row-error' }} @endif">
                        <input type="password" class="form__row__input" name="password" id="password" autocomplete="new-password" required>
                        <label for="password" class="form__row__label">{{ __('interface.messages.new_password') }}</label>
                        <div class="form__row__showPassword"></div>
                        @error('password')
                            <div class="form__row__error">{{ $message }}</div>
                        @enderror
                    </div>
                    
                    <div class="form__row form__row-required">
                        <input type="password" class="form__row__input" name="password_confirmation" id="password-confirm" autocomplete="new-password" required>
                        <label for="password-confirm" class="form__row__label">{{ __('interface.messages.confirm_password') }}</label>
                        <div class="form__row__showPassword"></div>
                    </div>
                    <div class="form__row">
                        <button type="submit" class="button button-primary">{{ __('interface.buttons.reset_password') }}</button>
                    </div>
                </form>
			</div>
		</div>
	</div>
</div>
@endsection
