@extends('layouts.market.index')

@section('content')
	<div class="errors">
		<div class="container">
			<div class="errors__content">
				<h1 class="errors__title">{{ __('interface.errors.not_found') }}</h1>
				<div class="errors__image"></div>
			</div>
		</div>
	</div>
@endsection
