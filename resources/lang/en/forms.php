<?php

return [
    'login' => 'Login',
    'contacts' => 'Contacts',
    'you' => 'You',
    'seller' => 'Seller',
    'buyer' => 'Buyer',
    'tell_about' => 'Tell about',
    'name' => 'Name',
    'last_name' => 'Last name',
    'patronymic' => 'Patronymic',
    'birthday' => 'Birthday',
    'gender' => 'Gender',
    'man' => 'Man',
    'women' => 'Women',
    'position' => 'Position',
    'phone' => 'Phone',
    'image' => 'Image',
    'images_company' => 'Images company',
    'site' => 'Site',
    'company_about' => 'About company',
    'company_name' => 'Name company',
    'company_adress_judicial' => 'Judicial adress',
    'company_adress_fact' => 'Fact adress',
    'inn' => 'INN',
    'additionally' => 'Additionally',
    'city' => 'City',
    'country' => 'Country',
    'save' => 'Save',
    'save_changes' => 'Save changes',
    'change' => 'Change',
    'create' => 'Create',
    'add_manager' => 'Add manager',
    'actions' => 'Actions',
    'add' => 'Add',
    'delete' => 'Delete',
    'documents' => 'Documents',
];
