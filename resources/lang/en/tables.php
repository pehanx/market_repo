<?php

return [
    'headers' => [
    
        'email' => 'Email',
        'login' => 'Login',
        'type' => 'Type',
        'registration_type' => 'Registration type',
        'profile' => 'Profile',
        'actions' => 'Actions',
        'roles' => 'Roles',
        'status' => 'Status',
        'name' => 'Name',
        'manager' => 'Manager',
    ],
    
    'data' => [
        'buyer' => 'Buyer',
        'seller' => 'Seller',
        'approved' => 'Approved',
        'not_approved' => 'Not approved',
        'send_message' => 'Send_message',
        'send_notice' => 'Send notice',
        'approve' => 'Approve',
        'delete' => 'Delete',
        'change' => 'Change',
        'disable' => 'Disable',
        'social' => 'Social',
        'native' => 'Native',
        'wait_approve' => 'Wait approve',
    ]

];

