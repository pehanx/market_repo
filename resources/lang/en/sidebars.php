<?php

return [
    'my_account' => 'Account',
    'profiles' => 'Profiles',
    'admin_panel' => 'Admin panel', 
    'users' => 'Users',
    'managers' => 'Managers',
    'managers_profiles' => "Profile's managers",
    'all_profiles' => 'All profiles',
    'private_information' => 'Private information',
    'cabinet' => 'Cabinet',
]; 
