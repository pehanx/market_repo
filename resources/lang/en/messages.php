<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
    

    'login' => 'Login',
    'logout' => 'Logout',
    'cabinet' => 'Cabinet',
    'entrance'   => 'Entrance',
    'enter'   => 'Enter',
    'remember' => 'Remember me',
    'password' => 'Password',
    'email' => 'Еmail',
    'confirm_password' => 'Confirm password',
    'registration' => 'Registration',
    'registrate' => 'Registrate', 
    'register' => 'Register',
    'forgot_password' => 'Forgot your password?',
    'settings' => 'Settings',
    'create_profile' => 'You need to create a profile in order to use the full functionality of the site',
    'approve_profile' => 'Company documents',
    'need_documents' => 'Add a photo or scan Of the certificate of registration or other documents. So we can quickly confirm the profile of Your company, and You-to use the full functionality of the site',

];
