<?php
    return [
        'cabinet' => [
            'cabinet' => 'Cabinet',
            'private_information' => 'Private information',
            'entrance_information' => 'Entrance information',
            'all_profiles' => 'All profiles',
            'all_managers' => 'All managers',
            'all_products' => 'All products',
            'profile_create' => 'Creating  profile',
            'profile_managers' => "Profile's managers",
            'new_profile' => 'New profile',
            'new_manager' => 'New manager',
        ],
    ];
?>