<?php

return [
    'headers' => [
    
        'email' => 'Email',
        'login' => 'Логин',
        'type' => 'Тип',
        'registration_type' => 'Тип регистрации',
        'profile' => 'Профиль',
        'actions' => 'Действия',
        'roles' => 'Роли',
        'status' => 'Статус',
        'name' => 'Имя',
        'manager' => 'Менеджер',
    ],
    
    'data' => [
        'buyer' => 'Покупатель',
        'seller' => 'Продавец',
        'approved' => 'Подтвержден',
        'not_approved' => 'Не подтвержден',
        'send_message' => 'Отправить сообщение',
        'send_notice' => 'Отправить уведомление',
        'approve' => 'Подтвердить',
        'delete' => 'Удалить',
        'change' => 'Редактировать',
        'disable' => 'Деактивировать',
        'social' => 'Соц. сеть',
        'native' => 'Нативный',
        'wait_approve' => 'Ожидает подтверждения',
        'select' => 'Выбрать',
        'all' => 'Все',
    ],

    'exports' => [
        'progress' => 'Прогресс',
        'files' => 'Файлы',
        'profile' => 'Профиль',
        'download' => 'Скачать',
        'download_all' => 'Скачать все',
        'current' => 'Текущий',
        'start' => 'Начало',
    ]

];

