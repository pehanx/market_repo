<?php
    return [
        'profiles' => 'Профили', 
        'users' => 'Пользователи',
        'buyer' => 'Покупатель', 
        'seller' => 'Продавец',
        'search' => 'Найти',
        'all' => 'Все',
        'no_matter' => 'Не важно',
        'clear_filters' => 'Сбросить фильтры',
    ];

?>