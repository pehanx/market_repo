<?php
    return [
        'approve_user_profile' => 'Подтвердите профиль :Name',
        'approve' => 'Подтвердить',
        
        'messages' => [
            'user_need_approve_profile' => 'Пользователь :Name просит подтвердить свой профиль',
        ]
    ];
?>