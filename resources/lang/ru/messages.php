<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
    
    'entrance'   => 'Войти',
    'cabinet' => 'Личный кабинет',
    'enter'   => 'Вход',
    'forgot' => 'Забыли Ваш пароль?',
    'remember' => 'Запомнить',
    'login' => 'Имя',
    'logout' => 'Выйти',
    'current_password' => 'Текущий пароль',
    'password' => 'Пароль',
    'email' => 'Еmail',
    'new_password' => 'Новый пароль',
    'confirm_password' => 'Повторите пароль',
    'registration' => 'Регистрация',
    'registrate' => 'Зарегистрировать',
    'register' => 'Зарегистрироваться',
    'forgot_password' => 'Забыли свой пароль?',
    'settings' => 'Настройки',
    'create_profile' => 'Для того, чтобы пользоваться полным функционалом сайта Вам необходимо создать профиль',
    'approve_profile' => 'Учредительные документы компании',
    'need_documents' => 'Добавте фото или скан Свидетельства о регистрации ИНН вашего юридического лица или ОГРНИП для индивидуальных предпринимателей. Так мы сможем оперативно подтвердить профиль Вашей компании, а Вы - использовать полный функционал сайта',
    'current' => 'текущий',
    'from_socials' => 'Через социальные сети',
    'restoring_password' => 'Восстановление пароля',
    'restore' => 'Восстановить',
    'enter_email' => 'Введите Ваш E-mail',
    'accepting_private_data' => 'Я соглашаюсь на обработку персональных данных в соответствии с',
    'conditions' => 'Условиями использования сайта',
    'politics' => 'Политикой обработки персональных данных',
    'get_messages' => 'и на получение сообщений в процессе обработки заказа.',
];
