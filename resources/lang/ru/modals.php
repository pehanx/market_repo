<?php

return [
    'cancel' => 'Отмена',
    'delete' => 'Удалить',
    'approve_action' => 'Подтвердите действие',
    'your_sure_delete' => 'Вы действительно хотите удалить :item ?',
    'your_sure_activate' => 'Вы дествительно хотите активировать :item ?',
    'your_sure_restore' => 'Вы действительно хотите восстановить :item ?',
    'your_sure_disable' => 'Вы действительно хотите отключить :item ?',
    'your_sure_destroy' => 'Вы действительно хотите удалить :item ?',
    'your_sure_destroy_finally' => 'Вы действительно хотите окончательно удалить :item ?',
    'your_sure_disable_from_profiles' => 'Вы действительно хотите отключить :item ? <br/><br/> Выберите профили, для которых необходимо отключить товар:',
    'your_sure_destroy_from_profiles' => 'Вы действительно хотите удалить товар ":item"? <br/><br/> Выберите профили, из которых необходимо удалить товар:',
    'your_sure_destroy_profile' => 'Вы действительно хотите удалить профиль',
    'your_sure_approve_profile' => 'Вы действительно хотите активировать профиль',
    'your_sure_disable_profile' => 'Вы действительно хотите деактивировать профиль',
    'your_sure_destroy_manager' => 'Вы действительно хотите удалить менеджера :Manager ?',
    'ok' => 'Ок',
    'disable' => 'Деактивировать',
    'choose_local_profile' => 'Выберите язык профиля',
    'choose_profile_type' => 'Выберите тип профиля',
    'ru' => 'Русский',
    'en' => 'Английский',
    'cn' => 'Китайский',
    'create_profile' => 'Перейти к заполнению профиля',
    'checked_elements' => 'выбранные товары',
    'all_elements' => 'все товары',
    'buyer' => 'Покупатель',
    'seller' => 'Продавец',
    'export' => 'Выберите параметры для экспорта',
    'export_thumbnails_label' => 'C изображением товара',
    'export_models_label' => 'C моделями товара',
    'export_format_label' => 'формат выгрузки',
];
