<?php

return [

    'status' => [
        'active' => 'Активные',
        'inactive' => 'Неактивные',
        'deleted' => 'Удалённые',
        'all' => 'Все',
    ],

];
