<?php

return [
    
    'action' => 'Действие',
    'create' => 'Создать',
    'disable' => 'Отключить',
    'edit' => 'Редактировать',
    'delete' => 'Удалить',
    'export_submit' => 'Экспортировать',
    'import_submit' => 'Импортировать',
    'choose_all' => 'Выбрать всё',
    'disable_choosen' => 'Отключить выбранные',
    'delete_choosen' => 'Удалить выбранные',

];
