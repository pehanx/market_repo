<?php

return [

    'category' => 'Категория',

    'action' => [
        'index' => 'Действие',
        'disable' => 'Отключить',
        'edit' => 'Редактировать',
        'delete' => 'Удалить',
        'choose_all' => 'Выбрать всё',
        'disable_choosen' => 'Отключить выбранные',
        'delete_choosen' => 'Удалить выбранные',
    ],
    
    'status' => [
        'active' => 'Активные',
        'inactive' => 'Неактивные',
        'deleted' => 'Удалённые',
        'all' => 'Все',
    ],

    'search' => [
        'name' => 'Название',
        'submit' => 'Найти',
        'clear' => 'Очистить',
        'number' => 'Номер',
    ],

];
