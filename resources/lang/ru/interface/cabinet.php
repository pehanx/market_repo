<?php

return [

    'export' => [
        'index' => 'Экспорт',
        'page' => 'С текущей страницы',
        'progress' => 'Прогресс',
        'files' => 'Файлы',
        'profile' => 'Профиль',
        'download' => 'Скачать',
        'download_all' => 'Скачать все',
        'current' => 'Текущий',
        'start' => 'Начало',
        'format' => 'Формат экспорта',
        'with_models' => 'С торговыми предложениями',
        'with_thumbnails' => 'С изображениями',
    ],

    'import' => [
        'index' => 'Импорт',
        'progress' => 'Прогресс',
        'files' => 'Файлы',
        'profile' => 'Профиль',
        'download' => 'Скачать',
        'download_all' => 'Скачать все',
        'current' => 'Текущий',
        'start' => 'Начало',
        'format' => 'Формат экспорта',
        'with_models' => 'С торговыми предложениями',
        'with_thumbnails' => 'С изображениями',
    ],
    
    'product' => [
        'status' => 'Статус товаров',
        'all' => 'Все товары',
        'create' => 'Разместить товар',
        'disable_all' => '',
        'delete_all' => '',
    ],

    'request_product' => [
        'status' => 'Статус запросов',
        'all' => 'Все запросы',
        'create' => 'Разместить запрос',
        'disable_all' => '',
        'delete_all' => '',
    ],
];
