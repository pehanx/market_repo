<?php
    return [
        'cabinet' => [
            'cabinet' => 'Кабинет',
            'private_information' => 'Личные данные',
            'entrance_information' => 'Учетная запись',
            'all_profiles' => 'Все профили',
            'all_managers' => 'Все менеджеры',
            'all_products' => 'Все товары',
            'profile_create' => 'Создание профиля',
            'profile_managers' => 'Менеджеры профиля',
            'new_profile' => 'Новый профиль',
            'new_manager' => 'Новый менеджер',
            'creating_product' => 'Создание товара',
            'editing_product_model' => 'Изменение торгового предложения',
            'editing_product' => 'Изменение товара',
            'edit_profile' => 'Изменение профиля',
            'all_request_products' => 'Список запросов',
            'creating_request_product' => 'Создание запроса',
            'editing_request_product' => 'Изменение запроса',
            'export_view' => 'Экспорт',
            'export_create' => 'Экспортировать',
            'export_product_create' => 'Экспорт товаров',
            'export_request_product_create' => 'Экспорт запросов',
        ],
    ];
?>