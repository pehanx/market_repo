<?php

return [
    'my_account' => 'Аккаунт',
    'entrance_account' => 'Учетная запись',
    'profiles' => 'Профили',
    'admin_panel' => 'Панель администратора', 
    'users' => 'Пользователи',
    'managers' => 'Менеджеры',
    'all_managers' => 'Все менеджеры',
    'managers_profiles' => 'Менеджеры профилей',
    'all_profiles' => 'Все профили',
    'private_information' => 'Личные данные',
    'cabinet' => 'Кабинет',
    'products' => 'Товары',
    'all_products' => 'Все товары',
    'request_products' => 'Запросы на покупку',
    'all_request_products' => 'Список запросов',
    'exports' => 'Список экспортов',
];
