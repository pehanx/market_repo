import { NavigationUI, FormUI, ViewportUI } from "./modules/common.js";
import { CabinetUI } from "./modules/cabinet.js";
import { ProductUI } from "./modules/product.js";

require('./modules/categorySelect');

const naviUI = new NavigationUI();
const formUI = new FormUI();
const viewportUI = new ViewportUI();
const cabinetUI = new CabinetUI();
const productUI = new ProductUI();

$(document).ready(function(){
	naviUI.headerNavigation();
	naviUI.cabinetNavigation();
	naviUI.catalogCategories();
	naviUI.staticSidebarNavigation(); // навигация на статических страницах
	formUI.setFormInputsListeners();
	cabinetUI.init(); // используются прямые обращения к viewportUI
	productUI.init(); // инициализация элементов управления карточкой товара (запроса)
	window.viewportUI = viewportUI; // важно для работы cabinetUI
	$(".preloader").hide();
	$("body").css("overflow", "auto");
});