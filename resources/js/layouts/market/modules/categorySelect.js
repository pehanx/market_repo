(function ($) {

	// Позиционирование скрытого спииска категорий 
	$('.cabinet__content__category__li').on('click', function () {
		let $menuItem = $(this),
			$submenuWrapper = $('> .cabinet__content__category__wrapper', $menuItem);

		// grab the menu item's position relative to its positioned parent
		let menuItemPos = $menuItem.position();

		// place the submenu in the correct position relevant to the menu item
		$submenuWrapper.css({
			top: 0,
			left: menuItemPos.left + Math.round($menuItem.outerWidth() * 1.07)
		});
	});

	// Показываем скрытый список при клике по категории
	$('.cabinet__content__category__li').on("click", function (event) {

		event.stopPropagation()

		$(this).toggleClass("cabinet__content__category__li__active");
		$(this).children().toggleClass("cabinet__content__category__active");

	});

	// Скрываем списки неактивной категории
	$(".cabinet__content__category__li").on('click', function (event) {

		// Ролучаем скрытый список активной категории
		let children = ($(this).siblings()).children()

		//Скрываем списки неактивной категории
		$(this).siblings().removeClass("cabinet__content__category__li__active") //первый уровень
		children.removeClass("cabinet__content__category__active") // вложенные списки

	});

})(jQuery);