export class NavigationUI {
	constructor() {}
	headerNavigation() {
		$('.header__bottom__left').click(function(){
			$('.headerNavigation').toggleClass('active');
			$(this).find('.header__bottom__left__icon').toggleClass('active');
			// Получаем высоту списка категорий
			let headerNavigationMainHeight = $(".headerNavigation__main").height();
			// Устанавливаем мин. высоту для развернутого списка
			$(".headerNavigation__content__block").each(function(i, elem){
				$(this).css("min-height", `${headerNavigationMainHeight}`);
			});
			// Ловим клики не по списку категорий и скрываем его
			$("body").on("click", function (e) {
				let headerBottomLeft = document.querySelector(".header__bottom__left");
				let headerNavigationMain = document.querySelector('.headerNavigation__main');
				if(e.target.parentNode !== headerNavigationMain && e.target.parentNode !== headerBottomLeft){
					$(".header__bottom__left__icon").removeClass("active");
					$(".headerNavigation").removeClass("active");
				}
			});
		});
		$('.headerNavigation__main__item').hover(function(){
			let navigationBlock = $(this).attr('data-content-block');
			$(this).addClass('active').siblings('div').removeClass('active');
			$('.headerNavigation__content__block').removeClass('active');
			$('#headerNavigation__content__block-'+navigationBlock).addClass('active');
		});	
	}
	cabinetNavigation() {
		$('.sidebar__menu__item').click(function(){
			let navigationBlock = $(this).parents('.sidebar');
			if( $(navigationBlock).is('.sidebar-hidden') ) {
				$(navigationBlock).removeClass('sidebar-hidden').find('.sidebar__menu__item__expand').addClass('active');
			}
			$(this).toggleClass('sidebar__menu__item-expanded');
		});
	}
	catalogCategories() {
		$('.catalog__categories__block__title').click(function(){
			$(this).parent('.catalog__categories__block').toggleClass('active');
		});

        $('body').on('click', '.catalog__categories__block__links__showAll', function(e){

			$(this).closest('ul').find(".overflow-links").removeClass('d-none');

			$(this).text($(this).data('text-hide'));

			$(this).removeClass('catalog__categories__block__links__showAll')
					.addClass('catalog__categories__block__links__hideAll');
		
		});

		$('body').on('click', '.catalog__categories__block__links__hideAll', function(e){

			$(this).closest('ul').find(".overflow-links").addClass('d-none');

			$(this).text($(this).data('text-show'));

			$(this).removeClass('catalog__categories__block__links__hideAll')
				.addClass('catalog__categories__block__links__showAll')
				
			//$(this).toggle();
        });
	}
	/* tabsInit() { 
		let tabsElements = $('.tabs');
		if(tabsElements.length > 0) {
			$(tabsElements).each(function(index, el) {
				$(el).lightTabs();
			});
		}
	} */
	staticSidebarNavigation() {
		$('.static__sidebar__list__title').on('click', function(){
			$(this).toggleClass('static__sidebar__list__title-show').parents('.static__sidebar__list').toggleClass('static__sidebar__list-show');
		});
		$('.static__sidebar__list__ul__title').on('click', function(){
			$(this).toggleClass('static__sidebar__list__ul__title-show').siblings('.static__sidebar__list__ul__sublist').toggleClass('static__sidebar__list__ul__sublist-show');;
		});
	}
}

export class FormUI {
	constructor() {
        this.checkInputValue();
    }
	setFormInputsListeners() {
		$('body')
            .on('focusin', '.form__row__input', function(){
                let formRow = $(this).parent('.form__row');

                if(!$(formRow).is('.form__row-hasText')) {
                    $(this).parent('.form__row').addClass('form__row-hasText');	
                }
            })
            .on('focusout', '.form__row__input', function(){
                let inputValue = $(this).val(),
                    formRow = $(this).parent('.form__row');

                if(inputValue.length == 0) {
                    $(formRow).removeClass('form__row-hasText');
                }
            });
        
		$('.form__row__showPassword').on('click', function(){
			$(this).toggleClass('on');
			if( $(this).is('.on') ){
				$(this).siblings('.form__row__input').attr('type', 'text');
			} else {
				$(this).siblings('.form__row__input').attr('type', 'password');
			}
		});
		$('.form__row__selectorColor__colorWrapper').on('click', function(){
			$(this).toggleClass('form__row__selectorColor__colorWrapper-selected');
		});
		$('.catalog__filterColors__colorWrapper').on('click', function(){
			$(this).toggleClass('catalog__filterColors__colorWrapper-selected');
		});
	}
    checkInputValue(){
        $('.form__row__input').each((i, item) => {
            if($(item).val().length !== 0){
                $(item).parent('.form__row').addClass('form__row-hasText');
            } 
        });
    }
}

// Функции для управления вьюпортом
export class ViewportUI { 
	constructor() {
		this.keys = {37: 1, 38: 1, 40: 1, 32: 1, 33: 1, 34: 1, 35: 1, 36: 1}
	}
	getViewportPosition() {
		let posTop = (window.pageYOffset !== undefined) ? window.pageYOffset : (document.documentElement || document.body.parentNode || document.body).scrollTop;
		let posLeft = (window.pageXOffset !== undefined) ? window.pageXOffset : (document.documentElement || document.body.parentNode || document.body).scrollLeft;
		let posBottom = (window.pageYOffset !== undefined) ? window.pageYOffset + window.innerHeight : (document.documentElement || document.body.parentNode || document.body).scrollTop + window.innerHeight;
		return {posTop, posBottom, posLeft};
	}
	preventDefault(e) {
		e = e || window.event;
		if (e.preventDefault) 
				e.preventDefault();
		e.returnValue = false;
	}
	preventDefaultForScrollKeys(e) {
		if(keys[e.keyCode]) {
			preventDefault(e);
			return false;
		}
	}
	disableScroll() {
		if(window.addEventListener)
			window.addEventListener('DOMMouseScroll', this.preventDefault, false);
		document.addEventListener('wheel', this.preventDefault, {passive: false}); // Disable scrolling in Chrome
		window.onwheel = this.preventDefault;
		window.onmousewheel = this.preventDefault;
		window.ontouchmove = this.preventDefault;
		document.onkeydown = this.preventDefaultForScrollKeys;
	}
	enableScroll() {
		if(window.removeEventListener)
			window.removeEventListener('DOMMouseScroll', this.preventDefault, false);
		document.removeEventListener('wheel', this.preventDefault, {passive: false});
		window.onmousewheel = document.onmousewheel = null;
		window.onwheel = null;
		window.ontouchmove = null;
		document.onkeydown = null;
	}
}

// Утилиты
export class Utils {
	checkSessionStorage() {
		// https://developer.mozilla.org/en-US/docs/Web/API/Window/sessionStorage
		let test = 'test';
		try {
			sessionStorage.setItem(test, test);
			sessionStorage.removeItem(test);
			return true;
		} catch(e) {
			return false;
		}
	}
	checkLocalStorage() {
		// https://developer.mozilla.org/en-US/docs/Web/API/Window/localStorage
		let test = 'test';
		try {
			localStorage.setItem(test, test);
			localStorage.removeItem(test);
			return true;
		} catch(e) {
			return false;
		}
	}
}