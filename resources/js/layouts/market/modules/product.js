// Карточка товара (запроса)
export class ProductUI {
	constructor() {}
	productSliders() {
		// Элементы для работы слайдеров
		let mainSlider = $('.product__card__image'),
			mainSliderControl = $('.product__card__imageControl');

		// Слайдер с главным изображением
		mainSlider.owlCarousel({
			items: 1,
			margin: 5,
			loop: true,
			nav: false,
			dots: false
		});

		// Управляющий слайдер
		mainSliderControl.owlCarousel({
			items: 5,
			margin: 5,
			loop: false,
			nav: true,
			dots: false,
			navText: ['',''],
		});

		/***
		 * Управление слайдером с большими изображениями при помощи слайдера 
		 * с маленькими изображениями, который находится ниже.
		 ***/
		$('.product__card__imageControl__item').on('click', function(){
			mainSlider.trigger('to.owl.carousel', [$(this).attr('data-go-slider'), 250]);
		});

		$('.product__card__model__items__element').on('click', function(){
			mainSlider.trigger('to.owl.carousel', [$(this).attr('data-go-slider'), 250]);
			$('.product__model__info').hide();
			$('#product__main__info').hide();
			$(`.product__model__info[data-model="${$(this).data('id')}"]`).show();
		});

		$(document).scroll(function () {
			var scrollCard = 320;
			var scrollToTop = $(this).scrollTop();

			if (scrollToTop > scrollCard) {
				$('.product__card__blockContact').addClass('sticky');
			} else {
				$('.product__card__blockContact').removeClass('sticky');
			}
		});

	}
	init() {
		this.productSliders();
	}
}