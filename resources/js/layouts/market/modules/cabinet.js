export class CabinetUI {
	constructor() {
	}
	getHeaderHeight() {
		return $('header').height();
	}
	selectProductsInCabinet() {

		$('.cabinet__products__checkboxActions__select').on('click', function(){
			$(this).find('.cabinet__products__checkboxActions__select__items').toggleClass('cabinet__products__checkboxActions__select__items-active');
		});

		$(document).on('click', function(e) {
			let container = $(".cabinet__products__checkboxActions__select");
			let submenu = $(".cabinet__products__checkboxActions__select__items");
			if (!container.is(e.target) && container.has(e.target).length === 0)
				submenu.removeClass('cabinet__products__checkboxActions__select__items-active');
		});

	}
	createSalerProfile() {
		$('.cabinet__hidden__title').on('click', function(){
			$(this).parents('.cabinet__hidden').toggleClass('cabinet__hidden-show');
		});
	}
    // Инпут в загрузке документов
	uploadInputs() {
		$('.form__row__uploadFile input[type="file"]').on('change', function(e){
			let textLabel = $(this).siblings('.form__row__uploadFile__label').find('.form__row__uploadFile__text');
            let fileNames = '';
			if(this.files.length > 0) {
                for(let index in this.files) {
                    if(!isNaN(index)) {
                        fileNames += `${+index + 1}.&nbsp;&nbsp;${this.files[index].name};<br><br>`;
                    }
                }
			} 
            $(textLabel).html(fileNames);
		});
	}
	showForm__hideUnhide() {
		$('.cabinet__unhide').on('click', function(){
			$(this).parents('.cabinet__hideArea').find('.cabinet__hide').toggleClass('cabinet__hide-unactive');
			$(this).toggleClass('cabinet__unhide-active');
		});
	}
	autosizeTextarea(){
		let text = $('.chat__input__text');
		text.each(function(){
			$(this).attr('rows',1);
			resize($(this));
		});
		text.on('input', function(){
			resize($(this));
		});
		text.on('click', function(){
			resize($(this));
		});
		function resize ($text) {
			$text.css('height', 'auto');
			$text.css('height', $text[0].scrollHeight+'px');
			if($text.css('height') === '0px') $text.css('height', 'auto');
		}
	}
	init() { // Инициализация
		this.selectProductsInCabinet();
		this.createSalerProfile(); // Механика страницы создания профиля продавца
		this.uploadInputs();
		this.showForm__hideUnhide();
		this.autosizeTextarea();
	}
}