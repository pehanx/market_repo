require('./bootstrap');
require('../../node_modules/bootstrap-datepicker/dist/locales/bootstrap-datepicker.ru.min.js');
require('../../node_modules/bootstrap-datepicker/dist/locales/bootstrap-datepicker.en-GB.min.js');
require('../../node_modules/jquery.fancytree/dist/modules/jquery.fancytree.edit.js');
require('../../node_modules/jquery.fancytree/dist/modules/jquery.fancytree.filter.js');
require('../../node_modules/jquery.fancytree/dist/modules/jquery.fancytree.multi.js');
require('../../node_modules/jquery.fancytree/dist/modules/jquery.fancytree.table.js');
require('../../node_modules/jquery.fancytree/dist/modules/jquery.fancytree.glyph.js');
require('../../node_modules/jquery.fancytree/dist/modules/jquery.fancytree.columnview.js');
require('../../node_modules/summernote/lang/summernote-ru-RU.js');
require('../../node_modules/select2/dist/js/i18n/ru.js');
require('../../node_modules/select2/dist/js/i18n/zh-CN.js');
//require('../../node_modules/select2/dist/js/i18n/de.js');
//require('../../node_modules/select2/src/js/select2/i18n/de.js');

Vue.component(
    'passport-clients',
    require('./components/passport/Clients.vue').default
);

Vue.component(
    'passport-authorized-clients',
    require('./components/passport/AuthorizedClients.vue').default
);

Vue.component(
    'passport-personal-access-tokens',
    require('./components/passport/PersonalAccessTokens.vue').default
);