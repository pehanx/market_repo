class Info
{
    /**
     * Читает уведомления info_message из session
     */
    constructor($modal)
    {
        this.$modal = $modal;
    }

    /**
     * @param string body - текст сообщения
     * @param string title - заголовок сообщения
     * @param string type - тип сообщения
     */
    show(body = '', title = '', type = 'info')
    {
        this.$modal
            .find('.modal-body')
            .html(body);
        
        if(title) 
        {
            this.$modal
                .find('.modal-header')
                .html(title);
        }
        
        this.$modal
            .modal('show');
    }
}