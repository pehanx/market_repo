import AbstractBaseParams from "./extensions/AbstractBaseParams.js"

export default class InputFileHandler extends AbstractBaseParams
{
    /*  Если добавляем в инпут файлы, которые в нем уже есть. 
     *  Но при этом из дома пользователь удалил некоторые из них, onchange не сработает.
     */
    onchangeTrigger(eventButton)
    {
        document.body.onfocus = function(eventWindow) {
            let inputId = $(eventButton.target).closest('label').attr('for');
            this.countFiles = $(`#${inputId}`).prop('files').length;
            $(`#${inputId}`).trigger('change');
            document.body.onfocus = null;
        } 
    }
    
    renderFile(e, data) 
    {
        let $inputFile = $(e.currentTarget);
        
        let expression = '';
        
        if(window.navigator.userAgent.match('Windows') !== null)
        {
            expression = (!!!e.isTrigger && $(e.currentTarget).prop('multiple') && $inputFile.prop('files').length === this.countFiles);
            
        } else 
        {
            expression = (!!!e.isTrigger && $(e.currentTarget).prop('multiple'));
        }
        
        if(expression)
        {
            return;
        }

        let inputName = $inputFile.attr('name');
        let availableExtensions = ['jpg', 'jpeg', 'png', 'JPG', 'JPEG', 'PNG']; 
        let $form = $(e.currentTarget.form);
        
        this.countFiles = $inputFile.prop('files').length;
        
        // Чистим контейнер определенного input
        $form.find(`div[data-input-name="${inputName}"]`)
            .closest(`.${data.containerImgs}`)
            .remove();

        $($inputFile.prop('files')).each(function(i, file){ 
                
            // Проверка расширения файла
            let fileExtension = file.name.split(".").splice(-1, 1)[0];
            
            if(availableExtensions.includes(fileExtension)) {
                // Отображение файлов в превью
                let reader = new FileReader();
                
                reader.onload = function(e) {
                    let $container = (i === 0) ? 
                        $(`<div class="${data.containerImgs}">`) : 
                        $form
                            .find(`div[data-input-name="${inputName}"]`)
                            .closest(`.${data.containerImgs}`);
                    
                    let $wrapper = $(`<div class="${data.wrapperImg}">`)
                        .append($('<div>', { 
                            'data-input-name':inputName, 
                            'data-file-key': i, 
                            class: `${data.deleteFile}`, 
                        }))
                        .append($('<div>', {
                            'data-file-key': i, 
                            class: `${data.img}`,
                            style: `background-image: url(${e.target.result})`
                        }));
                    
                    $container.append($wrapper);

                    if(i === 0)
                    {
                        $inputFile.before($container); // Вставили перед инпутом контейнер с изображениями
                    }
                    
                }
                
                reader.readAsDataURL(file); // Отрисовали
                
            } else 
            {
                $inputFile.val('');
                console.warn('File extension not supported — "' + file.name + '"');
            }
        });      
    }
    
    deleteFile(mutations, eventName, data) 
    {
        for(let mutation of mutations) 
        {
            for(let node of mutation.addedNodes) 
            {
                if (!(node instanceof HTMLElement)) 
                {
                    continue;
                }

                $(node).find(`.${data.deleteFile}`).on(eventName, (e) => {
                    
                    let inputName = $(e.currentTarget).data('input-name');
                    let fileListKey = $(e.currentTarget).data('file-key');
                    let $inputFile = $(e.currentTarget).closest('form').find(`[name="${inputName}"]`);
                    let files =  $inputFile.prop('files');
                    let $container = $(e.currentTarget).closest(`.${data.containerImgs}`);
                    
                    if(!!files && !!files[fileListKey])
                    {
                        files[fileListKey].delete = true; 
                        $(e.currentTarget).closest(`.${data.wrapperImg}`).remove(); // Кнопка с крестом и картинка в одном диве
                    }
                    
                    let hasFile = $(`[data-input-name="${inputName}"]`).length;
                    // Если файлов больше нет - чистим value
                    if(hasFile === 0)
                    {
                        $inputFile.val('');
                        $container.remove();
                    }
                });
            }
        }
    }
    
    getClassName() 
    {
        return 'InputFileHandler';
    }
}