class Cabinet
{
    constructor () 
    {
        this.actions = {
            'deleteFile': this._setDeletedIdFile,
            'deleteProfile': this._showDestroyModalWithData,
            'approveProfile': this._showApproveModalWithData,
            'disableProfile': this._showDisableModalWithData,
            'chooseTypeLocal': this._showChooseTypeLocalModalWithData.bind(this),
            'setCategory': this._submitFormWithCategory,
            'toogleFilterProducts': this._toogleFilterProducts,
            'deleteProductOrModel': this._showDestroyModalWithData,
            'disableProduct': this._showDisableModalWithData,
            'makeActionProduct': this._showActionModalWithData.bind(this), 
            'toogleActionElements': this._toogleActionElements,
            'setCheckedElementType': this._setCheckedElementType,
            'setCheckedElementParentId': this._setCheckedElementParentId,
            'exportElements': this._showExportModalWithData,
            'updateExportIndex': this._updateExportIndex,
            'resetFavoriteFilters': this._resetFavoriteFilters,
            'toogleActiveElements': this._toogleActiveElements,
            'downloadExports': this._downloadExports,
            'restoreImportedColumn': this._restoreImportedColumn,
            'removeImportedColumn': this._removeImportedColumn,
            'selectImportedColumn': this._selectImportedColumn,
            'sendImportForm': this._sendImportForm,
            'disablePage': this._showDisableModalWithData,
            'deletePage': this._showDestroyModalWithData,
        };
    }

    init (documentSelector, eventName, action) 
    {
        $(documentSelector).on(eventName, this.actions[action]);
        //$(documentSelector).on(eventName, (e)=>this.actions[action](e));
    }
    
    _setDeletedIdFile(e)
    {
        let idFile = $(this).data('id');
        let input = $('<input>', {type: 'hidden',  name:'delete_files[]', value: idFile});
        $(this).after(input);
        $(this).hide();
        $(`[data-id-file='${idFile}']`).hide();
    }
    
    _showDestroyModalWithData(e)
    {
        $('#destroy-modal')
            .find('#destroy-modal-form')
            .attr({action: $(this).data('route'), method:'POST'});  
        
        $('#destroy-modal')
            .modal('show')
            .find('.modal-body')
            .html($(this).data('text'));
    }

    _makeSubmitExportButton(e)
    {
        if($($(e)[0].target).attr('data-for') === "export_all")
        {
            let $modalForm = $('#export-modal').find('#export-modal-form');
            axios
                .post($modalForm.attr('action'), new FormData($modalForm[0]), {
        
                })
                .then(response => {
                    
                })
                .catch (error => {
        
                });
        }
        else
        {
            $('#export-modal').find('#export-modal-form').submit();
        }
    }

    _showExportModalWithData(e)
    {
        let isRequestProduct = $('.export-page')
            .data('route')
            .split('/')
            .indexOf('request_product');
        
        if(isRequestProduct !== -1)
        {
            $('#export-modal')
                .find('#export-modal-form')
                .find(".models-wrapper")
                .addClass('d-none');

            $('#export-modal')
                .find('#export-modal-form')
                .find("[name='with_models']")
                .prop('disabled','true');
        }
        
        $('#export-modal')
            .find('#export-modal-form')
            .find("[name='elements_id']")
            .val($(this).attr('data-elements'));
            
        $('#export-modal')
            .find('#export-modal-form')
            .attr({action: $(this).data('route')});   
            
        $('#export-modal')
            .modal('show');
    }

    _updateExportIndex()
    {
        let dataKeys = ['progress', 'id', 'downloads'];
        
        axios
            .get( $(this).data('route') )
            .then(response => {
                
                for (let index = 0; index < response.data.length; index++) 
                {
                    for (let key in response.data[index])
                    {
                        if ( +dataKeys.indexOf(key) === -1)
                        {
                            console.log('undefined_index', key);
                            return null;
                        }
                    }

                    $('#export-'+response.data[index].id)
                            .find('progress')
                            .attr('value', response.data[index].progress)
                            .text(response.data[index].progress);
                        
                    if(response.data[index].progress == 100)
                    {
                        let checkFilesExist = $('#export-'+response.data[index].id)
                            .find('.navbar-nav')
                            .find('.exports')
                            .length;

                        let downloadFiles = [];
                                                
                        for ( let indexFile = 0; indexFile < response.data[index].downloads.length; indexFile++ )
                        {
                            let $downloadFileLink = $('<a>', {

                                class: "exports",
                                href: response.data[index].downloads[indexFile].path,
                                text: response.data[index].downloads[indexFile].title
                            }); 

                            downloadFiles.push($downloadFileLink);
                        }
                        
                        if(+checkFilesExist === 0)
                        {
                            $('#export-' + response.data[index].id)
                                .find('.navbar-nav')
                                .prepend(downloadFiles);
                            
                            $('.download-all')
                                .removeClass('d-none');
                        }
                    }
                }
            })
            .catch (error => {
                if(typeof(error.response.status !== 'undefined') && +error.response.status === 422)
                {
                    console.log(error);
                }
            });
    }

    _downloadExports(e)
    {
        axios
        .get($(this).data('link'))
        .then(response => {

            for (let index = 0; index < response.data.length; index++)
            {   
                let link = document.createElement('a');
                link.setAttribute('href', response.data[index]);
                document.body.appendChild(link);
                link.click();                
                document.body.removeChild(link);
            }

        });
    }
    
    _showApproveModalWithData(e)
    {
        $('#approve-modal')
            .find('#approve-modal-form')
            .attr({action: $(this).data('route')});  
        
        $('#approve-modal')
            .modal('show')
            .find('.modal-body')
            .html($(this).data('text'));
    }
    
    _showDisableModalWithData(e)
    {
        $('#disable-modal')
            .find('#disable-modal-form')
            .attr({action: $(this).data('route')});  
        
        $('#disable-modal')
            .modal('show')
            .find('.modal-body')
            .html($(this).data('text'));
    }
    
    _showChooseTypeLocalModalWithData(e)
    {
        this._bindChoseTypeLocalModal('#choose-type-local-modal');

        $('#choose-type-local-modal')
            .modal('show');
    }
    
    _bindChoseTypeLocalModal(modalSelector)
    {
        $(modalSelector).one('click', '#choose-type-locale', function () {           
           let chosenLocalisation = $(modalSelector).find('.select2.localisations').val();
           let chosenProfileType = $(modalSelector).find('input:checked').val();
           
            // Удаляем ненужную форму из DOM
            $('.forms-wrapper').not(`#${chosenProfileType}`).remove();
            $(`#${chosenProfileType}`).removeClass('d-none');
            $(`#${chosenProfileType}`).find("[name='profile_type']").val(chosenProfileType);
            $(`#${chosenProfileType}`).find("[name='localisation']").val(chosenLocalisation);
        }); 
    }
    
    _submitFormWithCategory(e)
    {
        let dataRoute = $(e)[0].currentTarget.dataset.route;
        let categotyId = treeCategories.getActiveNode().key;
        
        let action = dataRoute.replace('category', categotyId);
        
        $('#set-category-form')
            .attr({action: action})
            .trigger('submit');
    }
    
    _toogleFilterProducts(e)
    {
        let $eTarget = $(e)[0].target;
        let $fullFilter = $('[data-area]').not('#'+$eTarget.id);
        let flag = $fullFilter.attr('data-area');

        if(flag === 'hide')
        {
            $fullFilter
                .toggleClass('cabinet__content__filter__search__more-active')
                .attr('data-area', 'show')
                .find('.filter')
                .prop('disabled', false);
            
            $($eTarget)
                .toggleClass('cabinet__content__filter__showDetailFilter-active')
                .attr('data-area', 'show')
                .text($eTarget.dataset.hide);
                
            $('[name="full_filter"]').val('show');
            
        } else if(flag === 'show')
        {
            $fullFilter
                .toggleClass('cabinet__content__filter__search__more-active')
                .attr('data-area', 'hide')
                .find('.filter')
                .prop('disabled', true);
            
            $($eTarget)
                .toggleClass('cabinet__content__filter__showDetailFilter-active')
                .attr('data-area', 'hide')
                .text($eTarget.dataset.show);
                
            $('[name="full_filter"]').val('hide');
        }
    }
    
    _showActionModalWithData(e)
    {
        let $form = $('[id $= "action-form"]');
        
        if(!this._hasCheckedElement($form) && $(e)[0].currentTarget.dataset.actionInput.includes('_checked'))
        {
            return;
        };
        
        this._bindShowActionModal();
        
        $form
            .find('input[name="action"]')
            .val($(e)[0].currentTarget.dataset.actionInput);
        
        $('#action-modal')
            .modal('show')
            .find('.modal-body')
            .html($(e)[0].currentTarget.dataset.text);
    }
    
    _hasCheckedElement($form) 
    {
        let existCheckedElement = false
        
        $form.find('.action-elements').each(function(){
            if(!!$(this).prop('checked') === true)
            {
                existCheckedElement = true;
                return false;
            }
        });
        
        return existCheckedElement;
    }
    
    _bindShowActionModal()
    {
        $('#action-modal').on('hidden.bs.modal', function () {
            $(this).find('button.submit').unbind('click');
        }); 
        
        $('#action-modal').find('button.submit').on('click', function () {
            $('#product-action-form')
                .trigger('submit');
        }); 
    }
    
    _toogleActionElements()
    {
        $(".action-elements").prop('checked', $(this).prop('checked'));
        $(".action-elements-type").attr('disabled', !$(this).prop('checked'));
    }
    
    _setCheckedElementType()
    {
        let name = $(this).attr('name');
        let selectorName = name.substr(0, name.indexOf('[id]') )+'[type]';
        $("[name ^= '"+selectorName+"']").attr('disabled', !$(this).prop('checked'));
    }
    
    _setCheckedElementParentId()
    {
        let name = $(this).attr('name');
        let selectorName = name.substr(0, name.indexOf('[id]') )+'[parent_id]';
        $("[name ^= '"+selectorName+"']").attr('disabled', !$(this).prop('checked'));
    }
    
    _resetFavoriteFilters () 
    {
        let searchParams = new URLSearchParams(location.search);
        let trash = [];
        let redirect = '';

        for (let param of searchParams) 
        {
            // Эти фильтры не сбрасываеам
            if( (param[0] === 'is_active' && param[1] === 'true') || (param[0] === 'order_by') )
            {
                continue;
                
            } else
            {
                trash.push(param[0]);
            }
        }
        
        for (let name of trash) 
        {
            searchParams.delete(name);
        }
        
        if(searchParams.toString() !== '')
        {
            redirect = `${location.origin}${location.pathname}?${searchParams.toString()}`;
            
        } else
        {
            redirect = `${location.origin}${location.pathname}`;
        }
        
        location.href = redirect;
    }
    
    _toogleActiveElements () 
    {
        let flag = ($(this).prop('checked') === true) ? 'true' : '';
        let searchParams = new URLSearchParams(location.search);
        let redirect = '';
        
        if(flag)
        {
            searchParams.set($(this).attr('name'), flag);
            
        } else
        {
            searchParams.delete($(this).attr('name'));
        }
        
        if(searchParams.toString() !== '')
        {
            redirect = `${location.origin}${location.pathname}?${searchParams.toString()}`;
            
        } else
        {
            redirect = `${location.origin}${location.pathname}`;
        }
        
        location.href = redirect;
    }

    _restoreImportedColumn () 
    {
        $(this).prev()
            .show();

        $(this).hide();

        $(this).closest('tr')
            .find('.column')
            .parent()
            .show()

        $(this).closest('tr')
            .clone()
            .appendTo('#import_added');

        $('#import_added').find('.column')
            .last()
            .val('')
            .select2({
                width: '100%',
                placeholder: $(this).data('placeholder')
            });
            
        $(this).closest('tr')
            .remove();
    }

    _removeImportedColumn () 
    {
        $(this).parent()
            .prev()
            .find('.column')
            .select2('destroy');

        $(this).next()
            .show();

        $(this).hide();

        $(this).closest('tr')
            .find('.column')
            .parent()
            .hide();

        $(this).closest('tr')
            .clone()
            .appendTo('tbody.import_removed');
            
        $(this).closest('tr')
            .remove();
    }

    _selectImportedColumn () 
    {
        let endOfValue = ($(this).val()).indexOf('__');
        let endOfName = ($(this).attr('name')).indexOf(']') +1 ; // Для уборки дублей

        $(this).attr('name', $(this).attr('name').substr(0, endOfName));

        let newValue = $(this).val().substr(0, endOfValue); 

        $(this).attr('name', $(this).attr('name') + "[" + newValue + "]" ); //Получаем чистый name 

        $('.column')
            .not($(this))
            .filter(function(endOfValue){
                if($(this).val() !== null)
                {
                    endOfValue = ($(this).val()).indexOf('__');
                    return $(this).val().substr(0, endOfValue) == newValue
                }
            })
            .val('')
            .select2({
                width: '100%',
                placeholder: $(this).data('placeholder')
            });
    }

    _sendImportForm()
    {
        let test = $(this).prev();
        axios
            .post($(this).prev().attr('action'), new FormData(test[0]), {

            })
            .then(response => {

                let importId = response.data.import_id;
                let dataRoute = $(this).data('route');
                let action = dataRoute.replace('import_id', importId);

                window.location.replace(action);
            })
            .catch (error => {
                if (typeof(error.response.data) !== 'undefined' )
                {
                    for (let key in error.response.data) {
                        alert(error.response.data[key]);
                        return null;
                    }
                }
            });
    }
}