export default class Contacts
{
    /* Добавление в контакты */
    static addToContact(e)
    {
        let endpoint = $(e.currentTarget).data('route-contact');

        return axios
            .post(endpoint, {

            });
            
    }

    static removeContact(e)
    {
        let endpoint = $(e.currentTarget).data('route-contact');

        return axios
            .delete(endpoint, {

            });
    } 
}