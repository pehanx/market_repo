export default class Translate
{
    /* Перевод сообщения из чата */
    static translateMessage(e)
    {
        let message_id = $(e.currentTarget).data('message');

        return axios
            .post($(e.currentTarget).data('route'), {
                translate:true,
                message_id:message_id
            }, {})
    }
}