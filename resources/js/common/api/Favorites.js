export default class Favorites
{
    static addToFavorites(e)
    {
        axios
            .post($(e.currentTarget).data('route'), {
                
            })
            .then(response => {
                if(response.status === 201)
                {
                    location.reload();
                }
            })  
            .catch (error => {
                // TODO обработать ошибки
                if(error.response.status == 422)
                {
                   // Недопустимые параметры сервера
                }
                
                if(error.response.status == 404)
                {
                   // Уже добавлено в избранное
                }
            });
    }
    
    static removeFromFavorites(e)
    {
        axios
            .delete($(e.currentTarget).data('route'), {
                
            })
            .then(response => {
                if(response.status === 204)
                {
                    location.reload();
                }
            })  
            .catch (error => {
                // TODO обработать ошибки
                if(error.response.status == 422)
                {
                   // Недопустимые параметры сервера
                }
                
                if(error.response.status == 404)
                {
                   // Не найдено в избранном
                }
            });
    } 
}