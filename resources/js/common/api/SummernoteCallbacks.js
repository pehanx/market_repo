class SummernoteCallbacks
{
    static onImageUpload(file)
    {
        let editor = $(this);
        let endpoint = editor.data('route');
        let data = new FormData();

        data.append('file', file[0]);
        
        axios
            .post(endpoint, data)
            .then(response => {
                if(response.status === 201 && !!response.data.url && !!response.data.id)
                {
                    editor.summernote('insertImage', response.data.url);
                    let $insertedImage = $(`img[src="${response.data.url}"]`);
                    $insertedImage.attr('route', endpoint.replace('image', response.data.id));
                } 
            })
            .catch(error => {
                
                // TODO обработать ошибки
                if(!!error.response && !!error.response.status && error.response.status == 422)
                {
                   // Недопустимые параметры сервера
                }
                
            }); 
    }
    
    static onMediaDelete($media)
    {
        let editor = $(this);
        let endpoint = null;
        
        if($media.is('img'))
        {
            endpoint = $media.attr('route');
        }
        
        if(!endpoint) return;

        axios
            .delete(endpoint)
            .then(response => {
                if(response.status === 204)
                {
                    console.log('ok');
                } 
            })
            .catch(error => {
                // TODO обработать ошибки
                console.log(error);
            }); 
    } 
}