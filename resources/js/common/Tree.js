class Tree 
{
    constructor(selectors, source, extensions)
    {
        for(let selector in selectors)
        {
            this['$'+selector] = $(selectors[selector]);
        }

        this.source = source;
        this.extensions = extensions;
        this.options = {};
        
        this.setOptions();
        this.initTree();
    }
    
    setOptions()
    {
        this.options.source = this.source;
        this.options.extensions = this.extensions;
        this.options.postProcess = this.postProcess;
        
        if(this.extensions.indexOf('filter') !== -1)
        {
            this.options.quicksearch = false;
            this.options.filter = {
                autoApply: false,   // Re-apply last filter if lazy data is loaded
                autoExpand: false, // Expand all branches that contain matches while filtered
                counter: false,     // Show a badge with number of matching child nodes near parent icons
                fuzzy: false,      // Match single characters in order, e.g. 'fb' will match 'FooBar'
                hideExpandedCounter: true,  // Hide counter badge if parent is expanded
                hideExpanders: true,       // Hide expanders if all child nodes are hidden by filter
                highlight: true,   // Highlight matches by wrapping inside <mark> tags
                leavesOnly: false, // Match end nodes only
                nodata: !true,      // Display a 'no data' status node if result is empty
                mode: "hide"       // Grayout unmatched nodes (pass "hide" to remove unmatched node instead)
            };
        }
    }
    
    initTree()
    {
        this.$tree.fancytree(this.options);   
        this.fancyTree = $.ui.fancytree.getTree();
        this.initListeners();
    }

    postProcess(event, data)
    {
        
    }
    
    activate(event, data)
    {
        console.log(data);
    }
    
    initListeners()
    {
        if('$filter' in this)
        {
            $(this.$searchFilter).on("click", function(e){

                let find = $(this.$filter).val();
                this.fancyTree.filterNodes(find, this.options);

            }.bind(this)).focus();
        }
    }
    
    getActiveNode()
    {
        return this.fancyTree.activeNode;
    }
}