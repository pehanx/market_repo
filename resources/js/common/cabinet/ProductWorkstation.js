import AbstractWorkstation from "./../extensions/AbstractWorkstation.js"

export default class ProductWorkstation extends AbstractWorkstation
{
    constructor(params, inputFileHandler)
    {
        super(params);

        if(!!inputFileHandler && ('getClassName' in inputFileHandler) &&  inputFileHandler.getClassName() === 'InputFileHandler') 
        {
            this.services = {};
            this.services.inputFileHandler = inputFileHandler;
        } 
    }
    
    initProductModel(data) 
    {
        this.entities.productModel = data;
        this.entities.productModel.countModelForms = 0;
        this.entities.productModel.deletedModelPrifixes = [];
        
        this._initColorpicker(this.entities.productModel.$addModelForm); 
       
        if(!!this.services.inputFileHandler)
        {
            // Следим за всеми формами в parent
            this.services.inputFileHandler.initObservers({
            
                deleteFile: {
                    domSelector: this.entities.productModel.$addModelForm.parent()[0],
                    event: 'click',
                    callback: 'deleteFile',
                    // Передать извне
                    data: {
                        containerImgs: 'form__row__uploadImages__preview',
                        wrapperImg: 'form__row__uploadImages__preview__wrapper',
                        deleteFile: 'form__row__uploadImages__preview__image__remove',
                    }
                }
            }); 
        }
    }
    
    _initColorpicker ($form)
    {
        $form.find('select[name="color"]').simplecolorpicker({picker: true, theme: 'glyphicons'});
    }
    
    _initSelect ($form)
    {
        let language = this.entities.productModel.$addModelForm.find("select[name='available[type]']").data('language');

        $form
            .find("select[name='available[type]']")
            .select2({
                theme: "marketplace-forms",
                width: "style",
                language: language
            });
    }
    
    _initFileHandler ($form) 
    {
        $form.find('input[type="file"]').on('change', e => this.services.inputFileHandler.renderFile(e, this.services.inputFileHandler.params.renderFile.data));
    }
    
    deleteModelForm (e) 
    {
        let currentFormId = $(e.currentTarget).parents('form').attr('id');
        this.entities.productModel.deletedModelPrifixes.push($(e.currentTarget).parents('form').find('span.prefix').html());
        $(`#${currentFormId}`).remove();
    }
    
    addModelForm(e)
    {
        ++this.entities.productModel.countModelForms;
        let addModelFormClone = this.entities.productModel.$addModelFormClone.clone(); // Не используем оригинальный клон
        
        // Меняем id формы
        addModelFormClone
            .attr('id', `${addModelFormClone.attr('id')}-${this.entities.productModel.countModelForms}`);
        
        // Меняем label for 
        addModelFormClone
            .find('label')
            .each((index, item) => {
                $(item).attr('for', `${$(item).attr('for')}-${this.entities.productModel.countModelForms}`);
            });
            
        // Меняем id input
        addModelFormClone
            .find('input')
            .each((index, item) => {
                $(item).attr('id', `${$(item).attr('id')}-${this.entities.productModel.countModelForms}`);
            });
        
        // Меняем префикс артикула т.п.
        if(this.entities.productModel.deletedModelPrifixes.length === 0) // Берем наибольший префикс из форм в контейнере + 1
        {
            let prefixes = [];
            
            this.entities.productModel.$addModelForm
                .parent()
                .find('span.prefix')
                .each((index, item) => {
                    prefixes.push($(item).html());
                });
            
            addModelFormClone
                .find('span.prefix')
                .html(Math.max.apply(null, prefixes) + 1);
                
        } else // Берем наименьший префикс из удаленных форм
        {
            let minPrefix = String(Math.min.apply(null, this.entities.productModel.deletedModelPrifixes));
            
            this.entities.productModel.deletedModelPrifixes
                .splice(this.entities.productModel.deletedModelPrifixes.indexOf(minPrefix), 1);

            addModelFormClone
                .find('span.prefix')
                .html(minPrefix);
        }
            
        /* // Убираем иконку колорпикера и перепривязываем в _bindClonedForm
        addModelFormClone.find('.simplecolorpicker.icon').remove(); 
        
        // Убираем select2 и перепривязываем в _bindClonedForm
        addModelFormClone
            .find("select[name='available[type]']")           
            .removeAttr('data-live-search')
            .removeAttr('data-select2-id')
            .removeAttr('aria-hidden')
            .removeAttr('tabindex');
            
        addModelFormClone
            .find("select[name='available[type]'] option")
            .removeAttr('data-select2-id');
            
        addModelFormClone
            .find('span.select2')
            .remove();
        
        // Чистим инпут типа файл и его контейнер
        addModelFormClone.find('input[type="file"]').val(''); 
        addModelFormClone.find(`.${this.services.inputFileHandler.params.renderFile.data.containerImgs}`).remove(); */
        
        addModelFormClone.appendTo('.cabinet__createModel');
        this._bindClonedForm(addModelFormClone);
    }
    
    _bindClonedForm(addModelFormClone)
    {
        addModelFormClone.find('#delete-model').removeClass('d-none').on('click', e => this.deleteModelForm(e));
        this._initColorpicker(addModelFormClone); 
        this._initSelect(addModelFormClone);        
        this._initFileHandler(addModelFormClone); 
    }
    
    saveModels(e)
    {
        if($('.form__row').hasClass('form__row-error'))
        {
            $('.form__row')
                .removeClass('form__row-error')
                .find('.form__row__error')
                .html('');
        }
        
        let endpoint = $(e.currentTarget).attr('data-action');
        let $productModels = $("form[id |= '"+this.entities.productModel.$addModelForm.attr('id')+"']");
        let countForms = $productModels.length;
        
        $productModels.each(function(key) 
        {
            axios
                .post(endpoint, new FormData(this), {
                    
                })
                // В this - наша форма
                .then(response => {
                    // удаляем  из DOM форму с успешно созданной моделью. Если все модели сохранились без ошибок - редирект на просмотр карточки товара с моделями
                    --countForms;
                    this.remove();

                    if(+countForms === 0)
                    {   
                        //Показать модалку об успешном создании торгового предложения(предложений)
                        location.replace(location.href.replace('/add', ''));
                    }
                })  
                .catch (error => {
                    if(error.response.status == 422)
                    {
                        for(let name in error.response.data.errors) 
                        {
                            let selector = '';
                            if( name.indexOf('.') !== -1 )
                            {
                                selector =  `[name *= '${name.replace('.', '[')+']'}']`;
                                
                            } else
                            {
                                selector = `[name *= '${name}']`;
                            }
                                
                            $(this)
                                .find(selector)
                                .closest('.form__row')
                                .addClass('form__row-error')
                                .find('.form__row__error')
                                .html(error.response.data.errors[name]);
                        }
                    }
                });

        });
    }
}