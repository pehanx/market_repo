import AbstractBaseParams from "./extensions/AbstractBaseParams.js"

export default class RequestParams extends AbstractBaseParams
{
    searchItems(e, data) 
    {
        let inputName = (!!data && !!data.inputName) ? data.inputName : 'search';
        let term = $(e.currentTarget).parent().find(`input[name="${inputName}"]`).val().trim();
        let searchParams = new URLSearchParams(location.search);
        
        if(term)
        {
           searchParams.set(inputName, term);
           
        }else if(searchParams.has(inputName))
        {
            searchParams.delete(inputName);
        }
        
        let redirect = `${location.origin}${location.pathname}?${searchParams.toString()}`;
     
        location.href = redirect;  
        
    }
    
    // transparent - значение цвета по дефолту (не выбрано)
    filtrateItems(e) 
    {
        let filterParamsStr = $(e.currentTarget).closest('form').serialize();
        let locationSearchParamsStr = (location.search !== '') ? location.search.substr(1) : '';
        
        let searchFilterParams = new URLSearchParams(filterParamsStr);
        let searchLocaionParams = new URLSearchParams(locationSearchParamsStr);
        let trash = [];
        
        // Собираем пустые значения фильтров из сериализованной формы для дальнейшего удаления
        for (let param of searchFilterParams)
        {
            if(param[1] === '' || param[1] === 'transparent')
            {
                trash.push(param[0]);
            }
            
            // Убираем данный фильтр из параметров предыдущей строки запроса
            if(searchLocaionParams.has(param[0]))
            {
                searchLocaionParams.delete(param[0]);
            }
        } 
        
        // Удаляем пустые значения фильтров
        for (let name of trash) 
        {
            searchFilterParams.delete(name);
        }
        
        let redirect = `${location.origin}${location.pathname}?`;
        
        if(searchLocaionParams.toString() !== '')
        {
            redirect += searchLocaionParams.toString();
        }
        
        if(searchFilterParams.toString() !== '')
        {
            redirect += '&' + searchFilterParams.toString();
        }
        
        if(searchLocaionParams.toString() !== '' && searchFilterParams.toString() !== '')
        {
            redirect.slice(-1); 
        }
        
        location.href = redirect; 
    }
    
    sortItems(e) 
    {
        let locationSearchParamsStr = (location.search !== '') ? location.search.substr(1) : '';
        let redirect = `${location.origin}${location.pathname}`;

        let searchParams = new URLSearchParams(locationSearchParamsStr ? locationSearchParamsStr : '');
        searchParams.set($(e.currentTarget).attr('name'), $(e.currentTarget).val());
        redirect += `?${searchParams.toString()}`;
        
        location.href = redirect; 
    }
}