import AbstractBaseParams from "./extensions/AbstractBaseParams.js"

export default class InputFileHandler extends AbstractBaseParams
{
    /*  Если добавляем в инпут файлы, которые в нем уже есть. 
     *  Но при этом из дома пользователь удалил некоторые из них, onchange не сработает.
     */
    onchangeTrigger(eventButton)
    {
        document.body.onfocus = function(eventWindow) {
            let inputId = $(eventButton.target).closest('label').attr('for');
            $(`#${inputId}`).trigger('change');
            document.body.onfocus = null;
        } 
    }
    
    renderFile(e, data) 
    {
        let $inputFile = $(e.currentTarget);

        if(!!!e.isTrigger && $(e.currentTarget).prop('multiple') && window.navigator.userAgent.match('Chrome') )
        {
            return;
        }

        let inputName = $inputFile.attr('name');
        let availableExtensions = ['jpg', 'jpeg', 'png'];
        let $form = $(e.currentTarget.form);

        // Чистим контейнер определенного input
        $form.find(`div[data-input-name="${inputName}"]`)
            .closest(`.${data.containerImgs}`)
            .remove();

        let readedFiles = [];

        $($inputFile.prop('files')).each(function(i, file){ 
            // Проверка расширения файла
            let fileExtension = file.name.split(".").splice(-1, 1)[0];
            
            if(availableExtensions.includes(fileExtension)) {
                // Отображение файлов в превью
                let reader = new FileReader();
                
                reader.onload = function(e) {
                    let $container = (i === 0) ? 
                        $(`<div class="${data.containerImgs}">`) : 
                        $form
                            .find(`div[data-input-name="${inputName}"]`)
                            .closest(`.${data.containerImgs}`);
                    
                    let $wrapper = $(`<div class="${data.wrapperImg}">`)
                        .append($('<div>', { 
                            'data-input-name':inputName, 
                            'data-file-key': i, 
                            class: `${data.deleteFile}`, 
                        }))
                        .append($('<div>', {
                            'data-file-key': i, 
                            class: `${data.img}`,
                            style: `background-image: url(${e.target.result})`
                        }));
                    
                    readedFiles.push($wrapper);

                    //$container.append($wrapper);

                    if(i === 0)
                    {
                        $inputFile.before($container); // Вставили перед инпутом контейнер с изображениями
                        
                    }
                    
                }

                reader.readAsDataURL(file);// Отрисовали
                
                if(i === ( $inputFile.prop('files').length - 1) )
                {
                    setTimeout(() => {
                        for (let key in readedFiles)
                        {
                            let test = $form
                                .find(`input[name="${inputName}"]`)
                                .siblings(`.${data.containerImgs}`)
                                .last()
                                .get(0);
    
                            $(test).append(readedFiles[key].get(0));
    
                        }
                    }, 1000);
                }

            } else 
            {
                console.warn('File extension not supported — "' + file.name + '"');
            }
        }); 
        
        
    }
    
    deleteFile(mutations, eventName, data) 
    {
        for(let mutation of mutations) 
        {
            for(let node of mutation.addedNodes) 
            {
                if (!(node instanceof HTMLElement)) 
                {
                    continue;
                }

                $(node).find(`.${data.deleteFile}`).on(eventName, (e) => {
                    
                    let inputName = $(e.currentTarget).data('input-name');
                    let fileListKey = $(e.currentTarget).data('file-key');
                    let $inputFile = $(e.currentTarget).closest('form').find(`[name="${inputName}"]`);
                    let files =  $inputFile.prop('files');
                    let $container = $(e.currentTarget).closest(`.${data.containerImgs}`);
                    
                    if(!!files && !!files[fileListKey])
                    {
                        files[fileListKey].delete = true; 
                        $(e.currentTarget).closest(`.${data.wrapperImg}`).remove(); // Кнопка с крестом и картинка в одном диве
                    }
                    
                    let hasFile = $(`[data-input-name="${inputName}"]`).length;
                    // Если файлов больше нет - чистим value
                    if(hasFile === 0)
                    {
                        $inputFile.val('');
                        $container.remove();
                    }
                });
            }
        }
    }
}