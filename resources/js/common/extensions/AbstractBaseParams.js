export default class AbstractBaseParams
{
    constructor(params) 
    {
        if (this.constructor === AbstractBaseParams)
        {
            throw new TypeError('Abstract class "AbstractBaseParams" cannot be instantiated directly.'); 
        }
        
        this.observers = {};
        this.entities = {};
        this.params = params;
        
        this._initListeners();
    }

    _initListeners() 
    {
        for(let param in this.params)
        {
            this.params[param].jqSelector
                .off(this.params[param].event)
                .on(this.params[param].event, (e) => {
                        let callback = this.params[param].callback;
                        
                        if(!!this[callback] && typeof this[callback] === 'function')
                        {
                            if(!!this.params[param].data)
                            {
                                this[callback](e, this.params[param].data);
                                
                            } else 
                            {
                                this[callback](e); 
                            }
                        } 
                    });
        } 
    }
    
    initObservers(nodes) 
    {
        this.observers.params = nodes;
        
        for(let node in this.observers.params)
        {
            let callback = this.observers.params[node].callback;
            
            if(!!this[callback] && typeof this[callback] === 'function')
            {   
                let observerName = `${node}Observer`;
                
                this.observers[observerName] = new MutationObserver( (mutations) => {
                    
                    if(!!this.observers.params[node].data)
                    {
                        this[callback](mutations, this.observers.params[node].event, this.observers.params[node].data);
                        
                    } else 
                    {
                        this[callback](mutations, this.observers.params[node].event);
                    }
                }); 

                this.observers[observerName]
                    .observe(this.observers.params[node].domSelector, {
                        childList: true, 
                        subtree: true
                    });  
            } 
        }
    } 
}