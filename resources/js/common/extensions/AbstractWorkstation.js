import AbstractBaseParams from "./AbstractBaseParams.js"

export default class AbstractWorkstation extends AbstractBaseParams
{
    constructor(params) 
    {
        super(params);
        
        if (this.constructor === AbstractWorkstation)
        {
            throw new TypeError('Abstract class "AbstractWorkstation" cannot be instantiated directly.'); 
        }
    }
    
    submitForm (e)
    {
        let $form = $(e.currentTarget).closest('form');
        let $inputsFile = $form.find('input[type="file"]');
        let deleted = {};
        
        $inputsFile.each(function(num, input) {
            
            let inputName = $(this).attr('name').replace("[]", "");
            
            $( $(this).prop('files') ).each(function(i, file) { 
            
                if(!!file.delete && file.delete === true){
                    
                    if(deleted[inputName] === undefined) 
                    {
                        deleted[inputName] = [];
                    }
                        
                    deleted[inputName].push(file.name);
                }
                
            });
        });
        
        $form.append( $ ('<input>', {type: 'hidden' , name: 'deleted_files', value: JSON.stringify(deleted) }) );
        $form.submit();
    }
    // Удаление существующего на сервере файла
    deleteFile(e, data)
    {
        let idFile = $(e.currentTarget).data('id');
        let input = $('<input>', {type: 'hidden',  name:'delete_files[]', value: idFile});
        
        $(e.currentTarget).closest('form').append(input);
        
        // Удаляем контейнер, если в нём больше нет изображений
        let hasFile = $(`[data-id-file='${idFile}']`)
            .closest(`.${data.containerImgs}`)
            .find(`.${data.deleteFile}`)
            .length;

        if(hasFile === 1)
        {
            $(e.currentTarget)
                .closest(`.${data.containerImgs}`)  
                .remove(); 
                
        } else if (hasFile > 1)
        {
            $(e.currentTarget).closest(`.${data.wrapperImg}`).remove(); // Изображение + крестик
        }
    }
    
    renderVideo(e, data) 
    {
        if(!!$(e.currentTarget).val())
        {
            let url = new URL($(e.currentTarget).val());
            let searchParams = new URLSearchParams(url.search);
            let videoCode = searchParams.get('v');
            
            if(videoCode)
            {
                $(e.currentTarget)
                    .closest(`.${data.videoContainer}`)
                    .find(`.${data.videoRender}`)
                    .removeClass('d-none')
                    .prop('src', 'https://www.youtube.com/embed/' + videoCode);
            }
            
        } else 
        {
            $(e.currentTarget)
                .closest(`.${data.videoContainer}`)
                .find(`.${data.videoRender}`)
                .addClass('d-none')
        }
    }
}