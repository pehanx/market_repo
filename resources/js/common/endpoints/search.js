import SearchRequestParams from '../SearchRequestParams.js'
import Favorites from '../api/Favorites.js'

document.addEventListener('DOMContentLoaded', function() {
    let requestParams = new SearchRequestParams({

            filtrateItems: {
                jqSelector: $('#filters').find('button[type="button"]'),
                event: 'click',
                callback: 'filtrateItems'
            },
            
            sortItems: {
                jqSelector: $('#order-by'),
                event: 'change',
                callback: 'sortItems'
            }
        });
        
    $('.favorite-add-form').on('click', function (e) 
    {
        e.preventDefault();
        let $form = $(e.target).closest('form');
        
        if($form.length === 1)
        {
            $($form[0]).trigger('submit');
        }
    });
    
    $('.favorite-add').on('click', e => {
        e.preventDefault();
        Favorites.addToFavorites(e);
    });
    
    $('.favorite-remove').on('click', e => {
        e.preventDefault();
        Favorites.removeFromFavorites(e);
    }); 
});