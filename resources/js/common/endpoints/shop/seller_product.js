import RequestParams from '../../RequestParams.js'

document.addEventListener('DOMContentLoaded', function() {
    
    let requestParams = new RequestParams({
            
        searchItems: {
            jqSelector: $('#seller-search'),
            event: 'click',
            callback: 'searchItems',
            data: {
                inputName: 'seller_search',
            }
        },
        
        sortItems: {
            jqSelector: $('#order-by'),
            event: 'change',
            callback: 'sortItems'
        }
    });
});