import Favorites from '../../api/Favorites.js'
import Contact from '../../api/Contact.js'

document.addEventListener('DOMContentLoaded', function() {

    $('.favorite-add-form').on('click', function (e) 
    {
        e.preventDefault();
        let $form = $(e.target).closest('form');
        
        if($form.length === 1)
        {
            $($form[0]).trigger('submit');
        }
    });
    
    $('.favorite-add').on('click', e => {
        e.preventDefault();
        Favorites.addToFavorites(e);
    });
    
    $('.favorite-remove').on('click', e => {
        e.preventDefault();
        Favorites.removeFromFavorites(e);
    });
    
    $('.store__top__contacts').on('click', function(e){

        Contact.addToContact(e).then(response => {
            if(!!response && response.status === 201 || response.status == 200)
            {
                location.reload();
            }
        });
    });

});