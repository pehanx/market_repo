document.addEventListener('DOMContentLoaded', function() {
    
    $('.store__mainSlider').owlCarousel({
        items: 1,
        margin: 0,
        loop: true,
        nav: false,
        dots: true
    });

    $('.store__about__block__slider').owlCarousel({
        items: 1,
        margin: 0,
        loop: true,
        nav: true,
        dots: false,
        autoplay: true,
        autoplayTimeout: 3500,
        autoplayHoverPause: true,
        navText: ['',''],
    });
    
});