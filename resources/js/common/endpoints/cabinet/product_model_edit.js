import ProductWorkstation from '../../cabinet/ProductWorkstation.js'

document.addEventListener('DOMContentLoaded', function() {

    let productWorkstation = new ProductWorkstation({
   
        submitForm: {
            jqSelector: $('.submit-form'),
            event: 'click',
            callback: 'submitForm',
        },
        
        deleteFile: {
            jqSelector: $('.form__row__uploadImages__preview__image__remove'),
            event: 'click',
            callback: 'deleteFile',
            data: {
                containerImgs: 'form__row__uploadImages__preview',
                deleteFile: 'form__row__uploadImages__preview__image__remove',
            }
        }
    });
    
});