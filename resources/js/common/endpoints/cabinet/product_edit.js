import ProductWorkstation from '../../cabinet/ProductWorkstation.js'

document.addEventListener('DOMContentLoaded', function() {

    let productWorkstation = new ProductWorkstation({
   
        submitForm: {
            jqSelector: $('.submit-form'),
            event: 'click',
            callback: 'submitForm',
        },
        
        renderVideo: {
            jqSelector: $('.video-handler'),
            event: 'change',
            callback: 'renderVideo',
            data: {
                videoRender: 'video-render',
                videoContainer: 'form__row__youtube',
            }
        },
        
        deleteFile: {
            jqSelector: $('.form__row__uploadImages__preview__image__remove'),
            event: 'click',
            callback: 'deleteFile',
            data: {
                containerImgs: 'form__row__uploadImages__preview',
                deleteFile: 'form__row__uploadImages__preview__image__remove',
                wrapperImg: 'form__row__uploadImages__preview__wrapper',
            }
        }
    });
    
    $('.video-handler').trigger('change');
    
});