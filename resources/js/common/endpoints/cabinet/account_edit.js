import ProfileWorkstation from '../../cabinet/ProfileWorkstation.js'

document.addEventListener('DOMContentLoaded', function() {

    let profileWorkstation = new ProfileWorkstation({
   
        submitForm: {
            jqSelector: $('.submit-form'),
            event: 'click',
            callback: 'submitForm',
        },
        
        deleteFile: {
            jqSelector: $('.form__row__uploadImages__preview__image__remove'),
            event: 'click',
            callback: 'deleteFile',
            data: {
                containerImgs: 'form__row__uploadImages__preview',
                deleteFile: 'form__row__uploadImages__preview__image__remove',
            }
        }
    });
    
});