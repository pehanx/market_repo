import ProductWorkstation from '../../cabinet/ProductWorkstation.js'
import InputFileHandler from '../../InputFileHandler.js'

document.addEventListener('DOMContentLoaded', function() {
    
    let productWorkstation = new ProductWorkstation(
        {

            deleteModelForm: {
                jqSelector: $('#add-model-form').find('#delete-model'),
                event: 'click',
                callback: 'deleteModelForm',
            },
            
            addModelForm: {
                jqSelector: $('#add-model'),
                event: 'click',
                callback: 'addModelForm',
            },
            
            saveModels: {
                jqSelector: $('#save-models'),
                event: 'click',
                callback: 'saveModels',
            },
                
        }, 
        
        new InputFileHandler({
            
            renderFile: {
                jqSelector: $('input[type="file"]'),
                event: 'change',
                callback: 'renderFile',
                data: {
                    containerImgs: 'form__row__uploadImages__preview',
                    wrapperImg: 'form__row__uploadImages__preview__wrapper', 
                    img: 'form__row__uploadImages__preview__image',
                    deleteFile: 'form__row__uploadImages__preview__image__remove',
                }
            }
        })
    );

    productWorkstation.initProductModel({
        $addModelForm: $('#add-model-form'),
        $addModelFormClone: addModelFormClone,
    });

});