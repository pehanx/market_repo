import chat from '../../widgets/chat.js';
import chatSettings from '../../widgets/chatSettings';

document.addEventListener('DOMContentLoaded', function() {

    chat.init();
    chatSettings.init();

});