import ProductWorkstation from '../../cabinet/ProductWorkstation.js'

document.addEventListener('DOMContentLoaded', function() {

    let productWorkstation = new ProductWorkstation({
   
        submitForm: {
            jqSelector: $('.submit-form'),
            event: 'click',
            callback: 'submitForm',
        }
    });
    
});