import ProfileWorkstation from '../../cabinet/ProfileWorkstation.js'

document.addEventListener('DOMContentLoaded', function() {

    let profileWorkstation = new ProfileWorkstation({
   
        submitForm: {
            jqSelector: $('.submit-form'),
            event: 'click',
            callback: 'submitForm',
        },
        
        renderVideo: {
            jqSelector: $('.video-handler'),
            event: 'change',
            callback: 'renderVideo',
            data: {
                videoRender: 'video-render',
                videoContainer: 'form__row__youtube',
            }
        }
    });
    
    $('.video-handler').trigger('change');
    
});