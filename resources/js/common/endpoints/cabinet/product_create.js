import ProductWorkstation from '../../cabinet/ProductWorkstation.js'

document.addEventListener('DOMContentLoaded', function() {

    let productWorkstation = new ProductWorkstation({
   
        submitForm: {
            jqSelector: $('.submit-form'),
            event: 'click',
            callback: 'submitForm',
        },
        
        renderVideo: {
            jqSelector: $('.video-handler'),
            event: 'change',
            callback: 'renderVideo',
            data: {
                videoRender: 'video-render',
                videoContainer: 'form__row__youtube',
            }
        }
    });
    
    $('.video-handler').trigger('change');
    
});