import RequestParams from '../SearchRequestParams.js'
import InputFileHandler from '../InputFileHandler.js'

document.addEventListener('DOMContentLoaded', function() {
    let requestParams = new RequestParams({
            
            searchItems: {
                jqSelector: $('#search').find('button[type="button"]:not(.catalog-search)'),
                event: 'click',
                callback: 'searchItems',
                data: {
                    csrf: $('meta[name="csrf-token"]').prop('content'),
                    term: $('.select2-container').find('input[type="search"]')
                }
            }
        });
        
    // Вешаемся на инпуты с классом file-handler и на их предков
    let $inputsFile = $('input[type="file"].file-handler');

    let fileHandlerParams = {};
    
    fileHandlerParams.renderFile = {
        jqSelector: $('input[type="file"].file-handler'),
        event: 'change',
        callback: 'renderFile',
        data: {
            containerImgs: 'form__row__uploadImages__preview',
            wrapperImg: 'form__row__uploadImages__preview__wrapper', 
            img: 'form__row__uploadImages__preview__image',
            deleteFile: 'form__row__uploadImages__preview__image__remove',
        }
    };

    // Отрабатывает первый только на windows. Первоначально files пустой
    if(window.navigator.userAgent.match('Firefox') === null)
    {
        fileHandlerParams.onchangeTrigger = {
            jqSelector: $('.file-handler-multiple-button'),
            event: 'click',
            callback: 'onchangeTrigger',
        } 
    }

    if ($inputsFile.length !== 0)
    {
        let inputFileHandler = new InputFileHandler(fileHandlerParams);
        
        $inputsFile.each(function(index) {

            inputFileHandler.initObservers({
                
                deleteFile: {
                    domSelector: $(this).closest('form')[0],
                    event: 'click',
                    callback: 'deleteFile',
                    data: {
                        containerImgs: 'form__row__uploadImages__preview',
                        wrapperImg: 'form__row__uploadImages__preview__wrapper',
                        deleteFile: 'form__row__uploadImages__preview__image__remove',
                    }
                }
            }); 
        });
    }
   
    $('#set-locale').on('change', (e) => { location.href = $(e.target).val(); });

    $('#set-currency').on('change', (e) => { location.href = $(e.target).val(); });
    
    $("[data-libraries='lightgallery']").lightGallery({
        thumbnail:true,
    });

    $('.colorpicker-handle').simplecolorpicker({
        picker: true,
        theme: 'fontawesome'
    });

}); 