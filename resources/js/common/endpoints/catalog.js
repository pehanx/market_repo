import RequestParams from '../RequestParams.js'
import Favorites from '../api/Favorites.js'
import Contacts from '../api/Contact.js'

document.addEventListener('DOMContentLoaded', function() {
    let requestParams = new RequestParams({
            
            searchItems: {
                jqSelector: $('#search').find('button.catalog-search'),
                event: 'click',
                callback: 'searchItems'
            },
            
            filtrateItems: {
                jqSelector: $('#filters').find('button[type="button"]'),
                event: 'click',
                callback: 'filtrateItems'
            },
            
            sortItems: {
                jqSelector: $('#order-by'),
                event: 'change',
                callback: 'sortItems'
            }
        });

    $('.favorite-add-form').on('click', function (e) 
    {
        e.preventDefault();
        let $form = $(e.target).closest('form');
        
        if($form.length === 1)
        {
            $($form[0]).trigger('submit');
        }
    });
    
    $('.favorite-add').on('click', e => {
        e.preventDefault();
        Favorites.addToFavorites(e);
    });
    
    $('.favorite-remove').on('click', e => {
        e.preventDefault();
        Favorites.removeFromFavorites(e);
    });

    $('.contact-add-form').on('click',function(e)
    {
        e.preventDefault();
        let $form = $(e.target).closest('form');
        
        if($form.length === 1)
        {
            $($form[0]).trigger('submit');
        }
    });

    $('.contact-add').on('click', function(e)
    {
        Contacts.addToContact(e).then(response => {
            if(!!response && response.status === 201)
            {
                location.reload();
            }

            if(!!response && response.status == 200)
            {
                location.reload();
            }
        });
    });
    
    $('.contact-remove').on('click', function(e)
    {
        Contacts.removeContact(e).then(response => {
            if(!!response && response.status === 201)
            {
                location.reload();
            }

            if(!!response && response.status == 200)
            {
                location.reload();
            }
        });
    });
});