import RequestParams from "./RequestParams.js"

export default class SearchRequestParams extends RequestParams
{
    searchItems(e, data) 
    {
        let route = $(e.currentTarget).parent('form').attr('action');
        let categoryId = $(e.currentTarget).parent().find('#select').val().shift();
        let token = data.csrf;
        let term = data.term.val().trim();
        let redirect = '';
        let searchParams = new URLSearchParams();
        let entity = (new URLSearchParams(location.search)).get('entity');
        
        if(!token)
        {
           throw new Error('Page expired');
        }
        
        if(!!entity) 
        {
            searchParams.set("entity", entity);
        }
        
        if(token && term && !categoryId)
        {
           searchParams.set("search", term);
           searchParams.set("_token", token);
           
           redirect = `${route}?${searchParams.toString()}`;
        } 
        
        if(token && categoryId)
        {
           searchParams.set("category", categoryId);
           searchParams.set("_token", token);
           
           redirect = `${route}?${searchParams.toString()}`;
        }

        // Сбрасываем выбранный option перед redirect
        $(e.currentTarget).parent().find('#select').val('');
        
        location.href = redirect;
    } 
}