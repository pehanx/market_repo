import ChatHelper from '../api/Chat';
import Contacts from '../api/Contact';
import Translate from '../api/Translate';

export default (function(){
    
    let rendered = false;
    
    let message_input_to = '#test input[name="profile_id"]'; //Инпут с указанием владельца (сейчас пока profile_id)
    let chat_search = '#chat_search'; //Поиск по чату, который оборачивается в select2

    // Блоки в которые аппендятся сообщения, чаты итд
    let chat_place = '.tabs__content__chats'; // Место для отображения чатов
    let contact_place = '.tabs__content__contacts'; // Место для отображения контактов
    let search_place = '.tabs__content__search'; //Место для отображения результатов поиска
    let messages_content = '.chat__content'; // div в котором будут показываться сообщения

    // Статичные блоки
    let cabinet_bloc = '.cabinet__contacts'; //Общий блок всего чата, в дата-аттр. содержит статичные настройки
    let chat_type =  $('.cabinet__contacts').data('type'); //Тип чата
    let tabs_content = '.tabs__header'; // div в котором отображаются вкладки
    let tab_selector = '.tabs__header > div'; //селектор вкладки
    let chat_menu = '.chat__menu'; // Контекстное меню чата

    // Изменяемые элементы
    let dialog = '.contacts__chat'; // Верхний див над дивом сообщений
    let dialog_empty = '.contacts__chat.empty'; //Блок показаный когда чат не выбран
    let dialog_name = '.chat__header__name > a'; //Селектор в котором отображается имя чата
    let dialog_avatar = '.chat__header__logo'; //Аватар чата, в блоке справа
    let message_avatar = '.chat__content__mess-stack__logo'; //мелкая аватарка в сообщениях

    // Селекторы блоков, которые отрисовываются на js
    let contact_selector = '.contact'; //селектор контакта
    let chat_selector = '.tabs__content__chats__chat';  // селектор только чата
    let active_chat = '.tabs__content__chats__chat.active'; //Селектор активного чата
    let message = '.chat__content__mess-stack'; // Селектор сообщения

    // Содержимое чата, которое отображается во вкладке справа
    let avatar_chat ='.tabs__content__chats__chat__logo'; //Аватар чата
    let selected_chat_name = '.tabs__content__chats__chat__name'; //Имя выбранного чата
    let chat_history = '.tabs__content__chats__chat__history'; //Последнее сообщение в чате, показаное слева
    let chat_date = '.tabs__content__chats__chat__date';
    let chat_count = '.tabs__content__chats__chat__messages-count';

    //Содержимое контакта, отображаемое слева
    let contact = '.tabs__content__contacts__contact';
    let contact_name = '.tabs__content__contacts__contact__name';
    let contact_logo = '.tabs__content__contacts__contact__logo';

    // Кнопки и кликабельные иконки
    let open_menu = '.message__arrow'; // Селектор при нажатии открывает меню сообщения
    let translate_button = '.chat__menu__menu-item[data-type="translate"]'; //По нажатии переводит сообщение
    let delete_message_button = '.chat__menu__menu-item[data-type="delete_message"]'; //По нажатии переводит сообщение
    let send_message_button = '.chat__input__button'; //Кнопка отправки сообщения
    let add_contact = '.tabs__content__contacts__contact__add-contact'; // добавление/удаление контакта
    let remove_contact = '.tabs__content__contacts__contact__remove-contact'; // удаление контакта

    // Подключение к ребиту
    let domain_url = $(cabinet_bloc).data('domain');
    let userId = $(cabinet_bloc).data('user');
    let chatSupportToken = $(cabinet_bloc).data('support-token');
    let chatToken = $(cabinet_bloc).data('token');
    let profileId = $(cabinet_bloc).data('id');
    let queue_prefix = $(cabinet_bloc).data('queue-prefix');
    let rabbit_user = '';
    let rabbit_password = '';
    let localisation = $(cabinet_bloc).data('localisation');

    function bind () 
    {
        if(!!!localStorage.openpages)
        {
            localStorage.openpages = 1;

        } else
        {
            ++localStorage.openpages;
        }

        connectToChats();

        $(dialog).on('click', open_menu, function(e) {

            $(this).addClass('active');

            $(translate_button).data('message', $(this).parent().data('id'));
            
            $(delete_message_button).data('route-delete', $(this).parent().data('route-delete'));

            onContextMenu(e);

        });

        window.onunload = function() {

            if(!!localStorage.openpages)
            {
                --localStorage.openpages;
            }

            if( Number(localStorage.openpages) === 0 )
            {
                disconnectFromChats();
            }

            window.onunload = null;
        }

        $(cabinet_bloc).on('click', add_contact+'[data-added=false]', e => addToContact(e));

        $(cabinet_bloc).on('click', remove_contact+'[data-added=true]', e => removeContact(e));

        $(cabinet_bloc).on('click', '.tabs__content__chats__chat__remove-chat', function(e){
 
            console.log($(this));

            $('#destroy-modal')
                .modal('show')
                .find('.modal-body')
                .html($('#destroy-modal-text').data('text-delete-chat'));

            $('#destroy-modal')
                .find('#destroy-modal-form')
                .attr({action: $(this).data('route'), method:'POST'}); 

            $('#destroy-modal-form').on('submit', function(e){
                
                e.preventDefault();

                axios
                    .delete($('#destroy-modal-form').attr('action'), {}, {

                    })
                    .then(response => {
                        $('#destroy-modal').modal('hide');
                        $('.tabs__header__chats').click();
                    });  
            });         
        });

        $(translate_button).on('click', e => translateMessage(e));

        $(delete_message_button).on('click', deleteMessage);

        $(cabinet_bloc).keypress(function(event){
            let keycode = (event.keyCode ? event.keyCode : event.which);

            if(keycode == '13'){
                event.preventDefault();
                $(send_message_button).click();
            }
        
        });

        $(document).on('click', chat_selector, selectChat);

        $(send_message_button).on('click', e => sendMessage(e));

        //В зависимости от типа чата, разные бинды, для чата без профиля например нет поиска и контактов
        if (chat_type === 'profile' || chat_type === 'with_member')
        { 
            $(tab_selector).on('click', switchTab); //Выбор таба

            // Если переход был по кнопке назад вперёд или вкладка дублируется, то заново загружаем чаты и отправляем статус онлайн
            if (window.performance && window.performance.navigation.type === window.performance.navigation.TYPE_BACK_FORWARD)
            {                
                setTimeout( () => {
                    axios.get($(cabinet_bloc).data('route-connect'));
                    $(chat_place).children().remove();
                    $(contact_place).children().remove();
                    $(tab_selector+'.active').click();
                }, 500);

            } else
            {
                $(tab_selector+'.active').click();
            }

            $(document).on('click', contact, openChatWithContact);

            searchInit(); // Оборачиваем поиск в select2

        } else
        {
            // Если переход был по кнопке назад вперёд или вкладка дублируется, то заново загружаем чаты и отправляем статус онлайн
            if (window.performance && window.performance.navigation.type === window.performance.navigation.TYPE_BACK_FORWARD)
            {                
                setTimeout( () => {
                    axios.get($(cabinet_bloc).data('route-connect'));
                    $(chat_place).children().remove();
                    $(contact_place).children().remove();
                    chatsDownload();
                }, 500);

            } else
            {
                chatsDownload();
            }
            
        }

        $(messages_content).scroll(getMessagesByScroll);
    }

    function openChatWithContact()
    {
        $(contact).removeClass('active');
        $(this).addClass('active');

        $(dialog).show();
        $(dialog_empty).hide();

        $(messages_content).children().remove();
        $(chat_selector).removeClass('active');

        let dialogName = $(this).find(contact_name).text();
        let contactAvatar = $(this).find(contact_logo).css('background-image');

        $(dialog_avatar).css('background-image', contactAvatar);
        $(dialog_name).text(dialogName);

        if($(this).data('chat-id') !== null)
        {
            axios
                .get( $(this).data('route-messages') , {}, //По дефолту для только что выбранного чата открывается первая сраница сообщений
                {})
                .then(response => {

                    $(message).remove();

                    $('.chat__content').css('overflow-y', 'hidden');
                    rendered = showMessages(response.data,'');

                    if(rendered)
                    {
                        $('.chat__content').scrollTop( $('.chat__content')[0].scrollHeight );
                        $('.chat__content').css('overflow-y', 'auto');
                    }
            });
        }

        console.log($(this).data('route-send'));
        
        $(send_message_button).data('route', $(this).data('route-send'));
        $(send_message_button).data('profile', $(this).data('profile'));
    }

    function sendMessage(e)
    {
        let value = $('.chat__input__text').val();
        let currentTarget = e.target;

        if( value.trim().toString() === '' )
        {
            return;
        }

        if($(`${contact}.active`).length !== 0)
        {
            console.log($(`${contact}.active`));

            ChatHelper.sendMesssage(currentTarget, value).then(response => {
                
                $('.tabs__header__chats').click();

                setTimeout(() => {
                    $(`${chat_selector}[data-profile="${$(currentTarget).data('profile')}"]`).click();
                    $(active_chat).data('route-send', response.data.route_send);
                    $(active_chat).data('route-messages', response.data.route_messsages);
                    $(active_chat).data('route-translate', response.data.route_translate);

                }, 500);
    
                $('.chat__input__text').val('');
            });

        } else
        {
            ChatHelper.sendMesssage(currentTarget, value).then(response => {

                if( $('.tabs__header__search.active').length !== 0 )
                {
                    $('.tabs__header__chats').click();
                    
                    setTimeout(() => {
                        $(`${chat_selector}[data-chat="${ $(currentTarget).data('chat') }"]`).click();
                    }, 500);
                }

                $(active_chat).data('route-send', response.data.route_send);
                $(active_chat).data('route-messages', response.data.route_messsages);
                $(active_chat).data('route-translate', response.data.route_translate);
    
                $('.chat__input__text').val('');
            });
        }
    }

    /* Удалить непрочитанное сообщение */
    function deleteMessage()
    {
        hideMenu();

        axios
            .delete($(this).data('route-delete'), {}, {});
    }

    /* Загрузка контактов из апи */
    function contactsDownload()
    {
        axios
            .get($(tabs_content).data('route-contact'), {}, {})

            .then(response => {
                for (let i = 0; i < response.data.length; i++)
                {
                    getContacts(response.data[i]);
                }
            });
    }

    /* Перевод сообщения в чате и его отрисовка с новым текстом */
    function translateMessage(e)
    {
        hideMenu();

        let translatedText = {};
        let message_id = $(e.currentTarget).data('message');
        
        Translate.translateMessage(e)
            .then(response => {

                translatedText = response.data.text;
        
                $(message+'[data-id='+message_id+']').find('.message__text').text(translatedText);
        
                $('.chat__menu').removeClass('show-menu');
                $('.message__arrow').removeClass('active');

            });
    }

    /* Переключение между табами */
    function switchTab()
    {
        $(tab_selector).removeClass('active');

        $(chat_place).children().remove();
        $(contact_place).children().remove();

        $(chat_place).hide();
        $(search_place).hide();
        $(contact_place).hide();

        $(dialog).hide();
        $(dialog_empty).show();

        switch($(this).attr('class'))
        {
            case 'tabs__header__contacts':
                $(contact_place).show();
                contactsDownload(); // Загрузка контактов
                //$('#chat_search').data('type', 'contacts'); // Переключение поиска на поиск по контактам
                break;
            
            case 'tabs__header__search':
                $(search_place).show(); // Показать поиск
                $(chat_place).show();
                $(chat_place).addClass('tabs__header__search__content');
                //$('#chat_search').data('type', 'contacts'); // Переключение поиска на поиск по контактам
                break;

            default:
                $(chat_place).show();
                $(chat_place).removeClass('tabs__header__search__content');
                chatsDownload();
                //$('#chat_search').data('type', 'all');
                break;
        }

        $(this).addClass('active');
    }

    /* Выбор чата, профиля из поиска*/
    function selectChat()
    {
        $(this).find(chat_count).hide();
        $(dialog).show();
        $(dialog_empty).hide();

        $(messages_content).children().remove();
        $(chat_selector).removeClass('active');

        $(this).addClass('active');
        $(this).find(chat_count).text(0);

        $(active_chat).attr('data-page', 1);

        $(send_message_button).data('route', $(this).data('route-send'));
        $(send_message_button).data('chat', $(this).data('chat'));
        $(send_message_button).data('profile', $(this).data('profile'));

        let $dialogAvatar = $(this).find(avatar_chat).css('background-image');
        $(dialog_avatar).css('background-image', $dialogAvatar);
        
        let name = $(this).find(selected_chat_name).text(); //Название чата
        let id = $(this).data('profile'); //Id профиля, которому сообщение

        $(dialog_name).text(name);
        $(dialog).data('contact',id); //id собеседника, нужен чтобы добавить в контакты

        if($(this).data('route-messages') === null) // Если чата ещё не было
        {
          $(message_input_to).val($(this).data('profile'));
          
        } else
        {
            rendered = false;

            axios
                .get( $(this).data('route-messages') , {}, //По дефолту для только что выбранного чата открывается первая сраница сообщений
                {})
                .then(response => {

                    $(message).remove();

                    $('.chat__content').css('overflow-y', 'hidden');
                    rendered = showMessages(response.data,'');

                    if(rendered)
                    {
                        $('.chat__content').scrollTop( $('.chat__content')[0].scrollHeight );
                        $('.chat__content').css('overflow-y', 'auto');
                    }
                    
                });
        }
    }

    /* Получение сообщений по скроллу */
    function getMessagesByScroll()
    {
        let current_page = parseInt($(active_chat).data('page')); //Страница для скролла

        if ( $(messages_content).scrollTop() === 0 
            && $(active_chat).data('can-load')
            && rendered )
        {
            $(active_chat).data('page', ( current_page + 1 ) );
            
            let route = $(active_chat).data('route-messages');
            
            let params = new URLSearchParams();

            params.append('page', ( current_page + 1 ));

            axios.get( `${route}?${params.toString()}`, {},{

            })
            .then(response => {

                let loadedMessages = $(messages_content).children();

                if(response.data.length === 0) // Если старых сообщений больше нет и не будет, то они 
                {
                    $(active_chat).data('can-load', false); // Параметр, который определяет можно ли отправлять запрос на подгрузку старых сообщений
                    return null;
                }

                $(message).val($(this).data('chat'));
                $(message_input_to).val($(this).data('profile'));

                $(messages_content).children().remove();

                showMessages(response.data,'');

                $(messages_content).append(loadedMessages);

                $(messages_content).animate({scrollTop: $(messages_content).height() });
            })
        }
    }

    /* Подключение к очереди профиля */
    function connectProfile()
    {
        axios
            .post(domain_url+'/broadcasting/auth', 'channel_name=chat.'+profileId+'.'+chatToken, {

            })
            .then(response => {
                socketInit('profile');
            });
    }

    /* Подключение к очереди пользователя */
    function connectUser()
    {
        axios
            .post(domain_url+'/broadcasting/auth', 'channel_name=support.chat.'+chatSupportToken, {

            })
            .then(response => {
                socketInit('user');
            });
    }

    /* Создание сокета для конкретного типа профиль или юзер */
    function socketInit(type)
    {   
        let bc = new BroadcastChannel(type);
        let socketUrl = (new URL(domain_url)).hostname;
        let ws = new WebSocket('ws://'+socketUrl+':15674/ws');
        let client = Stomp.over(ws);
        let choosenToken = '';
        let ownerId = '';

        if(type === 'profile')
        {
            choosenToken = chatToken;
            ownerId = profileId;
            
        }else
        {
            choosenToken = chatSupportToken;
            ownerId = userId;
        }

        bc.onmessage = function (messageData) {
            let currentId = (bc.name === 'user') ? userId : profileId;
            socketListener(messageData.data, currentId);
        }

        client.debug = true;
        
        let on_connect = function() {

            client.subscribe(`/queue/${queue_prefix}-${choosenToken}`, function(data) {
                bc.postMessage(JSON.parse(data.body)); // Все неактивные вкладки
                socketListener(JSON.parse(data.body), ownerId); // Активная
            });
        };

        let on_error =  function() {
            disconnectFromChats(); // Ошибка также включается перед закрытием страницы, поэтому эта функция тут
        };
        
        client.connect(rabbit_user, rabbit_password, on_connect, on_error, '/');
    }
    
    function socketListener(messageData, ownerId)
    {
        if(messageData.type === 'message')
        {
            let $neededChat = $(`${chat_selector}[data-chat="${messageData.chat_id}"]`);
            
            $neededChat.find(chat_history).text(messageData.message);
            $neededChat.find(chat_date).text( transformDate(messageData.created_at) );

            //Если выбран именно этот чат, то дополнительно пишем сообщения в окно справа
            if($neededChat.length !== 0 )
            {
                if(messageData.from_id !== parseInt(ownerId))
                {
                    if($neededChat.hasClass('active'))
                    {
                        $neededChat.find(chat_count).show();                        
                        $neededChat.find(chat_count).text( parseInt($neededChat.find(chat_count).text()) + 1);
                    }

                    newDelivery(messageData, 'new');
                    
                } else
                {   
                    $neededChat.find(chat_date).text( transformDate( messageData.created_at, 'today') );
                    $neededChat.find(chat_count).text(0);
                    myMessage(messageData, 'new');
                }
            }
        }
        
        if (messageData.type === 'disconnect')
        {
            changeStatus(messageData.value, messageData.key);
        }

        if (messageData.type === 'connect')
        {
            changeStatus(messageData.value, messageData.key);
        }

        if (messageData.type === 'readed')
        {
            readMessages(messageData);
        }

        if (messageData.type === 'delete')
        {
            $(`${message}[data-id="${messageData.value}"]`).remove();
        }
    }

    /* 
     * Прочитать сообщения начиная с последнего непрочитанного
     */
    function readMessages(data)
    {
        let lastUnreaded = $(`${message}[data-readed="false"][data-my="true"]`).data('id');

        for (let messageIndex = lastUnreaded; messageIndex <= data.key; messageIndex++)
        {
            $(`${message}[data-id='${messageIndex}'][data-readed="false"][data-my="true"]`)
                .removeClass('unread');

            $(`${message}[data-id='${messageIndex}'][data-readed="false"][data-my="true"]`)
                .attr('data-readed',true);
        }
    }

    /* Загрузка чатов из апи */
    function chatsDownload()
    {
        $(chat_place).children().remove();
        
        axios
            .get($(tabs_content).data('route'), {}, {})

            .then(response => {
                
                for (let i = 0; i < response.data.length; i++)
                {
                    getChats(response.data[i]);
                }

                if(chat_type === 'with_member')
                {
                    $(`${chat_selector}[data-chat="${$('.cabinet__contacts').data('chat')}"]`).click();
                }
            });
    }

    // Смена статуса у профиля собеседника
    function changeStatus(from_id, status)
    {
        $(`${chat_selector}[data-profile='${from_id}']`)
            .find('.tabs__content__chats__chat__logo')
            .removeClass('offline online')
            .addClass(status);

        $(`${message}[data-my='false']`)
            .find(message_avatar)
            .removeClass('offline online')
            .addClass(status);
    }

    //Отрисовка сообщения, которое мне отправили
    function newDelivery(data, type)
    {
        $(`${message}[data-readed="false"][data-my="true"]`).removeClass('unread');

        let avatar_member = $(active_chat).find(avatar_chat).css('background-image');

        let messageReaded = false;

        if( typeof data.is_readed !== "undefined")
        {
            messageReaded = data.is_readed;
        }

        let $bloc = $(`<div data-id="${data.id}" data-readed="${messageReaded}" data-from="${data.from_id}" data-my="false" class="chat__content__mess-stack">
                    <div class="chat__content__mess-stack__logo online"
                        style='background-image:${avatar_member}'></div>
                    <div class="chat__content__mess-stack__content">
                        <div class="chat__content__mess-stack__name">${$(dialog_name).text()}</div>
                            <ul class="mess-stack__messages">
                                <li class="message__text">
                                ${data.message}
                                </li>
                            </ul>
                    </div>
                <div class="chat__content__mess-stack message__arrow"></div>
                <div class="message__time">${ transformDate(data.created_at, 'message') }</div>
            </div>`);

        $bloc.appendTo(messages_content);

        if(type == 'new')
        {
            $(active_chat).find(chat_history).text(message.message);
        }

        //$(".chat__body").animate({ scrollTop: $('.chat__body').srollHeight() }, "fast");
    };

    // Отрисовка сообщения, которое отправлено мной
    function myMessage(data, type)
    {
        $(`${message}[data-readed="false"][data-my="false"]`).removeClass('unread');

        let avatar = $('.sidebar__contact__container-name__logo').css('background-image');
        let name = $('.sidebar__contact__container-name__name').text();

        let messageReaded = '';

        if( typeof data.is_readed !== "undefined")
        {
            messageReaded = data.is_readed;

        } else
        {
            messageReaded = false;
        }

        let isReaded = messageReaded ? '' : 'unread';

        let $bloc = $(`<div data-id="${data.id}" data-readed="${messageReaded}" data-my="true" data-route-delete="${data.route_delete}" class="chat__content__mess-stack right ${isReaded}">
                <div class="chat__content__mess-stack__logo online"
                    style='background-image:${avatar}'></div>
                <div class="chat__content__mess-stack__content">
                    <div class="chat__content__mess-stack__name">${name}</div>
                    <ul class="mess-stack__messages">
                        <li class="message__text">
                        ${data.message}
                        </li>
                    </ul>
                </div>
                <div class="chat__content__mess-stack message__arrow"></div>
                <div class="message__time">${ transformDate(data.created_at, 'message') }</div>
            </div>`);

        $bloc.appendTo('.chat__content');

        if(type == 'new')
        {
            $(active_chat).find(chat_history).text(message.message);
        }
    };

    //Отрисовка всех сообщений после выбора чата
    function showMessages(data, type)
    {
        for (let i = 0; i < data.length; i++)
        {
            if(data[i].mine !== true)
            {
                newDelivery(data[i], type);

            } else
            {
                myMessage(data[i], type);
            }
        }

        if(data.length < 10)
        {
            $(active_chat).data('can-load') === false;
        }

        return true;
    }

    //Отрисовка контактов
    function getContacts(data)
    {
        $(`<div class="tabs__content__contacts__contact ${data.status}"
                data-route-send="${data.route_chat}"
                data-route-messages="${data.route_messages}"
                data-profile="${data.id}"
                data-chat="${data.chat_id}">
                <div class="tabs__content__contacts__contact__logo" style="background-image: url('${data.contact_image}')"></div>
                <div class="tabs__content__contacts__contact__name">${data.contact_name}</div>
                <div data-added="${data.is_contact}" data-route-contact="${data.route_contact}" class="tabs__content__contacts__contact__remove-contact"></div>
            </div>`).appendTo('.tabs__content__contacts');
      }

    //Отрисовка чатов
    function getChats(data)
    {
        let $chatBloc;
        
        if(data.type === 'profile')
        {
            $chatBloc = $(`<div class="tabs__content__chats__chat"
                data-route-send="${data.route_send}"
                data-route-messages="${data.route_messages}"
                data-profile="${data.profile_id}"
                data-chat="${data.id}"
                data-page="2"
                data-type="profile"
                data-can-load="true">
                <div class="tabs__content__chats__chat__logo ${data.status}" style="background-image: url('${data.chat_image}');"></div>
                    <div class="tabs__content__chats__chat__container">
                        <div class="tabs__content__chats__chat__name">${data.chat_name}</div>
                        <div class="tabs__content__chats__chat__date">${ transformDate(data.created_at) }</div>
                        <div class="tabs__content__chats__chat__history">${data.last_message}</div>
                        <div class="tabs__content__chats__chat__messages-count">${data.messages_count}</div>
                        <div data-chat="${data.id}" data-route="${data.route_delete}" class="tabs__content__chats__chat__remove-chat"></div>
                    </div>
            </div>`).appendTo(chat_place);
        }
        else
        {
            $chatBloc = $(`<div class="tabs__content__chats__chat"
                data-route-send="${data.route_send}"
                data-route-messages="${data.route_messages}"
                data-profile="${data.profile_id}"
                data-chat="${data.id}"
                data-page="2"
                data-type="support"
                data-can-load="true">
                <div class="tabs__content__chats__chat__logo ${data.status}" style="background-image: url('${data.chat_image}');"></div>
                    <div class="tabs__content__chats__chat__container">
                        <div class="tabs__content__chats__chat__name fix">${data.chat_name}</div>
                        <div class="tabs__content__chats__chat__date">${ transformDate(data.created_at) }</div>
                        <div class="tabs__content__chats__chat__history">${data.last_message}</div>
                        <div class="tabs__content__chats__chat__messages-count">${data.messages_count}</div>
                    </div>
            </div>`)
        }

        $chatBloc.appendTo(chat_place);

        if(parseInt($chatBloc.find(chat_count).text()) === 0)
        {
            $chatBloc.find(chat_count).hide();
        }
    }

    // Показать дату в правильном формате
    function transformDate(date, type = 'default')
    {
        date = new Date(date).toLocaleString();

        //let timezone = moment.tz.guess();

        let result = moment(date,'DD.MM.YYYY, hh:mm:ss', localisation ).format('DD MMM');

        if(result === moment().locale(localisation).format('DD MMM'))
        {
            result = moment(date,'DD.MM.YYYY, hh:mm:ss', localisation ).calendar().split(',')[0];
        }

        if(type === 'today')
        {
            result = moment(date,'DD.MM.YYYY, hh:mm:ss', localisation ).calendar().split(',')[0];
        }

        if(type === 'message')
        {
            result = moment(date,'DD.MM.YYYY, hh:mm:ss', localisation ).format('hh:mm');
        }

        return result;
    }

    // Выпадающий список профилей с поиска
    function formatData (data)
    {
        let status = '';
        let $bloc = {};
        let $iconBloc = {};

        if(data.loading)
        {
            $(chat_place).children().remove();

        }else
        {
            if(data.is_online === true)
            {
                status = 'online';
            }

            if(data.is_contact === false)
            {
                $iconBloc = $(`<div class="tabs__content__contacts__contact__add-contact"
                    data-added="${data.is_contact}"
                    data-route-contact="${data.route_contact}"></div>`);
            }

            if(data.chat_exist === true)
            {
                $bloc = $(`<div class="tabs__content__contacts__contact tabs__content__chats__chat ${status}"
                    data-route-send="${data.route_send}"
                    data-route-messages="${data.route_messages}"
                    data-profile="${data.profile_id}"
                    data-chat="${data.chat_id}"
                    data-page="1"
                    data-type="profile"
                    data-can-load="true">
                    <div class="tabs__content__contacts__contact__logo tabs__content__chats__chat__logo" style="background-image:url('${data.logo}');"></div>
                    <div class="tabs__content__contacts__contact__name tabs__content__chats__chat__name">${data.value}</div>
                </div>`);

                $bloc.appendTo(chat_place);
                $bloc.append($iconBloc);

            } else
            {
                $bloc = $(`<div class="tabs__content__contacts__contact ${status}"
                    data-profile="${data.profile_id}"
                    data-route-send="${data.route_send}">
                    <div class="tabs__content__contacts__contact__logo" style="background-image:url('${data.logo}');"></div>
                    <div class="tabs__content__contacts__contact__name">${data.value}</div>
                </div>`);

                $bloc.appendTo(chat_place);
                $bloc.append($iconBloc);
            }
        }
    }

    // Форматтер выбранного контакта
    function formatDataSelection (data)
    {
        return data.full_name || data.text;
    }

    //При выходе со страницы отправляется сообщение на реббит, чтобы собесседник увидел что профиль не в сети
    function disconnectFromChats()
    {
        localStorage.removeItem('openpages');

        if($(cabinet_bloc).data('route-disconnect') !== undefined)
        {
            navigator.sendBeacon($(cabinet_bloc).data('route-disconnect'), null); // POST
        }
    }

    //При выходе со страницы отправляется сообщение на реббит, чтобы собесседник увидел что профиль не в сети
    function connectToChats()
    {
        if($(cabinet_bloc).data('route-connect') !== undefined)
        {
            axios.get($(cabinet_bloc).data('route-connect'))
            .then(response => {
                rabbit_user = response.data.user; // На проде отдельного пользователя заведём, который будет слушать с вьюшки
                rabbit_password = response.data.password;

                connectUser();

                if (chat_type === 'profile' || chat_type === 'with_member')
                {
                    connectProfile(); // Подключаем очередь профиля
                }

            });
        }
    }

    //Обёртка для select2
    function searchInit()
    {
        $(chat_search).select2({
            width:"100%",
            theme: 'marketplace-chat-search',
            minimumInputLength: 1,
            ajax: {
                transport: function(params, success, failure) {
                    
                    let endpoint = $(chat_search).data('action');

                    let requestParams = {
                        search_string: params.data.q, 
                        search_type: $(chat_search).data('type')
                    };
                    axios
                        .post(endpoint, requestParams)
                        .then(success)
                        .catch(failure);
                },
                dataType: 'json',
                type: 'post',
                processResults: function (data, params) {
                    $(chat_place).children().remove();
                    params.page = params.page || 1;
                    return {
                        results: data.data,
                    };
                },
            },
            templateResult: formatData,
            templateSelection: formatDataSelection
        });
    }

    // Всё для контекстного меню
    function hideMenu() {
        $(chat_menu).removeClass('show-menu');
        $('.message__arrow').removeClass('active');
    }

    function showMenu(x, y, target) {

        let currentMessage = $(target).closest(open_menu).parent();

        if( currentMessage.data('readed') === false  &&  currentMessage.data('my') === true )
        {
            $(delete_message_button).show();

        } else
        {
            $(delete_message_button).hide();
        }

        let top = y + 10 + 'px';
        let left = x - 100 + 'px';

        $(chat_menu).css('left', left);
        $(chat_menu).css('top', top);
        $(chat_menu).addClass('show-menu');
    }

    function onContextMenu(e) {
        showMenu(e.pageX, e.pageY, e.currentTarget);            
        document.addEventListener('mousedown', onMouseDown, false);
    }

    function onMouseDown(e) {
        if( e.target.className !== 'chat__menu__menu-item')
        {
            hideMenu();
            document.removeEventListener('mousedown', onMouseDown);
        }
    }
    // Конец контекстного меню

    // Добавление в контакты
    function addToContact(e)
    {
        Contacts.addToContact(e).then(response => {
            if(!!response && response.status === 201)
            {
                $(e.currentTarget).remove();
            }

            if(!!response && response.status == 200)
            {
                $(e.currentTarget).remove();
            }
        });
    }

    // Удаление из контактов
    function removeContact(elementEvent)
    {
        $('#destroy-modal').modal('show');

        $('#destroy-modal-form').on('submit', function(formEvent) {

            formEvent.preventDefault();

            Contacts.removeContact(elementEvent).then(response => {
                if(response.status === 201)
                {
                    $(elementEvent.currentTarget).parent().remove();
                }
        
                if(response.status == 200)
                {
                    $(elementEvent.currentTarget).remove();
                }

                $('#destroy-modal').modal('hide');
            });
        });
    }

    return {
        init: bind,
    }
    
})();

