var WidgetSearch = (function () {

    function init(data, placeholder)
    {
        let term = (new URLSearchParams(location.search)).get('search');

        $('#select')
            .select2({
                allowClear: false,
                multiple: true,
                theme: 'marketplace-search',
                maximumSelectionLength: 1,
                minimumInputLength: 3,
                data: data,
                placeholder: {
                    id: '-1',
                    text: placeholder
                }
            })
            .on('select2:closing', function (e) {

                term = $(e.target.form)
                    .find('input[type="search"]')
                    .val()
                    .trim();
            })
            .on('select2:close', function (e, params) {

                if($(e.target).select2('data').length === 0)
                {
                    $(e.target.form)
                        .find('input[type="search"]')
                        .val(term);
                }
            })
            .on('select2:select', function (e) {

                $('#search')
                    .find('button[type="button"]')
                    .trigger('click');

            });
    }

    return {
       init: init,
    };

})();