export default (function () {

    function init()
    {
        $('.sidebar__contact__settings').on('click', showSettingsModal);

        $('.modal__content__close').on('click', closeSettingsModal);

        $('.modalmenu__item').on('click', changeTab);

        $('.chat__menu__menu-item[data-type="translate_settings"]').on('click', openTranslateSettings);

        $('#chat_settings_save').on('click',saveSettings);
    }

    /* Показать модалку */
    function showSettingsModal()
    {
        $('.content__settings__button-wrapper').hide();
        let tab_title = $('.modalmenu__item.active').find('.item__name').text();

        $('.settings__item').children().remove();
        $('.content__settings__title').text(tab_title);
        getSettings();
        $('#settings-modal').modal('show');
    }

    /* Открыть модалку настроек для переводов */
    function openTranslateSettings()
    {
        $('.modalmenu__item').removeClass('active');
        $('.modalmenu__item[data-type="localisation"]').addClass('active');
        showSettingsModal();
    }

    /* Смена вкладок слева в модалке чата */
    function changeTab()
    {
        $('.content__settings__button-wrapper').hide();
        let tab_title = $(this).find('.item__name').text();

        $('.content__settings__title').text(tab_title);

        $('.settings__item').children().remove();
            $('.modalmenu__item').removeClass('active');
            $(this).addClass('active');
            getSettings();
    }

    /* Получение инпутов и селектов для настроек, отображаемых справа */
    function getSettings()
    {
        let setting_type = $('.modalmenu__item.active').data('type');

        let route = $('#settings-modal').data('route-settings');

        let params = new URLSearchParams();

        params.append('setting_type', setting_type);

        axios
            .get(`${route}?${params.toString()}`, {}, {})
            .then(response => {

                for (let i = 0; i < response.data.length; i++)
                {
                    let data = response.data[i];

                    if ( data.type == 'select' )
                    {
                        formatSelect(data);
                    }
                }

                if(response.data.length !== 0)
                {
                    $('.content__settings__button-wrapper').show();
                }

            });
    }

    /* форматтер для селекта */
    function formatSelect(data)
    {
        let $bloc = $('<div class="item__settings"><div class="item__settings__title">'+data.name+'</div>'+
            '<div class="item__settings__content">'+
                '<select name="localisation" class="settings__content__select select2-hidden-accessible"></select>'+
            '</div></div>');
            
        $bloc.appendTo('.settings__item');

        for (let index = 0; index < data.values.length; index++)
        {
            let selected = data.values[index].selected ? 'selected' : '';

            $('.settings__content__select.select2-hidden-accessible').append(`<option ${selected} value="${data.values[index].code}">${data.values[index].value}</option>`);
        }

        $('.settings__content__select.select2-hidden-accessible').select2({
            placeholder: "{{ __('cabinet.profile.choose_language') }}",
            theme: "marketplace-forms",
            minWidth: "200px",
            language: "{{ session()->get('locale') }}" 
        });
    }

    /* Сохранить настройки для чата */
    function saveSettings()
    {
        let data = new FormData();

        let inputs = $('.item__settings').find('input, select');

        for (let index = 0; index < inputs.length; index++)
        {
            let key = $(inputs[index]).attr('name');
            let value = $(inputs[index]).val();

            data.append(`chat_settings[${key}]`, value);               
        }

        axios
            .post($('#settings-modal').data('route-save'), data, {})
            .then(response => {
                $('#settings-modal').modal('hide');
            });
    }

    /* Закрыть модалку */
    function closeSettingsModal()
    {
        $('#settings-modal').modal('hide');
    }

    return {
       init: init,
    };

})();