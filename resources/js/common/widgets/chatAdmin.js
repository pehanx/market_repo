export default (function(){

    let rendered = false;
    
    let message_input_to = '#test input[name="profile_id"]'; //Инпут с указанием владельца (сейчас пока profile_id)
    let message_input_chat = '#test input[name="chat_id"]'; //Инпут с айдишником чата, еси чата ещё не было, то инпут пустой
    let messages_content = '.messages'; // div в котором отрисовываются сообщения
    let message = '.messages > ul > li'; // Селектор сообщения
    let tabs_content = '#contacts'; // div в котором отображаются контакты, чаты либо поиск
    let contact = '#contacts > ul > li'; // селектор контакта, чата
    let chat_selector = '.chat';  // селектор только чата
    let tab_selector = '#bottom-bar > button'; //селектор вкладки
    let dialog = '.content > #dialog'; // Верхний див над дивом сообщений
    let dialog_name = '.content > #dialog > p'; //Селектор в котором отображается имя чата
    let dialog_image = '.content > #dialog > img'; // Селектор изображения чата
    let active_chat = '.chat.active'; //Селектор активного чата, выбранного

    //Подключение к ребиту
    let domain_url = $('#profile').data('domain');
    let userId = $('#profile').data('user');
    let chatSupportToken = $("#profile").data('support-token');
    let queue_prefix = $('#profile').data('queue-prefix');
    let rabbit_user = '';
    let rabbit_password = '';

    function bind () 
    {
       /*  window.onunload = function() {
            disconnectPost();
            window.onbeforeunload = null;
        } */
        
        connectToChats();

        $('.submit').click(sendMesssage);

        $('#frame').keypress(function(event){
            var keycode = (event.keyCode ? event.keyCode : event.which);
            if(keycode == '13'){
                event.preventDefault();
                sendMesssage();
            }
        
        });

        chatsDownload();

        // Поиск по контактам
        $('#chat_search').select2({
            width:"100%",
            minimumInputLength: 1,
            theme: 'marketplace-chat-search',
            ajax: {
                transport: function(params, success, failure) {
                    
                    let endpoint = $('#search_string').attr('action');
                    let requestParams = {
                        search_string: params.data.q, 
                        search_type: $('#chat_search').data('type')
                    };
                    axios
                        .post(endpoint, requestParams)
                        .then(success)
                        .catch(failure);
                },
                dataType: 'json',
                type: 'post',
                processResults: function (data, params) {
                    params.page = params.page || 1;
                    return {
                        results: data.data,
                    };
                },
            },
            templateResult: formatData,
            templateSelection: formatDataSelection
        });

        $(document).on('click', chat_selector, selectChat);
        $(messages_content).scroll(getMessagesByScroll);
        $(tab_selector).on('click', switchTab);
    }

    function connectToChats()
    {
        if($('#profile').data('route-connect') !== undefined)
        {
            axios.get($('#profile').data('route-connect'))
            .then(response => {
                rabbit_user = response.data.user; // На проде отдельного пользователя заведём, который будет слушать с вьюшки
                rabbit_password = response.data.password;
                connectUser();
            });
        }
    }

    /* Переключение между табами */
    function switchTab()
    {
        $(tab_selector).removeClass('active');
        $(this).addClass('active');
        $('#search-chat').addClass('d-none');
        
        switch($(this).data('type'))
        {            
            case 'search':
                $(contact).remove();
                $('#search-chat').removeClass('d-none'); // Показать поиск
                //$('#chat_search').data('type', 'contacts'); // Переключение поиска на поиск по контактам
                break;

            default:
                chatsDownload();
                //$('#chat_search').data('type', 'all');
                break;
        }
    }

    /* Перевод сообщения из чата */
    function translateMessage(element)
    {  
        let message_text = $(element).find('p').text();
        let language_code = $('.messages').data('language');

        axios
            .post($('.chat.active').data('route-translate'), {
                translate:true,
                language_code:language_code,
                message_text:message_text
            }, {})
            .then(response => {
                let translatedText = response.data.text;
                $(element).find('p').text(translatedText);
            })
    }

    /* Отправка сообщения */
	function sendMesssage()
    {
        let value = $('#test > input.message-text').val();

        if( value.trim().toString() === '' )
        {
            return;
        }

        axios
            .post($('.chat.active').data('route-send'), new FormData($('#test')[0]), {

            })
            .then(response => {
                $('.chat.active').data('route-send', response.data.route_send);
                $('.chat.active').data('route-messages', response.data.route_messsages);
                $('.chat.active').data('route-translate', response.data.route_translate);
                $('.chat.active').find('.preview').text(value);
            });

        $('#test > input.message-text').val('');
    }

    /* Выбор чата, профиля из поиска*/
    function selectChat()
    {
        $('.content').show();
        $(active_chat).data('page',1);
        $('.messages > ul > li').remove();
        $(chat_selector).removeClass('active');
        $(this).addClass('active')

        let name = $(this).find('.name').text(); //Название чата
        let id = $(this).data('profile'); //Id профиля, которому сообщение
        let image = $(this).find('img').attr('src'); //Изображение чата

        $(dialog_name).text(name);
        $(dialog).data('contact',id); //id собеседника, нужен чтобы добавить в контакты
        $(dialog_image).attr('src',image);

        if($(this).data('route-messages') === null) // Если чата ещё не было
        {
          $(message_input_to).val($(this).data('profile'));
        } else
        {
            rendered = false;

            axios
                .get( $(this).data('route-messages') , {}, {}) //По дефолту для только что выбранного чата открывается первая сраница сообщений
                .then(response => {

                    $(message_input_chat).val($(this).data('chat'));

                    $(message).remove();

                    $(messages_content).css('overflow-y', 'hidden');

                    rendered = showMessages(response.data,'');

                    if(rendered)
                    {
                        $(messages_content).scrollTop( $(messages_content)[0].scrollHeight );
                        $(messages_content).css('overflow-y', 'auto');
                    }
                })
        }
    }

    /* Получение сообщений по скроллу */
    function getMessagesByScroll()
    {
        let route = $(active_chat).data('route-messages');
        let params = new URLSearchParams();
        let current_page = parseInt($(active_chat).data('page'));

        if ($(messages_content).scrollTop() === 0 
            && $(active_chat).data('can-load')
            && rendered )
        {
            $(active_chat).data('page', current_page + 1 );

            params.append('page', current_page + 1);

            axios.get( `${route}?${params.toString()}`, {},{

            })
            .then(response => {

                if(response.data.length === 0) // Если старых сообщений больше нет и не будет, то они 
                {
                    $(active_chat).data('can-load', false); // Параметр, который определяет можно ли отправлять запрос на подгрузку старых сообщений
                    return null;
                }

                $(message).val($(this).data('chat'));
                $(message_input_to).val($(this).data('profile'));

                let loadedMessages = $(message);
                $(message).remove();

                showMessages(response.data,'');

                $(messages_content).find('ul').append(loadedMessages);
            })
        }
    }

    /* Подключение к очереди пользователя */
    function connectUser()
    {
        axios
            .post(domain_url+'/broadcasting/auth', 'channel_name=support.chat.'+chatSupportToken, {

            })
            .then(response => {
                let socketUrl = (new URL(domain_url)).hostname;

                let ws = new WebSocket('ws://'+socketUrl+':15674/ws');
                let client = Stomp.over(ws);

                //client.debug = null;

                let on_connect = function() {

                    client.subscribe(`/queue/${queue_prefix}-${chatSupportToken}`, function(data) {
                        let messageData = JSON.parse(data.body);

                        if(messageData.type === 'message')
                        {
                            $(`${chat_selector}[data-user='${messageData.from_id}']`).find('.preview').text(messageData.message);

                            if(messageData.from_id !== parseInt(userId))
                            {
                                newDelivery(messageData, 'new');
                                
                            } else
                            {
                                myMessage(messageData, 'new');
                            }
                        }

                        if (messageData.type === 'disconnect')
                        {
                            changeStatus(messageData.value, messageData.key);
                        }

                        if (messageData.type === 'connect')
                        {
                            changeStatus(messageData.value, messageData.key);
                        }

                        if (messageData.type === 'readed')
                        {
                            readMessages(messageData);
                        }
                        
                        if (messageData.type === 'delete')
                        {
                            $(`${message}[data-id="${messageData.value}"]`).remove();
                        }

                        /* if (messageData.type === 'disconnect')
                        {
                            changeStatus(messageData.from_id, messageData.status);
                        } */
                    });
                };

                let on_error =  function() {
                    //disconnectPost(); // Ошибка также включается перед закрытием страницы, поэтому эта функция тут
                };
                
                client.connect(rabbit_user, rabbit_password, on_connect, on_error, '/');
        });
    }

    /* 
     * Прочитать сообщения начиная с последнего непрочитанного
     */
    function readMessages(data)
    {
        let lastUnreaded = $(`${message}[data-readed="false"][data-my="true"]`).data('id');

        for (let messageIndex = lastUnreaded; messageIndex <= data.key; messageIndex++)
        {
            $(`${message}[data-id='${messageIndex}'][data-readed="false"][data-my="true"]`)
                .find('p')
                .prepend($('<i class="fa fa-eye" aria-hidden="true"></i>'));

            $(`${message}[data-id='${messageIndex}'][data-readed="false"][data-my="true"]`)
                .attr('data-readed',true);
        }
    }

    /* Загрузка чатов из апи */
    function chatsDownload()
    {
        $(contact).remove();
        
        axios
            .get($(tabs_content).data('route'), {}, {})

            .then(response => {
                
                for (let i = 0; i < response.data.length; i++)
                {
                    getChats(response.data[i]);
                }
            });
    }

    // Смена статуса у профиля собеседника
    function changeStatus(from_id, status)
    {
        $(`.chat[data-user='${from_id}']`)
            .find('.contact-status')
            .removeClass('offline online')
            .addClass(status);
    }

    //Отрисовка сообщения, которое мне отправили
    function newDelivery(message, type)
    {
        let messageReaded = false;

        if( typeof message.is_readed !== "undefined")
        {
            messageReaded = message.is_readed;
        }

        let $avatar = $(active_chat).find('img').attr('src');

        let $bloc = $(`<li data-id="${message.id}" "class="replies context-menu-message"
            data-from="${message.from_id}"
            data-readed="${messageReaded}"
            data-my="false">
            <img src="${$avatar}"/><p>${message.message}</p>
            </li>`);

        $bloc.appendTo('.messages > ul');

        if(type == 'new')
        {
          $('.chat.active.preview').find('.preview').text(message.message);
        }
    };

    // Отрисовка сообщения, которое отправлено мной
    function myMessage(message, type)
    {
        
        let messageReaded = false;

        if( typeof message.is_readed !== "undefined")
        {
            messageReaded = message.is_readed;
        }

        let $avatar = $('#profile-img').attr('src');

        let $bloc = $(`<li data-id="${message.id}" class="send context-menu-message"
            data-readed="${messageReaded}"
            data-my="true">
            <img src="${$avatar}" /><p>${message.message}</p>
            </li>`);

        $bloc.appendTo('.messages > ul');

        if(type == 'new')
        {
          $('.chat.active .preview').find('.preview').text(message.message);
        }
    };

    //Отрисовка всех сообщений после выбора чата
    function showMessages(data, type)
    {
        for (let i = 0; i < data.length; i++)
        {
            if(data[i].mine !== true)
            {
                newDelivery(data[i], type);

            } else
            {
                myMessage(data[i], type);
            }
        }

        if(data.length < 10)
        {
            $(active_chat).data('can-load') === false;
        }

        return true;
    }

    //Отрисовка чатов
    function getChats(data)
    {
        $(`<li class="chat"
            data-route-send="${data.route_send}"
            data-route-messages="${data.route_messages}"
            data-route-translate="${data.route_translate}"
            data-chat="${data.id}"
            data-user="${data.user_id}"
            data-page="1"
            data-type="support"
            data-can-load="true">
            <div class="wrap">
                <span class="contact-status ${data.status}"></span>
                <img src="${data.chat_image}" alt="" />
                <div class="meta">
                    <p class="name">${data.chat_name}</p>
                    <p class="preview">${data.last_message}</p>
                </div>
            </div>
        </li>`).appendTo('#contacts > ul');
    }

    // Выпадающий список профилей
    function formatData (data)
    {
        let type;
        let $statusBlock;
        let $bloc;

        if(data.loading)
        {
            $('.contact.search-result').remove();
            $('.chat.search-result').remove();

        }else
        {
            if(data.is_online === false)
            {
                $statusBlock = $('<span class="contact-status offline"></span>');
    
            } else
            {
                $statusBlock = $('<span class="contact-status online"></span>');
            }
    
            if(data.type === 'chat')
            {
                type = 'chat'
                
            } else  
            {
                type = 'contact'
            }
    
            let dataMessages = null;
            let dataTranslate = null;
            let icon = 'fa-user-plus';
    
            if(data.route_messages !== undefined)
            {
                dataMessages = data.route_messages;
            }

            if (data.route_translate !== undefined)
            {
                dataTranslate = data.route_translate;
            }

            if(data.is_contact === true)
            {
                icon = 'fa-trash';
            }
    
            //Отрисовка контакта в шаблоне
            $bloc = $("<li class='"+type+" search-result'"+
                'data-route-send="'+data.route_send+'" '+
                'data-route-translate="'+dataTranslate+'" '+
                'data-route-send="'+data.route_send+'" '+
                'data-profile="'+data.profile_id+'"'+
                'data-can-load="true"'+
                'data-route-messages="'+dataMessages+'">'+
                '<div class="wrap">'+
                    '<img src="'+data.logo+'" alt="" />'+
                    '<i data-route-contact="'+data.route_contact+'" '+
                        'data-added="'+data.is_contact+'"'+
                        'class="contact-profile fa '+icon+' fa-fw">'+
                    '</i>'+
                    '<div class="meta">'+
                        '<p class="name">'+data.value+'</p>'+
                        '<p class="preview"></p>'+
                    '</div>'+
                '</div>'+
                '</li>');
    
            $bloc.appendTo('#contacts > ul');
            $statusBlock.appendTo($bloc.find('.wrap'));
        }
    }

    // Форматтер выбранного контакта
    function formatDataSelection (data)
    {
        return data.full_name || data.text;
    }

    //При выходе со страницы отправляется сообщение на реббит, чтобы собесседник увидел что профиль не в сети
    /* function disconnectPost()
    {
        if($('#profile').data('route-disconnect') !== undefined)
        {
            axios.get($('#profile').data('route-disconnect'));
        }
    } */

    return {
        init: bind,
    }
})();
