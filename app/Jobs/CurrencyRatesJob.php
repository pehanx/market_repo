<?php

namespace App\Jobs;

use App\Services\Currency\OExchangeCurrency;
use App\Models\Profile;
use App\User;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class CurrencyRatesJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable;

    protected $service;

    /**
     * Execute the job.
     */
    public function handle(OExchangeCurrency $service)
    {
        $data = $service->getCurrencies();
        
        if($data)
        {
            $service->updateCurrencyRates($data);
            
        } else
        {
            throw new \Exception('Empty data');
        }
    }
}