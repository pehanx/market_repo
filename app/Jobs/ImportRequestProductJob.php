<?php

namespace App\Jobs;

use App\Helpers\RequestProduct\ImportRequestProductService;
use App\Models\Profile;
use App\User;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class ImportRequestProductJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable;

    protected $service;

    private $importParams;
    private $profileId;

    /**
     * Create a new job instance.
     */
    public function __construct(array $importParams, int $profileId)
    {
        $this->importParams = $importParams;
        $this->profileId = $profileId;
    }

    /**
     * Execute the job.
     */
    public function handle(ImportRequestProductService $service)
    {
        $service->start($this->importParams, $this->profileId);
    }
}