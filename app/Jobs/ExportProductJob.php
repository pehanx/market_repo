<?php

namespace App\Jobs;

use App\Helpers\Product\ExportProductService;
use App\Models\Profile;
use App\User;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class ExportProductJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable;

    private $exportParams;
    private $profileId;
    private $exportModelId;

    /**
     * Create a new job instance.
     */
    public function __construct(array $exportParams, int $profileId, int $exportModelId)
    {
        $this->exportParams = $exportParams;
        $this->profileId = $profileId;
        $this->exportModelId = $exportModelId;
    }

    /**
     * Execute the job.
     */
    public function handle(ExportProductService $service)
    {
        $elements = $service->getElementsForJob($this->exportParams['status'], $this->profileId);
        unset($this->exportParams['status']);
        
        $service->exportAll( 
            array_merge($this->exportParams, ['elements' =>  $elements]),
            (int)$this->profileId,
            (int)$this->exportModelId
        );
    }
}
