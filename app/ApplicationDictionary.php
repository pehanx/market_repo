<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Cache;

class ApplicationDictionary extends Model
{
    protected $guarded = ['id'];
    private static $data = null;
    private static $localisationId = null;
    
    /**
     * Данные в нужном формате для select2
     *
     * @param string $groupCode
     * @param int $localisationId
     
     * @return array $data
     */
    static function getDataForSelect($localisationId, $groupCode): array 
    {
        $data = [];
        
        static::where('localisation_id', $localisationId)
            ->where('group_code', $groupCode)
            ->get()
            ->mapWithKeys(function ($item) use(& $data){
                
                array_push($data, [
                    'id' => $item->code,
                    'text' => $item->value,
                    'key' => $item->id,
                ]);
                return[];
                
            });
            
        return $data;
    } 
    
    /**
     * Берем значения из словаря. Оптимизируем количество запросов в БД
     *
     * @param string $code
     * @param int $localisationId
     
     * @return string $data
     */
    static function getValue($code, int $localisationId): string
    {
        if(static::$data === null)
        {
            static::$data = [];
        }

        if(array_key_exists($code.$localisationId, static::$data ? static::$data : []))
        {
            $data = static::$data[$code.$localisationId];
        }
        else
        {
            $localisation = static::where('code', $code)->where('localisation_id', $localisationId)->first();

            $data = $localisation ? $localisation->value : $code;
            static::$data[$code.$localisationId] = $data;
        }
        
        return $data; 
    }  
    
  /*   static function getValue($code, int $localisationId): string
    {
        if(static::$data === null && static::$localisationId !== $localisationId)
        {
            static::$data = \Cache::tags('application', static::class)->rememberForever('application_dictionary_' . $localisationId, function() use($localisationId){
                return static::select('localisation_id', 'code', 'value')->where('localisation_id', $localisationId)->get();
            }); 
            
            static::$localisationId = $localisationId;
        }

        $value = static::$data
            ->where('code', $code)
            ->where('localisation_id', $localisationId)
            ->first()
            ->value;
        
        return $value;
    }  
    
    static function getValues($groupCode, int $localisationId)
    {
        if(static::$data === null && static::$localisationId !== $localisationId)
        {
            static::$data = \Cache::tags('application', static::class)->rememberForever('application_dictionary_' . $localisationId, function() use($localisationId){
                return static::select('localisation_id', 'code', 'value')->where('localisation_id', $localisationId)->get();
            }); 
            
            static::$localisationId = $localisationId;
        }

        $values = static::$data
            ->where('group_code', $groupCode)
            ->where('localisation_id', $localisationId)
            ->all();
        
        return $values;
    }   */
    
    static function getAlphabetsArray ($localisationId) 
    {
        return static::select('code')
                ->where('group_code', 'alphabets')
                ->where('localisation_id', $localisationId)
                ->get()
                ->keyBy('code') // Уникальные
                ->sortKeys()
                ->toArray();
    }
    
    /**
     * Локализация магазина поставщика может отличаться от локализации приложения.
     * Берем её из словаря на необходимой, переданной в route.
     *
     * @param int $localisationId
     */
    static function getShopInterface($localisationId) 
    {
        return Cache::tags(static::class)
            ->rememberForever(
                'shop_interface_' . $localisationId, 
                function() use($localisationId)
                {
                    return static::where('group_code', 'shops')
                        ->where('localisation_id', $localisationId)
                        ->get()
                        ->keyBy('code');
                }
            );
    }
}
