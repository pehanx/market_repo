<?php

namespace App\Http\Middleware;

use Closure;

class SetActiveProfileIfRemember
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if
        (   
            auth()->check() && auth()->viaRemember() && $request->hasCookie('active_profile') && !session()->has('active_profile')
                AND 
            auth()->user()->hasRole(['buyer', 'seller']) 
        )
        {
            $profile = auth()
                ->user()
                ->profiles()
                ->where('profile_id', $request->cookie('active_profile'))
                ->with([
                    'profileTypes', 
                    'profileProperties' => function ($query){
                        $query
                            ->with('localisations');                        
                    }
                ])
                ->first();
            
            if(!!$profile)
            {
                session()->put('active_profile', $profile);
            } 
        }
        
        return $next($request);
    }
}
