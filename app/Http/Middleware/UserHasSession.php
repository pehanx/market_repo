<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
use App\Models\Profile;
use Carbon\Carbon;
use Cache;

class UserHasSession
{    
    /**
     * Если у юзера нет сида и при этом он вошёл с remember_me, то происходит logout
     * Нужно чтобы после выполнения команды, которой хотим пользователя разлогинить всё работало так же как и без remember_me
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        if(Auth::check())
        {
            if( auth()->user()->session_id === null )
            {
                Auth::logout();
            }
        }

        return $next($request);
    }
}
