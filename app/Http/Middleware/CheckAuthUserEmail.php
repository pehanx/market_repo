<?php

namespace App\Http\Middleware;

use Closure;
use App\User;

class CheckAuthUserEmail
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $dbUser = User::where('email', $request->email)->first();
        
        // Если есть пользователь с таким email и он является залогиненым пользователем - пропускаем.
        if( ($dbUser && (int)$dbUser->id === (int)auth()->user()->id) || !$dbUser)
        {
            return $next($request);
            
        }else
        {
            return redirect()->back()->withInput()->withErrors(['email' => __('validation.unique', ['attribute' => 'email']) ]); 
        }
    }
}
