<?php

namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as Middleware;

class VerifyCsrfToken extends Middleware
{
    /**
     * Indicates whether the XSRF-TOKEN cookie should be set on the response.
     *
     * @var bool
     */
    protected $addHttpCookie = true;

    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array
     */
    protected $except = [
        //
    ];
    
    private $includeRouteNames = [
        'search.index'
    ];
    
    protected function isReading($request)
    {
        return $this->checkRouteNameAndMethod($request);
    }
    
    private function checkRouteNameAndMethod($request): bool
    {
        $result = null;
        
        if( in_array($request->method(), ['HEAD', 'GET', 'OPTIONS']) 
            && !in_array($request->route()->getName(), $this->includeRouteNames)
        )
        {
            $result = true;
            
        } else 
        {
            $result = false;  
        }
        
        return $result;
    }
}
