<?php

namespace App\Http\Middleware;

use Closure;
use App\Models\ProfileType;

class RedirectIfNotCreateOrSelectProfile
{
    public function handle($request, Closure $next)
    {
        if(auth()->check())
        {
            // Пользователь не имеет ролей (соответственно не создал ни одного профиля покупателя\продавца)
            if(!auth()->user()->hasAnyRole(['buyer', 'seller']))
            {
                $message = [
                    'title' => __('notification.attention'),
                    'body' => __('notification.create_profile_buyer_seller'),
                    'type' => 'info'
                ];  
            } 
            
            // Пользователь создал профиль покупателя и(или) продавца, но не активировал ни один из них
            if(auth()->user()->hasAnyRole(['buyer', 'seller'])) 
            {
                $hasActiveProfile = auth()
                    ->user()
                    ->profiles()
                    ->where('status', 'active')
                    ->whereIn('profile_type_id', function ($query) {
                        $query->select('id')
                            ->from((new ProfileType)->getTable())
                            ->whereIn('code', ['buyer', 'seller'])
                            ->get()
                            ->toArray();
                    })
                    ->exists();
                    
                if(!$hasActiveProfile) 
                {
                    $message = [
                        'title' => __('notification.attention'),
                        'body' => __('notification.activate_created_profile'),
                        'type' => 'info'
                    ];
                }
            }
            
            // Пользователь создал профиль покупателя и(или) продавца, но не выбрал его в ЛК
            if(auth()->user()->hasAnyRole(['buyer', 'seller']) && !session()->has('active_profile'))
            {
                $message = [
                    'title' => __('notification.attention'),
                    'body' => __('notification.select_active_profile'),
                    'type' => 'info'
                ];
            }
            
            if(!empty($message)) 
            {
                session()->flash('info_message', $message);
                return redirect()->back();
                
            } else 
            {
                return $next($request);
            }
        }
    }
}
