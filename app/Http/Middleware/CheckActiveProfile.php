<?php

namespace App\Http\Middleware;

use Closure;

class CheckActiveProfile
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $type)
    {
        if(\Session::has('active_profile') && (string)\Session::get('active_profile')->profileTypes->code === $type)
        {
            return $next($request);
            
        }else
        {
            abort(403);
        }
    }
}
