<?php

namespace App\Http\Middleware;

use Closure;
use App\Models\Currency as CurrencyModel;

class Currency
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $rawCurrency = \Session::get('currency');    

        if ( CurrencyModel::where('code', $rawCurrency)->first() !== null ) 
        {  
            $currency = $rawCurrency;  
            
        }else 
        {
            $currency = CurrencyModel::where('is_base', true)->first()->code;
            \Session::put('currency', $currency);            
        }

        return $next($request);
    }
}
