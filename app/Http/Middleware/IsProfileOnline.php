<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
use App\Models\Profile;
use Carbon\Carbon;
use Cache;
use Session;


class IsProfileOnline
{
    const SECONDS_BEFORE_OFFLINE = 30;
    
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ( Session::has('active_profile') )
        {
            $profile = Session::get('active_profile');

            Cache::put( 'online_profile_' . $profile->id, true, self::SECONDS_BEFORE_OFFLINE );
        }
        
        return $next($request);
    }
}
