<?php
namespace App\Http\ViewComposers;

use Illuminate\View\View;
use Illuminate\Support\Facades\Auth;

class HtmlLangComposer
{
    public function compose(View $view): void
    {
        $langCodeAliases = [
            'cn' => 'zh-CN'
        ];

        $localeCode = session()->get('locale');

        $htmlLang = !empty( $langCodeAliases[$localeCode] )
            ? $langCodeAliases[$localeCode]
            : $localeCode;

        $view->with([
            'htmlLang' => $htmlLang,
        ]);
    }
}
?>
