<?php
namespace App\Http\ViewComposers;

use App\User;
use App\Models\Category;
use Illuminate\View\View;
use Cache;
use Illuminate\Support\Facades\Auth;

class CatalogMenuComposer
{
    public function compose(View $view): void
    {
        $menu = Cache::tags(Category::class)->rememberForever('main_menu', function(){
            return Category::getMainMenu()->first()->children;
        });

        $view->with([
            'mainMenu' => $menu
        ]);
    }
}
?>