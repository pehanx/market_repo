<?php
namespace App\Http\ViewComposers;

use App\{User, ApplicationDictionary};
use App\Models\{Category, Localisation, Currency, CurrencyRate, Profile};
use Illuminate\View\View;
use Cache;
use Illuminate\Support\Facades\Auth;

class CurrencyRateComposer
{
    public function compose(View $view): void
    {
        if( auth()->user() !== null )
        {
            $currency = Currency::where( 'code', \Session::get('prefer_currency') )->first();

            if($currency === null)
            {
                $currency = Currency::where( 'code', \Session::get('currency') )->first();
            }

        } else
        {
            $currency = Currency::where( 'code', \Session::get('currency') )->first();
        }

        if($currency === null)
        {
            $currency = Currency::where('is_base',true)->first();
        }

        $currentCurrencyCode = $currency;

        $currency = Cache::tags(CurencyRate::class)->remember('currency_rate', 3600, function(){

            $currencySymbols = ApplicationDictionary::where('group_code','currency')->get();

            $currencyRates = CurrencyRate::orderBy('created_at','desc')->limit(CurrencyRate::distinct('code')->count())->get()
                ->each(function($item) use($currencySymbols) {

                    $item->symbol = $currencySymbols->where('code',$item->code)->first()->value;
                    $item->rate = round($item->rate);
                    return $item;

                });
                
            if( $currencyRates->first() === null )
            {
                $currencyRates->push(
                    new CurrencyRate([
                        'rate' => 1,
                        'symbol' => '$',
                        'code' => 'usd'
                    ])
                );
            }

            return $currencyRates;
        });

        $view->with([
            'currencyRates' => $currency,
            'currentCurrencyCode' => $currentCurrencyCode->code
        ]);
    }
}
?>