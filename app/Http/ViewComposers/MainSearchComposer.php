<?php
namespace App\Http\ViewComposers;

use App\Models\{Category, CategoryProperty};
use App\Models\Localisation;
use Illuminate\View\View;
use Cache;

class MainSearchComposer
{
    public function compose(View $view): void
    {
        $appLocId = Localisation::where('code', app('translator')->getLocale())->first()->id;
        
        $categories = Cache::tags(Category::class)->rememberForever('main_search_' . $appLocId, function() use ($appLocId) {
            
            $categories = [];
            
            array_push($categories, [
                'id' => -1,
                'text' => 'placeholder',
                'link' => ''
            ]);
            
            CategoryProperty::where([
                ['localisation_id', $appLocId],
                ['code', 'name']
            ])
            ->whereIn('category_id', Category::where('is_active', true)->pluck('id')->all())
            ->with('categories')
            ->get()
            ->mapWithKeys(function ($item) use($appLocId, & $categories) {
                array_push($categories, [
                    'id' => $item->categories->id,
                    'text' => $item->value,
                    'link' => $item->categories->link
                ]);
                return [];
            }); 
            
            return  $categories;
        });
        
        $view->with(['categories' => $categories]);
    }
}
?>
