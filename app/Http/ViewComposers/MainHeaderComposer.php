<?php
namespace App\Http\ViewComposers;

use App\User;
use App\Models\{Category, Localisation, Currency, Profile, CurrencyRate, ProfileType};
use Illuminate\View\View;
use Cache;
use Illuminate\Support\Facades\Auth;

class MainHeaderComposer
{
    public function compose(View $view): void
    {
        $appLocId = Localisation::where('code', app('translator')->getLocale())->first()->id;
        $user = Auth::user();
        $localisations = Localisation::whereIn('code', ['ru', 'en'])->get();

        if( CurrencyRate::first() )
        {
            $currencies = Currency::all();

        } else
        {
            $currencies = collect();
        }

        if($user)
        {
            $user = User::getUserWithAvatar($user->id);
            $profileTypes = ProfileType::all()->keyBy('id')->toArray();
            $profiles = Profile::getProfilesForUser($user->id);
                
            $profiles->each(function($item, $key) use($profileTypes){
                $item->type = $profileTypes[$item->profile_type_id]['code'];
            });

            $profiles = $profiles->groupBy('type');

            //$profiles = collect();
        }
        
        $view->with(array_filter([
            'appLocId' => $appLocId,
            'localisations' => $localisations,
            'currencies' => $currencies,
            'user' => $user ?? null,
            'profiles' => $profiles ?? null
        ]));
    }
}
?>