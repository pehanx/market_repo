<?php

namespace App\Http\Controllers;

use App\Services\Translate\TranslatorInterface;

class TranslateController extends Controller
{
    public function __construct(TranslatorInterface $service)
    {
        $this->service = $service;
    }
    
    public function checkTranslateApi(): bool 
    {
        try 
        {
            $result = $this->service->checkTranslateApi(); 
            
        } catch (\Throwable $e)
        {
            //$errorMessage = $e->getMessage();
            
            $result = false;
        }
        
        return $result;
    }

    public function translateString(string $string, string $toLocaleCode, string $fromLocaleCode = ''): string
    {
        return $this->service->translateString($string, $toLocaleCode, $fromLocaleCode);
    }

    public function translateArray($array, string $toLocaleCode, string $fromLocaleCode = ''): array
    {
        return $this->service->translateArray($array, $toLocaleCode, $fromLocaleCode);
    }
}