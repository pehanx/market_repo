<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\Pages\SearchResult\SearchParamsRequest;
use Illuminate\Support\Facades\DB;
use App\Http\Router\CategoryPath;
use Illuminate\Support\Facades\Auth;
use App\Models\{RequestProduct, Product, Category, Localisation, CategoryProperty};
use App\Helpers\Catalog\CatalogService;
use App\Helpers\Shop\ShopService;
use App\Http\Controllers\TranslateController;
use Illuminate\Support\Str;
use App\{ApplicationDictionary, User};

class MainController extends Controller
{
    private $catalogService;
    private $shopService;
    private $translator;    
    
    public function __construct(CatalogService $catalogService, ShopService $shopService, TranslateController $translator)
    {
        $this->catalogService = $catalogService;
        $this->shopService = $shopService;
        $this->translator = $translator;
    }
    
    /**
     * Главная страница.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
        $appLocId = Localisation::where('code', app('translator')->getLocale())->first()->id;

        $categoriesWithImages = Category::getCategoriesWithImages($appLocId);

        return view('main')->with(['categoriesWithImages' => $categoriesWithImages]);
    }

    public function loginByInvite(Request $request)
    {
        if($request->has('invite_token'))
        {
            $user = User::where('invite_token', $request->invite_token)->first();

            if($user !== null)
            {
                $user->invite_token = Str::random(20);
                $user->session_id = session()->getID();
                $user->update();

                Auth::loginUsingId((int)$user->id, true);
            }

            return redirect()->to(route('main'));
        }
    }
    
    /**
     * Страница с результатами поиска по сайту.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function indexSearchResult(SearchParamsRequest $request)
    {
        $user = Auth::user();
        $entity = $request->get('entity') ? $request->get('entity') : 'product';
        $appLocId = Localisation::where('code', app('translator')->getLocale())->first()->id;
        
        $searchParams = $request->except(['order_by', 'category']);    
        $orderBy = $request->only(['order_by']) ? $this->catalogService->explodeOrderByParams($request->only(['order_by'])) : null;
        $categoryId = $request->get('category');
        
        $search = $request->get('search');
       
        if($categoryId)
        {
            $category = Category::findOrFail($categoryId);
            $categoryName = $category->categoryProperties()
                ->where([
                    ['localisation_id', $appLocId],
                    ['code', 'name']
                ])
                ->first()
                ->value;
            
        } else // Будем искать по root по параметру search
        {
            $category = Category::whereNull('parent_id')->firstOrFail();
        }
        
        $categoryIds = Category::descendantsAndSelf($category->id)->pluck('id')->all();

        if($entity === 'request_product')
        {
            $results = RequestProduct::getAllRequestProductsFromCategories($categoryIds, (int)$appLocId, 3, $searchParams, $orderBy);
            $results->appends($request->input());
        
        } else 
        {
            $results = Product::getAllProductsFromCategories($categoryIds, (int)$appLocId, 3, $searchParams, $orderBy);
            $results->appends($request->input());
            $results->transform(function ($item) {
                $item->shop_link = $this->shopService->getProductIndexRoute($item->shops->users->first(), app('translator')->getLocale(), null, null, null);
                return $item;
            });
        }

        // Если у данной категории есть потомки, то выводим их в фильтре
        if(!empty($categoryIds) && is_countable($categoryIds) && count($categoryIds) > 1)
        {
            $categoryDescendants = Category::getCategoryDescendantsWithGetParams( $category->id, $appLocId, 'search', $request->except(['category']) );

        // Если нет, то выводим фильтры для категории (для каждой - свои - в будущем)    
        } elseif($entity !== 'request_product')
        {
            $countries = ApplicationDictionary::getDataForSelect($appLocId, 'countries');
            $colors = ApplicationDictionary::where('group_code', 'colors')->where('localisation_id', $appLocId)->get();
        }
        
        if($category->parent) 
        {
            if(!empty($categoryDescendants))
            {
                $backLink = 'search?' . http_build_query( array_merge($request->except(['category']), [
                    'category' => $category->parent->id 
                ]));
                
            } else
            {
                $backLink = 'search?' . http_build_query( array_merge($request->except(['category', 'min_price', 'max_price', 'color', 'country']), [
                    'category' => $category->parent->id 
                ]));
            }
            
            $previousCategoryName = $category
                ->parent
                ->categoryProperties()
                ->where('code', 'name')
                ->where('localisation_id', $appLocId)
                ->first()
                ->value;
        }

        return view(
            'search.index.' . $entity,
            compact(
                'search',
                'entity',
                'results',
                !empty($categoryName) ? 'categoryName' : null,
                !empty($previousCategoryName) ? 'previousCategoryName' : null,
                !empty($categoryDescendants) ? 'categoryDescendants' : null,
                !empty($countries) ? 'countries' : null,
                !empty($colors) ? 'colors' : null,
                !empty($user) ? 'user' : null,
                !empty($backLink) ? 'backLink' : null
            )
        ); 
    }
}