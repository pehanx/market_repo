<?php

namespace App\Http\Controllers\Api\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use App\Models\Download;
use Illuminate\Support\Str;

class UploadController extends Controller
{   
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function image(Request $request)
    {
        $user = Auth::user();
        $entity = $request->route('type');
        $file = $request->file('file');
        $folder = 'images/' . $user->id;

        if($file)
        {
            $fileId = current($this->addFiles($user,  [$file], "{$entity}_image", $folder));
            $fileUrl = asset('storage/' . Download::findOrFail($fileId)->path);
        }
        
        return response()->json(
            [
                'url' => $fileUrl,
                'id' => $fileId
            ], 
            
            Response::HTTP_CREATED,
        );
    }
    
    public function remove(Request $request)
    {
        $user = Auth::user();
        $id = $request->route('id');
        $entity = $request->route('type');
        
        $removingFile = $user->downloads()->findOrFail($id);

        if(strpos($removingFile->type, $entity) !== false)
        {
            $this->destroyFiles($user, [$id], $removingFile->type);
        }
        
        return response()->json([], Response::HTTP_NO_CONTENT);
    }   
}