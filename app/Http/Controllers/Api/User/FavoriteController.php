<?php

namespace App\Http\Controllers\Api\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use App\Helpers\Favorites\FavoriteService;

class FavoriteController extends Controller
{
    private $service;
    
    public function __construct(FavoriteService $service)
    {
        $this->middleware('auth');
        $this->service = $service;
    }
    
    public function add(Request $request)
    {
        if($request->route('id') && !in_array($request->route('type'), $this->service::ALLOWED_TYPES))
        {
            abort(403);
        }
        
        $user = Auth::user();
        $model = implode( '', array_map( 'ucfirst', explode('_', $request->route('type') ) ) );

        $this->service->add($user, (int)$request->route('id'), (string)$model);
        
        return response()->json([], Response::HTTP_CREATED);
    }
    
    public function remove(Request $request)
    {
        if($request->route('id') && !in_array($request->route('type'), $this->service::ALLOWED_TYPES))
        {
            abort(403);
        }
        
        $user = Auth::user();
        $model = implode( '', array_map( 'ucfirst', explode('_', $request->route('type')) ) );

        $this->service->delete($user, (int)$request->route('id'), (string)$model);
        
        return response()->json([], Response::HTTP_NO_CONTENT);
    }  
}
    /*  $favorite = Favorite::where('favoriteable_type', Product::class)->with('favoriteable')->first();
        dd($favorite); */