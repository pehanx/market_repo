<?php

namespace App\Http\Controllers\Api\User;

use App\Http\Controllers\TranslateController;
use Illuminate\Http\Response;
use Illuminate\Http\Request;
use App\Http\Requests\Cabinet\Chat\{ChatMessagesRequest, ChatContactRequest, ChatHelperRequest, ChatSupportRequest, ChatAddRequest};
use App\Http\Controllers\Controller;
use App\Models\{Chat, Message, Profile, Setting, Localisation, ProfileContact};
use App\Events\Chat\{ChatSupportSend, ChatSupportPush};
use App\Helpers\Chat\ChatSupportService;
use App\User;
use Cache;

class ChatController extends Controller
{
    const MESSAGES_PER_PAGE = 10;

    public function __construct(ChatSupportService $service, TranslateController $translator)
    {
        app('translator')->setLocale( session()->get('locale') );
        
        $this->service = $service;
        $this->tr = $translator;
        
        $this->middleware('auth');
        
        $this->middleware('redirect_if_not_create_or_select_profile', ['only' => ['addToContact', 'chatSettingsSave', 'translateSelectedMessage']]);
        $this->middleware('permission:send admin', ['only' => ['sendSupport']]);
        $this->middleware('permission:list message', ['only' => ['getMessages']]);
    }

    /*
     * Отправка сообщений в чат с админом
     */
    public function sendSupport(Chat $chat, ChatSupportRequest $request)
    {
        if(!auth()->user()->chats()->where('chat_id', $chat->id)->exists())
        {
            abort(403);
        }

        event(new ChatSupportSend(
            $chat->id, //id чата
            $request->message_text,
        ));

        event(new ChatSupportPush(
            $chat->id, //id чата
            $request->message_text,
        ));
    }

    /* 
     * Удалить непрочитанное сообщение
     */
    public function deleteMessage(Chat $chat, Message $message)
    {
        //Ищем владельца сообщения
        if($chat->users->first() !== null)

        {
            $owner = auth()->user();
            $type = 'user';

        } else
        {
            $owner = session()->get('active_profile');
            $type = 'profile';
        }

        //Перед удалением сообщения проверяем существует ли владелец сообщения, не прочитано сообщение, совпадает ли owner и есть ли в этом чате такое сообщение
        if( !!$owner
            && !!$message
            && !$message->is_readed
            && $chat->id === $message->chat_id
            && $message->owner_id === $owner->id )
        {
            $this->service->deleteMessage($message, $type);

            return response()->json( [], Response::HTTP_NO_CONTENT);
        }

        return response()->json( [], Response::HTTP_FORBIDDEN);
    }

    /*
     * json с сообщениями для чата с саппортом
     */
    public function getMessages( Chat $chat)
    {
        if(auth()->user()->chats()->where('chat_id', $chat->id)->exists())
        {
            $messages = $chat->messages();
                
            if(!auth()->user()->hasRole('admin'))
            {
                $messages = $messages->whereNull('type');
            }

            $notMyMessages = Message::whereIn('id', $messages->pluck('id')->all())
                ->where('owner_id','<>',  auth()->user()->id);
    
            $notMyMessages->update(['is_readed' => true]);
    
            $notMyMessage = $notMyMessages->get()->last();
            
            $messages = $messages->orderBy('created_at', 'desc')->paginate(self::MESSAGES_PER_PAGE);
    
            if($notMyMessage !== null)
            {
                $this->service->readMessages($notMyMessage->id, $chat );
            }

            //Вынести в модель
            $messagesArray = $messages->transform(function($item, $key) {

                if(auth()->user()->hasRole('admin'))
                {
                    $item->from = User::find($item->owner_id)->name;

                } else
                {
                    $item->from = __('cabinet.chat.support');
                }

                $item->chat_name = $item->chat_id;
                
                $item->route_delete = route('chat.messages.delete', ['chat' => $item->chat_id, 'message' => $item->id]);

                if($item->owner_id === auth()->user()->id)
                {
                    $item->mine = true;
                    
                } else
                {
                    $item->mine = false;
                }
                
                return $item;
            })
            ->reverse()
            ->toArray();

            return response()->json(array_values($messagesArray), Response::HTTP_OK);

        } else 
        {
            return response()->json([], Response::HTTP_FORBIDDEN);
        }
    }

    /*
     * json с чатами для чата с саппортом
     */
    public function getChats()
    {
        if( !auth()->user()->can('list message'))
        {
            abort(403);
        }

        $chats = $this->service->getChatTemplates();

        return response()->json($chats, Response::HTTP_OK);
    }

    /*
     * Поиск по пользователям, доступен только с админки, добавлю чуть позже проверку ещё на роль
     */
    public function searchUsers(ChatHelperRequest $request)
    {
        if( !auth()->user()->hasRole('admin') )
        {
            abort(403);
        }

        $data = $this->service->userSearch($request->search_string);

        return response()->json( $data, Response::HTTP_OK);
    }

    /*
     * Создание чата с пользователем со стороны админа
     */
    public function createChat(ChatSupportRequest $request)
    {
        if( !auth()->user()->hasRole('admin') )
        {
            abort(403);
        }

        $messageDecoded = urldecode($request->message_text);
        $chat = $this->service->createChat(User::find($request->profile_id));

        event(new ChatSupportSend(
            $chat->id, //id чата
            $messageDecoded,
        ));

        event(new ChatSupportPush(
            $chat->id, //id чата
            $messageDecoded,
        ));

        $newRoutes = [
            'route_send'=> route('chat.support', ['chat'=>$chat->id ]), // Роут, по которому отправляются сообщения
            'route_messages' => route('chat.messages', ['chat' => $chat->id]), // Роут, по которому загружаются сообщения
        ];

        return response()->json( $newRoutes, Response::HTTP_OK);
    }

    /* 
     * Перевод выбранного сообщения
     */
    public function translateSelectedMessage(ChatHelperRequest $request)
    {
        $profile = session()->get('active_profile');

        if($profile)
        {
            $message = Message::findOrFail($request->message_id);

            $messageFromMyChat = Chat::isMessageFromMyChat($message, $profile) ?? Chat::isMessageFromSupport($message, auth()->user());
            
            if($messageFromMyChat)
            {
                $settings = $profile->settings()->get();
    
                $localeSettings = $settings->where('code','localisation')->first();
                
                //На который переводим
                $toLocaleCode  = $localeSettings ? $localeSettings->value : session()->get('locale');
    
                //С которого переводим
                $fromLocaleId= Profile::find($message->owner_id)->profileProperties()->first()->localisation_id;
    
                $fromLocaleCode = Localisation::find($fromLocaleId)->code;
    
                $messageTranslated = $this->tr->translateString($message->message, $toLocaleCode, $fromLocaleCode);
    
                return response()->json( [ 'text' => $messageTranslated ], Response::HTTP_OK);
            }

        }
        
        return response()->json( [], Response::HTTP_FORBIDDEN);
    }

    /*
     * Добавление в контакты , либо удаление если контакт уже есть в списке
     */
    public function addToContact(Profile $contact)
    {        
        $profile = session()->get('active_profile');

        if($profile->id === $contact->id || $profile->contacts()->where('contact_id', $contact->id)->exists())
        {
            abort(403);
        }

        $this->service->addToContact($profile, $contact);
       
        return response()->json([], Response::HTTP_OK);
    }

    /* 
     * Получение данных для модалки настроек для чата
     */
    public function getSettings(ChatHelperRequest $request)
    {
        $profile = session()->get('active_profile');

        $settings = $this->service->getChatSettingItems( $profile, $request->setting_type);

        return response()->json( $settings, Response::HTTP_OK);
    }

    /* 
     * Сохранение настроек для чатов 
     */
    public function chatSettingsSave(ChatHelperRequest $request)
    {
        $profile = session()->get('active_profile');

        foreach( $request->chat_settings as $key => $option )
        {
            Setting::addSetting( $key, 'chat', $option, $profile);
        }

        return response()->json( [], Response::HTTP_CREATED);
    }

    /*
     * Метод вызывается при закрытии страницы с чатом и меняет статус у профиля на оффлайн
     */
    public function chatClosed()
    {
        $this->service->disconnectFromChat();
    }

    /*
     * Метод вызывается при открытии страницы с чатом и меняет статус у профиля на онлайн
     */
    public function chatOpened()
    {
        $rabbitSettings = [
            'user' => env('RABBITMQ_LISTENER_USER') ?? 'guest',
            'password' => env('RABBITMQ_LISTENER_PASSWORD') ?? 'guest',
        ];

        $this->service->sendOnlineStatus();

        return response()->json( $rabbitSettings, Response::HTTP_OK);
    }
}