<?php

namespace App\Http\Controllers\Api\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use App\Helpers\Favorites\FavoriteService;

class FavoriteController extends Controller
{
    private $service;
    
    public function __construct(FirebaseService $service)
    {
        $this->middleware('auth');
    }
    
    public function add(Request $request)
    {
        
    }
    
    public function remove(Request $request)
    {
        
    }  
}