<?php

namespace App\Http\Controllers\Api\Mobile;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Http\Response;
use App\Models\{Chat, Message, Profile};
use App\User;
use App\Events\Chat\{ChatSupportSend, ChatSupportPush, ChatPush, ChatDBSend};
use App\Helpers\Chat\{ChatService, ChatSupportService};
use App\Http\Requests\Cabinet\Chat\ChatMessagesRequest;
use Carbon\Carbon;

class ChatController extends Controller
{

    const MESSAGES_PER_PAGE = 10;

    public function __construct(ChatService $service, ChatSupportService $supportService)
    {
        $this->service = $service;
        $this->supportService = $supportService;
    }
    
    public function getAll(Request $request)
    {
        if( !auth('api')->user()->can('list message') )
        {
            return response()->json([], Response::HTTP_FORBIDDEN);
        }

        $contacts = [];

        $profile = null;

        if( isset($request->profile) && is_numeric($request->profile) )
        {
            $profile = User::find(auth('api')->user()->id)
                ->profiles()
                ->findOrFail($request->profile);

            $contacts = $this->service->getContactTemplates($profile);

            $chats = $this->service->getChatTemplates($profile);

        } else
        {
            $chats = $this->supportService->getChatTemplates();
        }

        return response()->json(['chats' => $chats, 'contacts' => $contacts], Response::HTTP_OK);
    }

    public function getSupportMessages(Chat $chat, Request $request)
    {
        if(auth('api')->user()->chats()->where('chat_id', $chat->id)->exists())
        {
            $messages = $chat->messages();
                
            if(!auth('api')->user()->hasRole('admin'))
            {
                $messages = $messages->whereNull('type');
            }

            $notMyMessages = Message::whereIn('id', $messages->pluck('id')->all())
                ->where('owner_id','<>',  auth()->user()->id);
    
            $notMyMessages->update(['is_readed' => true]);
    
            $notMyMessage = $notMyMessages->get()->last();
            
            $messages = $messages->orderBy('created_at', 'desc')->paginate(self::MESSAGES_PER_PAGE);
    
            if($notMyMessage !== null)
            {
                $this->supportService->readMessages($notMyMessage->id, $chat );
            }

            //Вынести в модель
            $messagesArray = $messages->transform(function($item, $key) {

                if(auth('api')->user()->hasRole('admin'))
                {
                    $item->from = User::find($item->owner_id)->name;

                } else
                {
                    $item->from = __('cabinet.chat.support');
                }

                if($item->owner_id === auth('api')->user()->id)
                {
                    $item->mine = true;
                    
                } else
                {
                    $item->mine = false;
                }
                
                return $item;
            })
            ->reverse()
            ->toArray();

            return response()->json(array_values($messagesArray), Response::HTTP_OK);

        } else 
        {
            return response()->json([], Response::HTTP_FORBIDDEN);
        }
    }

    public function getMessages(Chat $chat, Request $request)
    {
        $profile = auth('api')->user()->profiles()->findOrFail($request->profile);

        $messages = $chat->messages()
            ->orderBy('created_at', 'desc')
            ->paginate(self::MESSAGES_PER_PAGE);
        
        $notMyMessages = Message::whereIn('id', $messages->pluck('id')->all())
            ->where('owner_id','<>', $profile->id);

        $notMyMessages->update(['is_readed' => true]);

        $notMyMessage = $notMyMessages->get()->last();

        if($notMyMessage !== null)
        {            
            $this->service->readMessages($notMyMessage->id, $chat, $profile);
        }

        $messagesArray = $messages->transform(function($item, $key) use($profile) {

            $item->created_at = Carbon::createFromFormat('Y-m-d H:i:s', Carbon::now()->toDateTimeString())->toDateString();

            $item->chat_name = $item->chat_id;
            $item->from = Profile::find($item->owner_id)->profileProperties()->where('code','company_name')->first()->value;

            if($item->owner_id === $profile->id)
            {
                $item->mine = true;
            } else
            {
                $item->mine = false;
            }

            $item->route_delete = route('chat.messages.delete', ['chat' => $item->chat_id, 'message' => $item->id]);

            return $item;

        })
        ->reverse()
        ->toArray();

        return response()->json( array_values($messagesArray), Response::HTTP_OK);
    }

    public function sendSupport(Chat $chat, Request $request)
    {

        if(!auth('api')->user()->chats()->where('chat_id', $chat->id)->exists())
        {
            return response()->json([], Response::HTTP_FORBIDDEN);
        }

        event(new ChatSupportSend(
            $chat->id, //id чата
            $request->message_text,
        ));

        event(new ChatSupportPush(
            $chat->id, //id чата
            $request->message_text,
        ));

        return response()->json([], Response::HTTP_OK);
    }

    public function send(Chat $chat, Request $request)
    {
        $profile = Profile::findOrFail($request->profile);

        if ( $profile->chats()->where('chat_id',$chat->id)->first() === null
            || auth('api')->user()->profiles()->where('profile_id', $request->profile)->first() === null
            || !auth('api')->user()->can('send message') )
        { 
            return response()->json([], Response::HTTP_FORBIDDEN);
        }

        event(new ChatDBSend(
            $request->profile, //От профиля
            $chat->id, //id чата
            $request->message_text,
        ));

        event(new ChatPush(
            (int)$request->profile, //От профиля
            (int)$request->to_profile, //Кому
            $chat->id, //id чата
            $request->message_text,
        ));

        return response()->json([], Response::HTTP_OK);
    }
}