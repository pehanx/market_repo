<?php

namespace App\Http\Controllers\Api\Mobile;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Http\Response;
use App\Models\{Localisation, Profile};
use Illuminate\Database\Eloquent\Builder;
use App\User;
use App\Helpers\Account\AccountService;
use App\Http\Resources\Cabinet\AccountCollection;

class AccountController extends Controller
{
    private $hiddenFields = [
        'created_at',
        'updated_at',
        'deleted_at',
        'session_id',
        'localisation_id',
        'profile_id',
        'pivot',
        'remember_token',
        'registration_type'
    ];

    public function __construct(AccountService $service)
    {
        $this->service = $service;
    }

    public function saveDevice(Request $request)
    {
        if(!auth('api')->check() && !$request->has('device_id'))
        {
            abort(403);
        }

        $user = User::findOrFail(auth('api')->user()->id);

        if(!$user->devices()->where('device_id', $request->device_id)->exists())
        {
            $user->devices()->create([
                'device_id' => $request->device_id,
                'type' => $request->type
            ]);
        }
    }
    
    public function get(Request $request)
    {
        if(!auth('api')->check())
        {
            abort(403);
        }

        $account = User::with([
            'profiles' => function ($query) {
                $query
                    ->whereHas('profileProperties', function (Builder $query) {
                        $query
                            ->whereIn('localisation_id', Localisation::whereIn('code', ['ru', 'en'])->pluck('id')->toArray());   
                    })
                    ->whereHas('profileTypes', function (Builder $query) {
                        $query
                            ->where('code', 'account');
                    })
                    ->with('profileProperties');
             },
            'downloads' => function($query) {
                $query
                    ->select('path','type');
            },
            'roles',
         ])
         ->where('id', auth('api')->user()->id)
         ->get()
         ->makeHidden($this->hiddenFields)
         ->transform(function($item) {

            if($item->profiles->first() && $item->profiles->first()->profileProperties->first())
            {
                $item->account = $item->profiles->first()->profileProperties->keyBy('code')->makeHidden(array_merge(['id'], $this->hiddenFields));
            }

            if($item->downloads->first())
            {
                $item->avatar = $item->downloads->first()->makeHidden($this->hiddenFields);
            }

            $roles = $item->roles->pluck('name');
            unset($item->roles);

            $item->roles = $roles;

            unset($item->profiles, $item->downloads);            
            return $item;
         });

        return new AccountCollection($account);
    }

    public function edit(Request $request)
    {
        if(!auth('api')->check())
        {
            abort(403);
        }

        $user = User::findOrFail( auth('api')->user()->id );
            
        $deletedFiles = $request->filled('deleted_files') ? json_decode($request->deleted_files, true) : null;
    
        if($deletedFiles)
        {
            $userFiles = $this->filtrateInputFiles($request->allFiles(), $deletedFiles);
            
        }else
        {
            $userFiles = $request->allFiles();
        }
        
        if($request->has('delete_files'))
        {
            $countDeletedFiles = $this->softDeleteFiles($user, $request->delete_files);
        }

        // Обновляем информацию об аккаунте пользователя
        $account = $this->service->update($user, $request->except(['avatar', '_token', '_method', 'deleted_files', 'delete_files']));
        
        if($userFiles)
        {
            foreach ($userFiles as $fileType => & $file) 
            {
                if( in_array($fileType, $this->service::ALLOWED_FILES) )
                {
                    if(is_object($file)) // Прилетело с инпут без аттрибута multiple. Перезаписываем.
                    {
                        $this->destroyFiles($user, $user->downloads->where('type', $fileType)->pluck('id')->toArray() );
                        $file = $this->addFiles($user, [$file], $fileType);
                        
                    }elseif(is_array($file))
                    {
                        $file = $this->addFiles($user, $file, $fileType);
                    }
                }
            }
        }
    }
}