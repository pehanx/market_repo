<?php

namespace App\Http\Controllers\Api\Mobile;

use Illuminate\Http\Request;
use App\Http\Requests\Cabinet\Product\{CreateRequest, CreateModelRequest, FilterRequest, EditRequest, EditModelRequest, ActionRequest};
use App\Http\Controllers\Controller;
use App\Helpers\Product\ProductService;
use Illuminate\Http\Response;
use App\Models\{Product, Localisation, Profile, Category};
use Cache;
use App\Http\Resources\ProductCollection;

class ProductController extends Controller
{
    private $hiddenFields = [
        'created_at',
        'updated_at',
        'deleted_at',
        'depth',
        'parent_id',
        'sort',
        'link',
        '_lft',
        'profile_id',
        '_rgt',
        'is_active',
    ];

    public function __construct(ProductService $service)
    {
        $this->service = $service;
    }
    
    public function get(Request $request)
    {
        $localisation = Localisation::where('code', $request->localisation)->first();

        $localisationId = $localisation
            ? $localisation->id
            : Localisation::where('code', \Config::get('app.fallback_locale'))->first()->id;

        $appLocId = Localisation::where('code', app('translator')->getLocale())->first()->id;

        $product = Product::findOrFail($request->id);

        $product = \Cache::tags(Product::class)->rememberForever("product_" . $product->id . "_" . $localisationId, function() use($localisationId, $appLocId, $product) {

            $product = Product::getProductWithModels($product, $localisationId, $appLocId, true);
            $product = Product::getSimilarProducts($product, $localisationId, 3);
            
            $product->setRelation('downloads', $product->downloads->groupBy('type'));
            
            $product->load([
                'profiles' => function ($query) use($localisationId){
                    $query->with([
                        'profileProperties' => function ($query) use($localisationId){
                            $query->where([
                                ['localisation_id', $localisationId],
                                ['code', 'company_name']
                            ]);
                        }
                    ]);
                }
            ]);

            //Приблуда чтобы менеджеры не отображались как магазин пользователя
            $userShops = $product->shops->users;

            $userShop = $userShops->first();

            foreach ($userShops as $shop)
            {
                if(!$shop->hasRole('site_manager'))
                {
                    $userShop = $shop;
                }
            }

            $product->shop_id = $userShop->id;

            if($localisationId !== $appLocId)
            {
                $localisationCode = Localisation::find($localisationId)->code;

                $product->productProperties->transform(function ($item, $key) use($localisationCode){
                    $item->value = $this->translator->translateString($item->value, app('translator')->getLocale(), $localisationCode);
                    return $item;
                });
            }

            return $product;
            
        });

        if(auth('api')->user() !== null && $product->favorites->where('user_id', auth('api')->user()->id)->first())
        {
            $product->favorite = true;
        }

        return new ProductCollection($product->toArray());
    }

    public function create(Profile $profile, CreateRequest $request)
    {
        // Проверяем принадлежность пользователю профиля
        if ( !$profile->users()->find( auth('api')->user()->id ) || !$request->filled('category_id'))
        {
            return response()->json([], Response::HTTP_FORBIDDEN);
        }

        Category::findOrFail($request->category_id);
        
        $deletedFiles = $request->filled('deleted_files') ? json_decode($request->deleted_files, true) : null;
        
        if($deletedFiles)
        {
            $productFiles = $this->filtrateInputFiles($request->allFiles(), $deletedFiles);
            
        }else
        {
            $productFiles = $request->allFiles();
        }

        $localisationId = $profile->profileProperties()->first()->localisation_id;

        // Создаем товар для профиля(локализация профиля = локализации товара) пользователя
        $product = $this->service->create($profile, (int)$localisationId, $request->except(['deleted_files', '_token', '_method', 'preview_image', 'detail_images', 'describle_images']));

        // Чтобы грохнуть файлы определенного профиля быстро, если понадобиться
        $folderForImages = 'products/' . $profile->id;
        
        // Добавляем файлы
        if($productFiles)
        {
            foreach ($productFiles as $fileType => & $file) 
            {
                if( in_array($fileType, $this->service::ALLOWED_FILES) )
                {
                    if(is_object($file)) 
                    {
                        $file = $this->addFiles($product, [$file], $fileType, $folderForImages);
                        
                    }elseif(is_array($file))
                    {
                        $file = $this->addFiles($product, $file, $fileType, $folderForImages);
                    }
                }
            }
        }

        return response()->json([], Response::HTTP_OK);
    }

    public function list(Profile $profile, FilterRequest $request)
    {
        // Проверяем принадлежность пользователю профиля
        if ( !$profile->users()->find( auth('api')->user()->id ) )
        {
            return response()->json([], Response::HTTP_FORBIDDEN);
        }

        $products = collect();
        $localisationId = $profile->profileProperties()->first()->localisation_id;
        $appLocalisationId = Localisation::where('code', app('translator')->getLocale())->first()->id;

        if($request->type === "active" && $profile->products()->where('is_active', 'true')->first())
        {
            $products = Product::getFiltrateProducts($profile->products()->where('is_active', 'true'), $request->except(['full_filter']))->paginate(10);
            // Записали в пагинацию GET параметры фильтра
            $products->appends($request->input());
            //dump($profile->products()->where('is_active', 'true')->count());exit();
            Product::getAllProductsWithModels(collect($products->items()), (int)$localisationId, (int)$appLocalisationId);
        }

        if($request->type === "deleted")
        {
            $productHasTrashedModels = Product::getProductHasModels($profile->products(), 'trashed');

            if($profile->products()->onlyTrashed()->first() || $productHasTrashedModels->first())
            {            
                $products = Product::getFiltrateProducts($profile->products()->onlyTrashed()->whereNull('destroyed_at'), $request->except(['full_filter']) );

                $products = $products->union( Product::getFiltrateProducts($productHasTrashedModels, $request->except(['full_filter']) ) )->paginate(10);
                //dump($products->toSql(), $products->getBindings());exit();

                // Записали в пагинацию GET параметры фильтра
                $products->appends($request->input());

                Product::getAllProductsWithModels(collect($products->items()), (int)$localisationId, (int)$appLocalisationId, true);
            }
        }

        if($request->type === "inactive" && $profile->products()->where('is_active', 'false')->first())
        {
            $products = Product::getFiltrateProducts($profile->products()->where('is_active', 'false'), $request->except(['full_filter']))->paginate(10);

            // Записали в пагинацию GET параметры фильтра
            $products->appends($request->input());

            Product::getAllProductsWithModels(collect($products->items()), (int)$localisationId, (int)$appLocalisationId);
        }

        return response()->json($products, Response::HTTP_OK);
    }


    public function getCabinet(Product $product, Request $request)
    {
        $localisationId = $profile->profileProperties()->first()->localisation_id;
        $appLocalisationId = Localisation::where('code', app('translator')->getLocale())->first()->id;

        $product = Product::getProductWithModels($product, (int)$localisationId,  $appLocalisationId);
        $product->setRelation('downloads', $product->downloads->groupBy('type'));
        
        $category = Category::findOrFail($product->category_id)
            ->load(['categoryProperties' => function ($query) use($appLocalisationId){
                $query
                    ->where('code', 'name')
                    ->where('localisation_id', $appLocalisationId);
            }]);

        //dump($product);exit();

        return response()->json($product->toArray(), Response::HTTP_OK);
    }
}