<?php

namespace App\Http\Controllers\Api\Mobile;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Http\Response;
use App\Models\{Category, Localisation};
use App\Http\Resources\CategoryCollection;

class CategoryController extends Controller
{
    private $hiddenFields = [
        'created_at',
        'updated_at',
        'deleted_at',
        'depth',
        'parent_id',
        'sort',
        'link',
        '_lft',
        'profile_id',
        '_rgt',
        'is_active',
    ];

    public function __construct()
    {

    }
    
    public function get(Request $request)
    {
        $localisation = Localisation::where('code', $request->localisation)->first();

        $localisationId = $localisation
            ? $localisation->id
            : Localisation::where('code', \Config::get('app.fallback_locale'))->first()->id;

        if($request->type && $request->type === 'popular')
        {
            $categories = \Cache::tags(Category::class)->rememberForever('popular_categories_api_' . $localisationId, function() use($localisationId) {
                return Category::getCategoriesWithImagesMobile($localisationId, $this->hiddenFields);
            });

        } else
        {
            $categories = \Cache::tags(Category::class)->rememberForever('all_categories_api_' . $localisationId, function() use($localisationId) {
                return Category::where('is_active', true)
                    ->with('categoryProperties')
                    ->orderBy('sort')
                    ->withDepth()
                    ->get()
                    ->transform(function ($item, $key) use($localisationId) {

                        $item->title = $item->categoryProperties
                            ->where('localisation_id', $localisationId)
                            ->where('code', 'name')
                            ->first()->value;

                        $item->makeHidden($this->hiddenFields);

                        unset($item->categoryProperties);
                        
                        return $item;
                    })
                    ->toTree();
            });
        }

        return new CategoryCollection($categories);
    }

    public function getList(Request $request)
    {
        $localisation = Localisation::where('code', $request->localisation)->first();

        $localisationId = $localisation
            ? $localisation->id
            : Localisation::where('code', \Config::get('app.fallback_locale'))->first()->id;

        $categories = \Cache::tags(Category::class)->rememberForever('categories_api_' . $localisationId, function() use($localisationId) {
            return Category::with([
                'categoryProperties' => function ($query) use($localisationId){
                    $query->where([
                        ['localisation_id', $localisationId],
                        ['code', 'name']
                    ]);
                }
            ])->select('id')->get()->transform(function($item){
    
                $item->name = $item->categoryProperties->first()->value;
                $item->unsetRelation('categoryProperties');
    
                return $item;
            });
        });

        return new CategoryCollection($categories);
    }
}