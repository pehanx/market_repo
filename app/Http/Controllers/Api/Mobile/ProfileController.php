<?php

namespace App\Http\Controllers\Api\Mobile;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Http\Response;
use App\Http\Requests\Cabinet\Profile\CreateRequest;
use App\Http\Requests\Cabinet\Profile\EditRequest;
use App\Http\Requests\Cabinet\Profile\ApproveRequest;
use App\Models\{Profile, Localisation, ProfileType};
use App\Helpers\Profile\ProfileService;
use App\Http\Resources\Cabinet\ProfileCollection;
use App\User;

class ProfileController extends Controller
{    
    private $hiddenFields = [
        'created_at',
        'updated_at',
        'deleted_at',
        'category_id',
    ];

    public function __construct(ProfileService $service)
    {
        $this->service = $service;
    }
    
    public function get(Request $request)
    {
        $user = auth('api')->user();
        $type = $request->route('type');

        $hasPossibleLocales = !!Localisation::getPossibleLocales($user, $type);

        if( !in_array($type, $this->service::ALLOWED_TYPES) )
        {
            abort(403);
        }
        
        // Если профилей y пользователя нет, отобразим кнопку создать профиль на фронте.
        $profiles = Profile::getProfilesForUser($user->id, $type);

        return new ProfileCollection($profiles->makeHidden($this->hiddenFields)->toArray());
    }

    public function index(Request $request)
    {
        $localisation = Localisation::where('code', $request->localisation)->first();

        $localisationId = $localisation
            ? $localisation->id
            : Localisation::where('code', \Config::get('app.fallback_locale'))->first()->id;

        $user = auth()->user();

        if(auth()->user()->hasRole('site_manager'))
        {
            $user = User::findOrFail((int)$request->company);
        }

        $profile = Profile::getProfileForUser( (int)$request->route('id'), $user->id, $localisationId );

        $profile->setRelation('profileProperties', $profile->profileProperties->keyBy('code'));
        
        $localisation = Localisation::where('id', $profile->profileProperties()->first()->localisation_id)->pluck('code')->first();

        return new ProfileCollection($profile->makeHidden($this->hiddenFields)->toArray());
    }

    public function create(CreateRequest $request)
    {
        $deletedFiles = $request->filled('deleted_files') ? json_decode($request->deleted_files, true) : null;
        
        if($deletedFiles)
        {
            $profileFiles = $this->filtrateInputFiles($request->allFiles(), $deletedFiles);
            
        }else
        {
            $profileFiles = $request->allFiles();
        }

        //Костыль для Гришы
        if($request->has('number_phone'))
        {
            $request->merge([
               'number_phone' => str_replace(['+','-'], '', filter_var($request->number_phone, FILTER_SANITIZE_NUMBER_INT))
            ]);
        }

        if($request->has('number_phone_mobile'))
        {
            $request->merge([
                'number_phone_mobile' => str_replace(['+','-'], '', filter_var($request->number_phone_mobile, FILTER_SANITIZE_NUMBER_INT))
             ]);
        }

        $user = auth()->user();

        if(auth()->user()->hasRole('site_manager'))
        {
            $user = User::find( $request->company_user);
        }

        // Сохраняем профиль для данного пользователя
        $profile = $this->service->create($user, $request->except( array_merge(array_keys($profileFiles), ['_token', 'deleted_files']) ) );

        if(auth()->user()->hasRole('site_manager'))
        {
            $profile->users()->attach(auth()->user()->id);
        }
        
        if($profileFiles)
        {
            foreach ($profileFiles as $fileType => & $file) 
            {
                if( in_array($fileType, $this->service::ALLOWED_FILES) )
                {
                    if(is_object($file))
                    {
                        $file = $this->addFiles($profile, [$file], $fileType);
                        
                    }elseif(is_array($file))
                    {
                        $file = $this->addFiles($profile, $file, $fileType);
                        
                    }
                }
            }
        }
    }

    public function approve(ApproveRequest $request)
    {
        $user = auth('api')->user();

        $profile = User::find($user->id)
                ->profiles()
                ->findOrFail($request->profile);
         
        if ( !$profile->users()->find($user->id) ) 
        {
            abort(403);
        } 
        
        // Регистрируем документы пользователя и сохраняем себе на диск
        if($request->has('documents'))
        {
            $this->addFiles($profile,  $request->documents, 'profile_documents');
        } 
    
        $result = $this->service->approval($profile, $user, $profile->downloads()->where('type', 'profile_documents')->get());
        
        if($result)
        {
            $type = ProfileType::where('id', $profile->profile_type_id)->pluck('code')->first();
            
            return response()->json([], Response::HTTP_OK);
        }
    }
}