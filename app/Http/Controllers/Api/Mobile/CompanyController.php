<?php

namespace App\Http\Controllers\Api\Mobile;

use App\Models\{Profile, Localisation, Manager, ProfileType};
use App\{User, ApplicationDictionary};
use App\Helpers\Profile\ProfileService;
use App\Helpers\Account\AccountService;
use Illuminate\Http\Request;
use Illuminate\Contracts\Mail\Mailer;// Мб в будущем будем использовать другой метод отправки уведомлений
use App\Mails\InviteUserMail;
use App\Http\Requests\Cabinet\Manager\{CreateRequest, EditEntranceRequest, EditRequest};
use App\Http\Controllers\Controller;
use Illuminate\Support\Str;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Mail;

class CompanyController extends Controller
{
    private $profileService;
    private $accountService;
    private $mailer;

    public function __construct(ProfileService $profileService, AccountService $accountService, Mailer $mailer)
    {
        $this->profileService = $profileService;
        $this->accountService = $accountService;
        $this->mailer = $mailer;

        $this->middleware('role:site_manager');
    }

    public function store(Request $request)
    {
        $email = (string) $request->email;

        if(!User::where('email', $email)->exists())
        {
            $user = User::create([
                'name' => $request->name,
                'email' => $email,
                'password' => bcrypt(Str::random(8)),
                'invite_token' => Str::random(20),
                'registration_type' => 'manager',
            ]);

            $user->profiles()->create([
                'profile_type_id' => ProfileType::where('code', 'account')->first()->id,
                'status' => 'unactive'
            ]);


            $user->managers()->attach( auth()->user()->id );
        }

        return response()->json([], Response::HTTP_OK);
    }
    
    /* 
    * Все профили у созданной компании
    */
    public function indexAll(Request $request) 
    {
        $user = User::findOrFail((int)$request->user);

        $companies = Profile::getCompaniesForManager($user, 10, $request->type);

        return response()->json($companies, Response::HTTP_OK);
    }

    public function approval(Profile $profile)
    {
        $profile->status = "active";
        $profile->update();

        $user = $profile->users->where('id', '<>', auth()->user()->id)->first();

        if($user !== null)
        {
            return response()->json([], Response::HTTP_OK);
        }

        return response()->json([], Response::HTTP_FORBIDDEN);
    }

    public function companyAll() 
    {   
        $users = Manager::find(auth('api')->user()->id)->companies()->with([
            'profiles' => function($query){
                $query->with('downloads')->where('profile_type_id', 1)->orWhere('profile_type_id', 2);
            },
            'downloads'
        ])->get();

        return response()->json($users, Response::HTTP_OK);
    }

    public function invite(Request $request) 
    {   
        if($request->has('user') && User::find($request->user) !== null)
        {
            $user = User::find($request->user);

            if($user->invite_token === null)
            {
                $user->invite_token = Str::random(20);
                $user->save();
            }

            $result = $this->mailer->to($user->email)->send(new InviteUserMail($user));
        }
    }

    public function createProfile(Request $request)
    {
        $type = $request->route('type');

        $user = User::findOrFail($request->route('company'));

        $localisations = Localisation::getPossibleLocales($user, $type);
       
        if( !in_array($type, $this->profileService::ALLOWED_TYPES) || !$localisations)
        {
            abort(403);
        } 
        
        $localisationId = Localisation::where('code', app('translator')->getLocale())->first()->id;
        $account = Profile::getAccountForUser((int)$user->id)->first();
        
        if($account)
        {
            $account = $account
                ->profileProperties
                ->mapWithKeys(function ($item) {
                    return [$item['code'] => $item['value']];
                });
        }
  
        return response()->json([
            'user' => $user,
            'account' => $account,
            'localisations' => $localisations,
            'type' => $type
        ], Response::HTTP_OK);
    }

    public function edit(string $userId)
    {
        $user = User::getUserWithAvatar((int)$userId);
        $account = Profile::getAccountForUser((int)$userId)->first();
        $locale = app('translator')->getLocale();
        $locale = ($locale === 'en') ? 'en-GB' : $locale;
        
        if($account)
        {
            $account = $account
                ->profileProperties
                ->mapWithKeys(function ($item) {
                    return [$item['code'] => $item['value']];
                });
        }
        
        return view('cabinet.manager.company_edit', compact('account', 'user', 'locale'));
    }
    
    public function update(EditRequest $request, string $userId)
    { 
        $user = User::findOrFail((int)$userId);
            
        $deletedFiles = $request->filled('deleted_files') ? json_decode($request->deleted_files, true) : null;
    
        if($deletedFiles)
        {
            $userFiles = $this->filtrateInputFiles($request->allFiles(), $deletedFiles);
            
        }else
        {
            $userFiles = $request->allFiles();
        }
        
        if($request->has('delete_files'))
        {
            $countDeletedFiles = $this->softDeleteFiles($user, $request->delete_files);
        }

        // Обновляем информацию об аккаунте пользователя
        $account = $this->accountService->update($user, $request->except(['avatar', '_token', '_method', 'deleted_files', 'delete_files']));
        
        if($userFiles)
        {
            foreach ($userFiles as $fileType => & $file) 
            {
                if( in_array($fileType, $this->accountService::ALLOWED_FILES) )
                {
                    if(is_object($file)) // Прилетело с инпут без аттрибута multiple. Перезаписываем.
                    {
                        $this->destroyFiles($user, $user->downloads->where('type', $fileType)->pluck('id')->toArray() );
                        $file = $this->addFiles($user, [$file], $fileType);
                        
                    }elseif(is_array($file))
                    {
                        $file = $this->addFiles($user, $file, $fileType);
                    }
                }
            }
        }
        
        return redirect()->route('cabinet.company.edit', $user->id);
    }
}
