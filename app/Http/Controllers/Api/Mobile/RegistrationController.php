<?php

namespace App\Http\Controllers\Api\Mobile;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Hash;
use App\User;
use App\Models\OauthClient;

class RegistrationController extends Controller
{    
    public function __construct()
    {

    }
    
    public function createUser(Request $request)
    {
        $valid = validator($request->only('email', 'name', 'password', 'app'), [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6',
            'app' => 'required|in:ios,android'
        ]);

        if ($valid->fails()) {
            $jsonError=response()->json($valid->errors()->all(), Response::HTTP_BAD_REQUEST);
            return \Response::json($jsonError);
        }

        $data = request()->only('email','name','password','app');

        $user = User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
            'registration_type' => "mobile_app_".$data['app'],
            'session_id' => null,
            'ip' => \Request::ip(),
        ]);

        $user->assignRole('member', 'buyer', 'seller');

        return response()->json([], Response::HTTP_CREATED);
    }
}