<?php

namespace App\Http\Controllers\Api\Mobile;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Http\Response;
use App\Models\{Category, Localisation, Product, ProductProperties};
use App\Http\Resources\CatalogCollection;

class CatalogController extends Controller
{
    private $hiddenFields = [
        'created_at',
        'updated_at',
        'deleted_at',
        'depth',
        'parent_id',
        'sort',
        'link',
        '_lft',
        'profile_id',
        '_rgt',
        'is_active',
    ];

    public function __construct()
    {

    }
    
    public function get(Request $request)
    {
        $localisation = Localisation::where('code', $request->localisation)->first();

        $localisationId = $localisation
            ? $localisation->id
            : Localisation::where('code', \Config::get('app.fallback_locale'))->first()->id;

        $categories = \Cache::tags(Category::class)->rememberForever('descendants_and_self_' . $localisationId, function() use($request) {
            return Category::descendantsAndSelf($request->category)->pluck('id')->all();
        });

        $items = Product::getAllProductsFromCategoriesMobile($categories, $localisationId, auth('api')->user() ? auth('api')->user()->id : null);

        return new CatalogCollection($items);
    }
}