<?php

namespace App\Http\Controllers\Api\Mobile;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Http\Response;
use App\Models\{Favorite, Localisation, Product};
use App\User;
use App\Http\Resources\Cabinet\FavoriteCollection;
use App\Helpers\Favorites\FavoriteService;

class FavoriteController extends Controller
{
    private $hiddenFields = [
        'created_at',
        'updated_at',
        'deleted_at',
        'depth',
        'parent_id',
        'sort',
        'link',
        '_lft',
        'profile_id',
        '_rgt',
        'is_active',
    ];

    public function __construct(FavoriteService $service)
    {
        $this->service = $service;
    }
    
    public function get(Request $request)
    {
        $user = User::find(auth('api')->user()->id);
        $localisation = Localisation::where('code', $request->localisation)->first();

        $localisationId = $localisation
            ? $localisation->id
            : Localisation::where('code', \Config::get('app.fallback_locale'))->first()->id;

        $appLocId = Localisation::where('code', app('translator')->getLocale())->first()->id;

        $favoriteableModelName = implode( '', array_map( 'ucfirst', explode('_', $request->route('type') ) ) );
        $favoriteableName = lcfirst($favoriteableModelName);

        $filtrateFavoritableIds = Favorite::getFiltrateFavoritablesForUser($user->favorites(), $request->all(), $favoriteableModelName, $favoriteableName)
            ->pluck('favoriteable_id')
            ->all(); 

        $favorites = Favorite::getNeededFavorites($filtrateFavoritableIds, $favoriteableModelName, (int)$appLocId);

        return new FavoriteCollection($favorites);
    }

    public function add(Request $request)
    {
        if($request->id && !in_array($request->route('type'), $this->service::ALLOWED_TYPES))
        {
            abort(403);
        }
        
        $user = User::find(auth()->user()->id);

        $model = implode( '', array_map( 'ucfirst', explode('_', $request->route('type') ) ) );

        $this->service->add($user, (int)$request->id, (string)$model);
        
        return response()->json([], Response::HTTP_CREATED);
    }
    
    public function remove(Request $request)
    {
        if($request->id && !in_array($request->route('type'), $this->service::ALLOWED_TYPES))
        {
            abort(403);
        }
        
        $user = User::find(auth()->user()->id);

        $model = implode( '', array_map( 'ucfirst', explode('_', $request->route('type')) ) );

        $this->service->delete($user, (int)$request->id, (string)$model);
        
        return response()->json([], Response::HTTP_NO_CONTENT);
    }
}