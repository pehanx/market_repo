<?php

namespace App\Http\Controllers\Api\Profile;

use Illuminate\Http\Response;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use App\Http\Requests\Cabinet\Chat\ChatMessagesRequest;
use App\Http\Requests\Cabinet\Chat\ChatContactRequest;
use App\Http\Requests\Cabinet\Chat\ChatHelperRequest;
use App\Http\Requests\Cabinet\Chat\ChatSupportRequest;
use App\Http\Requests\Cabinet\Chat\ChatAddRequest;
use App\Http\Controllers\Controller;
use App\Models\{Chat,Message,Profile,Setting,Localisation};
use App\User;
use App\Events\Chat\ChatDBSend;
use App\Events\Chat\ChatPush;
use App\Helpers\Chat\ChatService;
use Carbon\Carbon;

class ChatController extends Controller
{
    const MESSAGES_PER_PAGE = 10; //Количество сообщений при ленивой загрузке

    public function __construct(ChatService $service)
    {
        $this->service = $service;
        $this->middleware('auth');
        app('translator')->setLocale( session()->get('locale') );
    }

    /*
     * Создание чата, если с собеседником ещё не было переписки 
     */
    public function createChat(Profile $profile, ChatMessagesRequest $request)
    {
        if ( !\Session::has('active_profile')
             || \Session::get('active_profile')->id !== $profile->id
             || auth()->user()->profiles()->where('profile_id',$request->profile_id)->first() !== null )
        { 
            abort(403);
        }

        $chat = $this->service->createChat($profile, Profile::find($request->profile_id));

        event(new ChatDBSend(
            (int)\Session::get('active_profile')->id, //От профиля
            $chat->id, //id чата
            $request->message_text,
        ));

        event(new ChatPush(
            (int)\Session::get('active_profile')->id, //От профиля
            (int)$request->profile_id, //Кому
            $chat->id, //id чата
            $request->message_text,
        ));

        $newRoutes = [
            'route_send' => route('profile.chat.send', ['profile' => $profile, 'chat'=>$chat->id ]),
            'route_messages' => route('profile.chat.messages', ['profile' => $profile, 'chat' => $chat->id]),
            'route_translate' => route('chat.translate'),
            'chat' => $chat->id
        ];

        return response()->json( $newRoutes, Response::HTTP_OK);
    }

    /*
     * Отправка пуша в джобик, который постоянно слушается, отправка сообщений в чат
     */
    public function sendPush(Profile $profile, Chat $chat, ChatMessagesRequest $request)
    {
        if ( !\Session::has('active_profile')
            || \Session::get('active_profile')->id !== $profile->id
            || $profile->chats()->where('chat_id',$chat->id)->first() === null
            || auth()->user()->profiles()->where('profile_id',$request->profile_id)->first() !== null
            || !auth()->user()->can('send message') )
        { 
            abort(403);
        }

        event(new ChatDBSend(
            (int)\Session::get('active_profile')->id, //От профиля
            $chat->id, //id чата
            $request->message_text,
        ));

        event(new ChatPush(
            (int)\Session::get('active_profile')->id, //От профиля
            (int)$request->profile_id, //Кому
            $chat->id, //id чата
            $request->message_text,
        ));
    }
    
    /*
     * Добавление в контакты , либо удаление если контакт уже есть в списке
     */
    public function removeContact(Profile $profile, Profile $contact)
    {
        if( !\Session::has('active_profile')
            || $profile->id === $contact->id
            || \Session::get('active_profile')->id !== $profile->id 
            || $profile->contacts()->where('contact_id',$contact->id)->first() === null )
        {
            abort(403);
        }

        $profile->contacts()->where('contact_id',$contact->id)->first()->delete();
        return response()->json( [], Response::HTTP_CREATED);
    }

    /*
     * Получить в контакты
     */
    public function getContacts(Profile $profile, Request $request)
    {
        if( !\Session::has('active_profile') 
            || \Session::get('active_profile')->id !== $profile->id )
        {
            abort(403);
        }

        $contacts = $this->service->getContactTemplates($profile);
        
        return response()->json( $contacts, Response::HTTP_OK);
    }
    
    /*
     * json с сообщениями для конкретного чата
     */
    public function getMessages(Profile $profile, Chat $chat)
    {
        if ( !\Session::has('active_profile')
             || \Session::get('active_profile')->id !== $profile->id
             || !auth()->user()->can('list message')
             || $profile->chats()->where('chat_id',$chat->id)->first() === null )
        { 
            abort(403);
        }

        $messages = $chat->messages()
            ->orderBy('created_at', 'desc')
            ->paginate(self::MESSAGES_PER_PAGE);
        
        $notMyMessages = Message::whereIn('id', $messages->pluck('id')->all())
            ->where('owner_id','<>', $profile->id);

        $notMyMessages->update(['is_readed' => true]);

        $notMyMessage = $notMyMessages->get()->last();

        if($notMyMessage !== null)
        {            
            $this->service->readMessages($notMyMessage->id, $chat, $profile);
        }

        $messagesArray = $messages->transform(function($item, $key) use($profile) {

            $item->created_at = Carbon::createFromFormat('Y-m-d H:i:s', Carbon::now()->toDateTimeString())->toDateString();

            $item->chat_name = $item->chat_id;
            $item->from = Profile::find($item->owner_id)->profileProperties()->where('code','company_name')->first()->value;

            if($item->owner_id === $profile->id)
            {
                $item->mine = true;
            } else
            {
                $item->mine = false;
            }

            $item->route_delete = route('chat.messages.delete', ['chat' => $item->chat_id, 'message' => $item->id]);

            return $item;

        })
        ->reverse()
        ->toArray();

        return response()->json( array_values($messagesArray), Response::HTTP_OK);
    }

    /*
     * Добавление профиля в чат
     */
    /* public function addToChat(Profile $profile, Chat $chat, ChatContactRequest $request)
    {
        if ( !\Session::has('active_profile')
             || \Session::get('active_profile')->id !== $profile->id
             || auth()->user()->profiles()->where('profile_id',$request->profile_id)->first() !== null )
        { 
            abort(403);
        }

        $profile = Profile::where([
            ['id',$request->profile_id],
            ['status', 'active']
        ])->firstOrFail();

        $this->service->addToChat(
            $chat,
            $profile,
            (bool) $request->with_managers ?? false
        );

        return response()->json( [], Response::HTTP_OK);
    } */

    /*
     * Удаление профиля из чата
     */
    /* public function removeFromChat(Profile $profile, Chat $chat, ChatMessagesRequest $request)
    {
        if ( !\Session::has('active_profile')
             || \Session::get('active_profile')->id !== $profile->id
             || auth()->user()->profiles()->where('profile_id',$request->profile_id)->first() !== null )
        { 
            abort(403);
        }

        $profile = Profile::where([
            ['id',$request->profile_id],
            ['status', 'active']
        ])->firstOrFail();

        $this->service->removeFromChat(
            $chat,
            $profile,
            (bool) $request->with_managers ?? false
        );

        return response()->json( [], Response::HTTP_OK);
    } */

    /*
     * Удаление чата
     */
    public function deleteChat(Profile $profile, Chat $chat)
    {
        if ( !\Session::has('active_profile')
            || \Session::get('active_profile')->id !== $profile->id
            || $chat->profiles()->where('chatable_id',$profile->id)->first() === null )
        { 
            abort(403);
        }

        $this->service->deleteChat(
            $chat
        );

        return response()->json( [], Response::HTTP_OK);
    }

    /*
     * Поиск профилей
     */
    public function profileSearch(Profile $profile, ChatHelperRequest $request)
    {
        if ( !\Session::has('active_profile')
             || \Session::get('active_profile')->id !== $profile->id )
        { 
            abort(403);
        }

        $data = $this->service->profileSearch($request->search_string, $request->search_type, $profile);
        
        return response()->json( $data, Response::HTTP_OK);
    }

    /* 
     * Получение массива с чатами профиля
     */
    public function getChats(Profile $profile)
    {
        if ( !\Session::has('active_profile')
             || \Session::get('active_profile')->id !== $profile->id ) 
        { 
            abort(403);
        }

        $chats = $this->service->getChatTemplates($profile);

        return response()->json( $chats, Response::HTTP_OK);
    }
    
    /* 
     * Написать контакту из поиска
     */
    public function writeToContact(Profile $profile, Profile $contact)
    {
        $hasChat = Chat::isChatWithMemberExists($profile, $contact->id);

        if (!$hasChat)
        {
            $hasChat = $this->service->createChat($profile, $contact);
        }

        return $hasChat->id;
    }
}