<?php

namespace App\Http\Controllers\Api\Profile;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use App\Models\{Profile, ProfileType, Export};

class ExportController extends Controller
{   
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    /**
     * Загрузка всех архивов от конкретного экспорта
     */ 
    public function downloadAll(Profile $profile, int $exportId)
    {
        if ( !$profile->users()->find( auth()->user()->id ) || (int)\Session::get('active_profile')->id !== (int)$profile->id ) 
        { 
            abort(403);
        }

        $exportFilePaths = Export::getFilesForDownload($profile, $exportId); //Пути до архивов

        $exportFileLinks = []; //Массив содержащий полную ссылку на архив

        foreach ($exportFilePaths as $exportFilePath)
        {
            array_push($exportFileLinks, asset('storage/' . $exportFilePath) );
        }
        
        return response()->json($exportFileLinks, Response::HTTP_OK);
    }
    
    /**
     * Обновление данных об экспортах во вьюшке
     */ 
    public function updateIndex(Profile $profile)
    {
        $profileType = ProfileType::where('id', $profile['profile_type_id'])->pluck('code')->first();

        $downloadTypes = ($profileType === 'buyer') ? 'request_product':
            (($profileType === 'seller') ? 'product' : '');
            
        $exports = Export::getDataForProfile($profile, [$downloadTypes])->toArray();

        return response()->json($exports, Response::HTTP_OK);
    }
}