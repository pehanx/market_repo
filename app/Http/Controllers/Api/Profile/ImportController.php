<?php

namespace App\Http\Controllers\Api\Profile;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use App\Helpers\Product\ImportProductService;
use App\Helpers\RequestProduct\ImportRequestProductService;
use App\Models\Profile;
use Illuminate\Http\File;
use App\Http\Requests\Cabinet\Import\ImportRequest;

class ImportController extends Controller
{
    public function __construct(ImportProductService $productService, ImportRequestProductService $requestProductService)
    {
        $this->productService = $productService;
        $this->requestProductService = $requestProductService;
    }

     /**
     * Открытие файлов импорта,
     * обработка файлов из архива
     * проверка перед сбамитом формы
     */
    public function importUpload(Profile $profile, ImportRequest $request)
    {
        // Проверяем принадлежность пользователю профиля
        if ( !$profile->users()->find( auth()->user()->id ) || (int)\Session::get('active_profile')->id !== (int)$profile->id)
        {
            abort(403);
        }

        if($request->route('type') === 'request_product')
        {
            $result = $this->requestProductService->saveFiles(
                $request->import_images,
                $request->file('import_table'),
                $profile
            );  
            
            if( !isset($result['errors']) && isset($result['import_id']) )
            {
                return response()->json( ['import_id' => $result['import_id'] ], Response::HTTP_OK);

            } else
            {
                return response()->json( $result['errors'], Response::HTTP_BAD_REQUEST);
            }
            
        } elseif($request->route('type') === 'product')
        {
            $result = $this->productService->saveFiles(
                $request->import_images,
                $request->file('import_table'),
                $profile
            );  
            
            if( !isset($result['errors']) && isset($result['import_id']) )
            {
                return response()->json( ['import_id' => $result['import_id'] ], Response::HTTP_OK);

            } else
            {
                return response()->json( $result['errors'], Response::HTTP_BAD_REQUEST);
            }

        } else
        {
            return response()->json( ['invalid_type' => __('cabinet.import.invalid_type') ], Response::HTTP_BAD_REQUEST);
        }

    }
}
