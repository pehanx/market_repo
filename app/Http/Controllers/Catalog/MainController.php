<?php

namespace App\Http\Controllers\Catalog;

use App\Http\Controllers\Controller;
use App\Models\{Category, Localisation};
use App\ApplicationDictionary;
use Cache;

class MainController extends Controller
{
    public function index()
    {
        if(app('translator')->getLocale() === 'cn')
        {
            return $this->indexChinese();
        }       
        
        $appLocId = Localisation::where('code', app('translator')->getLocale())->first()->id;

        // Мб понадобиться для фронта, выделим серым группы, в которых нет категорий с depthlevel = 1.
        $allGroups = array_map('mb_strtoupper', array_keys( ApplicationDictionary::getAlphabetsArray( (int)$appLocId) ) );

        $categories = Category::getCatalogIndex((int)$appLocId);

        $groupsWithCategories = array_map('mb_strtoupper', $categories->keys()->all() ); // Группы, в которых имеются категории

        return view('catalog.index',  compact('categories', 'groupsWithCategories', 'allGroups'));
    }

    private function indexChinese()
    {
        $appLocId = Localisation::where('code', app('translator')->getLocale())->first()->id;

        $categories = Category::getCatalogIndexChinese((int)$appLocId);

        return view('catalog.index_cn',  compact('categories'));
    }
}
