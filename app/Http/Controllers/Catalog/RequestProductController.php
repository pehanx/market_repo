<?php

namespace App\Http\Controllers\Catalog;

use App\Http\Controllers\Controller;
use App\Http\Router\CategoryPath;
use App\Models\{RequestProduct, Category, Localisation};
use App\Helpers\Catalog\CatalogService;
use App\ApplicationDictionary;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\Catalog\Product\SearchParamsRequest;
use Cache;
use Breadcrumbs;
use App\Http\Controllers\TranslateController;

class RequestProductController extends Controller
{
    public function __construct(CatalogService $catalogService, TranslateController $translator)
    {
        $this->catalogService = $catalogService;
        $this->translator = $translator;
    }
    
    public function index(SearchParamsRequest $request, CategoryPath $categoryPath)
    {
        $searchParams = $request->except(['order_by']);
        $orderBy = $request->only(['order_by']) ? $this->catalogService->explodeOrderByParams($request->only(['order_by'])) : null;
        
        $category = $categoryPath->category;
        
        // Покажем от root если не передан $categoryPath
        if(!$category)
        {
            $category = Category::whereNull('parent_id')->with('categoryProperties')->firstOrFail();
        }
        
        $user = Auth::user();
        $appLocId = Localisation::where('code', app('translator')->getLocale())->first()->id;
        $categoryIds = Category::descendantsAndSelf($category->id)->pluck('id')->all();
        
        $categoryName = ($category->link === 'catalog') ? 
        __('interface.catalog.purchase_requests')
        :$category
            ->categoryProperties
            ->where('code', 'name')
            ->where('localisation_id', $appLocId)
            ->first()
            ->value;
            
        $categoryMetaTitle = $category
            ->categoryProperties
            ->where('code', 'seo_title')
            ->where('localisation_id', $appLocId)
            ->first()
            ->value;
            
        $categoryMetaDescription = $category
            ->categoryProperties
            ->where('code', 'seo_description')
            ->where('localisation_id', $appLocId)
            ->first()
            ->value;
        
        $requestProducts = RequestProduct::getAllRequestProductsFromCategories($categoryIds, (int)$appLocId, 12, $searchParams, $orderBy);
        $requestProducts->appends($request->input());
        
        // Если у данной категории есть потомки, то выводим их в фильтре
        if(!empty($categoryIds) && is_countable($categoryIds) && count($categoryIds) > 1)
        {
            $categoryDescendants = Category::getCategoryDescendants($category->id, $appLocId, RequestProduct::class);
        }
        
        $back = Breadcrumbs::generate();
        $back = ($back[$back->count() - 2]) ?? null;
        //dd($requestProducts);
        return view(
            'catalog.request_product.index',
            compact(
                'requestProducts',
                'categoryName',
                'categoryMetaTitle',
                'categoryMetaDescription',
                !empty($categoryDescendants) ? 'categoryDescendants' : null,
                !empty($user) ? 'user' : null,
                !empty($back) ? 'back' : null
            )
        );
    }
    
    public function show(RequestProduct $requestProduct)
    {
        if(!$requestProduct->is_active || !!!$requestProduct->profiles || !!!$requestProduct->categories)
        {
            abort(404);
        }

        $requestProduct->increment('shows');
        $user = Auth::user();

        $localisationId = $requestProduct->requestProductProperties()->first()->localisation_id;
        $appLocId = Localisation::where('code', app('translator')->getLocale())->first()->id;
        
        $requestProduct = Cache::tags(RequestProduct::class)->rememberForever(
            "catalog_show_request_{$appLocId}_{$requestProduct->id}", 
            
            function() use ($requestProduct, $localisationId, $appLocId)
            {
                $requestProduct = RequestProduct::getRequestProduct($requestProduct, $localisationId, $appLocId);
                $requestProduct->setRelation('downloads', $requestProduct->downloads->groupBy('type'));
                
                $requestProduct->load([
                    'profiles' => function ($query) use($localisationId){
                        $query->with([
                            'profileProperties' => function ($query) use($localisationId){
                                $query->where([
                                    ['localisation_id', $localisationId],
                                    ['code', 'name']
                                ]);
                            }
                        ]);
                    }
                ]);
                
                if($localisationId !== $appLocId)
                {
                    $localisationCode = Localisation::find($localisationId)->code;
                    
                    $requestProduct->requestProductProperties->transform(function ($item, $key) use($localisationCode){
                        $item->value = $this->translator->translateString($item->value, app('translator')->getLocale(), $localisationCode);
                        return $item;
                    });
                }
                
                return $requestProduct;
            }
        );

        $requestProductMetaTitle = $requestProduct
            ->requestProductProperties
            ->where('code', 'seo_title')
            ->where('localisation_id', $localisationId)
            ->first();
            
        $requestProductMetaTitle = ($requestProductMetaTitle) ? 
            $requestProductMetaTitle->value 
            : $requestProduct
                ->requestProductProperties
                ->where('code', 'name')
                ->where('localisation_id', $localisationId)
                ->first()
                ->value;
            
        $requestProductMetaDescription = $requestProduct
            ->requestProductProperties
            ->where('code', 'seo_description')
            ->where('localisation_id', $localisationId)
            ->first();
            
        $requestProductMetaDescription = ($requestProductMetaDescription) ? 
            $requestProductMetaDescription->value 
            : $requestProduct
                ->requestProductProperties
                ->where('code', 'description')
                ->where('localisation_id', $localisationId)
                ->first()
                ->value ?? null;
        
        $requestProductMetaDescription = ($requestProductMetaDescription) ? $requestProductMetaDescription : 'Дефолтное описание для продукта на необходимой локализации';   
        
        $buyer = $requestProduct->profiles->id;
        
        //В зависимости от этого флага, определяется возможность добавить в контакты покупателя
        if( session()->get('active_profile') === null
            || session()->get('active_profile')->contacts()->where('contact_id',$buyer)->first() === null)
        {
            $isContact = false;
            
        } else
        {
            $isContact = true;
        }

        $myProfile = false;

        if(auth()->check())
        {
            $myProfile = !!(auth()->user()->profiles()->find($buyer));
        }
        
        return view(
            'catalog.request_product.show', 
            compact(
                'requestProduct',
                'buyer',
                'isContact',
                'requestProductMetaTitle',
                'requestProductMetaDescription',
                'myProfile',
                !empty($user) ? 'user' : null
            )
        );
    }
}
