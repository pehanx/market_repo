<?php

namespace App\Http\Controllers\Catalog;

use App\Helpers\Shop\ShopService;
use App\Helpers\Catalog\CatalogService;
use App\Helpers\Product\ProductService;
use App\Http\Controllers\Controller;
use App\Http\Requests\Catalog\Product\SearchParamsRequest;
use App\Http\Requests\Catalog\Product\WriteSupplierRequest;
use App\Http\Router\CategoryPath;
use App\Models\{Product, Category, Currency, CurrencyRate, Localisation};
use App\{ApplicationDictionary, User};
use Illuminate\Http\Request;
use Illuminate\Contracts\Mail\Mailer;// Мб в будущем будем использовать другой метод отправки уведомлений
use App\Mails\WriteSupplierMail;
use Illuminate\Support\Facades\Auth;
use Cache;
use Breadcrumbs;
use Illuminate\Http\Response;
use App\Http\Controllers\TranslateController;
// TODO: продумать какие данные отображать при отсутствии продуктов на определенной локализации интерфейса
// Исправить миграции с properties поле value - text
// Дефотные сео аттрибуты для товара на определенной локали хранить в словаре 
class ProductController extends Controller
{
    private $catalogService;
    private $shopService;
    private $translator;     
    
    public function __construct(CatalogService $catalogService, ShopService $shopService, TranslateController $translator, ProductService $productService)
    {
        $this->catalogService = $catalogService;
        $this->shopService = $shopService;
        $this->translator = $translator;
        $this->productService = $productService;
    }
    
    public function index(SearchParamsRequest $request, CategoryPath $categoryPath)
    {
        $searchParams = $request->except(['order_by']);
        $orderBy = $request->only(['order_by']) ? $this->catalogService->explodeOrderByParams($request->only(['order_by'])) : null;
        
        $user = Auth::user();
        $category = $categoryPath->category;
        $categoryIds = Category::descendantsAndSelf($category->id)->pluck('id')->all();
        $appLocId = Localisation::where('code', app('translator')->getLocale())->first()->id;
        
        $categoryName = $category
            ->categoryProperties
            ->where('code', 'name')
            ->where('localisation_id', $appLocId)
            ->first()
            ->value;
            
        $categoryMetaTitle = $category
            ->categoryProperties
            ->where('code', 'seo_title')
            ->where('localisation_id', $appLocId)
            ->first()
            ->value;
            
        $categoryMetaDescription = $category
            ->categoryProperties
            ->where('code', 'seo_description')
            ->where('localisation_id', $appLocId)
            ->first()
            ->value;
            
        $products = Product::getAllProductsFromCategories($categoryIds, (int)$appLocId, 12, $searchParams, $orderBy);
        $products->appends($request->input());
        
        $products->transform(function ($item) {

            //Приблуда чтобы менеджеры не отображались как магазин пользователя
            $userShops = $item->shops->users;

            $userShop = $item->first();

            foreach ($userShops as $shop)
            {
                if(!$shop->hasRole('site_manager'))
                {
                    $userShop = $shop;
                }
            }

            $item->shop_link = $this->shopService->getProductIndexRoute($userShop, app('translator')->getLocale(), null, null, null);
            return $item;
        });

        // Если у данной категории есть потомки, то выводим их в фильтре
        if(!empty($categoryIds) && is_countable($categoryIds) && count($categoryIds) > 1)
        {
            $categoryDescendants = Category::getCategoryDescendants($category->id, $appLocId, Product::class);
            
        // Если нет, то выводим фильтры для категории (для каждой - свои - в будущем)    
        }else
        {
            $countries = ApplicationDictionary::getDataForSelect($appLocId, 'countries');
            $colors = ApplicationDictionary::where('group_code', 'colors')->where('localisation_id', $appLocId)->get();
        }
        
        $back = Breadcrumbs::generate();
        $back = $back[$back->count() - 2];

        return view(
            'catalog.product.index',
            compact(
                'products',
                'categoryName',
                'categoryMetaTitle',
                'categoryMetaDescription',
                'back',
                !empty($categoryDescendants) ? 'categoryDescendants' : null,
                !empty($countries) ? 'countries' : null,
                !empty($colors) ? 'colors' : null,
                !empty($user) ? 'user' : null
            )
        );
    }
    
    public function show(Product $product)
    {
        if(!$product->is_active || !!!$product->profiles || !!!$product->categories)
        {
            abort(404);
        }
        
        $product->increment('shows');
        $user = Auth::user();

        $localisationId = $product->productProperties()->first()->localisation_id;
        $appLocId = Localisation::where('code', app('translator')->getLocale())->first()->id;
        
        $product = Cache::tags(Product::class)->rememberForever(
            "catalog_show_product_{$appLocId}_{$product->id}", 
            
            function() use ($product, $localisationId, $appLocId)
            {
                $product = Product::getProductWithModels($product, $localisationId, $appLocId, true);
                $product = Product::getSimilarProducts($product, $localisationId, 3);
                
                $product->setRelation('downloads', $product->downloads->groupBy('type'));
                
                $product->load([
                    'profiles' => function ($query) use($localisationId){
                        $query->with([
                            'profileProperties' => function ($query) use($localisationId){
                                $query->where([
                                    ['localisation_id', $localisationId],
                                    ['code', 'company_name']
                                ]);
                            }
                        ]);
                    }
                ]);

                //Приблуда чтобы менеджеры не отображались как магазин пользователя
                $userShops = $product->shops->users;

                $userShop = $userShops->first();

                foreach ($userShops as $shop)
                {
                    if(!$shop->hasRole('site_manager'))
                    {
                        $userShop = $shop;
                    }
                }

                $product->shop_link = route('shop.show', [
                    'user' => $userShop,
                    'localisation' => Localisation::where('id', $localisationId)->pluck('code')->first()
                ]);

                $product->customCharacteristics = array_filter($product->characteristics->toArray(), function($v, $k) {
                    return !in_array($k, $this->productService::ALLOWED_CHARACTERISTICS);
                }, ARRAY_FILTER_USE_BOTH);
                
                if($localisationId !== $appLocId)
                {
                    $localisationCode = Localisation::find($localisationId)->code;

                    $product->productProperties->transform(function ($item, $key) use($localisationCode){
                        $item->value = $this->translator->translateString($item->value, app('translator')->getLocale(), $localisationCode);
                        return $item;
                    });
                }
                
                return $product;
            }
        );
  
        $productMetaTitle = $product
            ->productProperties
            ->where('code', 'seo_title')
            ->where('localisation_id', $localisationId)
            ->first();
            
        $productMetaTitle = ($productMetaTitle) ? 
            $productMetaTitle->value 
            : $product
                ->productProperties
                ->where('code', 'name')
                ->where('localisation_id', $localisationId)
                ->first()
                ->value;
            
        $productMetaDescription = $product
            ->productProperties
            ->where('code', 'seo_description')
            ->where('localisation_id', $localisationId)
            ->first();
            
        $productMetaDescription = ($productMetaDescription) ? 
            $productMetaDescription->value 
            : $product
                ->productProperties
                ->where('code', 'description')
                ->where('localisation_id', $localisationId)
                ->first()
                ->value ?? null;
        
        $productMetaDescription = ($productMetaDescription) ? $productMetaDescription : 'Дефолтное описание для продукта на необходимой локализации'; 
        
        $supplier = $product->profiles->id;

        //В зависимости от этого флага, определяется возможность добавить в контакты поставщика
        if( session()->get('active_profile') === null
            || session()->get('active_profile')->contacts()->where('contact_id',$supplier)->first() === null)
        {
            $isContact = false;
            
        } else
        {
            $isContact = true;
        }

        $myProfile = false;

        if(auth()->check())
        {
            $myProfile = !!(auth()->user()->profiles()->find($supplier));
        }

        return view(
            'catalog.product.landing', 
            compact(
                'product',
                'supplier',
                'isContact',
                'productMetaTitle',
                'productMetaDescription',
                'myProfile',
                !empty($user) ? 'user' : null,
            )
        );
    }

    /* 
     * Модалка написать поставщику (Хотелка Рушана)
     */
    public function writeSupplierForm(WriteSupplierRequest $request, Product $product, Mailer $mailer)
    {

        $data = [
            'product_link' => route('catalog.product.show', ['product' => $product]),
            'product_name' => $product->productProperties->where('code', 'name')->first()->value,
            'company_name' => $request->company_name,
            'phone' => $request->phone,
            'email' => $request->email,
        ];

        $result = $mailer->to('sale5@g2r.ru')->send(new WriteSupplierMail($data));

        return response()->json(['result' => __('interface.messages.success') ], Response::HTTP_CREATED);
    }
}

//$currentCategoryName = \Breadcrumbs::generate()->last()->title;