<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Setting;
use App\Models\Currency;

class CurrencyController extends Controller
{
    /**
     * Запись выбранной валюты ( из списка возможных) в сессии пользователя.
     * Валюта в дальнейшем считывается и устанавливается в middleware Currency.
     *
     * @param string $currency
     * @return void
     */
    public function setCurrency(Request $request, string $currency)
    {
        if ( Currency::where('code', $currency)->first() === null  )
        {
            $currency = Currency::where('is_base',true)->first()->code;
        }

        if(auth()->user() !== null) // Если есть авторизация, пока что записываем валюту как предпочитаемую
        {
            $this->setDefaultCurrency($currency);
            \Session::put('prefer_currency', $currency);
        }

        \Session::put('currency',$currency);
        
        return redirect()->to( url()->previous() );
    }

    /**
     * Запись выбранной валюты ( из списка возможных) в таблице настроек пользователя.
     * Потом будет для локальных настроек кабинета пользователя
     * @param string $currency
     * @return void
     */
    public function setDefaultCurrency($currency)
    {
        if ( Currency::where('code', $currency)->first() === null )
        {
            \Session::put('prefer_currency', $currency);

            Setting::addSetting( 'prefer_currency', 'cabinet', $currency, auth()->user() );

            return redirect()->to( url()->previous() );
        } 
        
        return redirect()->to( url()->previous() );
    }
}
