<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use LaravelLocalization;
use App\User;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function showLoginForm()
    {
        if(url()->previous())
        {
            session([ 'previous_url' => parse_url(url()->previous())['path'] ]);
        }
        
        return view('auth.login');
    }

    protected function authenticated(Request $request, $user)
    {
        $user->session_id = session()->getID();
        $user->update();

        if ( \Session::has('prefer_locale') && in_array(\Session::get('prefer_locale'), config('app.locales')) )
        {
            $localisation = \Session::get('prefer_locale');

        } else
        {
            $localisation = User::getUserSetting( auth()->user()->id, 'prefer_locale') ?? config('app.fallback_locale') ;
            \Session::put('prefer_locale', $localisation);
        }

        \Session::put('locale', $localisation);
        
        return redirect()->to(LaravelLocalization::localizeUrl(session('previous_url'), $localisation));
    }

    public function logout(Request $request)
    {
        if( auth()->user() )
        {
            $this->destroySid( auth()->user() );
        }

        $this->guard()->logout();

        $request->session()->invalidate();

        $request->session()->regenerateToken();

        return $this->loggedOut($request) ?: redirect('/');
    }

    public function destroySid(User $user)
    {
        $user->session_id = null;
        $user->update();
    }
}
