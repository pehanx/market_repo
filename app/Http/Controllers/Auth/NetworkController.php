<?php
namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Helpers\Auth\NetworkService;
use Illuminate\Support\Facades\Auth;
use Laravel\Socialite\Facades\Socialite;

class NetworkController extends Controller
{
    private $service;

    /**
     * NetworkController зависит от сервиса NetworkService
     *
     * @param App\Helpers\Auth\NetworkService $service
     *
     */
    public function __construct(NetworkService $service)
    {
        $this->service = $service;
    } 
    
    /**
     * Редирект на страницу входа социальной сети
     *
     * @param string $network
     */
    public function redirect(string $network)
    {
        return Socialite::driver($network)->redirect();
    }

    /**
     * Обработка данных, пришедших из социальной сети.
     *
     * @param string $network
     */
    public function callback(string $network)
    {
        $networkUser = Socialite::driver($network)->stateless()->user();
        $user = $this->service->auth($network, $networkUser);
        Auth::login($user);
        return redirect()->intended();
    }
}