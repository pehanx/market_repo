<?php

namespace App\Http\Controllers\Admin;

use App\Models\{ProfileType, Profile};
use App\User;
use Illuminate\Http\Request;
use App\Helpers\Profile\ProfileService;
use App\Http\Controllers\Controller;

class ProfilesController extends Controller
{
    private $service;

    public function __construct(ProfileService $service)
    {
        $this->service = $service;
    }

    public function index(Request $request)
    {
        $users = User::getUsersWithProfiles($request, 10);
        $profileTypes = ProfileType::where('code', '!=', 'account')->select('code')->get();
        
        return view('admin.profiles.index', compact('users', 'profileTypes'));
    }
    
    public function destroy(Profile $profile)
    {
        $result = $this->service->destroy ($profile, auth()->user());
        
        if($result)
        {
            return redirect()->route('admin.profiles.index');
        }
    }
    
    /**
     * Активируем профиль пользователя.
     *
     * @param  \App\Profile  $profile
     *
     */
    public function approval(Profile $profile)
    {
        $profile->status = 'active';
        $profile->save();
        
        return redirect()->back();
    }
    
     /**
     * Отключаем профиль пользователя.
     *
     * @param  \App\Profile  $profile
     *
     */
    public function disable(Profile $profile)
    {
        $profile->status = 'inactive';
        $profile->save();
        
        $profile->users()->role(['buyer', 'seller'])
            ->update(['session_id' => null]);

        return redirect()->back();
    }
}
