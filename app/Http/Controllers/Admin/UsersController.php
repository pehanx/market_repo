<?php

namespace App\Http\Controllers\Admin;

use App\Models\{ProfileType, Localisation};
use App\User;
use App\Http\Requests\Cabinet\Profile\EditRequest;
use App\Http\Requests\Cabinet\Profile\ApproveRequest;
use App\Helpers\Profile\ProfileService;
use App\Http\Controllers\Controller;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UsersController extends Controller
{
    private $service;

    public function __construct(ProfileService $service)
    {
        $this->service = $service;
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $users = User::getUsersWithAccounts($request, 10);

        return view('admin.users.index', compact('users'));
    }

    public function addRole(Request $request)
    {
        $role = $request->role;

        User::find($request->user)->assignRole($role);
    }

    public function login(Request $request)
    {
        Auth::loginUsingId($request->id);
    }
}
