<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

class HomeController extends Controller
{
    public function index()
    {
        dd(\App\User::find(4)->profiles()->get(), \App\Models\Profile::find(16)->users()->get() );
        return view('admin.home');
    }
}
