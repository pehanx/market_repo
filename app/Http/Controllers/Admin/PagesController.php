<?php

namespace App\Http\Controllers\Admin;

use App\Models\{Page, Localisation, PageProperty};
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Helpers\Page\PageService;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\Admin\Page\{CreateRequest, CreatePropertiesRequest, EditRequest, EditPropertiesRequest};

class PagesController extends Controller
{
    private $service;

    public function __construct(PageService $service)
    {
        $this->service = $service;
    }
    
    public function index(Request $request)
    {
        $localisationId = Localisation::where('code', 'ru')->first()->id;
        
        // appends title attribute
        $pages = Page::defaultOrder()
            ->withDepth()
            ->with([
                'pageProperties' => function ($query) use ($localisationId){
                    $query
                        ->where(function($query){
                            return $query
                                   ->where('code', 'menu_title')
                                   ->orWhere('code', 'title');
                        })
                        ->where('localisation_id', $localisationId);
                }
            ])
            ->paginate(30);
        
        return view('admin.pages.index', compact('pages'));
    }
    // Условие для кнопки добавить ещё категорий
    public function show(Page $page)
    {   
        $link = url()->to('/') . '/' . Page::getFullLink($page);
        $localisations = Localisation::select('code', 'id')->get()->keyBy('id')->pluck('code')->all();

        $page->load('pageProperties');
        
        $pageProperties = $page->pageProperties
            ->each(function ($item) use ($localisations){
                $item->localisation_code = $localisations[$item->localisation_id - 1];
            });
        
        $pageProperties = $page->pageProperties->groupBy('localisation_code');
        
        $addMoreProperties = (count($localisations) > $pageProperties->count()) ? true : false;
        
        $page->setRelation('pageProperties', $pageProperties); 
      
        return view('admin.pages.show', compact('page', 'addMoreProperties', 'link'));
    }
    
    public function create()
    {
        $localisationId = Localisation::where('code', 'ru')->first()->id;
        
        // appends title attribute
        $parents = Page::defaultOrder()
            ->withDepth()
            ->with([
                'pageProperties' => function ($query) use ($localisationId){
                    $query
                        ->where(function($query){
                            return $query
                                   ->where('code', 'menu_title')
                                   ->orWhere('code', 'title');
                        })
                        ->where('localisation_id', $localisationId);
                }
            ])
            ->get();
            
        $localisations = Localisation::all();

        return view('admin.pages.create', compact('parents', 'localisations'));
    }
    
    public function store(CreateRequest $request)
    {
        $user = Auth::user();
        $page = $this->service->create($user, $request->except(['_token', 'files']));

        return redirect()->route('admin.pages.show', $page);
    }
    
    public function addProperties(Page $page)
    {
        if($page->parent)
        {
            $parent = $page
                ->parent
                ->load([
                    'pageProperties' => function ($query){
                        $query
                            ->where(function($query){
                                return $query
                                       ->where('code', 'menu_title')
                                       ->orWhere('code', 'title');
                            })
                            ->where('localisation_id', Localisation::where('code', 'ru')->first()->id);
                    }
                ]);
                
        } else
        {
            $parent = null;
        }
        
        $localisationCodes = Localisation::select('code', 'id')->get()->keyBy('id')->pluck('code')->all();
        $localisations = Localisation::all()->keyBy('id');
        
        $page->load('pageProperties');
        
        $pageProperties = $page->pageProperties
            ->each(function ($item) use ($localisationCodes, & $localisations){
                $item->localisation_code = $localisationCodes[$item->localisation_id - 1];
                $localisations->forget($item->localisation_id);
            });
        
        $pageProperties = $page->pageProperties->groupBy('localisation_code');
        
        $page->setRelation('pageProperties', $pageProperties); 
        
        return view('admin.pages.add_properties', compact('page', 'localisations', 'parent'));
    }
    
    public function storeProperties(CreatePropertiesRequest $request, Page $page)
    {
        $user = Auth::user();
        $user->pages()->where('id', $page->id)->firstOrFail();
        
        $page = $this->service->addProperties($page, $request->except(['_token', 'files']));

        return redirect()->route('admin.pages.show', $page);
    }
    
    public function editProperties(Request $request, Page $page)
    {
        $localisation = Localisation::where('code', $request->route('localisation'))->firstOrFail();
        
        if($page->parent)
        {
            $parent = $page
                ->parent
                ->load([
                    'pageProperties' => function ($query) {
                        $query
                           ->where('code', 'menu_title')
                           ->orWhere('code', 'title');
                    }
                ]);
                
        } else
        {
            $parent = null;
        }

        $page
            ->load([
                'pageProperties' => function ($query) use ($localisation){
                    $query
                        ->where('localisation_id', $localisation->id);
                }
            ]);

        return view('admin.pages.edit.properties', compact('page', 'localisation', 'parent'));
    }
    
    public function updateProperties(EditPropertiesRequest $request, Page $page)
    {
        $localisation = Localisation::where('code', $request->route('localisation'))->firstOrFail();
        
        PageProperty::where([
            ['localisation_id', $localisation->id],
            ['page_id', $page->id],
        ])
        ->delete();
        
        $page = $this->service->addProperties($page, array_merge($request->all(), ['localisation' => $localisation->id]));

        return redirect()->route('admin.pages.show', $page);
    }
    
    public function disable(Page $page)
    {
        $user = Auth::user();
        $page = $user->pages()->where('id', $page->id)->firstOrFail();   

        $page->is_active = false;
        $page->save();
        
        return redirect()->route('admin.pages.show', $page);
    }
    
    public function activate(Page $page)
    {
        $user = Auth::user();
        $page = $user
            ->pages()
            ->where([
                ['id', $page->id],
                ['is_active', false]
            ])
            ->firstOrFail(); 
            
        $page->is_active = true;
        $page->save();
        
        return redirect()->route('admin.pages.show', $page);
    }
    
    public function delete(Page $page)
    {
        $user = Auth::user();
        $page = $user->pages()->where('id', $page->id)->firstOrFail(); 
        
        $page->delete();

        return redirect()->route('admin.pages.index');
    } 
    
    public function deleteProperties(Request $request, Page $page)
    {
        $user = Auth::user();
        $page = $user->pages()->where('id', $page->id)->firstOrFail(); 
        $localisation = Localisation::where('code', $request->route('localisation'))->firstOrFail();
        
        $redirect = null;
        
        PageProperty::where([
            ['localisation_id', $localisation->id],
            ['page_id', $page->id],
        ])
        ->delete(); 
        
        if($page->pageProperties->count() === 0)
        {
            $page->delete();
            unset($page);
        }
        
        if(isset($page))
        {
            $redirect = redirect()->route('admin.pages.show', $page);
            
        } else 
        {
            $redirect = redirect()->route('admin.pages.index');
        }

        return $redirect;
    } 
    
    // Поменять парент и линк
    public function edit(Page $page)
    {
        $localisationId = Localisation::where('code', 'ru')->first()->id;
        
        // appends title attribute
        $parents = Page::defaultOrder()
            ->withDepth()
            ->with([
                'pageProperties' => function ($query) use ($localisationId){
                    $query
                        ->where(function($query){
                            return $query
                                   ->where('code', 'menu_title')
                                   ->orWhere('code', 'title');
                        })
                        ->where('localisation_id', $localisationId);
                }
            ])
            ->where('id', '!=', $page->id)
            ->get();
            
        return view('admin.pages.edit.page', compact('page', 'parents'));
    }
    
    public function update(EditRequest $request, Page $page)
    {
        $user = Auth::user();
        $page = $user->pages()->where('id', $page->id)->firstOrFail(); 
        
        if($page->id === $request->parent)
        {
            abort(422);
        }
        
        $page->update([
            'link' => $request->link,
            'parent_id' => $request->parent ?? null
        ]);
        
        return redirect()->route('admin.pages.show', $page);
    } 
    
    public function first(Page $page)
    {
        if ($first = $page->siblings()->defaultOrder()->first()) 
        {
            $page->insertBeforeNode($first);
        }

        return redirect()->route('admin.pages.index');
    }

    public function up(Page $page)
    {
        $page->up();

        return redirect()->route('admin.pages.index');
    }

    public function down(Page $page)
    {
        $page->down();

        return redirect()->route('admin.pages.index');
    }

    public function last(Page $page)
    {
        if ($last = $page->siblings()->defaultOrder('desc')->first()) 
        {
            $page->insertAfterNode($last);
        }

        return redirect()->route('admin.pages.index');
    }
}
