<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Response;
use App\Http\Requests\Cabinet\Chat\ChatMessagesRequest;
use App\Http\Controllers\Controller;
use App\Models\{Chat,Message,Profile};
use App\User;
use App\Events\Chat\ChatDBSend;
use App\Events\Chat\ChatPush;
use App\Events\Chat\ChatBroadcast;
use App\Helpers\Chat\{ChatService, ChatSupportService};

class ChatController extends Controller
{
    public function __construct(ChatService $service, ChatSupportService $supportService)
    {
        $this->service = $service;
        $this->supportService = $supportService;
        $this->middleware('auth');
    }
    
    /*
     * Вьюшка чата, для админа
     */
    public function view()
    {
        if( !auth()->user()->hasRole('admin') )
        {
            abort(403);
        }
        
        $userSupportHash = $this->service::getHash( auth()->user()->id, 'support' );

        $userAvatar = auth()->user()->downloads->where('type', 'avatar')->first()
            ? asset('storage/' . auth()->user()->downloads->where('type', 'avatar')->first()->path)
            : asset('assemble/images/cabinetImages/avatar_default.png');

        return view('admin.chat')->with([
            'userSupportHash' => $userSupportHash,
            'userAvatar' => $userAvatar,
        ]);
    }

    /*
     * Вьюшка чата, чтобы написать пользователю
     */
    public function writeToUser(int $memberId)
    {
        $member = User::find($memberId);

        $hasChat = (bool) Chat::isChatWithUserExists( auth()->user(), $member->id);           
        
        if ( $member->id !== auth()->user()->id && !!!$hasChat)
        {
            $this->supportService->createChat( $member, 'admin' );
        }

        $userSupportHash = $this->service::getHash( auth()->user()->id, 'support' );

        $userAvatar = auth()->user()->downloads->where('type', 'avatar')->first()
            ? asset('storage/' . auth()->user()->downloads->where('type', 'avatar')->first()->path)
            : asset('assemble/images/cabinetImages/avatar_default.png');

        return view('admin.chat')->with([
            'userSupportHash' => $userSupportHash,
            'userAvatar' => $userAvatar,
        ]);
    }
    
}