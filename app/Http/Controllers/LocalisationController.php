<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Setting;
use LaravelLocalization;

class LocalisationController extends Controller
{
    /**
     * Запись выбранной локализации ( из списка возможных) в сессии пользователя.
     * Локализация в дальнейшем считывается и устанавливается в middleware Localisation.
     *
     * @param string $localisation
     * @return void
     */
    public function setLocalisation(Request $request, string $localisation)
    {
        if ( !in_array($localisation, \Config::get('app.locales')) )
        {
            $localisation = \Config::get('app.fallback_locale');
        }

        if(auth()->user() !== null) // Если есть авторизация, пока что записываем локаль как предпочитаемую
        {
            $this->setDefaultLocale($localisation);
            \Session::put('prefer_locale', $localisation);
        }

        \Session::put('locale',$localisation);
        
        $redirect = '';
        $previousUrl = url()->previous();
 
        if(mb_strpos($previousUrl, 'catalog/products'))
        {
            $redirect = redirect()->to(LaravelLocalization::localizeUrl(route('catalog.index'), $localisation));
            
        } elseif(mb_strpos($previousUrl, 'catalog/request_products'))
        {
            $redirect = redirect()->to(LaravelLocalization::localizeUrl(route('catalog.request_product.index'), $localisation));
            
        } elseif(mb_strpos($previousUrl, 'search'))
        {
            $redirect = redirect()->to(LaravelLocalization::localizeUrl(route('main'), $localisation));
            
        } else
        {
            $redirect = redirect()->to(LaravelLocalization::localizeUrl($previousUrl, $localisation));
        }
    
        return $redirect; 
    }

    /**
     * Запись выбранной локализации ( из списка возможных) в таблице настроек пользователя.
     * Потом будет для локальных настроек кабинета пользователя
     * @param string $localisation
     * @return void
     */
    public function setDefaultLocale($localisation)
    {
        if ( in_array($localisation, \Config::get('app.locales')) )
        {
            \Session::put('prefer_locale', $localisation);

            Setting::addSetting( 'prefer_locale', 'cabinet', $localisation, auth()->user() );

            return redirect()->to(LaravelLocalization::localizeUrl(url()->previous(), $localisation));
        } 
        
        abort(404);
    }
}
