<?php
// TODO: написать метод show, закрыть rbacom
namespace App\Http\Controllers\Cabinet;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Helpers\RequestProduct\RequestProductService;
use App\Models\{Profile, Category, RequestProduct, Localisation, CategoryProperty};
use App\{User, ApplicationDictionary};
use App\Http\Requests\Cabinet\RequestProduct\{FilterRequest, ActionRequest, CreateRequest, EditRequest};

class RequestProductController extends Controller
{
    public function __construct(RequestProductService $service)
    {
        $this->service = $service;

        $this->middleware('check_active_profile:buyer');// Проверить что у пользователя выбран профиль покупателя и он висит в сессии
        // $this->middleware('permission:list request_product', ['only' => ['index', 'indexInactive', 'indexDeleted', 'show']]);
        // $this->middleware('permission:create request_product', ['only' => ['chooseCategory', 'fillProperties', 'store', 'makeAction']]);
        // $this->middleware('permission:edit request_product', ['only' =>['edit', 'update', 'disable', 'activate', 'makeAction']]);
        // $this->middleware('permission:delete request_product', ['only' =>['delete', 'destroy', 'makeAction']]);
    }
    
    public function show(Profile $profile, RequestProduct $requestProduct)
    {
        // Проверяем принадлежность пользователю профиля
        if ( !$profile->users()->find( auth()->user()->id ) || !$profile->requestProducts()->find($requestProduct->id) || (int)\Session::get('active_profile')->id !== (int)$profile->id)
        {
            abort(403);
        }

        $localisationId = $profile->profileProperties()->first()->localisation_id;
        $appLocalisationId = Localisation::where('code', app('translator')->getLocale())->first()->id;
        
        $requestProduct = RequestProduct::getRequestProduct($requestProduct, (int)$localisationId, $appLocalisationId);
        $requestProduct->category = CategoryProperty::where('category_id', $requestProduct->category_id)->where('localisation_id', $appLocalisationId)->first()->value;
        $requestProduct->setRelation('downloads', $requestProduct->downloads->groupBy('type'));
        
        $category = Category::findOrFail($requestProduct->category_id)
            ->load(['categoryProperties' => function ($query) use($appLocalisationId){
                $query
                    ->where('code', 'name')
                    ->where('localisation_id', $appLocalisationId);
            }]);
        
        return view('cabinet.request_product.show', compact('profile', 'requestProduct', 'category'));
    }

    public function index(Profile $profile, FilterRequest $request)
    {
        // Проверяем принадлежность пользователю профиля
        if ( !$profile->users()->find( auth()->user()->id ))
        {
            abort(403);
        }

        $requestProducts = collect();
        $localisationId = $profile->profileProperties()->first()->localisation_id;
        $appLocalisationId = Localisation::where('code', app('translator')->getLocale())->first()->id;

        if($profile->requestProducts()->where('is_active', 'true')->first())
        {
            $requestProducts = RequestProduct::getFiltrateRequestProducts($profile->requestProducts()->where('is_active', 'true'), $request->except(['full_filter']))->paginate(3);

            // Записали в пагинацию GET параметры фильтра
            $requestProducts->appends($request->input());

            RequestProduct::getAllRequestProducts(collect($requestProducts->items()), (int)$localisationId, (int)$appLocalisationId);
        }

        //$exportFormats = Export::EXPORTED_FORMATS;
        
        $categories = Category::getDataForSelect($appLocalisationId, 'name');
        //dd(collect($requestProducts->items())->groupBy('link'));
        return view('cabinet.request_product.index.active', compact('profile', 'categories', 'requestProducts'));
    }

    public function indexInactive(Profile $profile, FilterRequest $request)
    {
        // Проверяем принадлежность пользователю профиля
        if ( !$profile->users()->find( auth()->user()->id ))
        {
            abort(403);
        }

        $requestProducts = collect();
        $localisationId = $profile->profileProperties()->first()->localisation_id;
        $appLocalisationId = Localisation::where('code', app('translator')->getLocale())->first()->id;

        if($profile->requestProducts()->where('is_active', 'false')->first())
        {
            $requestProducts = RequestProduct::getFiltrateRequestProducts($profile->requestProducts()->where('is_active', 'false'), $request->except(['full_filter']))->paginate(3);

            // Записали в пагинацию GET параметры фильтра
            $requestProducts->appends($request->input());

            RequestProduct::getAllRequestProducts(collect($requestProducts->items()), (int)$localisationId, (int)$appLocalisationId);
        }

        //$exportFormats = Export::EXPORTED_FORMATS;
        
        $categories = Category::getDataForSelect($appLocalisationId, 'name');

        return view('cabinet.request_product.index.inactive', compact('profile', 'requestProducts', 'categories'));
    }

    public function indexDeleted(Profile $profile, FilterRequest $request)
    {
        // Проверяем принадлежность пользователю профиля
        if ( !$profile->users()->find( auth()->user()->id ))
        {
            abort(403);
        }

        $requestProducts = collect();
        $localisationId = $profile->profileProperties()->first()->localisation_id;
        $appLocalisationId = Localisation::where('code', app('translator')->getLocale())->first()->id;

        if($profile->requestProducts()->onlyTrashed()->first())
        {
            $requestProducts = RequestProduct::getFiltrateRequestProducts($profile->requestProducts()->onlyTrashed()->whereNull('destroyed_at'), $request->except(['full_filter']))->paginate(3);

            // Записали в пагинацию GET параметры фильтра
            $requestProducts->appends($request->input());

            RequestProduct::getAllRequestProducts(collect($requestProducts->items()), (int)$localisationId, (int)$appLocalisationId);
        }

        //$exportFormats = Export::EXPORTED_FORMATS;
        
        $categories = Category::getDataForSelect($appLocalisationId, 'name');

        return view('cabinet.request_product.index.deleted', compact('profile', 'requestProducts', 'categories'));
    }

    public function chooseCategory(Profile $profile)
    {
        // Проверяем принадлежность пользователю профиля
        if ( !$profile->users()->find( auth()->user()->id ) || (int)\Session::get('active_profile')->id !== (int)$profile->id)
        {
            abort(403);
        }

        $appLocId = Localisation::where('code', app('translator')->getLocale())->first()->id;
        
        $categories = \Cache::tags(Category::class)->rememberForever('full_tree_' . $appLocId, function() use($appLocId) {
            return Category::getFullTree($appLocId)->first()->children->toArray();
        });

        return view('cabinet.request_product.create.choose_category', compact('profile', 'categories'));
    }

    public function fillProperties(Profile $profile, $categoryId)
    {
        // Проверяем принадлежность пользователю профиля
        if ( !$profile->users()->find( auth()->user()->id ) || (int)\Session::get('active_profile')->id !== (int)$profile->id)
        {
            abort(403);
        }

        // Категория, выбранная на предыдущем шаге пользователем
        $category = Category::findOrFail($categoryId)
            ->load(['categoryProperties' => function ($query) {
                $query
                    ->where('code', 'name');
            }]);

        $localisationId = Localisation::where('code', app('translator')->getLocale())->first()->id;

        $quantities = ApplicationDictionary::where('group_code', 'quantities')->where('localisation_id', $localisationId)->get();
        //dd($quantities);
        return view('cabinet.request_product.create.fill_properties', compact('profile', 'category', 'quantities'));
    }
    //ок
    public function store(Profile $profile, CreateRequest $request)
    {
        // Проверяем принадлежность пользователю профиля
        if ( !$profile->users()->find( auth()->user()->id ) || !$request->filled('category_id') || (int)\Session::get('active_profile')->id !== (int)$profile->id)
        {
            abort(403);
        }

        Category::findOrFail($request->category_id);

        $localisationId = $profile->profileProperties()->first()->localisation_id;

        // Создаем товар для профиля(локализация профиля = локализации товара) пользователя
        $requestProduct = $this->service->create($profile, (int)$localisationId, $request->except(['_token', 'detail_images', 'deleted_files']));
        
        $deletedFiles = $request->filled('deleted_files') ? json_decode($request->deleted_files, true) : null;
        
        if($deletedFiles)
        {
            $requestProductFiles = $this->filtrateInputFiles($request->allFiles(), $deletedFiles);
            
        }else
        {
            $requestProductFiles = $request->allFiles();
        }

        if( !empty($requestProductFiles['detail_images']) )
        {
            // Чтобы грохнуть файлы определенного профиля быстро, если понадобиться
            $folderForImages = 'request_products/' . $profile->id;
            $productPreviewImage = $this->addFiles($requestProduct,  [current($requestProductFiles['detail_images'])], 'preview_image', $folderForImages);
            $productDetailImages = $this->addFiles($requestProduct,  $requestProductFiles['detail_images'], 'detail_images', $folderForImages);
        }

        return redirect()->route('cabinet.request_product.index', ['profile' => $profile->id]);
    }

    public function edit(Profile $profile, RequestProduct $requestProduct)
    {
        // Проверяем принадлежность пользователю профиля
        if ( !$profile->users()->find( auth()->user()->id ) || !$profile->requestProducts()->find($requestProduct->id) || (int)\Session::get('active_profile')->id !== (int)$profile->id)
        {
            abort(403);
        }

        $localisationId = $profile->profileProperties()->first()->localisation_id; 
        $appLocalisationId = Localisation::where('code', app('translator')->getLocale())->first()->id;
        $requestProduct = RequestProduct::getRequestProduct($requestProduct, (int)$localisationId, $appLocalisationId);
        $requestProduct->setRelation('downloads', $requestProduct->downloads->groupBy('type'));

        $categories = Category::getDataForSelect($appLocalisationId, 'name');
        $quantities = ApplicationDictionary::where('group_code', 'quantities')->where('localisation_id', $appLocalisationId)->get();

        return view('cabinet.request_product.edit', compact('profile', 'requestProduct', 'categories', 'quantities'));
    }

    public function update(Profile $profile, RequestProduct $requestProduct, EditRequest $request)
    {
        // Проверяем принадлежность пользователю профиля
        if ( !$profile->users()->find( auth()->user()->id ) || !$profile->requestProducts()->find($requestProduct->id) || (int)\Session::get('active_profile')->id !== (int)$profile->id)
        {
            abort(403);
        }

        // Если он хочет удалить все фото для описания запроса и ничего не добавил взамен - хрена лысого ему
        if($request->has('delete_files') && !array_diff($requestProduct->downloads->where('type', 'detail_images')->pluck('id')->toArray(), $request->delete_files) && !$request->filled('detail_images'))
        {
            return redirect()->back()->withInput()->withErrors(['detail_images' => __('validation.required', ['attribute' => __('validation.attributes.detail_images')] ) ]);
        }
        
        $deletedFiles = $request->filled('deleted_files') ? json_decode($request->deleted_files, true) : null;
        
        if($deletedFiles)
        {
            $requestProductFiles = $this->filtrateInputFiles($request->allFiles(), $deletedFiles);
            
        }else
        {
            $requestProductFiles = $request->allFiles();
        }

        if($request->has('delete_files'))
        {
            $countDeletedFiles = $this->softDeleteFiles($requestProduct,  $request->delete_files);
        }

        $localisationId = $profile->profileProperties()->first()->localisation_id;

        $requestProduct = $this->service->update($profile, $requestProduct, (int)$localisationId, $request->except(['delete_files', 'deleted_files', 'detail_images', '_token', '_method']));

        if($request->has('detail_images') && $request->detail_images)
        {
            // Чтобы грохнуть файлы определенного профиля быстро, если понадобиться
            $folderForImages = 'request_products/' . $profile->id;
            $productPreviewImage = $this->addFiles($requestProduct,  [current($requestProductFiles['detail_images'])], 'preview_image', $folderForImages);
            $productDetailImages = $this->addFiles($requestProduct,  $requestProductFiles['detail_images'], 'detail_images', $folderForImages);
        }

        return redirect()->back();
    }

    /**
     * Удаляем запрос на покупку (ок)
     *
     * @param App\Models\Profile $profile
     *
     */
    public function delete(Profile $profile, RequestProduct $requestProduct)
    {
        // Проверяем принадлежность пользователю профиля
        if ( !$profile->users()->find( auth()->user()->id ) || (int)\Session::get('active_profile')->id !== (int)$profile->id)
        {
            abort(403);
        }

        $this->service->delete($profile, $requestProduct->id);

        return redirect()->back();
    }

    /**
     * Помечаем как мусор товар с его торговыми предложениями (ок)
     *
     * @param App\Models\Profile $profile
     * @param int $requestProductId
     *
     * @return void
     */
    public function destroy(Profile $profile, int $requestProductId)
    {
        // Проверяем принадлежность пользователю профиля
        if ( !$profile->users()->find( auth()->user()->id ) )
        {
            abort(403);
        }

        $this->service->destroy($profile, $requestProductId);

        return redirect()->back();
    }

     /**
     * Деактивируем запрос на покупку (ок)
     *
     * @param App\Models\Profile $profile
     *
     */
    public function disable(Profile $profile, RequestProduct $requestProduct)
    {
        // Проверяем принадлежность пользователю профиля
        if ( !$profile->users()->find( auth()->user()->id ) || (int)\Session::get('active_profile')->id !== (int)$profile->id)
        {
            abort(403);
        }

        $this->service->disable($profile, $requestProduct->id);

        return redirect()->back();
    }

    /**
     * Активируем запрос на покупку (ок)
     *
     * @param App\Models\Profile $profile
     *
     */
    public function activate(Profile $profile, RequestProduct $requestProduct)
    {
         // Проверяем принадлежность пользователю профиля
        if ( !$profile->users()->find( auth()->user()->id ) || (int)\Session::get('active_profile')->id !== (int)$profile->id)
        {
            abort(403);
        }

        $this->service->activate($profile, $requestProduct->id);

        return redirect()->back();
    }

    /**
     * Восстанавливаем запрос из удаленных (ок)
     *
     * @param App\Models\Profile $profile
     * @param int $requestProductId
     *
     * @return void
     */
    public function restore(Profile $profile, int $requestProductId)
    {
        // Проверяем принадлежность пользователю профиля
        if ( !$profile->users()->find( auth()->user()->id ) )
        {
            abort(403);
        }

        $this->service->restore($profile, $requestProductId);

        return redirect()->back();
    }

    public function makeAction(Profile $profile, ActionRequest $request)
    {
        //dump($request->all());exit();
        // Проверяем принадлежность пользователю профиля
        if ( !$profile->users()->find( auth()->user()->id ) )
        {
            abort(403);
        }

        if($request->filled('elements') && in_array($request->action, $this->service::ALLOWED_ACTIONS) && mb_strpos($request->action, '_all') === false )
        {
            $action = str_replace('_checked', '', $request->action);
            //dd($action);
            foreach($request->elements as $id)
            {
                if(method_exists($this->service, $action))
                {
                    $this->service->$action($profile, $id);
                }
            }

        } elseif(in_array($request->action, $this->service::ALLOWED_ACTIONS) && mb_strpos($request->action, '_all') !== false)
        {
            $action = str_replace('_all', '_requestProducts', $request->action);
            $action = explode('_', $action);
            $action = lcfirst(implode('', array_map('ucfirst', $action)));
            //dd($action);
            if(method_exists($this->service, $action))
            {
                $this->service->$action($profile);
            }
        }

        return redirect()->back();
    }
}
