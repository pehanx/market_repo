<?php

namespace App\Http\Controllers\Cabinet;

use App\Models\{Profile, Localisation, ProfileProperty, ProfileType};
use App\{User, ApplicationDictionary};
use App\Http\Requests\Cabinet\Profile\CreateRequest;
use App\Http\Requests\Cabinet\Profile\EditRequest;
use App\Http\Requests\Cabinet\Profile\ApproveRequest;
use App\Helpers\Profile\ProfileService;
use App\Http\Controllers\Controller;
use App\Events\Profile\ProfileAdded;
use Illuminate\Http\Request;

class ProfileController extends Controller
{
    private $service;

    public function __construct(ProfileService $service)
    {
        $this->service = $service;
        
        $this->middleware('permission:list profile', ['only' => ['index','show','showDocuments']]);
        $this->middleware('permission:create profile', ['only' => ['create','store']]);
        $this->middleware('permission:edit profile', ['only' => ['edit','update']]);
        $this->middleware('permission:approve profile', ['only' => ['approve', 'approval', 'selectActiveProfile']]);
        $this->middleware('permission:delete profile', ['only' => ['destroy']]);
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $user = auth()->user();
        $type = $request->route('type');
        $hasPossibleLocales = !!Localisation::getPossibleLocales($user, $type);

        if( !in_array($type, $this->service::ALLOWED_TYPES) )
        {
            abort(403);
        }
        
        // Если профилей y пользователя нет, отобразим кнопку создать профиль на фронте.
        $profiles = Profile::getProfilesForUser($user->id, $type);
        //$profiles = collect();
        return view("cabinet.profile.index.{$type}", compact('profiles', 'type', 'hasPossibleLocales'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {  
        $type = $request->route('type');
        $user = auth()->user();
        $localisations = Localisation::getPossibleLocales($user, $type);
       
        if( !in_array($type, $this->service::ALLOWED_TYPES) || !$localisations)
        {
            abort(403);
        }
        
        $localisationId = Localisation::where('code', app('translator')->getLocale())->first()->id;
        $countries = ApplicationDictionary::getDataForSelect($localisationId, 'countries');
        $account = Profile::getAccountForUser((int)$user->id)->first();
        
        if($account)
        {
            $account = $account
                ->profileProperties
                ->mapWithKeys(function ($item) {
                    return [$item['code'] => $item['value']];
                });
        }
  
        return view(
            "cabinet.profile.create.{$type}", 
            compact(
                'user',
                'account',
                'localisations',
                'type',
                'countries'
            )
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateRequest $request)
    {

        //Костыль для Гришы
        if($request->has('number_phone'))
        {
            $request->merge([
               'number_phone' => str_replace(['+','-'], '', filter_var($request->number_phone, FILTER_SANITIZE_NUMBER_INT))
            ]);
        }

        if($request->has('number_phone_mobile'))
        {
            $request->merge([
                'number_phone_mobile' => str_replace(['+','-'], '', filter_var($request->number_phone_mobile, FILTER_SANITIZE_NUMBER_INT))
             ]);
        }

        $deletedFiles = $request->filled('deleted_files') ? json_decode($request->deleted_files, true) : null;
        
        if($deletedFiles)
        {
            $profileFiles = $this->filtrateInputFiles($request->allFiles(), $deletedFiles);
            
        }else
        {
            $profileFiles = $request->allFiles();
        }

        $user = auth()->user();

        if(auth()->user()->hasRole('site_manager'))
        {
            $user = User::find($request->company_user);
        }

        // Сохраняем профиль для данного пользователя
        $profile = $this->service->create($user, $request->except( array_merge(array_keys($profileFiles), ['_token', 'deleted_files']) ) );
        
        if($profileFiles)
        {
            foreach ($profileFiles as $fileType => & $file) 
            {
                if( in_array($fileType, $this->service::ALLOWED_FILES) )
                {
                    if(is_object($file))
                    {
                        $file = $this->addFiles($profile, [$file], $fileType);
                        
                    }elseif(is_array($file))
                    {
                        $file = $this->addFiles($profile, $file, $fileType);
                        
                    }
                }
            }
        }

        //Временный костыль для менеджеров (внутрянка)
        if(auth()->user()->hasRole('site_manager'))
        {
            if(!$profile->users()->where('users.id', auth()->user()->id)->exists())
            {
                $profile->users()->attach(auth()->user()->id);
            }

            if(!$profile->users()->where('users.id', $user->id)->exists())
            {
                $profile->users()->attach($user->id);
            }
        }
        
        return redirect()->route('cabinet.profile.show', $profile->id);
    }
    
    /**
     * Display the specified resource.
     *
     * @param   string $profileId
     * @return \Illuminate\Http\Response
     */
    public function show(string $profileId)
    {
        $appLocalisationId = Localisation::where('code', app('translator')->getLocale())->first()->id;

        $profile = Profile::getProfileForUser( (int)$profileId, (int)auth()->user()->id, $appLocalisationId );

        $profile->setRelation('profileProperties', $profile->profileProperties->keyBy('code'));
        //dd($profile);
        $type = $profile->profileTypes->code;
        $localisation = Localisation::where('id', $profile->profileProperties()->first()->localisation_id)->pluck('code')->first();

        return view("cabinet.profile.show.{$type}", compact('profile', 'localisation'));
    }
    
    /**
     * Display the specified resource.
     *
     * @param   string $profileId
     * @return \Illuminate\Http\Response
     */
    public function showDocuments(string $profileId)
    {
        $profile = Profile::where('id', $profileId)->first();
        
        // Проверяем принадлежность пользователю профиля
        if ( !$profile->users()->find(auth()->user()->id) ) 
        { 
            abort(403);
        } 
        
        $profile = $profile->load(['downloads' => function ($query) {
            $query
                ->where('type', 'profile_documents');
            }
        ]);
            
        return view('cabinet.profile.show.documents', compact('profile'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Profile  $profile
     * @return \Illuminate\Http\Response
     */
    public function edit(string $profileId)
    {
        $appLocalisationId = Localisation::where('code', app('translator')->getLocale())->first()->id;

        $profile = Profile::getProfileForUser( (int)$profileId, (int)auth()->user()->id, $appLocalisationId );
        $type = $profile->profileTypes->code;
        
        if (!$profile || !$type) 
        {
            abort(403);
        } 
        
        $localisation = Localisation::where('id', $profile->profileProperties()->first()->localisation_id)->pluck('code')->first();
        $countries = ApplicationDictionary::getDataForSelect(Localisation::where('code', app('translator')->getLocale())->first()->id, 'countries');
        $profilePropertiesArray = $profile
            ->profileProperties
            ->mapWithKeys(function (&$item){
                return [$item['code'] => $item['value']];
            });
            
        $profile->profileProperties = $profilePropertiesArray;
        //dd($profile);
        return view("cabinet.profile.edit.{$type}", compact('profile', 'localisation', 'type', 'countries'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Profile  $profile
     * @return \Illuminate\Http\Response
     */
    public function update(EditRequest $request, Profile $profile)
    {
        // Проверяем принадлежность пользователю профиля
        if ( !$profile->users()->find(auth()->user()->id) ) 
        { 
            abort(403);
        } 
    
        // Если он хочет удалить обязательное фото для главного баннера магазина и ничего не добавил взамен - хрена лысого ему
        if($request->has('delete_files') && !array_diff($profile->downloads->where('type', 'company_main_banner_images')->pluck('id')->toArray(), $request->delete_files) && !$request->has('company_main_banner_images'))
        {
            return redirect()->back()->withInput()->withErrors(['company_main_banner_images' => __('validation.required', ['attribute' => __('validation.attributes.company_main_banner_images')] ) ]); 
        } 
        
        $deletedFiles = $request->filled('deleted_files') ? json_decode($request->deleted_files, true) : null;
        
        if($deletedFiles)
        {
            $profileFiles = $this->filtrateInputFiles($request->allFiles(), $deletedFiles);
            
        }else
        {
            $profileFiles = $request->allFiles();
        }
        
        // Удаляем файлы
        if($request->has('delete_files'))
        {
            $countDeletedFiles = $this->destroyFiles($profile,  $request->delete_files);
        }
        
        // Обновляем профиль пользователя
        $profile = $this->service->update($profile, $request->except( array_merge(array_keys($profileFiles), ['_token', '_method', 'profile_type', 'delete_files', 'deleted_files']) ));
        
        // Добавляем файлы
        if($profileFiles)
        {
            foreach ($profileFiles as $fileType => & $file) 
            {
                if( in_array($fileType, $this->service::ALLOWED_FILES) )
                {
                    if(is_object($file)) // Прилетело с инпут без аттрибута multiple. Перезаписываем.
                    {
                        $this->destroyFiles($profile, $profile->downloads->where('type', $fileType)->pluck('id')->toArray() );
                        $file = $this->addFiles($profile, [$file], $fileType);
                        
                    }elseif(is_array($file))
                    {
                        $file = $this->addFiles($profile, $file, $fileType);
                    }
                }
            }
        }
        
        return redirect()->route('cabinet.profile.show', $profile->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Profile  $profile
     * @return \Illuminate\Http\Response
     */
    public function destroy(Profile $profile)
    {
        $user = auth()->user();

        // Проверяем принадлежность пользователю профиля
        if ( !$profile->users()->find($user->id) && !$user->hasRole('site_manager')) 
        { 
            abort(403);
        } 

        $result = $this->service->destroy ($profile, $user);
        
        if($result)
        {
            return redirect()->back()->withCookie( \Cookie::make('active_profile', $profile->id, -1) );;
        }
    }
    
    /**
     * Переводим на страницу активации профиля пользователя.
     *
     * @param  \App\Profile  $profile
     * @return \Illuminate\Http\Response
     */
    public function approve(Profile $profile)
    {
        $appLocalisationId = Localisation::where('code', app('translator')->getLocale())->first()->id;

        $user = auth()->user();
        
        $profile = Profile::getProfileForUser( (int)$profile->id, (int)$user->id, $appLocalisationId );
        
        if (!$profile) 
        {
            abort(403);
        }

        if(auth()->user()->hasRole('site_manager'))
        {
            $profile->status = 'active';
            $profile->save();
            
            return redirect()->back();
        }
        
        return view('cabinet.profile.approve', compact('user', 'profile')); 
    }
    
    /**
     * Выполняем действия для дальнейшей активации профиля пользователя.
     *
     * @param  \App\Profile  $profile
     * @return \Illuminate\Http\Response
     */
    public function approval(Profile $profile, ApproveRequest $request)
    {
        $user = auth()->user();
         
        if ( !$profile->users()->find($user->id) ) 
        {
            abort(403);
        } 
        
        // Регистрируем документы пользователя и сохраняем себе на диск
        if($request->has('documents'))
        {
            $this->addFiles($profile,  $request->documents, 'profile_documents');
        } 
    
        $result = $this->service->approval($profile, $user, $profile->downloads()->where('type', 'profile_documents')->get());
        
        if($result)
        {
            $type = ProfileType::where('id', $profile->profile_type_id)->pluck('code')->first();
            
            return redirect()->route('cabinet.profile.index', ['type' => $type]);
        }
    }
    
    public function selectActiveProfile(Profile $profile) 
    {
        // Проверяем принадлежность пользователю профиля
        if ( !$profile->users()->find(auth()->user()->id) ) 
        { 
            abort(403);
        } 
        
        $response = redirect()->back();
        
        if($profile->status === 'active')
        {  
            $profile->load([
                'profileTypes', 
                'profileProperties' => function ($query){
                    $query
                        ->with('localisations');                        
                }
            ]);
            
            \Session::put('active_profile', $profile);
            $response = $response->withCookie( \Cookie::forever('active_profile', $profile->id) );
        }
        
        return $response;
    }
}
