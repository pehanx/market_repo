<?php

namespace App\Http\Controllers\Cabinet;

use App\Models\{Profile, Localisation, Manager, ProfileType};
use App\{User, ApplicationDictionary};
use App\Helpers\Profile\ProfileService;
use App\Helpers\Account\AccountService;
use Illuminate\Http\Request;
use Illuminate\Contracts\Mail\Mailer;// Мб в будущем будем использовать другой метод отправки уведомлений
use App\Mails\InviteUserMail;
use App\Http\Requests\Cabinet\Manager\{CreateRequest, EditEntranceRequest, EditRequest};
use App\Http\Controllers\Controller;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Mail;

class CompanyController extends Controller
{
    private $profileService;
    private $accountService;
    private $mailer;

    public function __construct(ProfileService $profileService, AccountService $accountService, Mailer $mailer)
    {
        $this->profileService = $profileService;
        $this->accountService = $accountService;
        $this->mailer = $mailer;

        $this->middleware('role:site_manager');
    }
    
    public function create(Request $request)
    {
        return view('cabinet.manager.company_create');
    }

    public function store(Request $request)
    {
        $email = (string) $request->email;

        if(!User::where('email', $email)->exists())
        {
            $user = User::create([
                'name' => $request->name,
                'email' => $email,
                'password' => bcrypt(Str::random(8)),
                'invite_token' => Str::random(20),
                'registration_type' => 'manager',
            ]);

            $user->profiles()->create([
                'profile_type_id' => ProfileType::where('code', 'account')->first()->id,
                'status' => 'unactive'
            ]);


            $user->managers()->attach( auth()->user()->id );
        }

        return redirect()->to( route('cabinet.manager.companies', ['user' => auth()->user()]) );
    }
    
    /* 
    * Все профили у созданной компании
    */
    public function indexAll(User $user) 
    {
        if((int)auth()->user()->id !== (int)$user->id)
        {
            $companies = Profile::getCompaniesForManager($user, 10);

            $selectedCompany = $user->id;

            return view('cabinet.manager.companies_all', compact('companies', 'selectedCompany') );
        }
        
        abort(403);
    }

    public function approval(Profile $profile)
    {
        $profile->status = "active";
        $profile->update();

        $user = $profile->users->where('id', '<>', auth()->user()->id)->first();

        if($user !== null)
        {
            return redirect()->to( route('cabinet.company.list', $user->id) );
        }

        return redirect()->to( route('cabinet.manager.companies', auth()->user()->id) );
    }

    public function companyAll(User $user) 
    {   
        if((int)auth()->user()->id === (int)$user->id)
        {
            $users = Manager::find(auth()->user()->id)->companies()->with([
                'profiles' => function($query){
                    $query->with('downloads')->where('profile_type_id', 1)->orWhere('profile_type_id', 2);
                },
                'downloads'
            ])->get();

            return view('cabinet.manager.companies_users', compact('users') );
        }
        
        abort(403);
    }

    public function invite(Request $request) 
    {   
        if($request->has('user') && User::find($request->user) !== null)
        {
            $user = User::find($request->user);

            if($user->invite_token === null)
            {
                $user->invite_token = Str::random(20);
                $user->save();
            }

            $result = $this->mailer->to($user->email)->send(new InviteUserMail($user));
        }
    }

    public function createProfile(Request $request)
    {
        $type = $request->route('type');

        $user = User::findOrFail($request->route('company'));

        $localisations = Localisation::getPossibleLocales($user, $type);
       
        if( !in_array($type, $this->profileService::ALLOWED_TYPES) || !$localisations)
        {
            abort(403);
        } 
        
        $localisationId = Localisation::where('code', app('translator')->getLocale())->first()->id;
        $countries = ApplicationDictionary::getDataForSelect($localisationId, 'countries');
        $account = Profile::getAccountForUser((int)$user->id)->first();
        
        if($account)
        {
            $account = $account
                ->profileProperties
                ->mapWithKeys(function ($item) {
                    return [$item['code'] => $item['value']];
                });
        }
  
        return view(
            "cabinet.profile.create.{$type}", 
            compact(
                'user',
                'account',
                'localisations',
                'type',
                'countries'
            )
        );
    }

    public function edit(string $userId)
    {
        $user = User::getUserWithAvatar((int)$userId);
        $account = Profile::getAccountForUser((int)$userId)->first();
        $locale = app('translator')->getLocale();
        $locale = ($locale === 'en') ? 'en-GB' : $locale;
        
        if($account)
        {
            $account = $account
                ->profileProperties
                ->mapWithKeys(function ($item) {
                    return [$item['code'] => $item['value']];
                });
        }
        
        return view('cabinet.manager.company_edit', compact('account', 'user', 'locale'));
    }
    
    public function update(EditRequest $request, string $userId)
    { 
        $user = User::findOrFail((int)$userId);
            
        $deletedFiles = $request->filled('deleted_files') ? json_decode($request->deleted_files, true) : null;
    
        if($deletedFiles)
        {
            $userFiles = $this->filtrateInputFiles($request->allFiles(), $deletedFiles);
            
        }else
        {
            $userFiles = $request->allFiles();
        }
        
        if($request->has('delete_files'))
        {
            $countDeletedFiles = $this->softDeleteFiles($user, $request->delete_files);
        }

        // Обновляем информацию об аккаунте пользователя
        $account = $this->accountService->update($user, $request->except(['avatar', '_token', '_method', 'deleted_files', 'delete_files']));
        
        if($userFiles)
        {
            foreach ($userFiles as $fileType => & $file) 
            {
                if( in_array($fileType, $this->accountService::ALLOWED_FILES) )
                {
                    if(is_object($file)) // Прилетело с инпут без аттрибута multiple. Перезаписываем.
                    {
                        $this->destroyFiles($user, $user->downloads->where('type', $fileType)->pluck('id')->toArray() );
                        $file = $this->addFiles($user, [$file], $fileType);
                        
                    }elseif(is_array($file))
                    {
                        $file = $this->addFiles($user, $file, $fileType);
                    }
                }
            }
        }
        
        return redirect()->route('cabinet.company.edit', $user->id);
    }
}
