<?php

namespace App\Http\Controllers\Cabinet;

use App\Http\Requests\Cabinet\Import\{ImportRequest,ImportSettingsRequest};
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Http\File;
use App\Jobs\ImportRequestProductJob;
use App\Jobs\ImportProductJob;
use App\Helpers\Product\ImportProductService;
use App\Helpers\RequestProduct\ImportRequestProductService;

use App\Models\{Localisation, Profile, Import};

class ImportController extends Controller
{
    public function __construct( ImportProductService $productService, ImportRequestProductService $requestProductService)
    {
        $this->productService = $productService;
        $this->requestProductService = $requestProductService;
    }

    /**
     * Открытие вьюшки index
     */
    public function index(Profile $profile)
    {
        // Проверяем принадлежность пользователю профиля
        if ( !$profile->users()->find( auth()->user()->id ) || (int)\Session::get('active_profile')->id !== (int)$profile->id)
        {
            abort(403);
        }

        $profile->load('profileTypes'); 

        $profile->import_type = ($profile->profileTypes->code === 'buyer') ? 'request_product':
            (($profile->profileTypes->code === 'seller') ? 'product' : '');
        
        $imports = Import::getDataForProfile($profile);

        return view('cabinet.import.index', compact('imports', 'profile'));
    }

    /**
     * Открытие вьюшки создания импорта
     */
    public function create(Profile $profile, Request $request)
    {
        // Проверяем принадлежность пользователю профиля
        if ( !$profile->users()->find( auth()->user()->id ) || (int)\Session::get('active_profile')->id !== (int)$profile->id)
        {
            abort(403);
        }

        $allowedImportedEntity = \Session::get('active_profile')->profileTypes->code === 'seller' ? 'product' :
            (\Session::get('active_profile')->profileTypes->code === 'buyer' ? 'request_product' : '');
            
        $profile->import_type = $allowedImportedEntity;
        
        if ($request->route('type') && in_array($request->route('type'), Import::IMPORTED_ENTITY) && $allowedImportedEntity === $request->route('type'))
        {
            return view('cabinet.import.create', compact('profile'));

        } else
        {
            abort(403);
        }
    }

    /**
     * Отображение заголовков таблицы,
     * необходимо для второго этапа настроек импорта
     * дёргается по json
     * Вынести в апи
     */
    public function getRequiredHeaders()
    {
        return $this->requestProductService->requiredHeaders();
    }

    /**
     * Открытие файлов импорта,
     * обработка файлов из архива, заголовков таблицы
     * отображение вьюшки с сопоставлением колонок
     */
    public function importSettings(Profile $profile, string $type, int $importId)
    {
        // Проверяем принадлежность пользователю профиля
        if ( !$profile->users()->find( auth()->user()->id ) || (int)\Session::get('active_profile')->id !== (int)$profile->id)
        {
            abort(403);
        }

        $checkImportId = Import::findIdForProfile($profile, (int)$importId);

        if($checkImportId === null)
        {
            return redirect()->route('cabinet.import.create',['profile'=>$profile->id,'type'=>$type])
                ->withErrors(['invalid_id' => __('cabinet.import.invalid_id') ]);
        }

        // Проверяем принадлежность импорта профилю
        if($checkImportId === null)
        {
            abort(403);
        }

        if($type === 'request_product')
        {
            
            $settingsArray = $this->requestProductService->settingsForRequestProducts($profile, (int)$importId);

            return view('cabinet.import.request_product',
                [
                'quantities' => $settingsArray['quantities'],
                'profile' => $profile,
                'allowedColumns' => $settingsArray['allowedColumns'],
                'tableHeadings' => $settingsArray['tableHeadings'],
                'importId' => (int)$importId,
                ]
            );

            
        } elseif($type === 'product')
        {
            $settingsArray = $this->productService->settingsForProducts($profile, (int)$importId);

            return view('cabinet.import.product',
                [
                'quantities' => $settingsArray['quantities'],
                'profile' => $profile,
                'allowedColumns' => $settingsArray['allowedColumns'],
                'tableHeadings' => $settingsArray['tableHeadings'],
                'importId' => (int)$importId,
                ]
            );

        } else
        {
            return redirect()->back()->withInput()
                ->withErrors(['invalid_type' => __('cabinet.import.invalid_type') ]);
        }

    }

    /**
     * Запуск джобика для импорта запросов
     * метод запускается после сопоставления колонок
     */
    public function requestProductStore(ImportSettingsRequest $request, Profile $profile)
    {
        // Проверяем принадлежность пользователю профиля и импорта
        if ( !$profile->users()->find( auth()->user()->id ) 
            || (int)\Session::get('active_profile')->id !== (int)$profile->id
            || !$profile->imports()->find((int) $request->import_id)
            )
        {
            abort(403);
        }

        //Проверка на то, проставлены ли обязательные колонки для импорта
        foreach ($this->requestProductService->requiredHeaders() as $key => $value)
        {
            if ( !array_column($request->import_heading, $key) )
            {
                $profile->imports()
                    ->find((int) $request->import_id)
                    ->delete();

                return redirect()->back()
                    ->withErrors(['few_columns' => __('cabinet.import.few_required_columns') ]);
                
            }
        }

        if( count($request->import_heading) >= count($this->requestProductService->requiredHeaders()))
        {
            $headersWithCols = $this->getTableHeaders($request->import_heading);
            $import = Import::find($request->import_id);
            $importTable = Import::findImportTable($profile, $import);

            $settingsArray = [
                'headings' => $headersWithCols,
                'importId' => $request->import_id,
                'tableName' => $importTable,
            ];

            ImportRequestProductJob::dispatch($settingsArray, $profile->id)->onQueue('application');
        }

        return redirect()->route('cabinet.import.index',['profile' => $profile->id]);
    }

    /**
     * Запуск джобика для импорта запросов
     * метод запускается после сопоставления колонок
     */
    public function productStore(ImportSettingsRequest $request, Profile $profile)
    {
        // Проверяем принадлежность пользователю профиля и импорта
        if ( !$profile->users()->find( auth()->user()->id ) 
            || (int)\Session::get('active_profile')->id !== (int)$profile->id
            || !$profile->imports()->find((int) $request->import_id)
            )
        {
            abort(403);
        }

        //Проверка на то, проставлены ли обязательные колонки для импорта
        foreach ($this->productService->requiredHeaders() as $key => $value)
        {
            if ( !array_column($request->import_heading, $key) )
            {
                $profile->imports()
                    ->find((int) $request->import_id)
                    ->delete();

                return redirect()->back()
                    ->withErrors(['few_columns' => __('cabinet.import.few_required_columns') ]);
                
            }
        }

        if( count($request->import_heading) >= count($this->requestProductService->requiredHeaders()))
        {
            $headersWithCols = $this->getTableHeaders($request->import_heading);
            $import = Import::find($request->import_id);
            $importTable = Import::findImportTable($profile, $import);

            $settingsArray = [
                'headings' => $headersWithCols,
                'importId' => $request->import_id,
                'tableName' => $importTable,
            ];

            ImportProductJob::dispatch($settingsArray, $profile->id)->onQueue('application');
        }

        return redirect()->route('cabinet.import.index',['profile' => $profile->id]);
    }

    /**
     * Сборка корректного массива
     */
    private function getTableHeaders(array $headings) :array
    {
        $headersWithCols = [];

        foreach ($headings as $innerArray)
        {
            $headersWithCols[array_key_first($innerArray)] = str_replace('__' , '', strstr( array_shift($innerArray), '__'));
        }

        return $headersWithCols;
    }
    
    /**
     * Получение таблицы с инструкциями по импорту
     */
    public function getExampleFile(Profile $profile, $type)
    {
        // Проверяем принадлежность пользователю профиля
        if ( !$profile->users()->find( auth()->user()->id ) || (int)\Session::get('active_profile')->id !== (int)$profile->id)
        {
            abort(403);
        }
        
        $langCode = app('translator')->getLocale();

        if ( $type === 'request_product')
        {
            $requestExample = \Storage::disk( env('FILESYSTEM_DRIVER') )
                ->path($this->productService::IMPORT_PATH . "$langCode/request_product.xls");

            if(is_file($requestExample))
            {
                return response()->download($requestExample);

            } else
            {
                return redirect()->back();
            }
            
        } else 
        {
            $productExample = \Storage::disk( env('FILESYSTEM_DRIVER') )
                ->path($this->productService::IMPORT_PATH."$langCode/product.xls");

            if(is_file($productExample))
            {
                return response()->download($productExample);

            } else
            {
                return redirect()->back();
            }
        }
    }

    /* 
     * Получение таблицы со списком значений для импорта
     */
    public function getExampleLists(Profile $profile, string $type)
    {
        // Проверяем принадлежность пользователю профиля
        if ( !$profile->users()->find( auth()->user()->id ) || (int)\Session::get('active_profile')->id !== (int)$profile->id)
        {
            abort(403);
        }

        if($type === 'request_product')
        {
            return $this->requestProductService->getExampleCats(); //Для запросов отрисовывается таблица только с категориями

        } elseif($type === 'product')
        {
            return $this->productService->getExampleLists(); //Для товаров, с цветами, категориями и странами
        }
        
        return redirect()->back();
    }
}
