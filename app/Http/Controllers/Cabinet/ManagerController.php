<?php

namespace App\Http\Controllers\Cabinet;

use App\Models\Profile;
use App\User;
use App\Helpers\Manager\ManagerService;
use Illuminate\Http\Request;
use App\Http\Requests\Cabinet\Manager\{CreateRequest, EditEntranceRequest, EditRequest};
use App\Http\Controllers\Controller;

class ManagerController extends Controller
{
    private $service;

    public function __construct(ManagerService $service)
    {
        $this->service = $service;

        $this->middleware('permission:list manager');
        $this->middleware('permission:create manager', ['only' => ['create','store']]);
        $this->middleware('permission:edit manager', ['only' => ['edit','update', 'editEntrance', 'updateEntrance']]);
        $this->middleware('permission:delete manager', ['only' => ['destroy']]);
    }
    
    public function create(Profile $profile)
    {
        if($this->service->checkUser(auth()->user(), $profile))
        {
            $companyName = ($profile->profileProperties->where('code', 'company_name')->first()) ? $profile->profileProperties->where('code', 'company_name')->first()->value : '';
            return view('cabinet.manager.create', compact('companyName', 'profile') );
        }
        
        abort(403);
    }

    public function store(CreateRequest $request, Profile $profile)
    {
        if($this->service->checkUser(auth()->user(), $profile) && $request['password'] === $request['password_confirmation'])
        {
            $manager = $this->service->create($profile, $request->except(['_token', 'password_confirmation']));
            return redirect()->route('cabinet.manager.edit', ['profile' => $profile->id, 'manager' => $manager->id]);
        }
        
        abort(403);
    }

    public function index(Profile $profile) 
    {
        if($this->service->checkUser(auth()->user(), $profile))
        {
            $managers = User::getManagersForProfile($profile, 10);
            
            $profile->load(['profileProperties']);
            
            return view('cabinet.manager.index', compact('managers', 'profile') );
        }
        
        abort(403);
    }
    
    public function indexAll(User $user) 
    {
        if((int)auth()->user()->id === (int)$user->id)
        {
            $profiles = Profile::getProfilesWithManagersForUser($user, 1);

            return view('cabinet.manager.index_all', compact('profiles') );
        }
        
        abort(403);
    }

    public function companyAll(User $user) 
    {
        if((int)auth()->user()->id === (int)$user->id)
        {
            $companies = Profile::getCompaniesForUser($user, 1);

            return view('cabinet.manager.companies_all', compact('companies') );
        }
        
        abort(403);
    }
    
    public function edit(Profile $profile, Profile $manager)
    {
        if($this->service->checkUser(auth()->user(), $profile))
        {
            $companyName = ($profile->profileProperties->where('code', 'company_name')->first()) ? $profile->profileProperties->where('code', 'company_name')->first()->value : '';
                    
            $manager = Profile::getAccountForUser( $manager->users->pluck('id')->shift() )->with('users')->first();
            
            return view('cabinet.manager.edit', compact('profile', 'manager', 'companyName') );
        }
        
        abort(403);
    }
    
    public function update(EditRequest $request, Profile $profile, Profile $manager)
    {
        if($this->service->checkUser(auth()->user(), $profile))
        {
            // Обновляем информацию о менеджере профиля(его аккаунт)
            $this->service->update($manager, $request->except(['_token', '_method']));
            return redirect()->route('cabinet.manager.edit', ['profile' => $profile, 'manager' => $manager]);
        }
        
        abort(403);
    }
    
    public function editEntrance(Profile $profile, Profile $manager)
    {
        if($this->service->checkUser(auth()->user(), $profile))
        {
            $companyName = ($profile->profileProperties->where('code', 'company_name')->first()) ? $profile->profileProperties->where('code', 'company_name')->first()->value : '';
                    
            $manager = Profile::getAccountForUser( $manager->users->pluck('id')->shift() )->with('users')->first();
            
            return view('cabinet.manager.edit_entrance', compact('profile', 'manager', 'companyName') );
        }
        
        abort(403);
    }
    
    public function updateEntrance(EditEntranceRequest $request, Profile $profile, Profile $manager)
    { 
        if($this->service->checkUser(auth()->user(), $profile))
        {
            $dbUser = User::where('email', $request->email)->first();
            $user = $manager->users->first();
            
            // Проверка на уникальность email
            if( ($dbUser && (int)$dbUser->id === (int)$user->id) || !$dbUser)
            {
                $this->service->updateUser($user, $request->except(['_token', '_method']));
                return redirect()->route('cabinet.manager.edit_entrance', ['profile' => $profile, 'manager' => $manager]);
                
            }else
            {
                return redirect()->back()->withInput()->withErrors(['email' => __('validation.unique', ['attribute' => 'email']) ]); 
            }
        }
        
        abort(403);
    }
    
    public function destroy(Profile $profile, Profile $manager)
    {
        if($this->service->checkUser(auth()->user(), $profile))
        {
            $result = $this->service->destroy($manager);
        
            if($result)
            {
                return redirect()->back();
            }
        }
        
        abort(403);
    }
}
