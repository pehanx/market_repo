<?php

namespace App\Http\Controllers\Cabinet;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Jobs\ExportProductJob;
use App\Jobs\ExportRequestProductJob;
use App\Helpers\Product\ExportProductService;
use App\Helpers\RequestProduct\ExportRequestProductService;
use App\Models\{Localisation, Profile, ProfileType, Product, RequestProduct, Export};
use App\Http\Requests\Cabinet\Export\{ExportRequestPage, ExportRequestAll};

class ExportController extends Controller
{
    public function __construct(ExportProductService $expProdService, ExportRequestProductService $expRqtProdService)
    {
        $this->expProdService = $expProdService;
        $this->expRqtProdService = $expRqtProdService;
    }

    public function index(Profile $profile) // ок
    {
        if ( !$profile->users()->find( auth()->user()->id ) || (int)\Session::get('active_profile')->id !== (int)$profile->id) 
        { 
            abort(403);
        } 
       
        $profile->load('profileTypes'); 

        $profile->export_type = ($profile->profileTypes->code === 'buyer') ? 'request_product':
            (($profile->profileTypes->code === 'seller') ? 'product' : '');
            
        $exports = Export::getDataForProfile($profile, [$profile->export_type]);

        return view('cabinet.export.index', compact('exports', 'profile'));
    }

    /**
     * В зависимости от типа экспорта рендерит разные виды
     */ 
    public function create(Request $request, Profile $profile) // ok
    {
        if ( !$profile->users()->find( auth()->user()->id ) || (int)\Session::get('active_profile')->id !== (int)$profile->id) 
        { 
            abort(403);
        } 
        
        $allowedExportedEntity = \Session::get('active_profile')->profileTypes->code === 'seller' ? 'product' :
            (\Session::get('active_profile')->profileTypes->code === 'buyer' ? 'request_product' : '');
            
        $profile->export_type = $allowedExportedEntity;

        if($request->route('type') && in_array($request->route('type'), Export::EXPORTED_ENTITY) && $allowedExportedEntity === $request->route('type'))
        {
            $allowedFormats = Export::EXPORTED_FORMATS;
            return view('cabinet.export.create.' . $request->route('type'), compact('profile', 'allowedFormats'));
            
        }else
        {
            abort(403);
        }
    }
    
    /**
     * Экспорт всех элементов с заданными параметрами
     */ 
    public function storeAll(ExportRequestAll $request, Profile $profile) // ок
    {   
        if ( !$profile->users()->find( auth()->user()->id ) || (int)\Session::get('active_profile')->id !== (int)$profile->id) 
        { 
            abort(403);
        }
        
        $allowedExportedEntity = \Session::get('active_profile')->profileTypes->code === 'seller' ? 'product' :
            (\Session::get('active_profile')->profileTypes->code === 'buyer' ? 'request_product' : '');

        if(in_array($request->export_format, Export::EXPORTED_FORMATS) && $allowedExportedEntity === $request->type)
        {
            if($allowedExportedEntity === 'product')
            {
                $this->dispatchExportProductJob($request, $profile);
            }
            
            if($allowedExportedEntity === 'request_product')
            {
                $this->dispatchExportRequestProductJob($request, $profile);
            }
            
            return redirect()->route('cabinet.export.index', $profile->id);
            
        }else 
        {
            abort(403);
        }
    }
    
    /**
     * Вспомогательный метод. Запускает job с необходимыми параметрами
     */ 
    private function dispatchExportProductJob(ExportRequestAll $request, Profile $profile) // ок
    {   
        $export = $profile->exports()->create([
           'progress' => 0
        ]);

        $exportParams = [
            'export_format' => $request->export_format,
            'with_thumbnails' => $request->with_thumbnails,
            'with_models' => $request->with_models,
            'status' => $request->status,
        ];
        
        ExportProductJob::dispatch($exportParams, $profile->id, $export->id);
    }

    /**
     * Вспомогательный метод. Запускает job с необходимыми параметрами
     */  
    private function dispatchExportRequestProductJob(ExportRequestAll $request, Profile $profile) // Сам job и сервис не проверен
    { 
        $export = $profile->exports()->create([
           'progress' => 0
        ]);

        $exportParams = [
            'export_format' => $request->export_format,
            'with_thumbnails' => $request->with_thumbnails,
            'status' => $request->status,
        ];

        ExportRequestProductJob::dispatch($exportParams, $profile->id, $export->id);
    }

    /**
     * Экспорт элементов с текущей страницы с заданными параметрами
     */ 
    public function storePage(ExportRequestPage $request, Profile $profile)
    {
        if ( !$profile->users()->find( auth()->user()->id ) || (int)\Session::get('active_profile')->id !== (int)$profile->id) 
        { 
            abort(403);
        }

        $result = null;
        
        $allowedExportedEntity = \Session::get('active_profile')->profileTypes->code === 'seller' ? 'product' :
            (\Session::get('active_profile')->profileTypes->code === 'buyer' ? 'request_product' : '');
       
        if(in_array($request->export_format, Export::EXPORTED_FORMATS) && $allowedExportedEntity === $request->type && $request->filled('elements_id'))
        {
            $exportParams = array_merge($request->all(), ['status' => $request->status]);
            unset($exportParams['_token']);
            
            if($allowedExportedEntity === 'product')
            {
                $result = $this->expProdService->exportPage($exportParams, $profile);
            }
            
            if($allowedExportedEntity === 'request_product')
            {
                $result = $this->expRqtProdService->exportPage($exportParams, $profile); // Сервис не проверен
            } 
            
        }else 
        {
            abort(403);
        }    
        
        return $result;

    }
}