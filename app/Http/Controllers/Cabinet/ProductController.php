<?php

namespace App\Http\Controllers\Cabinet;

use App\Models\{Profile, Category, Product, Localisation, ProductModel};
use App\Helpers\Product\ProductService;
use App\{User, ApplicationDictionary};
use App\Http\Controllers\Controller;
use App\Http\Requests\Cabinet\Product\{CreateRequest, CreateModelRequest, FilterRequest, EditRequest, EditModelRequest, ActionRequest};
use Illuminate\Http\Request;

class ProductController extends Controller
{
    private $service;

    public function __construct(ProductService $service)
    {
        $this->service = $service;

        $this->middleware('check_active_profile:seller');// Проверить что у пользователя выбран профиль продавца и он висит в сессии
        $this->middleware('permission:list product', ['only' => ['index', 'indexInactive', 'indexDeleted', 'show']]);
        $this->middleware('permission:create product', ['only' => ['chooseCategory', 'fillProperties', 'store', 'storeModel', 'makeAction']]);
        $this->middleware('permission:edit product', ['only' =>['edit', 'editModel', 'update', 'updateModel', 'disable', 'activate', 'makeAction']]);
        $this->middleware('permission:delete product', ['only' =>['delete', 'destroy', 'deleteModel', 'destroyModel', 'makeAction']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Profile $profile, FilterRequest $request)
    {
        // Проверяем принадлежность пользователю профиля
        if ( !$profile->users()->find( auth()->user()->id ))
        {
            abort(403);
        }

        $products = collect();
        $localisationId = $profile->profileProperties()->first()->localisation_id;
        $appLocalisationId = Localisation::where('code', app('translator')->getLocale())->first()->id;

        if($profile->products()->where('is_active', 'true')->first())
        {
            $products = Product::getFiltrateProducts($profile->products()->where('is_active', 'true'), $request->except(['full_filter']))->paginate(10);
            // Записали в пагинацию GET параметры фильтра
            $products->appends($request->input());
            //dump($profile->products()->where('is_active', 'true')->count());exit();
            Product::getAllProductsWithModels(collect($products->items()), (int)$localisationId, (int)$appLocalisationId);
        }

        //$exportFormats = Export::EXPORTED_FORMATS;
        
        $categories = Category::getDataForSelect($appLocalisationId, 'name');

        return view('cabinet.product.index.active', compact('profile', 'products', 'categories'));
    }

    public function indexInactive(Profile $profile, FilterRequest $request)
    {
        // Проверяем принадлежность пользователю профиля
        if ( !$profile->users()->find( auth()->user()->id ))
        {
            abort(403);
        }

        $products = collect();
        $localisationId = $profile->profileProperties()->first()->localisation_id;
        $appLocalisationId = Localisation::where('code', app('translator')->getLocale())->first()->id;

        if($profile->products()->where('is_active', 'false')->first())
        {
            $products = Product::getFiltrateProducts($profile->products()->where('is_active', 'false'), $request->except(['full_filter']))->paginate(10);

            // Записали в пагинацию GET параметры фильтра
            $products->appends($request->input());

            Product::getAllProductsWithModels(collect($products->items()), (int)$localisationId, (int)$appLocalisationId);
        }

        //$exportFormats = Export::EXPORTED_FORMATS;
        
        $categories = Category::getDataForSelect($appLocalisationId, 'name');

        return view('cabinet.product.index.inactive', compact('profile', 'products', 'categories'));
    }

    public function indexDeleted(Profile $profile, FilterRequest $request)
    {
        // Проверяем принадлежность пользователю профиля
        if ( !$profile->users()->find( auth()->user()->id ))
        {
            abort(403);
        }

        $products = collect();
        $localisationId = $profile->profileProperties()->first()->localisation_id;
        $appLocalisationId = Localisation::where('code', app('translator')->getLocale())->first()->id;
        $productHasTrashedModels = Product::getProductHasModels($profile->products(), 'trashed');
        
        if($profile->products()->onlyTrashed()->first() || $productHasTrashedModels->first())
        {
            $products = Product::getFiltrateProducts($profile->products()->onlyTrashed()->whereNull('destroyed_at'), $request->except(['full_filter']) );

            $products = $products->union( Product::getFiltrateProducts($productHasTrashedModels, $request->except(['full_filter']) ) )->paginate(10);
            //dump($products->toSql(), $products->getBindings());exit();

            // Записали в пагинацию GET параметры фильтра
            $products->appends($request->input());

            Product::getAllProductsWithModels(collect($products->items()), (int)$localisationId, (int)$appLocalisationId, true);
        }

        //$exportFormats = Export::EXPORTED_FORMATS;
        
        $categories = Category::getDataForSelect($appLocalisationId, 'name');

        return view('cabinet.product.index.deleted', compact('profile', 'products', 'categories'));
    }

    public function show(Profile $profile, Product $product, Request $request)
    {
        // Проверяем принадлежность пользователю профиля
        if ( !$profile->users()->find( auth()->user()->id ) || !$profile->products()->find($product->id))
        {
            abort(403);
        }

        if($request->filled('steps'))
        {
            $steps = true;
        }else
        {
            $steps = false;
        }

        $localisationId = $profile->profileProperties()->first()->localisation_id;
        $appLocalisationId = Localisation::where('code', app('translator')->getLocale())->first()->id;

        $product = Product::getProductWithModels($product, (int)$localisationId,  $appLocalisationId);
        $product->setRelation('downloads', $product->downloads->groupBy('type'));
        
        $category = Category::findOrFail($product->category_id)
            ->load(['categoryProperties' => function ($query) use($appLocalisationId){
                $query
                    ->where('code', 'name')
                    ->where('localisation_id', $appLocalisationId);
            }]);
            
        //dump($product);exit();
        return view('cabinet.product.show', compact('profile', 'product', 'category', 'steps'));
    }

    public function chooseCategory(Profile $profile)
    {
        // Проверяем принадлежность пользователю профиля
        if ( !$profile->users()->find( auth()->user()->id ) )
        {
            abort(403);
        }

        $appLocalisationId = Localisation::where('code', app('translator')->getLocale())->first()->id;
        
        $categories = \Cache::tags(Category::class)->rememberForever('full_tree_' . $appLocalisationId, function() use($appLocalisationId) {
            return Category::getFullTree($appLocalisationId)->first()->children->toArray();
        });
        
        return view('cabinet.product.create.choose_category', compact('profile', 'categories'));
    }

    public function fillProperties(Profile $profile, $categoryId)
    {
        // Проверяем принадлежность пользователю профиля
        if ( !$profile->users()->find( auth()->user()->id ) )
        {
            abort(403);
        }
        
        $appLocalisationId = Localisation::where('code', app('translator')->getLocale())->first()->id;

        // Категория, выбранная на предыдущем шаге пользователем
        $category = Category::findOrFail($categoryId)
            ->load(['categoryProperties' => function ($query) use($appLocalisationId){
                $query
                    ->where('code', 'name')
                    ->where('localisation_id', $appLocalisationId);
            }]);

        // Данные для вьюшки на необходимой локализации
        $countries = ApplicationDictionary::getDataForSelect($appLocalisationId, 'countries');

        $quantities = ApplicationDictionary::where('group_code', 'quantities')->where('localisation_id', $appLocalisationId)->get();
        $sizes = ApplicationDictionary::where('group_code', 'sizes')->where('localisation_id', $appLocalisationId)->get();
        $colors = ApplicationDictionary::where('group_code', 'colors')->where('localisation_id', $appLocalisationId)->get(); // хранится в hex
        $weights = ApplicationDictionary::where('group_code', 'weights')->where('localisation_id', $appLocalisationId)->get();

        return view('cabinet.product.create.fill_properties', compact('profile', 'category', 'countries', 'colors', 'quantities', 'sizes', 'weights'));
    }

    public function addModel(Profile $profile, Product $product, Request $request)
    {
        // Проверяем принадлежность пользователю профиля
        if ( !$profile->users()->find( auth()->user()->id ) )
        {
            abort(403);
        }
        
        $localisationId = $profile->profileProperties()->first()->localisation_id;
        $appLocalisationId = Localisation::where('code', app('translator')->getLocale())->first()->id;
        
        $category = Category::findOrFail($product->category_id)
            ->load(['categoryProperties' => function ($query) use($appLocalisationId){
                $query
                    ->where('code', 'name')
                    ->where('localisation_id', $appLocalisationId);
            }]);

        if($request->filled('steps'))
        {
            $steps = true;
        }else
        {
            $steps = false;
        }

        $product = Product::getProductWithModels($product, (int)$localisationId, $appLocalisationId);
        $product->setRelation('downloads', $product->downloads->groupBy('type'));
        
        $quantities = ApplicationDictionary::where('group_code', 'quantities')->where('localisation_id', $appLocalisationId)->get();
        $colors = ApplicationDictionary::where('group_code', 'colors')->where('localisation_id', $appLocalisationId)->get(); // хранится в hex
        $articulPrefix = ProductModel::getPrefixForArticul($product->id) + 1;
   
        return view('cabinet.product.add_model', compact('profile', 'category', 'quantities', 'colors', 'product', 'steps', 'articulPrefix'));
    }
    //ок
    public function store(Profile $profile, CreateRequest $request)
    {
        // Проверяем принадлежность пользователю профиля
        if ( !$profile->users()->find( auth()->user()->id ) || !$request->filled('category_id'))
        {
            abort(403);
        }

        Category::findOrFail($request->category_id);
        
        $deletedFiles = $request->filled('deleted_files') ? json_decode($request->deleted_files, true) : null;
        
        if($deletedFiles)
        {
            $productFiles = $this->filtrateInputFiles($request->allFiles(), $deletedFiles);
            
        }else
        {
            $productFiles = $request->allFiles();
        }

        $localisationId = $profile->profileProperties()->first()->localisation_id;

        // Создаем товар для профиля(локализация профиля = локализации товара) пользователя
        $product = $this->service->create($profile, (int)$localisationId, $request->except(['deleted_files', '_token', '_method', 'preview_image', 'detail_images', 'describle_images']));

        // Чтобы грохнуть файлы определенного профиля быстро, если понадобиться
        $folderForImages = 'products/' . $profile->id;
        
        // Добавляем файлы
        if($productFiles)
        {
            foreach ($productFiles as $fileType => & $file) 
            {
                if( in_array($fileType, $this->service::ALLOWED_FILES) )
                {
                    if(is_object($file)) 
                    {
                        $file = $this->addFiles($product, [$file], $fileType, $folderForImages);
                        
                    }elseif(is_array($file))
                    {
                        $file = $this->addFiles($product, $file, $fileType, $folderForImages);
                    }
                }
            }
        }

        return redirect()->route('cabinet.product.show', ['profile' => $profile->id, 'product' => $product->id, 'steps' => 'steps']);
    }
    //ок
    public function storeModel(Profile $profile, Product $product, CreateModelRequest $request)
    {
        // Проверяем принадлежность пользователю профиля
        if ( !$profile->users()->find( auth()->user()->id ) || !$profile->products()->find($product->id))
        {
            abort(403);
        }

        $localisationId = $profile->profileProperties()->first()->localisation_id;

        // Создаем торговое предложение для товара(локализация профиля = локализации товара(торгового предложения))
        $productModel = $this->service->createModel($profile, $product, (int)$localisationId, $request->except(['_token', '_method', 'preview_image']));

         // Чтобы грохнуть файлы определенного профиля быстро, если понадобиться
        $folderForImages = 'products/'.$profile->id;

        if($request->has('preview_image') && $request->preview_image)
        {
            $productPreviewImage = $this->addFiles($productModel,  [$request->preview_image], 'preview_image', $folderForImages);
        }

        return response()->json(true, 200);
    }

    public function edit(Profile $profile, Product $product)
    {
        // Проверяем принадлежность пользователю профиля
        if ( !$profile->users()->find( auth()->user()->id ) || !$profile->products()->find($product->id))
        {
            abort(403);
        }
        // Товар отображаем на своей локализации
        $localisationId = $profile->profileProperties()->first()->localisation_id;
        // Интерфейс - на активной локализации сайта
        $appLocalisationId = Localisation::where('code', app('translator')->getLocale())->first()->id;

        $product = Product::getProduct($product, (int)$localisationId, $appLocalisationId);

        $countries = ApplicationDictionary::getDataForSelect($appLocalisationId, 'countries');
        $categories = Category::getDataForSelect($appLocalisationId, 'name'); // TODO: Добавить категорий для всех активных локализаций
        $quantities = ApplicationDictionary::where('group_code', 'quantities')->where('localisation_id', $appLocalisationId)->get();
        $sizes = ApplicationDictionary::where('group_code', 'sizes')->where('localisation_id', $appLocalisationId)->get();
        $colors = ApplicationDictionary::where('group_code', 'colors')->where('localisation_id', $appLocalisationId)->get(); // хранится в hex
        $weights = ApplicationDictionary::where('group_code', 'weights')->where('localisation_id', $appLocalisationId)->get();

        //dd($product, $quantities);
        return view('cabinet.product.edit.product', compact('profile', 'product', 'categories', 'countries', 'colors', 'quantities', 'sizes', 'weights'));
    }

    public function editModel(Profile $profile, Product $product, ProductModel $model)
    {
        // Проверяем принадлежность пользователю профиля
        if ( !$profile->users()->find( auth()->user()->id ) || !$profile->products()->find($product->id))
        {
            abort(403);
        }

        $localisationId = $profile->profileProperties()->first()->localisation_id;
        $appLocalisationId = Localisation::where('code', app('translator')->getLocale())->first()->id;

        $product = Product::getProductWithModels($product, (int)$localisationId, $appLocalisationId);
        $product->setRelation('downloads', $product->downloads->groupBy('type'));
        
        // Убираем то что редактируем из списка торговых предложений
        $product = $product->setRelation('productModels', $product->productModels->except($model->id));

        $category = Category::findOrFail($product->category_id)
            ->load(['categoryProperties' => function ($query) use($appLocalisationId){
                $query
                    ->where('code', 'name')
                    ->where('localisation_id', $appLocalisationId);
            }]);
       
        $quantities = ApplicationDictionary::where('group_code', 'quantities')->where('localisation_id', $appLocalisationId)->get();
        $colors = ApplicationDictionary::where('group_code', 'colors')->where('localisation_id', $appLocalisationId)->get();
        $editedModel = ProductModel::getModel($model, (int)$localisationId, $appLocalisationId);
        
        return view('cabinet.product.edit.model', compact('profile', 'product', 'colors', 'quantities', 'editedModel', 'category'));
    }
    // ок
    public function update(Profile $profile, Product $product, EditRequest $request)
    {
        // Проверяем принадлежность пользователю профиля
        if ( !$profile->users()->find( auth()->user()->id ) || !$profile->products()->find($product->id))
        {
            abort(403);
        }
        
        // Если он хочет удалить обязательное фото для превью карточки и ничего не добавил взамен - хрена лысого ему
        if($request->has('delete_files') && in_array($product->downloads->where('type', 'preview_image')->first()->id, $request->delete_files) && !$request->has('preview_image'))
        {
            return redirect()->back()->withInput()->withErrors(['preview_image' => __('validation.required', ['attribute' => __('validation.attributes.preview_image')] ) ]);
        }
        
        $deletedFiles = $request->filled('deleted_files') ? json_decode($request->deleted_files, true) : null;
        
        if($deletedFiles)
        {
            $productFiles = $this->filtrateInputFiles($request->allFiles(), $deletedFiles);
            
        }else
        {
            $productFiles = $request->allFiles();
        }
        
        // Удаляем файлы
        if($request->has('delete_files'))
        {
            $countDeletedFiles = $this->softDeleteFiles($product,  $request->delete_files);
        }

        $localisationId = $profile->profileProperties()->first()->localisation_id;

        $product = $this->service->update($profile, $product, (int)$localisationId, $request->except(['delete_files', 'deleted_files', 'preview_image', 'detail_images', 'describle_images', '_token', '_method']));

        $folderForImages = 'products/' . $profile->id;
        
        // Добавляем файлы
        if($productFiles)
        {
            foreach ($productFiles as $fileType => & $file) 
            {
                if( in_array($fileType, $this->service::ALLOWED_FILES) )
                {
                    if(is_object($file)) // Прилетело с инпут без аттрибута multiple. Перезаписываем.
                    {
                        $this->destroyFiles($product, $product->downloads->where('type', $fileType)->pluck('id')->toArray() );
                        $file = $this->addFiles($product, [$file], $fileType, $folderForImages);
                        
                    }elseif(is_array($file))
                    {
                        $file = $this->addFiles($product, $file, $fileType, $folderForImages);
                    }
                }
            }
        }

        return redirect()->back();
    }
    //ок
    public function updateModel(Profile $profile, Product $product, ProductModel $model, EditModelRequest $request)
    {
        // Проверяем принадлежность пользователю профиля
        if ( !$profile->users()->find( auth()->user()->id ) || !$profile->products()->find($product->id))
        {
            abort(403);
        }

        // Если он хочет удалить обязательное фото для превью торгового предложения и ничего не добавил взамен - хрена лысого ему
        if($request->has('delete_files') && in_array($model->downloads->where('type', 'preview_image')->first()->id, $request->delete_files) && !$request->has('preview_image'))
        {
            return redirect()->back()->withInput()->withErrors(['preview_image' => __('validation.required', ['attribute' => __('validation.attributes.preview_image')] ) ]);
        }
        
        $deletedFiles = $request->filled('deleted_files') ? json_decode($request->deleted_files, true) : null;
        
        if($deletedFiles)
        {
            $productModelFiles = $this->filtrateInputFiles($request->allFiles(), $deletedFiles);
            
        }else
        {
            $productModelFiles = $request->allFiles();
        }

        if($request->has('delete_files'))
        {
            $countDeletedFiles = $this->softDeleteFiles($model,  $request->delete_files);
        }

        $localisationId = $profile->profileProperties()->first()->localisation_id;

        $model = $this->service->updateModel($profile, $model, (int)$localisationId, $request->except(['delete_files', 'preview_image', '_token', '_method', 'deleted_files', 'delete_files']));

        $folderForImages = 'products/' . $profile->id;
        
        // Добавляем файлы
        if($productModelFiles)
        {
            foreach ($productModelFiles as $fileType => & $file) 
            {
                if( in_array($fileType, $this->service::ALLOWED_FILES) )
                {
                    if(is_object($file)) // Прилетело с инпут без аттрибута multiple. Перезаписываем.
                    {
                        $this->destroyFiles($model, $model->downloads->where('type', $fileType)->pluck('id')->toArray() );
                        $file = $this->addFiles($model, [$file], $fileType, $folderForImages);
                        
                    }elseif(is_array($file))
                    {
                        $file = $this->addFiles($model, $file, $fileType, $folderForImages);
                    }
                }
            }
        }

        return redirect()->back();
    }

    public function delete(Profile $profile, Product $product)
    {
        // Проверяем принадлежность пользователю профиля
        if ( !$profile->users()->find( auth()->user()->id ) )
        {
            abort(403);
        }

        $this->service->delete($profile, $product->id);

        return redirect()->back();
    }

    public function deleteModel(Profile $profile, Product $product, ProductModel $model)
    {
        // Проверяем принадлежность пользователю профиля
        if ( !$profile->users()->find( auth()->user()->id ) )
        {
            abort(403);
        }

        $this->service->deleteModel($profile, $product->id, $model->id);

        return redirect()->back();
    }

    public function disable(Profile $profile, Product $product)
    {
        // Проверяем принадлежность пользователю профиля
        if ( !$profile->users()->find( auth()->user()->id ) )
        {
            abort(403);
        }

        $this->service->disable($profile, $product->id);

        return redirect()->back();
    }

    public function activate(Profile $profile, Product $product)
    {
         // Проверяем принадлежность пользователю профиля
        if ( !$profile->users()->find( auth()->user()->id ) )
        {
            abort(403);
        }

        $this->service->activate($profile, $product->id);

        return redirect()->back();
    }

    public function destroy(Profile $profile, int $productId)
    {
        // Проверяем принадлежность пользователю профиля
        if ( !$profile->users()->find( auth()->user()->id ) )
        {
            abort(403);
        }

        $this->service->destroy($profile, $productId);

        return redirect()->back();
    }

    public function destroyModel(Profile $profile, int $productId, int $modelId)
    {
        // Проверяем принадлежность пользователю профиля
        if ( !$profile->users()->find( auth()->user()->id ) || !$profile->products()->withTrashed()->find($productId))
        {
            abort(403);
        }

        $this->service->destroyModel($profile, $productId, $modelId);

        return redirect()->back();
    }

    public function restore(Profile $profile, int $productId)
    {
        // Проверяем принадлежность пользователю профиля
        if ( !$profile->users()->find( auth()->user()->id ) )
        {
            abort(403);
        }

        $this->service->restore($profile, $productId);

        return redirect()->back();
    }

    public function restoreModel(Profile $profile, int $productId, int $modelId)
    {
        // Проверяем принадлежность пользователю профиля
        if ( !$profile->users()->find( auth()->user()->id ) )
        {
            abort(403);
        }

        $this->service->restoreModel($profile, $productId, $modelId);

        return redirect()->back();
    }

    public function makeAction(Profile $profile, ActionRequest $request)
    {
        // Проверяем принадлежность пользователю профиля
        if ( !$profile->users()->find( auth()->user()->id ) )
        {
            abort(403);
        }

        if($request->filled('elements') && in_array($request->action, $this->service::ALLOWED_ACTIONS) && mb_strpos($request->action, '_all') === false )
        {
            foreach($request->elements as $element)
            {
                if( isset($element['type']) && isset($element['id']) )
                {
                    switch ($element['type'])
                    {
                        case 'product':
                            $action = str_replace('_checked', '', $request->action);
                            if(method_exists($this->service, $action))
                            {
                                $this->service->$action($profile, $element['id']);
                            }
                            break;
                        case 'model':
                            $action = str_replace('_checked', 'Model', $request->action);
                            if(method_exists($this->service, $action))
                            {
                                $this->service->$action($profile, $element['parent_id'], $element['id']);
                            }
                            break;
                    }
                }
            }

        }elseif(mb_strpos($request->action, '_all') !== false)
        {
            $action = str_replace('_all', '_products', $request->action);
            $action = explode('_', $action);
            $action = lcfirst(implode('', array_map('ucfirst', $action)));

            if(method_exists($this->service, $action))
            {
                $this->service->$action($profile);
            }
        }

        return redirect()->back();
    }
}
