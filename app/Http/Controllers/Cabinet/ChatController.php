<?php

namespace App\Http\Controllers\Cabinet;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\{Chat, Profile};
use App\User;
use App\Helpers\Chat\ChatService;

class ChatController extends Controller
{
    public function __construct(ChatService $service)
    {
        $this->service = $service;
        
        $this->middleware('redirect_if_not_create_or_select_profile', ['only' => ['view', 'writeMember']]);
        $this->middleware('permission:send message', ['only' => ['view', 'writeMember']]);
        $this->middleware('permission:send admin', ['only' => ['view', 'writeMember', 'viewWithoutProfile']]);
        $this->middleware('permission:list message', ['only' => ['view', 'writeMember']]);
    }

    /*
     * Вьюшка чата
     */
    public function view(Profile $profile, Request $request)
    {
        if ( ( !$profile->users()->find(auth()->user()->id) ) || ($profile->id !== \Session::get('active_profile')->id) )
        { 
            abort(403);
        }

        $userHash = $this->service::getHash($profile->id);
        $userSupportHash = $this->service::getHash(auth()->user()->id, 'support');
        
        $currentProfileName = $profile->profileProperties()->where('code', 'company_name')->first()->value;
        
        $logo = $profile->downloads()->where('type','company_logo_image')->first();
        $logo = (!!$logo) ? asset('storage/' . $logo->path) : asset('assemble/images/cabinetImages/avatar_default.png');

        $activeTab = $request->route('tab') ? $request->route('tab') : 'chat';

        return view('cabinet.chat.index')
            ->with([
                'profile' => $profile,
                'activeTab' => $activeTab,
                'currentProfileName' => $currentProfileName,
                'currentProfileImage' => $logo,
                'userHash' => $userHash,
                'userSupportHash' => $userSupportHash,
                'chatType' => 'profile',
            ]);
    }

    /*
     * Вьюшка чата, если профиль не создан
     */
    public function viewWithoutProfile()
    {
        $this->service->getAdminChatTemplate();

        $userSupportHash = $this->service::getHash(auth()->user()->id, 'support');

        $userAvatar = auth()->user()->downloads->where('type', 'avatar')->first();
        $userAvatar = (!!$userAvatar) ? asset('storage/' . $userAvatar->path) : asset('assemble/images/cabinetImages/avatar_default.png');
        
        return view('cabinet.chat.support', compact('userSupportHash', 'userAvatar'));
    }

    /*
     * Вьюшка чата, для написания сообщения
     */
    public function writeMember(Profile $member)
    {
        $profile = session()->get('active_profile');  

        if ( !$profile->users()->find(auth()->user()->id) || $member->id === $profile->id || auth()->user()->profiles()->find($member->id) )
        { 
            abort(403);
        } 

        $hasChat = Chat::isChatWithMemberExists($profile, $member->id);

        if (!$hasChat)
        {
            $hasChat = $this->service->createChat($profile, $member);
        }

        $userHash = $this->service::getHash($profile->id);
        $userSupportHash = $this->service::getHash( auth()->user()->id, 'support');
        
        $currentProfileName = $profile->profileProperties()->where('code', 'company_name')->first()->value;

        $logo = $profile->downloads()->where('type','company_logo_image')->first();
        $logo = (!!$logo) ? asset('storage/' . $logo->path) : asset('assemble/images/cabinetImages/avatar_default.png');
            
        return view('cabinet.chat.index')
            ->with([
                'profile' => $profile,
                'activeTab' => 'chat',
                'currentProfileName' => $currentProfileName,
                'currentProfileImage' => $logo,
                'userHash' => $userHash,
                'userSupportHash' => $userSupportHash,
                'chatType' => 'with_member',
                'chatId' => $hasChat->id,
            ]);
    }
}