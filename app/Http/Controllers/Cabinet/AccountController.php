<?php
//TODO: добавить методы меняющие пароль и логин, добавляющие привязку к социальным аккаунтам.
namespace App\Http\Controllers\Cabinet;

use App\Models\Profile;
use App\User;
use App\Helpers\Account\AccountService;
use App\Http\Requests\Cabinet\Account\{EditRequest, EditEntranceRequest};
use App\Http\Controllers\Controller;
use Session;

class AccountController extends Controller
{
    private $service;
    
    public function __construct(AccountService $service)
    {
        $this->service = $service;
        
        $this->middleware('permission:list account');
        $this->middleware('permission:edit account', ['only' => ['edit','update', 'editEntrance', 'updateEntrance']]);
        $this->middleware('auth_email_unique', ['only' => ['updateEntrance']]);
    }
    // Не сверстали
    /* public function show(string $userId)
    {
        if($this->service->checkUser((int)$userId))
        {
            $user = User::getUserWithAvatar((int)$userId);
            $account = Profile::getAccountForUser((int)$userId)->first();

            return view('cabinet.account.show', compact('account', 'user'));
            
        }else
        {
            abort(403);
        }
    } */
    
    public function edit(string $userId)
    {
        if($this->service->checkUser((int)$userId))
        {
            $user = User::getUserWithAvatar((int)$userId);
            $account = Profile::getAccountForUser((int)$userId)->first();
            $locale = app('translator')->getLocale();
            $locale = ($locale === 'en') ? 'en-GB' : $locale;
            
            if($account)
            {
                $account = $account
                    ->profileProperties
                    ->mapWithKeys(function ($item) {
                        return [$item['code'] => $item['value']];
                    });
            }
            
            return view('cabinet.account.edit', compact('account', 'user', 'locale'));
            
        }else
        {
            abort(403);
        }
    }

    public function update(EditRequest $request, string $userId)
    {
        if($this->service->checkUser((int)$userId))
        {
            $user = User::findOrFail((int)$userId);
            
            $deletedFiles = $request->filled('deleted_files') ? json_decode($request->deleted_files, true) : null;
        
            if($deletedFiles)
            {
                $userFiles = $this->filtrateInputFiles($request->allFiles(), $deletedFiles);
                
            }else
            {
                $userFiles = $request->allFiles();
            }
            
            if($request->has('delete_files'))
            {
                $countDeletedFiles = $this->softDeleteFiles($user, $request->delete_files);
            }

            // Обновляем информацию об аккаунте пользователя
            $account = $this->service->update($user, $request->except(['avatar', '_token', '_method', 'deleted_files', 'delete_files']));
            
            if($userFiles)
            {
                foreach ($userFiles as $fileType => & $file) 
                {
                    if( in_array($fileType, $this->service::ALLOWED_FILES) )
                    {
                        if(is_object($file)) // Прилетело с инпут без аттрибута multiple. Перезаписываем.
                        {
                            $this->destroyFiles($user, $user->downloads->where('type', $fileType)->pluck('id')->toArray() );
                            $file = $this->addFiles($user, [$file], $fileType);
                            
                        }elseif(is_array($file))
                        {
                            $file = $this->addFiles($user, $file, $fileType);
                        }
                    }
                }
            }
            
            return redirect()->route('cabinet.account.edit', $user->id);
        
        }else
        {
            abort(403);
        }
    }
    
    public function editEntrance(string $userId)
    {
        if($this->service->checkUser((int)$userId))
        {
            $user = User::find($userId)->load('socialAccounts');
            $needOldPassword = ($user->registration_type !== 'social') ? true : false;
            
            return view('cabinet.account.edit_entrance', compact('needOldPassword', 'user'));
            
        }else
        {
            abort(403);
        }
    }
    
    public function updateEntrance(EditEntranceRequest $request, string $userId)
    { 
        if($this->service->checkUser((int)$userId))
        {
            $user = User::find($userId);
            $this->service->updateUser($user, $request->except(['_token', '_method']));
            return redirect()->route('cabinet.account.edit_entrance', $userId);
            
        }else
        {
            abort(403);
        }
    }
}