<?php

namespace App\Http\Controllers\Cabinet;

use Illuminate\Http\Request;
use App\User;
use App\Models\{Favorite, Localisation, Category};
use Illuminate\Support\Facades\Auth;
use App\Helpers\Favorites\FavoriteService;
use App\Http\Controllers\Controller;
use App\Http\Requests\Cabinet\Favorite\FilterRequest;
use App\Helpers\Shop\ShopService;

class FavoriteController extends Controller
{
    private $favoriteService;
    private $shopService;
    
    public function __construct(FavoriteService $favoriteService, ShopService $shopService)
    {
        $this->middleware('auth');
        $this->favoriteService = $favoriteService;   
        $this->shopService = $shopService;      
    }
    
    public function index(User $user, FilterRequest $request)
    {
        if( (!in_array($request->route('type'), $this->favoriteService::ALLOWED_TYPES))
            AND
        ((int)Auth::user()->id !== (int)$user->id) )
        {
            abort(403);
        }
        
        $favorites = collect();
        $appLocalisationId = Localisation::where('code', app('translator')->getLocale())->first()->id;
        
        $favoriteableModelName = implode( '', array_map( 'ucfirst', explode('_', $request->route('type') ) ) );
        $favoriteableName = lcfirst($favoriteableModelName);

        $filtrateFavoritableIds = Favorite::getFiltrateFavoritablesForUser($user->favorites(), $request->all(), $favoriteableModelName, $favoriteableName)
            ->pluck('favoriteable_id')
            ->all(); 
        
        if(!empty($filtrateFavoritableIds))
        {
            $favorites = Favorite::getNeededFavorites($filtrateFavoritableIds, $favoriteableModelName, (int)$appLocalisationId);

            if($request->route('type') === 'product')
            {
                $localisation = Localisation::all()->keyBy('id');
                
                $favorites->transform(function ($item) use($localisation){

                    $item->shop_link = $this->shopService->getProductIndexRoute(
                        $item->shops->users->first(),
                        $localisation[$item->productProperties->first()->localisation_id]->code,
                        null, 
                        null, 
                        null
                    ); 
                    
                    return $item;
                });
            }
        }

        $categories = Category::getDataForSelect($appLocalisationId, 'name');   

        return view('cabinet.favorite.index.' . $request->route('type'), compact('favorites', 'categories'));
    }  
    
    public function remove(User $user, Request $request)
    {
        if( ($request->route('id') && !in_array($request->route('type'), $this->favoriteService::ALLOWED_TYPES))
            AND
        ((int)Auth::user()->id !== (int)$user->id) )
        {
            abort(403);
        }
        
        $model = implode( '', array_map( 'ucfirst', explode('_', $request->route('type') ) ) );

        $this->favoriteService->delete($user, (int)$request->route('id'), (string)$model);
        
        return redirect()->back();
    }  
}
