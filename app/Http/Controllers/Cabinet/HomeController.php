<?php

namespace App\Http\Controllers\Cabinet;

use App\Http\Controllers\Controller;

class HomeController extends Controller
{
    public function index()
    {
        if (!auth()->user()->email)
        {
            $message = [
                'title' => __('notification.fill_account'),
                'body' => __('notification.fill_email_in_account_notify'),
                'type' => 'info'
            ];
        
            \Session::flash('message', $message);
        }
        
        return view('cabinet.home');
    }
}
