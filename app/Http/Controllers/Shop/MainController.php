<?php

namespace App\Http\Controllers\Shop;

use App\Http\Controllers\Controller;
use App\Helpers\Shop\ShopService;
use App\Models\{Profile, Shop, Localisation};
use App\{User, ApplicationDictionary};
use App\Helpers\Shop\Seller;

class MainController extends Controller
{
    private $shopLocalisationId = null;
    private $shopService;    
    
    public function __construct(ShopService $shopService)
    {
        $this->shopService = $shopService;
    }
    
    public function show(string $localisation, User $user)
    {
        if($user->hasRole('site_manager'))
        {
            abort(404);
        }

        $this->shopService->prepareLocalisation($localisation);
        
        $this->shopLocalisationId = Localisation::where('code', $localisation)->first()->id;
        $localisation;
        
        $interface = ApplicationDictionary::getShopInterface($this->shopLocalisationId); 
        
        $seller = \Cache::tags(Shop::class)->rememberForever(
        
            'seller_main_' . $this->shopLocalisationId, 
            
            function() use($user, $localisation)
            {
                $profiles = Profile::getProfilesWithProductsForShop($user, $this->shopLocalisationId);
                
                $data = $this->shopService->prepareDataForSeller($this->shopLocalisationId, $profiles, $user);
                
                return new Seller($data['id'], $data['profile'], $data['categories'], $data['localisations'], $localisation, $data['top_products']);
            }
        );

        return view('shop.show.main',  compact('seller', 'interface'));
    }
}
