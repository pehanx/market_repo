<?php

namespace App\Http\Controllers\Shop;

use App\Http\Controllers\Controller;
use App\Helpers\Shop\ShopService;
use App\Helpers\Catalog\CatalogService;
use App\Models\{Profile, Localisation, Product, Category};
use App\Http\Router\CategoryPath;
use App\Helpers\Shop\Seller;
use App\Http\Requests\Shop\SearchParamsRequest;
use App\{User, ApplicationDictionary};

class ProductController extends Controller
{
    private $shopLocalisationId = null;
    private $catalogService;
    private $shopService;    
    
    public function __construct(CatalogService $catalogService, ShopService $shopService)
    {
        $this->catalogService = $catalogService;
        $this->shopService = $shopService;
    }
    
    public function index(string $localisation, User $user, CategoryPath $categoryPath, SearchParamsRequest $request)
    {
        $this->shopService->prepareLocalisation($localisation);
        
        $this->shopLocalisationId = Localisation::where('code', $localisation)->first()->id;
        $currentLocalisation = $localisation;
        $interface = ApplicationDictionary::getShopInterface($this->shopLocalisationId); 
        
        $id = $user->id;
        $profile = null;
        $categories = collect();
        
        $searchParams = $request->except(['order_by']);
        $orderBy = $request->only(['order_by']) ? $this->catalogService->explodeOrderByParams($request->only(['order_by'])) : null;
        $category = $categoryPath->category;
        $categoryIds = [];

        // Покажем от root если не передан $categoryPath
        if(!$category)
        {
            $profile = Profile::getProfileWithProductsForShop($user, $this->shopLocalisationId);
            
            $profile->products->each(function ($item) use (& $categoryIds, & $categories, $user, $request, $localisation, $searchParams){
                array_push($categoryIds, $item->categories->id);
               
                $item->categories->shop_link = $this->shopService->getProductIndexRoute($user, $localisation, $item->categories->link, $searchParams, $request);
                
                $categories->push($item->categories);
            });

            $categories = $categories->unique('id');
            $categoryIds = array_values(array_unique($categoryIds));
            
            $profile->unsetRelation('products');
            
        } else 
        {
            $profile = Profile::getProfileForShop($user, $this->shopLocalisationId);   
            $categoryIds = Category::descendantsAndSelf($category->id)->pluck('id')->all();
            
            if(!empty($categoryIds) && is_countable($categoryIds) && count($categoryIds) > 1)
            {
                $categories = Category::getCategoryDescendantsForShop($profile, $category, $this->shopLocalisationId);
            }

            if(!$categories->first())
            {
                $back = $category
                    ->parent
                    ->load(['categoryProperties' =>  function ($query) {
                        $query
                            ->where([
                                ['code', 'name'],
                                ['localisation_id', $this->shopLocalisationId],
                            ]);
                    }]);
                    
                $back->shop_link = $this->shopService->getProductIndexRoute($user, $localisation, $back->link, $searchParams, $request);
                    
            } else 
            {
                $categories->transform(function ($item) use ($user, $request, $localisation, $searchParams){
                    $item->shop_link = $this->shopService->getProductIndexRoute($user, $localisation, $item->link, $searchParams, $request);
                    return $item;
                });
            } 
            
            $currentCategory = $category;
            $currentCategory->name = $currentCategory
                ->categoryProperties
                ->where('code', 'name')
                ->where('localisation_id', $this->shopLocalisationId)
                ->first()
                ->value;
        }
     
        $products = Product::getAllProductsFromCategories($categoryIds, $this->shopLocalisationId, 12, $searchParams, $orderBy, $profile);
        $products->appends($request->input());
      
        $seller = new Seller($id, $profile, null, null, $currentLocalisation, null);

        return view(
            'shop.index.product',
            compact(
                'interface',
                'seller',
                'products', 
                !empty($categories->first()) ? 'categories' : null,
                !empty($back) ? 'back' : null,
                !empty($currentCategory) ? 'currentCategory' : null,
            )
        );
    }
}
