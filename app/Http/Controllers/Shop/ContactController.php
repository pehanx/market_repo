<?php

namespace App\Http\Controllers\Shop;

use App\Http\Controllers\Controller;
use App\Models\{Profile, Localisation};
use App\{User, ApplicationDictionary};
use App\Helpers\Shop\{Seller, ShopService};

class ContactController extends Controller
{
    private $shopService;    
    
    public function __construct(ShopService $shopService)
    {
        $this->shopService = $shopService;
    }
    
    public function show(string $localisation, User $user)
    {
        if(!$this->shopService->userHasShop($user))
        {
            abort(404);
        }

        $this->shopService->prepareLocalisation($localisation);
        
        $localisationId = Localisation::where('code', $localisation)->first()->id;
        $interface = ApplicationDictionary::where('group_code', 'shops')->where('localisation_id', $localisationId)->get()->keyBy('code');
        
        $id = $user->id;
        $profile = Profile::getProfileForShop($user, $localisationId); 
 
        $seller = new Seller($id, $profile, null, null, $localisation, null);

        //В зависимости от этого флага, определяется возможность добавить в контакты поставщика
        if( session()->get('active_profile') === null
            || session()->get('active_profile')->contacts()->where('contact_id', $profile->id)->first() === null)
        {
            $isContact = false;
            
        } else
        {
            $isContact = true;
        }

        $myProfile = false;

        if(auth()->check())
        {
            $myProfile = !!(auth()->user()->id === $user->id);
        }


        return view('shop.show.contact', compact('interface', 'seller', 'isContact', 'myProfile'));
    }
}
