<?php

namespace App\Http\Controllers;

use App\Http\Router\PagePath;
use App\Models\{Page, Localisation};

class PageController extends Controller
{
    public function show(PagePath $pagePath)
    {  
        $page = $pagePath->page;

        if(!$page->is_active)
        {
            abort(404);
        }
        
        $appLocId = Localisation::where('code', app('translator')->getLocale())->first()->id;
        $page->setRelation('pageProperties', $page->pageProperties->where('localisation_id', $appLocId));
        
        $redirect = null;
        $parent = $page->parent;

        // Если описания страницы на данной локализации нет, то редирект к предку. Если предка нет, или изьм езьм root, то редиректим на главную.
        if($page->pageProperties->count() === 0 && $page->parent_id !== null && $parent)
        {
            $path = new PagePath;
            $path = $path->resolveRouteBinding(Page::getFullLink($parent));
            
            $redirect = redirect()->route('page', $path);
            
        }elseif($page->pageProperties->count() === 0 && $page->parent_id === null) 
        {
            $redirect = redirect()->route('main');
        }
                
        if($redirect)
        {
            return $redirect;
            
        } else 
        {
            // Строим полное дерево от нужного root
            $root = ($page->parent_id === null) ? $page : $page->ancestors()->where('parent_id', null)->first(); 
            $pageDescendants = Page::getPageDescendants($root->id, $appLocId);
          
            return view(
                'pages.show',
                compact(
                    'page',
                    !empty($pageDescendants) ? 'pageDescendants' : null,
                )
            );
        }
    }
}
