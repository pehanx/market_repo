<?php

namespace App\Http\Requests\Pages\SearchResult;

use Illuminate\Foundation\Http\FormRequest;

class SearchParamsRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [           
            "category" => "numeric|nullable",
            "search" => "string|nullable",
            "min_price" => "numeric|nullable",
            "max_price" => "numeric|nullable",
            "country" => "string|max:255|nullable",
            "color" => "string|max:255|nullable",
            "order_by" => "string|max:255|nullable|in:price_desc,price_asc,shows_asc,shows_desc"
        ];
    }
}