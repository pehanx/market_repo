<?php

namespace App\Http\Requests\Catalog\Product;

use Illuminate\Foundation\Http\FormRequest;

class WriteSupplierRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [           
            "company_name" => "string|required",
            "email" => "string|email|required",
            "phone" => "string|max:255|required"
        ];
    }
}