<?php

namespace App\Http\Requests\Admin\Page;

use Illuminate\Foundation\Http\FormRequest;

class EditPropertiesRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'title' => 'required|string|max:255',
            'menu_title' => 'string|max:255|nullable',
            'content' => 'required|string',
            'seo_title' => 'string|nullable',
            'seo_keywords' => 'string|nullable',
            'seo_descriptions' => 'string|nullable',
        ];
    }
}