<?php

namespace App\Http\Requests\Admin\Page;

use Illuminate\Foundation\Http\FormRequest;

class CreateRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'localisation' => 'required|numeric|exists:localisations,id',
            'parent' => 'numeric|nullable',
            'title' => 'required|string|max:255',
            'menu_title' => 'string|max:255|nullable',
            'link' => 'required|string|max:255',
            'content' => 'required|string',
            'seo_title' => 'string|nullable',
            'seo_keywords' => 'string|nullable',
            'seo_descriptions' => 'string|nullable',
        ];
    }
}