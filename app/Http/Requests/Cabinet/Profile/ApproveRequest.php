<?php

namespace App\Http\Requests\Cabinet\Profile;

use Illuminate\Foundation\Http\FormRequest;

class ApproveRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        $mimeTypes = [
            'jpeg',
            'png',
            'jpg',
            'gif',
            'svg',
            'docx',
            'doc',
            'xls',
            'xlsx',
            'ppt',
            'pptx',
            'csv',
            'odp',
            'ods',
            'txt',
            'odt'
        ];

        return [
            'documents.*' => 'required|mimes:' . implode(',', $mimeTypes),    
        ];
    }
}
