<?php

namespace App\Http\Requests\Cabinet\Profile;

use Illuminate\Foundation\Http\FormRequest;

class CreateRequest extends FormRequest
{
    public function authorize(): bool
    {
        //dd($this->all());
        return true;
    }

    public function rules(): array
    {
        $rules = [];
        
        if ($this->filled('profile_type') && $this->profile_type === 'seller')
        {
            $rules = [
                'company_user' => 'numeric|exists:users,id',
                'company_name' => 'required|string|max:255',
                'country' => 'required|string|max:255',
                'region' => 'required|string|max:255',
                'city' => 'required|string|max:255',
                'index' => 'numeric|nullable',
                'street' => 'string|nullable',
                'number_home' => 'numeric|nullable',
                'number_build' => 'numeric|nullable',
                'number_room' => 'numeric|nullable',
                
                'name' => 'required|string|max:255',
                'last_name' => 'required|string|max:255',
                'patronymic' => 'string|nullable',
                'position' => 'string|nullable',
                //'number_phone' => 'required|string|regex:/^\d+$/s',
                //'number_phone_mobile' => 'string|regex:/^\d+$/s|min:11|nullable',
                'number_phone' => 'required|string',
                'number_phone_mobile' => 'string|nullable',
                
                'company_description' => 'string|nullable',
                'company_youtube_link' => ['nullable','url','regex:/^http(?:s?):\/\/(?:www\.)?youtu(?:be\.com\/watch\?v=|\.be\/)([\w\-\_]*)(&(amp;)?‌​[\w\?‌​=]*)?/'],
                'company_logo_image' => 'required|image|mimes:jpeg,png,jpg,gif,svg', 
                'company_main_banner_images' => 'required|array',
                'company_main_banner_images.*' => 'required|image|mimes:jpeg,png,jpg,gif,svg', 
                'company_promo_banner_images' => 'array',
                'company_promo_banner_images.*' => 'image|mimes:jpeg,png,jpg,gif,svg',
                'company_description_image' => 'image|mimes:jpeg,png,jpg,gif,svg', 
                'company_other_images' => 'array',
                'company_other_images.*' => 'image|mimes:jpeg,png,jpg,gif,svg', 
                'company_video' => 'mimes:mpeg,mp4,mpg,qt,mov,avi|max:32768',
                'deleted_file' => 'json|nullable',
                
                'profile_type' => 'required|string|max:255',  
                //'localisation' => 'required|string|exists:localisations,code', 
            ];
            
        }elseif($this->filled('profile_type') && $this->profile_type === 'buyer')
        {
            $rules = [
                'company_user' => 'numeric|exists:users,id',
                'company_name' => 'required|string|max:255',
                'country' => 'required|string|max:255',
                'region' => 'required|string|max:255',
                'city' => 'required|string|max:255',
                'index' => 'numeric|nullable',
                'street' => 'string|nullable',
                'number_home' => 'numeric|nullable',
                'number_build' => 'numeric|nullable',
                'number_room' => 'numeric|nullable',
                
                'name' => 'required|string|max:255',
                'last_name' => 'required|string|max:255',
                'patronymic' => 'string|nullable',
                'position' => 'string|nullable',
                //'number_phone' => 'required|string|regex:/^\d+$/s',
                //'number_phone_mobile' => 'string|regex:/^\d+$/s|min:11|nullable',
                'number_phone' => 'required|string',
                'number_phone_mobile' => 'string|nullable',

                'profile_type' => 'required|string|max:255',  
                'localisation' => 'required|string|exists:localisations,code', 
            ];
            
        }else
        {
            $rules = [
                'profile_type' => 'required|string|in:seller,buyer',       
            ];
        }
        
        return $rules;
    }
}
