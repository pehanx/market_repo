<?php

namespace App\Http\Requests\Cabinet\Profile;

use Illuminate\Foundation\Http\FormRequest;

class EditRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        $rules = [];
        
        if ($this->filled('profile_type') && $this->profile_type === 'seller')
        {
            $rules = [
                'company_name' => 'required|string|max:255',
                'country' => 'required|string|max:255',
                'region' => 'required|string|max:255',
                'city' => 'required|string|max:255',
                'index' => 'required|string|regex:/^\d+$/s',
                'street' => 'string|nullable',
                'number_home' => 'string|regex:/^\d+$/s|nullable',
                'number_build' => 'string|regex:/^\d+$/s|nullable',
                'number_room' => 'string|regex:/^\d+$/s|nullable',
                
                'name' => 'required|string|max:255',
                'last_name' => 'required|string|max:255',
                'patronymic' => 'string|nullable',
                'position' => 'string|nullable',
                'number_phone' => 'required|string|regex:/^\d+$/s',
                'number_phone_mobile' => 'string|regex:/^\d+$/s|min:11|nullable',
                
                'company_description' => 'string|nullable',
                'company_youtube_link' => ['nullable','url','regex:/^http(?:s?):\/\/(?:www\.)?youtu(?:be\.com\/watch\?v=|\.be\/)([\w\-\_]*)(&(amp;)?‌​[\w\?‌​=]*)?/'],
                'company_logo_image' => 'image|mimes:jpeg,png,jpg,gif,svg', 
                'company_main_banner_images' => 'array|nullable',
                'company_main_banner_images.*' => 'image|mimes:jpeg,png,jpg,gif,svg|nullable', 
                'company_promo_banner_images' => 'array',
                'company_promo_banner_images.*' => 'image|mimes:jpeg,png,jpg,gif,svg',
                'company_description_image' => 'image|mimes:jpeg,png,jpg,gif,svg', 
                'company_other_images' => 'array',
                'company_other_images.*' => 'image|mimes:jpeg,png,jpg,gif,svg', 
                'company_video' => 'mimes:mpeg,mp4,mpg,qt,mov,avi|max:32768',
                'delete_files' => 'array|nullable',
                'delete_files.*' => 'string|regex:/^\d+$/s|nullable',
                
                'profile_type' => 'required|string|max:255',  
                'localisation' => 'required|string|exists:localisations,code',  
            ];
            
        }elseif($this->filled('profile_type') && $this->profile_type === 'buyer')
        {
            $rules = [
                'company_name' => 'required|string|max:255',
                'country' => 'required|string|max:255',
                'region' => 'required|string|max:255',
                'city' => 'required|string|max:255',
                'index' => 'required|string|regex:/^\d+$/s',
                'street' => 'string|nullable',
                'number_home' => 'string|regex:/^\d+$/s|nullable',
                'number_build' => 'string|regex:/^\d+$/s|nullable',
                'number_room' => 'string|regex:/^\d+$/s|nullable',
                
                'name' => 'required|string|max:255',
                'last_name' => 'required|string|max:255',
                'patronymic' => 'string|nullable',
                'position' => 'string|nullable',
                'number_phone' => 'required|string|regex:/^\d+$/s',
                'number_phone_mobile' => 'string|regex:/^\d+$/s|min:11|nullable',
                
                'profile_type' => 'required|string|max:255',  
                'localisation' => 'required|string|exists:localisations,code',  
            ];
            
        }
        
        return $rules;
    }
}
