<?php

namespace App\Http\Requests\Cabinet\Chat;

use Illuminate\Foundation\Http\FormRequest;

class ChatSupportRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'message_text' => 'string|max:255|required',
        ];
    }
}