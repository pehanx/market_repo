<?php

namespace App\Http\Requests\Cabinet\Chat;

use Illuminate\Foundation\Http\FormRequest;

class ChatContactRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        if($this->has('search_string')) //Можно сделать в отдельном реквесте для поиска по контактам
        {
            return [
                'search_string' => 'string|required'
            ];
        }

        return [
            'profile_id' => 'numeric|exists:profiles,id|required', // Профиль которому отправляем
            'with_managers' => 'boolean',
        ];
    }
}