<?php

namespace App\Http\Requests\Cabinet\Chat;

use Illuminate\Foundation\Http\FormRequest;

class ChatMessagesRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'profile_id' => 'numeric|exists:profiles,id|required', // Профиль которому отправляем
            'message_text' => 'string|max:255|required',
            'with_managers' => 'boolean',
        ];
    }
}