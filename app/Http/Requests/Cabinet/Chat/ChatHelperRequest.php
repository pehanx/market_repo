<?php

namespace App\Http\Requests\Cabinet\Chat;

use Illuminate\Foundation\Http\FormRequest;

class ChatHelperRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        if($this->has('search_string')) // Для поиска контактов
        {
            return [
                'search_string' => 'string|required',
                'search_type' => 'string|in:all,contacts'
            ];
        }

        if($this->has('translate')) // Для перевода конкретного сообщения
        {
            return [
                'message_id' => 'numeric|exists:messages,id|required',
                'translate' => 'boolean|required',
            ];
        }

        if($this->has('messages_read')) // Для поиска контактов
        {
            return [
                'messages' => 'array|required',
            ];
        }

        if($this->has('setting_type')) // Для настроек в модалке
        {
            return [
                'setting_type' => 'string|required',
            ];
        }

        if($this->has('chat_settings')) // Для перевода конкретного сообщения
        {
            return [
                'chat_settings' => 'array|required',
                'chat_settings.localisation' => 'string|exists:localisations,code|required',
            ];
        }

        if($this->has('message_delete')) // Для перевода конкретного сообщения
        {
            return [
                'message_id' => 'numeric|exists:messages,id|required',
            ];
        }

        if($this->has('chat_delete')) // Для удаления чатов
        {
            return [
                'with_messages' => 'in:on,off|required',
            ];
        }

        return [
            'translate' => 'bool|required',
            'search_string' => 'bool|required',
            'chat_settings' => 'bool|required',
            'messages_read' => 'bool|required',
            'chat_delete' => 'bool|required',
            'setting_type' => 'bool|required',
            'message_delete' => 'bool|required',
        ];
    }
}