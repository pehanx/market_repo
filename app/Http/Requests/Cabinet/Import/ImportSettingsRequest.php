<?php

namespace App\Http\Requests\Cabinet\Import;

use Illuminate\Foundation\Http\FormRequest;
use App\Helpers\RequestProduct\ImportRequestProductService;
use App\Helpers\Product\ImportProductService;
use Illuminate\Validation\Validator;

class ImportSettingsRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        if( $this->filled('type') && $this->filled('import_heading') )
        {
            return [
                'type' => 'required|in:request_product,product',
                'import_heading' => 'required|array',
                'import_id' => 'required|numeric', 
            ];
        }
        return [
            'type' => 'filled',
            'import_heading' => 'filled|array'
        ];
    }

    public function withValidator(Validator $validator)
    {
        if( $this->type === 'request_product' )
        {
            $service = new ImportRequestProductService();

        } elseif ( $this->type === 'product' )
        {
            $service = new ImportProductService();
        }
        
        $required = $service->requiredHeaders();

        if($this->import_heading === null)
        {
            $this->import_heading = [];
        }

        foreach ($this->import_heading as $row => $fields)
        {
            foreach ($fields as $key => $name)
            {
                if ( array_key_exists($key,$required) )
                {
                    unset($required[$key]);
                }
            }
        }

        if($required !== [])
        {
            $field = array_key_last($required);
            $message = array_pop($required);

            $validator->after(function ($validator) use ($field, $message) {
                
                $validator->errors()->add($field, $message);
                
            });
        }
    }
}
