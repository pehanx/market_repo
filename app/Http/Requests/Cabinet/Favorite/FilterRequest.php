<?php

namespace App\Http\Requests\Cabinet\Favorite;

use Illuminate\Foundation\Http\FormRequest;

class FilterRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'name' => 'string|max:255|nullable',
            'category_id' => 'exists:categories,id|numeric|nullable',
            'is_active' => 'string|max:255|nullable|in:true,false',
        ];
    }
}