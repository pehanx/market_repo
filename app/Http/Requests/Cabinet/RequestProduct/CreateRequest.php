<?php

namespace App\Http\Requests\Cabinet\RequestProduct;

use Illuminate\Foundation\Http\FormRequest;

class CreateRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'category_id' => 'required|numeric|exists:categories,id',
            'name' => 'required|string',
            'full_description' => 'required|string',
            'min_price' => 'numeric|nullable|max:9223372036854775807',
            'max_price' => 'numeric|nullable|max:9223372036854775807',
            'min_order.value' => 'required|numeric|max:9223372036854775807',
            'min_order.type' => 'required|string|max:255',
            'detail_images.*' => 'image|mimes:jpeg,png,jpg,gif,svg', 
            'deleted_file' => 'json|nullable',
        ];
    }
}