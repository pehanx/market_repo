<?php

namespace App\Http\Requests\Cabinet\RequestProduct;

use Illuminate\Foundation\Http\FormRequest;

class ActionRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'elements' => 'array',
            'elements.id' => 'string|max:255|regex:/^\d+$/s',
            'action' => 'required|string|max:255',
        ];
    }
}