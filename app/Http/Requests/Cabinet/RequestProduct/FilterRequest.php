<?php

namespace App\Http\Requests\Cabinet\RequestProduct;

use Illuminate\Foundation\Http\FormRequest;

class FilterRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'id' => 'string|max:255|regex:/^\d+$/s|nullable',
            'name' => 'string|max:255|nullable',
            'category' => 'string|max:255|regex:/^\d+$/s|nullable',
            'full_filter' => 'string|max:255|nullable',
        ];
    }
}