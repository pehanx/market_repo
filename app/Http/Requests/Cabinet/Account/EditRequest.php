<?php

namespace App\Http\Requests\Cabinet\Account;

use Illuminate\Foundation\Http\FormRequest;

class EditRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'name' => 'required|string',
            'last_name' => 'string|nullable',
            'patronymic' => 'string|nullable',
            'birthday' => 'date|nullable',
            'phone' => 'string|nullable',
            'gender' => 'string|in:man,woman|nullable',
            'avatar' => 'image|mimes:jpeg,png,jpg,gif,svg'
        ];
    }
}
