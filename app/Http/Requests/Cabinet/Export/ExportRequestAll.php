<?php

namespace App\Http\Requests\Cabinet\Export;

use Illuminate\Foundation\Http\FormRequest;

class ExportRequestAll extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {   
        if($this->filled('type') && $this->type === 'product')
        {
            return [
                'export_format' => 'required|string',
                'with_thumbnails' => 'required|string|in:true,false',
                'with_models' => 'required|string|in:true,false',
                'status' => 'required|string|in:active,inactive,deleted',
                'type' => 'required|string',
            ];
            
        } elseif($this->filled('type') && $this->type === 'request_product')
        {
            return [
                'export_format' => 'required|string',
                'with_thumbnails' => 'required|string|in:true,false',
                'status' => 'required|string|in:active,inactive,deleted',
                'type' => 'required|string',
            ];
            
        } else
        {
            abort(422);
        } 
    }
}
