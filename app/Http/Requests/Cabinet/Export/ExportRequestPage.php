<?php

namespace App\Http\Requests\Cabinet\Export;

use Illuminate\Foundation\Http\FormRequest;
use App\Helpers\RequestProduct\RequestProductService as RqtPrService; 
use App\Helpers\Product\ProductService as PrService;

class ExportRequestPage extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {   
        if($this->route('type') === 'product' && in_array($this->route('status'), PrService::ALLOWED_STATUSES) )
        {
            return [
                'export_format' => 'required|string',
                'with_thumbnails' => 'string|in:true',
                'with_models' => 'string|in:true',
                'elements_id' => 'required|json',
            ];
            
        } elseif( $this->route('type') === 'request_product' && in_array($this->route('status'), RqtPrService::ALLOWED_STATUSES) )
        {
            return [
                'export_format' => 'required|string',
                'with_thumbnails' => 'string|in:true',
                'elements_id' => 'required|json',
            ];
        } 
    }
}
