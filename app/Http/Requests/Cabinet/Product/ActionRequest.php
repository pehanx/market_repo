<?php

namespace App\Http\Requests\Cabinet\Product;

use Illuminate\Foundation\Http\FormRequest;

class ActionRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'elements' => 'array',
            'elements.*' => 'array',
            'elements.*.id' => 'string|max:255|regex:/^\d+$/s',
            'elements.*.parent_id' => 'string|max:255|regex:/^\d+$/s',
            'elements.*.type' => 'string|max:255',
            'action' => 'required|string|max:255',
        ];
    }
}