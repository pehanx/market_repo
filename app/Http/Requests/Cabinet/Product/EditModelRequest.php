<?php

namespace App\Http\Requests\Cabinet\Product;

use Illuminate\Foundation\Http\FormRequest;

class EditModelRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'min_price' => 'required|numeric',
            'max_price' => 'numeric|nullable|max:9223372036854775807',
            'available.value' => 'required|numeric|max:9223372036854775807',
            'available.type' => 'required|string|max:255',
            'color' => 'required|string',
            'preview_image' => 'image|mimes:jpeg,png,jpg,gif,svg',
            'delete_files' => 'array|nullable',
            'delete_files.*' => 'string|numeric',            
        ];
    }
}
