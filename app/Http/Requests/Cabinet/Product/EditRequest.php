<?php

namespace App\Http\Requests\Cabinet\Product;

use Illuminate\Foundation\Http\FormRequest;

class EditRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'category_id' => 'required|string|regex:/^\d+$/s',
            'name' => 'required|string',
            'country' => 'required|string',
            'min_price' => 'required|string|regex:/^\d+$/s',
            'max_price' => 'string|regex:/^\d+$/s',
            'available.value' => 'required|string|regex:/^\d+$/s',
            'available.type' => 'required|string|max:255',
            'min_order.value' => 'required|string|regex:/^\d+$/s',
            'min_order.type' => 'required|string|max:255',
            'color' => 'required|string',
            'preview_image' => 'image|mimes:jpeg,png,jpg,gif,svg', 
            'detail_images.*' => 'image|mimes:jpeg,png,jpg,gif,svg', 
            'describle_images.*' => 'image|mimes:jpeg,png,jpg,gif,svg', 
            'video' => 'mimes:mpeg,mpg,mp4,qt,mov,avi|max:32768',
            'delete_files' => 'array|nullable',
            'delete_files.*' => 'string|regex:/^\d+$/s|nullable',
            'deleted_file' => 'json|nullable'
        ];
    }
}