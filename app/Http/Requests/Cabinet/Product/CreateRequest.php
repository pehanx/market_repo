<?php

namespace App\Http\Requests\Cabinet\Product;

use Illuminate\Foundation\Http\FormRequest;

class CreateRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'category_id' => 'required|numeric|max:9223372036854775807',
            /* 'articul' => 'required|numeric|max:9223372036854775807', */
            'name' => 'required|string|max:255',
            'country' => 'required|string|max:255',
            'min_price' => 'required|numeric|max:9223372036854775807',
            'max_price' => 'numeric|nullable',
            'available.value' => 'required|numeric|max:9223372036854775807',
            'available.type' => 'required|string|max:255',
            'min_order.value' => 'required|numeric|max:9223372036854775807',
            'min_order.type' => 'required|string|max:255',
            'color' => 'required|string|max:255',
            'description' => 'string|nullable',
            'preview_image' => 'required|image|mimes:jpeg,png,jpg,gif,svg', 
            'detail_images.*' => 'image|mimes:jpeg,png,jpg,gif,svg', 
            'describle_images.*' => 'image|mimes:jpeg,png,jpg,gif,svg', 
            'video' => 'mimes:mpeg,mpg,mp4,qt,mov,avi|max:32768',
            'seo_keywords' => 'string|nullable',
            'seo_title' => 'string|nullable',
            'seo_descriptions' => 'string|nullable',
            'weight.value' => 'numeric|nullable',
            'lenght.value' => 'numeric|nullable',
            'width.value' => 'numeric|nullable',
            'height.value' => 'numeric|nullable',
            'package_type' => 'string|nullable',
            'shelf_life' => 'string|nullable',
            'certification' => 'string|nullable',
        ];
    }
}