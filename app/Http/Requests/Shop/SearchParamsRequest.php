<?php

namespace App\Http\Requests\Shop;

use Illuminate\Foundation\Http\FormRequest;

class SearchParamsRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [           
            "seller_search" => "string|nullable",
            "order_by" => "string|max:255|nullable|in:price_desc,price_asc,shows_asc,shows_desc"
        ];
    }
}