<?php

namespace App\Http\Router;

use App\Models\Page;
use Illuminate\Contracts\Routing\UrlRoutable;
use Illuminate\Support\Facades\Cache;

class PagePath implements UrlRoutable
{
    public $category;

    public function withPage(Page $page): self
    {
        $clone = clone $this;
        $clone->page = $page->load('pageProperties');
        return $clone;
    } 
    
    /* 
    * Должен вернуть полный путь до страницы
    */
    public function getRouteKey()
    {
        if (!$this->page) 
        {
            throw new \BadMethodCallException('Empty page.');
        }

        return Cache::tags(Page::class)->rememberForever('page_path_' . $this->page->id, function () {
            return Page::getFullLink($this->page);
        });
    }

    public function getRouteKeyName(): string
    {
        return 'page_path';
    }

    public function resolveRouteBinding($rawPath, $field = null)
    {
        $chunks = explode('/', $rawPath);

        $page = null;
        
        do {
            $link = reset($chunks);
            
            if ($link && $next = Page::where('link', $link)->where('parent_id', $page ? $page->id : null)->first()) 
            {
                $page = $next;
                array_shift($chunks);
            }
            
        } while (!empty($link) && !empty($next));

        if (!empty($chunks)) 
        {
            abort(404);
        }
  
        return $this->withPage($page);
    }
    // Laravel 7 костыль
    public function resolveChildRouteBinding($childType, $value, $field) 
    {
        return null;    
    }
}
