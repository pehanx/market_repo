<?php

namespace App\Http\Router;

use App\Models\Category;
use Illuminate\Contracts\Routing\UrlRoutable;
use Illuminate\Support\Facades\Cache;

class CategoryPath implements UrlRoutable
{
    public $category;

    public function withCategory(Category $category): self
    {
        $clone = clone $this;
        $clone->category = $category->load('categoryProperties');
        return $clone;
    } 
    
    /* 
    * Должен вернуть полный путь до категории каталога
    */
    public function getRouteKey()
    {
        if (!$this->category) 
        {
            abort(404);
        }
       
        return $this->category->link;
    }

    public function getRouteKeyName(): string
    {
        return 'category_path';
    }

    public function resolveRouteBinding($rawPath, $field = null)
    {
        $categories = Cache::tags(Category::class)->rememberForever('links', function() {
            return Category::select('link')->pluck('link')->all();
        });
   
        if(in_array($rawPath, $categories) && $rawPath !== 'catalog')
        {
            unset($categories);
            $category = Category::where('link', $rawPath)->firstOrFail();

            return $this
                ->withCategory($category); 
             
        } else 
        {
            abort(404);
        }
    }

    public function resolveChildRouteBinding($childType, $value, $field) 
    {
        return null;    
    }
}
