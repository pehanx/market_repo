<?php

namespace App\Services\Notification;

use GuzzleHttp\Client;
use Illuminate\Support\Facades\Http;
use Exception;

class FirebaseService
{
    const API_KEY = "AAAAi2n9k7I:APA91bHw3ehWTWOk1vJMyyWNWZOz1B5gxqKQyAzi-wb4PZ4ImmEVZZNI22ONhC7vNV5z_dqLVcS50dtb1CVanefBKxdlCgdZwAkW0NGzU19XsL92etNol1kOgypEoJoqdKruesODBgvZ";
    const API_URL = "https://fcm.googleapis.com/fcm/send";

    public function send(array $message, string $deviceId = "", array $extraData = ["data"=>"test"])
    {

        /* роророррор
            "body" => "SOMETHING",
            "title" => "SOMETHING",
            "icon" => "ic_launcher"
        */

        $fields = [
            "to" => $deviceId,
            "notification" => $message,
            "data" => $extraData
        ];

        $fields = json_encode($fields, true);

        $headers = array (
            'Authorization:key=' . self::API_KEY,
            'Content-Type: application/json'
        );

        $ch = curl_init();
        
        curl_setopt($ch, CURLOPT_URL, self::API_URL);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);

        $result = curl_exec($ch);

        curl_close($ch);
    }
}