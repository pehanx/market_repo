<?php

namespace App\Services\Translate;

use ReflectionClass;
use UnderflowException;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Support\Arr;

abstract class AbstractTranslator
{
    public function __construct() 
    {
        $reflection = new ReflectionClass(static::class);
        
        if(!$reflection->getConstant('LOCALE_CODE_ALIASES'))
        {
            throw new UnderflowException('constant LOCALE_CODE_ALIASES not define');
        }
    }
   
    protected function prepareToLocaleCode(string & $toLocaleCode): string
    {
        return $toLocaleCode = array_key_exists($toLocaleCode, static::LOCALE_CODE_ALIASES) ?
            static::LOCALE_CODE_ALIASES[$toLocaleCode] :
            $toLocaleCode;
    }
    
    protected function checkResult(& $result, string $callback, array $args)
    {
        list($data, $toLocaleCode, $fromLocaleCode) = $args;        

        if(is_array($result))
        {
            // метод translateArray
            
        } elseif(!$result || $result === $data) 
        {
            $translator = $this->changeTranslator();
            $result = $translator->$callback($data, $toLocaleCode, $fromLocaleCode);
        }
        
        return $result;
    }
    
    private function changeTranslator(): TranslatorInterface
    {
        $currentService = ( new ReflectionClass( app()->get(TranslatorInterface::class) ) )->getShortName();

        app()->bind(TranslatorInterface::class, function (Application $app) use($currentService){
            
            $config = $app->make('config')->get('translate');

            switch ($currentService) 
            {
                case 'YandexTranslator':
                    return new YandexTranslator();
                    
                case 'AsiaTranslator':
                    $params = $config['drivers']['asia'];
                    return new AsiaTranslator($params['key'], $params['url']);
                    
                default:
                    throw new \InvalidArgumentException('Undefined Translate driver ' . $config['driver']);
            }
            
        });
        
        session()->put(['translator_changed' => true]);

        return app()->make(TranslatorInterface::class);
    }
    
    public function __destruct() 
    {
        session()->forget('translator_changed');
    }
}