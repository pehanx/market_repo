<?php

namespace App\Services\Translate;

use Illuminate\Support\Facades\Route;
use Mirza;

class YandexTranslator extends AbstractTranslator implements TranslatorInterface
{
    const LOCALE_CODE_ALIASES = [
        'cn' => 'zh'
    ];

    public function checkTranslateApi() 
    {
        $result = (Mirza::translate('test', 'ru') === 'тест') ? true : false;
        
        if(!$result)
        {
            throw new Exception('Api YandexTranslator dont work');
        }
        
        return $result;
    }

    public function translateString(string $string, string $toLocaleCode, string $fromLocaleCode = ''): string 
    {   
        $this->prepareToLocaleCode($toLocaleCode);
        
        $result = Mirza::translate($string, $toLocaleCode);
        
        if(!session()->has('translator_changed'))
        {
            $this->checkResult($result, __FUNCTION__, func_get_args());
        }
        
        return $result;
    }
    
    public function translateArray(array $array, string $toLocaleCode, string $fromLocaleCode = ''): array 
    {
        $this->prepareToLocaleCode($toLocaleCode);
        
        $data = [];
        
        $result = json_decode(Mirza::translateArray($array, $toLocaleCode));
        
        if (!empty($result))
        {
            foreach ($result as $value)
            {
                array_push($data, $value->translatedText);
            }
        }

        return $data;
    }
    
    public function detectLanguage($string) 
    {
        return Mirza::detectLanguage($string);
    }
}