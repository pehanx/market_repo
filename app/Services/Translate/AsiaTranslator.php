<?php

namespace App\Services\Translate;

use GuzzleHttp\Client;
use Illuminate\Support\Facades\Http;
use Exception;

class AsiaTranslator extends AbstractTranslator implements TranslatorInterface
{
    private $key;
    private $url;
    private $client;
    const LOCALE_CODE_ALIASES = [
        'cn' => 'zh-CN'
    ];
    
    public function __construct($key = 'market', $url = 'https://cdn.asiaoptom.com/tr/')
    {
        $this->key = 'market';
        $this->url = 'https://cdn.asiaoptom.com/tr/';
        $this->client = new Client();
    }
    
    public function checkTranslateApi() 
    {
        $data = [];
        array_push($data, 'test');
        
        $result = $this->client->post(
            $this->url, 
            [
                'form_params' => [
                    'to' => 'ru',
                    'from' => 'en',
                    'pkey' => $this->key,
                    'tr' => $data
                ]
            ]
        );
        
        unset ($data);
        $result = json_decode($result->getBody()->getContents(), true);
        
        if( $result && current($result) && !empty( current($result)['translation'] ) && current($result)['translation'] === 'тест' )
        {
           return true;
           
        } else 
        {
            throw new Exception('Api AsiaTranslator dont work');
        }
    }
    
    public function translateString(string $string, string $toLocaleCode, string $fromLocaleCode = 'ru'): string 
    {
        $this->prepareToLocaleCode($toLocaleCode);
        
        $data = [];
        array_push($data, $string);
        
        $result = $this->client->post(
            $this->url, 
            [
                'form_params' => [
                    'to' => $toLocaleCode,
                    'from' => $fromLocaleCode,
                    'pkey' => $this->key,
                    'tr' => $data
                ]
            ]
        );

        unset ($data);
        $result = json_decode($result->getBody()->getContents(), true);
        
        if( $result && current($result) && !empty( current($result)['translation'] ) )
        {
            $result = current($result)['translation'];
            
            if(!session()->has('translator_changed'))
            {
                $this->checkResult($result, __FUNCTION__, func_get_args());
            }

            return $result;
           
        } else
        {
            return '';
        }   
    }

    public function translateArray(array $array, string $toLocaleCode, string $fromLocaleCode = ''): array 
    {
        $this->prepareToLocaleCode($toLocaleCode);
        
        $data = [];
        
        $result = $this->client->post(
            $this->url, 
            [
                'form_params' => [
                    'from' => $fromLocaleCode,
                    'to' => $toLocaleCode,
                    'pkey' => $this->key,
                    'tr' => $array
                ]
            ]
        );
        
        $result = json_decode($result->getBody()->getContents(), true);

        if (!empty($result) && !empty( current($result)['translation'] ) && current($result)['translation'] !== array_shift($array) )
        {
            foreach($result as $fields)
            {
                array_push($data, $fields['translation']);
            }
        }
        
        return $data;
    }
}