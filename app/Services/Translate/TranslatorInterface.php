<?php

namespace App\Services\Translate;

interface TranslatorInterface
{ 
    public function __construct(); 
    
    public function checkTranslateApi(); 

    public function translateString(string $string, string $toLocaleCode, string $fromLocaleCode = ''): string; 

    public function translateArray(array $array, string $toLocaleCode, string $fromLocaleCode = ''): array; 
}