<?php

namespace App\Services\Currency;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Models\{Currency, CurrencyRate};
use GuzzleHttp\Client;

class OExchangeCurrency extends Controller
{
    const API_LINK = 'https://openexchangerates.org/api/latest.json?app_id=';

    private function getCurrencyCodes()
    {
        return Currency::select('code')->get()->pluck('code')->toArray();
    }

    public function getCurrencies(){
        
        $client = new Client();
        
        $link = self::API_LINK . env('CURRENCY_API_KEY');
        
        $html = $client
            ->request(
                'GET', 
                $link,
                ['connect_timeout' => 30]
            );
            
        $html = json_decode($html->getBody()->getContents(), true);
        
        $data = [];
        
        if( $html && isset($html['rates']) )
        {
            $currencyCodes = $this->getCurrencyCodes();

            foreach ($currencyCodes as $currencyCode ) 
            {   
                if( isset( $html['rates'][ strtoupper($currencyCode) ] ) )
                {
                    $data[$currencyCode] = $html['rates'][ strtoupper($currencyCode) ];
                }
            }
        }

        $result = $data ? $this->prepareCurrencyRates($data) : null;
        
        return $result; 
    }

    private function getBaseCurrencyCode(): string
    {
        return Currency::select('code')->where('is_base',true)->pluck('code')->first();
    }

    private function prepareCurrencyRates($data)
    {
        $correctData = [];
        
        $baseCurrencyCode = $this->getBaseCurrencyCode();

        foreach ( $data as $key => $value ) 
        {
            array_push($correctData, [
                'base_currency_code' => $baseCurrencyCode,
                'code' => $key,
                'rate' => $value,
                'created_at'=>Carbon::now(),
            ]);
        }
        
        return $correctData;
    }

    public function updateCurrencyRates($data)
    {   
        CurrencyRate::insert($data);
    }
}