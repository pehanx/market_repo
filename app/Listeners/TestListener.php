<?php

namespace App\Listeners;

use Illuminate\Cache\Events\KeyForgotten;
use App\User;
use App\Http\Controllers\Auth\LoginController;

class TestListener
{
    public function handle(KeyForgotten $event): void
    {
        if(!!$event->tags)
        {
            $user = User::where('session_id', $event->key)->first();

            if($user)
            {
                (new LoginController)->destroySid($user);
            }
        }

    }
}
