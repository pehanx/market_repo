<?php

namespace App\Listeners\Auth;

use Illuminate\Auth\Events\Registered;

class MemberRoleAssignedListener
{
    public function handle(Registered $event): void
    {
        $user = $event->user;
        $user->assignRole('member');
    }
}
