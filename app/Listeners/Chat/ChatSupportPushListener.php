<?php

namespace App\Listeners\Chat;

use App\Events\Chat\ChatSupportPush;
use App\Helpers\Chat\ChatSupportService;

class ChatSupportPushListener
{

    public function __construct( ChatSupportService $service )
    {
        $this->service = $service;
    }

    public function handle(ChatSupportPush $event): void
    {        
        $this->service->sendPush(
            $event->chatId,
            $event->text,
        );
    }
}
