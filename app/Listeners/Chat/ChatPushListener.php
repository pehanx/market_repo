<?php

namespace App\Listeners\Chat;

use App\Events\Chat\ChatPush;
use App\Helpers\Chat\ChatService;

class ChatPushListener
{
    public function __construct(ChatService $service)
    {
        $this->service = $service;
    }

    public function handle(ChatPush $event): void
    {
        $this->service->sendPush(
            $event->currentProfile,
            $event->toProfile,
            $event->chatId,
            $event->text,
        );
    }
}
