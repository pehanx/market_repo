<?php

namespace App\Listeners\Chat;

use App\Events\Chat\ChatSupportSend ;
use App\Helpers\Chat\ChatSupportService;

class ChatSupportDBListener
{
    public function __construct( ChatSupportService $service )
    {
        $this->service = $service;
    }

    public function handle(ChatSupportSend $event): void
    {        
        $this->service->writeData(
            $event->chatId,
            $event->text,
            $event->type,
        );
    }
}
