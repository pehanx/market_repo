<?php

namespace App\Listeners\Chat;

use App\Events\Chat\ChatDBSend;
use App\Helpers\Chat\ChatService;

class ChatDBListener
{
    public function __construct(ChatService $service)
    {
        $this->service = $service;
    }

    public function handle(ChatDBSend $event): void
    {
        $this->service->writeData(
            $event->currentProfile,
            $event->chatId,
            $event->text,
        );
    }
}
