<?php

namespace App\Listeners\Profile;

use App\Events\Profile\ProfileAdded;


class RoleAssignedListener
{
    public function handle(ProfileAdded $event): void
    {
        $user = $event->user;
        $userRoles = $user->getRoleNames()->toArray();
        
        // Удаляем базовую роль member
        if( in_array('member', $userRoles) )
        {
            $user->removeRole('member');
        }
        
        // Добавляем новую роль, если необходимо
        if( !in_array($event->profileType, $userRoles) )
        {
            $user->assignRole($event->profileType);
        }
    }
}
