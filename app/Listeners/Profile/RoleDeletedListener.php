<?php

namespace App\Listeners\Profile;

use App\Events\Profile\ProfileDeleted;


class RoleDeletedListener
{
    /**
     * Если профилей с таким типом у юзера больше нет, удаляем у него эту роль.
     *
     * @param App\Events\Profile\ProfileDeleted $event
     *
     */
    public function handle(ProfileDeleted $event): void
    {
        $user = $event->user;
        $profileType = $event->profile->profileTypes;
        $userRoles = $user->getRoleNames()->toArray();
        $hasRole = $user->profiles()->where('profile_type_id', $profileType->id)->first();
        
        if(!$hasRole && in_array($profileType->code, $userRoles))
        {
            $result = $user->removeRole($profileType->code);
        }
        
        // Добавляем роль member пользователю, если у него больше нет ролей.
        if(!$user->getRoleNames()->toArray())
        {
            $user->assignRole('member');
        }
        
        //app()->make(\Spatie\Permission\PermissionRegistrar::class)->forgetCachedPermissions();
    }
}
