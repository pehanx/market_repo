<?php

namespace App\Helpers\Shop;

class Seller
{
    public $id;
    public $profile;
    public $categories;
    public $localisations;
    public $currentLocalisation;
    public $topProducts;

    public function __construct($id, $profile, $categories, $localisations, $currentLocalisation, $topProducts)
    {
        $this->id = $id;
        $this->profile = $profile;
        $this->categories = $categories;
        $this->localisations = $localisations;
        $this->currentLocalisation = $currentLocalisation;
        $this->topProducts = $topProducts;
    }
}
