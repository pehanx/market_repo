<?php

namespace App\Helpers\Shop;

use App\User;
use App\Models\{Product, Category, Shop};

class ShopService
{
    public function userHasShop(User $user) :bool
    {
        return Shop::where('id', $user->id)->exists();
    }

    public function prepareLocalisation(& $localisationCode)
    {
        if ( !in_array($localisationCode, \Config::get('app.locales')) )
        {
            $localisationCode = \Config::get('app.fallback_locale');
        }
        
        return $localisationCode;
    }
    
    public function prepareDataForSeller($localisationId, $profiles, $user)
    {   
        $id = $user->id;
        $profile = null;
        $localisations = collect();
        $categories = null;
        $top_products = null;
 
        $profiles
            ->each(function ($item) use ($localisationId, & $profile, & $localisations, & $categories, & $top_products){
                        
                $localisation = $item->profileProperties->first()->localisations;
                
                $localisations->push($localisation);
                
                if($localisation->id === $localisationId)
                {
                    $categories = $item
                        ->products
                        ->whereNotNull('categories')
                        ->map(function ($item){
                            $item->categories->image = $item
                                ->downloads
                                ->where('type', 'detail_images')
                                ->first();
                                
                            if(!$item->categories->image)
                            {
                                $item->categories->image = $item
                                    ->downloads
                                    ->where('type', 'preview_image')
                                    ->first();
                            }
                            
                            $item->categories->shop_link = Product::SHOP_ROUTE_PREFIXES['index'] . Category::getFullLink($item->categories);
                
                            return $item->categories;
                        })
                        ->unique('id');
                    
                    $top_products = $item
                        ->products
                        ->take(8);
                        
                    $top_products->each(function (& $item) use($localisationId){
                        Product::setCorrectCharacteristics($item, $localisationId);
                    });
                    
                    $profile = $item
                        ->load('downloads')
                        ->unsetRelation('products');
                        
                    $profile->setRelation('downloads', $profile->downloads->groupBy('type'));
        
                    $youtubeLink = $profile->profileProperties->where('code', 'company_youtube_link')->first();
 
                    if($youtubeLink)
                    {
                        $params = [
                            'autoplay' => 0,
                            'loop' => 1,
                            'accelerometer' => 1,
                            'encrypted-media' => 1, 
                            'gyroscope' => 1, 
                            'picture-in-picture' => 1
                        ];
                        
                        $attributes = [
                            'width' => '100%',
                            'height' => '675',
                            'allow' => 'accelerometer'
                        ];
                        
                        $profile->youtube_frame = \LaravelVideoEmbed::parse($youtubeLink->value, ['YouTube'], $params, $attributes)->getEmbedCode();
                    }
                }
            });
            
        $localisations->transform(function ($item, $key) {
            $item->value = ucfirst($item->value);
            return $item;
        });
        
        return compact('id', 'profile', 'localisations', 'categories', 'top_products');
    }
    
    public function getProductIndexRoute(User $user, string $localisationCode, $link, $searchParams, $request)
    {
        return route(
            'shop.product.index', 
            array_filter([
                'user' => $user->id,
                'localisation' => $localisationCode,
                'category_path' => $link,
                'seller_search' => (!empty($searchParams['seller_search']) ? $searchParams['seller_search'] : null),
                'order_by' => ($request && $request->filled('order_by')) ? $request->get('order_by') : null
            ]
        ));
    }
}
