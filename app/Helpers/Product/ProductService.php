<?php

namespace App\Helpers\Product;

use App\User;
use App\Models\{Profile, Localisation, ProfileType, ProfileProperty, ProductPrice, Currency, Product, ProductModel};
use Illuminate\Support\{Str, Carbon};

class ProductService
{
    const ALLOWED_STATUSES = [
        'active',
        'inactive',
        'deleted',
    ];
    
    const ALLOWED_CHARACTERISTICS = [
        "available",
        "color",
        'min_order',
        "country",
        "weight",
        "lenght",
        "width",
        "height",
    ];
    
    const ALLOWED_ACTIONS = [
        'disable_checked',
        'delete_checked',
        'restore_checked',
        'destroy_checked',
        'activate_checked',
        'disable_all',
        'activate_all',
        'delete_active_all',
        'delete_unactive_all',
        'restore_deleted_all',
        'destroy_deleted_all',
    ];
    
    const ALLOWED_FILES = [
        'preview_image',
        'detail_images',
        'describle_images',
    ];
    
    public function create(Profile $profile, int $localisationId, array $request): Product
    {
        $product = $profile->products()->create([
            'articul' => rand(10000, 9999999),
            'category_id' => $request['category_id'],
            'link' => Str::slug($request['name'], '-'),
            'sort' => 1
        ]);

        unset($request['category_id']);

        //сохранение свойств (пока их в модели нет)
        if(isset($request['new_char']))
        {
            $newChars = array_combine($request['new_char']['name'], $request['new_char']['value']);

            foreach ($newChars as $key => $value)
            {
                if( !in_array($key, array_keys($request)) )
                {
                    $request[$key] = $value;
                }
            }
        }
        
        unset($request['new_char']);

        // сохранение цена на продукт
        $product->productPrices()->create([
            'currency_id' => Currency::where('is_base', true)->first()->id,
            'is_parent' => true, // Признак того, что характеристика относиться к товару а не к торговому предложению
            'min' => $request['min_price'],
            'max' => $request['max_price'] ?? 0,
        ]);

        

        unset($request['min_price'], $request['max_price']);

        //сохранение характеристик, в дальнейшем используемых при фильтрации
        $product->characteristics()->createMany( $this->prepareCharacteristics($request, (int)$profile->id) );

        //сохранение свойств
        $product->productProperties()->createMany($this->prepareProperties($request, $localisationId) );
        
        return $product;
    }
    
    public function createModel(Profile $profile, Product $product, int $localisationId, array $request): ProductModel
    {
        $articulPrefix = ProductModel::getPrefixForArticul($product->id);
        
        $productModel = $product->productModels()->create([
            'articul' => $articulPrefix,
            'sort' => 1
        ]);
        
        unset($articulPrefix);
        
        //сохранение характеристик, в дальнейшем используемых при фильтрации
        $productModel->characteristics()->createMany( $this->prepareCharacteristics($request, (int)$profile->id) ); 
        
        // сохранение цена на продукт
        $productModel->productPrices()->create([
            'currency_id' => Currency::where('is_base', true)->first()->id,
            'min' => $request['min_price'],
            'max' => $request['max_price'] ?? 0,
        ]);
        
        unset($request['min_price'], $request['max_price']);

        $productModel->productProperties()->createMany($this->prepareProperties($request, $localisationId, $product->id) );
        
        return $productModel;
    }
    
    private function prepareCharacteristics(array &$data, $profileId): array
    {
        $correctData = [];

        foreach ($data as $code => $value)
        {

            if( $value && !in_array($code, ['name', 'description', 'seo_title', 'seo_keywords', 'seo_descriptions']) )
            {
                $correctValue = (is_array($value) && array_key_exists('value', $value)) ? $value['value'] : $value;

                if(!$correctValue)
                {
                    unset($data[$code]);
                    continue;
                }
                
                array_push($correctData, [
                    'code' => $code,
                    'value' => $correctValue,
                    'profile_id' => $profileId,
                    'type' => $value['type'] ?? '',
                ]);
                
                unset($data[$code]);
            }
        }
        //dump($correctData);exit();
        return $correctData;
    }
    
    private function prepareProperties(array $data, $localisationId, $productId = null): array
    {
        $correctData = [];
        
        foreach ($data as $code => $value)
        {
            if(!$value)
            {
                continue;
            }
            
            // Свойства товара
            if(!$productId)
            {
                array_push($correctData, [
                    'localisation_id' => $localisationId,
                    'code' => $code,
                    'value' => $value,
                    'is_parent' => true, // Признак того, что характеристика относиться к товару а не к торговому предложению
                ]);
            
            // Свойства торгового предложения (модели товара)
            }else
            {
                array_push($correctData, [
                    'localisation_id' => $localisationId,
                    'code' => $code,
                    'value' => $value,
                ]);
            }
        }
        
        return $correctData;
    }
    
    public function update(Profile $profile, Product $product, int $localisationId, array $request): Product
    {
        $product->update([
            'category_id' => $request['category_id'],
            'link' => Str::slug($request['name'], '-'),
            'sort' => 1
        ]);

        unset($request['articul'], $request['category_id']);
        
        $product->characteristics()->delete(); 
        $product->characteristics()->createMany( $this->prepareCharacteristics($request, (int)$profile->id) ); 

        $product->productPrices()->update([
            'min' => $request['min_price'],
            'max' => $request['max_price'] ?? 0,
        ]);

        unset($request['min_price'], $request['max_price']);
        
        $product->productProperties()->where('localisation_id', $localisationId)->delete();
        $product->productProperties()->createMany($this->prepareProperties($request, $localisationId) );
        
        $message = [
            'title' => __(''),
            'body' => __('notification.save_changes_success'),
            'type' => 'info'
        ];
        
        \Session::flash('message', $message);
        
        return $product; 
    }
    
    public function updateModel(Profile $profile, ProductModel $model, int $localisationId, array $request): ProductModel
    {
        $model->characteristics()->delete(); 
        $model->characteristics()->createMany( $this->prepareCharacteristics($request, (int)$profile->id) ); 

        $model->productPrices()->update([
            'min' => $request['min_price'],
            'max' => $request['max_price'] ?? 0,
        ]);

        unset($request['min_price'], $request['max_price']);
        
        $model->productProperties()->where('localisation_id', $localisationId)->delete();
        $model->productProperties()->createMany($this->prepareProperties($request, $localisationId) );
        
        $message = [
            'title' => __(''),
            'body' => __('notification.save_changes_success'),
            'type' => 'info'
        ];
        
        \Session::flash('message', $message);
        
        return $model; 
    }
    
    /**
     * Удаляем товар с его торговыми предложениями, не вызывая событие в boot
     *
     * @param App\Models\Profile $profile
     * @param int $productId
     *
     * @return void
     */
    public function delete(Profile $profile, int $productId): void
    {
        $profile->products()->findOrFail($productId);      

        Product::where('id', $productId)->update(['is_active' => false]);
        Product::where('id', $productId)->delete();
        ProductModel::where('product_id', $productId)->delete();
    }
    
    /**
     * Удаляем торговое предложение, не вызывая событие в boot
     *
     * @param App\Models\Profile $profile
     * @param int $productId
     * @param int $modelId
     *
     * @return void
     */
    public function deleteModel(Profile $profile, int $productId, int $modelId): void
    {
        $profile->products()->findOrFail($productId);   

        ProductModel::where('id', $modelId)->delete();
    }
    
    /**
     * Отключаем товар
     *
     * @param App\Models\Profile $profile
     * @param int $productId
     *
     * @return void
     */
    public function disable(Profile $profile, int $productId): void
    {
        $product = $profile->products()->findOrFail($productId);   

        $product->is_active = false;
        $product->save();
    }
    
    /**
     * Активируем товар
     *
     * @param App\Models\Profile $profile
     * @param int $productId
     *
     * @return void
     */
    public function activate(Profile $profile, int $productId): void
    {
        $product = $profile->products()->findOrFail($productId);   

        $product->is_active = true;
        $product->save();
    }
    
    /**
     * Восстанавливаем товар со всеми его торговыми предложениями из удаленных
     *
     * @param App\Models\Profile $profile
     * @param int $productId
     *
     * @return void
     */
    public function restore(Profile $profile, int $productId)
    {
        $product = $profile->products()->onlyTrashed()->findOrFail($productId);
        
        $product->is_active = true; // Из удаленных восстанавливаем в активные
        $product->restore();

        $product->productModels()
            ->onlyTrashed()
            ->restore();
    }
    
    /**
     * Восстанавливаем торговое предложение из удаленных
     *
     * @param App\Models\Profile $profile
     * @param int $productId
     * @param int $modelId
     *
     * @return void
     */
    public function restoreModel(Profile $profile, int $productId, int $modelId)
    {
        $profile->products()->withTrashed()->findOrFail($productId);
        
        ProductModel::onlyTrashed()->where('id', $modelId)->restore();
    }
    
    /**
     * Помечаем как мусор товар с его торговыми предложениями 
     *
     * @param App\Models\Profile $profile
     * @param int $productId
     *
     * @return void
     */
    public function destroy(Profile $profile, int $productId)
    {
        $product = $profile->products()->onlyTrashed()->findOrFail($productId);
        
        $product->destroyed_at = Carbon::now();
        $product->save();
        
        $product->productModels()
            ->withTrashed()
            ->update(['destroyed_at' => $product->destroyed_at]);
    }
    
    /**
     * Помечаем как мусор торговое предложение
     *
     * @param App\Models\Profile $profile
     * @param int $productId
     *
     * @return void
     */
    public function destroyModel(Profile $profile, int $productId, int $modelId)
    {
        $product = $profile->products()->withTrashed()->findOrFail($productId);

        $model = $product->productModels()->onlyTrashed()->find($modelId);

        $model->destroyed_at = Carbon::now();
        $model->save();
    }
    
    /**
     * Отключаем активные товары
     *
     * @param App\Models\Profile $profile
     *
     * @return void
     */
    public function disableProducts(Profile $profile)
    {
        $profile->products()->where('is_active', true)->update(['is_active' => false]);
    }
    
    /**
     * Активируем отключенные товары
     *
     * @param App\Models\Profile $profile
     *
     * @return void
     */
    public function activateProducts(Profile $profile)
    {
        $profile->products()->where('is_active', false)->update(['is_active' => true]);
    }
    
     /**
     * Удаляем активные товары вместе с торговыми предложениями
     *
     * @param App\Models\Profile $profile
     *
     * @return void
     */
    public function deleteActiveProducts(Profile $profile)
    {
        $profile->products()->where('is_active', true)->delete();
        ProductModel::whereIn('product_id', $profile->products()->onlyTrashed()->where('is_active', true)->pluck('id')->toArray() )->delete();
        $profile->products()->onlyTrashed()->where('is_active', true)->update(['is_active' => false]);
    }
    
     /**
     * Удаляем отключенные товары с торговыми предложениями
     *
     * @param App\Models\Profile $profile
     *
     * @return void
     */
    public function deleteUnactiveProducts(Profile $profile)
    {
        $profile->products()->where('is_active', false)->delete();
        ProductModel::whereIn('product_id', $profile->products()->onlyTrashed()->where('is_active', false)->pluck('id')->toArray() )->delete();
    }
    
     /**
     * Восстанавливаем уделенные товары с торговыми предложениями и отдельные торговые предложения
     *
     * @param App\Models\Profile $profile
     *
     * @return void
     */
    public function restoreDeletedProducts(Profile $profile)
    {
        $hasManyProducts = $profile->products();
        ProductModel::whereIn('product_id', $hasManyProducts->withTrashed()->pluck('id')->toArray())->onlyTrashed()->restore();
        $hasManyProducts->onlyTrashed()->update(['is_active' => true]);
        $hasManyProducts->onlyTrashed()->restore();
    }
    
     /**
     * Помечаем как мусор уделенные товары с торговыми предложениями и отдельные торговые предложения
     *
     * @param App\Models\Profile $profile
     *
     * @return void
     */
    public function destroyDeletedProducts(Profile $profile)
    {
        $dateTime = Carbon::now();
        $hasManyProducts = $profile->products();
        ProductModel::whereIn('product_id', $hasManyProducts->withTrashed()->pluck('id')->toArray())->onlyTrashed()->update(['destroyed_at' => $dateTime]);
        $hasManyProducts->onlyTrashed()->update(['destroyed_at' => $dateTime]);
    }
}
