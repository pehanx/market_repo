<?php

namespace App\Helpers\Product;

use App\ApplicationDictionary;
use Illuminate\Http\File;
use App\Helpers\Product\ProductService;
use App\Extensions\Service as BaseService;
use Illuminate\Support\Str;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Style\Border;
use App\Models\{
    Import,
    Product,
    ProductProperty,
    ProductModel,
    Category,
    ProductPrice,
    Profile,
    Localisation,
    Currency,
    User,
};

class ImportProductService extends BaseService
{

    const PRODUCT_IMAGE_PATH = 'public/products/';
    const IMPORT_PATH = 'public/imports/';

    const ALLOWED_MIME_TYPES = [
        'image/jpg',
        'image/png',
        'image/jpeg',
        'text/plain',
        'text/csv',
        'application/vnd.ms-excel',
        'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
    ];

    //Колонки в которых значение должно быть числом
    const INTEGER_COLUMNS = [
        'articul',
        'min_price',
        'max_price',
        'min_order',
        'category',
        'weight',
        'lenght',
        'width',
        'height'
    ];

    //Значения, которые существуют у модели товара
    const MODEL_PROPS = [
        'min_price',
        'max_price',
        'preview_image',
        'available',
        'available_type',
        'color',
        'preview_image',
    ];

    //Вспомогательные значения и изображения
    //игнорируются при создании товара или модели, чтобы они не записались в properties
    const UNUSED_PROPERTIES = [
        'available_type',
        'weight_type',
        'size_type',
        'describle_images',
        'preview_image',
        'detail_images',
    ];

    //Значения по умолчанию для некоторых колонок
    const DEFAULT_CATEGORY = 2;
    const DEFAULT_COLOR = '#000000';
    const DEFAULT_COUNTRY = 'RU'; 
    const DEFAULT_WEIGHT = 'gr'; 
    const DEFAULT_SIZE = 'mm'; 
    const DEFAULT_QUANTITY = 'pcs'; 

    private $profile; //Профиль
    private $importModel; //Модель импорта
    private $localisationId; //Id локализации

    private $tableHeaders; //Заголовки импортируемой таблицы
    private $tableData = []; //Данные таблицы
    private $productData = []; //Массив для создания продукта
    private $errors = []; //Массив для хранения ошибок
    private $tableModelData = []; //Данные моделей товара
    private $quantities; //Тип измерения количества товара
    
    private $className; //Вспомогательная переменная, нужна для того чтобы выбирать нужный класс в зависимости от расширения
    private $tableFile; //Файл таблицы
    private $importFolder; //Папка импорта, нужна чтобы добавлять изображения в базу (Надо скопировать их из неё в request_product)

    /**
     * Названия столбцов
     */
    public function allowedHeaders()
    {
        return [
            'articul' => __('cabinet.import.articul'),
            'name' => __('cabinet.import.name'),
            'description' => __('cabinet.import.description'),
            'country' => __('cabinet.import.country'),
            'category' => __('cabinet.import.category'),
            'min_price' => __('cabinet.import.min_price'),
            'max_price' => __('cabinet.import.max_price'),
            'min_order' => __('cabinet.import.min_order'),
            'available' => __('cabinet.import.available'),
            'available_type' => __('cabinet.import.available_type'),
            'color' => __('cabinet.import.color'),
            'describle_images' => __('cabinet.import.describle_images'),
            'preview_image' => __('cabinet.import.preview_image'),
            'detail_images' => __('cabinet.import.detail_images'),
            'youtube' => __('cabinet.import.youtube'),
            'weight' => __('cabinet.import.weight'),
            'weight_type' => __('cabinet.import.weight_type'),
            'lenght' => __('cabinet.import.lenght'),
            'width' => __('cabinet.import.width'),
            'height' => __('cabinet.import.height'),
            'size_type' => __('cabinet.import.size_type'),
            'seo_title' => __('cabinet.import.seo_title'),
            'seo_keywords' => __('cabinet.import.seo_keywords'),
            'seo_description' => __('cabinet.import.seo_description')
        ];
    }

    /**
     * Столбцы обязательные для заполнения и с переводом ошибок
     */
    public function requiredHeaders()
    {
        return [
            'articul' => __('cabinet.import.articul_required'),
            'name' => __('cabinet.import.name_required'),
            'description' => __('cabinet.import.description_required'),
            'country' => __('cabinet.import.country_required'),
            'category' => __('cabinet.import.category_required'),
            'min_price' => __('cabinet.import.price_min_required'),
            'max_price' => __('cabinet.import.price_max_required'),
            'available' => __('cabinet.import.available_required'),
            'min_order' => __('cabinet.import.min_order_required'),
            'preview_image' => __('cabinet.import.preview_image_required'),
        ];
    } 

    /**
     * Получение дополнительных настроек импорта для товаров
     */
    public function settingsForProducts(Profile $profile, int $importId) :array
    {
        $import = Import::find($importId);
        $importTable = $import::findImportTable($profile, $import);

        $importTable = \Storage::disk( env('FILESYSTEM_DRIVER') )
            ->path(
                'public/' . $importTable
            );

        $tableHeadings = $this->getHeaders($importTable);
        $allowedColumns = $this->allowedHeaders();
        $localisationId = Localisation::where('code', app('translator')->getLocale() )->first()->id;

        $quantities = \App\ApplicationDictionary::where([
            ['group_code', 'quantities'],
            ['localisation_id', $localisationId]
            ])
            ->get();

        return [
            'quantities' => $quantities,
            'allowedColumns' => $allowedColumns,
            'tableHeadings' => $tableHeadings
        ];
    }

    /**
     * Получение настроек импорта для запросов
     */
    public function saveFiles($importArchive, $tableFile, Profile $profile) :array
    {
        $this->profile = $profile;
        //Убираем из архива файлы, которые не подходят по формату

        $zip = new \ZipArchive();
        $zip->open($importArchive->path());

        if($zip->numFiles === null)
        {
            return redirect()->back()->withErrors(['empty_archive' => __('cabinet.import.empty_archive') ]);
        }

        $this->importModel = new Import;
        $this->importModel->profile_id = $this->profile->id;
        $this->importModel->progress = 0;
        $this->importModel->save();

        // Перенос файлов из временной дирректории в директорию с импортами
        $this->importFolder = \Storage::disk(env('FILESYSTEM_DRIVER'))
            ->path(self::IMPORT_PATH. $this->profile->id ."/". $this->importModel->id);

        $zip->extractTo($this->importFolder);

        $tableName = $tableFile->getClientOriginalName();
        copy($tableFile, $this->importFolder. '/' .$tableName);

        $tablePath = $this->importFolder. '/' . $tableName;

        // Получение массива с изображениями
        $images = $this->filterFilesArchive();

        if ($images !== [])
        {
            $this->prepareImportFilesForDB($tableName, $images);
        }
        $tableHeadings = $this->getHeaders($tablePath);

        // Если заголовков из таблицы меньше чем должно быть
        if(count($tableHeadings) < count($this->requiredHeaders()))
        {
            $this->importModel->delete();
            return ['errors' => ['few_columns' => __('cabinet.import.few_columns') ] ];
        }

        return ['import_id' => $this->importModel->id ];
        
    }  

    /* 
     * Метод для получения заголовков из таблицы
     */
    public function getHeaders(string $tableFile) :array
    {
        $file = new File($tableFile);
        
        $className = 'PhpOffice\PhpSpreadsheet\Reader\\' .  ucfirst($file->getExtension());

        if(class_exists($className))
        {
            $reader = new $className;
            $reader->setReadDataOnly(true);
            $spreadsheet = $reader->load($tableFile);

            $lastColumn = $spreadsheet->getActiveSheet()->getHighestColumn();

            $headerArray = [];

            foreach (range('A',$lastColumn) as $columnIndex)
            {
                if( $spreadsheet->getActiveSheet()->getCell($columnIndex . 1)->getValue() !== null)
                {
                    $headColumn = strip_tags(
                        $spreadsheet->getActiveSheet()->getCell($columnIndex . 1)->getValue()
                    );

                    $headerArray[$columnIndex] = $headColumn;

                    unset($headColumn);
                }
            }

            return $headerArray;

        } else
        {
            return null;
        }
    }

    /**
     * Получение файла со списком категорий, цветов и стран
     */
    public function getExampleLists()
    {
        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();

        //Стили для заголовков таблицы
        $styleArray = [
            'alignment' => [
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER
            ],
            'font' => [
                'bold' => true,
            ]
        ];

        $colors = [];
        $categories = [];
        $countries = [];

        $localisation = Localisation::where('code', app('translator')->getLocale())->first();

        Category::where('is_active', true)->where('id','>', 1)
            ->with(['categoryProperties' =>  function ($query) use($localisation) {
                $query
                    ->where([
                        ['code', 'name'],
                        ['localisation_id', $localisation->id],
                    ]);
            }])->orderBy('id')
            ->get()
            ->mapWithKeys(function ($item) use (&$categories) {

                array_push($categories, [
                    'id' => $item->id,
                    'name' => $item->categoryProperties->first()->value
                ]);

                return[];
            });

        $colors = $this->getExampleEntity($colors, 'colors', $localisation->id);

        $countries = $this->getExampleEntity($countries, 'countries', $localisation->id);

        // Шапка таблицы
        foreach ( $this->exampleListHeaders() as $key => $value)
        {
            $sheet->setCellValue($key, $value)->getStyle($key)->getFont()->setBold(true);
        }

        //Список с категориями
        $sheet->mergeCells('A1:B1')->setCellValue('A1', __('cabinet.import.category_list'))->getStyle($key)->getFont()->setBold(true);

        $sheet->fromArray($categories, null, 'A3', false);
        $sheet->getColumnDimension('A')->setWidth(10);
        $sheet->getColumnDimension('B')->setWidth(45);

        //Список цветов
        $sheet->mergeCells('D1:F1')->setCellValue('D1', __('cabinet.import.color_list'))->getStyle($key)->getFont()->setBold(true);
        
        $sheet->fromArray($colors, null, 'D3', false);
        $sheet->getColumnDimension('D')->setWidth(15);
        $sheet->getColumnDimension('E')->setWidth(25);
        $sheet->getColumnDimension('F')->setWidth(10);

        //Окрашивание ячеек
        for ($i=0; $i < count($colors); $i++)
        { 
            $sheet->getStyle('F'.($i+3))->getFill()
                ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                ->getStartColor()
                ->setRGB( str_replace('#','',$colors[$i]['code']) );
        }
        
        //Список стран
        $sheet->mergeCells('H1:I1')->setCellValue('H1', __('cabinet.import.country_list'))->getStyle($key)->getFont()->setBold(true);

        $sheet->fromArray($countries, null, 'H3', false);
        $sheet->getColumnDimension('H')->setWidth(10);
        $sheet->getColumnDimension('I')->setWidth(45);

        $sheet->getDefaultRowDimension()->setRowHeight(20);
        $sheet->getStyle('A1:B1')->applyFromArray($styleArray);
        $sheet->getStyle('D1:F1')->applyFromArray($styleArray);
        $sheet->getStyle('H1:I1')->applyFromArray($styleArray);

        unset($styleArray);

        $writer = new \PhpOffice\PhpSpreadsheet\Writer\Xls($spreadsheet);

        $writer->save(\Storage::disk( env('FILESYSTEM_DRIVER') )->path( self::IMPORT_PATH  . "$localisation->code/example_list.xls"));

        return response()->download(\Storage::disk( env('FILESYSTEM_DRIVER') )->path(self::IMPORT_PATH  . "$localisation->code/example_list.xls"));
    }

    /**
     * Создание массива из сущности для записи в файл
     */
    private function getExampleEntity(array $entity, string $entityCode, int $localisationId) : array
    {
        ApplicationDictionary::where('group_code', $entityCode)
            ->where('localisation_id', $localisationId)
            ->orderBy('id')
            ->get()
            ->mapWithKeys(function ($item) use (&$entity) {

                array_push($entity, [
                    'code' => $item->code,
                    'name' => $item->value
                ]);

                return[];
            });
        return $entity;
    }

    /*
     * Метод для записи в таблицу файлов импорта
     */
    private function associateImportFiles(array $importFiles)
    {
        foreach ($importFiles['table'] as $importTable)
        {
            $this->associateWithFiles( $this->importModel,
                [new File($importTable)],
                'import_table',
                $this->profile->users()->first()->id
            );
        }

        foreach ($importFiles['images'] as $importImage)
        {
            $this->associateWithFiles( $this->importModel,
                [new File($importImage)],
                'import_image',
                $this->profile->users()->first()->id
            );
        }

    }

    /*
     * Точка входа для джобика после выбора правильных параметров
     */
    public function start(array $settingsArray, int $profileId)
    {
        $this->profile = Profile::find($profileId);
        $this->importModel = Import::find($settingsArray['importId']);
        $this->importFolder = self::IMPORT_PATH . $this->profile->id. '/' . $this->importModel->id . '/';
        $this->localisationId = $this->profile->profileProperties()->first()->localisation_id;

        $this->tableHeaders = $settingsArray['headings'];
        
        $this->headerFilter();

        $this->tableFile = new File(
            \Storage::disk(env('FILESYSTEM_DRIVER'))
                ->path("public/". $settingsArray['tableName'] )
        );

        $this->className = 'PhpOffice\PhpSpreadsheet\Reader\\' .  ucfirst($this->tableFile->getExtension());

        if(class_exists($this->className))
        {
            $this->extractData();
            $this->writeData();

        } else
        {
            return abort(400);
        }
    }

    /*
     * Открытие импортируемой таблицы и получение данных оттуда
     */
    private function extractData()
    {
        $reader = new $this->className;
        $reader->setReadDataOnly(true);
        $spreadsheet = $reader->load($this->tableFile->getRealPath());

        $this->lastRow = $spreadsheet->getActiveSheet()->getHighestRow();
        $this->lastColumn = $spreadsheet->getActiveSheet()->getHighestColumn();

        $row = 2; //Номер строки начинается с двойки, потому что в первой строке заголовки

        while ($row <= $this->lastRow)
        {
            foreach (range('A', $this->lastColumn) as $columnIndex)
            {
                $this->tableData[$row][$columnIndex] = strip_tags(
                    $spreadsheet->getActiveSheet()
                        ->getCell($columnIndex . $row)
                        ->getValue()
                );
            }

            $this->checkDoubles($row);

            $row++;
        }
        unset($row);
    }

    /*
     * Создание товара и моделей из данных, полученных с таблицы
     */
    private function writeData()
    {
        $countTableData = count($this->tableData); //Исходное количество данных в таблице

        foreach ($this->tableData as $rowNumber => $tableData)
        {
            $this->productData = []; //Каждый раз обнуляется массив с данными

            if( $this->checkRequiredHeaders($tableData) !== false && $this->checkTypeOfData($tableData) !== false) //Проверка на то, заполнены ли обязательные колонки и проверка на их тип
            {
                //Готовим массив для создания товара из всех возможных заголовков
                foreach ($this->allowedHeaders() as $key => $value)
                {
                    $this->filterArray($key, $tableData);
                }

                \DB::transaction( function() use($tableData)
                {
                    $this->productModel = $this->createProduct();
                    
                    $this->writePreviewImage($tableData);

                    if( isset($tableData[ $this->tableHeaders['detail_images'] ])
                        && $tableData[ $this->tableHeaders['detail_images'] ] !== null
                        && is_string($tableData[ $this->tableHeaders['detail_images'] ]) !== false
                        )
                    {
                        $this->writeDetailImages($tableData[ $this->tableHeaders['detail_images'] ]);
                    }

                    if( isset($tableData[ $this->tableHeaders['describle_images'] ])
                        && $tableData[ $this->tableHeaders['describle_images'] ] !== null
                        && is_string($tableData[ $this->tableHeaders['describle_images'] ]) !== false
                        )
                    {
                        $this->writeDescribleImages($tableData[ $this->tableHeaders['describle_images'] ]);
                    }

                });

            } else
            {
                array_push($this->errors, $rowNumber);
            }

            $progress = round((count($this->tableData) / $countTableData * 100), 2); // Запись процентов
            $this->importModel->progress = $progress;
            $this->importModel->error_rows = $this->errors;
            $this->importModel->update();
            unset($tableData);
        }
    }

    /*
     * Получение массива изображений из колонки деталки
     * копирование изображений в папку запросов
     * запись в downloads
     */
    private function writeDetailImages(string $imageRow)
    {
        $detailArray = [];
        $detailImages = explode(';', $imageRow); //Колонку с названиями изображений разделяем по знаку

        foreach ($detailImages as $detailImage)
        {
            $imagePath = realpath(\Storage::disk(env('FILESYSTEM_DRIVER'))
                ->path($this->importFolder . $detailImage));
                
            if( is_file($imagePath) && basename($imagePath) !== null ) //Проверка на существование такого изображения в архиве
            {
                $imageFile = new File($imagePath);

                if($imageFile->extension() === 'png'
                    || $imageFile->extension() === 'jpg'
                    || $imageFile->extension() === 'jpeg')
                {
                    $newPath =  \Storage::disk(env('FILESYSTEM_DRIVER'))
                        ->path(
                            self::PRODUCT_IMAGE_PATH . $this->profile->id . '/'
                            . \Str::random(10) . '.' . $imageFile->extension()
                        );
                    
                    copy( $imagePath, $newPath);

                    $imageFile = new File($newPath);
                    unset($newPath);

                    array_push($detailArray, $imageFile);
                }
                unset($imageFile);
            }
            unset($imagePath);
        }
        if($detailArray !== null)
        {
            $this->associateWithFiles($this->productModel,
                $detailArray,
                'detail_images',
                $this->profile->users()->first()->id
            );
        }
    }

    /*
     * Получение массива изображений для Describle Image
     * копирование изображений в папку запросов
     * запись в downloads
     */
    private function writeDescribleImages(string $imageRow)
    {
        $sliderArray = [];
        $detailImages = explode(';', $imageRow); //Колонку с названиями изображений разделяем по знаку

        foreach ($detailImages as $detailImage)
        {
            $imagePath = realpath(\Storage::disk(env('FILESYSTEM_DRIVER'))
                ->path($this->importFolder . $detailImage));
                
            if( is_file($imagePath) && basename($imagePath) !== null ) //Проверка на существование такого изображения в архиве
            {
                $imageFile = new File($imagePath);

                if( $imageFile->extension() === 'png'
                    || $imageFile->extension() === 'jpg'
                    || $imageFile->extension() === 'jpeg')
                {
                    $newPath =  \Storage::disk(env('FILESYSTEM_DRIVER'))
                        ->path(
                            self::PRODUCT_IMAGE_PATH . $this->profile->id . '/'
                            . \Str::random(10) . '.' . $imageFile->extension()
                        );
                    
                    copy( $imagePath, $newPath);

                    $imageFile = new File($newPath);
                    unset($newPath);

                    array_push($sliderArray, $imageFile);
                }
                unset($imageFile);
            }
            unset($imagePath);
        }
        if($sliderArray !== null)
        {
            $this->associateWithFiles($this->productModel,
                $sliderArray,
                'describle_images',
                $this->profile->users()->first()->id
            );
        }
    }

    /*
     * Проверка изображения из колонки превью
     * копирование изображения в папку запросов
     * запись в downloads
     */
    private function writePreviewImage(array $tableData) :bool
    {
        $previewImage = realpath(\Storage::disk(env('FILESYSTEM_DRIVER'))
            ->path($this->importFolder . $tableData[ $this->tableHeaders['preview_image'] ]));

        $imageFile = new File($previewImage);
        
        if(!is_file($imageFile))
        {
            return false;
        }

        if( $imageFile->extension() === 'png'
            || $imageFile->extension() === 'jpg'
            || $imageFile->extension() === 'jpeg')
        {
            $newPath = \Storage::disk( env('FILESYSTEM_DRIVER') )
                ->path(
                    self::PRODUCT_IMAGE_PATH .  $this->profile->id . '/'
                    . \Str::random(10) . '.' . $imageFile->extension()
                );

            copy( $previewImage, $newPath);
            unset($previewImage);

            $this->previewImage = new File($newPath);
            unset($newPath); 

            $this->associateWithFiles($this->productModel,
                [ $this->previewImage ],
                'preview_image',
                $this->profile->users()->first()->id
            );
        }

        return true;

        unset($imageFile);
    }

    /*
     * Метод содержит проверку на обязательные заголовки
     */
    private function checkRequiredHeaders(array $tableData) : bool
    {
        $requiredHeaders = $this->requiredHeaders();

        foreach ($requiredHeaders as $key => $value)
        {
            if( !isset($tableData[ $this->tableHeaders[$key] ]) )
            {
                return false;
            }
        }

        // Если превью заполнено в таблице, но не существует этого файла
        $previewImage = realpath(\Storage::disk(env('FILESYSTEM_DRIVER'))
            ->path($this->importFolder . $tableData[ $this->tableHeaders['preview_image'] ]));
        
        if( !is_file($previewImage) )
        {
            return false;
        }

        return true;    
    }

    /*
     * Метод содержит проверку на типы данных
     */
    private function checkTypeOfData(array $tableData) : bool
    {
        $errorsArray = [];
        
        foreach (self::INTEGER_COLUMNS as $integerColumn)
        {
            if( !in_array( $integerColumn, array_keys($this->requiredHeaders())) ) //Если значение не обязательное
            {
                if(isset($this->tableHeaders[$integerColumn]) && isset($tableData[ $this->tableHeaders[$integerColumn] ]) && $tableData[ $this->tableHeaders[$integerColumn] ] !== null)
                {
                    if( filter_var($tableData[ $this->tableHeaders[$integerColumn] ], FILTER_SANITIZE_NUMBER_INT) !== "")
                    {
                        $tableData[ $this->tableHeaders[$integerColumn] ] = (int)filter_var($tableData[ $this->tableHeaders[$integerColumn] ], FILTER_SANITIZE_NUMBER_INT);
                    
                    }
                }

            } elseif(isset($tableData[ $this->tableHeaders[$integerColumn] ]) && in_array( $integerColumn, array_keys($this->requiredHeaders())) )
            {
                if( filter_var($tableData[ $this->tableHeaders[$integerColumn] ], FILTER_SANITIZE_NUMBER_INT) !== "")
                {
                    $tableData[ $this->tableHeaders[$integerColumn] ] = (int)filter_var($tableData[ $this->tableHeaders[$integerColumn] ], FILTER_SANITIZE_NUMBER_INT);
                
                } else 
                {
                    array_push($errorsArray, $tableData[ $this->tableHeaders[$integerColumn] ]);
                }
            }

        }

        if ($errorsArray === [])
        {
            return true;
        }

        return false;
    }

    /*
     * Метод содержит проверку на дубли
     */
    private function checkDoubles(int $row) :bool
    {
        foreach ($this->tableData as $key => $value)
        {
            if( $this->tableData[$row] === $this->tableData[$key] && $row !== $key)
            {
                unset($this->tableData[$row]);
                return false;
            }
        }
        return true; 
    }

    /*
     * Метод исправляет содержимое заголовков
     */
    private function headerFilter()
    {
        foreach ($this->tableHeaders as $key => $value)
        {
            $this->tableHeaders[$key] = strip_tags($value);
        }
    }
    
    /**
     * Создание записей в таблице downloads для модели импорта
     */
    private function prepareImportFilesForDB( string $tableName, array $images)
    {
        $importFiles = [
            'table' => [],
            'images' => [],
        ];

        array_push( $importFiles['table'], new File($this->importFolder . '/' . $tableName));

        foreach ($images as $image)
        {
            array_push( $importFiles['images'], new File($this->importFolder . '/' . $image));
        }

        $this->associateImportFiles($importFiles);
    }

    /**
     * Фильтрация файлов из архива
     */
    private function filterFilesArchive() :array
    {
        $filesFolder = scandir($this->importFolder);
        $images = [];

        foreach ($filesFolder as $file)
        {
            if( is_file($this->importFolder . '/'. $file) )
            {
                $mimeType = (new File($this->importFolder . '/'. $file))->getMimeType();
                if(in_array($mimeType, self::ALLOWED_MIME_TYPES)) //Доп проверка на mime тип
                {
                    array_push($images, $file);

                } else
                {
                    unlink($this->importFolder . '/'. $file);
                }
                unset($mimeType);
            }
        }

        return $images;
    }

    /*
     * Создание товара
     */
    private function createProduct()
    {
        if($this->profile->products()->where('articul',$this->productData['articul'])->first() !== null)
        {
            $product = Product::where('articul', $this->productData['articul'])->first();
            return $this->createModel($product);
        }

        $product = $this->profile->products()->create([
            'articul' => $this->productData['articul'],
            'category_id' => $this->productData['category_id'],
            'link' => Str::slug($this->productData['name'], '-'),
            'sort' => 1
        ]);

        unset($this->productData['articul'], $this->productData['category_id']);
        
        //сохранение характеристик, в дальнейшем используемых при фильтрации
        $product->characteristics()->createMany( $this->prepareCharacteristics() ); 
        
        // сохранение цена на продукт
        $product->productPrices()->create([
            'currency_id' => Currency::where('is_base', true)->first()->id,
            'is_parent' => true, // Признак того, что характеристика относиться к товару а не к торговому предложению
            'min' => (int) $this->productData['min_price'] ?? 1,
            'max' => (int) $this->productData['max_price'] ?? 1,
        ]);
        
        unset($this->productData['min_price'], $this->productData['max_price']);
        
        //сохранение свойств
        $product->productProperties()->createMany($this->prepareProperties() );

        return $product;
    }

    /*
     * Создание модели товара
     */
    private function createModel(Product $product) : ProductModel
    {
        $articulPrefix = ProductModel::getPrefixForArticul($product->id);

        $productModel = $product->productModels()->create([
            'articul' => "-{$articulPrefix}",
            'sort' => 1
        ]);
        
        unset($articulPrefix, $this->productData['category_id']);

        foreach ($this->allowedHeaders() as $unsetHeader => $value)
        {
            if( !in_array($unsetHeader, self::MODEL_PROPS) )
            {
                unset($this->productData[$unsetHeader]);
            }
        }
        //сохранение характеристик, в дальнейшем используемых при фильтрации
        $productModel->characteristics()->createMany( $this->prepareCharacteristics() ); 
        
        // сохранение цена на продукт
        $productModel->productPrices()->create([
            'currency_id' => Currency::where('is_base', true)->first()->id,
            'min' => (int) $this->productData['min_price'] ?? 1,
            'max' => (int) $this->productData['max_price'] ?? 1,
        ]);
        
        unset($this->productData['min_price'], $this->productData['max_price']);
        
        //сохранение свойств (пока их в модели нет)
        $productModel->productProperties()->createMany($this->prepareProperties($product->id) );
        
        return $productModel;
    }

    /*
     * Работает так же как и метод с ProductService
     */
    private function prepareCharacteristics(): array
    {
        $correctData = [];

        foreach ($this->productData as $code => $value)
        {
            if($value && in_array($code, ProductService::ALLOWED_CHARACTERISTICS))
            {
                $correctValue = (is_array($value) && array_key_exists('value', $value)) ? $value['value'] : $value;

                if(!$correctValue)
                {
                    unset($this->productData[$code]);
                    continue;
                }
                
                array_push($correctData, [
                    'code' => $code,
                    'value' => $correctValue,
                    'profile_id' => $this->profile->id,
                    'type' => $value['type'] ?? '',
                ]);
                
                unset($this->productData[$code]);
            }
        }

        return $correctData;
    }
    
    /*
     * Работает так же как метод с ProductService
     */
    private function prepareProperties($productId = null): array
    {
        $correctData = [];
        
        foreach ($this->productData as $code => $value)
        {
            if(!$value)
            {
                continue;
            }
            
            // Свойства товара
            if(!$productId)
            {
                array_push($correctData, [
                    'localisation_id' => $this->localisationId,
                    'code' => $code,
                    'value' => $value,
                    'is_parent' => true, // Признак того, что характеристика относиться к товару а не к торговому предложению
                ]);
            
            // Свойства торгового предложения (модели товара)
            }else
            {
                array_push($correctData, [
                    'localisation_id' => $this->localisationId,
                    'code' => $code,
                    'value' => $value,
                ]);
            }
        }
        
        return $correctData;
    }

    /**
     * Собирается массив для создания продукта
     */
    private function filterArray(string $key, array $tableData)
    {
        if( isset($this->tableHeaders[$key]) //Не берём в массив вспомогательные значения, чтобы они не записались в properties
            && !in_array($key , self::UNUSED_PROPERTIES)
            && in_array($key, array_keys( $this->allowedHeaders()) )
        )
        {
            if( $key === 'width' || $key === 'lenght' || $key === 'height' && isset($this->tableHeaders['size_type']))
            {
                $dictionary = $this->getDictionaryCode('size_type', 'sizes', $tableData);

                $this->makeQuantitiesArray($tableData, $dictionary, $key,  self::DEFAULT_SIZE);
                
                unset($dictionary);

            } elseif( $key === 'weight' && isset($this->tableHeaders['weight_type']))
            {
                $dictionary = $this->getDictionaryCode('weight_type', 'weights', $tableData);

                $this->makeQuantitiesArray($tableData, $dictionary, $key,  self::DEFAULT_WEIGHT);

                unset($dictionary);

            } elseif( $key === 'available' || $key === 'min_order' && isset($this->tableHeaders['available_type'])
            )
            {
                $dictionary = $this->getDictionaryCode('available_type', 'quantities', $tableData);

                $this->makeQuantitiesArray($tableData, $dictionary, $key,  self::DEFAULT_QUANTITY);

                unset($dictionary);

            } elseif($key === 'country')
            {
                $tableData[ $this->tableHeaders[$key] ] = strtoupper($tableData[ $this->tableHeaders[$key] ]);

                $dictionary = $this->getDictionaryCode($key, 'countries', $tableData);

                $this->productData[$key] =  $dictionary ? $dictionary->code : self::DEFAULT_COUNTRY;

                unset($dictionary);
                
            } elseif($key === 'color')
            {
                $dictionary = $this->getDictionaryCode($key, 'colors', $tableData);

                $this->productData[$key] = $dictionary ? $dictionary->code : self::DEFAULT_COLOR;

                unset($dictionary);
                
            } elseif($key === 'category')
            {
                $dictionary = Category::select('id')
                    ->where([
                        ['id', (int) $tableData[ $this->tableHeaders['category'] ]],
                        ['is_active', true]
                    ])
                    ->whereNotNull('parent_id')
                    ->first();    
                
                $this->productData['category_id'] = $dictionary ? (int) $dictionary->id : self::DEFAULT_CATEGORY;

                unset($dictionary); 

            } elseif( $key === 'youtube')
            {
                if(preg_match('/^http(s|)\:\/\/w{3}\.youtube\..*?\//', $tableData[ $this->tableHeaders[$key] ]))
                {
                    $this->productData[$key] = $tableData[ $this->tableHeaders[$key] ];
                }

            } else
            {
                $this->productData[$key] = $tableData[ $this->tableHeaders[$key] ];
            }
        }
    }

    /**
     * Метод для поиска значения некоторых колонок в словаре
     */
    private function getDictionaryCode(string $key, string $group_code, array $tableData)
    {
        return ApplicationDictionary::where('group_code',$group_code)
            ->where('value', $tableData[ $this->tableHeaders[$key] ])
            ->first();
    }

    /**
     * Метод сбора массива для габаритов и количества товара
     */
    private function makeQuantitiesArray(array $tableData, $dictionary, string $key, $defaultType, $defaultValue = 0)
    {
        $this->productData[$key] = [
            'value' => (int) $tableData[ $this->tableHeaders[$key] ] ?? $defaultValue,
            'type' =>  $dictionary ? $dictionary->code : $defaultType,
        ];

        unset($dictionary);
    }

    /*
     * Массив с шапкой для таблицы со списком категорий, цвета, стран
     */
    private function exampleListHeaders() :array
    {
        return [
            'A2' => '#',
            'B2' => __('cabinet.import.category'),

            'D2' => '#',
            'E2' => __('cabinet.import.color'),
            'F2' => __('cabinet.import.color_example'),

            'H2' => '#',
            'I2' => __('cabinet.import.country'),
        ];
    }
}