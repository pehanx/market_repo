<?php

namespace App\Helpers\Product;

use App\ApplicationDictionary;
use Illuminate\Http\Request;
use Illuminate\Http\File;
use App\Jobs\ExportProductJob;
use App\Extensions\Service as BaseService;
use ZipArchive;
use Illuminate\Support\Str;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Style\Border;
use PhpOffice\PhpSpreadsheet\Writer\{Xlsx, Xls, Csv};
use App\Models\{
    Export,
    ProfileExport,
    Product,
    ProductProperty,
    Category,
    CategoryProperty,
    ProductPrice,
    Profile,
    Localisation,
    Currency,
    User,
};

class ExportProductService extends BaseService
{
    /**
     * Названия столбцов
     */ 
    const ALLOWED_INDEXES = [
        'articul' => 'A',
        'thumbnail' => 'B',
        'name' => 'C',
        'description' => 'D',
        'category' => 'E',
        'color' => 'F',
        'weight' => 'G',
        'lenght' => 'H',
        'width' => 'I',
        'height' => 'J',
        'price_min' => 'K',
        'price_max' => 'L',
        'order_min' => 'M',
        'avialable' => 'N',
        'preview_image' => 'O',
        'detail_images' => 'P',
        'describle_images' => 'Q',
    ];

    const FOLDER_PAGE = 'exports/page/';
    const FOLDER_ALL = 'exports/all/';
    
    private $profile;

    private $needElements; // Продукты со всеми их зависимостями
    private $localisationId;
    private $baseCurrencyCode;
    private $exportFormat;
    private $withThumbnails; // Параметр с превьюшками или без
    private $withModels; // Параметр с моделями или без
    private $productsData = []; // Массив из которого заполняется таблица
    private $archiveData; // Массив с файлами для загрузки
    private $spreadsheet; // Объект таблицы
    private $archives = []; // Массив с архивами
    private $exportModel;
    private $countFilesInArchive; // Исходное количество файлов в архиве (Нужно для получения процентов)

    /**
     * Общие стили столбцов
     */       
    private $columnStyles = [
        'borders' => [
            'outline' => [
                'borderStyle' => Border::BORDER_HAIR,
                'color' => ['argb' => '0000000'],
            ],
        ],
    ];
      
    /**
    * Отправка данных для экспорта (Одна страница)
    */
    public function exportPage(array $exportParams, Profile $profile)
    {
        $this->profile = $profile;
        $this->localisationId = (int)$this->profile->profileProperties()->first()->localisation_id;
        $this->exportFormat = $exportParams['export_format'];
        $this->withThumbnails = $exportParams['with_thumbnails'] ?? false;
        $this->withModels = $exportParams['with_models'] ?? false;
        
        $elementsId = json_decode($exportParams['elements_id']);

        $elements = $this->getElementsForPage($exportParams['status'], $elementsId, $this->profile);

        Product::getAllProductsWithModels($elements, $this->localisationId, $this->localisationId, false, true);

        $elements
            ->load('characteristics')
            ->each(function (& $element){
                Product::setCorrectCharacteristics($element,  $this->localisationId);
            });

        $this->needElements = $elements;
        unset($elements, $elementsIds);

        $this
            ->prepareProductsData() // Формирование массива необходимой структуры для таблицы и получение путей изображений для архива
            ->makeTable() // Создание объекта таблицы
            ->saveTable(); // Сохранение таблицы в необходимом формате 

        return $this->getArchiveForPage();
    }

    /**
    * Отправка данных для экспорта
    */ 
    public function exportAll(array $exportParams, int $profileId, int $exportId)
    {
        $this->profile = Profile::find($profileId);
        $this->exportModel = Export::find($exportId);
        $this->localisationId = (int)$this->profile->profileProperties()->first()->localisation_id;
        
        $elements = $exportParams['elements'];
        
        Product::getAllProductsWithModels($elements, $this->localisationId, $this->localisationId, false, true);
    
        $elements
            ->load('characteristics')
            ->each(function (& $element){
                Product::setCorrectCharacteristics($element,  $this->localisationId);
            });

        $this->needElements = $elements;
        unset($elements);

        $this->exportFormat = $exportParams['export_format'];
        $this->withThumbnails = $exportParams['with_thumbnails'];
        $this->withModels = $exportParams['with_models'];

        $this
            ->prepareProductsData() // Формирование массива необходимой структуры для таблицы и получение путей изображений для архива
            ->makeTable() // Создание объекта таблицы
            ->addTableToArchiveData()
            ->createArchivesWithData();
        
        // Метод из BaseService
        $this->associateWithFiles($this->exportModel, $this->archives, 'product', $this->profile->users()->first()->id);
    }

    /**
     * Колонки с габаритами и ценами, которые при создании стилей будут уже
     */ 
    private function narrowColumns(): array
    {
        return [
            self::ALLOWED_INDEXES['weight'],
            self::ALLOWED_INDEXES['lenght'],
            self::ALLOWED_INDEXES['width'],
            self::ALLOWED_INDEXES['height'],
            self::ALLOWED_INDEXES['price_min'],
            self::ALLOWED_INDEXES['price_max'],
            self::ALLOWED_INDEXES['color'],
            self::ALLOWED_INDEXES['avialable'],
        ];
    }

    /**
     * Получение валюты по умолчанию
     */ 
    private function getDefaultCurrency()
    {
        if(!$this->baseCurrencyCode)
        {
            $this->baseCurrencyCode = Currency::where('is_base', true)
                ->pluck('code')
                ->first();
        }
        
        return $this->baseCurrencyCode;
    }

    /**
     * Получение заголовков таблицы на нужной локали
     */ 
    private function getColumnNames()
    {
        $columnNames = [];
        
        ApplicationDictionary::select('value','code')
                ->where([
                    ['localisation_id', $this->localisationId],
                    ['group_code', 'columns']
                ])
                ->get()
                ->mapWithKeys(function ($item) use(& $columnNames)
                    {
                        $columnNames[ self::ALLOWED_INDEXES[$item->code] . 1 ] = $item->value;
                        return [];
                    }
                ); 

        return $columnNames;
    }

    /**
     * Отрисовка превьюшек в колонке, еси они нужны
     */ 
    private function getDrawingThumbnail($sheet, $thumbSize, $key, $dataValues) // Отрисовка изображения
    {
        $drawing = new \PhpOffice\PhpSpreadsheet\Worksheet\Drawing();
        
        $drawing->setName('Thumbnail'.($key+2));
        $drawing->setDescription('Thumbnail Image');
        $drawing->setOffsetX(1);
        $drawing->setOffsetY(1);
        $drawing->setPath(\Storage::disk(env('FILESYSTEM_DRIVER'))->path("public/$dataValues"));
        $drawing->setCoordinates(self::ALLOWED_INDEXES['thumbnail'] . ( $key+2 ));
        $drawing->setResizeProportional(false);
        $drawing->setWidth($thumbSize);
        $drawing->setHeight($thumbSize);
        $drawing->setWorksheet($sheet);
        
        return $drawing;
    }

    /**
     * Получение данных для продукта
     */ 
    private function prepareProductsData()
    {
        $columnNames = $this->getColumnNames();
        
        $this->needElements->each(function ($element, $key) use(& $columnNames){
            
            // Код базовой валюты для шапки
            $currencyCode = $this->getDefaultCurrency();

            $columnNames[ self::ALLOWED_INDEXES['price_max'] . 1] .= $currencyCode;
            $columnNames[ self::ALLOWED_INDEXES['price_min'] . 1] .= $currencyCode;
            
            // Ед. изм. веса
            $weightType = $element->characteristics['weight']['type'] ?? ''; 
            
            // Ед. изм. количества
            $quantType = $element->characteristics['available']['type'] ?? ''; 

            // Ед. изм. размеров
            $sizeType = !empty($element->characteristics['width']['type']) ? $element->characteristics['width']['type'] : 
                ( !empty($element->characteristics['lenght']['type']) ? $element->characteristics['lenght']['type'] :
                ( !empty($element->characteristics['height']['type']) ? $element->characteristics['height']['type'] : '' ));

            $lastKey = count($this->productsData) + 2;

            array_push($this->productsData, [
                    self::ALLOWED_INDEXES['articul'] . $lastKey => $element->articul,
                    self::ALLOWED_INDEXES['thumbnail'] . $lastKey => $element->downloads()->where('type', 'preview_image')->pluck('path')->first() ?? '',
                    self::ALLOWED_INDEXES['name'] . $lastKey => $element->productProperties()->where('code', 'name')->pluck('value')->first() ?? '',
                    self::ALLOWED_INDEXES['description'] . $lastKey => $element->productProperties()->where('code', 'description')->pluck('value')->first() ?? '',
                    self::ALLOWED_INDEXES['category'] . $lastKey => $element->categories()->first()->categoryProperties()->where('code', 'name')->pluck('value')->first() ?? '',
                    self::ALLOWED_INDEXES['color'] . $lastKey => $element->characteristics['color']['value'] ?? '',
                    self::ALLOWED_INDEXES['weight'] . $lastKey => $element->characteristics()->where('code', 'weight')->pluck('value')->first().$weightType ?? '',
                    self::ALLOWED_INDEXES['lenght'] . $lastKey => $element->characteristics()->where('code', 'lenght')->pluck('value')->first().$sizeType ?? '',
                    self::ALLOWED_INDEXES['width'] . $lastKey => $element->characteristics()->where('code', 'width')->pluck('value')->first().$sizeType ?? '',
                    self::ALLOWED_INDEXES['height'] . $lastKey => $element->characteristics()->where('code', 'height')->pluck('value')->first().$sizeType ?? '',              
                    self::ALLOWED_INDEXES['price_min'] . $lastKey => $element->productPrices()->pluck('min')->first() ?? '',
                    self::ALLOWED_INDEXES['price_max'] . $lastKey => $element->productPrices()->pluck('max')->first() ?? '',
                    self::ALLOWED_INDEXES['order_min'] . $lastKey => $element->characteristics()->where('code', 'min_order')->pluck('value')->first().$quantType ?? '',
                    self::ALLOWED_INDEXES['avialable'] . $lastKey => $element->characteristics()->where('code', 'available')->pluck('value')->first().$quantType ?? '',
                    self::ALLOWED_INDEXES['preview_image'] . $lastKey => basename($element->downloads()->where('type', 'preview_image')->pluck('path')->first() ),
                    self::ALLOWED_INDEXES['detail_images'] . $lastKey => $this->getCorrectImageNames($element->downloads()->where('type', 'detail_images')->pluck('path')->all() ?? '' ),
                    self::ALLOWED_INDEXES['describle_images'] . $lastKey => $this->getCorrectImageNames($element->downloads()->where('type', 'describle_images')->pluck('path')->all() ?? '' ),
                    'R1' => 'product',
            ]);
            
            $rawImagePaths = array_merge(
                $element->downloads()->where('type', 'preview_image')->pluck('path')->all(),
                $element->downloads()->where('type', 'detail_images')->pluck('path')->all() ?? [],
                $element->downloads()->where('type', 'describle_images')->pluck('path')->all() ?? []
            );
            
            $this->archiveData = array_map([$this, 'prepareImagePath'], $rawImagePaths);
            unset($rawImagePaths);

            //Если выбран вариант выгрузки с моделями продукта
            if($this->withModels === "true" && $element->productModels()->first())
            {
                $this->addProductModels($element->productModels()->get(), $lastKey, $quantType);
            }  
            
        });

        return $this;
    }

    private function addProductModels($elements, $lastKey, $quantType) 
    {
        $elements->each(function ($element) use($lastKey, $quantType){
            
            ++$lastKey; 
            
            array_push($this->productsData, [
            
                self::ALLOWED_INDEXES['articul'] . $lastKey => '',
                self::ALLOWED_INDEXES['thumbnail'] . $lastKey => $element->downloads()->where('type','preview_image')->pluck('path')->first() ?? '',
                self::ALLOWED_INDEXES['name'] . $lastKey => '',
                self::ALLOWED_INDEXES['description'] . $lastKey => '',
                self::ALLOWED_INDEXES['category'] . $lastKey => '',
                self::ALLOWED_INDEXES['color'] . $lastKey => $element->characteristics['color']['value'] ?? '',
                self::ALLOWED_INDEXES['weight'] . $lastKey => '',
                self::ALLOWED_INDEXES['lenght'] . $lastKey => '',
                self::ALLOWED_INDEXES['width'] . $lastKey => '',
                self::ALLOWED_INDEXES['height'] . $lastKey => '',              
                self::ALLOWED_INDEXES['price_min'] . $lastKey => $element->productPrices()->pluck('min')->first() ?? '',
                self::ALLOWED_INDEXES['price_max'] . $lastKey => $element->productPrices()->pluck('max')->first() ?? '',
                self::ALLOWED_INDEXES['order_min'] . $lastKey => '',
                self::ALLOWED_INDEXES['avialable'] . $lastKey => $element->characteristics()->where('code', 'available')->pluck('value')->first().$quantType ?? '',
                self::ALLOWED_INDEXES['preview_image'] . $lastKey => basename($element->downloads()->where('type', 'preview_image')->pluck('path')->first() ),
                self::ALLOWED_INDEXES['detail_images'] . $lastKey => $this->getCorrectImageNames($element->downloads()->where('type', 'detail_images')->pluck('path')->all() ?? '' ),
                self::ALLOWED_INDEXES['describle_images'] . $lastKey => $this->getCorrectImageNames($element->downloads()->where('type', 'describle_images')->pluck('path')->all() ?? '' ),
                'R1' => 'model', 
            ]);
            
            // Получение путей изображений для массива из которого будет производиться запись в архив
            $rawImagePaths = array_merge(
                $element->downloads()->where('type', 'preview_image')->pluck('path')->all(),
                $element->downloads()->where('type','detail_images')->pluck('path')->all() ?? [],
                $element->downloads()->where('type','describle_images')->pluck('path')->all() ?? []
            );
            
            array_push($this->archiveData, array_map( [$this, 'prepareImagePath'], $rawImagePaths) );
            
            unset($rawImagePaths);
        });
    }

    /**
     * Создание таблицы, накатывание туда стилей
     */ 
    private function makeTable() //Стили для таблицы и заполнение
    {
        $columnNames = $this->getColumnNames();
        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();
        
        // имена колонок
        foreach ($columnNames as $key => $value)
        {
            $sheet->setCellValue($key, $value);
            $sheet->getStyle($key)->applyFromArray($this->columnStyles);
        }

        $sheet->fromArray($this->productsData, null, 'A2', false);
        
        //Стили
        $sheet->getDefaultRowDimension()->setRowHeight(100);
        $sheet->getDefaultColumnDimension()->setWidth(25);

        foreach ($this->narrowColumns() as $narrowColumns) //Стили для колонок с маленьким значениями
        {
            $sheet->getColumnDimension($narrowColumns)->setWidth(10);
        }
        
        //Превью и стили для моделей
        foreach ($this->productsData as $key => $value)
        {
            foreach ($value as $dataKey => $dataValues) 
            {
                $sheet->setCellValue('R' . ($key+2), ''); // Вместо вспомогательного значения из массива переписываю пустоту
                
                //Разные размеры от типа модель или продукт
                if($this->productsData[$key]['R1'] === 'model') 
                {
                    $thumbSize = 80;
                    
                    $modelProductDivision = self::ALLOWED_INDEXES['articul'] . ($key+1) . ':' . self::ALLOWED_INDEXES['articul'] . ($key+2);
                    
                    $spreadsheet
                        ->getActiveSheet()
                        ->mergeCells($modelProductDivision);
                    
                    $sheet
                        ->getRowDimension($key+2)
                        ->setRowHeight(80);
                        
                    $sheet
                        ->getStyle($dataKey)
                        ->applyFromArray($this->columnStyles); // стиль для моделей пока по дефолту
                    
                } elseif($this->productsData[$key]['R1'] === 'product')
                {
                    $thumbSize = 100;
                    $sheet
                        ->getStyle($dataKey)
                        ->applyFromArray($this->columnStyles); // стиль для продукта пока по дефолту
                }

                // Если ячейка для превьюшек, то пишем туда превьюшку если она выбрана в параметрах
                if($dataKey === self::ALLOWED_INDEXES['thumbnail'] . ( $key + 2 ) && $dataValues)
                {
                    $sheet->setCellValue(self::ALLOWED_INDEXES['thumbnail'].($key + 2),'');

                    if($this->withThumbnails === "true" && $this->exportFormat !== 'csv') // Если выбран вариант выгрузки с превью и формат не csv. Для csv превью не будет?
                    {
                        $drawing = $this->getDrawingThumbnail($sheet, $thumbSize, $key, $dataValues);
                    }
                }
            }
        }
        
        $this->spreadsheet = $spreadsheet;
        
        return $this;
    }

    /**
     * 
     */ 
    private function getArchiveForPage()
    {
        $zip = new ZipArchive;
        $zipPath = $this->getUploadedDir() . 'exports/page/export_' . time() . '.zip';

        if ( $zip->open($zipPath, ZipArchive::CREATE) )
        {
            foreach ($this->archiveData as $filePath)
            {
                $fileName = basename($filePath);

                if($filePath)
                {
                    $zip->addFile($filePath, $fileName);
                }
            }

            $zip->close();

            return response()->download($zipPath);
        } 
    }
    
    private function saveTable()
    {
        $spreadsheetWriterClass = 'PhpOffice\PhpSpreadsheet\Writer\\' . ucfirst($this->exportFormat);
        $tablePath = $this->getUploadedDir() . 'exports/page/table_' . time() . '.' . $this->exportFormat;
        
        if ( class_exists($spreadsheetWriterClass) )
        {
            $writer = new $spreadsheetWriterClass($this->spreadsheet);

            $writer->save($tablePath);
           
            array_push($this->archiveData, $tablePath);
        }
    }

    /**
     * Метод, создающий файл таблицы и записывающий его в массив для архива, для всех страниц
     */ 
    private function addTableToArchiveData() 
    {
        $spreadsheetWriterClass = 'PhpOffice\PhpSpreadsheet\Writer\\' . ucfirst($this->exportFormat);
        $tablePath = $this->getUploadedDir() . 'exports/page/table_' . time() . '.' . $this->exportFormat;

        if(class_exists($spreadsheetWriterClass))
        {
            $writer = new $spreadsheetWriterClass($this->spreadsheet);
            
            $writer->save($tablePath);
            
            array_push($this->archiveData, $tablePath);
        }
        
        $this->countFilesInArchive = count($this->archiveData);
        
        return $this;
    }

    /**
     * Метод для создания архива(ов)
     */ 
    private function createArchivesWithData()
    {
        $zip = new ZipArchive;
        $zipPath = $this->getUploadedDir() . self::FOLDER_ALL . 'export_' . time() . '.zip';
        $this->prepareFolder(self::FOLDER_ALL);

        if ($zip->open($zipPath, ZipArchive::CREATE) && $this->archiveData )
        {
            foreach ($this->archiveData as $key => $filePath)
            {
                $fileName = basename($filePath); // Имя файла в архиве

                if($fileName)
                {
                    $zip->addFile($filePath, $fileName);

                    unset($this->archiveData[$key]);
                    $this->updateProgress($zipPath);

                    if($zip->numFiles > 1000) // Дробление архива по файлам
                    {
                        $zip->close();
                        array_push($this->archives, new File($zipPath));
                        return $this->createArchivesWithData();
                    }
                }
            }
            $zip->close();
            array_push($this->archives, new File($zipPath));
        }
    }

    public function getElementsForJob(string $status, int $profileId)
    {
        $elements = [];
        $product = new Product;

        if($status === 'deleted')
        {
            $elements = $product
                ->where([
                    ['profile_id', $profileId],
                    ['is_active', false]
                ])
                ->onlyTrashed()
                ->get();
            
        } elseif($status === 'active')
        {
            $elements = $product
                ->where([
                    ['profile_id', $profileId],
                    ['is_active', true]
                ])
                ->get();
                
        } elseif($status === 'inactive')
        {
            $elements = $product
                ->where([
                    ['profile_id', $profileId],
                    ['is_active', false]
                ])
                ->get();
        }
        
        return $elements;
    }
    
    private function getElementsForPage(string $status, array $elementsId, Profile $profile)
    {
        $elements = [];

        if($status === 'deleted')
        {
            $elements = $profile->products()
                ->onlyTrashed()
                ->whereIn('id', $elementsId)
                ->where('is_active', false)
                ->get();
            
        } elseif($status === 'active')
        {
            $elements = $profile->products()
                ->whereIn('id', $elementsId)
                ->where('is_active', true)
                ->get();
    
        } elseif($status === 'inactive')
        {
            $elements = $profile->products()
                ->whereIn('id', $elementsId)
                ->where('is_active', false)
                ->get();
        }
        
        return $elements;
    }
    
    /**
     * Получение названий изображений для таблицы
     */ 
    private function getCorrectImageNames(array $imagePaths): string
    {
        if(!current($imagePaths))
        {
            return '';
        }
        
        $correctData = '';

        foreach ($imagePaths as $key => $imagePath)
        {
            $tmp = explode('/', $imagePath);
            $correctData .= array_pop($tmp);
            unset($tmp);
            
            if(is_countable($imagePaths) && (int)$key < count($imagePaths) - 1)
            {
                $correctData .= ',';
            }
        }
        
        return $correctData;
    }
    
    /**
     * Формируем корректный путь до изображения
     */ 
    private function prepareImagePath($rawImagePath)
    {
        return $this->getUploadedDir() . $rawImagePath;
    }
    
    private function getUploadedDir(): string
    {
        return \Storage::disk( env('FILESYSTEM_DRIVER') )->path("public/");
    }

    private function updateProgress($zipPath)
    {
        $this->exportModel->progress = round(100 - (count($this->archiveData) / $this->countFilesInArchive * 100), 2); // Запись процентов
        $this->exportModel->save();
    }

    private function prepareFolder($folder)
    {
        $dirName = $this->getUploadedDir() . $folder;

        if( !is_dir( $this->getUploadedDir() . $folder ) )
        {
            @mkdir( $dirName , 0777, true);
            $permissions = substr(sprintf( '%o', fileperms( $dirName ) ), -4);
            
            if( (int) $permissions !== 0777)
            {
                @chmod($dirName, 0777);
            }
        }
    }
}
