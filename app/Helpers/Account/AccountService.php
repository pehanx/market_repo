<?php

namespace App\Helpers\Account;

use App\User;
use App\Models\{Profile, Localisation, ProfileType, ProfileProperty};
use Illuminate\Support\Facades\Hash;

class AccountService
{
    const ALLOWED_FILES = [
        'avatar',
    ];
    
    public function update(User $user, array $request): void
    {
        $user->update([ 
            'name' => $request['name']
        ]);
        
        unset($request['name']);
        
        $account = Profile::getAccountForUser((int)$user->id)->first();

        // Обновление характеристик существующего аккаунта пользователя
        if($account)
        {
            ProfileProperty::where('profile_id', $account->id)->forceDelete();
            $account->profileProperties()->createMany( $this->prepareProperties($request) );
           
         // Создание аккаунта и заполнение характеристик
        }else
        {
            $account = $user->profiles()->create([
                'profile_type_id' => ProfileType::where('code', 'account')->first()->id,
                'status' => 'unactive',
            ]);
            
            $account->profileProperties()->createMany( $this->prepareProperties($request) );
        }
        
        $message = [
            'title' => __('notification.change_private_data'),
            'body' => __('notification.success_update_account_data_notify'),
            'type' => 'info'
        ];

        \Session::flash('message', $message);
    }

    /**
     * Не приватный, используется в других классах (не дочерних) 
     *
     * @param array $data
     * @return array $correctData
     */
    public function prepareProperties(array $data): array
    {
        $correctData = [];
        $localisationId = Localisation::where('code', app('translator')->getLocale())->first()->id; 
        
        foreach ($data as $code => $value)
        {
            if($value)
            {
                array_push($correctData, [
                    'localisation_id' => $localisationId,
                    'code' => $code,
                    'value' => $value
                ]);
            }
        }
        
        return $correctData;
    }
    
    /**
     * Проверка авторизованного пользователя 
     *
     * @param int $userId
     * @return bool
     */
    public function checkUser(int $userId): bool
    {
        if(auth()->user()->id === $userId)
        {
            return true;
        }
        
        return false;
    }
    
    /**
     * Обновление email и пароля авторизованного пользователя.
     * При входе через соц. сеть пользователь при заполнении пароля получает возможность входа по паролю и email в свой кабинет, с автоматической привязкой своего аккаунта соц. сети  
     *
     * @param App\User $user
     * @param array $data
     *
     */
    public function updateUser(User $user, array $data)
    {
        $message = '';
        
        if($user->registration_type === 'social' && $data['password'] === $data['password_confirmation'])
        {
            $user->registration_type = 'all';
            $user->email =  $data['email'];
            $user->password = bcrypt($data['password']);
            $user->save();
            
        }elseif($user->registration_type !== 'social' && isset($data['password_current']) && $data['password'] === $data['password_confirmation'] )
        {
            if ( Hash::check($data['password_current'], $user->password) )
            {
                $user->email =  $data['email'];
                $user->password = bcrypt($data['password']);
                $user->save();
                
            }else
            {
                return redirect()->back()->withInput()->withErrors(['password_current' => __('validation.password')]); 
            }
            
        }else
        {
            $message = [
                'title' => __('notification.change_entrance_data'),
                'body' => __('notification.dont_update_entrance_data_notify'),
                'type' => 'info'
            ];
        }
        
        if(!$message)
        {
            $message = [
                'title' => __('notification.change_entrance_data'),
                'body' => __('notification.success_update_entrance_data_notify'),
                'type' => 'info'
            ];
        }
        
        \Session::flash('message', $message);
    }
}
