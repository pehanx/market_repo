<?php

namespace App\Helpers\Favorites;

use App\User;
use App\Models\{Product, RequestProduct, Favorite};

class FavoriteService
{
    private $namespacePrefix = '';
    
    const ALLOWED_TYPES = [
        'product',
        'request_product'
    ];
       
    public function __construct()
    {
        $this->namespacePrefix = Favorite::getNamespacePrefix();
    }   
       
    public function add(User $user, int $id, string $model): void
    {
        // Ищем только среди неудаленных
        ($this->namespacePrefix . $model)::findOrFail($id); 
        
        // Отсекаем дубли в избранном у определенного пользователя
        $existFavorite = $user->favorites()
            ->where([
                ['favoriteable_type', $this->namespacePrefix . $model],
                ['favoriteable_id', $id]
            ])
            ->exists();
            
        if ( class_exists($this->namespacePrefix . $model) && !$existFavorite ) 
        {
            $user->favorites()->create([
                'favoriteable_id' => $id,
                'favoriteable_type' => $this->namespacePrefix . $model
            ]);
            
        } else
        {
            abort(403);
        }
    }
    
    public function delete(User $user, int $id, string $model): void
    {
        $favorite = $user->favorites()
            ->where([
                ['favoriteable_type', $this->namespacePrefix . $model],
                ['favoriteable_id', $id]
            ])
            ->firstOrFail();
          
        if ( class_exists($this->namespacePrefix . $model) ) 
        {    
            $favorite->delete();
            
        } else
        {
            abort(403);
        }
    }
}