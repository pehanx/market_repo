<?php

namespace App\Helpers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Str;
use Illuminate\Auth\Events\Registered;
use Laravel\Socialite\Contracts\User as NetworkUser;
use App\User;
use App\Models\SocialAccount;

class NetworkService extends Controller
{
    private $email;
    
    /**
     * Аутентификации и авторизации пользователя (регистрация с помощью соц. аккаунта, если пользователя с полученным email не существует)
     *
     * @param string $network
     * @param Laravel\Socialite\Contracts\User $networkUser
     
     * @return App\User
     */
    public function auth(string $network, NetworkUser $networkUser): User
    {
        // Находим соц. аккаунт пользователя
        // TODO: можно обновить данные о пользователе...
        if ( $socialAccount = SocialAccount::findById($network, (int)$networkUser->getId()) ) 
        {
            return $user = $socialAccount->users;
        }
        
        // Получаем email пользователя
        $this->email = $this->getNetworkUserEmail($network, $networkUser);
        
        // Добавление нового соц. аккаунта существующему пользователю (идентифицируем по email)
        if ( $this->email && $user = User::findByEmail($this->email, ['social', 'all', 'native']) )
        {
            $this->addAccount($network, $user, $networkUser);
            return $user;
        }

        // Регистрация пользователя, создание соц. аккаунта
        $user = User::create([
            'name' => $networkUser->user['first_name'],
            'email' => $this->email,
            'password' => bcrypt(Str::random(25)),
            'registration_type' => 'social',
            'ip' => \Request::ip(),
            'session_id' => session()->getID()
        ]); 
        
        $this->addAccount($network, $user, $networkUser);
        
        // Событие после создания юзера (заполнение spattie таблиц, без отправки сообщения на верефикацию email)
        event(new Registered($user));
        
        return $user;
    }
    
    /**
     * Создаем социальный аккаунт в БД для пользователя
     *
     * @param string $network
     * @param App\User $user
     * @param Laravel\Socialite\Contracts\User $networkUser
     *
     * @return void
     */
    private function addAccount(string $network, User $user, NetworkUser $networkUser): void
    {
        $user->socialAccounts()->create([
            'social_id' => $networkUser->getId(),
            'social' => $network,
            'access_token' => $networkUser->token
        ]);
    }
    
    /**
     * Получаем email пользователя. В зависимости от соц. сети используем доп. api методы
     *
     * @param string $network
     * @param Laravel\Socialite\Contracts\User $networkUser
     
     * @return string $email
     */
    private function getNetworkUserEmail(string $network, NetworkUser $networkUser): string
    {
        $email = $networkUser->getEmail();
        
        if( !$email && isset($networkUser->accessTokenResponseBody['email']) )
        {
            $email = $networkUser->accessTokenResponseBody['email'];
        }
        
        return (string)$email;
        
       /*  if(!$email)
        {
            switch ($network) 
            {
                case 'odnoklassniki':
                    $accessTokenResponseBody = $networkUser->accessTokenResponseBody;
                    $sign = md5("application_key=CLFNMFJGDIHBABABAformat=jsonmethod=users.getCurrentUser" . md5("{$accessTokenResponseBody ['access_token']}cfe47f36e1b563e03537c2b47aa4538a") );
                   
                    $params = 
                    [
                        'method'          => 'users.getCurrentUser',
                        'access_token'    => $accessTokenResponseBody['access_token'],
                        'application_key' => 'CLFNMFJGDIHBABABA',
                        'format'          => 'json',
                        'sig'             => $sign,
                        'scope' => 'GET_EMAIL',
                    ];
                    
                    $userInfo = json_decode(file_get_contents('http://api.odnoklassniki.ru/fb.do' . '?' . urldecode(http_build_query($params))), true);
                    dump($userInfo);exit();
                    break;
            }
        }
        
        */
                  
        /* 
        Вечный access_token odnoklassniki
        access_token	tkn1kfHKub844jFvfD1Z1viwjYWu8evCsar6bdDUB9QvqdSexIT0BzHvNpX6fpzcfPSDB6
        secret_session_key	
        https://connect.ok.ru/oauth/authorize

?client_id=512000052314

&scope=VALUABLE_ACCESS;LONG_ACCESS_TOKEN

&response_type=token

&redirect_uri=https://apiok.ru/oauth_callback

access_token	-s-4-a5bUpQMnaX2SL.NNZdaRtzML2c6.MMIn757QnyIM2Z1tsPJm7W-QnyMOb68yqQrN161RqPHNZ8dSKTsl3Yb1
session_secret_key	50d8c0b38ecb085776d780b1b7dbd528


Используем для подписи secret_key = session_secret_key
Сортируем и склеиваем параметры запроса и secret_key
application_key=CLFNMFJGDIHBABABAformat=jsonmethod=users.getInfo50d8c0b38ecb085776d780b1b7dbd528
Рассчитываем MD5 от полученной строки и получаем параметр sig
2f2b16c40905fa8b6127b2442a3d9a1d
https://api.ok.ru/fb.do

?application_key=CLFNMFJGDIHBABABA

&format=json

&method=users.getInfo

&sig=2f2b16c40905fa8b6127b2442a3d9a1d

&access_token=-s-4-a5bUpQMnaX2SL.NNZdaRtzML2c6.MMIn757QnyIM2Z1tsPJm7W-QnyMOb68yqQrN161RqPHNZ8dSKTsl3Yb1
        */
    }
}
