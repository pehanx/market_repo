<?php
namespace App\Helpers\Chat;
use Carbon\Carbon;

use App\Models\{Profile,Message,Chat,ProfileProperty,Localisation};
use App\User;
use App\Services\Notification\FirebaseService;
use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;
use PhpAmqpLib\Exchange\AMQPExchangeType;
use PhpAmqpLib\Channel\AMQPChannel;
use PhpAmqpLib\Wire\AMQPTable;
use PhpAmqpLib\Exception\AMQPConnectionBlockedException;

class ChatService 
{
    //Кабинет адм - чаты - отд контролл 
    //полиморф на чатах
    const DEFAULT_CHAT_NAME = ''; // Название чата, если у профиля не будет указано company_name
    const DEFAULT_CHAT_IMAGE = 'images/cabinetImages/avatar_default.png'; // Путь до изображения, которое отображается если у собеседника нет фото 

    private $currentProfile; //Текущий профиль, от которого сообщения 
    private $toProfile; //Кому отправляется
    private $messageText;
    private $chat;
    private $messageModel;
    private $chatModel;

    /* 
     * Метод в котором создаётся хеш
     */
    public static function getHash(int $listenerId, string $type = 'profile') : string
    {
        if($type === 'profile')
        {
            return base64_encode($listenerId);

        } else
        {
            return base64_encode(User::find($listenerId)->name . User::find($listenerId)->id );
        }
    }

    /* 
     * Поиск по профилям
     */
    public function profileSearch(string $searchString, string $type, Profile $currentProfile) : array
    {
        $filteredQuery = $this->filterMessage($searchString);
        
        if($type === 'contacts')
        {
            $profiles = $this->contactList($currentProfile, $filteredQuery);

        } else
        {
            $profiles = $this->profilesList($currentProfile, $filteredQuery);
        }

        return $profiles;
    }

    /* 
     * Создание чата в бд
     */
    public function createChat(Profile $currentProfile, Profile $toProfile) : Chat
    {
        $hasChat = Chat::isChatWithMemberExists($currentProfile, $toProfile->id);

        if(!$hasChat)
        {
            $chatModel = auth()->user()->profiles()->where('profile_id',$currentProfile->id)
                ->first()
                ->chats()->create([
                    'owner_id' => $currentProfile->id,
                ]);
            
            $this->addToChat($chatModel, $toProfile);

        } else
        {
            $chatModel = $hasChat;
        }
        
        return $chatModel;
    }  

    /* 
     * Метод в котором в существующий чат добавляется профиль
     */
    public function addToChat(Chat $currentChat, Profile $toProfile) : void
    {
        $toProfile->chats()->attach($currentChat->id);
    }

    /* 
     * Метод в котором из существующего чата удаляется профиль
     */
    /* public function removeFromChat(Chat $currentChat, Profile $toProfile) : void
    {
        if( $currentChat->profiles()->get()->count() > 2 )// Чтобы в чате не оставался один профиль
        {
            $toProfile->chats()->detach($currentChat->id);
        }
    } */

    /* 
     * Удаление чата
     */
    public function deleteChat(Chat $chat) : void
    {
        /* if($withMessages === true) // Если чат удаляем с сообщениями
        {
            $messages = $chat->messages()->get();

            foreach ($messages as $message)
            {
                $message->delete();
            }
        } */
        $chat->delete();
    }

    /* 
     * Проверка содержимого сообщения, используется и в ChatSupportService
     */
    public function filterMessage(string $text) : string
    {
        $text = clean($text);
        
        if(trim($text) === '')
        {
            $text = false;
        }
        
        return $text;
    }

    /* 
     * Отправка пуша
     */
    public function sendPush(int $currentProfile, int $toProfile, int $chatId, string $messageText) : void
    {
        $this->setData( $currentProfile, $messageText, $toProfile );

        if( $this->currentProfile !== null
            && $this->toProfile !== null)
        {
            $this->pushData($chatId);

        }
    }

    /* 
     * Запись в бд
     */
    public function writeData(int $currentProfile, int $chatId, string $messageText) : void
    {
        $this->setData( $currentProfile, $messageText );

        if( $this->currentProfile !== null )
        {
            $this->pushData($chatId, 'db');
        }
    }

    /* 
     * Создание сообщения в бд, используется и в ChatSupportService
     */
    public function createMessage(Chat $chat, string $text, int $ownerId, $type = null) : Message
    {
        return $chat->messages()->create([
            'message' => $text,
            'owner_id' => $ownerId,
            'type' => $type,
            'is_readed' => false,
        ]);
    }

    /* 
     * Метод в котором собирается массив для простого сообщения, используется и в ChatSupportService
     */
    public function prepareMessage(int $ownerId, int $chatId, string $messageText, string $type = 'profile') : array
    {
        $message = [];
        $message['chat_id'] = $chatId;
        $message['message'] = $messageText;
        $message['from_id'] = $ownerId;

        $chat = Chat::where('id',$chatId)->whereHas('messages')
            ->with(['messages' =>  function ($query) {
                $query->orderBy('created_at','desc')->limit(1);
            }])
            ->first();
        $message['id'] = $chat ? $chat->messages->first()->id : '';

        if($type === 'support')
        {
            $owner = User::find($ownerId);

            if( !$owner->hasRole('admin') ) //Если сообщение отправлено не админом
            {
                $message['from'] = $owner->name;

            } else
            {
                $message['from'] = __('cabinet.chat.support');
            }

        } else
        {
            $profileName = Profile::find($ownerId)->profileProperties()->where('code','company_name')->first();

            $message['from'] = $profileName
                    ? $profileName->value
                    : null;
        }

        $message['route_delete'] = route('chat.messages.delete', ['chat' => $chat->id, 'message' => $message['id']]);
        $message['type'] = 'message';
        $message['created_at'] = Carbon::now();

        return $message;
    }

    /* 
     * Метод в котором собирается массив для сервисного сообщения
     */
    public function prepareServiceMessage(string $type, $key, $value) : array
    {
        $message = [];
        $message['type'] = $type;
        $message['key'] = $key;
        $message['value'] = $value;
        $message['service'] = true;
        return $message;
    }

    /* 
     * Получение данных для отрисовки контактов из js
     */
    public function getContactTemplates(Profile $profile) : array
    {
        $contacts = $profile->contacts()->get()
        ->transform( function($item) use($profile) {

            $contactImage = Profile::find($item->contact_id)->downloads()->where('type', 'company_logo_image')->first();
            
            $item = Profile::find($item->contact_id);
            $item->contact_name = $item->profileProperties()->whereIn('code',['company_name','name'])->first()->value;
            $item->contact_image = $contactImage
                ? asset('storage/' . $contactImage->path)
                : asset('assemble/' . self::DEFAULT_CHAT_IMAGE);

            $contactExist = $profile->contacts()->where('contact_id',$item->id)->first();

            $item->route_contact = $contactExist
                ? route('profile.contact.remove', ['profile' => $profile, 'contact' => $item->id])
                : route('contact.add', ['contact' => $item->id]);

            // Существует ли уже чат с этим профилем
            $chatExist = Chat::isChatWithMemberExists($profile, $item->id);

            $item->route_chat = $chatExist
                ? route('profile.chat.send', ['profile' => $profile, 'chat'=>$chatExist ]) // Роут, по которому отправляются сообщения
                : route('profile.chat.create',['profile' => $profile->id]);
                
            $item->is_contact = $contactExist ? true : false;

            $item->chat_id = $chatExist ? $chatExist->id : null;

            $item->route_messages = $chatExist ?  route('profile.chat.messages', ['profile' => $profile, 'chat' => $chatExist]) : null;

            $item->status = $item->isOnline() ? 'online' : 'offline';

            unset(
                $item->rating,
                $item->pivot,
                $item->deleted_at,
                $item->updated_at,
                $item->created_at
            );

            return $item;   
        })
        ->toArray();

        return $contacts;
    }

    /* 
     * Данные, необходимые для отрисовки чатов из js
     */
    public function getChatTemplates(Profile $profile) : array
    {
        $myChats = $profile->chats()->pluck('chat_id')->toArray();
        $profileChats = Chat::whereIn('id', $myChats)->orderBy('created_at', 'asc')
            ->get()
            ->transform(function($item) use($profile){
                
                if(!auth()->user()->hasRole('admin'))
                {
                    $lastMessage = $item->messages()->whereNull('type')->orderBy('created_at','desc')->first();

                } else
                {
                    $lastMessage = $item->messages()->orderBy('created_at','desc')->first();
                }

                $item->type = 'profile';
                $item->profile_id = $item->profiles()->where('chatable_type', Profile::class)->where('chatable_id','<>',$profile->id)->first()->id;
                $item->chat_name = $this->getChatName($profile->id, $item);
                $item->chat_image = $this->getChatImage($profile->id, $item);
                $item->status = $this->getChatStatus($profile->id, $item);
                
                $item->last_message = $lastMessage ? $lastMessage->message : '';
                $item->route_delete = route('profile.chat.delete', ['profile' => $profile, 'chat'=>$item ]); // Роут для удаления чата
                $item->route_send = route('profile.chat.send', ['profile' => $profile, 'chat'=>$item ]); // Роут, по которому отправляются сообщения
                $item->route_messages = route('profile.chat.messages', ['profile' => $profile, 'chat' => $item]); // Роут, по которому загружаются сообщения
                $item->messages_count = $item->messages()->where([['is_readed', false], ['owner_id','<>', $profile->id ]])->count();

                $item->created_at = $lastMessage
                    ? $lastMessage->created_at
                    : $item->created_at;

                unset(
                    $item->pivot,
                    $item->deleted_at,
                    $item->updated_at,
                );
                return $item;
            })
            ->toArray();

            array_unshift($profileChats, $this->getAdminChatTemplate());

        return $profileChats;
    }

    /* 
     * Пуш на ребит, который показывает до какого сообщения считать сообщения прочитанными
     */
    public function readMessages(int $lastMessageId, Chat $chat, Profile $profile)
    {
        $message = $this->prepareServiceMessage('readed', $lastMessageId, $profile->id );
        $this->sendInAMQP($chat, $message);
    }

    /* 
     * Создание/получение чата с админом
     */
    public function getAdminChatTemplate() : array
    {
        $chatExist = auth()->user()->chats()->where('chatable_type',User::class)->first();
        
        //Если чат с админом уже существует
        if( $chatExist !== null )
        {
            
            $adminChat = $chatExist;

            $admin = User::find($adminChat->owner_id);

        } else
        {
            //Выбирается рандомный админ, который будет общаться с пользователем
            $admin = User::whereHas('roles', function($q){
                $q->whereIn('name', ['admin']);
            })
            ->get()
            ->random();

            $adminChat = auth()->user()->chats()
                ->where('chatable_type',User::class)
                ->where('chatable_id', auth()->user()->id)
                ->first()
                ?? auth()->user()->chats()->create([
                    'owner_id' => $admin->id,
                ]);

            if($admin->chats()->where('chat_id', $adminChat->id)->first() === null)
            {
                $admin->chats()->attach($adminChat->id);
            }
        }

        if(!auth()->user()->hasRole('admin'))
        {
            $lastMessage = $adminChat->messages()->whereNull('type')->orderBy('created_at','desc')->first();

        } else
        {
            $lastMessage = $adminChat->messages()->orderBy('created_at','desc')->first();
        }

        return [
            'id' => $adminChat->id,
            'type' => 'support',
            'owner_id' => $adminChat->owner_id,
            'chat_name' => __('cabinet.chat.support'),
            'chat_image' => asset('assemble/' . self::DEFAULT_CHAT_IMAGE),
            'status' => $admin->isOnline() ? 'online' : 'offline',
            'last_message' => $lastMessage
                ? $lastMessage->message 
                : '',

            'created_at' => $lastMessage
                ? $lastMessage->created_at
                : $adminChat->created_at,

            'route_send'=> route('chat.support', ['chat'=>$adminChat]), // Роут, по которому отправляются сообщения
            'route_messages' => route('chat.messages', ['chat' => $adminChat]), // Роут, по которому загружаются сообщения
            'messages_count' => $adminChat->messages()->where([['is_readed', false], ['owner_id','<>', auth()->user()->id ]])->count()
        ];
    }


    /* 
     * Получение названия чата, сейчас название профиля собеседника
     */
    private function getChatName(int $profileId, Chat $chat) : string
    {
        $toProfile = $chat->profiles()->where('chatable_type',Profile::class)->where('chatable_id','<>',$profileId)->first();
        
        if($toProfile !== null)
        {
            $chatName = ProfileProperty::where('profile_id', $toProfile->id)
                ->whereIn('code',['company_name','name'])
                ->first();
                
            return $chatName ? $chatName->value : self::DEFAULT_CHAT_NAME;
        }

        return self::DEFAULT_CHAT_NAME;
    }

    /* 
     * Получение статуса профиля собеседника онлайн/оффлайн
     */
    private function getChatStatus(int $profileId, Chat $chat) : string
    {
        $toProfile = $chat->profiles()->where('chatable_type',Profile::class)->where('chatable_id','<>',$profileId)->first();
        
        if($toProfile !== null)
        {
            if($toProfile->isOnline())
            {
                return 'online';
            }
        }

        return 'offline';
    }

    /* 
     * Получение изображения для чата, лого собеседника
     */
    private function getChatImage(int $profileId, Chat $chat) : string
    {
        $toProfile = $chat->profiles()->where('chatable_type', Profile::class)->where('chatable_id','<>',$profileId)->first();
        
        if($toProfile !== null)
        {
            $profileAvatar = $toProfile->downloads()->where('type','company_logo_image')->first();

            $image = $profileAvatar
                ? asset('storage/'. $profileAvatar->path )
                : asset('assemble/' . self::DEFAULT_CHAT_IMAGE);

            return $image;
        }
        return asset('assemble/' . self::DEFAULT_CHAT_IMAGE);
    }

    /* 
     * Проверка файла чата
     */
    private function prepareFile()
    {
        
    }

    /* 
     * Отправка в рэбит
     */
    public function sendInAMQP( Chat $chat, array $message, string $chatType = 'profile') : void
    {
        if($chatType === 'profile')
        {
            $chatSubscribers = $chat->profiles()->get()
                ->transform(function($item, $key){
                    $item->hash = env('RABBITMQ_PREFIX') . '-' . $this->getHash($item->id);

                    $item->phones = $item->users()->first()->devices->pluck('device_id');
                    return $item;
                })
                ->unique()
                ->toArray();

        } else
        {
            $chatSubscribers = $chat->users
                ->transform(function($item, $key){
                    $item->hash = env('RABBITMQ_PREFIX') . '-' . $this->getHash($item->id, 'user');
                    $item->phones = $item->devices->pluck('device_id');
                    return $item;
                })
                ->unique()
                ->toArray();
        }

        $connection = new AMQPStreamConnection(
            env('RABBITMQ_HOST', 'rabbitmq'),
            env('RABBITMQ_PORT', 5672),
            env('RABBITMQ_USER', 'guest'),
            env('RABBITMQ_PASSWORD', 'guest'),
        );
        $channel = $connection->channel();

        foreach ($chatSubscribers as $subscriber)
        {
            $msg = new AMQPMessage(json_encode($message));
            $channel->queue_declare(
                $subscriber['hash'],
                false, //passive
                true, //durable
                false, //exclusive
                false, //autodelete
                false, //nowait
                null, //arguments
                null //ticket
            );
            $channel->basic_publish($msg, '', $subscriber['hash']);

            //Отправка в Firebase
            if(isset($message['message']) && isset($subscriber['phones']))
            {
                $firebaseService = new FirebaseService();

                foreach($subscriber['phones'] as $deviceId)
                {
                    $firebaseService->send([
                        "body" => $message['message'],
                        "title" => $message['from'],
                        "icon" => "ic_launcher"
                    ],
                    $deviceId,
                    [
                        'created_at' => $message['created_at'],
                        'chat_id' => $message['chat_id'],
                        'from_id' => $message['from_id'],
                        'type' => $chatType
                    ]);
                }
            }
        }

        $channel->close();
        $connection->close();
    }

    /* 
     * Получение списка всех профилей доступных для поиска
     */
    private function profilesList(Profile $currentProfile, string $filteredQuery)
    {                        
        $profiles = ProfileProperty::where('code', 'company_name')
                    ->whereHas('profiles', function ($query) {

                        $query->whereNotIn('id', auth()->user()->profiles()->get()->pluck('id')->toArray())
                            ->where('status', 'active')
                            ->whereIn('profile_type_id',[1,2]);
                    })
                    ->where(
                        \DB::raw('LOWER(value)'), 'like', '%'.mb_strtolower($filteredQuery).'%'
                    )
                    ->select('profile_id','value')
                    ->get()
                    ->transform(function ($item) use($currentProfile) {

                        $profileLogo = Profile::find($item->profile_id)->downloads()
                            ->where('type','company_logo_image')->first();

                        $item->logo = $profileLogo
                             ? asset('storage/'. $profileLogo->path)
                             : null;
                             
                        // Существует ли уже чат с этим профилем
                        $chatExist = Chat::isChatWithMemberExists($currentProfile, $item->profile_id);

                        $contactExist = $currentProfile->contacts()->where('contact_id',$item->profile_id)->first();

                        $item->route_contact = $contactExist
                            ? route('profile.contact.remove', ['profile' => $currentProfile, 'contact' => $item->profile_id])
                            : route('contact.add', ['contact' => $item->profile_id]);

                        $item->is_contact = $contactExist ? true : false ;

                        $item->route_send = $chatExist 
                            ? route('profile.chat.send', ['profile' => $currentProfile, 'chat'=>$chatExist ]) // Роут, по которому отправляются сообщения
                            : route('profile.chat.create',['profile' => $currentProfile->id]);

                        $item->chat_exist = false;

                        if($chatExist !== null)
                        {
                            $item->chat_exist = true;
                            $item->chat_id = $chatExist->id;
                            $item->route_messages = route('profile.chat.messages', ['profile' => $currentProfile, 'chat' => $chatExist]); // Роут, по которому загружаются сообщения
                        }

                        $item->type = 'chat';
                        $item->is_online = Profile::find($item->profile_id)->isOnline();
                        
                        return $item;
                    })
                    ->toArray();
        
        return  $profiles;
    }

    /* 
     * Получение списка всех контактов доступных для поиска
     */
    private function contactList(Profile $currentProfile, string $filteredQuery)
    {     
        $profiles = ProfileProperty::where('code', 'company_name')
                    ->whereHas('profiles', function ($query) use($currentProfile) {
                        $query->whereIn('id', $currentProfile->contacts()->pluck('contact_id')->toArray())
                            ->where('status', 'active')
                            ->whereNotIn('id', auth()->user()->profiles()->get()->pluck('id')->toArray())
                            ->whereIn('profile_type_id',[1,2]);
                    })
                    ->where(
                        \DB::raw('LOWER(value)'), 'like', '%'.mb_strtolower($filteredQuery).'%'
                    )
                    ->select('profile_id','value')
                    ->get()
                    ->transform(function ($item) {

                        $profileLogo = Profile::find($item->profile_id)->downloads()
                            ->where('type','company_logo_image')->first();

                        $item->logo = $profileLogo
                             ? asset('storage/'. $profileLogo->path)
                             : null;

                        $item->type = 'contact';
                        $item->is_online = Profile::find($item->profile_id)->isOnline();
                        return $item;
                    })
                    ->toArray();
        
        return  $profiles;
    }

    /* 
     * Запись данных, необходимых для сервиса
     */
    private function setData(int $currentProfile, string $messageText, int $toProfile = null)
    {
        if( $toProfile === null )
        {
            $this->currentProfile = Profile::find($currentProfile);

        } else
        {
            $this->currentProfile = auth()->user()->profiles()->where('profile_id', $currentProfile)->first();
            $this->toProfile = Profile::find($toProfile);
        }

        $this->messageText = $this->filterMessage($messageText);

    }

    /* 
     * Отправка пуша, либо создание сообщения
     */
    private function pushData(int $chatId, string $type = 'push')
    {
        $chatModel = $this->currentProfile->chats()->where('chat_id', $chatId)->first();

        if( $chatModel !== null )
        {
            if($type !== 'push')
            {
                $this->messageModel = $this->createMessage($chatModel, $this->messageText, $this->currentProfile->id);

            } else
            {
                $message = $this->prepareMessage($this->currentProfile->id, $chatModel->id, $this->messageText);
                $this->sendInAMQP($chatModel, $message);
            }
        }
    }
}