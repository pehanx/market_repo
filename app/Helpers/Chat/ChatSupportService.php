<?php
namespace App\Helpers\Chat;

use App\Models\{Profile,Message,Chat,ProfileProperty,Localisation};
use App\User;
use App\Helpers\Chat\ChatService;
use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;
use PhpAmqpLib\Exchange\AMQPExchangeType;
use PhpAmqpLib\Channel\AMQPChannel;
use PhpAmqpLib\Wire\AMQPTable;
use PhpAmqpLib\Exception\AMQPConnectionBlockedException;

class ChatSupportService 
{
    const DEFAULT_CHAT_NAME = ''; // Название чата, если у профиля не будет указано company_name
    const DEFAULT_CHAT_IMAGE = 'images/cabinetImages/avatar_default.png'; // Путь до изображения, которое отображается если у собеседника нет фото 

    private $currentUser; //Текущий профиль, от которого сообщения 
    private $toUser; //Кому отправляется
    private $messageText;
    private $chat;
    private $messageModel;
    private $service;

    public function __construct(ChatService $service)
    {
        $this->service = $service;
    }

    /* 
     * Создание чата в бд
     */
    public function createChat(User $toUser, string $type = 'user') : Chat
    {
        if ($type = 'user')
        {
            $this->chatModel = auth()->user()
            ->chats()->create([
                'owner_id' => $toUser->id,
            ]);

            $this->addToChat($this->chatModel, $toUser);

        } else
        {
            $this->chatModel = $toUser
            ->chats()->create([
                'owner_id' => auth()->user()->id,
            ]);

            $this->addToChat($this->chatModel, auth()->user());
        }
        
        return $this->chatModel;
    }

    /* 
     * Метод в котором в существующий чат добавляется пользователь
     */
    public function addToChat(Chat $currentChat, User $toUser) : void
    {
        $toUser->chats()->attach($currentChat->id);
    }

    /* 
     * Метод в котором происходит добавление в контакты
     */
    public function addToContact(Profile $profile, Profile $contact) : void
    {
        if($profile->contacts()->onlyTrashed()->where('contact_id',$contact->id)->first() === null)
        {
            $profile->contacts()->create([
                'contact_id' => $contact->id
            ]);

        } else
        {
            $profile->contacts()->onlyTrashed()->where('contact_id',$contact->id)->first()->restore();
        }
    }

    public function sendPush( int $chatId, string $messageText) : void
    {
        $this->currentUser = auth()->user();
        $this->messageText = $this->service->filterMessage($messageText);

        if( $this->currentUser !== null )
        {
            $this->pushData($chatId);
        }
    }

    /* 
     * Запись данных в бд
     */
    public function writeData( int $chatId, string $messageText, $type) : void
    {
        $this->currentUser = auth()->user();
        
        $this->messageText = $type ? $messageText : $this->service->filterMessage($messageText);
            

        if( $this->currentUser !== null )
        {
            $this->insertData($chatId, $type);
        }
    }

    /* 
     * Получить данные для модалки настроек для чата
     */
    public function getChatSettingItems(Profile $profile, string $settingType) : array
    {
        $settings = [];
        
        $profileSettings = $profile->settings()->get();
        
        if ( $settingType === 'localisation' )
        {
            $translateSettings = [];
            $localisationsArray = [];

            $localisations = Localisation::select('code')->get()->toArray();
    
            foreach ($localisations as $localisation)
            {
                $isSelected = null;

                if($profileSettings->first() && $profileSettings->where('code','localisation')->first()->value === $localisation['code'])
                {
                    $isSelected = true;
                }

                array_push($localisationsArray, [
                    'code' => $localisation['code'],
                    'value' => __('interface.localisation.'. $localisation['code']),
                    'selected' => $isSelected ? true : false,
                ]);
            }
            
            $translateSettings = [
                'name' => __('cabinet.chat.select_language'),
                'values' => $localisationsArray,
                'type' => 'select'
            ];
    
            array_push($settings, $translateSettings);
        }

        return $settings;
    }

    /* 
     * Удаление непрочитанного сообщения
     */
    public function deleteMessage(Message $message, string $ownerType = 'user') : void
    {
        $chat = $message->chats;

        $message->delete();

        //Отправляем сигнал на ребит о том что сообщение удалено
        $messageForAMQP = $this->service->prepareServiceMessage('delete', 'message', $message->id);

        $this->service->sendInAMQP( $chat, $messageForAMQP, $ownerType);    

    }

    /* 
     * При выходе из чата, смена статуса профиля
     */
    public function disconnectFromChat() : void
    {
        $currentProfile = session()->get('active_profile');

        //Если есть профиль в сессии, то дисконнект отправляем не только в чат с админом(другим пользоваелем), но и в чаты профиля
        if($currentProfile)
        {
            $chats = $currentProfile->chats()->get();

            foreach($chats as $chat)
            {
                $message = $this->service->prepareServiceMessage('disconnect', 'offline', $currentProfile->id);
                $this->service->sendInAMQP($chat, $message, 'profile');
            }
        }

        $chats = auth()->user()->chats()->get();

        foreach($chats as $chat)
        {
            $message = $this->service->prepareServiceMessage('disconnect', 'offline', auth()->user()->id);
            $this->service->sendInAMQP($chat, $message, 'user');
        }
    }

    /* 
     * смена статуса на онлайн у профиля и пользователя, чтобы статус поменялся у собеседников
     */
    public function sendOnlineStatus() : void
    {
        $currentProfile = session()->get('active_profile');

        //Если есть профиль в сессии, то дисконнект отправляем не только в чат с админом(другим пользоваелем), но и в чаты профиля
        if($currentProfile)
        {
            $chats = $currentProfile->chats()->get();

            foreach($chats as $chat)
            {
                $message = $this->service->prepareServiceMessage('connected', 'online', $currentProfile->id);
                $this->service->sendInAMQP($chat, $message, 'profile');
            }
        }

        $chats = auth()->user()->chats()->get();

        foreach($chats as $chat)
        {
            $message = $this->service->prepareServiceMessage('connect', 'online', auth()->user()->id);
            $this->service->sendInAMQP($chat, $message, 'user');
        }
    }

    /* 
     * Пуш на ребит, который показывает до какого сообщения считать сообщения прочитанными
     */
    public function readMessages(int $lastMessageId, Chat $chat)
    {
        $message = $this->service->prepareServiceMessage('readed', $lastMessageId, auth()->user()->id );
        $this->service->sendInAMQP($chat, $message, 'user');
    }

    /* 
     * Данные, необходимые для отрисовки чатов из js
     */
    public function getChatTemplates() : array
    {
        $myChats = auth()->user()->chats()->pluck('chat_id')->toArray();
        $userChats = Chat::whereIn('id', $myChats)->orderBy('created_at', 'asc')
            ->get()
            ->transform(function($item) {
                $item->type = 'support';
                $item->chat_name = $this->getChatName($item);
                $item->chat_image = $this->getChatImage($item);

                $item->user_id = $item->users()->where('chatable_id','<>',auth()->user()->id)->first()->id;

                $item->status = $item->users->where('id','<>', auth()->user()->id)->first()->isOnline() ? 'online' : 'offline';

                if(!auth()->user()->hasRole('admin'))
                {
                    $lastMessage = $item->messages()->whereNull('type')->orderBy('created_at','desc')->first();

                } else
                {
                    $lastMessage = $item->messages()->orderBy('created_at','desc')->first();
                }
                
                $item->created_at = $lastMessage
                    ? $lastMessage->created_at
                    : $item->created_at;

                $item->last_message = $lastMessage
                    ? $lastMessage->message
                    : '';

                $item->route_send = route('chat.support', ['chat'=>$item->id ]); // Роут, по которому отправляются сообщения
                $item->route_messages = route('chat.messages', ['chat' => $item->id]); // Роут, по которому загружаются сообщения
                $item->route_translate = route('chat.translate'); // Роут, по которому осуществляется перевод сообщения
                $item->messages_count = $item->messages()->where([['is_readed', false], ['owner_id','<>', auth()->user()->id ]])->count();

                unset(
                    $item->pivot,
                    $item->deleted_at,
                    $item->updated_at,
                    $item->users
                );

                return $item;
            })
            ->toArray();

        return $userChats;
    }

    /* 
     * Получить название чата (Используется с админки, чтобы имя пользователя получить с которым диалог)
     */
    private function getChatName(Chat $chat) : string
    {
        $chatUser = Chat::find($chat->id)->users()
            ->where('chatable_id','<>',auth()->user()->id)
            ->first();

        if(auth()->user()->hasRole('admin'))
        {
            return $chatUser ? $chatUser->name : self::DEFAULT_CHAT_NAME;

        } else
        {
            return __('cabinet.chat.support');
        }
    }

    /* 
     * Получить изображение чата
     */
    private function getChatImage(Chat $chat) : string
    {
        $chatUser = Chat::find($chat->id)->users()
            ->where('chatable_id','<>',auth()->user()->id)
            ->first();

        if($chatUser !== null)
        {
            $userAvatar = $chatUser->downloads()->where('type','avatar')->first();

            $image = $userAvatar
                ? asset('storage/' . $userAvatar->path )
                : asset('assemble/' . self::DEFAULT_CHAT_IMAGE);
                
            return  $image;
        }

        return asset('assemble/' . self::DEFAULT_CHAT_IMAGE);
    }

    /* 
     * Поиск по юзерам (Только с админки)
     */
    public function userSearch(string $searchString) : array
    {
        $filteredQuery = $this->service->filterMessage($searchString);
        
        $users = $this->usersList($filteredQuery);

        return $users;
    }

    /* 
     * Получение списка всех профилей доступных для поиска
     */
    private function usersList(string $filteredQuery) : array
    {                        
        $users = User::where('id','<>',auth()->user()->id)
            ->where(\DB::raw('LOWER(name)'), 'like', '%'.mb_strtolower($filteredQuery).'%')
            ->get()
            ->transform(function ($item) {

                $item->profile_id = $item->id;
                $item->value = $item->name;

                $item->logo = User::find($item->id)->downloads()
                    ->where('type','avatar')->first()
                        ? asset('storage/'. User::find($item->id)->downloads()
                        ->where('type','avatar')->first()->path)
                        : null;

                $chatExist = $item->chats()
                    ->where('owner_id', auth()->user()->id)
                    ->first();
                
                $item->route_send = $chatExist
                    ? route('chat.support', ['chat' => $chatExist->id])
                    : route('chat.create');

                if($chatExist !== null)
                {
                    $item->route_messages = route('chat.messages', ['chat' => $chatExist->id]);
                }

                $item->type = 'chat';
                $item->is_online = $item->isOnline() ? 'online' : 'offline';
                return $item;
            })
            ->toArray();
        
        return $users;
    }

    /* 
     * * Отправка пуша, либо создание сообщения
     */
    private function insertData(int $chatId, string $type = null)
    {
        $chat = $this->currentUser->chats()->where('chat_id', $chatId)->first();

        if( $chat !== null )
        {
            $this->messageModel = $this->service->createMessage($chat, $this->messageText, $this->currentUser->id, $type);
        }
    }

    /* 
     * * Отправка пуша, либо создание сообщения
     */
    private function pushData(int $chatId)
    {
        $chat = $this->currentUser->chats()->where('chat_id', $chatId)->first();

        if( $chat !== null )
        {
            $message = $this->service->prepareMessage($this->currentUser->id, $chat->id, $this->messageText, 'support');
            $this->service->sendInAMQP( $chat, $message, 'user');
        }
    }
}