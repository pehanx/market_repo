<?php

namespace App\Helpers\RequestProduct;

use App\User;
use App\Models\{Profile, Localisation, ProfileType, ProfileProperty, RequestProductPrice, Currency, RequestProduct};
use Illuminate\Support\{Str, Carbon};

class RequestProductService
{
    const ALLOWED_STATUSES = [
        'active',
        'inactive',
        'deleted',
    ];
    
    const ALLOWED_CHARACTERISTICS = [
        'min_order'
    ];
    
    const ALLOWED_ACTIONS = [
        'disable_checked',
        'delete_checked',
        'restore_checked',
        'destroy_checked',
        'activate_checked',
        'disable_all', //ок
        'activate_all', //ок
        'delete_active_all', //ок
        'delete_unactive_all',//ок
        'restore_deleted_all',//ок
        'destroy_deleted_all',
    ];
    
    public function create(Profile $profile, int $localisationId, array $request): RequestProduct
    {
        $reqProduct = $profile->requestProducts()->create([
            'category_id' => $request['category_id'],
            'link' => Str::slug($request['name'], '-'),
            'sort' => 1
        ]);

        unset($request['category_id']);
        
        //сохранение характеристик, в дальнейшем используемых при фильтрации
        $reqProduct->characteristics()->createMany( $this->prepareCharacteristics($request, (int)$profile->id) ); 
        
        // сохранение цена на продукт
        $reqProduct->requestProductPrices()->create([
            'currency_id' => Currency::where('is_base', true)->first()->id,
            'min' => $request['min_price'] ?? 0,
            'max' => $request['max_price'] ?? 0,
        ]);
        
        unset($request['min_price'], $request['max_price']);
        
        //сохранение свойств
        $reqProduct->requestProductProperties()->createMany($this->prepareProperties($request, $localisationId) );
        
        return $reqProduct;
    }

    private function prepareCharacteristics(array &$data, $profileId): array
    {
        $correctData = [];

        foreach ($data as $code => $value)
        {
            if($value && in_array($code, self::ALLOWED_CHARACTERISTICS))
            {
                $correctValue = (is_array($value) && array_key_exists('value', $value)) ? $value['value'] : $value;

                if(!$correctValue)
                {
                    unset($data[$code]);
                    continue;
                }
                
                array_push($correctData, [
                    'code' => $code,
                    'value' => $correctValue,
                    'profile_id' => $profileId,
                    'type' => $value['type'] ?? '',
                ]);
                
                unset($data[$code]);
            }
        }
        //dd($correctData);
        return $correctData;
    }
    
    private function prepareProperties(array $data, $localisationId): array
    {
        $correctData = [];
        
        foreach ($data as $code => $value)
        {
            if($value)
            {
                array_push($correctData, [
                    'localisation_id' => $localisationId,
                    'code' => $code,
                    'value' => $value,
                ]);
            }
        }
        //dd($correctData);
        return $correctData;
    }
    
    public function update(Profile $profile, RequestProduct $requestProduct, int $localisationId, array $request): RequestProduct
    {
        $requestProduct->update([
            'category_id' => $request['category_id'],
            'link' => Str::slug($request['name'], '-'),
            'sort' => 1
        ]);

        unset($request['category_id']);
        
        $requestProduct->characteristics()->delete(); 
        $requestProduct->characteristics()->createMany( $this->prepareCharacteristics($request, (int)$profile->id) ); 

        $requestProduct->requestProductPrices()->update([
            'min' => $request['min_price'],
            'max' => $request['max_price'] ?? 0,
        ]);

        unset($request['min_price'], $request['max_price']);
        
        $requestProduct->requestProductProperties()->where('localisation_id', $localisationId)->delete();
        $requestProduct->requestProductProperties()->createMany($this->prepareProperties($request, $localisationId) );
        
        $message = [
            'title' => '',
            'body' => __('notification.save_changes_success'),
            'type' => 'info'
        ];
        
        \Session::flash('message', $message);
        
        return $requestProduct; 
    }
    
    /**
     * Помечаем как мусор товар с его торговыми предложениями 
     *
     * @param App\Models\Profile $profile
     * @param int $requestProductId
     *
     * @return void
     */
    public function destroy(Profile $profile, int $requestProductId)
    {
        $requestProduct = $profile->requestProducts()->onlyTrashed()->findOrFail($requestProductId);
        
        $requestProduct->destroyed_at = Carbon::now();
        $requestProduct->save();
    }
    
    /**
     * Удаляем запрос на покупку, не вызывая событие в boot
     *
     * @param App\Models\Profile $profile
     * @param int $requestProductId
     *
     * @return void
     */
    public function delete(Profile $profile, int $requestProductId): void
    {
        $profile->requestProducts()->findOrFail($requestProductId);      

        RequestProduct::where('id', $requestProductId)->update(['is_active' => false]);
        RequestProduct::where('id', $requestProductId)->delete();
    }
    
    /**
     * Отключаем запрос на покупку
     *
     * @param App\Models\Profile $profile
     * @param int $requestProductId
     *
     * @return void
     */
    public function disable(Profile $profile, int $requestProductId): void
    {
        $requestProduct = $profile->requestProducts()->findOrFail($requestProductId);   

        $requestProduct->is_active = false;
        $requestProduct->save();
    }
    
    /**
     * Активируем запрос на покупку
     *
     * @param App\Models\Profile $profile
     * @param int $requestProduct
     *
     * @return void
     */
    public function activate(Profile $profile, int $requestProductId): void
    {
        $requestProduct = $profile->requestProducts()->findOrFail($requestProductId);   

        $requestProduct->is_active = true;
        $requestProduct->save();
    }
    
    /**
     * Восстанавливаем запрос из удаленных
     *
     * @param App\Models\Profile $profile
     * @param int $requestProductId
     *
     * @return void
     */
    public function restore(Profile $profile, int $requestProductId): void
    {
        $requestProduct = $profile->requestProducts()->onlyTrashed()->findOrFail($requestProductId);
        
        $requestProduct->is_active = true; // Из удаленных восстанавливаем в активные
        $requestProduct->restore();
    }
    
    /**
     * Активируем все отключенные запросы (ок)
     *
     * @param App\Models\Profile $profile
     *
     * @return void
     */
    public function activateRequestProducts(Profile $profile): void
    {
        $profile->requestProducts()->where('is_active', false)->update(['is_active' => true]);
    }
    
    /**
     * Отключаем все активные запросы (ок)
     *
     * @param App\Models\Profile $profile
     *
     * @return void
     */
    public function disableRequestProducts(Profile $profile): void
    {
        $profile->requestProducts()->where('is_active', true)->update(['is_active' => false]);
    }
    
     /**
     * Удаляем все активные запросы (ок)
     *
     * @param App\Models\Profile $profile
     *
     * @return void
     */
    public function deleteActiveRequestProducts(Profile $profile): void
    {
        $profile->requestProducts()->where('is_active', true)->delete();
        $profile->requestProducts()->onlyTrashed()->where('is_active', true)->update(['is_active' => false]);
    }
    
    /**
     * Восстанавливаем все уделенные запросы (ок)
     *
     * @param App\Models\Profile $profile
     *
     * @return void
     */
    public function restoreDeletedRequestProducts(Profile $profile): void
    {
        $profile->requestProducts()->onlyTrashed()->update(['is_active' => true]);
        $profile->requestProducts()->onlyTrashed()->restore();
    }
    
     /**
     * Удаляем все отключенные запросы (ок)
     *
     * @param App\Models\Profile $profile
     *
     * @return void
     */
    public function deleteUnactiveRequestProducts(Profile $profile): void
    {
        $profile->requestProducts()->where('is_active', false)->delete();
    }
    
      /**
     * Помечаем как мусор уделенные запросы с торговыми предложениями и отдельные торговые предложения
     *
     * @param App\Models\Profile $profile
     *
     * @return void
     */
    public function destroyDeletedRequestProducts(Profile $profile): void
    {
        $profile->requestProducts()->onlyTrashed()->update(['destroyed_at' => Carbon::now()]);
    }
}
