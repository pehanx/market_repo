<?php

namespace App\Helpers\RequestProduct;

use App\ApplicationDictionary;
use Illuminate\Http\Request;
use Illuminate\Http\File;
use App\Jobs\ExportProductJob;
use App\Extensions\Service as BaseService;
use ZipArchive;
use Illuminate\Support\Str;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Style\Border;
use PhpOffice\PhpSpreadsheet\Writer\{Xlsx, Xls, Csv};
use App\Models\{
    Export,
    ProfileExport,
    RequestProduct,
    ProductProperty,
    Category,
    CategoryProperty,
    ProductPrice,
    Profile,
    Localisation,
    Currency,
    User,
};

class ExportRequestProductService extends BaseService
{
    /**
     * Названия столбцов
     */ 
    const ALLOWED_INDEXES = [
        'thumbnail' => 'A',
        'name' => 'B',
        'description' => 'C',
        'category' => 'D',
        'price_min' => 'E',
        'price_max' => 'F',
        'order_min' => 'G',
        'preview_image' => 'H',
        'detail_images' => 'I',
    ];

    const FOLDER_PAGE = 'exports/page/';
    const FOLDER_ALL = 'exports/all/';
    
    private $profile;

    private $needElements; // Запросы со всеми их зависимостями
    private $localisationId;
    private $baseCurrencyCode;
    private $exportFormat;
    private $withThumbnails; // Параметр с превьюшками или без
    private $withModels; // Параметр с моделями или без
    private $productsData = []; // Массив из которого заполняется таблица
    private $archiveData; // Массив с файлами для загрузки
    private $spreadsheet; // Объект таблицы
    private $archives = []; // Массив с архивами
    private $exportModel;
    private $countFilesInArchive; // Исходное количество файлов в архиве (Нужно для получения процентов)

    /**
     * Общие стили столбцов
     */       
    private $columnStyles = [
        'borders' => [
            'outline' => [
                'borderStyle' => Border::BORDER_HAIR,
                'color' => ['argb' => '0000000'],
            ],
        ],
    ];
      
    /**
    * Отправка данных для экспорта (Одна страница)
    */
    public function exportPage(array $exportParams, Profile $profile)
    {
        $this->profile = $profile;
        $this->localisationId = (int)$this->profile->profileProperties()->first()->localisation_id;
        $this->exportFormat = $exportParams['export_format'];
        $this->withThumbnails = $exportParams['with_thumbnails'] ?? false;
        $this->withModels = $exportParams['with_models'] ?? false;
        
        $elementsId = json_decode($exportParams['elements_id']);

        $elements = $this->getElementsForPage($exportParams['status'], $elementsId, $this->profile);

        RequestProduct::getAllRequestProducts($elements, $this->localisationId, $this->localisationId);

        $elements
            ->load('characteristics')
            ->each(function (& $element){
                RequestProduct::getRequestProduct($element,  $this->localisationId);
            });

        $this->needElements = $elements;
        unset($elements, $elementsIds);

        $this
            ->prepareRequestProductsData() // Формирование массива необходимой структуры для таблицы и получение путей изображений для архива
            ->makeTable() // Создание объекта таблицы
            ->saveTable(); // Сохранение таблицы в необходимом формате 

        return $this->getArchiveForPage();
    }

    /**
    * Отправка данных для экспорта
    */ 
    public function exportAll(array $exportParams, int $profileId, int $exportId)
    {
        $this->profile = Profile::find($profileId);
        $this->exportModel = Export::find($exportId);
        $this->localisationId = (int)$this->profile->profileProperties()->first()->localisation_id;

        $elements = $exportParams['elements'];
        
        RequestProduct::getAllRequestProducts($elements, $this->localisationId, $this->localisationId, false, true);
    
        $elements
            ->load('characteristics')
            ->each(function (& $element){
                RequestProduct::getRequestProduct($element,  $this->localisationId);
            });

        $this->needElements = $elements;
        unset($elements);

        $this->exportFormat = $exportParams['export_format'];
        $this->withThumbnails = $exportParams['with_thumbnails'];

        $this
            ->prepareRequestProductsData() // Формирование массива необходимой структуры для таблицы и получение путей изображений для архива
            ->makeTable() // Создание объекта таблицы
            ->addTableToArchiveData()
            ->createArchivesWithData();

        // Метод из BaseService
        $this->associateWithFiles($this->exportModel, $this->archives, 'request_product', $this->profile->users()->first()->id);
    }

    /**
     * Колонки с габаритами и ценами, которые при создании стилей будут уже
     */ 
    private function narrowColumns(): array
    {
        return [
            self::ALLOWED_INDEXES['price_min'],
            self::ALLOWED_INDEXES['price_max'],
        ];
    }

    /**
     * Получение валюты по умолчанию
     */ 
    private function getDefaultCurrency()
    {
        if(!$this->baseCurrencyCode)
        {
            $this->baseCurrencyCode = Currency::where('is_base', true)
                ->pluck('code')
                ->first();
        }
        
        return $this->baseCurrencyCode;
    }

    /**
     * Получение заголовков таблицы на нужной локали
     */ 
    private function getColumnNames()
    {
        $columnNames = [];
        
        ApplicationDictionary::select('value','code')
                ->where([
                    ['localisation_id', $this->localisationId],
                    ['group_code', 'columns']
                ])
                ->get()
                ->mapWithKeys(function ($item) use(& $columnNames)
                    {
                        if( isset(self::ALLOWED_INDEXES[$item->code]) )
                        {
                        $columnNames[ self::ALLOWED_INDEXES[$item->code] . 1 ] = $item->value;
                        return [];
                        }
                        return [];
                    }
                ); 

        return $columnNames;
    }

    /**
     * Отрисовка превьюшек в колонке, еси они нужны
     */ 
    private function getDrawingThumbnail($sheet, $thumbSize, $key, $dataValues) // Отрисовка изображения
    {
        $drawing = new \PhpOffice\PhpSpreadsheet\Worksheet\Drawing();
        
        $drawing->setName('Thumbnail'.($key+2));
        $drawing->setDescription('Thumbnail Image');
        $drawing->setOffsetX(1);
        $drawing->setOffsetY(1);
        $drawing->setPath(\Storage::disk(env('FILESYSTEM_DRIVER'))->path("public/$dataValues"));
        $drawing->setCoordinates(self::ALLOWED_INDEXES['thumbnail'] . ( $key+2 ));
        $drawing->setResizeProportional(false);
        $drawing->setWidth($thumbSize);
        $drawing->setHeight($thumbSize);
        $drawing->setWorksheet($sheet);
        
        return $drawing;
    }

    /**
     * Получение данных для продукта
     */ 
    private function prepareRequestProductsData()
    {
        $columnNames = $this->getColumnNames();
        
        $this->needElements->each(function ($element, $key) use(& $columnNames){
            
            // Код базовой валюты для шапки
            $currencyCode = $this->getDefaultCurrency();

            $columnNames[ self::ALLOWED_INDEXES['price_max'] . 1] .= $currencyCode;
            $columnNames[ self::ALLOWED_INDEXES['price_min'] . 1] .= $currencyCode;
            
            $lastKey = count($this->productsData) + 2;

            // Ед. изм. количества
            $quantType = $element->characteristics['available']['type'] ?? ''; 

            array_push($this->productsData, [
                    self::ALLOWED_INDEXES['thumbnail'] . $lastKey => $element->downloads()->where('type', 'preview_image')->pluck('path')->first() ?? '',
                    self::ALLOWED_INDEXES['name'] . $lastKey => $element->requestProductProperties()->where('code', 'name')->pluck('value')->first() ?? '',
                    self::ALLOWED_INDEXES['description'] . $lastKey => $element->requestProductProperties()->where('code', 'full_description')->pluck('value')->first() ?? '',
                    self::ALLOWED_INDEXES['category'] . $lastKey => $element->categories()->first()->categoryProperties()->where('code', 'name')->pluck('value')->first() ?? '',
                    self::ALLOWED_INDEXES['price_min'] . $lastKey => $element->requestProductPrices()->pluck('min')->first() ?? '',
                    self::ALLOWED_INDEXES['price_max'] . $lastKey => $element->requestProductPrices()->pluck('max')->first() ?? '',
                    self::ALLOWED_INDEXES['order_min'] . $lastKey => $element->characteristics()->where('code', 'min_order')->pluck('value')->first().$quantType ?? '',
                    self::ALLOWED_INDEXES['preview_image'] . $lastKey => basename($element->downloads()->where('type', 'preview_image')->pluck('path')->first() ),
                    self::ALLOWED_INDEXES['detail_images'] . $lastKey => $this->getCorrectImageNames($element->downloads()->where('type', 'detail_images')->pluck('path')->all() ?? '' ),
            ]);
            
            $rawImagePaths = array_merge(
                $element->downloads()->where('type', 'preview_image')->pluck('path')->all(),
                $element->downloads()->where('type', 'detail_images')->pluck('path')->all() ?? [],
                $element->downloads()->where('type', 'describle_images')->pluck('path')->all() ?? []
            );
            
            $this->archiveData = array_map([$this, 'prepareImagePath'], $rawImagePaths);
            unset($rawImagePaths);
            
        });

        return $this;
    }

    /**
     * Создание таблицы, накатывание туда стилей
     */ 
    private function makeTable() //Стили для таблицы и заполнение
    {
        $columnNames = $this->getColumnNames();
        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();
        
        // имена колонок
        foreach ($columnNames as $key => $value)
        {
            $sheet->setCellValue($key, $value);
            $sheet->getStyle($key)->applyFromArray($this->columnStyles);
        }

        $sheet->fromArray($this->productsData, null, 'A2', false);
        
        //Стили
        $sheet->getDefaultRowDimension()->setRowHeight(100);
        $sheet->getDefaultColumnDimension()->setWidth(25);

        foreach ($this->narrowColumns() as $narrowColumns) //Стили для колонок с маленьким значениями
        {
            $sheet->getColumnDimension($narrowColumns)->setWidth(10);
        }
        
        //Превью и стили для моделей
        foreach ($this->productsData as $key => $value)
        {
            foreach ($value as $dataKey => $dataValues) 
            {
                $sheet->setCellValue('R' . ($key+2), ''); // Вместо вспомогательного значения из массива переписываю пустоту
                
                $sheet
                    ->getStyle($dataKey)
                    ->applyFromArray($this->columnStyles); // стиль для запроса пока по дефолту

                // Если ячейка для превьюшек, то пишем туда превьюшку если она выбрана в параметрах
                if($dataKey === self::ALLOWED_INDEXES['thumbnail'] . ( $key + 2 ) && $dataValues)
                {
                    $thumbSize = 100;

                    $sheet->setCellValue(self::ALLOWED_INDEXES['thumbnail'].($key + 2),'');

                    if($this->withThumbnails === "true" && $this->exportFormat !== 'csv') // Если выбран вариант выгрузки с превью и формат не csv. Для csv превью не будет?
                    {
                        $drawing = $this->getDrawingThumbnail($sheet, $thumbSize, $key, $dataValues);
                    }
                }
            }
        }
        
        $this->spreadsheet = $spreadsheet;
        
        return $this;
    }

    /**
     * 
     */ 
    private function getArchiveForPage()
    {
        $zip = new ZipArchive;
        $zipPath = $this->getUploadedDir() . 'exports/page/export_' . time() . '.zip';
        $this->prepareFolder(self::FOLDER_PAGE);

        if ( $zip->open($zipPath, ZipArchive::CREATE) )
        {
            foreach ($this->archiveData as $filePath)
            {
                $fileName = basename($filePath);

                if($filePath)
                {
                    $zip->addFile($filePath, $fileName);
                }
            }

            $zip->close();

            return response()->download($zipPath);
        } 
    }
    
    private function saveTable()
    {
        $spreadsheetWriterClass = 'PhpOffice\PhpSpreadsheet\Writer\\' . ucfirst($this->exportFormat);
        $tablePath = $this->getUploadedDir() . 'exports/page/table_' . time() . '.' . $this->exportFormat;
        
        if ( class_exists($spreadsheetWriterClass) )
        {
            $writer = new $spreadsheetWriterClass($this->spreadsheet);

            $writer->save($tablePath);
           
            array_push($this->archiveData, $tablePath);
        }
    }

    /**
     * Метод, создающий файл таблицы и записывающий его в массив для архива, для всех страниц
     */ 
    private function addTableToArchiveData() 
    {
        $spreadsheetWriterClass = 'PhpOffice\PhpSpreadsheet\Writer\\' . ucfirst($this->exportFormat);
        $tablePath = $this->getUploadedDir() . 'exports/page/table_' . time() . '.' . $this->exportFormat;

        if(class_exists($spreadsheetWriterClass))
        {
            $writer = new $spreadsheetWriterClass($this->spreadsheet);
            
            $writer->save($tablePath);
            
            array_push($this->archiveData, $tablePath);
        }
        
        $this->countFilesInArchive = count($this->archiveData);
        
        return $this;
    }

    /**
     * Метод для создания архива(ов)
     */ 
    private function createArchivesWithData()
    {
        $zip = new ZipArchive;
        $zipPath = $this->getUploadedDir() . self::FOLDER_ALL . 'export_' . time() . '.zip';
        $this->prepareFolder(self::FOLDER_ALL);

        if ($zip->open($zipPath, ZipArchive::CREATE) && $this->archiveData )
        {
            foreach ($this->archiveData as $key => $filePath)
            {
                $fileName = basename($filePath); // Имя файла в архиве

                if($fileName)
                {
                    $zip->addFile($filePath, $fileName);

                    unset($this->archiveData[$key]);
                    $this->updateProgress($zipPath);

                    if($zip->numFiles > 1000) // Дробление архива по файлам
                    {
                        $zip->close();
                        array_push($this->archives, new File($zipPath));
                        return $this->createArchivesWithData();
                    }
                }
            }
            $zip->close();
            array_push($this->archives, new File($zipPath));
        }
    }

    public function getElementsForJob(string $status, int $profileId)
    {
        $elements = [];
        $requestProduct = new RequestProduct;

        if($status === 'deleted')
        {
            $elements = $requestProduct
                ->where([
                    ['profile_id', $profileId],
                    ['is_active', false]
                ])
                ->onlyTrashed()
                ->get();
            
        } elseif($status === 'active')
        {
            $elements = $requestProduct
                ->where([
                    ['profile_id', $profileId],
                    ['is_active', true]
                ])
                ->get();
                
        } elseif($status === 'inactive')
        {
            $elements = $requestProduct
                ->where([
                    ['profile_id', $profileId],
                    ['is_active', false]
                ])
                ->get();
        }
        
        return $elements;
    }
    
    private function getElementsForPage(string $status, array $elementsId, Profile $profile)
    {
        $elements = [];

        if($status === 'deleted')
        {
            $elements = $profile->requestProducts()
                ->onlyTrashed()
                ->whereIn('id', $elementsId)
                ->where('is_active', false)
                ->get();
            
        } elseif($status === 'active')
        {
            $elements = $profile->requestProducts()
                ->whereIn('id', $elementsId)
                ->where('is_active', true)
                ->get();
    
        } elseif($status === 'inactive')
        {
            $elements = $profile->requestProducts()
                ->whereIn('id', $elementsId)
                ->where('is_active', false)
                ->get();
        }
        return $elements;
    }
    
    /**
     * Получение названий изображений для таблицы
     */ 
    private function getCorrectImageNames(array $imagePaths): string
    {
        $massa = [];
        if(!current($imagePaths))
        {
            return '';
        }
        
        $correctData = '';

        foreach ($imagePaths as $key => $imagePath)
        {
            $tmp = explode('/', $imagePath);
            $correctData .= array_pop($tmp);
            unset($tmp);
            
            if(is_countable($imagePaths) && (int)$key < count($imagePaths) - 1)
            {
                $correctData .= ',';
            }
        }
        
        return $correctData;
    }
    
    /**
     * Формируем корректный путь до изображения
     */ 
    private function prepareImagePath($rawImagePath)
    {
        return $this->getUploadedDir() . $rawImagePath;
    }
    
    private function getUploadedDir(): string
    {
        return \Storage::disk( env('FILESYSTEM_DRIVER') )->path("public/");
    }

    private function updateProgress($zipPath)
    {
        $this->exportModel->progress = round(100 - (count($this->archiveData) / $this->countFilesInArchive * 100), 2); // Запись процентов
        $this->exportModel->save();
    }

    private function prepareFolder($folder)
    {
        $dirName = $this->getUploadedDir() . $folder;

        if( !is_dir( $this->getUploadedDir() . $folder ) )
        {
            @mkdir( $dirName , 0777, true);
            $permissions = substr(sprintf( '%o', fileperms( $dirName ) ), -4);
            
            if( (int) $permissions !== 0777)
            {
                @chmod($dirName, 0777);
            }
        }
    }
}
