<?php

namespace App\Helpers\RequestProduct;

use App\ApplicationDictionary;
use Illuminate\Http\File;
use App\Helpers\RequestProduct\RequestProductService;
use App\Extensions\Service as BaseService;
use Illuminate\Support\Str;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Style\Border;
use App\Models\{
    Import,
    RequestProduct,
    RequestProductProperty,
    RequestProductPrice,
    Category,
    Profile,
    Localisation,
    Currency,
    User,
};

class ImportRequestProductService extends BaseService
{

    const REQUEST_PRODUCT_IMAGE_PATH = 'public/request_products/';
    const IMPORT_PATH = 'public/imports/';

    const INTEGER_COLUMNS = [
        'price_min',
        'price_max',
        'min_order',
        'category'
    ];

    const ALLOWED_MIME_TYPES = [
        'image/jpg',
        'image/png',
        'image/jpeg',
        'text/plain',
        'text/csv',
        'application/vnd.ms-excel',
        'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
    ];

    private $profile; //Профиль
    private $importModel; //Модель импорта
    private $localisationId; //Id локализации
    private $requestProduct;

    private $tableHeaders; //Заголовки импортируемой таблицы
    private $tableData = []; //Данные таблицы
    private $previewImage; //Превью
    
    private $errors = []; //Массив с несозданными запросами на покупку
    private $className; //Вспомогательная переменная, нужна для того чтобы выбирать нужный класс в зависимости от расширения
    private $tableFile; //Файл таблицы
    private $importFolder; //Папка импорта, нужна чтобы добавлять изображения в базу (Надо скопировать их из неё в request_product)

    /**
     * Названия столбцов
     */
    public function tableHeaders()
    {
        return [
            'name' => __('cabinet.import.name'),
            'description' => __('cabinet.import.description'),
            'category' => __('cabinet.import.category'),
            'price_min' => __('cabinet.import.price_min'),
            'price_max' => __('cabinet.import.price_max'),
            'min_order' => __('cabinet.import.min_order'),
            'preview_image' => __('cabinet.import.preview_image'),
            'detail_images' => __('cabinet.import.detail_images'),
            'quantities' => __('cabinet.import.quantities')
        ];
    }

    /**
     * Столбцы обязательные для заполнения и с переводом ошибок
     */
    public function requiredHeaders()
    {
        return [
            'name' => __('cabinet.import.name_required'),
            'description' => __('cabinet.import.description_required'),
            'category' => __('cabinet.import.category_required'),
            'price_min' => __('cabinet.import.price_min_required'),
            'price_max' => __('cabinet.import.price_max_required'),
            'min_order' => __('cabinet.import.min_order_required'),
            'preview_image' => __('cabinet.import.preview_required'),
        ];
    } 
    
    /**
     * Получение дополнительных настроек импорта для запросов
     */
    public function settingsForRequestProducts(Profile $profile, int $importId) :array
    {
        $import = Import::find($importId);
        $importTable = $import::findImportTable($profile, $import);

        $importTable = \Storage::disk( env('FILESYSTEM_DRIVER') )
            ->path(
                'public/' . $importTable
            );

        $tableHeadings = $this->getHeaders($importTable);
        $allowedColumns = $this->tableHeaders();
        $localisationId = Localisation::where('code', app('translator')->getLocale() )->first()->id;

        $quantities = \App\ApplicationDictionary::where('group_code', 'quantities')
            ->where('localisation_id', $localisationId)
            ->get();

        return [
            'quantities' => $quantities,
            'allowedColumns' => $allowedColumns,
            'tableHeadings' => $tableHeadings
        ];
    }
    
    /**
     * Получение настроек импорта для запросов
     */
    public function saveFiles($importArchive, $tableFile, Profile $profile) :array
    {
        $this->profile = $profile;

        $zip = new \ZipArchive();
        $zip->open($importArchive->getRealPath());

        if($zip->numFiles === null)
        {
            return redirect()->back()->withErrors(['empty_archive' => __('cabinet.import.empty_archive') ]);
        }

        $this->importModel = new Import;
        $this->importModel->profile_id = $this->profile->id;
        $this->importModel->progress = 0;
        $this->importModel->save();

        // Перенос файлов из временной дирректории в директорию с импортами
        $this->importFolder = \Storage::disk(env('FILESYSTEM_DRIVER'))
            ->path(self::IMPORT_PATH . $this->profile->id ."/". $this->importModel->id);

        $zip->extractTo($this->importFolder);

        $tableName = $tableFile->getClientOriginalName();
        copy($tableFile, $this->importFolder. '/' .$tableName);

        $tablePath = $this->importFolder. '/' . $tableName;

        // Получение массива с изображениями
        $images = $this->filterFilesArchive();

        if ($images !== [])
        {
            $this->prepareImportFilesForDB($tableName, $images);
        }
        $tableHeadings = $this->getHeaders($tablePath);

        // Если заголовков из таблицы меньше чем должно быть
        if(count($tableHeadings) < count($this->requiredHeaders()))
        {
            $this->importModel ->delete();
            return ['errors' => ['few_columns' => __('cabinet.import.few_columns') ] ];
        }

        return ['import_id' => $this->importModel->id ];
        
    }

    /**
     * Получение файла со списком категорий
     */
    public function getExampleCats()
    {
        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();
        $tableHeaders = $this->categoriesExample();
        $categories = [];
        $localisationId = Localisation::where('code', app('translator')->getLocale())->first()->id;

        Category::where('is_active', true)->where('id','>', 1)
            ->with(['categoryProperties' =>  function ($query) use($localisationId) {
                $query
                    ->where([
                        ['code', 'name'],
                        ['localisation_id', $localisationId],
                    ]);
            }])->orderBy('id')
            ->get()
            ->mapWithKeys(function ($item) use (&$categories) {

                array_push($categories, [
                    'id' => $item->id,
                    'name' => $item->categoryProperties->first()->value
                ]);

                return[];
            });

        // имена колонок
        foreach ( $tableHeaders as $key => $value)
        {
            $sheet->setCellValue($key, $value)->getStyle($key)->getFont()->setBold(true);
        }

        $sheet->fromArray($categories, null, 'A2', false);
        
        //Стили
        $sheet->getDefaultRowDimension()->setRowHeight(20);
        $sheet->getDefaultColumnDimension()->setWidth(25);

        $writer = new \PhpOffice\PhpSpreadsheet\Writer\Xls($spreadsheet);

        $writer->save(\Storage::disk( env('FILESYSTEM_DRIVER') )->path( self::IMPORT_PATH  . "categories_list.xls"));

        return response()->download(\Storage::disk( env('FILESYSTEM_DRIVER') )->path(self::IMPORT_PATH  . "categories_list.xls"));
    }  

    /* 
     * Метод для получения заголовков из таблицы
     */
    public function getHeaders(string $tableFile) :array
    {
        $file = new File($tableFile);
        
        $className = 'PhpOffice\PhpSpreadsheet\Reader\\' .  ucfirst($file->getExtension());

        if(class_exists($className))
        {
            $reader = new $className;
            $reader->setReadDataOnly(true);
            $spreadsheet = $reader->load($tableFile);

            $lastColumn = $spreadsheet->getActiveSheet()->getHighestColumn();

            $headerArray = [];

            foreach (range('A',$lastColumn) as $columnIndex)
            {
                if( $spreadsheet->getActiveSheet()->getCell($columnIndex . 1)->getValue() !== null)
                {
                    $headColumn = strip_tags(
                        $spreadsheet->getActiveSheet()->getCell($columnIndex . 1)->getValue()
                    );

                    $headerArray[$columnIndex] = $headColumn;

                    unset($headColumn);
                }
            }

            return $headerArray;

        } else
        {
            return null;
        }
    }

    /*
     * Метод для записи в таблицу файлов импорта
     */
    private function associateImportFiles(array $importFiles)
    {
        foreach ($importFiles['table'] as $importTable)
        {
            $this->associateWithFiles( $this->importModel,
                [new File($importTable)],
                'import_table',
                $this->profile->users()->first()->id
            );
        }

        foreach ($importFiles['images'] as $importImage)
        {
            $this->associateWithFiles( $this->importModel,
                [new File($importImage)],
                'import_image',
                $this->profile->users()->first()->id
            );
        }

    }

    /*
     * Точка входа для джобика после выбора правильных параметров
     */
    public function start(array $settingsArray, int $profileId)
    {
        $this->profile = Profile::find($profileId);
        $this->importModel = Import::find($settingsArray['importId']);
        $this->importFolder = self::IMPORT_PATH . $this->profile->id. '/' . $this->importModel->id . '/';
        $this->localisationId = $this->profile->profileProperties()->first()->localisation_id;

        $this->tableHeaders = $settingsArray['headings'];
        
        $this->headerFilter();

        $this->tableFile = new File(
            \Storage::disk(env('FILESYSTEM_DRIVER'))
                ->path("public/". $settingsArray['tableName'] )
        );

        $this->className = 'PhpOffice\PhpSpreadsheet\Reader\\' .  ucfirst($this->tableFile->getExtension());

        if(class_exists($this->className))
        {
            $this->extractData();
            $this->writeData();

        } else
        {
            return abort(400);
        }
    }

    /*
     * Открытие импортируемой таблицы и получение данных оттуда
     */
    private function extractData()
    {
        $reader = new $this->className;
        $reader->setReadDataOnly(true);
        $spreadsheet = $reader->load($this->tableFile->getRealPath());

        $this->lastRow = $spreadsheet->getActiveSheet()->getHighestRow();
        $this->lastColumn = $spreadsheet->getActiveSheet()->getHighestColumn();

        $row = 2; //Номер строки начинается с двойки, потому что в первой строке заголовки

        while ($row <= $this->lastRow)
        {
            foreach (range('A', $this->lastColumn) as $columnIndex)
            {
                $this->tableData[$row][$columnIndex] = strip_tags(
                    $spreadsheet->getActiveSheet()
                        ->getCell($columnIndex . $row)
                        ->getValue()
                );
            }

            $this->checkDoubles($row);

            $row++;
        }

        unset($row);
    }

    /*
     * Создание запроса на покупку из данных, полученных с таблицы
     */
    private function writeData()
    {
        $countTableData = count($this->tableData); //Исходное количество данных в таблице

        foreach ($this->tableData as $rowNumber => $tableData)
        {
            if( $this->checkRequiredHeaders($tableData) !== false && $this->checkTypeOfData($tableData) !== false) //Проверка на то, заполнены ли обязательные колонки и проверка на их тип
            {
                $categoryId = Category::select('id')
                        ->where([
                            ['id',$tableData[ $this->tableHeaders['category'] ]],
                            ['is_active', true]
                        ])
                        ->whereNotNull('parent_id')
                        ->first();
                        
                //Готовим массив для создания запроса
                $data = [
                    'category_id' => $categoryId ? (int)$categoryId->id : 2,
                    'name' => $tableData[ $this->tableHeaders['name'] ],
                    'price_min' => (int)$tableData[ $this->tableHeaders['price_min'] ],
                    'price_max' => (int)$tableData[ $this->tableHeaders['price_max'] ],
                    'full_description' => $tableData[ $this->tableHeaders['description'] ],
                    'min_order' => [
                        'value' => (int)$tableData[ $this->tableHeaders['min_order'] ],
                        'type' => $this->checkQuantities($tableData),
                        ],
                ];

                \DB::transaction( function() use($data, $tableData)
                {
                    $this->createRequestProduct($data);

                    $this->writePreviewImage($tableData);

                    if( isset($tableData[ $this->tableHeaders['detail_images'] ])
                        && $tableData[ $this->tableHeaders['detail_images'] ] !== null
                        && is_string($tableData[ $this->tableHeaders['detail_images'] ]) !== false
                        )
                    {
                        $this->writeDetailImages($tableData[ $this->tableHeaders['detail_images'] ]);
                    }
                });
                
                unset($dataArray);
                unset($categoryId);

            } else
            {
                array_push($this->errors, $rowNumber);
            }

            $progress = round((count($this->tableData) / $countTableData * 100), 2); // Запись процентов
            $this->importModel->progress = $progress;
            $this->importModel->error_rows = $this->errors;
            //$this->importModel->request_products = $this->completed;
            $this->importModel->update();
            unset($tableData);
        }
    }

    /*
     * Создание массива изображений из колонки деталки
     * копирование изображений в папку запросов
     * запись в downloads
     */
    private function writeDetailImages(string $imageRow)
    {
        $detailImages = explode(';', $imageRow); //Колонку с названиями изображений разделяем по знаку

        foreach ($detailImages as $detailImage)
        {
            $imagePath = realpath(\Storage::disk(env('FILESYSTEM_DRIVER'))
                ->path($this->importFolder . $detailImage));
                
            if( is_file($imagePath) && basename($imagePath) !== null ) //Проверка на существование такого изображения в архиве
            {
                $imageFile = new File($imagePath);

                if($imageFile->extension() === 'png'
                    || $imageFile->extension() === 'jpg'
                    || $imageFile->extension() === 'jpeg')
                {
                    $newPath =  \Storage::disk(env('FILESYSTEM_DRIVER'))
                        ->path(
                            self::REQUEST_PRODUCT_IMAGE_PATH . $this->profile->id . '/'
                            . \Str::random(10) . '.' . $imageFile->extension()
                        );
                    
                    copy( $imagePath, $newPath);

                    $imageFile = new File($newPath);
                    unset($newPath);

                    array_push($detailArray, $imageFile);
                }
                unset($imageFile);
            }
            unset($imagePath);
        }
        if($detailArray !== null)
        {
            $this->associateWithFiles($this->requestProduct,
                $detailArray,
                'detail_images',
                $this->profile->users()->first()->id
            );
        }
    }

    /*
     * Запись изображения из колонки превью
     * копирование изображения в папку запросов
     * запись в downloads
     */
    private function writePreviewImage(array $tableData)
    {
        $previewImage = realpath(\Storage::disk(env('FILESYSTEM_DRIVER'))
            ->path($this->importFolder . $tableData[ $this->tableHeaders['preview_image'] ]));

        if(is_file($previewImage) && basename($previewImage) !== null)
        {
            $imageFile = new File($previewImage);

            if($imageFile->extension() === 'png'
                || $imageFile->extension() === 'jpg'
                || $imageFile->extension() === 'jpeg')
            {
                $newPath = \Storage::disk( env('FILESYSTEM_DRIVER') )
                    ->path(
                        self::REQUEST_PRODUCT_IMAGE_PATH . $this->profile->id . '/'
                        . \Str::random(10) . '.' . $imageFile->extension()
                    );

                copy( $previewImage, $newPath);

                $this->previewImage = new File($newPath);
                unset($newPath); 

                $this->associateWithFiles($this->requestProduct,
                    [ $this->previewImage ],
                    'preview_image',
                    $this->profile->users()->first()->id
                );
            }
            unset($imageFile);
        }
    }

    /*
     * Метод содержит проверку на обязательные заголовки
     */
    private function checkRequiredHeaders(array $tableData) : bool
    {
        $requiredHeaders = $this->requiredHeaders();

        foreach ($requiredHeaders as $key => $value)
        {
            if( !isset($tableData[ $this->tableHeaders[$key] ]) )
            {
                return false;
            }
        }

        // Если превью заполнено в таблице, но не существует этого файла
        $previewImage = \Storage::disk(env('FILESYSTEM_DRIVER'))
            ->path($this->importFolder . $tableData[ $this->tableHeaders['preview_image'] ]);

        if( !is_file($previewImage) )
        {
            return false;
        }
        
        return true;    
    }

    /*
     * Метод содержит проверку на типы данных
     */
    private function checkTypeOfData(array $tableData) : bool
    {

        $errorsArray = [];
        
        foreach (self::INTEGER_COLUMNS as $integerColumn)
        {
            if( filter_var($tableData[ $this->tableHeaders[$integerColumn] ], FILTER_SANITIZE_NUMBER_INT) !== "")
            {
                $tableData[ $this->tableHeaders[$integerColumn] ] = (int)filter_var($tableData[ $this->tableHeaders[$integerColumn] ], FILTER_SANITIZE_NUMBER_INT);
            
            } else 
            {
                array_push($errorsArray, $integerColumn);
            }

        }

        if ($errorsArray === [])
        {
            return true;

        }

        return false;
    }

    /*
     * Метод содержит проверку на дубли
     */
    private function checkDoubles(int $row) :bool
    {
        foreach ($this->tableData as $key => $value)
        {
            if( $this->tableData[$row] === $this->tableData[$key] && $row !== $key)
            {
                unset($this->tableData[$row]);
                return false;
            }
        }
        return true; 
    }

    /*
     * Метод исправляет содержимое заголовков
     */
    private function headerFilter()
    {
        foreach ($this->tableHeaders as $key => $value)
        {
            $this->tableHeaders[$key] = strip_tags($value);
        }
    }
    
    /**
     * Создание записей в таблице downloads для модели импорта
     */
    private function prepareImportFilesForDB( string $tableName, array $images)
    {
        $importFiles = [
            'table' => [],
            'images' => [],
        ];

        array_push( $importFiles['table'], new File($this->importFolder . '/' . $tableName));

        foreach ($images as $image)
        {
            array_push( $importFiles['images'], new File($this->importFolder . '/' . $image));
        }

        $this->associateImportFiles($importFiles);
    }

    /**
     * Фильтрация файлов из архива
     */
    private function filterFilesArchive() :array
    {
        $filesFolder = scandir($this->importFolder);
        $images = [];

        foreach ($filesFolder as $file)
        {
            if( is_file($this->importFolder . '/'. $file) )
            {
                $mimeType = (new File($this->importFolder . '/'. $file))->getMimeType();
                if(in_array($mimeType, self::ALLOWED_MIME_TYPES)) //Доп проверка на mime тип
                {
                    array_push($images, $file);

                } else
                {
                    unlink($this->importFolder . '/'. $file);
                }
                unset($mimeType);
            }
        }

        return $images;
    }

    /*
     * Создание запроса на покупку
     */
    private function createRequestProduct(array $data)
    {
        $reqProduct = $this->profile->requestProducts()->create([
            'category_id' => (int)$data['category_id'],
            'link' => Str::slug($data['name'], '-'),
            'sort' => 1
        ]);

        unset($data['category_id']);
        
        //сохранение характеристик, в дальнейшем используемых при фильтрации
        $reqProduct->characteristics()->createMany( $this->createCharacteristics($data) ); 
        
        // сохранение цена на продукт
        $reqProduct->requestProductPrices()->create([
            'currency_id' => Currency::where('is_base', true)->first()->id,
            'min' => $data['price_min'] ?? 0,
            'max' => $data['price_max'] ?? 0,
        ]);
        
        unset($data['price_min'], $data['price_max']);
        
        //сохранение свойств
        $reqProduct->requestProductProperties()->createMany( $this->createProperties($data) );

        $this->requestProduct = $reqProduct;

    }

    /*
     * Создание массива с правильными характеристиками
     */
    private function createCharacteristics(array &$data) :array
    {
        $correctData = [];

        foreach ($data as $code => $value)
        {
            if($value && in_array($code, RequestProductService::ALLOWED_CHARACTERISTICS))
            {
                $correctValue = (is_array($value) && array_key_exists('value', $value)) ? $value['value'] : $value;

                if(!$correctValue)
                {
                    unset($data[$code]);
                    continue;
                }
                
                array_push($correctData, [
                    'code' => $code,
                    'value' => $correctValue,
                    'profile_id' => $this->profile->id,
                    'type' => $value['type'] ?? '',
                ]);
                
                unset($data[$code]);
            }
        }

        return $correctData;
    }

    /*
     * Создание массива со свойствами запроса на покупку
     */
    private function createProperties(array $data) :array
    {
        $correctData = [];
        
        foreach ($data as $code => $value)
        {
            if($value)
            {
                array_push($correctData, [
                    'localisation_id' => $this->localisationId,
                    'code' => $code,
                    'value' => $value,
                ]);
            }
        }

        return $correctData;
    }

    /*
     * Массив с шапкой для таблицы со списком категорий
     */
    private function categoriesExample() :array
    {
        return [
            'A1' => __('cabinet.import.category_id'),
            'B1' => __('cabinet.import.category'),
        ];
    }

    /*
     * Проверка единиц измерения из колонки таблицы
     */
    private function checkQuantities(array $tableData) :string
    {
        if (isset($tableData[ $this->tableHeaders['quantities'] ]))
        {
            $avialableQuantities = ApplicationDictionary::where('group_code','quantities')
                ->where('localisation_id', $this->localisationId)
                ->get()
                ->pluck('code')->toArray();


            if( in_array( $tableData[ $this->tableHeaders['quantities'] ], $avialableQuantities) )
            {
                return $tableData[ $this->tableHeaders['quantities'] ];
            }
        }

        return 'pcs'; 
    }
}
