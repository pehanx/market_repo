<?php

namespace App\Helpers\Manager;

use App\User;
use App\Models\{Profile, Localisation, ProfileType, ProfileProperty};
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use App\Helpers\Account\AccountService;
use App\Events\Profile\ProfileAdded;

class ManagerService
{
    private $mailer;

    public function __construct(AccountService $service)
    {
        $this->service = $service;
    }
    
    /**
     * Создаем пользователя и привязываем его к профилю компании(у которого тип buyer или seller)
     * Создаем аккаунт пользователя (привязываем пользователю профиль типа account)
     * Добавляем прав пользователю в зависимости от типа профиля компании (seller_manager или buyer_manager)
     *
     * @param App\Models\Profile $profile - профиль компании
     * @param array $request
     *
     * @return App\Models\Profile $profile - account менеджера
     */
    public function create(Profile $profile, array $request)
    {
        $user = $profile->users()->create([
            'name' => $request['name'],
            'email' => $request['email'],
            'password' => Hash::make($request['password']),
            'registration_type' => 'native',
            'ip' => \Request::ip(),
        ]);
        unset($request['name'], $request['email'], $request['password']);
        
        $manager = $user->profiles()->create([
            'profile_type_id' => ProfileType::where('code', 'account')->first()->id,
            'status' => 'unactive'
        ]);
            
        $manager->profileProperties()->createMany( $this->service->prepareProperties($request) );
       
        $role = ProfileType::where('id', $profile->profile_type_id)->first()->code;
        $role .= '_manager';
        
        event( new ProfileAdded( $profile, $role, $user ));
        
        /*
        $message = [
            'title' => __('notification.'),
            'body' => __('notification.'),
            'type' => 'info'
        ];
        
        \Session::flash('message', $message);
        
        */
        return  $manager; 
    }
    
    public function checkUser(User $user, Profile $profile): bool
    {
        if(in_array($user->id, $profile->users->pluck('id')->toArray()) && $profile->status === 'active')
        {
            return true;
        }
        
        return false;
    }
    
    /**
     * Обновление email и пароля менеджера.
     *  
     *
     * @param App\User $user
     * @param array $data
     *
     * @return void
     */
    public function updateUser(User $user, array $data): void
    {
        $message = '';
        
        if( $data['password'] === $data['password_confirmation'] )
        {
            $user->email =  $data['email'];
            $user->password = bcrypt($data['password']);
            $user->save();
            
        }else
        {
            $message = [
                'title' => __('notification.change_entrance_data'),
                'body' => __('notification.dont_update_entrance_data_notify'),
                'type' => 'info'
            ];
        }
        
        if(!$message)
        {
            $message = [
                'title' => __('notification.change_entrance_data'),
                'body' => __('notification.success_update_entrance_data_notify'),
                'type' => 'info'
            ];
        }
        
        \Session::flash('message', $message);
    }
    
    public function update(Profile $manager, array $request)
    {
        $user = $manager->users->first();
        $this->service->update($user, $request);
    }
    
    public function destroy(Profile $manager): bool
    {
        $manager->delete();
        return true; 
    }
}
