<?php

namespace App\Helpers\Currency;

use App\Jobs\CurrencyRatesJob;
use App\Services\Currency\OExchangeCurrency;

class CurrencyService
{
   public function updateCurrencyRates()
   {
      CurrencyRatesJob::dispatchNow();
   }
}