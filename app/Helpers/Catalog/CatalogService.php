<?php

namespace App\Helpers\Catalog;

class CatalogService
{
    public function explodeOrderByParams(array $orderBy)
    {
        list($orderBy['field'], $orderBy['direction']) = explode('_', $orderBy['order_by']);
        unset($orderBy['order_by']);

        return $orderBy;
    }
}
