<?php

namespace App\Helpers\Profile;

use App\User;
use App\Models\{Profile, Localisation, ProfileType, ProfileProperty};
use App\Events\Profile\{ProfileAdded, ProfileDeleted};
use App\Events\Chat\{ChatSupportSend, ChatSupportPush};
use App\Helpers\Chat\ChatService;
use Illuminate\Contracts\Mail\Mailer;// Мб в будущем будем использовать другой метод отправки уведомлений
use App\Mails\Profile\ApproveUserProfileMail;
use Illuminate\Database\Eloquent\Collection;

class ProfileService
{
    private $mailer;
    
    const ALLOWED_FILES = [
        'company_logo_image', 
        'company_main_banner_images',
        'company_promo_banner_images',
        'company_description_image', 
        'company_other_images',
        'company_video'
    ];
    
    const ALLOWED_TYPES = [
        'seller', 
        'buyer',
    ];
    
    const ALLOWED_CHARACTERISTICS = [
        "country",
    ];
    
    public function __construct(Mailer $mailer, ChatService $chatService)
    {
        $this->mailer = $mailer;
        $this->chatService = $chatService;
    }
    
    public function create(User $user, array $request): Profile
    {
        $profile = $user->profiles()->create([
            'profile_type_id' => ProfileType::where('code', $request['profile_type'])->first()->id,
            'status' => 'inactive'
        ]);
        
        $profile->characteristics()->createMany( $this->prepareCharacteristics($request, (int)$profile->id) );
        
        $profile->profileProperties()->createMany( $this->prepareProperties($request) );

        event( new ProfileAdded( $profile, $request['profile_type'], $user ));
        
        $message = [
            'title' => __('notification.approve_profile'),
            'body' => __('notification.need_approve_notify'),
            'type' => 'info'
        ];
        
        \Session::flash('message', $message);

        return  $profile;
    }
    
    public function update(Profile $profile, array $request): Profile
    {
        $profile->characteristics()->delete(); 
        $profile->characteristics()->createMany( $this->prepareCharacteristics($request, (int)$profile->id) );
        
        // Удаляем старые характеристики профиля для необходимой локали
        ProfileProperty::where('profile_id', $profile->id)
            ->where('localisation_id', Localisation::where('code', $request['localisation'])->first()->id)
            ->delete();
           
        // Заполняем обновленными характеристиками
        $profile->profileProperties()->createMany( $this->prepareProperties($request) );
        
        /* $message = [
            'title' => __('notification.approve_profile'),
            'body' => __('notification.need_approve_before_edit_notify'),
            'type' => 'info'
        ];
        
        \Session::flash('message', $message); */

        return  $profile;
    }

    public function destroy(Profile $profile, User $user): bool
    {
        $activeProfile = \Session::get('active_profile');
        
        if($activeProfile && $activeProfile->id === $profile->id)
        {
            \Session::forget('active_profile');
        }
        
        $profileClone = $profile->replicate();
        $profile->delete();
        
        event( new ProfileDeleted( $profileClone, $user));
        
        return true;
    }
    
    public function approval(Profile $profile, User $user, Collection $profileDocuments): Profile
    {
        $profile->status = 'wait';
        $profile->save();
        
        $this->mailer->to(User::role('admin')->first()->email)->send(new ApproveUserProfileMail($user, $profile, $profileDocuments));

        $approvalData = $this->prepareApprovalData($profile, $profileDocuments);

        $chat = $this->chatService->getAdminChatTemplate();

        event(new ChatSupportSend(
            $chat['id'], //id чата
            $approvalData,
            'approval'
        ));
        
        $message = [
            'title' => __('notification.approving_profile'),
            'body' => __('notification.approving_profile_notify'),
            'type' => 'info'
        ];
         
        \Session::flash('message', $message);
        
        return $profile;
    }

    private function prepareApprovalData($profile, $profileDocuments) : string
    {
        $approvalData = "";

        $approveRoute = route('admin.profiles.index', ['id' => $profile->id]);

        $profileDocuments->each(function ($document, $key) use(& $approvalData){
           $approvalData .= "<a href='" . asset('storage/' . $document->path ) . "'>$document->title</a><br>";
        });

        $approvalData .= "<a href='$approveRoute'>Подтвердить</a>";

        return $approvalData;
        
    }
    
    private function prepareProperties(array $data): array
    {
        $correctData = [];
        $localisationId = Localisation::where('code', $data['localisation'])->first()->id; 
        
        foreach ($data as $code => $value)
        {
            if( $value && !in_array( $code, ['profile_type', 'localisation']) )
            {
                array_push($correctData, [
                    'localisation_id' => $localisationId,
                    'code' => $code,
                    'value' => $value
                ]);
            }
        }
        
        return $correctData;
    }
    
    private function prepareCharacteristics(array &$data, $profileId): array
    {
        $correctData = [];

        foreach ($data as $code => $value)
        {
            if($value && in_array($code, self::ALLOWED_CHARACTERISTICS))
            {
                $correctValue = (is_array($value) && array_key_exists('value', $value)) ? $value['value'] : $value;

                if(!$correctValue)
                {
                    unset($data[$code]);
                    continue;
                }
                
                array_push($correctData, [
                    'code' => $code,
                    'value' => $correctValue,
                    'profile_id' => $profileId,
                    'type' => $value['type'] ?? '',
                ]);
                
                unset($data[$code]);
            }
        }
        //dump($correctData);exit();
        return $correctData;
    }
}
