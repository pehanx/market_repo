<?php

namespace App\Helpers\Page;

use App\User;
use App\Models\{Page};

class PageService
{
    public function create(User $user, array $request): Page
    {
        $page = $user->pages()->create([
            'link' => $request['link'],
            'is_active' => true,
            'parent_id' => $request['parent'],
            'sort' => 500
        ]);
        
        unset($request['link'], $request['parent']);
        
        $page->pageProperties()->createMany( $this->prepareProperties($request) );

        return  $page;
    }
    
    public function addProperties(Page $page, array $request): Page
    {
        $page->pageProperties()->createMany( $this->prepareProperties($request) );

        return  $page;
    }
    
    private function prepareProperties(array $data): array
    {
        $correctData = [];
        $localisationId = $data['localisation']; 
        unset($data['localisation']);
        
        foreach ($data as $code => $value)
        {
            if($value)
            {
                array_push($correctData, [
                    'localisation_id' => $localisationId,
                    'code' => $code,
                    'value' => $value
                ]);
            }
        }
        
        return $correctData;
    }
    
    /* public function update(Profile $profile, array $request): Profile
    {
        // Удаляем старые характеристики профиля для необходимой локали
        ProfileProperty::where('profile_id', $profile->id)
            ->where('localisation_id', Localisation::where('code', $request['localisation'])->first()->id)
            ->delete();
           
        // Заполняем обновленными характеристиками
        $profile->profileProperties()->createMany( $this->prepareProperties($request) );
        
        $message = [
            'title' => __('notification.approve_profile'),
            'body' => __('notification.need_approve_before_edit_notify'),
            'type' => 'info'
        ];
        
        \Session::flash('message', $message);

        return  $profile;
    }

    public function destroy(Profile $profile, User $user): bool
    {
        $profileClone = $profile->replicate();
        $profile->delete();

        return true;
    } */
}
