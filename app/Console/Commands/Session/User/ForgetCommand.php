<?php 

namespace App\Console\Commands\Session\User;

use Illuminate\Console\Command;

use App\User;

class ForgetCommand extends Command
{

    protected $signature = 'session:forget-user {--id=}';
    
    protected $description = 'Delete session by user id';

    public function handle()
    {
        $id = $this->option('id');

        $user = User::find($id);

        $result = false;

        if(!!$user)
        {
            session()->getHandler()->destroy( $user->session_id );

            $user->fill(['session_id' => null])->save();

            $this->info("User '{$id}' with name '{$user->name}' session successfully deleted");
            $result = true;
        
        } else 
        {
            $this->error("User '{$id}' not found");
        }

        return $result;
    }
}
