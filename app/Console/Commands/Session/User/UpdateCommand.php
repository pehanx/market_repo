<?php 

namespace App\Console\Commands\Session\User;

use Illuminate\Console\Command;

use App\User;

class UpdateCommand extends Command
{
    protected $signature = 'session:update-users';
    
    protected $description = 'Update session_id in database for all users';

    public function handle()
    {
        $sessionIds = array_diff(
            User::whereNotNull('session_id')->select('session_id')->pluck('session_id')->all(),
            \Redis::connection('sessions')->keys('*')
        );
        
        User::whereIn('session_id', $sessionIds)->update(['session_id' => null]);

        $this->info("All inactive users session_id now is null");
        $result = true;

        return $result;
    }
}
        