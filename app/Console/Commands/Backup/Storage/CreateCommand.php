<?php 

namespace App\Console\Commands\Backup\Storage;

use Illuminate\Console\Command;
use Symfony\Component\Process\Process;
use Carbon\Carbon;

class CreateCommand extends Command
{

    protected $signature = 'backup:storage-create';
    
    protected $description = 'Create backup archive for public';

    public function handle()
    {
        $result = false;

        $command = 'tar';

        $folderPath = storage_path() . '/backups/backup-' . Carbon::now()->format('Y-m-d') . '.tar.gz';

        $publicPath = \Storage::disk('public')->getAdapter()->getPathPrefix();

        $process = new Process([$command, '-czvf', $folderPath, $publicPath]);

        $process->start();

        $process->wait(function ($type, $buffer) {
            if (Process::ERR === $type) {

                $this->error("Backup for 'public' not created: $buffer");
                
                return $result;
            }
        });

        $this->info("Backup for 'public' created successfully");

        $result = true;

        return $result;
    }
}
