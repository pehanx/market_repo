<?php 

namespace App\Console\Commands\Backup\Database;

use Illuminate\Console\Command;
use Symfony\Component\Process\Process;
use Carbon\Carbon;

class CreateCommand extends Command
{

    protected $signature = 'backup:database-create';
    
    protected $description = 'Create backup for database';

    public function handle()
    {
        $result = false;

        $command = 'pg_dump';

        $folderPath = storage_path() . '/backups/dump-' . Carbon::now()->format('Y-m-d') . '.sql';

        //exec("$command -C -h postgres -p 543 -U batkov marketplace -f $folderPath");

        //exec("$command -C localhost:5432:marketplace:batkov:nitroglicerin marketplace -f $folderPath");

        $process = new Process(
            [
                $command , '-C', '-h', env('DB_HOST') ,'-U', env('DB_USERNAME'), env('DB_DATABASE'), '-f', $folderPath
            ],

            null, 

            ['PGPASSWORD' => env('DB_PASSWORD')]
        );


        $process->start();

        $process->wait(function ($type, $buffer) {
            if (Process::ERR === $type) {

                $this->error("Backup for 'database' not created: $buffer");
                
                return $result;
            }
        });

        $this->info("Backup for 'database' created successfully");

        $result = true;

        return $result;
    }
}
