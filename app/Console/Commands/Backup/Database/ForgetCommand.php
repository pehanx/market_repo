<?php 

namespace App\Console\Commands\Backup\Database;

use Illuminate\Console\Command;
use Symfony\Component\Process\Process;
use Carbon\Carbon;

class ForgetCommand extends Command
{

    protected $signature = 'backup:database-remove';
    
    protected $description = 'Create backup for database';

    public function handle()
    {
        $result = false;

        $command = 'rm';

        // Запускаем просмотр папки чтобы показать список файлов в папке бекапа
        $listFilesProcess = new Process(
            [
                'ls', storage_path() . "/backups"
            ]
        );

        $listFilesProcess->run();

        $allFiles = array_filter( explode("\n", $listFilesProcess->getOutput()), 'strlen'); //Вывод ls из строки превращаем в массив

        $sqlFiles = array_filter($allFiles, [$this, 'getFiles']); //Получить файлы из папки с бекапами по маске

        usort($sqlFiles, [$this, "sortFiles"]); // сортировка массива по дате в названии файлов

        $sqlFiles = array_map([$this, 'addRootFolderToFile'], $sqlFiles); // Файлы для удаления

        $processArray = [$command, '-rf']; // Массив команды с аргументами

        foreach ($sqlFiles as $file)
        {
            array_push($processArray,$file);
        }

        //Удаляем все бекапы кроме последнего
        $process = new Process($processArray);

        $process->start();

        $process->wait(function ($type, $buffer) {
            if (Process::ERR === $type) {

                $this->error("All backups for 'database' not removed: $buffer");
                
                return $result;
            }
        });

        $this->info("All previous backups for 'database' was removed successfully");

        $result = true;

        return $result;
    }

    /* 
     * Получаем файлы по маске для того чтобы не удалить бекап стораджа
     */
    private function getFiles($file)
    {
        if( preg_match('/dump-\d\d\d\d-\d\d-\d\d.sql/', $file) )
        {
            return $file;
        }
    }

    /* 
     * Сортировка по дате
     */
    private function sortFiles($file, $file2)
    {
        preg_match('/dump-(\d\d\d\d-\d\d-\d\d).sql/', $file, $date);

        preg_match('/dump-(\d\d\d\d-\d\d-\d\d).sql/', $file2, $date2);
            
        return strtotime(Carbon::parse($date[1])->toDateTimeString()) - strtotime(Carbon::parse($date2[1])->toDateTimeString());
    }

    /* 
     * Добавление корневого пути к файлу из массива
     */
    private function addRootFolderToFile($file)
    {
        $file = storage_path() . '/backups/' . $file;

        return $file;
    }
}
