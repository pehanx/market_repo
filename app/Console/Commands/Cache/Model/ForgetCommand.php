<?php 

namespace App\Console\Commands\Cache\Model;

use Illuminate\Console\Command;
use Cache;

class ForgetCommand extends Command
{
    protected $signature = 'cache:forget-model {--model=}';
    
    protected $description = 'Flush cache for a given model classname, where tag = classname.';

    public function handle()
    {
        $model = $this->option('model');
        $result = false;
        
        if (class_exists($model)) 
        {
            Cache::tags($model)->flush();
            $this->info("Model '{$model}' cache successfully flushed");
            $result = true;
            
        } else 
        {
            $this->error("Model '{$model}' not found");
        }

        return $result;
    }
}
