<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->call('App\Helpers\Currency\CurrencyService@updateCurrencyRates')->weekly();

        $schedule->command('session:update-users')->everyMinute(); // Для удаления старых сессий пользователей
        $schedule->command('backup:database-create')->weekly(); // Для бекапа БД
        $schedule->command('backup:storage-create')->weekly();  // Для бекапа файлов
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
