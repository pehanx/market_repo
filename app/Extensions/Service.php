<?php

namespace App\Extensions;

use App\Traits\Downloadables;

abstract class Service 
{
    use Downloadables;
}
?>