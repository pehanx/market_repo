<?php

namespace App\Events\Profile;

use App\Models\Profile;
use App\User;
use Illuminate\Queue\SerializesModels;

class ProfileApproved
{
    use SerializesModels;

    public $profile;
    public $profileDocuments;
    public $user;

    public function __construct(Profile $profile, array $profileDocuments, User $user)
    {
        $this->profile = $profile;
        $this->profileDocuments = $profileDocuments;
        $this->user = $user;
    }
}
