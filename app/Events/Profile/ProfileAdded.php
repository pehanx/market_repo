<?php

namespace App\Events\Profile;

use App\Models\Profile;
use App\User;
use Illuminate\Queue\SerializesModels;

class ProfileAdded
{
    use SerializesModels;

    public $profile;
    public $profileType;
    public $user;

    public function __construct(Profile $profile, string $profileType, User $user)
    {
        $this->profile = $profile;
        $this->profileType = $profileType;
        $this->user = $user;
    }
}
