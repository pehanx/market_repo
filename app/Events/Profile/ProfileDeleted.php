<?php

namespace App\Events\Profile;

use App\Models\Profile;
use App\User;
use Illuminate\Queue\SerializesModels;

class ProfileDeleted
{
    use SerializesModels;

    public $profile;
    public $user;

    public function __construct(Profile $profile, User $user)
    {
        $this->profile = $profile;
        $this->user = $user;
    }
}
