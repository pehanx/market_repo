<?php

namespace App\Events\Chat;

use Illuminate\Queue\SerializesModels;

class ChatPush
{
    use SerializesModels;

    public $currentProfile;
    public $toProfile;
    public $chatId;
    public $text;

    public function __construct(int $currentProfile, int $toProfile, $chatId, string $text)
    {
        $this->currentProfile = $currentProfile;
        $this->toProfile = $toProfile;
        $this->chatId = $chatId;
        $this->text = $text;
    }
}