<?php

namespace App\Events\Chat;

use Illuminate\Queue\SerializesModels;

class ChatSupportPush
{
    use SerializesModels;

    public $chatId;
    public $text;

    public function __construct(int $chatId, string $text)
    {
        $this->chatId = $chatId;
        $this->text = $text;
    }
}