<?php

namespace App\Events\Chat;

use Illuminate\Queue\SerializesModels;

class ChatSupportSend
{
    use SerializesModels;

    public $chatId;
    public $text;
    public $type;

    public function __construct(int $chatId, string $text, $type = null)
    {
        $this->chatId = $chatId;
        $this->text = $text;
        $this->type = $type;
    }
}