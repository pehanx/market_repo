<?php

namespace App\Events\Chat;

use Illuminate\Queue\SerializesModels;

class ChatDBSend
{
    use SerializesModels;

    public $currentProfile;
    public $chatId;
    public $text;

    public function __construct(int $currentProfile, $chatId, string $text)
    {
        $this->currentProfile = $currentProfile;
        $this->chatId = $chatId;
        $this->text = $text;
    }
}