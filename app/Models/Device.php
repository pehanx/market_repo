<?php

namespace App\Models;

use App\Extensions\Model;

class Device extends Model
{
    protected $guarded = ['id'];
	
	public function users()
    {
        return $this->belongsToMany('App\User');
    }
}