<?php

namespace App\Models;

use App\Extensions\Model;

class CategoryProperty extends Model
{    
    protected $guarded = ['id'];
    
    public function localisations()
    {
        return $this->belongsTo('App\Models\Localisation', 'localisation_id');
    }
    
    public function categories()
    {
        return $this->belongsTo('App\Models\Category', 'category_id');
    }
}
