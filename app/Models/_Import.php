<?php

namespace App\Models;

use App\Extensions\Model;

class Import extends Model
{
    protected $guarded = ['id'];
    
    const IMPORTED_ENTITY = [
        'product',
        'request_product'
    ];
    
    public function profiles()
    {
        return $this->belongsTo('App\Models\Profile', 'profile_id');
    }

    public function downloads()
    {
        return $this->morphToMany('App\Models\Download', 'downloadable');
    }

    static function getDataForProfile(Profile $profile)
    {
        return $profile->imports()
            ->select('id', 'progress', 'created_at')
            ->with('downloads')
            ->orderBy('created_at', 'desc')
            ->orderBy('updated_at', 'desc')
            ->get();
            /*->transform( function ($item, $key) {

                $item->error_rows =  json_decode($item->error_rows);
                $item->entities = json_decode($item->entities);
                return $item;
                
            }*/
    }

    static function findIdForProfile(Profile $profile, int $importId)
    {
        return $profile->imports()
            ->where('id', $importId)
            ->with('downloads')
            ->first();
    }

    static function findImportTable(Profile $profile, Import $import)
    {
        return $import->downloads
            ->where('type','import_table')
            ->first()->path;
            
    }

    static function getFilesForDownload(Profile $profile, int $importId)
    {
        $exports = $profile
            ->imports()
            ->where('id', $exportId)
            ->with('downloads')
            ->first();

        $exportFilePaths = $exports
            ->downloads()
            ->pluck('path')
            ->all();

        return $exportFilePaths;
    }
}
