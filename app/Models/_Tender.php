<?php

namespace App\Models;

use App\Extensions\Model;

class Tender extends Model
{
    protected $guarded = ['id'];
	
    public function profiles()
    {
        return $this->belongsTo('App\Models\Profile', 'profile_id');
    }
    
    public function tenderProperties()
    {
        return $this->hasMany('App\Models\TenderProperty');
    }
    
    public function tenderParticipants()
    {
        return $this->hasMany('App\Models\TenderParticipant');
    }
}
