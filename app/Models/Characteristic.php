<?php

namespace App\Models;

use App\Extensions\Model;

class Characteristic extends Model
{
    protected $guarded = ['id'];
     
    public function products()
    {
        return $this->morphedByMany('App\Models\Product', 'characteristicable');
    }
    
    public function productModels()
    {
        return $this->morphedByMany('App\Models\ProductModel', 'characteristicable');
    }
    
    public function requestProducts()
    {
        return $this->morphedByMany('App\Models\RequestProduct', 'characteristicable');
    }
    
    public function categories()
    {
        return $this->morphedByMany('App\Models\Category', 'characteristicable');
    } 
}
