<?php

namespace App\Models;

use App\Extensions\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Message extends Model
{
    use SoftDeletes;
    
    protected $guarded = ['id'];
	
	public function chats()
    {
        return $this->belongsTo('App\Models\Chat', 'chat_id');
    }
}
