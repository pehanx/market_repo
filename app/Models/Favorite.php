<?php

namespace App\Models;

use App\Extensions\Model;
use Illuminate\Database\Eloquent\{SoftDeletes, Builder};
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Facades\DB;

class Favorite extends Model
{
    use SoftDeletes;
    
    protected $guarded = ['id'];
    private static $filters = [];
    
	public function users()
    {
        return $this->belongsTo('App\User', 'user_id');
    }
    
    public function favoriteable()
    {
        return $this->morphTo();
    }
    
    public function getProductAttributes()
    {
        $data = null;
        
        if($this->favoriteable_type === Product::class)
        {
            $data = $this->favoriteable;
        }
        
        return $data;
    }

    public function getRequestProductAttributes()
    {
        $data = null;
        
        if($this->favoriteable_type === RequestProduct::class)
        {
            $data = $this->favoriteable;
        }
        
        return $data;
    }
    
    static function getNamespacePrefix()
    {
        return __namespace__ . '\\';
    }
    
    static function getFiltrateFavoritablesForUser(HasMany $favorites, array $request, string $favoriteableModelName, string $favoriteableName)
    {
        self::$filters = $request;
        
        return $favorites
            ->whereHasMorph('favoriteable', self::getNamespacePrefix() . $favoriteableModelName, function (Builder $query) use($favoriteableName) {
                $query
                    // По умолчанию берем все (активные и неактивные)
                    ->when(empty(self::$filters['is_active']), function(Builder $query) {
                        return $query
                            ->whereIn('is_active', [true, false]);
                    })
                    ->when(!empty(self::$filters['is_active']), function(Builder $query) {
                        return $query
                            ->where('is_active', self::$filters['is_active']);
                    })
                    ->when(!empty(self::$filters['name']), function(Builder $query) use($favoriteableName) {
                        return $query
                            ->whereHas($favoriteableName . 'Properties', function (Builder $query){
                                $query
                                    ->where([
                                        ['code', 'name'],
                                        [DB::raw('LOWER(value)'), 'like', '%'.mb_strtolower(self::$filters['name']).'%']
                                    ]);
                            });
                    })
                    ->when(!empty(self::$filters['category_id']), function(Builder $query) {
                        return $query
                            ->whereHas('categories', function (Builder $query){
                                $query
                                    ->where('id', self::$filters['category_id']);
                            });
                    });
            });
    }
    
    static function getNeededFavorites(array $filtrateFavoritableIds, string $favoriteableModelName, int $appLocalisationId)
    {
        $method = 'getAll' . $favoriteableModelName . 'ForFavorites';

        if(method_exists( (self::getNamespacePrefix() . $favoriteableModelName), $method ) )
        {
            return $favorites = (self::getNamespacePrefix() . $favoriteableModelName)::$method($filtrateFavoritableIds, $appLocalisationId);
            
        } else
        {
            abort(422);
        }
    }
}