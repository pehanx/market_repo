<?php

namespace App\Models;

use App\Extensions\Model;
use Kalnoy\Nestedset\NodeTrait;
use Illuminate\Database\Eloquent\{SoftDeletes, Builder};
use Cache;

class Category extends Model
{
    use NodeTrait, SoftDeletes;

    protected $guarded = ['id'];

	public function profiles()
    {
        return $this->belongsTo('App\Models\Profile', 'profile_id');
    }

    public function categoryProperties()
    {
        return $this->hasMany('App\Models\CategoryProperty');
    }

    public function products()
    {
        return $this->hasMany('App\Models\Product');
    }

    public function characteristics()
    {
        return $this->morphToMany('App\Models\Characteristic', 'characteristicables');
    }

    public function downloads()
    {
        return $this->morphToMany('App\Models\Download', 'downloadable');
    }

    static function getFullTree($appLocId): \Kalnoy\Nestedset\Collection
    {
        return static::with('categoryProperties')
            ->get()
            ->transform(function ($item, $key) use($appLocId){

                $item->title = $item->categoryProperties->where('localisation_id', $appLocId)->where('code', 'name')->pluck('value')->shift();
                $item->key = $item['id'];
                $item->icon = 'd-none';

                return $item;
            })
            ->toTree();
    }

    static function getMainMenu()
    {
        return static::where('is_active', true)
            ->with('categoryProperties')
            ->orderBy('sort')
            ->withDepth()
            ->get()
            ->transform(function ($item, $key) {

                $item->link = Product::CATALOG_ROUTE_PREFIXES['index'] . static::getFullLink($item);
                return $item;
            })
            ->toTree();
    }
    // Полный путь до категории
    /* static private function getFullLink(Category $category): string
    {
        return implode('/', array_merge($category->ancestors()->defaultOrder()->pluck('link')->toArray(), [$category->link]));
    } */

    static function getFullLink(Category $category): string
    {
        return $category->link;
    }

    static function getByDepth(int $depth)
    {
        return static::with('categoryProperties')
            ->withDepth()
            ->get()
            ->where('depth', $depth);
    }

    static function getDataForSelect($localisationId, $propertyCode)
    {
        $data = [];

        static::with('categoryProperties')
            ->get()
            ->mapWithKeys(function ($item) use(& $data, $localisationId, $propertyCode){

                array_push($data, [
                    'text' => $item->categoryProperties->where('code', $propertyCode)->where('localisation_id',  $localisationId)->first()->value,
                    'id' => $item->id,
                ]);

                return[];
            });

        return $data;
    }

    static function getCatalogIndex(int $localisationId)
    {
        return Cache::tags(Category::class)->rememberForever('all_categories_tree_' . $localisationId, function() use ($localisationId){
            return Category::where('is_active', true)
                ->with(['categoryProperties' => function ($query) use ($localisationId) {
                    $query
                        ->where('localisation_id', $localisationId);
                }])
                ->withDepth()
                ->get()
                ->transform(function ($item, $key) use($localisationId){

                    $item->link = Product::CATALOG_ROUTE_PREFIXES['index'] . static::getFullLink($item);
                    $item->group = mb_strtolower( mb_substr($item->categoryProperties->where('localisation_id', $localisationId)->where('code', 'name')->first()->value, 0, 1, 'utf-8') );

                    return $item;
                })
                ->toTree()
                ->first()
                ->children
                ->groupBy('group') // Неуникальные
                ->sortKeys();
        });
    }

    /* Метод для китайской сортировки */
    static function getCatalogIndexChinese(int $localisationId)
    {
        return Cache::tags(Category::class)->rememberForever('all_categories_tree_' . $localisationId, function() use ($localisationId){
            return Category::where('is_active', true)
                ->with(['categoryProperties' => function ($query) use ($localisationId) {
                    $query
                        ->where('localisation_id', $localisationId);
                }])
                ->withDepth()
                ->get()
                ->transform(function ($item, $key) use($localisationId){

                    $item->group = mb_strtolower( mb_substr($item->link, 0, 2, 'utf-8') );
                    $item->link = Product::CATALOG_ROUTE_PREFIXES['index'] . static::getFullLink($item);                   

                    return $item;
                })
                ->toTree()
                ->first()
                ->children
                ->groupBy('group') // Неуникальные
                ->sortKeys();
        });     
    }

    // Без кеширования пока норм работает
    static function getCategoryDescendants(int $categoryId, int $localisationId, string $model)
    {
        return Category::where('is_active', true)
            ->with(['categoryProperties' =>  function ($query) use ($localisationId) {
                $query
                    ->where([
                        ['code', 'name'],
                        ['localisation_id', $localisationId],
                    ]);
            }])
            ->descendantsOf($categoryId)
            ->transform(function ($item, $key) use($model) {

                $item->link = $model::CATALOG_ROUTE_PREFIXES['index'] . static::getFullLink($item);
                return $item;

            })
            ->toTree();
    }
    
    // Без кеширования пока норм работает
    static function getCategoryDescendantsWithGetParams(int $categoryId, int $localisationId, string $routeName, array $params)
    {
        return Category::where('is_active', true)
            ->with(['categoryProperties' =>  function ($query) use ($localisationId) {
                $query
                    ->where([
                        ['code', 'name'],
                        ['localisation_id', $localisationId],
                    ]);
            }])
            ->descendantsOf($categoryId)
            ->transform(function ($item, $key) use($routeName, $params) {
                
                $params = http_build_query( array_merge($params, ['category' => $item->id]) );
                $item->link = $routeName . '?' . $params;
                return $item;

            })
            ->toTree();
    }
    
    // Без кеширования пока норм работает
    static function getCategoryDescendantsForShop(Profile $profile, self $category, int $localisationId)
    {
        return static::where('is_active', true)
            ->whereHas('products', function (Builder $query) use($profile, $localisationId){
                $query
                    ->whereHas('productProperties', function (Builder $query) use($profile, $localisationId){
                        $query
                            ->where([
                                ['localisation_id', $localisationId],
                                ['is_parent', true],
                                ['is_active', true],
                                
                            ]);
                    })
                    ->where('profile_id', $profile->id);
            })
            ->with(['categoryProperties' =>  function ($query) use($localisationId){
                $query
                    ->where([
                        ['code', 'name'],
                        ['localisation_id', $localisationId],
                    ]);
            }])
            ->descendantsOf($category->id);
    }

    /* Метод для выбора категорий которые будут показаны на главной */
    static function getCategoriesWithImages(int $localisationId)
    {
        return Cache::tags(Category::class)->rememberForever('popular_categories_' . $localisationId, function() use ($localisationId){
            return Category::where('is_active', true)
                ->whereHas('downloads')
                ->with(
                    ['categoryProperties' => function ($query) use ($localisationId) {
                        $query
                            ->where([
                            ['localisation_id', $localisationId],
                            ['code', 'name']
                        ]);

                    }, 'downloads'],
                    
                )
                ->orderBy('sort')
                ->get()
                ->transform(function ($item) {

                    $item->title = $item->categoryProperties->first()->value;
                    $item->banner = $item->downloads->first()->path;       
                    $item->catalog_link = Product::CATALOG_ROUTE_PREFIXES['index'] . static::getFullLink($item);

                    $item->unsetRelation('downloads')->unsetRelation('categoryProperties');

                    return $item;
                });
        });     
    }

    /* Метод для выбора категорий которые будут показаны на главной */
    static function getCategoriesWithImagesMobile(int $localisationId, array $hiddenFields)
    {
        /* return Cache::tags(Category::class)->rememberForever('popular_categories_mobile_' . $localisationId, function() use ($localisationId){
            
        });  */

        return Category::withDepth()->where('is_active', true)
            ->whereHas('downloads')
            ->with(
                ['categoryProperties' => function ($query) use ($localisationId) {
                    $query
                        ->where([
                        ['localisation_id', $localisationId],
                        ['code', 'name']
                    ]);

                }, 'downloads'],
                
            )
            ->orderBy('sort')
            ->get()
            ->transform(function ($item) use($localisationId, $hiddenFields) {

                $item->title = $item->categoryProperties->first()->value;
                $item->banner = $item->downloads->first()->path;

                $item->children = Category::descendantsOf($item->id)->transform(function($child) use($localisationId, $hiddenFields){
                        $child->title = $child->categoryProperties()->where([
                            ['localisation_id', $localisationId],
                            ['code', 'name']
                        ])
                        ->first()->value;

                        $child->makeHidden($hiddenFields);

                        return $child;
                    })
                    ->toTree($item->id);

                $item->unsetRelation('downloads')->unsetRelation('categoryProperties');

                $item->makeHidden($hiddenFields);

                return $item;
            });
    }
}
