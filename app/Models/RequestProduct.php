<?php

namespace App\Models;

use App\Extensions\Model;
use Illuminate\Database\Eloquent\{SoftDeletes, Builder};
use Illuminate\Support\Facades\DB;
use App\ApplicationDictionary as AppDic;
use Illuminate\Database\Eloquent\Relations\HasMany;

class RequestProduct extends Model
{
    use SoftDeletes;
    
    protected $guarded = ['id'];
    private static $filters = [];
    const CATALOG_ROUTE_PREFIXES = [
        'index' => '/catalog/request_products/',
        'show' => '/catalog/request_product/'
    ];
    
    public function profiles()
    {
        return $this->belongsTo('App\Models\Profile', 'profile_id');
    }
    
    public function requestProductProperties()
    {
        return $this->hasMany('App\Models\RequestProductProperty');
    }
    
    public function requestProductPrices()
    {
        return $this->hasMany('App\Models\RequestProductPrice');
    }
    
    public function categories()
    {
        return $this->belongsTo('App\Models\Category', 'category_id');
    }
    
    public function downloads()
    {
        return $this->morphToMany('App\Models\Download', 'downloadable');
    }
    
    public function characteristics()
    {
        return $this->morphToMany('App\Models\Characteristic', 'characteristicable');
    }
    
    public function favorites()
    {
        return $this->morphMany('App\Models\Favorite', 'favoriteable');
    }
    
    static function getRequestProduct($requestProduct, int $localisationId, int $appLocId)
    {
        $requestProduct->load([
            'downloads' => function ($query) {
                $query->whereIn('type', ['preview_image', 'detail_images']);
            },
                
            'requestProductProperties' => function ($query) use($localisationId){
                $query->where('localisation_id', $localisationId);
            }, 
                
            'characteristics',
            
            'requestProductPrices'
        ]);

        Product::setCorrectCharacteristics($requestProduct, $appLocId);
        return $requestProduct; 
    }

    static function getFiltrateRequestProducts(HasMany $requestProducts, $request)
    {
        self::$filters = $request;

        return $requestProducts
            ->when(!empty(self::$filters['id'])? self::$filters['id'] : '', function($query, $id) {
                return $query->where('id', $id);
            })
            ->whereHas('requestProductProperties', function (Builder $query){
                $query
                    ->when((!empty(self::$filters['name']) || (!empty(self::$filters['name']) && mb_strlen(self::$filters['name']) > 0) )? '%'.mb_strtolower(self::$filters['name']).'%' : '', function($query, $name) {
                        return $query->where([
                            ['code', 'name'],
                            [DB::raw('LOWER(value)'), 'like', $name]
                        ]);
                    });
            })
            ->whereHas('categories', function (Builder $query){
                $query
                    ->when(!empty(self::$filters['category'])? self::$filters['category'] : '', function($query, $categoryId) {
                        return $query->where('id', $categoryId);
                    });
            })
            ->whereHas('requestProductPrices', function (Builder $query){
                $query
                    ->when(!empty(self::$filters['min_price'])? self::$filters['min_price'] : '', function($query, $minPrice) {
                        return $query
                            ->where('min', '>=', (int)$minPrice)
                            ->orWhere('max', '>=', (int)$minPrice);
                    })
                    ->when(!empty(self::$filters['max_price'])? self::$filters['max_price'] : '', function($query, $maxPrice) {
                        return $query
                            ->where('max', '<=', (int)$maxPrice);
                    }) 
                    ->when(( !empty(self::$filters['min_price']) && !empty(self::$filters['max_price']) )? ['min_price' => self::$filters['min_price'], 'max_price' => self::$filters['max_price']] : '', function($query, $prices) {
                        return $query->where([
                            ['min', '>=', $prices['min_price']],
                            ['max', '<=', $prices['max_price']]
                        ]);
                    });
            });
    }
    
    static function getAllRequestProducts($requestProducts, int $localisationId, int $appLocalisationId)
    {
        return $requestProducts->each(function (& $requestProduct, $key) use($localisationId, $appLocalisationId){
      
            $requestProduct->load([
                'downloads' => function ($query) {
                    $query
                        ->whereIn('type', ['preview_image']);
                    },
                    
                'requestProductProperties' => function ($query) use($localisationId){
                    $query
                        ->where('localisation_id', $localisationId);
                    }, 
                
                'requestProductPrices'
            ]);

            // Название категории 
            $requestProduct->categories->name = $requestProduct->categories->categoryProperties->where('code', 'name')->where('localisation_id', $appLocalisationId)->first()->value;
        });
    }
    
    static function getAllRequestProductForFavorites(array $requestProductIds, int $appLocalisationId)
    {
        $requestProducts = self::whereIn('id', $requestProductIds)
            ->with([
                'downloads' => function ($query) {
                    $query
                        ->whereIn('type', ['preview_image']);
                },
                    
                'requestProductProperties',
                
                'requestProductPrices' => function ($query) {
                    $query
                        ->where([
                            ['min', '!=', 0],
                            ['min', '!=', 0]
                        ]);
                },
                
                'characteristics' => function ($query) {
                    $query
                        ->where('code', 'min_order');
                },
                
                'profiles' => function ($query) {
                    $query
                        ->select('id');
                },
                
                'categories' => function ($query) use($appLocalisationId) {
                    $query
                        ->select('id')
                        ->with([
                            'categoryProperties' => function ($query) use($appLocalisationId) {
                                $query
                                    ->where([
                                        ['code', 'name'],
                                        ['localisation_id', $appLocalisationId]
                                    ]);
                            }
                        ]);
                },
            ])
            ->get();
            
        $requestProducts->each(function (& $item){
            $item->categories->name = $item->categories->categoryProperties->first()->value;
            Product::setCorrectCharacteristics($item,  $item->requestProductProperties()->first()->localisation_id);
        }); 
        
        return $requestProducts;
    }
    
    static function getAllRequestProductsFromCategories(array $categoryIds, int $appLocId, int $needPages = 15, $searchParams = null, $orderBy = null)
    {
        self::$filters = $searchParams;

        $requestProducts = self::whereIn('category_id', $categoryIds)
            ->where('is_active', true)
            ->with([
                'downloads' => function ($query) {
                    $query
                        ->whereIn('type', ['preview_image']);
                },
                    
                'requestProductProperties' => function ($query) {
                    $query
                        ->where('code', 'name');
                }, 
                
                'requestProductPrices' => function ($query) {
                    $query
                        ->where([
                            ['min', '!=', 0],
                            ['min', '!=', 0]
                        ]);
                },
                
                'characteristics' => function ($query) {
                    $query
                        ->where('code', 'min_order');
                },
                
                'profiles' => function ($query) {
                    $query
                        ->select('id');
                },
                
                'favorites'
            ])
            ->when(!empty($orderBy) ? $orderBy : '', function($query, $orderBy) use($categoryIds){
                return Product::orderByParameters($query, $orderBy, $categoryIds, null);
            })
            ->when(!empty(self::$filters), function($query) {
                return static::filtrateByParameters($query);
            })
            ->paginate($needPages);
       
        $requestProducts->each(function (& $item) use($appLocId){
         
            Product::setCorrectCharacteristics($item,  $appLocId); // TODO: что будем отображать, если для данной локализации приложения характеристик и свойств товара нету
        }); 

        return $requestProducts;       
    }
    
    private static function filtrateByParameters(Builder $query) 
    {
        if( !empty(self::$filters['search']) ) 
        {
            $query
                ->whereHas('requestProductProperties', function (Builder $query){
                    static::filtrateByProperties($query);
                });
        }

        return $query;
    }
    
    private static function filtrateByProperties(Builder $query)
    {
        return $query
            ->where([
                ['code', 'name'],
                [DB::raw('LOWER(value)'), 'like', '%'.mb_strtolower(self::$filters['search']).'%']
            ]);
    }
}
