<?php

namespace App\Models;

use App\Extensions\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProfileContact extends Model
{
    use SoftDeletes;

    protected $guarded = ['id'];
	
	public function profiles()
    {
        return $this->belongsTo('App\Models\Profile', 'profile_id');
    }

    public function downloads()
    {
        return $this->morphToMany('App\Models\Download', 'downloadable');
    }
}