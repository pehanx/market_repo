<?php

namespace App\Models;

use App\Extensions\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Chat extends Model
{
    use SoftDeletes;

    protected $guarded = ['id'];
	
	public function profiles()
    {
        return $this->morphedByMany('App\Models\Profile', 'chatable');
    }

    public function users()
    {
        return $this->morphedByMany('App\User', 'chatable');
    }

    public function chatables()
    {
        return $this->morphTo(__FUNCTION__,'chatable_type','chatable_id');
    }
    
    public function messages()
    {
        return $this->HasMany('App\Models\Message');
    }

    /* 
     * Проверка на существование чата с поставщиком
     */
    static function isChatWithMemberExists( Profile $profile, int $memberId )
    {
        return $profile->chats()
            ->whereHas('profiles', function ($query) use($memberId) {
                $query
                    ->where('profiles.id', $memberId);
            })
            ->first();
    }

    /* 
     * Проверка на существование чата с пользователем (для создания чата с админки)
     */
    static function isChatWithUserExists( $user, int $memberId )
    {
        return $user->chats()
            ->whereHas('users', function ($query) use($memberId) {
                $query
                    ->where('users.id', $memberId);
            })
            ->first();
    }

    /* 
     * Проверка на принадлежность сообщения чату профиля
     */
    static function isMessageFromMyChat(Message $message, Profile $profile)
    {   
        return $message->chats()->whereHas('profiles', function ($query) use($profile) {
            $query
                ->where('profiles.id', $profile->id);
        })
        ->first();
    }

    /* 
     * Проверка на принадлежность сообщения чату саппорта
     */
    static function isMessageFromSupport(Message $message, $user)
    {   
        return $message->chats()->whereHas('users', function ($query) use($user) {
            $query
                ->where('users.id', $user->id);
        })
        ->first();
    }
}
