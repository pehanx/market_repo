<?php

namespace App\Models;

use App\Extensions\Model;

class Manager extends Model {

    protected $guarded = ['id'];

    protected $table = 'users';
	
	public function companies()
    {
        return $this->belongsToMany('App\User', 'manager_user');
    }
}