<?php

namespace App\Models;

use App\Extensions\Model;

class OrderItemProperty extends Model
{
    protected $guarded = ['id'];
        
    public function orderItems()
    {
        return $this->belongsTo('App\Models\OrderItem', 'order_item_id');
    }
    
    public function localisations()
    {
        return $this->belongsTo('App\Models\Localisation', 'localisation_id');
    }
    
    public function currencies()
    {
        return $this->belongsTo('App\Models\Currency', 'currency_id');
    }
}
