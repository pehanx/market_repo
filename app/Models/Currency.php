<?php

namespace App\Models;

use App\Extensions\Model;

class Currency extends Model
{
    public function productProperties()
    {
        return $this->hasMany('App\Models\ProductProperty');
    }
    
    public function orderProperties()
    {
        return $this->hasMany('App\Models\OrderProperty');
    }
    
    public function orderItemProperties()
    {
        return $this->hasMany('App\Models\OrderItemProperty');
    }
    
    public function tenderProperties()
    {
        return $this->hasMany('App\Models\TenderProperty');
    }
}
