<?php

namespace App\Models;

use App\Extensions\Model;

class ProfileType extends Model
{
    protected $guarded = ['id'];
	
	public function profiles()
    {
        return $this->hasMany('App\Models\Profile');
    }
}
