<?php

namespace App\Models;

use App\Extensions\Model;

class Setting extends Model
{
    protected $guarded = ['id'];

    public function profile()
    {
        return $this->morphedByMany('App\Models\Profile', 'settingable');
    }
    
    public function user()
    {
        return $this->morphedByMany('App\User', 'settingable');
    }

    /* 
     * Добавление или обновление глобальной настройки для пользователя
     */
    public static function addSetting( string $code, string $type, $value, $entity )
    {
        $entity->settings()->updateOrCreate(
            ['type'=>$type, 'code'=>$code],
            ['value'=>$value, 'code'=>$code, 'type'=>$type]);
    }
    
}
