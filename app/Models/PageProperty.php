<?php

namespace App\Models;

use App\Extensions\Model;

class PageProperty extends Model
{    
    protected $guarded = ['id'];
    
    public function localisations()
    {
        return $this->belongsTo('App\Models\Localisation', 'localisation_id');
    }
    
    public function pages()
    {
        return $this->belongsTo('App\Models\Page', 'page_id');
    }
    
    public function getMenuTitle(): string
    {
        return $this->menu_title ?: $this->title;
    }
}