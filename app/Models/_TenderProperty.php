<?php

namespace App\Models;

use App\Extensions\Model;

class TenderProperty extends Model
{
    protected $guarded = ['id'];
	
    public function tenders()
    {
        return $this->belongsTo('App\Models\Tender', 'tender_id');
    }
    
	public function localisations()
    {
        return $this->belongsTo('App\Models\Localisation', 'localisation_id');
    }
    
    public function currencies()
    {
        return $this->belongsTo('App\Models\Currency', 'currency_id');
    }
}
