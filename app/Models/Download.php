<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use App\Extensions\Model;

class Download extends Model
{
    use SoftDeletes;
    
    protected $guarded = ['id'];
    
    public function users()
    {
        return $this->morphedByMany('App\User', 'downloadable');
    }
     
    public function profiles()
    {
        return $this->morphedByMany('App\Models\Profile', 'downloadable');
    }
    
    public function products()
    {
        return $this->morphedByMany('App\Models\Product', 'downloadable');
    }
    
    public function exports()
    {
        return $this->morphedByMany('App\Models\Export', 'downloadable');
    }

    public function imports()
    {
        return $this->morphedByMany('App\Models\Import', 'downloadable');
    }

    public function productModels()
    {
        return $this->morphedByMany('App\Models\ProductModel', 'downloadable');
    }
    
    public function categories()
    {
        return $this->morphedByMany('App\Models\Category', 'downloadable');
    }
}
