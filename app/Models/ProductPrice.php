<?php

namespace App\Models;

use App\Extensions\Model;

class ProductPrice extends Model
{
    protected $guarded = ['id'];
    
    public function currencies()
    {
        return $this->belongsTo('App\Models\Currency', 'currency_id');
    }
    
    public function products()
    {
        return $this->belongsTo('App\Models\Product', 'product_id');
    }
    
    public function productModels()
    {
        return $this->belongsTo('App\Models\ProductModel');
    }
}
