<?php

namespace App\Models;

use Illuminate\Support\Facades\Auth;
use App\User;
use App\Extensions\Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\SoftDeletes;

class Profile extends Model
{
    use SoftDeletes;
    
    protected $guarded = ['id'];
    
    public static function boot()
    {
        parent::boot();
        /**
         * Перед удалением самого профиля удаляем менеджеров профиля и связанную с ним информацию (softDeletes)
         *
         * @param self Profile
         * 
         */
        static::deleting(function ($model)
        {
            $model->profileProperties()->delete();
            
            $profileManagers = $model->users()->role(['seller_manager', 'buyer_manager'])->get();
            
            if($profileManagers)
            {
                $profileManagers->each(function ($item, $key) {
                    
                    $account = $item->profiles()->where('profile_type_id', ProfileType::where('code', 'account')->first()->id)->first();
                    
                    $item->delete(); 
                    $account->delete();
                });
            }
            
            $model->downloads()->delete(); 
         });
        
        
        
        /**
         * Перед удалением самого профиля удаляем менеджеров профиля и связанную с ним информацию.
         *
         * @param self Profile
         * 
         
        static::deleting(function ($model)
        {
            $model->profileProperties()->delete();
            
            $profileManagers = $model->users()->role(['seller_manager', 'buyer_manager'])->get();

            // Удаляем менеджеров профиля
            if($profileManagers->first())
            {
                $profileManagers->each(function ($item, $key) {
                    
                    $account = $item->profiles()->where('profile_type_id', ProfileType::where('code', 'account')->first()->id)->first();// Профиль менеджера типа account
                    
                    $item->delete(); // Пользователь (модель User поддерживает softDelete)
                    $item->profiles()->detach(); // Отвязываем менеджера от профилей (удаляемого профиля, аккаунта)
                    
                    $account->delete(); // Удаляем аккаунт профиля (заходим сюда же рекурсивно)
                });
            }
            
            $model->users()->detach(); // Отвязываем от удаляемого профиля всех пользователей
            
            $model->downloads()->delete(); 
            $model->downloads()->detach();
        });
        */
    }
	
	public function users()
    {
        return $this->belongsToMany('App\User');
    }

    public function chats()
    {
        return $this->morphToMany('App\Models\Chat', 'chatable');
    }
	
	public function profileTypes()
    {
        return $this->belongsTo('App\Models\ProfileType', 'profile_type_id');
    }
	
	public function profileProperties()
    {
        return $this->hasMany('App\Models\ProfileProperty');
    }
    
    public function downloads()
    {
        return $this->morphToMany('App\Models\Download', 'downloadable');
    }
    
    public function contacts()
    {
        return $this->hasMany('App\Models\ProfileContact');
    }

    public function settings()
    {
        return $this->morphToMany('App\Models\Setting', 'settingable');
    }

    public function characteristics()
    {
        return $this->morphToMany('App\Models\Characteristic', 'characteristicable');
    }
    
    public function products()
    {
        return $this->hasMany('App\Models\Product');
    }
    
    public function requestProducts()
    {
        return $this->hasMany('App\Models\RequestProduct');
    }

    public function exports()
    {
        return $this->hasMany('App\Models\Export');
    } 

    public function imports()
    {
        return $this->hasMany('App\Models\Import');
    }

    public function isOnline()
    {
        return \Cache::has('online_profile_' . $this->id);
    }

    /*
    public function categories()
    {
        return $this->hasMany('App\Models\Category');
    }
    
    public function tenders()
    {
        return $this->hasMany('App\Models\Tender');
    }
    */
    
    static function getAccountForUser(int $userId)
    {
        return static::where('profile_type_id', ProfileType::where('code', 'account')->pluck('id')->toArray())
            ->whereHas('users', function (Builder $query) use($userId) {
                $query
                    ->where('user_id', $userId);
            })
            ->has('profileProperties')
            ->with('profileProperties');
    }
    
    static function getProfileForUser(int $profileId, int $userId, int $appLocalisationId)
    {
        
        $profile = static::whereIn( 'profile_type_id', ProfileType::whereIn('code', ['buyer', 'seller'])->pluck('id')->toArray())
            ->where( 'id', $profileId )
            ->whereHas('profileProperties', function (Builder $query) {
                $query
                    ->whereIn('localisation_id', Localisation::whereIn('code', \Config::get('app.locales'))->pluck('id')->toArray());
            })
            ->whereHas('users', function (Builder $query) use($userId) {
                $query
                    ->where('user_id', $userId);
            })
            ->with([
                'downloads' => function ($query){
                    $query
                        ->where('type', '!=', 'profile_documents');                        
                }, 
                'profileTypes',
                'characteristics',
                'profileProperties'
                ])
            ->firstOrFail();

        Product::setCorrectCharacteristics($profile, $appLocalisationId);
            
        $profile->setRelation('downloads', $profile->downloads->groupBy('type'));
        
        $youtubeLink = $profile->profileProperties->where('code', 'company_youtube_link')->first();
        
        if($youtubeLink)
        {
            $params = [
                'autoplay' => 0,
                'loop' => 1,
                'accelerometer' => 1,
                'encrypted-media' => 1, 
                'gyroscope' => 1, 
                'picture-in-picture' => 1
            ];
            
            $attributes = [
                'width' => '580',
                'height' => '290',
                'allow' => 'accelerometer'
            ];
            
            $profile->youtube_frame = \LaravelVideoEmbed::parse($youtubeLink->value, ['YouTube'], $params, $attributes)->getEmbedCode();
        }
        
        return $profile;
    }
    
    static function getProfilesForUser(int $userId, $type = null)
    {   
        return static::when(
                $type, 
                
                function($query) use($type) {
                    return $query
                        ->where('profile_type_id', ProfileType::where('code', $type)->first()->id);
                },

                function($query) {
                    return $query
                        ->where('status', 'active') // Для хидера
                        ->whereIn('profile_type_id', ProfileType::whereIn('code', ['buyer','seller'])->pluck('id')->all());
                }

            )
            ->whereHas('users', function (Builder $query) use($userId) {
                $query
                    ->where('user_id', $userId);
            })
            ->with([
                'profileProperties',
                'downloads' => function ($query){
                    $query
                        ->where('type', 'company_logo_image');                        
                }
            ])
            ->get();
    }
 
    static function getProfilesWithManagersForUser(User $user, $needPages = 10)
    {
        return $user->profiles()
            ->whereIn('profile_type_id', ProfileType::whereIn('code', ['buyer', 'seller'])->pluck('id')->toArray() )
            ->whereHas('users', function (Builder $query){
                $query
                    ->role(['buyer_manager', 'seller_manager']);
            })
            ->with([
                'users' => function ($query){
                    $query
                        ->role(['buyer_manager', 'seller_manager'])
                        ->with([
                            'profiles' => function ($query){
                                $query 
                                    ->where('profile_type_id', ProfileType::where('code', 'account')->pluck('id')->shift() )
                                    ->with([
                                        'profileProperties' => function ($query){
                                            $query
                                                ->whereIn('localisation_id', Localisation::whereIn('code', \Config::get('app.locales'))->pluck('id')->toArray()); 
                                        }
                                    ]);
                            }
                        ]);
                },
                
                'profileProperties' => function ($query){
                    $query
                        ->whereIn('localisation_id', Localisation::whereIn('code', \Config::get('app.locales'))->pluck('id')->toArray()); 
                },
                
                'profileTypes'
            ])
            ->paginate($needPages);
    }

    static function getCompaniesForManager(User $user, int $needPages = 10, $type = 'all')
    {
        switch ($type)
        {
            case 'buyer':
                $types = ProfileType::whereIn('code', ['buyer'])->pluck('id')->toArray();
                break;

            case 'seller':
                $types = ProfileType::whereIn('code', ['seller'])->pluck('id')->toArray();
                break;
            
            case 'all':
                $types = ProfileType::whereIn('code', ['buyer', 'seller'])->pluck('id')->toArray();
                break;
            
            default:
                $types = ProfileType::whereIn('code', ['buyer', 'seller'])->pluck('id')->toArray();
                break;
        }

        return $user->profiles()
            ->whereIn('profile_type_id', $types )
            ->whereHas('users')
            ->with([
                'users' => function ($query){
                    $query
                        ->role(['buyer', 'seller']);
                },
                
                'profileProperties' => function ($query){
                    $query
                        ->whereIn('localisation_id', Localisation::whereIn('code', \Config::get('app.locales'))->pluck('id')->toArray()); 
                },
                
                'profileTypes',
                
                'downloads'
            ])
            ->paginate($needPages);
    }
    
    static function getProfileWithProductsForShop(User $user, int $localisationId): self
    {
        return $user
            ->profiles()
            ->where([
                ['status', 'active'],
                ['profile_type_id', ProfileType::where('code', 'seller')->first()->id],
            ])
            ->whereHas('profileProperties', function (Builder $query) use($localisationId){
                $query
                    ->where('localisation_id', $localisationId);
            })
            ->with([
                'profileProperties' => function ($query){
                    $query
                        ->where('code', 'company_name');                        
                }, 
                
                'downloads' => function ($query){
                    $query
                        ->where('type', 'company_logo_image');                        
                },
                
                'products' => function ($query) use($localisationId){
                    $query
                        ->whereHas('productProperties', function (Builder $query) use($localisationId){
                            $query
                                ->where([
                                    ['localisation_id', $localisationId],
                                    ['is_parent', true],
                                    ['is_active', true],
                                    
                                ]);
                        })
                        ->with([
                            'categories' => function ($query) use($localisationId){
                                $query
                                    ->with([
                                        'categoryProperties' => function ($query) use($localisationId){
                                            $query
                                                ->where([
                                                    ['code', 'name'],
                                                    ['localisation_id', $localisationId]
                                                ]);
                                        }
                                    ]);
                            }, 
                        ]);
                }
            ])
            ->first();
    }
    
    static function getProfileForShop(User $user, int $localisationId): self
    {
        return $user
            ->profiles()
            ->where([
                ['status', 'active'],
                ['profile_type_id', ProfileType::where('code', 'seller')->first()->id],
            ])
            ->whereHas('profileProperties', function (Builder $query) use($localisationId){
                $query
                    ->where('localisation_id', $localisationId);
            })
            ->with([
                'profileProperties', 
                
                'downloads' => function ($query){
                    $query
                        ->where('type', 'company_logo_image');                        
                },
            ])
            ->first();
    }
    
    static function getProfilesWithProductsForShop(User $user, int $localisationId)
    {
        return $user
            ->profiles()
            ->where([
                ['status', 'active'],
                ['profile_type_id', ProfileType::where('code', 'seller')->first()->id]
            ])
            ->with([
                'profileProperties' => function ($query){
                    $query
                        ->with('localisations');                        
                }, 
                
                'products' => function ($query) use($localisationId){
                    $query
                        ->whereHas('productProperties', function (Builder $query) use($localisationId){
                            $query
                                ->where([
                                    ['localisation_id', $localisationId],
                                    ['is_parent', true],
                                    ['is_active', true],
                                    
                                ]);
                        })
                        ->whereHas('downloads', function (Builder $query){
                            $query
                                ->where('type', ['preview_image']);
                        })
                        ->with([
                            'productProperties',
                            
                            'productPrices' => function ($query){
                                $query->where('is_parent', true);
                            },
                            
                            'characteristics', 
                            
                            'favorites',
                            
                            'categories' => function ($query) use($localisationId){
                                $query
                                    ->with([
                                        'categoryProperties' => function ($query) use($localisationId){
                                            $query->where('localisation_id', $localisationId);
                                        }
                                    ]);
                            }, 
                            
                            'downloads' => function ($query){
                                $query
                                    ->whereIn('type', ['preview_image', 'detail_images']);
                            }
                        ])
                        ->orderBy('shows', 'desc');
                }
            ])
            ->get();
    }

    /*
    Profile::whereHas('profileProperties', function (Builder $query) use($profile) {
        $query
            ->where('localisation_id', Localisation::where('code', app('translator')->getLocale())->first()->id)
            ->where('id', $profile);
    })
    ->whereHas('users', function (Builder $query) {
        $query
            ->where('user_id', auth()->user()->id);
    })
    ->with('profileProperties')
    ->first(); */
}
