<?php

namespace App\Models;

use App\Extensions\Model;

class ProductProperty extends Model
{
    protected $guarded = ['id'];
    
    public function localisations()
    {
        return $this->belongsTo('App\Models\Localisation', 'localisation_id');
    }
    
    public function products()
    {
        return $this->belongsTo('App\Models\Product', 'product_id');
    }
    
    public function productModels()
    {
        return $this->belongsTo('App\Models\ProductModel', 'product_id');
    }
}
