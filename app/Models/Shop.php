<?php

namespace App\Models;

use App\Extensions\Model;

class Shop extends Model
{
    protected $guarded = ['id'];
}
