<?php

namespace App\Models;

use App\Extensions\Model;

class TenderParticipant extends Model
{
    protected $guarded = ['id'];
	
    public function profiles()
    {
        return $this->belongsTo('App\Models\Profile', 'profile_id');
    }
    
    public function tenders()
    {
        return $this->belongsTo('App\Models\Tender', 'profile_id');
    }
    
    public function downloads()
    {
        return $this->morphMany('App\Models\Download', 'downloadables');
    }
}
