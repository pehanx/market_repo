<?php

namespace App\Models;

use App\Extensions\Model;
use Illuminate\Database\Eloquent\{SoftDeletes, Builder};
use Illuminate\Support\Facades\DB;
use App\ApplicationDictionary as AppDic;
use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Query\Expression;

class Product extends Model
{
    use SoftDeletes;
    
    protected $guarded = ['id'];
    private static $filters = [];
    const CATALOG_ROUTE_PREFIXES = [
        'index' => '/catalog/products/',
        'show' => '/catalog/product/'
    ];
    const SHOP_ROUTE_PREFIXES = [
        'index' => '/products/',
    ];
    
	public function profiles()
    {
        return $this->belongsTo('App\Models\Profile', 'profile_id');
    }
    
    public function shops()
    {
        return $this
            ->belongsTo('App\Models\Profile', 'profile_id')
            ->where([
                ['status', 'active'],
                ['profile_type_id', 2] //seller
            ]);
    }
    
    public function categories()
    {
        return $this->belongsTo('App\Models\Category', 'category_id');
    }
    
    public function productProperties()
    {
        return $this->hasMany('App\Models\ProductProperty');
    }
    
    public function characteristics()
    {
        return $this->morphToMany('App\Models\Characteristic', 'characteristicable');
    }
    
    public function downloads()
    {
        return $this->morphToMany('App\Models\Download', 'downloadable');
    }
    
    public function comments()
    {
        return $this->morphToMany('App\Models\Comment', 'commentable');
    }
    
    public function productModels()
    {
        return $this->hasMany('App\Models\ProductModel')->orderBy('id', 'asc');
    }
    
    public function productPrices()
    {
        return $this->hasMany('App\Models\ProductPrice');
    }
    
    public function favorites()
    {
        return $this->morphMany('App\Models\Favorite', 'favoriteable');
    }

    public static function boot()
    {
        parent::boot();
        
        // Прилетает при delete() и forceDelete(). Мы же будем вызывать только при методе forceDelete().
        static::deleting(function ($model)
        {
            dump('fuck');exit();
            $model->productProperties()->delete();
            $model->productModels()->delete(); 
        });
        
        static::restoring(function ($model)
        {
            //dump('fuck');exit();
            //$model->productProperties()->restore();
            //$model->productModels()->restore();
        });
    }
    
    static function getProduct($product, int $localisationId, $appLocalisationId): self
    {
        $product->load([
            'downloads' => function ($query) {
                $query
                    ->whereIn('type', ['preview_image', 'detail_images', 'describle_images']);
                },
                
            'productProperties' => function ($query) use($localisationId){
                $query
                    ->where([
                        ['localisation_id', $localisationId],
                        ['is_parent', true]
                    ]);
                }, 
                
            'characteristics',
            
            'productPrices' => function ($query) use($localisationId){
                $query
                    ->where('is_parent', true);
                },
        ]);

        static::setCorrectCharacteristics($product, $appLocalisationId);
        
        $product->setRelation('downloads', $product->downloads->groupBy('type'));
        
        $youtubeLink = $product->productProperties->where('code', 'youtube')->first();

        if($youtubeLink)
        {
            $product->youtube_frame = \LaravelVideoEmbed::parse($youtubeLink->value, ['YouTube'])->getEmbedCode();
        }
        
        return $product;
    }
    
    static function getProductWithModels($product, int $localisationId, int $appLocalisationId, $withShop = false)
    {
        $product->load([
            'downloads' => function ($query){
                $query
                    ->whereIn('type', ['preview_image', 'detail_images', 'describle_images']);
                },
                
            'productProperties' => function ($query) use($localisationId){
                $query
                    ->where([
                        ['localisation_id', $localisationId],
                        ['is_parent', true]
                    ]);
                }, 
                
            'characteristics',
            
            'productPrices' => function ($query) use($localisationId){
                $query
                    ->where('is_parent', true);
                },
            
            'productModels',
        ]);
        
        if($withShop)
        {
            $product->load([
                'shops'  => function ($query) {
                    $query
                        ->with('users');
                    }
            ]);
        }
        
        $youtubeLink = $product->productProperties->where('code', 'youtube')->first();

        if($youtubeLink)
        {
            $params = [
                'autoplay' => 0,
                'loop' => 1,
                'accelerometer' => 1,
                'encrypted-media' => 1, 
                'gyroscope' => 1, 
                'picture-in-picture' => 1
            ];
            
            $attributes = [
                'width' => '100%',
                'height' => '430',
                'allow' => 'accelerometer'
            ];
            
            $product->youtube_frame = \LaravelVideoEmbed::parse($youtubeLink->value, ['YouTube'], $params, $attributes)->getEmbedCode();
        }
        
        $product->productModels->load([
            'downloads' => function ($query) {
                $query
                    ->whereIn('type', ['preview_image']);
                },
                
            'productProperties' => function ($query) use($localisationId){
                $query
                    ->where([
                        ['localisation_id', $localisationId],
                        ['is_parent', false]
                    ]);
                },  
                
            'characteristics',
            
            'productPrices' => function ($query) use($localisationId){
                $query
                    ->where('is_parent', false);
                },
        ]); 
        
        static::setCorrectCharacteristics($product, $appLocalisationId);
        $product->productModels->each(function (& $item) use($appLocalisationId){
            static::setCorrectCharacteristics($item,  $appLocalisationId);
        });

        return $product;
    }
    
    // TODO: вынести метод в модель Application
    public static function setCorrectCharacteristics(& $product, int $localisationId)
    {
        $product->setRelation('characteristics', $product->characteristics
            ->mapWithKeys(function (& $item) use($localisationId){
                return [
                    $item['code'] => [
                        'type' => ( !empty($item['type']) ) ? AppDic::getValue($item['type'], $localisationId) : $item['value'],
                        'value' => ( !empty($item['type']) ) ? $item['value'] : AppDic::getValue($item['value'], $localisationId),
                    ]
                ];
            })
        ); 
    }
    
    static function getAllProductsWithModels($products, int $localisationId, int $appLocalisationId, bool $needTrashedModels = false, bool $needAllImages = false)
    {
        return $products->each(function (& $product, $key) use($localisationId, $appLocalisationId, $needTrashedModels, $needAllImages){
      
            $product->load([
                'downloads' => function ($query) use($needAllImages){
                    $query
                        ->when(!$needAllImages, function($query) {
                            return $query
                                ->whereIn('type', ['preview_image']);
                        });
                    },
                    
                'productProperties' => function ($query) use($localisationId){
                    $query
                        ->where([
                            ['localisation_id', $localisationId],
                            ['is_parent', true]
                        ]);
                    }, 
                
                'productPrices' => function ($query) use($localisationId){
                    $query
                        ->where('is_parent', true);
                    },
                
                'productModels' => function ($query) use($needTrashedModels, $product){
                    $query
                        // Берем все торговые предложения для удаленных товаров (включая удаленные т.п.)
                        ->when($needTrashedModels && $product->deleted_at !== null, function($query) {
                            return $query
                                ->withTrashed()
                                ->whereNull('destroyed_at');
                        })
                        // Если товар не удален, берем для отображения только удаленные торговые предложения
                        ->when($needTrashedModels && $product->deleted_at === null, function($query) {
                            return $query
                                ->onlyTrashed()
                                ->whereNull('destroyed_at');
                        });
                    }
            ]);

            $product->productModels->load([
                'downloads' => function ($query) {
                    $query
                        ->whereIn('type', ['preview_image']);
                    },
                    
                'productProperties' => function ($query) use($localisationId){
                    $query
                        ->where([
                                ['localisation_id', $localisationId],
                                ['is_parent', false]
                        ]);
                    },  
                
                'productPrices' => function ($query) use($localisationId){
                    $query
                        ->where('is_parent', false);
                    },
            ]); 
            // Название категории 
            $product->categories->name = $product->categories->categoryProperties->where('code', 'name')->where('localisation_id', $appLocalisationId)->first()->value;
        });
    }
    
    static function getFiltrateProducts(HasMany $products, $request)
    {
        self::$filters = $request;

        return $products
            ->when(!empty(self::$filters['articul'])? self::$filters['articul'] : '', function($query, $articul) {
                return $query->where('articul', 'like', '%'.$articul.'%');
            })
            ->whereHas('productProperties', function (Builder $query){
                $query
                    ->when(!empty(self::$filters['name'])? self::$filters['name'] : '', function($query, $name) {
                        return $query->where([
                            ['code', 'name'],
                            [DB::raw('LOWER(value)'), 'like', '%'.mb_strtolower($name).'%']
                        ]);
                    });
            })
            ->whereHas('categories', function (Builder $query){
                $query
                    ->when(!empty(self::$filters['category'])? self::$filters['category'] : '', function($query, $categoryId) {
                        return $query->where('id', $categoryId);
                    });
            })
            ->whereHas('productPrices', function (Builder $query){
                $query
                    ->when(!empty(self::$filters['min_price'])? self::$filters['min_price'] : '', function($query, $minPrice) {
                        return $query
                            ->where('min', '>=', (int)$minPrice)
                            ->orWhere('max', '>=', (int)$minPrice);
                    })
                    ->when(!empty(self::$filters['max_price'])? self::$filters['max_price'] : '', function($query, $maxPrice) {
                        return $query
                            ->where('max', '<=', (int)$maxPrice);
                    }) 
                    ->when(( !empty(self::$filters['min_price']) && !empty(self::$filters['max_price']) )? ['min_price' => self::$filters['min_price'], 'max_price' => self::$filters['max_price']] : '', function($query, $prices) {
                        return $query->where([
                            ['min', '>=', $prices['min_price']],
                            ['max', '<=', $prices['max_price']]
                        ]);
                    });
            });
    }

    static function getProductHasModels(HasMany $products, string $has)
    {
        return $products->whereHas('productModels', function (Builder $query) use($has){
                $query
                    ->when($has === 'trashed', function($query) {
                        $query
                            ->onlyTrashed()
                            ->whereNull('destroyed_at');
                    });
            });
    }
    
    static function getAllProductForFavorites(array $productIds, int $appLocalisationId)
    {
        $products = self::whereIn('id', $productIds)
            ->with([
                'downloads' => function ($query) {
                    $query
                        ->whereIn('type', ['preview_image']);
                },
                    
                'productProperties' => function ($query) {
                    $query
                        ->where('is_parent', true); // Не является торговым предложением 
                }, 
                
                'productPrices' => function ($query) {
                    $query
                        ->where('is_parent', true); // Не является торговым предложением 
                },
                
                'characteristics' => function ($query) {
                    $query
                        ->where('code', 'min_order');
                },
                
                'profiles' => function ($query) {
                    $query
                        ->select('id');
                },
                
                'categories' => function ($query) use($appLocalisationId) {
                    $query
                        ->select('id')
                        ->with([
                            'categoryProperties' => function ($query) use($appLocalisationId) {
                                $query
                                    ->where([
                                        ['code', 'name'],
                                        ['localisation_id', $appLocalisationId]
                                    ]);
                            }
                        ]);
                },
                
                'shops'  => function ($query) {
                    $query
                        ->with('users');
                }
            ])
            ->get();

        $products->each(function (& $item) use($appLocalisationId){
            $item->categories->name = $item->categories->categoryProperties->first()->value;
            static::setCorrectCharacteristics($item,  $appLocalisationId);
        }); 

        return $products;
    }
    
    static function getAllProductsFromCategories(array $categoryIds, int $appLocId, int $needPages = 15, $searchParams = null, $orderBy = null, $profile = null)
    {
        self::$filters = $searchParams;

        $products = self::whereIn('category_id', $categoryIds)
            ->where('is_active', true)
            ->whereHas('downloads', function (Builder $query){ // Как заполниться норм карточками товаров, а не сидером - убрать
                $query
                    ->where('type', ['preview_image']);
            })
            ->whereHas('productProperties', function (Builder $query) use($appLocId){ 
                $query
                    ->where('localisation_id', $appLocId);
            })
            ->with([
                'downloads' => function ($query) {
                    $query
                        ->whereIn('type', ['preview_image']);
                },
                    
                'productProperties' => function ($query) {
                    $query
                        ->where([
                            ['is_parent', true], // Не является торговым предложением 
                            ['code', 'name'],
                        ]);
                }, 
                
                'productPrices' => function ($query) use ($orderBy){
                    $query
                        ->where('is_parent', true); // Не является торговым предложением
                },
                
                'characteristics' => function ($query) {
                    $query
                        ->where('code', 'min_order');
                },
                
                'profiles' => function ($query) {
                    $query
                        ->select('id');
                },
                
                'favorites'
            ]) 
            ->when(!empty($orderBy) ? $orderBy : '', function($query, $orderBy) use($categoryIds, $profile){
                return static::orderByParameters($query, $orderBy, $categoryIds, $profile);
            })
            ->when(!empty(self::$filters), function($query) {
                return static::filtrateByParameters($query);
            })
            ->when(
                $profile, 
                
                function($query, $profile) {
                    return $query
                        ->where('profile_id', $profile->id);
                }, 
                
                function ($query) {
                    return $query
                        ->with([
                            'shops'  => function ($query) {
                                $query
                                    ->with('users');
                                }
                        ]);
                }
            )
            ->paginate($needPages);

        $products->each(function (& $item) use($appLocId){
            static::setCorrectCharacteristics($item,  $appLocId); // TODO: что будем отображать, если для данной локализации приложения характеристик и свойств товара нету
        }); 
        //dd($products);
        return $products;       
    }

    static function getAllProductsFromCategoriesMobile(array $categoryIds, int $appLocId,  int $userId = null, int $needPages = 15, $searchParams = null, $orderBy = null, $profile = null)
    {
        self::$filters = $searchParams;

        $products = self::whereIn('category_id', $categoryIds)
            ->where('is_active', true)
            ->whereHas('downloads', function (Builder $query){ // Как заполниться норм карточками товаров, а не сидером - убрать
                $query
                    ->where('type', ['preview_image']);
            })
            ->whereHas('productProperties', function (Builder $query) use($appLocId){ 
                $query
                    ->where('localisation_id', $appLocId);
            })
            ->with([
                'downloads' => function ($query) {
                    $query
                        ->whereIn('type', ['preview_image']);
                },
                    
                'productProperties' => function ($query) {
                    $query
                        ->where([
                            ['is_parent', true], // Не является торговым предложением 
                            ['code', 'name'],
                        ]);
                }, 
                
                'productPrices' => function ($query) use ($orderBy){
                    $query
                        ->where('is_parent', true); // Не является торговым предложением
                },
                
                'characteristics' => function ($query) {
                    $query
                        ->where('code', 'min_order');
                },
                
                'profiles' => function ($query) {
                    $query
                        ->select('id');
                },
                'favorites' => function ($query) use($userId){
                    $query
                        ->where('user_id', $userId);
                },

            ]) 
            ->when(!empty($orderBy) ? $orderBy : '', function($query, $orderBy) use($categoryIds, $profile){
                return static::orderByParameters($query, $orderBy, $categoryIds, $profile);
            })
            ->when(!empty(self::$filters), function($query) {
                return static::filtrateByParameters($query);
            })
            ->when(
                $profile, 
                
                function($query, $profile) {
                    return $query
                        ->where('profile_id', $profile->id);
                }, 
                
                function ($query) {
                    return $query
                        ->with([
                            'shops'  => function ($query) {
                                $query
                                    ->with('users');
                                }
                        ]);
                }
            )
            ->paginate($needPages);
            

        $products->each(function (& $item) use($appLocId){
            static::setCorrectCharacteristics($item,  $appLocId); // TODO: что будем отображать, если для данной локализации приложения характеристик и свойств товара нету
        }); 
        //dd($products);
        return $products;       
    }

    
    public static function orderByParameters(Builder $query, $orderBy, $categoryIds, $profile)
    {
        $sqlBuilder = null;
        
        switch ($orderBy['field']) 
        {
            case 'price':
                $sqlBuilder = ProductPrice
                    ::select('product_id as related_id', DB::raw("row_number() over(order by min {$orderBy['direction']}) as filter_sort"))
                    ->where('is_parent', true)
                    ->whereHas('products', function (Builder $query) use($categoryIds, $profile){ 
                        $query
                            ->whereIn('category_id', $categoryIds)
                            ->when($profile, function($query, $profile) {
                                return $query
                                    ->where('profile_id', $profile->id);
                            });
                    })
                    ->distinct('related_id', 'min') // На тот случай когда будет несколько диапазонов цен у товара - убрать.
                    ->orderBy('min', $orderBy['direction']);
               
                $query->leftJoinSub($sqlBuilder, 'sorting', function ($join){
                        $join
                            ->on('products.id', '=', 'sorting.related_id');
                    })
                    ->orderBy('filter_sort');
                break;
            case 'shows':
                $query->orderBy('shows', $orderBy['direction']);
                break;
            case 'rating':
                //$query->orderBy('rating', $orderBy['direction']);
                break;
        }
        
        return $query;
    }
    
    private static function filtrateByParameters(Builder $query) 
    {
        if(!empty(self::$filters['min_price']) || !empty(self::$filters['max_price'])) 
        {
            $query
                ->whereHas('productPrices', function (Builder $query){
                    static::filtrateByPrice($query);
                });
        }
        
        if( !empty(self::$filters['country']) || !empty(self::$filters['color'])) 
        {
            $query
                ->whereHas('characteristics', function (Builder $query){
                    static::filtrateByCharacteristics($query);
                });
        }
        
        if( !empty(self::$filters['search']) ) 
        {
            $query
                ->whereHas('productProperties', function (Builder $query){
                    static::filtrateByProperties($query, self::$filters['search']);
                });
        }
        
        if( !empty(self::$filters['seller_search']) ) 
        {
            $query
                ->whereHas('productProperties', function (Builder $query){
                    static::filtrateByProperties($query, self::$filters['seller_search']);
                });
        }
        
        
        //dd($query->toSql(),  $query->getBindings());
        return $query;
    }
    
    private static function filtrateByPrice(Builder $query)
    {
        return $query
            ->when(!empty(self::$filters['min_price'])? self::$filters['min_price'] : '', function($query, $minPrice) {
                return $query
                    ->where([
                        ['is_parent', true],
                        ['min', '>=', (int)$minPrice]
                    ])
                    ->orWhere([
                        ['is_parent', true],
                        ['max', '>=', (int)$minPrice]
                    ]);
            })
            ->when(!empty(self::$filters['max_price'])? self::$filters['max_price'] : '', function($query, $maxPrice) {
                return $query
                    ->where([
                        ['is_parent', true],
                        ['max', '<=', (int)$maxPrice]
                    ]);
            }) 
            ->when(( !empty(self::$filters['min_price']) && !empty(self::$filters['max_price']) )? ['min_price' => self::$filters['min_price'], 'max_price' => self::$filters['max_price']] : '', function($query, $prices) {
                return $query->where([
                    ['is_parent', true],
                    ['min', '>=', $prices['min_price']],
                    ['max', '<=', $prices['max_price']]
                ]);
            });
    }
    
    private static function filtrateByProperties(Builder $query, $term)
    {
        return $query
            ->where([
                ['code', 'name'],
                [DB::raw('LOWER(value)'), 'like', '%'.mb_strtolower($term).'%']
            ]);
    }
    
    private static function filtrateByCharacteristics(Builder $query)
    {
        return $query
            ->when(!empty(self::$filters['country'])? self::$filters['country'] : '', function($query, $country) {
                return $query->where([
                    ['code', 'country'],
                    ['value', $country]
                ]);
            })
            ->when(!empty(self::$filters['color'])? self::$filters['color'] : '', function($query, $color) {
                return $query->where([
                    ['code', 'color'],
                    ['value', $color]
                ]);
            });
    }
    
    static function getSimilarProducts($product, int $localisationId, int $quantity) 
    {
        $similarProducts = $product
            ->profiles
            ->products()
            ->take($quantity)
            ->whereHas('downloads')
            ->with([
                'downloads' => function ($query) {
                    $query
                        ->whereIn('type', ['preview_image']);
                },
                'productPrices' => function ($query){
                    $query
                        ->where('is_parent', true); // Не является торговым предложением
                },
                'productProperties' => function ($query) use ($localisationId){
                    $query
                        ->where([
                            ['localisation_id', $localisationId],
                            ['is_parent', true], // Не является торговым предложением 
                            ['code', 'name'],
                        ]);
                },
            ])
            ->get();
        
        return $product->setRelation('similarProducts', $similarProducts);
    }
}