<?php

namespace App\Models;

use App\Extensions\Model;
use App\User;

class Localisation extends Model
{
    protected $guarded = ['id'];
	public $timestamps = false;
	
	public function profileProperties()
    {
        return $this->hasMany('App\Models\ProfileProperty');
    }
    
    public function productProperties()
    {
        return $this->hasMany('App\Models\ProductProperty');
    }
    
    public function categoryProperties()
    {
        return $this->hasMany('App\Models\CategoryProperty');
    }
    
    public function orderProperties()
    {
        return $this->hasMany('App\Models\OrderProperty');
    }
    
    public function orderItemProperties()
    {
        return $this->hasMany('App\Models\OrderItemProperty');
    }
    
    public function tenderProperties()
    {
        return $this->hasMany('App\Models\TenderProperty');
    }
    
    public static function getPossibleLocales(User $user, string $type): array
    {
        return static::whereNotIn(
                'id',
                ProfileProperty::whereHas('profiles', function ($query) use ($user, $type) {
                    $query
                        ->whereIn(
                            'id', 
                           $user
                                ->profiles()
                                ->whereIn('profile_type_id', ProfileType::where('code', $type)->get('id')->toArray())
                                ->get()
                                ->pluck('id')
                                ->toArray()
                        );
                })
                ->get()
                ->pluck('localisation_id')
                ->toArray()
            )
            ->get()
            ->pluck('code')
            ->toArray();
    }
}
