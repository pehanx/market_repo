<?php

namespace App\Models;

use App\Extensions\Model;

class Comment extends Model
{  
    public function products()
    {
        return $this->morphedByMany('App\Models\Product', 'commentable');
    }
}
