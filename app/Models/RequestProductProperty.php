<?php

namespace App\Models;

use App\Extensions\Model;

class RequestProductProperty extends Model
{
    protected $guarded = ['id'];
    
    public function localisations()
    {
        return $this->belongsTo('App\Models\Localisation', 'localisation_id');
    }
    
    public function requestProducts()
    {
        return $this->belongsTo('App\Models\RequestProduct', 'request_product_id');
    }
}