<?php

namespace App\Models;

use Spatie\TranslationLoader\LanguageLine as BaseLangLine;

class LanguageLine extends BaseLangLine
{
    public static function getCacheKey(string $group, string $locale): string
    {
        return "spatie.lang.{$group}.{$locale}";
    }
}