<?php

namespace App\Models;

use App\Extensions\Model;

class Order extends Model
{
    protected $guarded = ['id'];
    
    public function orderProperties()
    {
        return $this->hasMany('App\Models\OrderProperty');
    }
    
    public function orderItems()
    {
        return $this->hasMany('App\Models\OrderItem');
    }
}
