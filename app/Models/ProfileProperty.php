<?php

namespace App\Models;

use App\Extensions\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProfileProperty extends Model
{
    use SoftDeletes;
    
    protected $guarded = ['id'];
	
	public function profiles()
    {
        return $this->belongsTo('App\Models\Profile', 'profile_id');
    }
	
	public function localisations()
    {
        return $this->belongsTo('App\Models\Localisation', 'localisation_id');
    }
}
