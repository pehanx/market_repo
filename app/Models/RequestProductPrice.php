<?php

namespace App\Models;

use App\Extensions\Model;

class RequestProductPrice extends Model
{
    protected $guarded = ['id'];
    
    public function currencies()
    {
        return $this->belongsTo('App\Models\Currency', 'currency_id');
    }
    
    public function requestProducts()
    {
        return $this->belongsTo('App\Models\RequestProduct', 'request_product_id');
    }
}
