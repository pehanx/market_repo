<?php

namespace App\Models;

use App\Extensions\Model;

class OrderItem extends Model
{
    public function orders()
    {
        return $this->belongsTo('App\Models\Order', 'order_id');
    }
    
    public function products()
    {
        return $this->belongsTo('App\Models\Product', 'product_id');
    }
    
    public function productModels()
    {
        return $this->belongsTo('App\Models\ProductModel', 'product_model_id');
    }
    
    public function orderItemProperties()
    {
        return $this->hasMany('App\Models\OrderItemProperty');
    }
}
