<?php

namespace App\Models;

use App\Extensions\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProductModel extends Model
{
    use SoftDeletes;
     
    protected $guarded = ['id'];
    
    public function products()
    {
        return $this->belongsTo('App\Models\Product', 'product_id');
    }
    
    public function characteristics()
    {
        return $this->morphToMany('App\Models\Characteristic', 'characteristicable');
    }
    
    public function downloads()
    {
        return $this->morphToMany('App\Models\Download', 'downloadable');
    }
    
    // Торговые предложения так же имеют характеристики, отличающиеся от основного товара
    public function productProperties()
    {
        return $this->hasMany('App\Models\ProductProperty', 'product_id');
    }
    
    // Торговые предложения так же имеют цены, отличающиеся от основного товара
    public function productPrices()
    {
        return $this->hasMany('App\Models\ProductPrice', 'product_id');
    }
    
    static function getPrefixForArticul($productId)
    {
        $result = static::where('product_id', $productId)
            ->max('articul');

        if(!$result)
        {
            $result = 1;

        } elseif($result === 1)
        {
            $result = 2;

        } else
        {
            $result = ++$result;
        }
        
        return $result;
    }
    
    static function getModel(self $model, $localisationId, $appLocalisationId)
    {
        $model->load([
            'downloads' => function ($query) {
                $query
                    ->whereIn('type', ['preview_image']);
                },
                
            'productProperties' => function ($query) use($localisationId){
                $query
                    ->where([
                        ['localisation_id', $localisationId],
                        ['is_parent', false]
                    ]);
                }, 
                
            'characteristics',
            
            'productPrices' => function ($query) use($localisationId){
                $query
                    ->where('is_parent', false);
                },
        ]);

        Product::setCorrectCharacteristics($model, $appLocalisationId);
        return $model;
    }
}
