<?php

namespace App\Models;

use App\Extensions\Model;

class CurrencyRate extends Model
{
    protected $guarded = ['id'];
}
