<?php

namespace App\Models;

use App\Extensions\Model;

class SocialAccount extends Model
{
    protected $guarded = ['id'];
	
	public function users()
    {
        return $this->belongsTo('App\User', 'user_id');
    }
    
    /**
     *
     * @param string $network
     * @param integer $id
     *
     */
    static function findById(string $network, int $id)
    {
        return static::where('social', $network)
            ->where('social_id', '=', $id)
            ->first();
    }
}