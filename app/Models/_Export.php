<?php

namespace App\Models;

use App\Extensions\Model;

class Export extends Model
{
    protected $guarded = ['id'];
    
    const EXPORTED_ENTITY = [
        'product',
        'request_product'
    ];
    
    const EXPORTED_FORMATS = [
        'csv',
        'xls',
        'xlsx'
    ];
    
    public function profiles()
    {
        return $this->belongsTo('App\Models\Profile', 'profile_id');
    }

    public function downloads()
    {
        return $this->morphToMany('App\Models\Download', 'downloadable');
    }

    static function getDataForProfile(Profile $profile, $downloadTypes)
    {
        return $profile->exports()
            ->select('id', 'progress', 'created_at')
            ->with([
                'downloads' => function ($query) use($downloadTypes) {
                    $query
                        ->whereIn('type', $downloadTypes); // Пока берем тип из профиля который в сессии
                }
            ])
            ->orderBy('created_at', 'desc')
            ->orderBy('updated_at', 'desc')
            ->get();
    }

    static function getFilesForDownload(Profile $profile, int $exportId)
    {
        $exports = $profile
            ->exports()
            ->where('id', $exportId)
            ->with('downloads')
            ->first();

        $exportFilePaths = $exports
            ->downloads()
            ->pluck('path')
            ->all();

        return $exportFilePaths;
    }
}
