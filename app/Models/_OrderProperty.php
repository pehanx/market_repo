<?php

namespace App\Models;

use App\Extensions\Model;

class OrderProperty extends Model
{
    protected $guarded = ['id'];
        
    public function orders()
    {
        return $this->belongsTo('App\Models\Order', 'order_id');
    }
    
    public function localisations()
    {
        return $this->belongsTo('App\Models\Localisation', 'localisation_id');
    }
    
    public function currencies()
    {
        return $this->belongsTo('App\Models\Currency', 'currency_id');
    }
}
