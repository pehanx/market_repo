<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Kalnoy\Nestedset\NodeTrait;
use Illuminate\Support\Facades\Cache;

class Page extends Model
{
    use NodeTrait;

    protected $guarded = ['id'];
    protected $appends = ['title'];
    
    public function users()
    {
        return $this->belongsTo('App\User', 'user_id');
    }
    
    public function pageProperties()
    {
        return $this->hasMany('App\Models\PageProperty');
    }
    
    public function getTitleAttribute()
    {
        $title = '';

        if($this->pageProperties)
        {
            $title = $this->pageProperties->where('code', 'menu_title')->first() ?
                $this->pageProperties->where('code', 'menu_title')->first()->value
                : $this->pageProperties->where('code', 'title')->first()->value;
        }
        
        return $title;
    } 

    static function getFullLink(Page $page): string
    {
        return implode('/', array_merge($page->ancestors()->defaultOrder()->pluck('link')->toArray(), [$page->link]));
    } 
    
    static function getPageDescendants(int $pageId, int $localisationId)
    {
        // Если для переданной локали нет описания для страницы - такую страницу не выводим в сайдбаре
        return static::where('is_active', true)
            ->whereHas('pageProperties', function ($query) use ($localisationId) {
                    $query
                        ->where([
                            ['code', 'title'],
                            ['localisation_id', $localisationId],
                        ])
                        ->OrWhere([
                            ['code', 'menu_title'],
                            ['localisation_id', $localisationId],
                        ]);
                }
            )
            ->with([
                'pageProperties' =>  function ($query) use ($localisationId) {
                    $query
                        ->where([
                            ['code', 'title'],
                            ['localisation_id', $localisationId],
                        ])
                        ->OrWhere([
                            ['code', 'menu_title'],
                            ['localisation_id', $localisationId],
                        ]);
                }
            ])
            ->descendantsOf($pageId)
            ->transform(function ($item, $key) use($localisationId){
                $item->link = '/' . static::getFullLink($item);
                return $item;
            })
            ->toTree();
    }
}