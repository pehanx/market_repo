<?php

namespace App\Traits;

trait Downloadables 
{
     /**
     * Сохранение файлов в upload, регистрация в БД
     *
     * @param  mixed $download - Объект, которому принадлежат файлы
     * @param  array $files - Массив с файлами
     * @param  string $type - Тип файла
     *
     * @return array - Идентификаторы добавленных файлов
     */
    public function addFiles($model, array $files, string $type, $folder = 'uploads'): array
	{
        $downloads = [];
        
        foreach ($files as $key => $file)
        {
            $path = $file->store($folder, 'public');

            $downloaded = $model->downloads()->create(
            [
                'title' => $file->getClientOriginalName(),
                'path' => $path,
                'size' => $file->getSize(),
                'mime_type' => $file->getMimeType(),
                'type' => $type,
                'user_id' => auth()->user()->id,
            ]);
            
            array_push($downloads, $downloaded->id);
        }
        
        return $downloads;
	}

    /**
     * Удаление файлов с диска и БД
     *
     * @param  mixed $download - Объект, которому принадлежат файлы
     * @param  array $filesId - Массив с id файлов
     * @param  string $type - Тип файлов. Если не передан - удалим файлы всех типов.
     *
     * @return int $counter - Количество удаленных файлов
     */
    public function destroyFiles($model, array $filesId, string $type = null): int
    {
        $counter = 0;

        // Проверяем дополнительно, что файлы принадлежат сущности
        $files = $model->downloads
            ->when($type, function($query, $type) {
                return $query->where('type', $type);
            })
            ->whereIn('id', $filesId)
            ->all();

        if($files)
        {
            foreach($files as $key => $file)
            {
                $model->downloads()->detach($file->id);// Отвязали файл от сущности
                \Storage::disk('public')->delete($file->path); // Удалили из файловой системы. При необходимости можно не удалять.
                $file->forceDelete(); // Удалили информацию из БД
                ++$counter;
            } 
        }
        
        return $counter;
    }
    
    /**
     * Мягкое удаление файлов
     *
     * @param  mixed $download - Объект, которому принадлежат файлы
     * @param  array $filesId - Массив с id файлов
     * @param  string $type - Тип файлов. Если не передан - удалим файлы всех типов.
     *
     * @return int $counter - Количество удаленных файлов
     */
    public function softDeleteFiles($model, array $filesId, string $type = null): int
	{
        $counter = 0;
        
        // Проверяем дополнительно, что файлы принадлежат сущности
        $files = $model->downloads
            ->when($type, function($query, $type) {
                return $query->where('type', $type);
            })
            ->whereIn('id', $filesId)
            ->all();
            
        if($files)
        {
            foreach($files as $key => $file)
            {
                $file->delete(); // Пометили файл как удаленный
                ++$counter;
            } 
        }
        
        return $counter;
    }
    
     /**
     * Сохранение файлов в exports, регистрация в БД
     *
     * @param  mixed $model - Объект, которому принадлежат файлы
     * @param  array $files - Массив с файлами
     * @param  string $type - Тип файла
     *
     */
    public function associateWithFiles($model, array $files, string $type, int $userId)
	{
        $model->downloads()->createMany(
            $this->prepareAssociatedFileArray($files, $type, $userId)
        );
    }
    
    /**
     * Возвращаем корректные файлы, переданные на клиенте
     *
     * @param  array $allFiles - Массив со всеми файлами, прилетевшими с инпутов
     * @param  array $deletedFiles - Массив с названиями инпутов и файлов, которые из них нужно удалить
     *
     */
    public function filtrateInputFiles($allFiles, $deletedFiles)
	{
        foreach ($allFiles as $inputName => $element) 
        {
            if(is_object($element))
            {
                $name = $element->getClientOriginalName();
                
                if(!empty($deletedFiles[$inputName]) && in_array($name, $deletedFiles[$inputName]))
                {
                    unset($deletedFiles[$inputName][ array_search($name, $deletedFiles[$inputName]) ]);
                    unset($allFiles[$inputName]);
                    continue;
                }
                
            } elseif(is_array($element))
            {
                foreach ($element as $index => $file) 
                {
                    $name = $file->getClientOriginalName();
                     
                    if(!empty($deletedFiles[$inputName]) && in_array($name, $deletedFiles[$inputName]))
                    {
                        unset($deletedFiles[$inputName][ array_search($name, $deletedFiles[$inputName]) ]);
                        unset($allFiles[$inputName][$index]);
                        continue;
                    }
                    
                } 
            }
        }
        
        if(!array_filter($deletedFiles))
        {
           return $allFiles;
           
        } else
        {
           abort(422);
        }
    }

    /**
     * Подготовка массива с данными для архивов с экспорта
     * Нужен для того чтобы снизить количество запросов в бд
     *
     * @param  mixed $data - Массив, в который записваем файлы
     * @param  array $files - Массив с файлами
     * @param  string $type - Тип файла
     *
     * @return array - Добавленные файлы
     */
    private function prepareAssociatedFileArray(array $files, string $type, int $userId): array
    {
        $data = [];

        foreach ($files as $key => $file)
        {
            $filePath = str_replace(
                realpath(\Storage::disk(env('FILESYSTEM_DRIVER'))->path('') . 'public/'),
                '',
                $file->getRealPath()
            );
            
            array_push($data, [
                'title' => basename($file->path()),
                'path' => $filePath,
                'size' => $file->getSize(),
                'mime_type' => $file->getMimeType(),
                'type' => $type,
                'user_id' => $userId,
            ]);
        }

        return $data;
    }
}
