<?php

namespace App\Mails;

use App\User;
use App\Models\Profile;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Database\Eloquent\Collection;

class InviteUserMail extends Mailable
{
    use SerializesModels;

    public $user;

    public function __construct(User $user)
    {
        $this->user = $user->toArray();
    }

    public function build()
    {
        $this
            ->subject("Вы получили приглашение")
            ->markdown('emails.login_link');
        
        return $this;
    }
}
