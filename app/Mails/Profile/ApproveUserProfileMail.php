<?php

namespace App\Mails\Profile;

use App\User;
use App\Models\Profile;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Database\Eloquent\Collection;

class ApproveUserProfileMail extends Mailable
{
    use SerializesModels;

    public $user;
    public $profile;
    public $profileDocuments;

    public function __construct(User $user, Profile $profile, Collection $profileDocuments)
    {
        $this->user = $user;
        $this->profile = $profile;
        $this->profileDocuments = $profileDocuments;
    }

    public function build()
    {
        $this
            ->subject(__('mails.approve_user_profile', ['name' => $this->user->name]))
            ->markdown('emails.profile.approve');

        $this->profileDocuments->each(function ($document, $key) {
            $this->attach( storage_path('app/public/' . $document->path) );
        });
        
        return $this;
    }
}
