<?php

namespace App\Mails;

use App\User;
use App\Models\Profile;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Database\Eloquent\Collection;

class WriteSupplierMail extends Mailable
{
    use SerializesModels;

    public $data;

    public function __construct(array $data)
    {
        $this->data = $data;
    }

    public function build()
    {
        $this
            ->subject("Новая заявка")
            ->markdown('emails.write_supplier');
        
        return $this;
    }
}
