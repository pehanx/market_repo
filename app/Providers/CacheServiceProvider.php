<?php

namespace App\Providers;

use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Redis;
use Illuminate\Support\Str;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use App\Models\{
    CategoryProperty,
    ProfileProperty,
    PageProperty,
    RequestProductProperty,
    ProductProperty,
    Characteristic,
    RequestProductPrice,
    ProductPrice,
    ProductModel,
    RequestProduct,
    Product,
    Shop,
    Page,
    Category,
};
use App\ApplicationDictionary;

/*
 *  Чистим (инвалидируем) кеш при заданных событиях в registerEvents. 
 *  Методы *forget используют инстанс модели, на которой произошло событие.    
 */
class CacheServiceProvider extends ServiceProvider
{
    private $modelTagsFlush = [
        //Model::class
    ];
    
    private $modelKeysPatternFlush = [
        
        Category::class => [
            '*links*' // Ссылки на все существующие категории для валидации в CategoryPath
        ],
        
        CategoryProperty::class => [
            '*all_categories_tree*', // Список всех категорий в /catalog
            '*main_menu*', // Меню каталога в header
            '*main_search*', // Поиск в header
            '*full_tree*', // Выбор категории при создании товара\запроса
        ],
        
        Page::class => [
            '*page_path_*' // Рекурсивная ссылка от parent на статическую страницу в PagePath
        ],
        
        ApplicationDictionary::class => [
            '*shop_interface_*' // Интерфейс магазина поставщика
        ] 
    ];
    
    // Убить кеш с тегом (ключ массива) при событии в связанных моделях
    private $relatedModelTagsFlush = [
        
        Shop::class => [
            ProductProperty::class,
            ProductPrice::class,
            CategoryProperty::class,
            ProfileProperty::class
        ],
        
        Page::class => [
            PageProperty::class
        ], 
    ];
    // Метод похож на forgetByKeysPattern без использования eventTarget $model
    private $relatedModelKeysPatternFlush = [
        
       /*  BaseModel::class => [
            'related_models' => [
                RelatedModel::class,
                RelatedModel2::class 
            ],
            
            'keys_patterns' => [
                "*key_*_{id}*",
                "*key_2_*_{id}*"
            ]
        ] */
    ];
    
    private $modelKeysPatternForget = [
        /* Model::class => [
            '*key_{id}*',
            '*key_2_{id}*' 
        ], */
    ];
    
    // При событии на основной модели и зависимых - убить кеш с определенными ключами
    private $relatedModelKeysPatternForget = [
        Product::class => [
            'related_models' => [
                ProductProperty::class,
                ProductPrice::class // is_parent: true - товар. 
            ],
            
            'keys_patterns' => [
                "*catalog_show_product_*_{id}*"
            ]
        ],
        
        RequestProduct::class => [
            'related_models' => [
                RequestProductProperty::class,
                RequestProductPrice::class
            ],
            
            'keys_patterns' => [
                "*catalog_show_request_*_{id}*"
            ]
        ],
    ];

    public function boot(): void
    {
        /* foreach ($this->modelTagsFlush as $className) 
        {
            if(class_exists($className))
            {
                $this->registerEvents($className, $this->flushByTag($className));
            }
        }
        
        unset($className); */
        
        foreach ($this->relatedModelTagsFlush as $tag => $relatedModels) 
        {
            foreach ($relatedModels as $className) 
            {
                if(class_exists($className))
                {
                    $this->registerEvents($className, $this->flushByTag($tag));
                }
            }
        }
        
        unset($className);
        
        foreach ($this->modelKeysPatternFlush as $className => $keysPatterns) 
        {
            if(class_exists($className))
            {
                $this->registerEvents($className, $this->flushByKeysPattern($keysPatterns));
            }
        }
        
        unset($className, $keysPatterns);
        
        foreach ($this->relatedModelKeysPatternForget as $baseClassName => $fields) 
        {
            // Вешаем события на основной класс тоже
            if(class_exists($baseClassName))
            {
                $this->registerEvents($baseClassName, $this->forgetByKeysPattern($fields['keys_patterns'], $baseClassName) );
            }
                
            foreach ($fields['related_models'] as $className) 
            {
                if(class_exists($baseClassName) && class_exists($className))
                {
                    $this->registerEvents( $className, $this->forgetByKeysPattern($fields['keys_patterns'], $baseClassName) );
                }
            }
        }
        
        unset($className);
        
        /* foreach ($this->modelKeysPatternForget as $className => $keysPatterns) 
        {
            if(class_exists($className))
            {
                $this->registerEvents($className, $this->forgetByKeysPattern($keysPatterns, $className) );
            }
        }
        
        unset($className, $keysPatterns); */
    }

    private function registerEvents($className, $callback): void
    {
        $className::created($callback);
        $className::saved($callback);
        $className::updated($callback);
        $className::deleted($callback);  
    }

    private function flushByTag($tag)
    {
        return function() use ($tag)
        {
            Cache::tags($tag)->flush();
        };
    }
    
    private function flushByKeysPattern($keysPatterns)
    {
        return function() use ($keysPatterns)
        {
            foreach ($keysPatterns as $keyPattern)
            {
                $keys = Redis::command('keys', [$keyPattern]);

                if(!!$keys)
                {
                    Redis::command('del', $keys);
                }
            }
        };
    }
    
    private function forgetByKeysPattern($keysPatterns, $baseClassName)
    {
        return function($model) use ($keysPatterns, $baseClassName)
        {
            $id = null;
            
            if(get_class($model) === $baseClassName)
            {
                $id = $model->id;
                
            } else
            {
                $relation = (new $baseClassName)->getTable();
                $model = $model->fresh();

                if(isset($model->is_parent) && $model->is_parent)
                {
                    $relatedModel = $model->$relation;

                } elseif (isset($model->is_parent) && !$model->is_parent)
                {
                    $belongsTo = ProductModel::find($model->product_id);
                    $relatedModel = $belongsTo->$relation;
                }

                if(!isset($relatedModel) || !$relatedModel)
                {
                    return;
                }
                
                $id = $relatedModel->id;
                
                unset($relation, $relatedModel);
            }
            
            foreach ($keysPatterns as $keyPattern)
            {
                $keyPattern = Str::replaceFirst('{id}', $id, $keyPattern);
                $keys = Redis::command('keys', [$keyPattern]);

                if(!!$keys)
                {
                    Redis::command('del', $keys);
                }
            }
        };
    }
}