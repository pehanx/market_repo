<?php

namespace App\Providers;

use App\Services\Translate\{TranslatorInterface, AsiaTranslator, YandexTranslator};
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Support\ServiceProvider;

class TranslateServiceProvider extends ServiceProvider
{
    public function register(): void
    {
        $this->app->singleton(TranslatorInterface::class, function (Application $app) {

            $config = $app->make('config')->get('translate');

            switch ($config['driver']) 
            {
                case 'asia':
                    $params = $config['drivers']['asia'];
                    return new AsiaTranslator($params['key'], $params['url']);
                    
                case 'yandex':
                    return new YandexTranslator();
                    
                default:
                    throw new \InvalidArgumentException('Undefined Translate driver ' . $config['driver']);
            }
        });
    }
}
