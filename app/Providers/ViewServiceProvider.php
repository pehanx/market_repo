<?php

namespace App\Providers;

use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;
use App\Http\ViewComposers\{
    MainHeaderComposer,
    CatalogMenuComposer,
    MainSearchComposer,
    CurrencyRateComposer,
    HtmlLangComposer
};

class ViewServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        // Using Closure based composers...
        View::composer('layouts.market.header', MainHeaderComposer::class);
        View::composer('layouts.market.header', CatalogMenuComposer::class);
        View::composer('widgets.search', MainSearchComposer::class);
        View::composer('layouts.market.cabinet.header', MainHeaderComposer::class);
        View::composer('*', CurrencyRateComposer::class);
        View::composer('layouts.market.*index', HtmlLangComposer::class);
    }
}