<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\View;
use App\Models\Category;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {

    }

    /**
     * Bootstrap any application services.
     * Меню каталога в header. Берем без root
     *
     * @return void
     */
    public function boot()
    {
        //dd(auth()->user());
        //View::share('currencyRate', $currencyRate);
        //View::share('currencySymbol', $currencySymbol);
    }
}
