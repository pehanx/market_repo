<?php

namespace App\Providers;

use Illuminate\Auth\Events\Registered;
use App\Events\Profile\{ProfileAdded, ProfileDeleted, ProfileApproved};
use App\Events\Chat\{ChatPush, ChatDBSend, ChatSupportPush, ChatSupportSend};
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Event;
use App\Listeners\Auth\MemberRoleAssignedListener;
use App\Listeners\Chat\{ChatDBListener, ChatPushListener, ChatSupportDBListener, ChatSupportPushListener};
use App\Listeners\Profile\{RoleAssignedListener, ProfileApprovedListener, RoleDeletedListener};

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        Registered::class => [
            // присвоение пользователю роли member. До регистрации используется guest(проверка через стандартный laravel midleware)
            MemberRoleAssignedListener::class,
        ],
        
        ProfileAdded::class => [
            RoleAssignedListener::class,
        ],
        
        ProfileDeleted::class => [
            RoleDeletedListener::class,
        ],
        
        ProfileApproved::class => [
            ProfileApprovedListener::class,
        ],

        ChatDBSend::class => [
            ChatDBListener::class,
        ],

        ChatPush::class => [
            ChatPushListener::class,
        ],

        ChatSupportPush::class => [
            ChatSupportPushListener::class,
        ],

        ChatSupportSend::class => [
            ChatSupportDBListener::class,
        ],

        'Illuminate\Cache\Events\KeyForgotten' => [
            'App\Listeners\TestListener',
        ],
        
        \SocialiteProviders\Manager\SocialiteWasCalled::class => [
            'SocialiteProviders\\VKontakte\\VKontakteExtendSocialite@handle',
            'SocialiteProviders\\Odnoklassniki\\OdnoklassnikiExtendSocialite@handle',
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
