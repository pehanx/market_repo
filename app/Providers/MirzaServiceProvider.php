<?php

namespace App\Providers;

use Blade;
use Illuminate\Support\ServiceProvider;
use App\Extensions\Mirza;
use App\Extensions\MirzaClient;

class MirzaServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        \App::singleton('MirzaClient', function () {
          
            return new MirzaClient(config('mirza.secret'));
        });
        \App::bind('Mirza', function () {
            $client = resolve('MirzaClient');
            return new Mirza($client);
        });

        \App::alias('Mirza', 'yak0d3\Mirza_Yandex_Translator\MirzaFacade');
    }
}
