<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Spatie\Permission\Traits\HasRoles;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Builder;
use App\Models\{Profile, Localisation, ProfileType};
use Illuminate\Database\Eloquent\SoftDeletes;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable
{
    use Notifiable, HasRoles, SoftDeletes, HasApiTokens;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'ip', 'registration_type', 'session_id'
    ];

    /**
     * До этих свойств в виде не достучаться
     *
     * @var array
     */
    protected $hidden = [
        'password', 'ip'
    ];
	
	public function profiles()
    {
        return $this->belongsToMany('App\Models\Profile');
    }

    public function managers()
    {
        return $this->belongsToMany('App\Models\Manager');
    }

    public function chats()
    {
        return $this->morphToMany('App\Models\Chat', 'chatable');
    }
    
    public function socialAccounts()
    {
        return $this->hasMany('App\Models\SocialAccount');
    }

    public function devices()
    {
        return $this->belongsToMany('App\Models\Device');
    }
    
    public function downloads()
    {
        return $this->morphToMany('App\Models\Download', 'downloadable');
    }
    
    public function favorites()
    {
        return $this->hasMany('App\Models\Favorite');
    }
    
    public function pages()
    {
        return $this->hasMany('App\Models\Page');
    }

    public function settings()
    {
        return $this->morphToMany('App\Models\Setting', 'settingable');
    }

    public function isOnline()
    {
        return (bool) $this->session_id;
    }
    
    /**
     *
     * @param string $email
     * @param array $types
     *
     */
    static function findByEmail(string $email, array $types)
    {
        return static::where('email', $email)
            ->whereIn('registration_type', $types)
            ->first();
    }
    
    static function getUsersWithProfiles(Request $request, $needPages = 10)
    {
        return static::role(['buyer', 'seller'])
            ->whereHas('profiles', function (Builder $query)  use ($request) {
                static::scopeUsersWithProfiles($query, $request);
            })
            ->when($request->has('login')? $request->login: '', function($query, $login) {
                return $query->where('name', $login);
            })
            ->with([
               'profiles' => function ($query) use ($request){
                    static::scopeUsersWithProfiles($query, $request)
                    ->with(['profileProperties', 'profileTypes']);
                }
            ])
            ->paginate($needPages);
    }
    
    static function getUsersWithAccounts(Request $request, $needPages = 10)
    {
        return static::with([
           'profiles' => function ($query) {
                $query
                    ->whereHas('profileProperties', function (Builder $query) {
                        $query
                            ->whereIn('localisation_id', Localisation::whereIn('code', ['ru', 'en'])->pluck('id')->toArray());   
                    })
                    ->whereHas('profileTypes', function (Builder $query) {
                        $query
                            ->where('code', 'account');   
                    })
                    ->with(['profileProperties', 'profileTypes']);
            },
            
            'roles', 'socialAccounts'
        ])
        ->paginate($needPages);
    }
    
    private static function scopeUsersWithProfiles($query, Request $request)
    {
        return $query
            ->when($request->has('status')? $request->status: '', function($query, $status) {
                return $query->where('status', $status);
            })
            ->when($request->has('id')? $request->id: '', function($query, $id) {
                return $query->where('profile_id', $id);
            })
            ->whereHas('profileProperties', function (Builder $query) use ($request){
                $query
                    ->whereIn('localisation_id', Localisation::whereIn('code', ['ru', 'en'])->pluck('id')->toArray())
                    ->when($request->has('name')? $request->name : '', function($query, $name) {
                        return $query
                            ->where([
                                ['code', 'company_name'],
                                ['value', $name]
                            ]);
                    }); 
            })
            ->whereHas('profileTypes', function (Builder $query) use ($request){
                $query
                    ->where('code', '!=', 'account')
                    ->when($request->has('type')? $request->type : '', function($query, $type) {
                        return $query
                            ->where('code', $type);
                    }); 
            });
    }
    
    static function getManagersForProfile(Profile $profile, $needPages = 10)
    {
        return $profile->users()
            ->role(ProfileType::where('id', $profile->profile_type_id)->pluck('code')->shift().'_manager') // Отображаем менеджеров с правами, соответствующими типу профиля
            ->with([
                'profiles' => function ($query){
                    $query
                        ->where('profile_type_id', ProfileType::where('code', 'account')->pluck('id')->shift())
                        ->with([
                            'profileProperties' => function ($query){
                                $query
                                    ->whereIn('localisation_id', Localisation::whereIn('code', ['ru', 'en'])->pluck('id')->toArray()); 
                            }
                        ]);
                }
            ])
            ->paginate($needPages);
    }
    
    static function getUserWithAvatar(int $userId)
    {
        return static::where('id', $userId)->first()->load(['downloads' => function ($query) {
                $query
                    ->where('type', 'avatar');
                }
            ]);
    }

    /* 
     * Получение конкретного параметра найстройки пользователя
     */
    public static function getUserSetting(int $userId, string $type)
    {
        $setting = static::where('id', $userId)->first()
            ->settings()
            ->where('type',$type)
            ->first();

        if($setting !== null)
        {
            return $setting->value;
        }
        
        return null;
    }
}
