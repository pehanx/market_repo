const mix = require('laravel-mix');
const webpack = require('webpack');
const mode = JSON.stringify(process.env.NODE_ENV);
require('laravel-mix-merge-manifest');

mix
    .setPublicPath('public/assemble')
    .setResourceRoot('/assemble/')
    
    // market layout
    .sass('resources/scss/layouts/market/main.scss', 'public/assemble/css/main.css')
    .copy('resources/js/common/Notify.js', 'public/assemble/js/')
    .copy('resources/js/common/modals/Info.js', 'public/assemble/js/')
    .copy('resources/js/common/Cabinet.js', 'public/assemble/js/')
    .copy('resources/js/common/Tree.js', 'public/assemble/js/')
    .copy('resources/js/common/widgets/search.js', 'public/assemble/js/widgets')    
    .copyDirectory('resources/images', 'public/assemble/images/')
    .copy('node_modules/wfk-montserrat/fonts',  'public/assemble/fonts')
    .js('resources/js/app.js', 'js')
    .js('resources/js/layouts/market/index.js', 'js')
    .js('resources/js/common/endpoints/search.js', 'js')
    .js('resources/js/common/endpoints/catalog.js', 'js')
    .js('resources/js/common/endpoints/main.js', 'js')
    .js('resources/js/common/endpoints/cabinet/product_create.js', 'js')
    .js('resources/js/common/endpoints/cabinet/product_model_add.js', 'js')
    .js('resources/js/common/endpoints/cabinet/product_model_edit.js', 'js')
    .js('resources/js/common/endpoints/cabinet/profile_create.js', 'js')
    .js('resources/js/common/endpoints/cabinet/profile_edit.js', 'js')
    .js('resources/js/common/endpoints/cabinet/product_edit.js', 'js')
    .js('resources/js/common/endpoints/cabinet/account_edit.js', 'js')
    .js('resources/js/common/endpoints/cabinet/request_product_create.js', 'js')
    .js('resources/js/common/endpoints/cabinet/request_product_edit.js', 'js')
    .js('resources/js/common/endpoints/shop/seller.js', 'js')
    .js('resources/js/common/endpoints/shop/seller_main.js', 'js')
    .js('resources/js/common/endpoints/shop/seller_product.js', 'js')
    .js('resources/js/common/endpoints/cabinet/chat.js', 'js')
    .js('resources/js/common/endpoints/cabinet/chat_admin.js', 'js')
    .js('resources/js/common/endpoints/admin/admin_common.js', 'js')
    
    // admin layout
    .extract(['bootstrap'])
    .sass('resources/scss/layouts/admin/main.scss', 'public/assemble/css/admin.css')
    .copy('resources/js/common/api/SummernoteCallbacks.js', 'public/assemble/js/api')
    
    if(mode.indexOf("production") === -1)
    {
        mix
            .version()
            .sourceMaps()
    }