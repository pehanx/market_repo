<?php
return [
    'driver' => env('TRANSLATE_DRIVER', 'asia'),
    'drivers' => [
        'asia' => [
            'key' => env('ASIA_KEY'),
            'url' => env('ASIA_URL'),
        ],
    ],
];