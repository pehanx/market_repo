class Notify
{
    /**
     * Читает уведомления из session
     */
    constructor()
    {
        this.options = {
            "closeButton": true,
            "debug": false,
            "newestOnTop": true,
            "positionClass": "toast-top-right",
            "preventDuplicates": false,
            "showDuration": "1000",
            "hideDuration": "1000",
            "timeOut": "5000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        };
    }

    /**
     * @param string body - текст сообщения
     * @param string title - заголовок сообщения
     * @param string type - тип сообщения
     */
    show(body = '', title = '', type = 'info')
    {
        toastr.options = this.options;
        toastr[type](body, title);
    }
}